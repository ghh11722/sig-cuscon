#!/bin/bash
#*******************************************************************************
# * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
# * プログラム名 : KDDIセキュリティGWシステム カスコン
# * ファイル名   : update_systeminfo.sh
# *
# * [変更履歴]
# * 日付       更新者             内容
# * 2013/09/01 kkato@PROSITE      初版作成
# * 2016/11/14 M.Tsunashima@PLUM  Terasoluna/PowerGresアップデート
# * 2019/06/24 M.Tsunashima@PLUM  JDBCドライバアップデート
#*******************************************************************************--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------
# System Setting                                                               --
#--------------------------------------------------------------------------------
# JAVA_HOME設定
JAVA_HOME=/usr/java/default
# バッチ処理実行フォルダ
BIN_FLD=/usr/local/cuscon-batch/conf;
# Lock File
#LOCKFILE=.lockfile_systeminfo
LOCKFILE=/var/tmp/lockfile_systeminfo

# 実行パス設定
PATH=$PATH:$JAVA_HOME/bin
PATH=$PATH:/usr/bin:/sbin:/usr/local/cuscon-batch/bin

# クラスパス設定
CLASSPATH=$CLASSPATH:$JAVA_HOME/lib/rt.jar
CLASSPATH=$CLASSPATH:$JAVA_HOME/lib/jsse.jar
CLASSPATH=$CLASSPATH:$JAVA_HOME/lib/jce.jar
CLASSPATH=$CLASSPATH:$JAVA_HOME/lib/charsets.jar
CLASSPATH=$CLASSPATH:$JAVA_HOME/lib/deploy.jar
CLASSPATH=$CLASSPATH:$JAVA_HOME/lib/javaws.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/aspectjweaver-1.7.4.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/commons-collections-3.2.2.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/commons-logging-1.1.3.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/commons-net-3.5.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/mybatis-2.3.5.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/jsch-0.1.54.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/log4j-1.2.16.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/postgresql-42.2.5.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/aopalliance-1.0.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/spring-beans-3.2.13.RELEASE.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/spring-context-3.2.13.RELEASE.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/spring-core-3.2.13.RELEASE.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/spring-aop-3.2.13.RELEASE.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/spring-jdbc-3.2.13.RELEASE.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/spring-tx-3.2.13.RELEASE.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/spring-expression-3.2.13.RELEASE.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/spring-orm-3.2.13.RELEASE.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/spring-struts-3.2.13.RELEASE.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/spring-web-3.2.13.RELEASE.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/terasoluna-commons-3.3.1.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/terasoluna-dao-3.3.1.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/terasoluna-ibatis-3.3.1.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/terasoluna-thin-2.0.6.2.jar
CLASSPATH=$CLASSPATH:/usr/local/cuscon-batch/lib/Cusconbatch.jar


# 設定読み込み
LANG=ja_JP.UTF-8
export JAVA_HOME PATH CLASSPATH LANG

# 稼働系チェック
service httpd status > /dev/null 2>&1
if [ $? -ne 0 ]
then
  echo "待機系"
  exit
fi

# 実行ディレクトリに移動
cd $BIN_FLD;

# 多重起動チェック
if [ -f $LOCKFILE ]; then
  echo "多重起動"
  exit
fi
touch $LOCKFILE

# JAVA起動
$JAVA_HOME/bin/java jp.co.kddi.secgw.cuscon.batch.master.GetSystemInfo

rm -f $LOCKFILE
