#!/bin/bash

# 環境変数設定
# ※psqlコマンドが実行できるように環境変数を設定する。
#export PATH="/usr/kerberos/bin:/usr/local/bin:/bin:/sbin:/usr/bin:/usr/local/jdk/bin:/usr/local/pgsql/bin:/home/postgres/bin"
export PATH="/opt/powergres/bin:/usr/kerberos/sbin:/usr/kerberos/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin:$PATH"

# 変数設定
db_name="cuscondb"              # データベース名
db_user="postgres"              # PostgreSQL接続ユーザ
db_pswd="#=gIHT%u"              # PostgreSQL接続パスワード

# 稼働系チェック
service httpd status > /dev/null 2>&1
if [ $? -ne 0 ]
then
  echo "cuscon_vacuum.sh 待機系のためスキップ"
  exit
fi

# T_LogTraffic vacuumdb実行
echo "[T_LogTraffic Vacuum開始]" && date
PGPASSWORD="$db_pswd" vacuumdb -d "$db_name" -U "$db_user" -z -v -t T_LogTraffic
date && echo "[T_LogTraffic Vacuum完了]"

# T_LogIdsIps vacuumdb実行
echo "[T_LogIdsIps Vacuum開始]" && date
PGPASSWORD="$db_pswd" vacuumdb -d "$db_name" -U "$db_user" -z -v -t T_LogIdsIps
date && echo "[T_LogIdsIps Vacuum完了]"

# T_LogUrlFilter vacuumdb実行
echo "[T_LogUrlFilter Vacuum開始]" && date
PGPASSWORD="$db_pswd" vacuumdb -d "$db_name" -U "$db_user" -z -v -t T_LogUrlFilter
date && echo "[T_LogUrlFilter Vacuum完了]"

# T_LogVirusCheck vacuumdb実行
echo "[T_LogVirusCheck Vacuum開始]" && date
PGPASSWORD="$db_pswd" vacuumdb -d "$db_name" -U "$db_user" -z -v -t T_LogVirusCheck
date && echo "[T_LogVirusCheck Vacuum完了]"

# T_LogSpyware vacuumdb実行
echo "[T_LogSpyware Vacuum開始]" && date
PGPASSWORD="$db_pswd" vacuumdb -d "$db_name" -U "$db_user" -z -v -t T_LogSpyware
date && echo "[T_LogSpyware Vacuum完了]"

# T_TopSrcSummary vacuumdb実行
echo "[T_TopSrcSummary Vacuum開始]" && date
PGPASSWORD="$db_pswd" vacuumdb -d "$db_name" -U "$db_user" -z -v -t T_TopSrcSummary
date && echo "[T_TopSrcSummary Vacuum完了]"

# T_TopDstSummary vacuumdb実行
echo "[T_TopDstSummary Vacuum開始]" && date
PGPASSWORD="$db_pswd" vacuumdb -d "$db_name" -U "$db_user" -z -v -t T_TopDstSummary
date && echo "[T_TopDstSummary Vacuum完了]"

# T_TopAppSummary vacuumdb実行
echo "[T_TopAppSummary Vacuum開始]" && date
PGPASSWORD="$db_pswd" vacuumdb -d "$db_name" -U "$db_user" -z -v -t T_TopAppSummary
date && echo "[T_TopAppSummary Vacuum完了]"

# T_TopDstCountriesSummary vacuumdb実行
echo "[T_TopDstCountriesSummary Vacuum開始]" && date
PGPASSWORD="$db_pswd" vacuumdb -d "$db_name" -U "$db_user" -z -v -t T_TopDstCountriesSummary
date && echo "[T_TopDstCountriesSummary Vacuum完了]"

# T_TopAttacksSummary vacuumdb実行
echo "[T_TopAttacksSummary Vacuum開始]" && date
PGPASSWORD="$db_pswd" vacuumdb -d "$db_name" -U "$db_user" -z -v -t T_TopAttacksSummary
date && echo "[T_TopAttacksSummary Vacuum完了]"

exit 0
