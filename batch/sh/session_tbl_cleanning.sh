#!/bin/bash
#*******************************************************************************
# * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
# * プログラム名 : KDDIセキュリティGWシステム カスコン
# * ファイル名   : session_tbl_cleanning.sh
# *
# * [変更履歴]
# * 日付        更新者       内容
# * 2010/03/10  h.kubo@JCCH  初版作成
# * 2010/07/02  T.Suzuki@NOP コミットプロセスロックファイル削除追加
#*******************************************************************************

### 変数定義 ###
SERVICE_NAME=httpd
SLEEP_TIME=10

JAVA_HOME=/usr/java/default
BASE_DIR=/usr/local/cuscon-batch

MAIN_CLASS=jp.co.kddi.secgw.cuscon.batch.SessionTableDelete
CONFIG_FILE=$BASE_DIR/conf/context.xml
MESSAGE_POPERTIES=$BASE_DIR/conf/shellmessage.properties
LOG4J_POPERTIES=$BASE_DIR/conf/log4j_batch.properties

# Commit Lock File
COMMITLOCKFILE=/var/tmp/LOCK.COMMITPROC

### 実行環境設定 ###
export LANG="ja_JP.UTF-8"
export JAVA_HOME=$JAVA_HOME
export PATH=$JAVA_HOME/bin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

### 1. 多重起動チェック ###
if [ $$ != `pgrep -fo $0`  ]; then
  echo "すでに実行中なので終了"
  exit
fi

### 停止中フラグON ###
STOP_FLAG=1

while :
do
  ### 2. httpdサービス状態チェック ###
  service $SERVICE_NAME status > /dev/null 2>&1
  if [ $? -eq 0 ]
  then
    ### 3. 停止中フラグチェック ###
    if [ $STOP_FLAG -eq 1 ]
    then
      ### 停止中フラグOFF ###
      STOP_FLAG=0

      ### PowerGres起動までの猶予 ###
      sleep 60

      ### クラスパス設定 ###
      CLASSPATH=$BASE_DIR/lib

      ### ユーザライブラリをクラスパスに追加 ###
      for file in `ls $BASE_DIR/lib`; do
        CLASSPATH=$CLASSPATH:$BASE_DIR/lib/$file
      done

      ### 4. セッション管理テーブル削除(Java 実行) ###
      java -Dlog4j.configuration=file://$LOG4J_POPERTIES -classpath $CLASSPATH $MAIN_CLASS "$CONFIG_FILE" "$MESSAGE_POPERTIES"

      ### 5. コミットプロセスロックファイル削除 ###
      rm -f $COMMITLOCKFILE
    fi
  else
    ### 停止中フラグON ###
    STOP_FLAG=1
  fi

  ### 7. Sleep(10秒) ###
  sleep $SLEEP_TIME

done
