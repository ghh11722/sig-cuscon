#!/bin/bash
#*******************************************************************************
# * Copyright(c) 2011 KDDI CORPORATION. All Rights Reserved.
# * プログラム名 : KDDIセキュリティGWシステム カスコン
# * ファイル名   : downloadlog_import.sh
# *
# * [変更履歴]
# * 日付       更新者            内容
# * 2011/05/25  inoue@prosite  初期版作成
#*******************************************************************************

### 変数定義 ###
JAVA_HOME=/usr/java/default
BASE_DIR=/usr/local/cuscon-batch

MAIN_CLASS=jp.co.kddi.secgw.cuscon.batch.DownloadLogImport
CONFIG_FILE=$BASE_DIR/conf/context.xml
MESSAGE_POPERTIES=$BASE_DIR/conf/shellmessage.properties
LOG4J_POPERTIES=$BASE_DIR/conf/log4j_batch.properties

### 実行環境設定 ###
export LANG="ja_JP.UTF-8"
export JAVA_HOME=$JAVA_HOME
export PATH=$JAVA_HOME/bin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

# Lock File
LOCKFILE=/var/tmp/LOCK.DLLOGIMPORT

### 多重起動チェック ###
#if [ $$ != `pgrep -fo $0`  ]; then
#  echo "すでに実行中なので終了"
#  exit
#fi

### 稼働系チェック ###
service httpd status > /dev/null 2>&1
if [ $? -ne 0 ]
then
  echo "待機系なので終了"
  exit
fi

### クラスパス設定 ###
CLASSPATH=$BASE_DIR/lib

### ユーザライブラリをクラスパスに追加 ###
for file in `ls $BASE_DIR/lib`; do
  CLASSPATH=$CLASSPATH:$BASE_DIR/lib/$file
done

# 多重起動チェック
if [ -f $LOCKFILE ]; then
  echo "多重起動"
  exit
fi
touch $LOCKFILE

### Java 実行 ###
java -Dlog4j.configuration=file://$LOG4J_POPERTIES -classpath $CLASSPATH $MAIN_CLASS "$CONFIG_FILE" "$MESSAGE_POPERTIES"

rm -rf $LOCKFILE
