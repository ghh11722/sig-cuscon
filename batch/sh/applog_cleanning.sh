#!/bin/bash
#*******************************************************************************
# * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
# * プログラム名 : KDDIセキュリティGWシステム カスコン
# * ファイル名   : applog_cleanning.sh
# *
# * [変更履歴]
# * 日付       更新者             内容
# * 2010/03/12 m.narikawa@JCCH    初期版作成
# * 2016/11/14 M.Tsunashima@PLUM  Tomcat6対応（ログ出力先変更）
#*******************************************************************************

### 変数定義 ###
LOG_PATH=/var/log/tomcat6/
DELETE_DATE=+90

### 実行環境設定 ###
export PATH=/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/cuscon-batch/bin

### DELETE_DATEを経過したファイルを削除 ###
find $LOG_PATH -name 'cuscon.log_*' -mtime $DELETE_DATE -exec rm -f {} \;
find $LOG_PATH -name 'cuscon-commit-proc.log_*' -mtime $DELETE_DATE -exec rm -f {} \;
find $LOG_PATH -name 'cuscon-batch.log_*' -mtime $DELETE_DATE -exec rm -f {} \;
find $LOG_PATH -name 'cuscon-batch-SessionInit.log_*' -mtime $DELETE_DATE -exec rm -f {} \;
find $LOG_PATH -name 'cuscon-batch-ModeSet.log_*' -mtime $DELETE_DATE -exec rm -f {} \;
find $LOG_PATH -name 'cuscon-batch-LogImport.log_*' -mtime $DELETE_DATE -exec rm -f {} \;
find $LOG_PATH -name 'cuscon-batch-LogDelete.log_*' -mtime $DELETE_DATE -exec rm -f {} \;
find $LOG_PATH -name 'cuscon-batch-DownloadLogImport.log_*' -mtime $DELETE_DATE -exec rm -f {} \;

exit 0
