#!/bin/sh
#
# プロセス存在チェック
# /usr/local/cuscon-batch/bin/update_report.sh
#
PS_NAME=update_report

echo "[chk_report_batch開始]" && date

PSCount=`ps axu | grep $PS_NAME | grep grep -v | wc -l`

if [ $PSCount = 0 ]; then
    date && echo "[chk_report_batch完了]"
    exit 0
fi

# プロセスが残っている場合の処理
echo "[FATAL]レポート取得処理が規定時間を超えています。"
date && echo "[chk_report_batch完了]"
exit 1


