CREATE OR REPLACE FUNCTION M_SubCategory_insert(varchar) RETURNS integer AS $$
DECLARE
 A_SubCategory ALIAS FOR $1;
 myrec integer;
BEGIN
   insert into M_SubCategory(Sub_Category) values(A_SubCategory);
   select seq_no into myrec from M_SubCategory where Sub_Category = A_SubCategory;
   RETURN myrec as seq_no;
END;
$$ LANGUAGE 'plpgsql' VOLATILE;