CREATE OR REPLACE FUNCTION M_Technology_delete() RETURNS integer AS $$
BEGIN
delete from m_Technology A
 where NOT EXISTS(select 1 from M_Applications where Technology_seq_no = A.seq_no)
  and  NOT EXISTS(select 1 from T_AplFltTechnologyLink where Technology_seq_no = A.seq_no);
RETURN 0;
END;
$$ LANGUAGE 'plpgsql' VOLATILE;