CREATE OR REPLACE FUNCTION M_category_delete() RETURNS integer AS $$
BEGIN
delete from m_category A
 where NOT EXISTS(select 1 from M_Applications where category_seq_no = A.seq_no)
   and NOT EXISTS(select 1 from T_AplFltCategoryLink where category_seq_no = A.seq_no);
RETURN 0;
END;
$$ LANGUAGE 'plpgsql' VOLATILE;