CREATE OR REPLACE FUNCTION T_TopDstSummary_insert(
        varchar,
        varchar,
        varchar,
        varchar,
        varchar,
        varchar,
        varchar
) RETURNS Integer AS $$

declare
        A_VsysID alias for $1;
        A_TargetDate alias for $2;
        A_Dst alias for $3;
        A_ResolvedDst alias for $4;
        A_DstUser alias for $5;
        A_Bytes alias for $6;
        A_Sessions alias for $7;
begin
		insert into T_TopDstSummary (
			VSYS_ID,
			TARGET_DATE,
			DST,
			RESOLVED_DST,
			DST_USER,
			BYTES,
			SESSIONS
		) values (
			A_VsysID,
			to_timestamp(A_TargetDate,'YYYY/MM/DD'),
			A_Dst,
			A_ResolvedDst,
			A_DstUser,
			A_Bytes,
			A_Sessions
		);
	return 0;
end;

$$ LANGUAGE 'plpgsql' VOLATILE;