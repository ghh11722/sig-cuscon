CREATE OR REPLACE FUNCTION M_subcategory_delete() RETURNS integer AS $$
BEGIN
delete from m_subcategory A
 where NOT EXISTS(select 1 from M_Applications where sub_category_seq_no = A.seq_no)
  and  NOT EXISTS(select 1 from T_AplFltSubCategoryLink where sub_category_seq_no = A.seq_no);
RETURN 0;
END;
$$ LANGUAGE 'plpgsql' VOLATILE;