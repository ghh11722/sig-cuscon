CREATE OR REPLACE FUNCTION T_TopDstCountriesSummary_insert(
        varchar,
        varchar,
        varchar,
        varchar,
        varchar
) RETURNS Integer AS $$

declare
        A_VsysID alias for $1;
        A_TargetDate alias for $2;
        A_DstLoc alias for $3;
        A_Bytes alias for $4;
        A_Sessions alias for $5;
begin
		insert into T_TopDstCountriesSummary (
			VSYS_ID,
			TARGET_DATE,
			DSTLOC,
			BYTES,
			SESSIONS
		) values (
			A_VsysID,
			to_timestamp(A_TargetDate,'YYYY/MM/DD'),
			A_DstLoc,
			A_Bytes,
			A_Sessions
		);
	return 0;
end;

$$ LANGUAGE 'plpgsql' VOLATILE;