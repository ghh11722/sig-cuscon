CREATE OR REPLACE FUNCTION T_TopSrcSummary_insert(
        varchar,
        varchar,
        varchar,
        varchar,
        varchar,
        varchar,
        varchar
) RETURNS Integer AS $$

declare
        A_VsysID alias for $1;
        A_TargetDate alias for $2;
        A_Src alias for $3;
        A_ResolvedSrc alias for $4;
        A_SrcUser alias for $5;
        A_Bytes alias for $6;
        A_Sessions alias for $7;
begin
		insert into T_TopSrcSummary (
			VSYS_ID,
			TARGET_DATE,
			SRC,
			RESOLVED_SRC,
			SRC_USER,
			BYTES,
			SESSIONS
		) values (
			A_VsysID,
			to_timestamp(A_TargetDate,'YYYY/MM/DD'),
			A_Src,
			A_ResolvedSrc,
			A_SrcUser,
			A_Bytes,
			A_Sessions
		);
	return 0;
end;

$$ LANGUAGE 'plpgsql' VOLATILE;