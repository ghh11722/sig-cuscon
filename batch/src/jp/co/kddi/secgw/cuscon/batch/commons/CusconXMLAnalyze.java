/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコンバッチ共通
 * ファイル名   : CusconXMLAnalyze.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/05     matsshtt                初版作成
 * 2012/12/01     kkato@PROSITE            PANOS4.1.x対応
 * 2013/11/19     kkato@PROSITE           XML解析不具合修正
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.commons;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;
import org.apache.log4j.Logger;

/**
 * クラス名 : CusconXMLAnalyze
 * 機能概要 :
 * 備考 :
 * @author matsshtt
 * @version 1.0 matsshtt
 *          Created 2010/02/24
 *          新規作成
 * @see
 */
public class CusconXMLAnalyze {

	// 1つのオブジェクト単位のデータ
	private HashMap<String, Object> hash = null;
	// 解析結果格納用リスト
	private List<HashMap<String, Object>> result = null;
	// メンバリスト
	List<String> memList = null;
	// 子要素名
	private String name = null;

	// 処理で判定を行うタグ
	private static final String ERROR = "error";
	private static final String RESULT = "result";
	private static final String KEY = "key";
	private static final String REPORT = "report";
	private static final String MSG = "msg";
	private static final String ENTRY= "entry";
	private static final String SUCCESS = "success";

	// ロガー
    Logger log = LogUtil.getCallerLogger();

	/**
	 * メソッド名 : analyzeXML
	 * 機能概要 : XMLを解析し、解析結果を返却する。
	 * @param xml
	 * @return List 解析結果
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public List<HashMap<String, Object>> analyzeXML(InputStream xml) throws Exception {
      	log.info( LogUtil.getClassMethod() + " 開始");

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		// イテレータ
		Iterator iterator;
		Map.Entry obj;

		// XMLをパース
		Document doc = null;

		try {
			builder = factory.newDocumentBuilder();
			doc = builder.parse(xml);
		} catch (Exception e) {
			log.debug("XML解析エラー");
			// XML解析エラー
			throw e;
		}

		// ルート要素の取得する。
		Element element = doc.getDocumentElement();

	    // 子要素を含んだノードを列挙
		NodeList list = element.getChildNodes();

		// ルートの子要素名を取得する。
		String nodeName = list.item(0).getNodeName();

	    hash = new HashMap<String, Object>();
	    result = new ArrayList<HashMap<String, Object>>();
	    memList = new ArrayList<String>();

	    // エラーレスポンスの場合、エラーメッセージを出力し、nullを返却する。
	    if(element.getAttributes().item(0) != null &&
	    		element.getAttributes().item(0).getNodeValue().equals(ERROR)) {

	      	log.error(LogUtil.getErrorMSG("EK170011"));

	    	// エラーメッセージを取得する。
	    	analyzeChildNode(element.getChildNodes());
	    	iterator = result.get(0).entrySet().iterator();
	    	obj = (Map.Entry)iterator.next();

	    	for(int i=0; i<result.size(); i++) {
	    		log.debug("取得エラー内容取得:" + i);
		 		iterator = result.get(i).entrySet().iterator();
		 		while (iterator.hasNext()) {
	 				log.debug("ハッシュマップ:");
		 			obj = (Map.Entry)iterator.next();
		 			if(obj.getKey().equals(MSG))
	 					log.debug("key:" + obj.getKey() + ", value = " + obj.getValue());
		 				log.error(obj.getValue());
		 		}
		 	}
	    	return null;
	    }
	    // アクセスキー生成、ポリシー/オブジェクトの場合
	    if (nodeName.equals(RESULT)) {
	    	log.debug("アクセスキー生成orポリシー/オブジェクト:nodeName = " + nodeName);
	        // 子要素を取得する。
	        NodeList childNodes = list.item(0).getChildNodes();

	        // アクセスキー生成の場合
	        if(childNodes.item(0).getNodeName().equals(KEY)) {
	        	log.debug("アクセスキー生成:nodeName = " + childNodes.item(0).getNodeName());
	        	analyzeChildNode(element.getChildNodes());
	        }
	        // ポリシー名/オブジェクト名の要素を読み飛ばす。
        	NodeList grandChildNodes = childNodes.item(0).getChildNodes();
        	analyzeChildNode(grandChildNodes);

	    // レポートの場合
	    } else if(nodeName.equals(REPORT)) {
	    	log.debug("レポート:nodeName = " + nodeName);
	    	// 子要素を取得する。
	    	NodeList childNodes = list.item(0).getChildNodes();

	    	// 子要素の属性を取得する。
			NamedNodeMap attribute =list.item(0).getAttributes();
			// 属性の数分繰り返す。
		    for (int iAttr=0; iAttr<attribute.getLength(); iAttr++) {
		    	log.debug("属性:" + iAttr);
		    	// 属性ノードを取得する。
		    	Node attr = attribute.item(iAttr);
		    	// 子要素をキー、属性を値としてハッシュマップに格納する。
		        hash.put(attr.getNodeName(), attr.getNodeValue());
		    }
		    //20131119 kkato
	    	// 子要素の属性を取得する。
			NamedNodeMap attribute1 = childNodes.item(1).getAttributes();
			// 属性の数分繰り返す。
		    for (int iAttr=0; iAttr<attribute1.getLength(); iAttr++) {
		    	log.debug("属性:" + iAttr);
		    	// 属性ノードを取得する。
		    	Node attr = attribute1.item(iAttr);
		    	// 子要素をキー、属性を値としてハッシュマップに格納する。
		        hash.put(attr.getNodeName(), attr.getNodeValue());
		    }
		    result.add(hash);
		    hash = new HashMap<String, Object>();

		    //analyzeChildNode(childNodes);
	        // result階層取得
		    NodeList grandChildNodes = childNodes.item(1).getChildNodes();
		    analyzeChildNode(grandChildNodes);
		    //20131119 kkato
	    // マスタの場合
	    } else {
	        analyzeChildNode(list);
	    }

      	log.info( LogUtil.getClassMethod() + " 終了");
		return result;
	  }

	/**
	 * メソッド名 : analyzeChildNode
	 * 機能概要 : （再帰処理）インプットの子ノードを解析する。
	 * @param childNodes
	 */
	private void analyzeChildNode(NodeList childNodes) {

		// メンバ数カウント
		int countMem;
		// 子要素格納用
		Node childNode;
		Node tmpNode;
		// 子要素名
		String childName;

		// childNodesの子要素分繰り返す。
		for (int i=0;(childNode = childNodes.item(i))!=null; i++) {
			log.debug("childNodes:" + i);

			// 子要素名を取得する。
			childName = childNode.getNodeName();
			countMem = 0;

			// 子要素の属性を取得する。
			NamedNodeMap attrMap = childNode.getAttributes();

			// 子要素の属性がnull出ない場合
			if (attrMap!=null) {
				log.debug("属性がnullでない");

				// 属性の数を取得
				int attrs = attrMap.getLength();

				// 属性の数分繰り返す。
			    for (int iAttr=0; iAttr<attrs; iAttr++) {
			    	log.debug("属性:" + iAttr);

			    	// 属性ノードを取得する。
			    	Node attr = attrMap.item(iAttr);
			    	// 子要素をキー、属性を値としてハッシュマップに格納する。
			        hash.put(attr.getNodeName(), attr.getNodeValue());
			    }
			}

			// 子要素の種別がELEMENT_NODE(要素)の場合
			if (childNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
				log.debug("要素種別:" + childNode.getNodeType());

				// 孫要素がnullでない場合
				if(childNode.getFirstChild() != null) {
					log.debug("孫要素がnullでない");

					// 孫要素の値がnullの場合
					if(childNode.getFirstChild().getNodeValue() == null) {
						log.debug("孫要素の値がnull");
						// ハッシュマップのキーに子要素名を設定する。
						hash.put(childName, null);
					} else {
						log.debug("子要素名がmember,protocolでない:childName = " + childName);
						// 子要素名をキー、子要素の値を値としてハッシュマップに格納する。
						hash.put(childName, childNode.getFirstChild().getNodeValue());

					}
				}

				// 子要素が存在する場合
//				if(childNode.hasChildNodes()) {
				if(childNode.hasChildNodes() &&
					!childNode.getFirstChild().getNodeName().equals(ENTRY) ){
					log.debug("子要素が存在するため再帰処理を行う。");
					// 再帰呼び出し
					analyzeChildNode(childNode.getChildNodes());
				}
				// 子要素名がentry、result(アクセスキー生成)、msg(エラー時)の場合、
				// ハッシュマップを結果格納リストに追加する。
				if(childName.equals(ENTRY) | childName.equals(RESULT) ) {
					log.debug("結果格納:" +  i);
					// 20121201 kkato@PROSITE mod start
					//result.add(hash);
					if (hash.size() > 0) {
						result.add(hash);
					}
					// 20121201 kkato@PROSITE mod end
					hash = new HashMap<String, Object>();
				}
			}
		  }
	  }
}