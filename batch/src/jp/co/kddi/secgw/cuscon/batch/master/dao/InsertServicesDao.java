/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコンバッチ
 * ファイル名   : InsertServicesDao.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     matsshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.master.dao;

import java.sql.Types;
import java.util.*;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.BatchSqlUpdate;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


/**
 * クラス名：InsertServicesDao<br>
 * 機能概要：アプリケーションデータのDB登録クラスです。<br>
 * 備考：<br>
 * @author matsshtt
 * @version 1.0 matsshtt<br>
 *             Created 2010/03/01<br>
 *             新規作成
 *
 */
public class InsertServicesDao {
	// ロガー
    Logger log = LogUtil.getCallerLogger();
    /** データソース */
    private DriverManagerDataSource dataSource;

    private static final String mapKey_name = "name";
    private static final String mapKey_protocol = "protocol";
    private static final String mapKey_port = "port";

    private static final String sql = "insert into m_services(name, protocol, port) values(?,?,?)";

    /**
     * メソッド名：M_Servicesテーブルのデータ追加<br>
     * 機能概要：M_Servicesテーブルにデータを追加する。<br>
     *
     * @param List<HashMap<String,Object>> PAServiceList 登録するデータのリスト
     */
    public void insert(List<HashMap<String, Object>> PAServiceList) throws DataAccessException{
	    log.debug( LogUtil.getClassMethod() + " 開始");

    	BatchSqlUpdate batchUpdater = new BatchSqlUpdate(this.dataSource, sql);
        batchUpdater.declareParameter(new SqlParameter(Types.VARCHAR));
        batchUpdater.declareParameter(new SqlParameter(Types.VARCHAR));
        batchUpdater.declareParameter(new SqlParameter(Types.VARCHAR));
        batchUpdater.compile();

  		// SQL実行
        try{
        	for(int i=0;i < PAServiceList.size(); ++i){
        		Map tmpMap = PAServiceList.get(i);
        		log.debug("M_Services挿入 name="+tmpMap.get(mapKey_name));
        		Object param[] = {tmpMap.get(mapKey_name),
        				tmpMap.get(mapKey_protocol),
        				tmpMap.get(mapKey_port)};
        		batchUpdater.update(param);
        	}
        } catch (DataAccessException e) {
        	log.fatal(LogUtil.getErrorMSG("FK160001"),e);
        	throw e;
        }

        log.debug("M_Services挿入 batchUpdater.flush");
    	batchUpdater.flush();

	    log.debug( LogUtil.getClassMethod() + " 終了");
    }


    /**
     * メソッド名：データソース設定<br>
     * 機能概要：データソースをセットする<br>
     * @param ds データソース
     */
    public void setdataSource(DriverManagerDataSource ds) {
	    log.debug( LogUtil.getClassMethod() + " 開始");
        this.dataSource = ds;
	    log.debug( LogUtil.getClassMethod() + " 終了");    }

}
