/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * �v���O������ : KDDI�Z�L�����e�BGW�V�X�e�� �J�X�R���o�b�`
 * �t�@�C����   : DeleteCategoryDao.java
 *
 * [�ύX����]
 * ��t           �X�V��                  ���e
 * 2010/03/01     matsshtt                ���ō쐬
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.master.dao;

import java.sql.Types;
import java.util.*;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


/**
 * クラス名：DeleteCategoryDao<br>
 * 機能概要：出力データのDB登録クラスです。<br>
 * 備考：<br>
 * @author matsshtt
 * @version 1.0 matsshtt<br>
 *             Created 2010/03/01<br>
 *             新規作成
 *
 */
public class DeleteCategoryDao {
	// ロガー
    Logger log = LogUtil.getCallerLogger();
    /** データソース */
    private DriverManagerDataSource dataSource;

    private static final String mapKey_name = "name";

    /**
     * メソッド名：Category削除SP実行<br>
     * 機能概要：Category削除SPの実行<br>
     *
     * @param String applicationName 削除するサービス名
     */
    public void delete() throws DataAccessException {
	    log.debug( LogUtil.getClassMethod() + " 開始");

    	Category_DeleteSP sproc = new Category_DeleteSP(this.dataSource);

    	 HashMap tmpMap = new HashMap();

   		// ストアド実行
    	try{
    		sproc.execute(tmpMap);
    	} catch (DataAccessException e) {
    		log.fatal(LogUtil.getErrorMSG("FK160001"),e);
		throw e;
    	}

	    log.debug( LogUtil.getClassMethod() + " 終了");
    }

    /**
     * クラス名：Category登録内部クラス<br>
     * 機能概要：Category登録内部クラス<br>
     */
    private class Category_DeleteSP extends StoredProcedure {
        public static final String SP_NAME = "M_Category_delete";


        /**
         * メソッド名：Category登録SP設定<br>
         * 機能概要：Category登録SPの設定<br>
         *
         * @param ds データソース
         */
        public Category_DeleteSP(DriverManagerDataSource ds) {

        	setDataSource(ds);
            setFunction(false);
            setSql(SP_NAME);
            compile();

        }
    }

    /**
     * メソッド名：データソース設定<br>
     * 機能概要：データソースをセットする<br>
     * @param ds データソース
     */
    public void setdataSource(DriverManagerDataSource ds) {
	    log.debug( LogUtil.getClassMethod() + " 開始");
        this.dataSource = ds;
	    log.debug( LogUtil.getClassMethod() + " 終了");    }

}