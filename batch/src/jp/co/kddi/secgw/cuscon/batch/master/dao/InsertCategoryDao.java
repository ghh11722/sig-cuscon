/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコンバッチ マスター情報蓄積
 * ファイル名   : InsertCategoryDao.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     matsshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.master.dao;

import java.sql.Types;
import java.util.*;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


/**
 * クラス名：InsertCategoryDao<br>
 * 機能概要：カテゴリーマスタ登録のデータアクセスクラスです。<br>
 * 備考：<br>
 * @author matsshtt
 * @version 1.0 matsshtt<br>
 *             Created 2010/03/01<br>
 *             新規作成
 *
 */
public class InsertCategoryDao {
	// ロガー
    Logger log = LogUtil.getCallerLogger();
    /** データソース */
    private DriverManagerDataSource dataSource;

    private static final String sqlParam_category = "category";
    private static final String sqlParam_seqno = "seq_no";

    /**
     * メソッド名：Category登録SP実行<br>
     * 機能概要：Categoryを登録し、そのseq_noを返す<br>
     *
     * @param String CategoryName   登録するCategory名
     */
    public int insert(String categoryName) throws DataAccessException {
	    log.debug( LogUtil.getClassMethod() + " 開始");

    	Category_InsertSP sproc = new Category_InsertSP(this.dataSource);

    	Map outMap = null;
    	int seq_no = -1;
    	HashMap tmpMap = new HashMap();
    	tmpMap.put(sqlParam_category, categoryName);

    	// ストアド実行
    	try {
    		outMap = sproc.execute(tmpMap);
    	} catch (DataAccessException e) {
    		log.fatal(LogUtil.getErrorMSG("FK160001"),e);
    		throw e;
    	}

    	// seq_no取り出し
		seq_no = (Integer)outMap.get(sqlParam_seqno);
   		log.debug( "Category登録 + seq_no取得 成功");

	    log.debug( LogUtil.getClassMethod() + " 終了");
	    return seq_no;
    }

    /**
     * クラス名：Category登録内部クラス<br>
     * 機能概要：Category登録内部クラス<br>
     */
    private class Category_InsertSP extends StoredProcedure {
        public static final String SP_NAME = "M_Category_insert";


        /**
         * メソッド名：Category登録SP設定<br>
         * 機能概要：Category登録SPの設定<br>
         *
         * @param ds データソース
         */
        public Category_InsertSP(DriverManagerDataSource ds) {
    	    log.debug( LogUtil.getClassMethod() + " 開始");
        	setDataSource(ds);
            setFunction(false);
            setSql(SP_NAME);
            declareParameter(new SqlParameter(sqlParam_category, Types.VARCHAR));
            declareParameter(new SqlOutParameter(sqlParam_seqno, Types.INTEGER));
            compile();
    	    log.debug( LogUtil.getClassMethod() + " 終了");
        }
    }

    /**
     * メソッド名：データソース設定<br>
     * 機能概要：データソースをセットする<br>
     * @param ds データソース
     */
    public void setdataSource(DriverManagerDataSource ds) {
	    log.debug( LogUtil.getClassMethod() + " 開始");
        this.dataSource = ds;
	    log.debug( LogUtil.getClassMethod() + " 終了");
    }

}