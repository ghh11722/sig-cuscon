/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコンバッチ共通
 * ファイル名   : CusconXmlApiExecute.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/05     matsshtt                初版作成
 * 2019/09/10     Plum Systems Inc.       TLS1.2対応（JDK8移行）
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.commons;

import java.io.InputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;
import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;
import org.apache.log4j.Logger;

/**
 * クラス名 : CusconXmlApiExecute
 * 機能概要 : XML-APIの実行を行う。
 * 備考 :
 * @author matsshtt
 * @version 1.0 matsshtt
 *          Created 2010/03/05
 *          新規作成
 *          2.0 T.Yamazaki@Plum Systems Inc.
 *          Updated 2019/09/10
 *          TLS1.2対応
 * @see
 */
public class CusconXmlApiExecute {
	// ロガー
    Logger log = LogUtil.getCallerLogger();

	/**
	 * メソッド名 : initSSLContext
	 * 機能概要 :
	 * @return SSLContext
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 */
	public static SSLContext initSSLContext() throws NoSuchAlgorithmException, KeyManagementException {
		Logger logger = LogUtil.getCallerLogger();
		
		logger.info("initSSLContext処理開始（TLSv1.2対応版）");
		SSLContext ssl = SSLContext.getInstance("TLSv1.2");

		X509TrustManager[] tm = new X509TrustManager[] {
			new X509TrustManager() {

				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] arg0, String arg) {

				}

				public void checkServerTrusted(X509Certificate[] arg0, String arg) {

				}
			}
		};

		ssl.init(null, tm, null);

		HttpsURLConnection.setDefaultHostnameVerifier(
			new HostnameVerifier() {
				public boolean verify(String host1, SSLSession session) {
					logger.debug(host1 + " に " + session.getProtocol() + " で接続しています。");
					return true;
				}
			}
		);

		HttpsURLConnection.setDefaultSSLSocketFactory(ssl.getSocketFactory());

		return ssl;
	}

	/**
	 * メソッド名 : doProc
	 * 機能概要 :
	 * @param urls
	 * @return InputStream
	 * @throws Exception
	 */
	public InputStream doProc(String urls) throws Exception {
      	log.info( LogUtil.getClassMethod() + " 開始");
		InputStream input = null;
		try {
			initSSLContext();
			URL url = new URL(urls);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			input = conn.getInputStream();
		} catch (Exception e) {
			log.debug("XML-API接続失敗");
			throw e;
		}

      	log.info( LogUtil.getClassMethod() + " 終了");
	    return input;
	}
}
