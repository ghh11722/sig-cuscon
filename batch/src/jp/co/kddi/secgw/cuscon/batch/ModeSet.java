/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ModeSet.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/02/23  m.narikawa@JCCH  初版作成
 * 2010/03/02  T.Sutoh@JCCH     ログ出力処理を追加
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

/**
 * クラス名 : ModeSet
 * 機能概要 : パラメータによってメンテナンスモード、通常モードの切り替えを行う。
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/02/23
 *          新規作成
 * @version 1.2 T.Sutoh@JCCH
 *          Created 2010/03/02
 *          ログ出力を追加
 */
public class ModeSet {
	// ログクラス
	private static Log log = LogFactory.getLog(ModeSet.class);

	// メッセージプロパティ
	private static Properties msg = new Properties();

	// モード定義
	private static final int NORMAL_MODE = 0;
	private static final int MAINTE_MODE = 1;
	// SQL
	private static final String MODE_SETTING_SQL = "UPDATE T_SystemInfo SET MODE=?;";

	/**
	 * メソッド名 : メイン処理
	 * 機能概要 : メンテナンスモード切替処理
	 * @param args 起動パラメータ（0:通常モード 1:メンテナンスモード）
	 */
	public static void main(String[] args) {

		String driverClassName = null;  // JDBCドライバクラス名
		String url = null;              // DB接続URL
		String username = null;         // DB接続ユーザ名
		String password = null;         // DB接続パスワード
		int mode = 0;                   // モード(0:通常モード 1:メンテナンスモード)
		String modeString = null;       // 出力メッセージ（画面）
		String modeStrinlog = null;     // 出力メッセージ（ログ）

		// パラメータチェック
		if ((args == null) || (args.length != 3)) {
			//log.error("パラメータエラー");
			System.out.println("パラメータエラー");
			System.exit(1); // 異常終了
		}

		// メッセージのプロパティファイルをロード
		try {
			msg.load(new FileInputStream(args[1]));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			System.out.println("パラメータエラー：メッセージプロパティファイルがオープンできません。");
			System.exit(1); // 異常終了
		} catch (IOException e1) {
			e1.printStackTrace();
			System.out.println("パラメータエラー：メッセージプロパティファイルがオープンできません。");
			System.exit(1); // 異常終了
		}

		if (args[2].equals("0") == true) {
			mode = NORMAL_MODE;
			modeString = msg.getProperty("DK150204");
			modeStrinlog = msg.getProperty("IK151201");
		} else if (args[2].equals("1") == true) {
			mode = MAINTE_MODE;
			modeString = msg.getProperty("DK150205");
			modeStrinlog = msg.getProperty("IK151202");
		} else {
			log.fatal(msg.getProperty("FK151201"));
			System.out.println(msg.getProperty("DK150201"));
			System.exit(1); // 異常終了
		}

		// DB接続情報読み込み
		try {
			// XMLパース
			DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = dbfactory.newDocumentBuilder();
			Document doc = builder.parse(new File(args[0]));
			Element root = doc.getDocumentElement();
			NodeList node = root.getElementsByTagName("Resource");
			if (node.getLength() < 1) {
				log.fatal(msg.getProperty("FK151202"));
				System.out.println(msg.getProperty("DK150202"));
				System.exit(1); // 異常終了
			}

			// DB接続情報の取得
			NamedNodeMap nodeMap = node.item(0).getAttributes();
			driverClassName = nodeMap.getNamedItem("driverClassName").getNodeValue();
			url = nodeMap.getNamedItem("url").getNodeValue();
			username = nodeMap.getNamedItem("username").getNodeValue();
			password = nodeMap.getNamedItem("password").getNodeValue();
			log.debug("DB接続情報取得完了");

		} catch (Exception e) {
			e.printStackTrace();
			log.fatal(msg.getProperty("FK151203") + e.getMessage());
			System.out.println(msg.getProperty("DK150202"));
			System.exit(1); // 異常終了
		}

		try {
			// DB接続
			Class.forName(driverClassName);
			Properties info = new Properties();
			info.put("user", username);
			info.put("password", password);
			Connection dbConnection = DriverManager.getConnection(url, info);
			log.debug("DB接続 OK");

			// SQL実行
			PreparedStatement stm = dbConnection.prepareStatement(MODE_SETTING_SQL);
			stm.setInt(1, mode);
			stm.execute();
			stm.close();
			log.debug("SQL実行 OK(モード切替)");

			// DB切断
			dbConnection.close();
			log.debug("DB切断 OK");

		} catch (Exception e) {
			e.printStackTrace();
			log.fatal(msg.getProperty("FK151204") + e.getMessage());
			System.out.println(msg.getProperty("DK150203"));
		}

		// 終了処理
		log.info(modeStrinlog);
		System.out.println(modeString);
		System.exit(0); // 正常終了
	}
}
