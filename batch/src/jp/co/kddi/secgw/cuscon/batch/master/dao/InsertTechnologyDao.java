/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコンバッチ
 * ファイル名   : InsertTechnologyDao.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     matsshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.master.dao;

import java.sql.Types;
import java.util.*;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


/**
 * クラス名：InsertTechnologyDao<br>
 * 機能概要：テクノロジーマスタ登録のデータアクセスクラスです。<br>
 * 備考：<br>
 * @author matsshttt
 * @version 1.0 matsshtt<br>
 *             Created 2010/03/01<br>
 *             新規作成
 *
 */
public class InsertTechnologyDao {
	// ロガー
    Logger log = LogUtil.getCallerLogger();
    /** データソース */
    private DriverManagerDataSource dataSource;

    private static final String sqlParam_technology = "Technology";
    private static final String sqlParam_seqno = "seq_no";

    /**
     * メソッド名：Technology登録SP実行<br>
     * 機能概要：Technologyを登録し、そのseq_noを返す<br>
     *
     * @param String TechnologyName   登録するTechnology名
     */
    public int insert(String TechnologyName) throws DataAccessException {
	    log.debug( LogUtil.getClassMethod() + " 開始");

    	Technology_InsertSP sproc = new Technology_InsertSP(this.dataSource);

	    Map outMap = null;
	    int seq_no = -1;
    	 HashMap tmpMap = new HashMap();
    	 tmpMap.put(sqlParam_technology, TechnologyName);

  		// ストアド実行
    	 try {
    		 outMap = sproc.execute(tmpMap);
    	 } catch (DataAccessException e) {
          	log.fatal(LogUtil.getErrorMSG("FK160001"),e);
    		 throw e;
    	 }

    	 // seq_no取り出し
    	 seq_no = (Integer)outMap.get(sqlParam_seqno);
    	 log.debug( "Technology登録 & seq_no取得 成功");

		 log.debug( LogUtil.getClassMethod() + " 終了");
    	 return seq_no;
    }

    /**
     * クラス名：Technology登録内部クラス<br>
     * 機能概要：Technology登録内部クラス<br>
     */
    private class Technology_InsertSP extends StoredProcedure {
        public static final String SP_NAME = "M_Technology_insert";


        /**
         * メソッド名：Technology登録SP設定<br>
         * 機能概要：Technology登録SPの設定<br>
         *
         * @param ds データソース
         */
        public Technology_InsertSP(DriverManagerDataSource ds) {
    	    log.debug( LogUtil.getClassMethod() + " 開始");
        	setDataSource(ds);
            setFunction(false);
            setSql(SP_NAME);
            declareParameter(new SqlParameter(sqlParam_technology, Types.VARCHAR));
            declareParameter(new SqlOutParameter(sqlParam_seqno, Types.INTEGER));
            compile();
    	    log.debug( LogUtil.getClassMethod() + " 終了");
        }
    }

    /**
     * メソッド名：データソース設定<br>
     * 機能概要：データソースをセットする<br>
     * @param ds データソース
     */
    public void setdataSource(DriverManagerDataSource ds) {
	    log.debug( LogUtil.getClassMethod() + " 開始");
        this.dataSource = ds;
	    log.debug( LogUtil.getClassMethod() + " 終了");    }

}