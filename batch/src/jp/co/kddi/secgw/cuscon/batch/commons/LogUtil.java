/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコンバッチ共通
 * ファイル名   : LogUtil.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/05     matsshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.commons;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import org.apache.log4j.Logger;

public class LogUtil {

    public static Logger getCallerLogger() {
    	// 呼び出し元のクラス名
    	return Logger.getLogger(new Throwable().getStackTrace()[1].getClassName());
 	}

    public static String getClassMethod(){
    	// 呼び出し元のメソッド名
    	String mthd = new Throwable().getStackTrace()[1].getMethodName();
    	// 呼び出し元のクラス名
    	String clss = new Throwable().getStackTrace()[1].getClassName();
    	return clss + "." + mthd;
    }

    public static String getErrorMSG(String ErrCode){
    	String errMSG = null;
		 Properties prop = new Properties();
		 InputStream inStream = null;

    	try {
    		inStream = LogUtil.class.getClassLoader().getResourceAsStream("error-msg.properties");
    		 prop.load(inStream);
    		 errMSG = prop.getProperty(ErrCode);
    	} catch (Exception e) {
    		Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.batch.commons.LogUtil");
    	    log.error("ログメッセージ取得に失敗しました ErrorCode=" + ErrCode);
        }
    	return errMSG;
    }

}
