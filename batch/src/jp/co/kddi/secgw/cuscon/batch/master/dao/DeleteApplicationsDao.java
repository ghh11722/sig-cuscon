/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコンバッチ
 * ファイル名   : DeleteApplicationsDao.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     matsshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.master.dao;

import java.util.*;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


/**
 * クラス名：DeleteApplicationsDao<br>
 * 機能概要：M_Applicationsテーブルのデータ削除クラスです。<br>
 * 備考：<br>
 * @author matsshtt
 * @version 1.0 matsshtt<br>
 *             Created 2010/03/01<br>
 *             新規作成
 *
 */
public class DeleteApplicationsDao {
	// ロガー
    Logger log = LogUtil.getCallerLogger();
    /** データソース */
    private DriverManagerDataSource dataSource;

    private static final String mapKey_names = "names";
    private static final String mapKey_name = "name";
	final String sql = "delete from m_applications where name not in (:names) ";
//	"and seq_no not in (select distinct LINK_SEQ_NO from T_ApplicationLink) " +
//	"and seq_no not in (select distinct LINK_SEQ_NO from T_ApplicationGroupsLink)";


    /**
     * メソッド名：M_Applicationsテーブルのデータ削除<br>
     * 機能概要：パラメータのアプリケーションのリストに含まれないアプリケーションデータを削除する。<br>
     *
     * @param List<HashMap<String, Object>> PAAplList 削除しないアプリケーション
     */
    public void delete(List<HashMap<String, Object>> PAAplList) throws DataAccessException {

	    log.debug( LogUtil.getClassMethod() + " 開始");

    	NamedParameterJdbcTemplate jt = new NamedParameterJdbcTemplate(this.dataSource);
	    // パラメータ作成
    	Map<String,ArrayList> params = new HashMap();
    	ArrayList tmpArray = new ArrayList();
    	for(int k=0; k < PAAplList.size(); ++k) {
    		tmpArray.add((String)PAAplList.get(k).get(mapKey_name));
    	}
    	params.put(mapKey_names, tmpArray);

/*↓DBG:2012/01/10(T.Suzuki) 削除対象をログにダンプ */
		String dbg_sql = "select name from m_applications where name not in (:names) ";
		List results = null;
		NamedParameterJdbcTemplate dbg_jt = new NamedParameterJdbcTemplate(this.dataSource);
		results = dbg_jt.queryForList(dbg_sql, params);
		log.info("m_applications削除対象:"+ results);
/*↑DBG:2012/01/10(T.Suzuki)*/

		// SQL実行
    	try{
    		jt.update(sql, params);
    	} catch (DataAccessException e) {
    		log.fatal(LogUtil.getErrorMSG("FK160001"),e);
    		throw e;
    	}

	    log.debug( LogUtil.getClassMethod() + " 終了");
    }


    /**
     * メソッド名：データソース設定<br>
     * 機能概要：データソースをセットする<br>
     * @param ds データソース
     */
    public void setdataSource(DriverManagerDataSource ds) {
	    log.debug( LogUtil.getClassMethod() + " 開始");
        this.dataSource = ds;
	    log.debug( LogUtil.getClassMethod() + " 終了");    }

}