/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコンバッチ
 * ファイル名   : SelectSystemConstantDao.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/05     matsshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.report.dao;


import java.sql.Types;
import java.text.MessageFormat;
import java.util.*;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;



/**
 * クラス名：SystemConstanDao<br>
 * 機能概要：システムコンスタントTBL検索クラス<br>
 * 備考：<br>
 * @author matsshtt
 * @version 1.0 matsshtt<br>
 *             Created 2010/03/05<br>
 *             新規作成
 *
 */
public class SelectSystemConstantDao {
	// ロガー
    private Logger log = LogUtil.getCallerLogger();
    /** データソース */
    private DriverManagerDataSource dataSource;
    /** SQL */
    public static final String sqlString = "select pa_xmlapi_url from m_systemconstant;";


    /**
　　 * クラス名：ＤＢ登録内部クラス<br>
 　　* 機能概要：<br>
     */
    public List select() {
      	log.info( LogUtil.getClassMethod() + " 開始");

        List results = null;

        try {
        	JdbcTemplate jt = new JdbcTemplate(this.dataSource);
        	results = jt.queryForList(sqlString);

        } catch (DataAccessException e) {
        	log.fatal(LogUtil.getErrorMSG("FK170014"),e);
        	return null;
        }

      	log.info( LogUtil.getClassMethod() + " 終了");

    	return results;
    }


    /**
     * メソッド名：データソース設定<br>
     * 機能概要：データソースをセットする<br>
     * @param ds データソース
     */
    public void setdataSource(DriverManagerDataSource ds) {
        this.dataSource = ds;
    }


}

