/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコンバッチ
 * ファイル名	: GetMasterInfo.java
 *
 * [変更履歴]
 * 日付 		  更新者				  内容
 * 2010/03/05	  matsshtt				  初版作成
 * 2011/05/19	  inoue@prosite			  Webフィルタ設定の追加
 * 2012/12/01     kkato@PROSITE           PANOS4.1.x対応
 * 2013/09/01	  kkato@prosite			  URLフィルタバージョン取得追加
 *                                        log.fatalをlog.errorに修正
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.master.bussiness;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
//2011/05/19 add start prosite@inoue
import java.util.Properties;
//2011/05/19 add end prosite@inoue

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.batch.commons.*;

/**
 * クラス名 : GetMasterInfo
 * 機能概要 : PAよりマスタ情報を取得する
 * 備考 :
 * @author matsshtt
 * @version 1.0 matsshtt
 *			Created 2010/03/05
 *			新規作成
 * @version 1.1 inoue@prosite
 *          Created 2011/05/19
 *          Webフィルタ設定の追加
 * @see
 */
public class GetMasterInfo {

	private static final String cliCommand1 = "Set cli scripting-mode on\n";
	private static final String cliCommand2 = "Set cli config-output-format xml\n";
	private static final String cliCommand3 = "configure\n";
	private static final String cliCommand_srv = "show predefined service\n";
	private static final String cliCommand_srv_shared = "show shared service\n";
	private static final String cliCommand_apl = "show predefined application\n";


	private static final String mapKey_protocol = "protocol";
	private static final String mapKey_tcp = "tcp";
	private static final String mapKey_udp = "udp";

	private static final String SERVICE = "service";
	private static final String SERVICE_SHARED = "service_shared";

//2011/05/19 inoue@PROSITE add start
	private static final String cliCommand_category = "show vsys %1 profiles url-filtering %2\n";
//2011/05/19 inoue@PROSITE add end

//2011/10/31 inoue@PROSITE add start
	private static final String cliCommand_apl_cont = "show predefined application-container\n";
	private static final String APPLICATION_CONTAINER = "application-container";
//2011/10/31 inoue@PROSITE add end
// 2013/09/01 kkato@prosite add start
	private static final String cliCommand_systeminfo = "show system info\n";
// 2013/09/01 kkato@prosite add end

	private CusconPACommandExecute command = null;

	Logger log = LogUtil.getCallerLogger();


	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 * @param
	 * @throws Exception
	 */
	public GetMasterInfo(HashMap inMap) throws Exception {
		log.debug( LogUtil.getClassMethod() + " 開始");
		try {
			// PAコマンド実行クラス作成
			this.command = new CusconPACommandExecute(inMap);
		} catch (Exception e) {
			// PA接続エラー
			log.error(LogUtil.getErrorMSG("FK160019"));
			throw e;
		}
		log.debug( LogUtil.getClassMethod() + " 終了");
	}


	public List<HashMap<String, Object>> getPAMaster(String masterName){
		log.info( LogUtil.getClassMethod() + " 開始:" + masterName);

		// コマンドリスト
		List<String> comList = new ArrayList<String>();
		// レスポンス
		String response2 = null;
		// rootエレメント名
		String rootElementName;

		List<HashMap<String, Object>>  newMasterList = null;

		// コマンドリスト作成
		comList.add(cliCommand1);
		comList.add(cliCommand2);
		comList.add(cliCommand3);
		if(masterName.equals(SERVICE)){
			log.debug( "CLIコマンドリスト作成 サービスマスタ取得");
			comList.add(cliCommand_srv);
			rootElementName = masterName;
		} else if(masterName.equals(SERVICE_SHARED)) {
			log.debug( "CLIコマンドリスト作成 サービスマスタ(shared)取得");
			comList.add(cliCommand_srv_shared);
			rootElementName = SERVICE;
//2011/10/31 inoue@PROSITE add start
		} else if(masterName.equals(APPLICATION_CONTAINER)) {
			log.debug( "CLIコマンドリスト作成 アプリケーションコンテナマスタ取得");
			comList.add(cliCommand_apl_cont);
			rootElementName = masterName;
//2011/10/31 inoue@PROSITE add end
		} else {
			log.debug( "CLIコマンドリスト作成 アプリマスタ取得");
			comList.add(cliCommand_apl);
			rootElementName = masterName;
		}

		// コマンド実行
		try {
			log.debug( "getCommandResponceをCall");
			response2 = this.command.getCommandResponce(comList);
		} catch(Exception e) {
			log.error(LogUtil.getErrorMSG("FK160004"),e);
			return null;
		}

//2011/10/31 add start inoue@prosite
		//  application-container情報取得時、application-containerタグが<application-container minver="3.1.0">の
		//  ような miver="3.1.0"を含む為、以降のformatResponseで切り出しが行えない。
		//  従って、<application-container minver="3.1.0"> -> <application-container>の置き換えをここで行う。
		if (masterName.equals(APPLICATION_CONTAINER)){
			response2 = formatResponse_ApplicationContainer(response2,rootElementName);
		}
//2011/10/31 add end inoue@prosite

		//レスポンスのXML部分取り出し⇒XML解析⇒MapListに格納
		newMasterList = formatResponse(response2,rootElementName);
		if(newMasterList == null) {
			log.debug( "レスポンスの解析に失敗");
			return null;
		}


		if(masterName.equals(SERVICE) ||
		   masterName.equals(SERVICE_SHARED)){
			log.debug( "サービスマスタに対し、protocolを設定する");
			// protocolの値を設定する
			for(int k=0; k < newMasterList.size(); ++k) {
				HashMap<String, Object> tmpService = newMasterList.get(k);
				log.debug( "service="+tmpService.get("name"));
				if(tmpService.containsKey(mapKey_tcp)) {
					log.debug( "protocol=tcpを設定");
					(newMasterList.get(k)).put(mapKey_protocol, mapKey_tcp);
				} else if(tmpService.containsKey(mapKey_udp)) {
					log.debug( "protocol=udpを設定");
					(newMasterList.get(k)).put(mapKey_protocol, mapKey_udp);
				}
			}
		}

		log.info( LogUtil.getClassMethod() + " 終了");
		return newMasterList;
	}

// 2011/05/19 add start prosite@inoue
	/**
	 * 機能概要 : URLFilterカテゴリをPAから取得
	 * @param cmd_vsys : URLFilterカテゴリ情報取得コマンド用 Vsys-ID
	 * @param cmd_urlprofile : URLFilterカテゴリ情報取得コマンド用 urlプロファイル
	 * @param urlctgrst : PAからのレスポンスよりカテゴリデータの開始位置（尚、この行の次の行から対象）
	 * @param urlctgred : PAからのレスポンスよりカテゴリデータの終了位置（尚、この行の前の行まで対象）
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> getPAMaster_UrlCategory(
			String cmd_vsys, String cmd_urlprofile, String urlctgrst, String urlctgred){
		log.info( LogUtil.getClassMethod() + " 開始");

		// コマンドリスト
		List<String> comList = new ArrayList<String>();
		// レスポンス
		String response2 = null;
		// rootエレメント名
// 20121201 kkato@PROSITE mod start
		//String rootElementName="allow";
		String rootElementName="alert";
// 20121201 kkato@PROSITE mod end
		int spo=0;
		int epo=0;

		List<HashMap<String, Object>> newMasterList = new ArrayList<HashMap<String, Object>>();

		// コマンドリスト作成
		comList.add(cliCommand1);
		comList.add(cliCommand2);
		comList.add(cliCommand3);
		String wk = cliCommand_category;
		wk = wk.replaceAll("%1",cmd_vsys);
		wk = wk.replaceAll("%2",cmd_urlprofile);
		comList.add(wk);

		// コマンド実行
		try {
			log.debug( "getCommandResponceをCall");
			response2 = this.command.getCommandResponce(comList);
		} catch(Exception e) {
			log.error(LogUtil.getErrorMSG("FK160004"),e);
			return null;
		}

		//レスポンスのXML部分取り出し⇒XML解析⇒MapListに格納
		newMasterList = formatResponse(response2,rootElementName);
		if(newMasterList == null) {
			log.debug( "レスポンスの解析に失敗");
			return null;
		}

		//レスポンスよりカテゴリとするデータ部の切り出し（開始、終了部分の判定）
		spo=response2.indexOf(urlctgrst);
		epo=response2.indexOf(urlctgred);
		if(spo==-1 || epo==-1) {
//2011/07/15 add start inoue@prosite
			log.error("PAからの応答 " + response2);
//2011/07/15 add end inoue@prosite
			String err = LogUtil.getErrorMSG("FK170023");
			err = err.replaceAll("#1",urlctgrst);
			err = err.replaceAll("#2",urlctgred);
//2011/07/15 rep start inoue@prosite
//			log.error(LogUtil.getErrorMSG(err));
			log.error(err);
//2011/07/15 rep end inoue@prosite
			return null;
		}
		String category = null;
		category = response2.substring(spo+urlctgrst.length()+1, epo-1);

		// 分解
		String sctgyList[] = category.trim().split("\n");

		for(String s : sctgyList) {
			spo=s.indexOf(">");
			epo=s.indexOf("</member>");
			if( spo<= epo ){
				HashMap<String, Object> hctgr = new HashMap<String, Object>();
				hctgr.put("name",s.substring(spo+1, epo));
				newMasterList.add(hctgr);
			}
		}
		log.info( LogUtil.getClassMethod() + " 終了");
		return newMasterList;
	}
// 2011/05/19 add end prosite@inoue


	private List<HashMap<String, Object>> formatResponse(String res, String rootElementName){

		// レスポンスからXML部分以外を取り除く
		String[] tmpStr1 = res.split("<" + rootElementName + ">");
		log.debug("tmpStr1.length=" + tmpStr1.length);
		if(tmpStr1.length != 2) {
			log.info("res=" + res);
			log.error(LogUtil.getErrorMSG("FK160005"));
			return null;
		}
		String[] tmpStr2 = tmpStr1[1].split("</" + rootElementName + ">");
		log.debug("tmpStr2.length=" + tmpStr2.length);
		if(tmpStr2.length != 2) {
			log.info("res=" + res);
			log.error(LogUtil.getErrorMSG("FK160005"));
			return null;
		}
		String tmpStr3 = "<" + rootElementName + ">"+ tmpStr2[0] +"</" + rootElementName + ">";

		// インデントのための 改行コード/ブランクを取り除く
		Pattern p = Pattern.compile(">\n.*?<", Pattern.DOTALL);
		Matcher m = p.matcher(tmpStr3);
		String tmpStr4 = m.replaceAll("><");


		// InputStreamに変換
		InputStream resXML = null;
		try {
			resXML = new ByteArrayInputStream(tmpStr4.getBytes("UTF-8"));
		} catch(Exception e) {
			log.error(LogUtil.getErrorMSG("FK160006"),e);
			return null;
		}

		// XML解析を行う
		List<HashMap<String, Object>> hoge = null;
		try {
			CusconXMLAnalyze xmlAnalyze = new CusconXMLAnalyze();
			hoge = xmlAnalyze.analyzeXML(resXML);
		} catch (Exception e) {
			log.error(LogUtil.getErrorMSG("FK160007"),e);
			return null;
		}

		return hoge;
	}

//2011/10/31 add start inoue@prosite
	private String formatResponse_ApplicationContainer(String res, String rootElementName){

		if (!res.matches(".*<" + rootElementName + ">.*")){
			Pattern pattern3 = Pattern.compile("<" + rootElementName + ".+?>", Pattern.DOTALL);
    	    Matcher matcher3 = pattern3.matcher(res);
    	    res = matcher3.replaceAll("<" + rootElementName + ">");
		}

		return res;
	}
//2011/10/31 add end inoue@prosite

	// 2013/09/01 kkato@prosite add start - URLフィルタバージョン取得
	/**
	 * 機能概要 : URLフィルタバージョンをPAから取得
	 * @throws Exception
	 */
	public HashMap<String, String> getPAMaster_SystemInfo(){
		log.info( LogUtil.getClassMethod() + " 開始");

		// コマンドリスト
		List<String> comList = new ArrayList<String>();
		// レスポンス
		String response2 = null;

		HashMap<String, String> newMasterList = new HashMap<String, String>();

		// コマンドリスト作成
		comList.add(cliCommand1);
		//comList.add(cliCommand2);
		//comList.add(cliCommand3);
		comList.add(cliCommand_systeminfo);

		// コマンド実行
		try {
			log.debug( "getCommandResponceをCall");
			response2 = this.command.getCommandResponce(comList);
		} catch(Exception e) {
			log.error(LogUtil.getErrorMSG("FK160004"),e);
			return null;
		}

		String[] tmpStr1 = response2.split("\n");
		int spo = 0;
		for(String s : tmpStr1) {
			spo = s.indexOf(":");
			if( spo > 0 ){
				HashMap<String, Object> hctgr = new HashMap<String, Object>();
				newMasterList.put(s.substring(0, spo), s.substring(spo+1).trim());
			}
		}
		log.info( LogUtil.getClassMethod() + " 終了");
		return newMasterList;
	}

	// 2013/09/01 kkato@prosite add end   - URLフィルタバージョン取得
}
