/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SessionTableDelete.java
 *
 * [変更履歴]
 * 日付        更新者       内容
 * 2010/03/10  h.kubo@JCCH  初版作成
 * 2010/06/29  T.Suzuki@NOP コミット非同期化(プロセス化)対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

/**
 * クラス名 : SessionTableDelete
 * 機能概要 : セッション管理テーブルの削除処理
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *          Created 2010/03/10
 *          新規作成
 * @version 1.1 T.Suzuki@NOP
 *          Modified 2010/04/09
 *          コミット排他テーブルの更新処理追加
 * @version 1.2 T.Suzuki@NOP
 *          Modified 2010/06/29
 *          コミット状態確認テーブルの更新処理追加
 */
public class SessionTableDelete {
	// ログクラス
	private static Log log = LogFactory.getLog(SessionTableDelete.class);

	// メッセージプロパティ
	private static Properties msg = new Properties();
	// SQL
	private static final String DELETE_SESSION_K_SQL = "DELETE FROM T_Session_K;";
	private static final String DELETE_SESSION_C_SQL = "DELETE FROM T_Session_C;";
	private static final String UPDATE_COMMITPROCESSINGC_SQL = "UPDATE T_CommitProcessing SET COMMITPROCESSING_FLG=0;";
	private static final String UPDATE_T_COMMITSTATUS_SQL = "UPDATE T_CommitStatus SET STATUS=1,MESSAGE='処理が中断されました。設定内容を確認してください。', END_TIME=current_timestamp WHERE STATUS=2;";

	/**
	 * メソッド名 : メイン処理
	 * 機能概要 : セッション管理テーブル削除処理
	 * @param args 起動パラメータ
	 */
	public static void main(String[] args) {

		String driverClassName = null;  // JDBCドライバクラス名
		String url = null;              // DB接続URL
		String username = null;         // DB接続ユーザ名
		String password = null;         // DB接続パスワード

		// パラメータチェック
		if ((args == null) || (args.length != 2)) {
			log.error("パラメータエラー");
			System.out.println("パラメータエラー：パラメータが正しく指定されていません。");
			System.exit(1); // 異常終了
		}

		// メッセージのプロパティファイルをロード
		try {
			msg.load(new FileInputStream(args[1]));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			System.out.println("パラメータエラー：メッセージプロパティファイルがオープンできません。");
			System.exit(1); // 異常終了
		} catch (IOException e1) {
			e1.printStackTrace();
			System.out.println("パラメータエラー：メッセージプロパティファイルがオープンできません。");
			System.exit(1); // 異常終了
		}

		System.out.println(msg.getProperty("DK150303"));
		log.info(msg.getProperty("IK151301"));

		// DB接続情報読み込み
		try {
			// XMLパース
			DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = dbfactory.newDocumentBuilder();
			Document doc = builder.parse(new File(args[0]));
			Element root = doc.getDocumentElement();
			NodeList node = root.getElementsByTagName("Resource");
			if (node.getLength() < 1) {
				log.fatal(msg.getProperty("FK151301"));
				System.out.println(msg.getProperty("DK150301"));
				System.exit(1); // 異常終了
			}

			// DB接続情報の取得
			NamedNodeMap nodeMap = node.item(0).getAttributes();
			driverClassName = nodeMap.getNamedItem("driverClassName").getNodeValue();
			url = nodeMap.getNamedItem("url").getNodeValue();
			username = nodeMap.getNamedItem("username").getNodeValue();
			password = nodeMap.getNamedItem("password").getNodeValue();
			log.debug("DB接続情報取得完了");

		} catch (Exception e) {
			e.printStackTrace();
			log.fatal(msg.getProperty("FK151302") + e.getMessage());
			System.out.println(msg.getProperty("DK150301"));
			System.exit(1); // 異常終了
		}

		try {
			// DB接続
			Class.forName(driverClassName);
			Properties info = new Properties();
			info.put("user", username);
			info.put("password", password);
			Connection dbConnection = DriverManager.getConnection(url, info);
			log.debug("DB接続 OK");

			// SQL実行（KDDI運用者セッション管理テーブル削除）
			PreparedStatement stm1 = dbConnection.prepareStatement(DELETE_SESSION_K_SQL);
			stm1.executeUpdate();
			stm1.close();
			log.debug("SQL実行 OK(KDDI運用者セッション管理テーブル削除)");

			// SQL実行（企業管理者セッション管理テーブル削除）
			PreparedStatement stm2 = dbConnection.prepareStatement(DELETE_SESSION_C_SQL);
			stm2.executeUpdate();
			stm2.close();
			log.debug("SQL実行 OK(企業管理者セッション管理テーブル削除)");

			// SQL実行（コミット排他テーブルフラグ更新）
			PreparedStatement stm3 = dbConnection.prepareStatement(UPDATE_COMMITPROCESSINGC_SQL);
			stm3.executeUpdate();
			stm3.close();
			log.debug("SQL実行 OK(コミット排他テーブルフラグ更新)");

			// SQL実行（コミット状態確認テーブルフラグ更新）
			PreparedStatement stm4 = dbConnection.prepareStatement(UPDATE_T_COMMITSTATUS_SQL);
			stm4.executeUpdate();
			stm4.close();
			log.debug("SQL実行 OK(コミット状態確認テーブルフラグ更新)");

			// DB切断
			dbConnection.close();
			log.debug("DB切断 OK");

		} catch (Exception e) {
			e.printStackTrace();
			log.fatal(msg.getProperty("FK151303") + e.getMessage());
			System.out.println(msg.getProperty("DK150302"));
		}

		log.info(msg.getProperty("IK151302"));
		System.out.println(msg.getProperty("DK150304"));
		System.exit(0); // 正常終了
	}
}
