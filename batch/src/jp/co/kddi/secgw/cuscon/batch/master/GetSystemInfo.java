/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコンバッチ URLフィルタバージョン取得
 * ファイル名	: GetSystemInfo.java
 *
 * [変更履歴]
 * 日付 	   更新者			   内容
 * 2013/09/01  kkato@PROSITE       初版作成
 * 2013/12/19  kamide              エラー時のリトライ処理追加
 * 2014/01/06  kkato@PROSITE       エラー時のリトライ処理のタイミング変更
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.master;

import java.io.InputStream;
import java.util.*;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;
import jp.co.kddi.secgw.cuscon.batch.master.bussiness.*;
import jp.co.kddi.secgw.cuscon.batch.master.dao.*;

/**
 * クラス名 : GetSystemInfo 機能概要 : system info情報をPAより取得し 、cusconDBに保存する 備考 :
 *
 * @author matsshtt
 * @version 1.0 kkato Created 2013/09/01 新規作成
 *              kamide        2013/12/19 エラー時のリトライ処理追加
 * @see
 */
public class GetSystemInfo {

	public static final String contxtFileName = "applicationContext.xml";
	public static final String daoName_SelSys = "SelectSystemConstant";

	public static final String daoName_UpdSys = "UpdateSystemConstant";

	public static void main(String[] arg) throws Exception {
		// ロガー
		Logger log = LogUtil.getCallerLogger();

		log.info(LogUtil.getClassMethod() + " 開始");

		ApplicationContext context = null;
		try {
			context = new ClassPathXmlApplicationContext(contxtFileName);
		} catch (BeansException e) {
			log.fatal(LogUtil.getErrorMSG("FK160022"), e);
			System.exit(0);
		}

		// 2013/12/19 kamide@PROSITE add start
		// プロパティの読み込み
		Properties propm = new Properties();
		try {
			InputStream inStream = MasterInfoChikuseki.class.getClassLoader().getResourceAsStream("systemInfo.properties");
			propm.load(inStream);
		} catch (Exception e) {
			log.fatal(LogUtil.getErrorMSG("FK170015"), e);
			System.exit(0);
		}
		// プロパティからリトライパラメータを取得する
		int maxRetryCount = Integer.parseInt(propm.getProperty("pa_retry_cnt"));
		int retryWaitTime = Integer.parseInt(propm.getProperty("pa_retry_wait_time"));
		int retryCount;
		// 2013/12/19 kamide@PROSITE add end

		// DBより、PA(Palo Alto)へのログイン情報を取得する
		SelectSystemConstantDao ssdao = (SelectSystemConstantDao) context
				.getBean(daoName_SelSys);
		List<HashMap> SystemConstantList = ssdao.select();
		if (SystemConstantList == null) {
			log.fatal(LogUtil.getErrorMSG("FK160003"));
			System.exit(0);
		}

		// 2013/09/01 kkato@prosite add start - URLフィルタバージョン取得
		// PAより、system infoを取得する
		GetMasterInfo gm_SystemInfo = null;
		HashMap<String, String> PASystemInfo = null;
		retryCount = 0;
		while(true) {
			retryCount++;
			try {
				gm_SystemInfo = new GetMasterInfo((HashMap)SystemConstantList.get(0));
				PASystemInfo = gm_SystemInfo.getPAMaster_SystemInfo();
				if(PASystemInfo == null) {
					throw new Exception();
				}
				break;
			} catch (Exception e) {
				if (retryCount >= maxRetryCount){
					log.fatal(LogUtil.getErrorMSG("FK170029"), e);
					System.exit(0);
				}
				log.error(LogUtil.getErrorMSG("FK170029") + " リトライ:" + retryCount,e);
				Thread.sleep(retryWaitTime);
			}
		}

		// 登録済みサービスの更新
		UpdateSystemConstantDao sydao = (UpdateSystemConstantDao) context
				.getBean(daoName_UpdSys);
		try {
			sydao.update(PASystemInfo);
		} catch (Exception e) {
			log.fatal(LogUtil.getErrorMSG("FK170031"), e);
			System.exit(0);
		}
		// 2013/09/01 kkato@prosite add end - URLフィルタバージョン取得

		log.info(LogUtil.getClassMethod() + " 終了\n");
		System.exit(0);
	}
}
