/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : FtpCtr.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/03/04  T.Sutoh@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch;

import java.io.IOException;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPReply;

/**
 * クラス名 : LogGetProc
 * 機能概要 : FTP制御クラス
 * 備考 :
 * @author T.Sutoh@JCCH
 * @version 1.0 T.Sutoh@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class FtpCtr extends org.apache.commons.net.ftp.FTPClient{

	//パラメータチェック
	String serverAddr 	= "";	// ログサーバIP
	String login 		= "";	// アカウント
	String pass 		= "";	// パスワード
	String serverDir 	= "";	// ログ取得先ディレクトリ（サーバ側）
	String localDir 	= "";	// ログ配置先ディレクトリ（ローカル側）

	/**
	 * メソッド名 : FTP制御処理
	 * 機能概要 : FTP制御
	 * @param sAddr ログサーバIP
	 * @param login アカウント
	 * @param pass パスワード
	 * @param lDir ログ取得先ディレクトリ（サーバ側）
	 * @param sDir ログ配置先ディレクトリ（ローカル側）
	 */
	public FtpCtr(String sAddr, String login, String pass, String lDir, String sDir){
		this.serverAddr = sAddr;
		this.login = login;
		this.pass = pass;
		this.localDir = lDir;
		this.serverDir = sDir;
	}

	/**
	 * メソッド名 : FTP接続関数
	 * 機能概要 : FTP接続からディレクトリチェンジまでを行う
	 * @return 正常終了:true、異常終了:false
	 */
	public boolean connectFtp(){
		try{
			// FTP接続
			this.connect(serverAddr);
			if (!FTPReply.isPositiveCompletion(this.getReplyCode())) {
				return false; // 異常終了
			}

			// ログイン
			if (this.login(login, pass) == false) {
				this.disconnect();
				return false; // 異常終了
			}

			// PASVモードに変更
			this.pasv();

			// チェンジディレクトリ
			this.changeWorkingDirectory(serverDir);

			// ファイル転送モード設定
			this.setFileType(FTP.BINARY_FILE_TYPE);

		} catch (Exception e) {
			try {
				this.disconnect();
			} catch (IOException eio) {
			}
			return false;
		}
		return true;
	}

	/**
	 * メソッド名 : FTP切断処理
	 * 機能概要 : FTP切断処理
	 */
	public void close(){
		try{
			this.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return;
	}
}
