/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : InsertTopAttacksSummaryDao.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/26     matsshtt                初版作成
 * 2012/12/01     kato@PROSITE            PANOS4.1.x対応
 *******************************************************************************
 */

package jp.co.kddi.secgw.cuscon.batch.report.dao;

import java.sql.Types;
import java.util.*;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
/**
 * クラス名：InsertTopAttacksSummaryDao<br>
 * 機能概要：出力データのDB登録クラスです。<br>
 * 備考：<br>
 * @author matsshtt
 * @version 1.0 matsshtt<br>
 *             Created 2010/02/28<br>
 *             新規作成
 *
 */
public class InsertTopAttacksSummaryDao {
	// ロガー
    private Logger log = LogUtil.getCallerLogger();
    /** データソース */
    private DriverManagerDataSource dataSource;
    // インプットMAPファイルのキー
    private static final String xmlTag_severityOfThreatid = "severity-of-threatid";
    private static final String xmlTag_threatid = "threatid";
    private static final String xmlTag_tid = "tid";
	// 20121201 kkato@PROSITE mod start
    //private static final String xmlTag_threatsubtype = "threatsubtype";
    private static final String xmlTag_threatsubtype = "subtype";
	// 20121201 kkato@PROSITE mod end
    private static final String xmlTag_count = "count";
    private static final String xmlTag_vsysID = "vsysid";
    private static final String xmlTag_targetDate = "target_date";

    /**
     * メソッド名：attacksトップサマリレポート登録SP実行<br>
     * 機能概要：attcaksトップサマリレポート登録SPの実行<br>
     *
     * @param List<HashMap<String,Object>> in 登録するデータのリスト
     * @param String vsysID 登録するデータのVsysID
     * @param String targetDate 登録するデータのtargetdate
     */
    public void insert(List<HashMap<String,Object>> in,
    					String vsysID,
    					String targetDate) throws DataAccessException{

      	log.debug( LogUtil.getClassMethod() + " 開始");

    	TopAttacksSummary_InsertSP sproc = new TopAttacksSummary_InsertSP(this.dataSource);

    	// データの件数分ループ
    	int cnt = 0;
    	 for (int i = 0; i < in.size(); i++) {
    		 Map tmpMap =in.get(i);
     		log.debug("攻撃トップサマリレポートInsert roop count="+ i);

    		 // データの欠損チェック
    		if(!tmpMap.containsKey(xmlTag_severityOfThreatid)){
    			tmpMap.put(xmlTag_severityOfThreatid, null);
      			log.debug("severityOfThreatidをnullで補足しました");
    		}
    		if(!tmpMap.containsKey(xmlTag_threatid)){
    			tmpMap.put(xmlTag_threatid, null);
      			log.debug("threatidをnullで補足しました");
    		}
    		if(!tmpMap.containsKey(xmlTag_tid)){
    			tmpMap.put(xmlTag_tid, null);
      			log.debug("tidをnullで補足しました");
    		}
    		if(!tmpMap.containsKey(xmlTag_threatsubtype)){
    			tmpMap.put(xmlTag_threatsubtype, null);
      			log.debug("threatsubtypeをnullで補足しました");
    		}
    		if(!tmpMap.containsKey(xmlTag_count)){
    			tmpMap.put(xmlTag_count, null);
      			log.debug("countをnullで補足しました");
    		}

    		// VsysID と TargetDate を付加
    		tmpMap.put(xmlTag_vsysID, vsysID);
    		tmpMap.put(xmlTag_targetDate, targetDate);

    		// ストアド実行
    		try {
    			sproc.execute(tmpMap);
    			cnt += 1;
    		} catch(DataAccessException e) {
    			log.fatal(LogUtil.getErrorMSG("FK170014"),e);
    			throw e;
    		}

    		tmpMap.clear();
    	 }
     	log.info( LogUtil.getErrorMSG("IK170017") + cnt);

       	log.debug( LogUtil.getClassMethod() + " 終了");
    }

    /**
     * クラス名：attacksトップサマリレポート登録内部クラス<br>
     * 機能概要：attacksトップサマリレポート登録内部クラス<br>
     */
    private class TopAttacksSummary_InsertSP extends StoredProcedure {
        public static final String SP_NAME = "T_TopAttacksSummary_Insert";


        /**
         * メソッド名：attacksトップサマリレポート登録SP設定<br>
         * 機能概要：attacksトップサマリレポート登録SPの設定<br>
         *
         * @param ds データソース
         */
        public TopAttacksSummary_InsertSP(DriverManagerDataSource ds) {

        	setDataSource(ds);
            setFunction(false);
            setSql(SP_NAME);
            declareParameter(new SqlParameter(xmlTag_vsysID, Types.VARCHAR));
            declareParameter(new SqlParameter(xmlTag_targetDate, Types.VARCHAR));
            declareParameter(new SqlParameter(xmlTag_tid, Types.VARCHAR));
            declareParameter(new SqlParameter(xmlTag_severityOfThreatid, Types.VARCHAR));
            declareParameter(new SqlParameter(xmlTag_threatid, Types.VARCHAR));
            declareParameter(new SqlParameter(xmlTag_threatsubtype, Types.VARCHAR));
            declareParameter(new SqlParameter(xmlTag_count, Types.VARCHAR));
            compile();

        }
    }

    /**
     * メソッド名：データソース設定<br>
     * 機能概要：データソースをセットする<br>
     * @param ds データソース
     */
    public void setdataSource(DriverManagerDataSource ds) {

        this.dataSource = ds;

    }

}
