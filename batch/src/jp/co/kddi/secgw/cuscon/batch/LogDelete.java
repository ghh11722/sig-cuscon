/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : LogDelete.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/02/23  m.narikawa@JCCH  初版作成
 * 2010/03/02  T.Sutoh@JCCH     ログメッセージ出力を追加
 * 2010/04/16  T.Suzuki@NOP     期限切れレポート削除処理を追加
 * 2011/05/30  inoue@prosite    ログの期限切れデータ削除を変更
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

/**
 * クラス名 : ログ削除バッチ処理メインクラス
 * 機能概要 : 一定期間を過ぎたDBのトラフィックログ、IDS/IPSログを削除する。
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/02/23
 *          新規作成
 * @version 1.1 T.Sutoh@JCCH
 *          Created 2010/03/02
 *          ログメッセージ出力を追加
 * @version 1.2 T.Suzuki@NOP
 *          Created 2010/04/16
 *          期限切れレポート削除処理を追加
 * @version 1.3 inoue@prosite
 *          Created 2011/05/30
 *          ログの期限切れデータ削除を変更( IPS/IDSはsubtypeで各々期限切れデータを削除 )
 */
public class LogDelete {
	// ログクラス
	private static Log log = LogFactory.getLog(LogDelete.class);

	// メッセージプロパティ
	private static Properties msg = new Properties();
	// SQL
//2011/05/30 inoue@prosite rep start
//	private static final String SELECT_CONSTANT_LOG_EXP_SQL = "SELECT log_expire_date FROM m_systemconstant;";
//2011/12/29 inoue@prosite rep start
//	private static final String SELECT_CONSTANT_LOG_EXP_SQL = "SELECT log_expire_date, urlfilter_log_expire_date, download_log_dir FROM m_systemconstant;";
	private static final String SELECT_CONSTANT_LOG_EXP_SQL = "SELECT log_expire_date, urlfilter_log_expire_date, download_log_dir, logfile_expire_date, urlfilter_logfile_expire_date FROM m_systemconstant;";
//2011/12/29 inoue@prosite rep end
//2011/05/30 inoue@prosite rep end
	private static final String DELETE_TLOG_SQL = "DELETE FROM t_logtraffic WHERE to_date(receive_time,'YYYY/MM/DD') <= to_date(?,'YYYY/MM/DD')";
	private static final String DELETE_ILOG_SQL = "DELETE FROM t_logidsips WHERE to_date(receive_time,'YYYY/MM/DD') <= to_date(?,'YYYY/MM/DD')";
//2011/05/30 inoue@prosite add start
	private static final String DELETE_URLFILTERLOG_SQL = "DELETE FROM t_logurlfilter WHERE to_date(receive_time,'YYYY/MM/DD') <= to_date(?,'YYYY/MM/DD')";
	private static final String DELETE_VIRUSCHECKLOG_SQL = "DELETE FROM t_logviruscheck WHERE to_date(receive_time,'YYYY/MM/DD') <= to_date(?,'YYYY/MM/DD')";
	private static final String DELETE_SPYWARELOG_SQL = "DELETE FROM t_logspyware WHERE to_date(receive_time,'YYYY/MM/DD') <= to_date(?,'YYYY/MM/DD')";
	private static final String ASSORT_TRA = "tra";
	private static final String ASSORT_IPS = "ips";
	private static final String ASSORT_WEB = "web";
	private static final String ASSORT_ANV = "anv";
	private static final String ASSORT_ANS = "ans";
//2011/05/30 inoue@prosite add end

	private static final String SELECT_CONSTANT_REPORT_EXP_SQL = "SELECT report_expire_date FROM m_systemconstant;";
	private static final String DELETE_TTOPSRC_SQL = "DELETE FROM T_TopSrcSummary WHERE target_date <= to_timestamp(?,'YYYY/MM/DD')";
	private static final String DELETE_TTOPDST_SQL = "DELETE FROM T_TopDstSummary WHERE target_date <= to_timestamp(?,'YYYY/MM/DD')";
	private static final String DELETE_TTOPAPP_SQL = "DELETE FROM T_TopAppSummary WHERE target_date <= to_timestamp(?,'YYYY/MM/DD')";
	private static final String DELETE_TTOPDSTCOUNTRIES_SQL = "DELETE FROM T_TopDstCountriesSummary WHERE target_date <= to_timestamp(?,'YYYY/MM/DD')";
	private static final String DELETE_TTOPATTACKS_SQL = "DELETE FROM T_TopAttacksSummary WHERE target_date <= to_timestamp(?,'YYYY/MM/DD')";

	/**
	 * メソッド名 : メイン処理
	 * 機能概要 : ログ削除バッチ処理
	 * @param args 起動パラメータ
	 */
	public static void main(String[] args) {

		String driverClassName = null;  // JDBCドライバクラス名
		String url = null;              // DB接続URL
		String username = null;         // DB接続ユーザ名
		String password = null;         // DB接続パスワード
		int log_expires = 0;            // ログ有効時間(単位日)
		int report_expires = 0;         // レポート有効時間(単位日)
//2011/05/30 inoue@prosite add start
		int urlfilter_log_expires = 0;  // Webフィルタログ有効時間(単位日)
		String saveDir = null;
//2011/05/30 inoue@prosite add end
//2011/12/29 inoue@prosite add start
		int logfile_expires = 0;  			// Webフィルタログファイル以外有効時間(単位日)
		int urlfilter_logfile_expires = 0;  // Webフィルタログファイル有効時間(単位日)
//2011/12/29 inoue@prosite add end

		// パラメータチェック
		if ((args == null) || (args.length != 2)) {
			// log.error("パラメータエラー");
			System.out.println("パラメータエラー：パラメータが正しく指定されていません。");
			System.exit(1); // 異常終了
		}

		// メッセージのプロパティファイルをロード
		try {
			msg.load(new FileInputStream(args[1]));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			System.out.println("パラメータエラー：メッセージプロパティファイルがオープンできません。");
			System.exit(1); // 異常終了
		} catch (IOException e1) {
			e1.printStackTrace();
			System.out.println("パラメータエラー：メッセージプロパティファイルがオープンできません。");
			System.exit(1); // 異常終了
		}

		System.out.println(msg.getProperty("DK150103"));
		log.info(msg.getProperty("IK151101"));

		// DB接続情報読み込み
		try {
			// XMLパース
			DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = dbfactory.newDocumentBuilder();
			Document doc = builder.parse(new File(args[0]));
			Element root = doc.getDocumentElement();
			NodeList node = root.getElementsByTagName("Resource");
			if (node.getLength() < 1) {
				log.fatal(msg.getProperty("FK151101"));
				System.out.println(msg.getProperty("DK150101"));
				System.exit(1); // 異常終了
			}

			// DB接続情報の取得
			NamedNodeMap nodeMap = node.item(0).getAttributes();
			driverClassName = nodeMap.getNamedItem("driverClassName").getNodeValue();
			url = nodeMap.getNamedItem("url").getNodeValue();
			username = nodeMap.getNamedItem("username").getNodeValue();
			password = nodeMap.getNamedItem("password").getNodeValue();
			log.debug("DB接続情報取得完了");

		} catch (Exception e) {
			e.printStackTrace();
			log.fatal(msg.getProperty("FK151102") + e.getMessage());
			System.out.println(msg.getProperty("DK150101"));
			System.exit(1); // 異常終了
		}

		try {
			// DB接続
			Class.forName(driverClassName);
			Properties info = new Properties();
			info.put("user", username);
			info.put("password", password);
			Connection dbConnection = DriverManager.getConnection(url, info);
			log.debug("DB接続 OK");

			PreparedStatement stm;

			// ログ保存期間を取得（SQL実行）
			stm = dbConnection.prepareStatement(SELECT_CONSTANT_LOG_EXP_SQL);
			stm.clearParameters();
			ResultSet res = stm.executeQuery();
			res.next();
			log_expires= res.getInt(1);
//2011/05/30 inoue@prosite add start
			urlfilter_log_expires= res.getInt(2);
			saveDir = res.getString(3);
//2011/05/30 inoue@prosite add end
//2011/12/29 inoue@prosite add start
			logfile_expires= res.getInt(4);
			urlfilter_logfile_expires= res.getInt(5);
//2011/12/29 inoue@prosite add end
			stm.close();
			log.debug("SQL実行 OK(ログ保存期間取得)");

			// 今日の日付からexpires日だけ減算
			Calendar now = Calendar.getInstance();
			now.add(Calendar.DATE, (-1)*log_expires);
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd");
			String delete_date = sdf1.format(now.getTime());
//2011/05/30 inoue@prosite add start
			Calendar urlnow = Calendar.getInstance();
			urlnow.add(Calendar.DATE, (-1)*urlfilter_log_expires);
			SimpleDateFormat urlsdf = new SimpleDateFormat("yyyy/MM/dd");
			String urlfilter_delete_date = urlsdf.format(urlnow.getTime());
//2011/05/30 inoue@prosite add end

			// トラフィックログを削除（SQL実行）
			stm = dbConnection.prepareStatement(DELETE_TLOG_SQL);
			stm.clearParameters();
			stm.setString(1, delete_date);
			stm.execute();
			stm.close();
			log.debug("SQL実行 OK(トラフィックログ削除)");

			// IDS/IPSログを削除（SQL実行）
			stm = dbConnection.prepareStatement(DELETE_ILOG_SQL);
			stm.clearParameters();
			stm.setString(1, delete_date);
			stm.execute();
			stm.close();
			log.debug("SQL実行 OK(スレットログ削除)");

//2011/05/30 inoue@prosite add start
			// Webフィルタログを削除（SQL実行） 2週間
			stm = dbConnection.prepareStatement(DELETE_URLFILTERLOG_SQL);
			stm.clearParameters();
			stm.setString(1, urlfilter_delete_date);
			stm.execute();
			stm.close();
			log.debug("SQL実行 OK(URLFILTERログ削除)");

			// ウィルスチェックログを削除（SQL実行） 3か月
			stm = dbConnection.prepareStatement(DELETE_VIRUSCHECKLOG_SQL);
			stm.clearParameters();
			stm.setString(1, delete_date);
			stm.execute();
			stm.close();
			log.debug("SQL実行 OK(ウィルスチェックログ削除)");

			// アンチスパイウェアログを削除（SQL実行） 3か月
			stm = dbConnection.prepareStatement(DELETE_SPYWARELOG_SQL);
			stm.clearParameters();
			stm.setString(1, delete_date);
			stm.execute();
			stm.close();
			log.debug("SQL実行 OK(アンチスパイウェアログ削除)");
//2011/05/30 inoue@prosite add end

			// レポート保存期間を取得（SQL実行）
			stm = dbConnection.prepareStatement(SELECT_CONSTANT_REPORT_EXP_SQL);
			stm.clearParameters();
			res = stm.executeQuery();
			res.next();
			report_expires = res.getInt(1);
			stm.close();
			log.debug("SQL実行 OK(レポート保存期間取得)");

			// 今日の日付からexpires日だけ減算
			now = Calendar.getInstance();
			now.add(Calendar.DATE, (-1)*report_expires);
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd");
			delete_date = sdf2.format(now.getTime());

			// 出所トップサマリレポート(T_TopSrcSummary)を削除（SQL実行）
			stm = dbConnection.prepareStatement(DELETE_TTOPSRC_SQL);
			stm.clearParameters();
			stm.setString(1, delete_date);
			stm.execute();
			stm.close();
			log.debug("SQL実行 OK(出所トップサマリレポート(T_TopSrcSummary)削除)");

			// 宛先トップサマリレポート(T_TopDstSummary)を削除（SQL実行）
			stm = dbConnection.prepareStatement(DELETE_TTOPDST_SQL);
			stm.clearParameters();
			stm.setString(1, delete_date);
			stm.execute();
			stm.close();
			log.debug("SQL実行 OK(宛先トップサマリレポート(T_TopDstSummary)削除)");

			// Appトップサマリレポート(T_TopAppSummary)を削除（SQL実行）
			stm = dbConnection.prepareStatement(DELETE_TTOPAPP_SQL);
			stm.clearParameters();
			stm.setString(1, delete_date);
			stm.execute();
			stm.close();
			log.debug("SQL実行 OK(Appトップサマリレポート(T_TopAppSummary)削除)");

			// 宛先国トップサマリレポート(T_TopDstCountriesSummary)を削除（SQL実行）
			stm = dbConnection.prepareStatement(DELETE_TTOPDSTCOUNTRIES_SQL);
			stm.clearParameters();
			stm.setString(1, delete_date);
			stm.execute();
			stm.close();
			log.debug("SQL実行 OK(宛先国トップサマリレポート(T_TopDstCountriesSummary)削除)");

			// 攻撃トップサマリレポート(T_TopAttacksSummary)を削除（SQL実行）
			stm = dbConnection.prepareStatement(DELETE_TTOPATTACKS_SQL);
			stm.clearParameters();
			stm.setString(1, delete_date);
			stm.execute();
			stm.close();
			log.debug("SQL実行 OK(攻撃トップサマリレポート(T_TopAttacksSummary)削除)");

			// DB切断
			dbConnection.close();
			log.debug("DB切断 OK");
//2011/05/30 inoue@prosite add start
			// ダウンロードログファイルの削除
//2011/12/29 inoue@prosite rep start
//			if (!DeleteLog(saveDir, log_expires, urlfilter_log_expires)){
			if (!DeleteLog(saveDir, logfile_expires+1, urlfilter_logfile_expires+1)){
//2011/12/29 inoue@prosite rep end
				System.exit(0); // 終了
			}
//2011/05/30 inoue@prosite add end
		} catch (Exception e) {
			e.printStackTrace();
			log.fatal(msg.getProperty("FK151103") + e.getMessage());
			System.out.println(msg.getProperty("DK150102"));
		}

		// 終了処理
		log.info(msg.getProperty("IK151102"));
		System.out.println(msg.getProperty("DK150104"));
		System.exit(0); // 正常終了
	}

//2011/05/30 inoue@prosite add start
	static boolean DeleteLog(String saveDir, int rangeDays1, int rangeDays2) {

		log.debug("ダウンロード用ログファイル削除開始");
		try{
			// ログファイル一覧を取得
			File targetDir = new File(saveDir);

			if (targetDir.exists() && targetDir.isDirectory()) {

				// 【ファイル命名規則】
				//		ファイル名「vsysXXX_YYYYMMDD_aaa.zip] xxxはvsysidで0埋めしない
				//		VSYS-IDの番号部分のみ（3桁0埋め）
				//		aaa:tra/ips/web/anv/ans
				File[] tmpfileList = targetDir.listFiles();

				// webは14日 web以外は90日
				// 範囲日付を取得
				int limitYmd1 = 0;
				int limitYmd2 = 0;
				Calendar cal1 = Calendar.getInstance();
				Calendar cal2 = Calendar.getInstance();
				// 今日の日付から範囲日付を引く
				cal1.add(Calendar.DATE, -rangeDays1);
				limitYmd1 = Integer.valueOf(String.format("%04d%02d%02d",
							cal1.get(Calendar.YEAR), cal1.get(Calendar.MONTH) + 1, cal1.get(Calendar.DATE)));
				cal2.add(Calendar.DATE, -rangeDays2);
				limitYmd2 = Integer.valueOf(String.format("%04d%02d%02d",
							cal2.get(Calendar.YEAR), cal2.get(Calendar.MONTH) + 1, cal2.get(Calendar.DATE)));

				// 範囲日付のファイルオブジェクト及びファイル名を保存する
				for (int i = 0; i < tmpfileList.length; i++) {
					boolean delflg = false;
					// ファイルの種類
					String filename = tmpfileList[i].getName();
//2011.07.19 rep start
					int po = filename.indexOf("_");
					if( po!=-1 ){
						// ファイルの日付
						int tmpYmd = Integer.valueOf(filename.substring(po+1, po+1+8));
						po = filename.lastIndexOf("_");
						// ファイルの種類
						if( po!=-1 ){
							String assort = filename.substring(po+1, po+1+3);
							// WEBログ以外のログ
							if(assort.equalsIgnoreCase(ASSORT_TRA) || assort.equalsIgnoreCase(ASSORT_IPS) ||
								assort.equalsIgnoreCase(ASSORT_ANS) || assort.equalsIgnoreCase(ASSORT_ANV) ){
								// 範囲日付以下ならば
								if (Integer.valueOf(tmpYmd) <= Integer.valueOf(limitYmd1)) {
									delflg = true;
								}
							}
							// WEBログ
							if(assort.equalsIgnoreCase(ASSORT_WEB)){
								// 範囲日付以下ならば
								if (Integer.valueOf(tmpYmd) <= Integer.valueOf(limitYmd2)) {
									delflg = true;
								}
							}
						}
					}
//					// ファイルの日付
//					int tmpYmd = Integer.valueOf(filename.substring(4, 12));
//					// ファイルの種類
//					String assort = filename.substring(13, 16);
//					// WEBログ以外のログ
//					if(assort.equalsIgnoreCase(ASSORT_TRA) || assort.equalsIgnoreCase(ASSORT_IPS) ||
//						assort.equalsIgnoreCase(ASSORT_ANS) || assort.equalsIgnoreCase(ASSORT_ANV) ){
//						// 範囲日付以下ならば
//						if (Integer.valueOf(tmpYmd) <= Integer.valueOf(limitYmd1)) {
//							delflg = true;
//						}
//					}
//					// WEBログ
//					if(assort.equalsIgnoreCase(ASSORT_WEB)){
//						// 範囲日付以下ならば
//						if (Integer.valueOf(tmpYmd) <= Integer.valueOf(limitYmd2)) {
//							delflg = true;
//						}
//					}
//2011.07.19 rep end
					if (delflg) {
						//削除対象ファイル
						File delfile = new File(saveDir + "/" + filename);
						delfile.delete();
						log.debug("削除ファイル名 : "+filename);
					}
				}
			}
			log.debug("ダウンロード用ログファイル削除終了");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			log.fatal(msg.getProperty("FK150024") + e.getMessage());
			System.out.println(msg.getProperty("DK150024"));
			return false;
		}
	}
//2011/05/30 inoue@prosite add end
}
