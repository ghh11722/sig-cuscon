/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコンバッチ共通
 * ファイル名	:CusconPACommandExecute.java
 *
 * [変更履歴]
 * 日付 		  更新者				  内容
 * 2010/03/05	  matsshtt				  初版作成
 * 2010/03/29	  hiroyasu				  telnet対応
 * 2010/08/17	  T.Suzuki(NOP) 		  PAコマンドWait値対応
 * 2011/05/17	  inoue@prosite			  Webフィルタカテゴリ時はEnter送信
 * 2013/09/01	  kkato@prosite			  log.fatalをlog.errorに修正
 * 2016/11/24     T.Yamazaki@Plum Systems SSH対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.commons;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.net.telnet.TelnetClient;
import org.apache.log4j.Logger;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import jp.co.kddi.secgw.cuscon.batch.report.ReportInfoChikuseki;

/**
 * クラス名 : CusconPACommandExecute
 * 機能概要 : PAに対してコマンドを発行する。
 * 備考 :
 * @author matsshtt
 * @version 1.0 matsshtt
 *			Created 2010/03/05
 *			新規作成
 *          2.0 T.Yamazaki@Plum Systems Inc.
 *          Updated 2016/11/24
 *          SSH対応
 * @see
 */
public class CusconPACommandExecute {

	// PA接続情報
	private String PALoginID;
	private String PAPass;
	private String PAAddress;
	private int PATimeOut;
	private int PACmdWait;
	// telnetクライアント
	private TelnetClient telnet = null;
	// 通信用の入出力ストリーム
	private InputStream istream = null;
	private OutputStream ostream = null;
	private Reader reader = null;
	private Writer writer = null;

	// プロンプト(通常)
	private String prompt1;
	// プロンプト(configモード)
	private String prompt2;

	private static int timeOutMilliSec = 0;
	// コンフィグモードコマンド
	private  static final String configure = "configure\n";
	// 終了コマンド
	private  static final String exit = "exit\n";

	// SSHを使用するかどうかを示すフラグ
	private Boolean useSecuredShell = false;

	// 使用するプロトコル（プロパティファイルから取得：デフォルトは「telnet」）
	private String protocol = "telnet";

	// SSHセッション
	private Session sshSession = null;
	// SSHチャンネル
	private Channel sshChannel = null;


	// ロガー
	private static Logger log = LogUtil.getCallerLogger();

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 * @param  inMap PA接続情報
	 * @throws Exception
	 */
	public CusconPACommandExecute(HashMap<String, Object> inMap) throws Exception {

	  	// 設定ファイルを読み込み
	  	Properties prop = new Properties();
		try {
			InputStream inStream = ReportInfoChikuseki.class.getClassLoader().getResourceAsStream("comand.properties");
			 prop.load(inStream);
		} catch (Exception e) {
			log.error(LogUtil.getErrorMSG("FK170015"),e);
			throw e;
		}
		this.prompt1 = prop.getProperty("normalPrompt");
		this.prompt2 = prop.getProperty("configPrompt");

		// 設定ファイルからプロトコルを読み込み（パラメータがない場合はtelnetとする）
		// UPDATED: 2016.11 by Plum Systems Inc.
		String configProtocol = prop.getProperty("pa_protocol");
		if(configProtocol.equals("ssh")){
			this.useSecuredShell = true;
			this.protocol = "ssh";
		} else {
			this.useSecuredShell = false;
			this.protocol = "telnet";
		}
		// --> 設定ファイルからプロトコルを読み込み

		this.PAAddress = (String)inMap.get("pa_cli_address");
		this.PALoginID = (String)inMap.get("pa_admin_login_id");
		this.PAPass = (String)inMap.get("pa_admin_login_pwd");
		this.PATimeOut = (((BigDecimal)inMap.get("pa_cmd_timeout")).intValue());
		this.PACmdWait = (((BigDecimal)inMap.get("pa_cmd_wait")).intValue());

		// timeout時間(ミリ秒)
		timeOutMilliSec = 60*1000*this.PATimeOut;

		// 指定されたプロトコルに応じて接続処理を開始
		if (this.useSecuredShell) {
			// SSH
			log.info("SSH接続開始");

			// クライアントの生成
			JSch jsch = new JSch();
			this.sshSession = jsch.getSession(PALoginID, this.PAAddress);
			this.sshSession.setPassword(PAPass);
			this.sshSession.setConfig("StrictHostKeyChecking", "no");

			try {
				log.info("SSHセッションを開始しています... (" + PALoginID + " => "
						+ this.PAAddress + ")");
				this.sshSession.connect(timeOutMilliSec);

				this.sshChannel = this.sshSession.openChannel("shell");

				// 通信用の入出力ストリームの生成
				this.istream = this.sshChannel.getInputStream();
				this.ostream = this.sshChannel.getOutputStream();
				this.reader = new InputStreamReader(this.istream);
				this.writer = new OutputStreamWriter(this.ostream);

				log.debug("SSHチャンネルを開始しています...");
				this.sshChannel.connect(timeOutMilliSec);
				log.info("SSH接続を開始しました。");

				// プロンプト出力待ち
				readMessage(reader, ".*" + prompt1);
				// コマンド入力待ち
				log.debug("SSHログイン完了⇒コマンド入力待ち");
				Thread.sleep(2000);
			}
			catch(Exception e){
				log.error("接続シーケンス失敗");
				throw e;
			}

		} else {
			// Telnet
			log.info("Telnet接続開始");

			this.telnet = new TelnetClient("VT100");

			// サーバに接続
			log.info("Telnet接続を開始しています... (" + this.PAAddress + ")");
			telnet.connect(this.PAAddress);

			// 通信用の入出力ストリームの生成
			this.istream = telnet.getInputStream();
			this.ostream = telnet.getOutputStream();
			this.reader = new InputStreamReader(istream);
			this.writer = new OutputStreamWriter(ostream);

			// 認証の実行
			log.debug("認証処理を行っています... ");
			try {
				// 認証の実行
				readMessage(reader, ".*login: $");
				writer.write(PALoginID + "\n");
				writer.flush();
				log.debug("<-- " + PALoginID);
				Thread.sleep(1000);

				readMessage(reader, ".*Password: $");
				writer.write(PAPass + "\n");
				writer.flush();
				log.debug("<-- " + PAPass);
				Thread.sleep(1000);

				// プロンプト出力待ち
				readMessage(reader, ".*" + prompt1);
				// コマンド入力待ち
				log.debug("Telnetログイン完了⇒コマンド入力待ち");
				Thread.sleep(2000);
			} catch (Exception e) {
				log.error("接続シーケンス失敗");
				throw e;
			}
		}
	}
	/**
	 * メソッド名 : readMessage
	 * 機能概要 : 特定のメッセージが出力されるまで、サーバからのメッセージを読み込む。
	 * @param reader ストリームリーダー
	 * @param message 終了メッセージ
	 * @return String コマンドレスポンス
	 */
	private static String readMessage(Reader reader, String message) throws Exception {

		log.debug("readMessage処理開始 : message = " + message);

		Pattern pattern = Pattern.compile(message, Pattern.DOTALL);
		StringBuffer buffer = new StringBuffer();
		Matcher matcher = null;

		// コマンド時間の計測
		long start;
		long stop;
		long elapsed;

		// 計測開始
		start = System.currentTimeMillis();

		while (true) {
			if (log.isTraceEnabled()) {
				log.trace("ストリームバッファ読取");
			}
			int c = reader.read();
			if (c < 0) {
				log.trace("** no read data ** ");
				break;
			}

			// 改行コード以外の制御文字を読み飛ばしてみる
			if ((c >= 0) && (c <= 0x1f)) {
				log.trace("改行コード以外の制御文字を読み飛ばし");
				if (c != 0x0a ) continue;
			}

			buffer.append((char) c);
			if (reader.ready() == false) {
				log.trace("reader.ready() == false");
				matcher = pattern.matcher(buffer.toString());
				if (matcher.matches()) break;
			}
			// log.debug("[" + buffer.toString() + "]");

			stop = System.currentTimeMillis();
			if (log.isTraceEnabled()) {
				log.trace("コマンド取得中:経過秒=" + ((stop- start)/1000));
			}

			// コマンド待ち時間判定
			elapsed = stop - start;
			if (elapsed >= timeOutMilliSec) {
				// タイムアウト
				log.error(LogUtil.getErrorMSG("FK160018"));
				Exception e = new Exception();
				throw e;
			}
		}
		// サーバーの応答を出力
		log.debug("---> " + buffer);

		if (matcher.find(0) && matcher.groupCount() >= 1) {
			log.debug("正常レスポンス");
			return (matcher.group(1));
		}
		log.debug("異常レスポンス");
		return (null);
	}

	/**
	 * メソッド名 : getCommandResponce
	 * 機能概要 : commandListのコマンドを実行し、標準出力内容を返却する。
	 * @param commandList コマンドリスト	exitnum: exitの回数
	 * @return String コマンドレスポンス
	 * @throws JSchException
	 * @throws Exception
	 */
	public String getCommandResponce(List<String> commandList, int prmflg)	throws Exception {

		log.debug("getCommandResponce処理開始 : コマンド数 = " + commandList.size());

		log.debug("------------------------------------------------------------");
		log.debug("送信コマンド");
		for(int i = 0; i < commandList.size(); i++){
			log.debug(" " + i + " : " + commandList.get(i).replace("\n",""));
		}
		log.debug("------------------------------------------------------------");

		// コンフィグコマンド有無フラグ
		boolean configFlg = false;
		// レスポンス
		String response = "";

		try {
			// commandList分繰り返す。
			for (int i = 0; i < commandList.size(); i++) {
				log.debug("コマンド実行:" + commandList.get(i));
				Thread.sleep(this.PACmdWait);
				log.debug("コマンド実行:" + commandList.get(i));
				writer.write(commandList.get(i));
				writer.flush();
				Thread.sleep(this.PACmdWait);

				// commandが"configure"だった場合にフラグをセット
				if (commandList.get(i).equals(configure)) {
					configFlg = true;
					log.debug("configモード");
				}

				// フラグがセットされているときはConfigモードで実行する
				if (configFlg) {
					log.debug("configモードコマンド実行");
					response = response + readMessage(reader, "(.*)" + prompt2);
				} else {
					log.debug("通常コマンド実行");
					response = response + readMessage(reader, "(.*)" + prompt1);
				}
			}

			// Configモードの場合
			if (configFlg) {
				log.debug("コマンドにコンフィグが含まれるのでexit");
				writer.write(exit);
				writer.flush();
				response = response + readMessage(reader, "(.*)" + prompt1);
			}
		} catch (Exception e) {
			// コマンド実行エラー
			log.error(LogUtil.getErrorMSG("FK160020"));
			throw e;
		}

		return response;

	}

	/**
	 * メソッド名 : getCommandResponce
	 * 機能概要 : commandListのコマンドを実行し、標準出力内容を返却する。
	 * @param commandList コマンドリスト
	 * @return String コマンドレスポンス
	 * @throws JSchException
	 * @throws Exception
	 */
	public String getCommandResponce(
				List<String> commandList)  throws Exception {
		return getCommandResponce(commandList, 0);
	}
	/**
	 * メソッド名 : 終了処理
	 * 機能概要 : -
	 */
	protected void finalize() {
		log.debug("CusconPACommandExecute finalize");
		try {
			this.reader.close();
			this.writer.close();
			this.istream.close();
			this.ostream.close();

			if (this.telnet != null) {
				log.debug("Telnet切断");
				this.telnet.disconnect();
				this.telnet = null;
			}
			if (this.sshChannel != null) {
				log.debug("SSH Channel切断");
				this.sshChannel.disconnect();
				this.sshChannel = null;
			}
			if (this.sshSession != null) {
				log.debug("SSH Session切断");
				this.sshSession.disconnect();
				this.sshSession = null;
			}
		} catch (IOException e) {
			log.debug("ネットワークの切断時の例外は無視::" + e.getStackTrace());
		}
	}
}
