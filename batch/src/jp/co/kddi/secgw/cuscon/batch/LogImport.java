/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : LogImport.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/02/17  T.Sutoh@JCCH    初版作成
 * 2010/03/02  T.Sutoh@JCCH    ログメッセージ出力を追加
 * 2011/05/30  inoue@prosite   ログの追加対応
 * 2012/12/01  kkato@PROSITE   PANOS4.1.x対応
 * 2013/04/01  kkato@PROSITE   複数レコードでコミットするように変更
 * 2013/09/03  kkato@PROSITE   エラー処理にロールバック追加
 *                             エラー発生後に次データの処理を続行するように変更
 * 2017/03/30  T.Yamazaki@Plum PANOS7対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.*;

import java.sql.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

//2011/07/25 add start
import org.apache.commons.csv.CSVParser;
import java.io.Reader;
import java.io.StringReader;
//2011/07/25 add end

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;
import jp.co.kddi.secgw.cuscon.batch.report.ReportInfoChikuseki;

/**
 * クラス名 : LogImport
 * 機能概要 : ログ登録バッチ処理メインクラス
 * 備考 :
 * @author T.Sutoh@JCCH
 * @version 1.0 T.Sutoh@JCCH
 *          Created 2010/02/10
 *          新規作成
 * @version 1.1 T.Sutoh@JCCH
 *          Created 2010/03/02
 *          ログメッセージ出力を追加
 * @version 1.2 inoue@prosite
 *          Created 2011/05/30
 *          ・YYYYMMDD-HHMMSS-4.txtの取り込み
 *          　DB出力先はsubtypeにより振り分け
 *
 *          subtype            出力先テーブル
 *          -----------------------------------------------------------------
 *          url                t_logurlfilter
 *          virus              t_logviruscheck
 *          spyware            t_logspyware
 * @version 2.0 T.Yamazaki@Plum Systems Inc.
 *          Created 2017/03/30
 *          PANOS7対応
 */
public class LogImport {

	// ログクラス
	private static Log log = LogFactory.getLog(LogImport.class);

	// メッセージプロパティ
	private static Properties msg = new Properties();

	// 動作設定取得用SQL
	private static final String SELECT_CONSTANT_SQL = "SELECT logsvr_ftp_addr,logsvr_ftp_login_id,pgp_sym_decrypt(logsvr_ftp_login_pwd::bytea, 'kddi-secgw'::text) as logsvr_ftp_login_pwd,logsvr_ftp_login_path,work_dir FROM m_systemconstant;";
	// 動作設定取得用フィールド名
	private static final String COLUMN_FTP_ADDR = "logsvr_ftp_addr";
	private static final String COLUMN_FTP_ID = "logsvr_ftp_login_id";
	private static final String COLUMN_FTP_PWD = "logsvr_ftp_login_pwd";
	private static final String COLUMN_REMOTE_DIR = "logsvr_ftp_login_path";
	private static final String COLUMN_LOCAL_DIR = "work_dir";
	// トラフィックログ登録用SQL
	private static final String TLOG_INSERT_SQL = "INSERT INTO T_LogTraffic(" +
	                                              "  FUTURE_USE1,RECEIVE_TIME,SERIAL_NUMBER,TYPE,SUBTYPE," +
	                                              "  FUTURE_USE2,FUTURE_USE3,SOURCE_IP,DESTINATION_IP," +
	                                              "  NAT_SOURCE_IP,NAT_DESTINATION_IP,RULE_NAME,SOURCE_USER," +
	                                              "  DESTINATION_USER,APPLICATION,VIRTUAL_SYSTEM,SOURCE_ZONE," +
	                                              "  DESTINATION_ZONE,INGRESS_INTERFACE,EGRESS_INTERFACE," +
	                                              "  LOG_FORWARDING_PROFILE,FUTURE_USE4,SESSION_ID,REPEAT_COUNT," +
	                                              "  SOURCE_PORT,DESTINATION_PORT,NAT_SOURCE_PORT,NAT_DESTINATION_PORT," +
	                                              "  FLAGS,PROTOCOL,ACTION,BYTES,FUTURE_USE5,FUTURE_USE6," +
	                                              "  PACKETS,START_TIME,ELAPSED_TIME,CATEGORY,FUTURE_USE7) " +
	                                              "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
	// IDS/IPSログ登録用SQL
	private static final String ILOG_INSERT_SQL = "INSERT INTO T_LogIdsIps(" +
	                                              "  FUTURE_USE1,RECEIVE_TIME,SERIAL_NUMBER,TYPE,SUBTYPE," +
	                                              "  FUTURE_USE2,FUTURE_USE3,SOURCE_IP,DESTINATION_IP," +
	                                              "  NAT_SOURCE_IP,NAT_DESTINATION_IP,RULE_NAME,SOURCE_USER," +
	                                              "  DESTINATION_USER,APPLICATION,VIRTUAL_SYSTEM,SOURCE_ZONE," +
	                                              "  DESTINATION_ZONE,INGRESS_INTERFACE,EGRESS_INTERFACE," +
	                                              "  LOG_FORWARDING_PROFILE,FUTURE_USE4,SESSION_ID,REPEAT_COUNT," +
	                                              "  SOURCE_PORT,DESTINATION_PORT,NAT_SOURCE_PORT,NAT_DESTINATION_PORT," +
	                                              "  FLAGS,PROTOCOL,ACTION,MISCELLANEOUS,THREAT_ID,CATEGORY," +
	                                              "  SEVERITY,DIRECTION) " +
	                                              "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	// ログファイル名パターン
	private static final String TLOG_FILE_PATTERN = "^[0-9]{6}-[0-9]{6}-2.txt$";
	private static final String ILOG_FILE_PATTERN = "^[0-9]{6}-[0-9]{6}-3.txt$";

	//2011/05/30 add start
	private static final String ULOG_INSERT_SQL = "INSERT INTO %table_name%(" +
		                                              "  FUTURE_USE1,RECEIVE_TIME,SERIAL_NUMBER,TYPE,SUBTYPE," +
		                                              "  FUTURE_USE2,FUTURE_USE3,SOURCE_IP,DESTINATION_IP," +
		                                              "  NAT_SOURCE_IP,NAT_DESTINATION_IP,RULE_NAME,SOURCE_USER," +
		                                              "  DESTINATION_USER,APPLICATION,VIRTUAL_SYSTEM,SOURCE_ZONE," +
		                                              "  DESTINATION_ZONE,INGRESS_INTERFACE,EGRESS_INTERFACE," +
		                                              "  LOG_FORWARDING_PROFILE,FUTURE_USE4,SESSION_ID,REPEAT_COUNT," +
		                                              "  SOURCE_PORT,DESTINATION_PORT,NAT_SOURCE_PORT,NAT_DESTINATION_PORT," +
		                                              "  FLAGS,PROTOCOL,ACTION,MISCELLANEOUS,THREAT_ID,CATEGORY," +
		                                              "  SEVERITY,DIRECTION) " +
		                                              "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String ULOG_FILE_PATTERN = "^[0-9]{6}-[0-9]{6}-4.txt$";

//2011/05/30 add end
	// ローカルディレクトリ名
	private static final String LOCAL_DIR_NAME = "logimport";
	// 最大フィールド長
	private static final int MAX_FUTURE_USE1 = 128;
	private static final int MAX_RECEIVE_TIME = 20;
	private static final int MAX_SERIAL_NUMBER = 16;
	private static final int MAX_TYPE = 16;
	private static final int MAX_SUB_TYPE = 8;
	private static final int MAX_FUTURE_USE2 = 128;
	private static final int MAX_FUTURE_USE3 = 128;
	private static final int MAX_SOURCE_IP = 48;
	private static final int MAX_DESTINATION_IP = 48;
	private static final int MAX_NAT_SOURCE_IP = 48;
	private static final int MAX_NAT_DESTINATION_IP = 48;
	private static final int MAX_RULE_NAME = 32;
	private static final int MAX_SOURCE_USER = 64;
	private static final int MAX_DESTINATION_USER = 64;
	private static final int MAX_APPLICATION = 64;
	private static final int MAX_VIRTUAL_SYSTEM = 20;
	private static final int MAX_SOURCE_ZONE = 16;
	private static final int MAX_DESTINATION_ZONE = 16;
	private static final int MAX_INGRESS_INTERFACE = 32;
	private static final int MAX_EGRESS_INTERFACE = 32;
	private static final int MAX_LOG_FOWERD_PROFILE = 64;
	private static final int MAX_FUTURE_USE4 = 128;
	private static final int MAX_SESSION_ID = 8;
	private static final int MAX_REPEAT_COUNT = 4;
	private static final int MAX_SOURCE_PORT = 6;
	private static final int MAX_DESTINATION_PORT = 6;
	private static final int MAX_NAT_SOURCE_PORT = 6;
	private static final int MAX_NAT_DESTINATION_PORT = 6;
	private static final int MAX_FLAGS = 8;
	private static final int MAX_PROTOCOL = 8;
	private static final int MAX_ACTION = 8;
	private static final int MAX_BYTES = 16;
	private static final int MAX_FUTURE_USE5 = 128;
	private static final int MAX_FUTURE_USE6 = 128;
	private static final int MAX_PACKETS = 4;
	private static final int MAX_START_TIME = 20;
	private static final int MAX_ELAPSED_TIME = 4;
	private static final int MAX_CATEGORY = 64;
	private static final int MAX_FUTURE_USE7 = 128;
	private static final int MAX_MISCELLANEOUS = 128;
	private static final int MAX_THREAT_ID = 128;
	private static final int MAX_SEVERITY = 16;
	private static final int MAX_DIRECTION = 4;
// 20121201 kkato@PROSITE add start
	private static final int MAX_SEQUENCE_NUMBER = 128;
	private static final int MAX_ACTION_FLAGS = 128;
	private static final int MAX_SOURCE_LOCATION = 128;
	private static final int MAX_DESTINATION_LOCATION = 128;
	private static final int MAX_FUTURE_USE8 = 128;
	private static final int MAX_CONTENT_TYPE = 128;
	private static final int PACKETS_SENT = 128;
	private static final int PACKETS_RECEIVED = 128;

    // UPDATED: PANOS7対応（2017.3 by Plum Systems Inc.)
	// トラフィックログ
	private static final int MAX_END_REASON = 128;
	private static final int MAX_ACTION_SOURCE = 128;

	// IPS/IDSログ
	private static final int MAX_PCAPID = 128;
	private static final int MAX_FILEDIGEST = 128;
	private static final int MAX_CLOUD = 128;
	private static final int MAX_URL_INDEX = 128;
	private static final int MAX_USER_AGENT = 128;
	private static final int MAX_FILE_TYPE = 128;
	private static final int MAX_XFORWARD = 128;
	private static final int MAX_REFERER = 128;
	private static final int MAX_SENDER = 128;
	private static final int MAX_SUBJECT = 128;
	private static final int MAX_RECEIPIENT = 128;
	private static final int MAX_REPORTID = 128;

	// トラフィック・IPS/IDSログ 共通項目
	private static final int MAX_DGH_1 = 128;
	private static final int MAX_DGH_2 = 128;
	private static final int MAX_DGH_3 = 128;
	private static final int MAX_DGH_4 = 128;
	private static final int MAX_VSYS_NAME = 128;
	private static final int MAX_DEVICENAME = 128;
	// --> PANOS7対応

	// Insert SQL のパラメータ数
	private static final int URLFILTER_LENGTH = 36;
	private static final int IPSIDS_LENGTH = 36;
	private static final int TRAFFIC_LENGTH = 39;
// 20121201 kkato@PROSITE afd end

//2011/05/30 add start
	private static final String LOG_IPSIDS_TABLE = "T_LogIdsIps";
	private static final String LOG_URLFILTER_TABLE = "t_logurlfilter";
	private static final String LOG_VIRUSCHECK_TABLE = "t_logviruscheck";
	private static final String LOG_SPYWARE_TABLE = "t_logspyware";
	private static final String SUBTYPE_URLFILTER = "url";
	private static final String SUBTYPE_VIRUS = "virus";
	private static final String SUBTYPE_SPYWARE = "spyware";
	private static final int SUBTYPE_INDEX = 4;
//2011/05/30 add start
// 2013/04/01 kkato@PROSITE add start
	private static int commitInterval = 1000;
// 2013/04/01 kkato@PROSITE add end

	/**
	 * メソッド名 : メイン関数
	 * 機能概要 : ログダウンロード処理
	 * @param args 起動パラメータ
	 */
	public static void main(String args[]) {



		String driverClassName = null; // JDBCドライバクラス名
		String url = null;             // DB接続URL
		String username = null;        // DB接続ユーザ名
		String password = null;        // DB接続パスワード
		int cnt = 0;                   // ループカウンタ
		String serverAddr = "";        // FTPサーバアドレス
		String login = "";             // FTPサーバアカウント
		String pass = "";              // FTPサーバパスワード
		String remoteDir = "";         // ログ取得先ディレクトリ（サーバ側）
		String localBaseDir = "";      // ログ配置先ベースディレクトリ（ローカル側）
		FtpCtr ftpObj = null;          // FTPオブジェクト
//2011/08/03 add strat
		boolean logTrafficRst = true;	// Trafficログ処理結果
		boolean logIdsIpsRst = true;	// IdsIpsログ処理結果
		boolean logUrlFilterRst = true;	// UrlFilterログ処理結果
//2011/08/03 add end

		// パラメータチェック
// 2013/04/01 kkato@PROSITE add start
		//if ((args == null) || (args.length != 2)) {
		if ((args == null) || (args.length != 3)) {
// 2013/04/01 kkato@PROSITE add end
			//log.error("パラメータエラー");
			System.out.println("パラメータエラー：パラメータが正しく指定されていません。");
			System.exit(1); // 異常終了
		}

		// メッセージのプロパティファイルをロード
		try {
			msg.load(new FileInputStream(args[1]));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			System.out.println("パラメータエラー：メッセージプロパティファイルがオープンできません。");
			System.exit(1); // 異常終了
		} catch (IOException e1) {
			e1.printStackTrace();
			System.out.println("パラメータエラー：メッセージプロパティファイルがオープンできません。");
			System.exit(1); // 異常終了
		}

		System.out.println(msg.getProperty("DK150015"));
		log.info(msg.getProperty("IK151001"));
// 2013/04/01 kkato@PROSITE add start
		// 設定ファイルを読み込み
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(args[2]));
			commitInterval = Integer.parseInt(prop.getProperty("commit_interval"));
		} catch (Exception e) {
			log.fatal(LogUtil.getErrorMSG("FK170016"), e);
			return;
		}
// 2013/04/01 kkato@PROSITE add end

		// DB接続情報読み込み
		try {
			// XMLパース
			DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = dbfactory.newDocumentBuilder();
			Document doc = builder.parse(new File(args[0]));
			Element root = doc.getDocumentElement();
			NodeList node = root.getElementsByTagName("Resource");
			if (node.getLength() < 1) {
				log.fatal(msg.getProperty("FK151001"));
				System.out.println(msg.getProperty("DK150001"));
				System.exit(1); // 異常終了
			}

			// DB接続情報の取得
			NamedNodeMap nodeMap = node.item(0).getAttributes();
			driverClassName = nodeMap.getNamedItem("driverClassName").getNodeValue();
			url = nodeMap.getNamedItem("url").getNodeValue();
			username = nodeMap.getNamedItem("username").getNodeValue();
			password = nodeMap.getNamedItem("password").getNodeValue();

		} catch (Exception e) {
			e.printStackTrace();
			log.fatal(msg.getProperty("FK151002") + e.getMessage());
			System.out.println(msg.getProperty("DK150002"));
			System.exit(1); // 異常終了
		}
		log.debug("DB接続情報取得完了");

		// 動作設定取得
		try {
			// DB接続
			Class.forName(driverClassName);
			Properties info = new Properties();
			info.put("user", username);
			info.put("password", password);
			Connection dbConnection = DriverManager.getConnection(url, info);
			log.debug("DB接続 OK");

			PreparedStatement stm;

			// 動作設定取得（SQL実行）
			stm = dbConnection.prepareStatement(SELECT_CONSTANT_SQL);
			stm.clearParameters();
			ResultSet res = stm.executeQuery();
			res.next();
			serverAddr = res.getString(COLUMN_FTP_ADDR);
			login = res.getString(COLUMN_FTP_ID);
			pass = res.getString(COLUMN_FTP_PWD);
			remoteDir = res.getString(COLUMN_REMOTE_DIR);
			localBaseDir = res.getString(COLUMN_LOCAL_DIR);
			stm.close();
			log.debug("SQL実行 OK(動作設定取得)");

			// DB切断
			dbConnection.close();
			log.debug("DB切断 OK");

		} catch (Exception e) {
			e.printStackTrace();
			log.fatal(msg.getProperty("FK151003") + e.getMessage());
			System.out.println(msg.getProperty("DK150003"));
			System.exit(1); // 異常終了
		}
		log.debug("動作設定取得完了");

		// ログサーバからログをダウンロード
		FileOutputStream os;
		String[] flistFtp = null;
		List<String> flist = new ArrayList<String>();
		// FTP接続
		try {
			ftpObj = new FtpCtr(serverAddr, login, pass, localBaseDir, remoteDir);
			if (ftpObj.connectFtp() == false) {
				log.fatal(msg.getProperty("FK151004"));
				System.out.println(msg.getProperty("DK150004"));
				System.exit(1); // 異常終了
			}
			log.debug("FTP接続 OK");

			// ログファイル一覧取得
			flistFtp = ftpObj.listNames();
			if (flistFtp.length <= 0) {
				ftpObj.close();
				log.error(msg.getProperty("EK151005"));
				System.out.println(msg.getProperty("DK150005"));
				System.exit(0); // 準正常終了
			}
			log.debug("FTP ファイル一覧取得 OK");
		} catch (Exception e) {
			log.fatal(msg.getProperty("FK151006") + e.getMessage());
			System.out.println(msg.getProperty("DK150006"));
			System.exit(1); // 異常終了
		}

		// ログファイルダウンロード
		try {
			int flistCnt = 0;
			for (cnt = 0; cnt < flistFtp.length; cnt++) {
				// ファイル受信
				String lFile = String.format("%s/%s/%s", localBaseDir, LOCAL_DIR_NAME, flistFtp[cnt]);
				Pattern chkTrafficPattern = Pattern.compile(TLOG_FILE_PATTERN);
				Pattern chkIdsPattern = Pattern.compile(ILOG_FILE_PATTERN);
				Matcher matcherTraffic = chkTrafficPattern.matcher(flistFtp[cnt]);
				Matcher matcherIdsPattern = chkIdsPattern.matcher(flistFtp[cnt]);
//2011/05/30 add start
				Pattern chkUrlFilterPattern = Pattern.compile(ULOG_FILE_PATTERN);
				Matcher matcherUrlFilter = chkUrlFilterPattern.matcher(flistFtp[cnt]);
//2011/05/30 add start
//2011/05/30 rep start
//				if ((matcherTraffic.matches() == true) || (matcherIdsPattern.matches() == true)) {
				if ((matcherTraffic.matches() == true) || (matcherIdsPattern.matches() == true) || (matcherUrlFilter.matches() == true)) {
//2011/05/30 rep end
					// トラフィックログ、IDS/IPSログの際にだけローカルにDLし、作業用の配列へ追加
					os = new FileOutputStream(lFile); // ローカルファイル名
					ftpObj.retrieveFile(flistFtp[cnt], os); // サーバー側
					os.close();
					flist.add(flistFtp[cnt]);
					log.debug("FTP ファイルダウンロード (" + flistFtp[cnt] + ")");

					flistCnt++;
				}
			}
			// FTP切断処理
			ftpObj.close();
			log.debug("FTP切断 OK");
			if (flist.size() <= 0) {
				log.error(msg.getProperty("EK151007"));
				System.out.println(msg.getProperty("DK150007"));
				System.exit(0); // 準正常終了
			}
		} catch (Exception e) {
			ftpObj.close();
			log.fatal(msg.getProperty("FK151008") + e.getMessage());
			System.out.println(msg.getProperty("DK150008"));
			System.exit(1); // 異常終了
		}
		log.debug("ログファイルダウンロード完了");

		// ログ登録
		try {
			// DB接続
			Class.forName(driverClassName);
			Properties info = new Properties();
			info.put("user", username);
			info.put("password", password);
			Connection dbConnection = DriverManager.getConnection(url, info);
			log.debug("DB接続 OK");

			// ログINSERT
			for (cnt = 0; cnt < flist.size(); cnt++) {
				// csvファイルオープン
				String lFile = String.format("%s/%s/%s", localBaseDir, LOCAL_DIR_NAME, flist.get(cnt));
				File csvFp = new File(lFile); // CSVデータファイル
				BufferedReader csvBr;
				csvBr = new BufferedReader(new FileReader(csvFp));

				Pattern chkTrafficPattern = Pattern.compile(TLOG_FILE_PATTERN);
				Pattern chkIdsPattern = Pattern.compile(ILOG_FILE_PATTERN);
				Matcher matcherTraffic = chkTrafficPattern.matcher(flist.get(cnt));
				Matcher matcherIdsPattern = chkIdsPattern.matcher(flist.get(cnt));
//2011/05/30 add start
				Pattern chkUrlFilterPattern = Pattern.compile(ULOG_FILE_PATTERN);
				Matcher matcherUrlFilter = chkUrlFilterPattern.matcher(flist.get(cnt));
//2011/05/30 add end
				if (matcherTraffic.matches() == true) {
					// トラフィックログのINSERT処理
//2011/08/03 rep start 戻り値の確認
//					logTrafficInsert(dbConnection, csvBr);
					log.debug("トラフィックログのINSERT処理");
					logTrafficRst = logTrafficInsert(dbConnection, csvBr);
//2011/08/03 rep end
				}
				if (matcherIdsPattern.matches() == true) {
					// IDS/IPSログのINSERT処理
//2011/08/03 rep start 戻り値の確認
//					logIdsIpsInsert(dbConnection, csvBr);
					log.debug("IDS/IPSログのINSERT処理");
					logIdsIpsRst = logIdsIpsInsert(dbConnection, csvBr);
//2011/08/03 rep end
				}
//2011/05/30 add start
				if (matcherUrlFilter.matches() == true) {
					// UrlFilterログのINSERT処理
//2011/08/03 rep start 戻り値の確認
//					logUrlFilterInsert(dbConnection, csvBr);
					log.debug("UrlFilterログのINSERT処理");
					logUrlFilterRst = logUrlFilterInsert(dbConnection, csvBr);
//2011/08/03 rep end
				}
//2011/05/30 add end
				// ファイルクローズ
				csvBr.close();
			}

			// DB切断
			dbConnection.close();
			log.debug("DB切断 OK");
		} catch (Exception e) {
			e.printStackTrace();
			log.fatal(msg.getProperty("FK151009") + e.getMessage());
			System.out.println(msg.getProperty("DK150009"));
			System.exit(1); // 異常終了
		}
		log.debug("ログデータインポート完了");

//2011/08/03 rep start 削除フェーズをif内に
		if( logTrafficRst && logIdsIpsRst && logUrlFilterRst ){
			// ファイル削除フェーズ
			try {
				// FTP接続
				if (ftpObj.connectFtp() == false) {
					log.fatal(msg.getProperty("FK151004"));
					System.out.println(msg.getProperty("DK150004"));
					System.exit(1); // 異常終了
				}
				log.debug("FTP接続 OK");

				// ログ削除（FTP）
				for (int i = 0; i < flist.size(); i++) {
					// ファイル受信
					try {
						// FTPサーバ側のファイル削除
						String sFile = flist.get(i);
						ftpObj.deleteFile(sFile);
						log.debug("リモートファイル削除 OK(" + sFile +")");

						// ローカルファイルの削除
						sFile = String.format("%s/%s/%s", localBaseDir, LOCAL_DIR_NAME, flist.get(i));
						File delFp = new File(sFile);
						delFp.delete();
						log.debug("ローカルファイル削除 OK(" + sFile +")");

					} catch (IOException e) {
						e.printStackTrace();
					}// サーバー側
				}

				// FTP切断処理
				ftpObj.close();
				log.debug("FTP切断 OK");
			} catch (Exception e) {
				// 共通例外処理
				e.printStackTrace();
				log.fatal(msg.getProperty("FK151010") + e.getMessage());
				System.out.println(msg.getProperty("DK150010"));
				System.exit(1); // 異常終了
			}
		}
//2011/08/03 rep end 削除フェーズをif内に

		// 終了処理
		log.info(msg.getProperty("IK151002"));
		System.out.println(msg.getProperty("DK150016"));
		System.exit(0); // 正常終了
	}

	/**
	 * メソッド名 : トラフィックログINSERT
	 * 機能概要 : トラフィックログファイルからの全レコードのINSERTを行う
	 * @param args
	 * @return
	 * @see
	 */
	static boolean logTrafficInsert(Connection dbConnection, BufferedReader csvBr) {

		try {
			PreparedStatement stm = dbConnection.prepareStatement(TLOG_INSERT_SQL);
			// ファイル処理ループ
			String logRec = "";// ログレコード
			// 2013/09/03 kkato@PROSITE add start
			String[] recList = null;
			// 2013/09/03 kkato@PROSITE add end
// 2013/04/01 kkato@PROSITE add start
			dbConnection.setAutoCommit(false);
			int rcnt = 0;
// 2013/04/01 kkato@PROSITE add end
			while ((logRec = csvBr.readLine()) != null) {
// 2013/04/01 kkato@PROSITE add start
				rcnt++;
// 2013/04/01 kkato@PROSITE add end
				// 2013/09/03 kkato@PROSITE del start
				// csvファイルレコード解析
				//String[] recList = logRec.split(",");

				// 項目の長さチェックと切り詰め処理
				//recList = chkTrafficField(recList);
				// 2013/09/03 kkato@PROSITE del end

				// 2013/09/03 kkato@PROSITE add start
				//セーブポイント作成
				Savepoint sp1 = dbConnection.setSavepoint();
				// 2013/09/03 kkato@PROSITE add end

				try {
					// 2013/09/03 kkato@PROSITE add start
					// csvファイルレコード解析
					logRec=replaceQuote(logRec);
					CSVParser csv = new CSVParser(new StringReader(logRec));
					recList = csv.getLine();
					// 項目の長さチェックと切り詰め処理
					recList = chkTrafficField(recList);
					// 2013/09/03 kkato@PROSITE add end

					// SQLパラメータセット
					stm.clearParameters();
					// 20121201 kkato@PROSITE mod start
					//int max = recList.length;
					int max = TRAFFIC_LENGTH;
					// 20121201 kkato@PROSITE mod end
					for (int i = 0; i < max; i++) {
						stm.setString(i + 1, recList[i]);
					}
					// SQL実行
					stm.executeUpdate();
					log.debug("[OK] " + rcnt);

				} catch (SQLException e) {
					log.debug("[NG:SQL] " + rcnt);
					// 2013/09/03 kkato@PROSITE add start
					//ロールバック
					dbConnection.rollback(sp1);
					// 2013/09/03 kkato@PROSITE add end
					e.printStackTrace();
					// SQLエラー時はログ出力
					log.error(msg.getProperty("EK151011") + "エラー " + rcnt + "行目：" + e.getMessage());
					System.out.println(msg.getProperty("DK150011"));

					// INSERTでエラーとなった際には該当レコードをログへ出力
					String errMsg = msg.getProperty("EK151012");
					int max = recList.length;
					for (int i = 0; i < max; i++) {
						if(i == 0){
							errMsg = errMsg + recList[i];
						}else{
							errMsg = errMsg + "," + recList[i];
						}
					}
					log.error(errMsg);
					// 2013/09/03 kkato@PROSITE add start
				} catch (IOException eIo) {
					log.debug("[NG:IO] " + rcnt);
					//ロールバック
					dbConnection.rollback(sp1);
					eIo.printStackTrace();
					log.error(msg.getProperty("EK151013") + "CSVParseエラー " + rcnt + "行目" );
					log.error(logRec);
					System.out.println(msg.getProperty("EK151012"));
				} catch (Exception e) {
					log.debug("[NG:OTHER] " + rcnt);
					//ロールバック
					dbConnection.rollback(sp1);
					e.printStackTrace();
					log.error(msg.getProperty("EK151013") + "エラー " + rcnt + "行目：" + e.getMessage());
					System.out.println(msg.getProperty("DK150012"));
					// 2013/09/03 kkato@PROSITE add end
				}
// 2013/04/01 kkato@PROSITE add start
				if( rcnt % commitInterval == 0) {
					dbConnection.commit();
					//System.out.println("commit:" + rcnt);
				}
// 2013/04/01 kkato@PROSITE add end
			} // ファイル処理ループ（終端）
// 2013/04/01 kkato@PROSITE add start
			dbConnection.commit();
			log.debug("Commit.");
			dbConnection.setAutoCommit(true);
// 2013/04/01 kkato@PROSITE add end
			stm.close();
			return true;
		} catch (IOException eIo) {
			eIo.printStackTrace();
			log.error(msg.getProperty("EK151013") + eIo.getMessage());
			System.out.println(msg.getProperty("DK150012"));
			return false;
		} catch (SQLException eSql) {
			eSql.printStackTrace();
			log.error(msg.getProperty("EK151011") + eSql.getMessage());
			System.out.println(msg.getProperty("DK150011"));
			return false;
			// 2013/09/03 kkato@PROSITE add start
		} catch (Exception e) {
			e.printStackTrace();
			// エラー時はログ出力
			log.error(msg.getProperty("EK151011") + e.getMessage());
			System.out.println(msg.getProperty("DK150011"));
			return false;
			// 2013/09/03 kkato@PROSITE add end
		}
	}

	/**
	 * メソッド名 : IDS/IPSログINSERT
	 * 機能概要 : IDSIPSログファイルからの全レコードのINSERTを行う
	 * @param args
	 * @return
	 * @see
	 */
//2011/07/19 rep start
//	static boolean logIdsIpsInsert(Connection dbConnection, BufferedReader csvBr) {
//		try {
//			PreparedStatement stm = dbConnection.prepareStatement(ILOG_INSERT_SQL);
//			// ファイル処理ループ
//			String logRec = "";// ログレコード
//			while ((logRec = csvBr.readLine()) != null) {
//				// csvファイルレコード解析
//				String[] recList = logRec.split(",");
//
//				// 項目の長さチェックと切り詰め処理
//				recList = chkIdsIpsField(recList);
//
//				// ファイルINSERT
//				try {
//					// SQLパラメータセット
//					stm.clearParameters();
//					int max = recList.length;
//					for (int i = 0; i < max; i++) {
//						stm.setString(i + 1, recList[i]);
//					}
//					// SQL実行
//					stm.executeUpdate();
//
//				} catch (SQLException e) {
//					e.printStackTrace();
//					// SQLエラー時はログ出力
//					log.error(msg.getProperty("EK151014") + e.getMessage());
//					System.out.println(msg.getProperty("DK150013"));
//
//					// INSERTでエラーとなった際には該当レコードをログへ出力
//					String errMsg = msg.getProperty("EK151015");
//					int max = recList.length;
//					for (int i = 0; i < max; i++) {
//						if(i == 0){
//							errMsg = errMsg + recList[i];
//						}else{
//							errMsg = errMsg + "," + recList[i];
//						}
//					}
//					log.error(errMsg);
//				}
//			}// ファイル処理ループ（終端）
//			stm.close();
//			return true;
//		} catch (IOException eIo) {
//			eIo.printStackTrace();
//			log.error(msg.getProperty("EK151016") + eIo.getMessage());
//			System.out.println(msg.getProperty("DK150014"));
//			return false;
//		} catch (SQLException eSql) {
//			eSql.printStackTrace();
//			log.error(msg.getProperty("EK151014") + eSql.getMessage());
//			System.out.println(msg.getProperty("DK150013"));
//			return false;
//		}
//	}
	static boolean logIdsIpsInsert(Connection dbConnection, BufferedReader csvBr) {
		try {
			// ファイル処理ループ
			String logRec = "";// ログレコード
			// 2013/09/03 kkato@PROSITE add start
			String[] recList = null;
			// 2013/09/03 kkato@PROSITE add end
			String sTableName = "";
			int ikbn=0;
// 2013/04/01 kkato@PROSITE add start
			dbConnection.setAutoCommit(false);
			PreparedStatement stm = null;
			int rcnt = 0;
// 2013/04/01 kkato@PROSITE add end
			while ((logRec = csvBr.readLine()) != null) {
// 2013/04/01 kkato@PROSITE add start
				rcnt++;
// 2013/04/01 kkato@PROSITE add end
// 2013/09/03 kkato@PROSITE add start
				//セーブポイント作成
	            Savepoint sp1 =  dbConnection.setSavepoint();
// 2013/09/03 kkato@PROSITE add end
				// 2013/09/03 kkato@PROSITE del start
				// csvファイルレコード解析
				//String[] recList = logRec.split(",");

				// 項目の長さチェックと切り詰め処理
				//recList = chkIdsIpsField(recList);
				// 2013/09/03 kkato@PROSITE del end

				// ファイルINSERT
				try {
					// 2013/09/03 kkato@PROSITE add start
					// csvファイルレコード解析
					logRec=replaceQuote(logRec);
					CSVParser csv = new CSVParser(new StringReader(logRec));
					recList = csv.getLine();
					// 項目の長さチェックと切り詰め処理
					recList = chkIdsIpsField(recList);
					// 2013/09/03 kkato@PROSITE add end
					sTableName=null;
					if(recList.length >= SUBTYPE_INDEX){
						// Subtypeにより出力テーブルを変更する。
						// 尚、ウィルス、スパイウェア以外は全てIPS/IDS扱いとする
						ikbn = 1;
						sTableName=ULOG_INSERT_SQL.replaceAll("%table_name%",LOG_IPSIDS_TABLE);
						if(recList[SUBTYPE_INDEX].equalsIgnoreCase(SUBTYPE_VIRUS)){
							ikbn = 2;
							log.debug(" => " + SUBTYPE_VIRUS + "/" + LOG_VIRUSCHECK_TABLE + " : " + rcnt);
							sTableName=ULOG_INSERT_SQL.replaceAll("%table_name%",LOG_VIRUSCHECK_TABLE);
						}
						if(recList[SUBTYPE_INDEX].equalsIgnoreCase(SUBTYPE_SPYWARE)){
							ikbn = 3;
							log.debug(" => " + SUBTYPE_SPYWARE + "/" + LOG_SPYWARE_TABLE + " : " + rcnt);
							sTableName=ULOG_INSERT_SQL.replaceAll("%table_name%",LOG_SPYWARE_TABLE);
						}
						if( sTableName != null){
							// 2013/04/01 kkato@PROSITE mod start
							//PreparedStatement stm = dbConnection.prepareStatement(sTableName);
							stm = dbConnection.prepareStatement(sTableName);
							// 2013/04/01 kkato@PROSITE mod end
							// SQLパラメータセット
							stm.clearParameters();
							// 20121201 kkato@PROSITE mod start
							//int max = recList.length;
							int max = IPSIDS_LENGTH;
							// 20121201 kkato@PROSITE mod end
							for (int i = 0; i < max; i++) {
								stm.setString(i + 1, recList[i]);
							}
							// SQL実行
							stm.executeUpdate();
							log.debug("[OK] " + rcnt);
							// 2013/04/01 kkato@PROSITE mod start
							//stm.close();
							// 2013/04/01 kkato@PROSITE mod end
						}
					}
				} catch (SQLException e) {
					log.debug("[NG:SQL] " + rcnt);
					// 2013/09/03 kkato@PROSITE add start
					//ロールバック
					dbConnection.rollback(sp1);
					// 2013/09/03 kkato@PROSITE add end
					e.printStackTrace();
					// SQLエラー時はログ出力
					if(ikbn == 1){
						log.error(msg.getProperty("EK151014") + "エラー " + rcnt + "行目：" + e.getMessage());
						System.out.println(msg.getProperty("DK150013"));
					}
					if(ikbn == 2){
						log.error(msg.getProperty("EK151020") + "エラー " + rcnt + "行目：" + e.getMessage());
						System.out.println(msg.getProperty("DK150019"));
					}
					if(ikbn == 3){
						log.error(msg.getProperty("EK151021") + "エラー " + rcnt + "行目：" + e.getMessage());
						System.out.println(msg.getProperty("DK150020"));
					}

					// INSERTでエラーとなった際には該当レコードをログへ出力
					String errMsg = msg.getProperty("EK151015");
					int max = recList.length;
					for (int i = 0; i < max; i++) {
						if(i == 0){
							errMsg = errMsg + recList[i];
						}else{
							errMsg = errMsg + "," + recList[i];
						}
					}
					log.error(errMsg);
					// 2013/09/03 kkato@PROSITE add start
				} catch (IOException eIo) {
					log.debug("[NG:IO] " + rcnt);
					//ロールバック
					dbConnection.rollback(sp1);
					eIo.printStackTrace();
					log.error(msg.getProperty("EK151015") + "CSVParseエラー " + rcnt + "行目" );
					log.error(logRec);
					System.out.println(msg.getProperty("EK151014"));
				} catch (Exception e) {
					log.debug("[NG:OTHER] " + rcnt);
					//ロールバック
					dbConnection.rollback(sp1);
					e.printStackTrace();
					log.error(msg.getProperty("EK151015") + "エラー " + rcnt + "行目：" + e.getMessage());
					System.out.println(msg.getProperty("EK151014"));
					// 2013/09/03 kkato@PROSITE add end
				}
// 2013/04/01 kkato@PROSITE add start
				if( rcnt % commitInterval == 0) {
					dbConnection.commit();
					//System.out.println("commit:" + rcnt);
				}
// 2013/04/01 kkato@PROSITE add end
			}// ファイル処理ループ（終端）
// 2013/04/01 kkato@PROSITE add start
			dbConnection.commit();
			log.debug("Commit.");
			dbConnection.setAutoCommit(true);
			stm.close();
// 2013/04/01 kkato@PROSITE add end
			return true;
		} catch (IOException eIo) {
			eIo.printStackTrace();
			log.error(msg.getProperty("EK151016") + eIo.getMessage());
			System.out.println(msg.getProperty("DK150014"));
			return false;
// 2013/04/01 kkato@PROSITE add start
		} catch (SQLException eSql) {
			eSql.printStackTrace();
			log.error(msg.getProperty("EK151016") + eSql.getMessage());
			System.out.println(msg.getProperty("DK150014"));
			return false;
// 2013/04/01 kkato@PROSITE add end
		}
	}
//2011/07/19 rep and


//2011/08/03 全体見直し end
	/**
	 * メソッド名 : UrlFilter、ウィルスチェック、アンチスパイウェアログINSERT
	 * 機能概要 : UrlFilterログファイルからの全レコードのINSERTを行う
	 * @param args
	 * @return
	 * @see
	 */
	static boolean logUrlFilterInsert(Connection dbConnection, BufferedReader csvBr) {
		try {
			// ファイル処理ループ
			String logRec = "";// ログレコード
			String sTableName = "";
			String[] recList = null;
			int cnt=0;
			// 2013/04/01 kkato@PROSITE add start
			dbConnection.setAutoCommit(false);
			PreparedStatement stm = null;
			int rcnt = 0;
// 2013/04/01 kkato@PROSITE add end

			while ((logRec = csvBr.readLine()) != null) {
				cnt++;
// 2013/04/01 kkato@PROSITE add start
				rcnt++;
// 2013/04/01 kkato@PROSITE add end
// 2013/09/03 kkato@PROSITE add start
				//セーブポイント作成
	            Savepoint sp1 =  dbConnection.setSavepoint();
// 2013/09/03 kkato@PROSITE add end

//				// csvファイルレコード解析
//				String[] recList = logRec.split(",");

				// 2013/09/03 kkato@PROSITE del start
				//logRec=replaceQuote(logRec);
				//CSVParser csv = new CSVParser(new StringReader(logRec));
				// 2013/09/03 kkato@PROSITE del end

				// CSVParseエラーチェック
				try{
					// 2013/09/03 kkato@PROSITE add start
					logRec=replaceQuote(logRec);
					CSVParser csv = new CSVParser(new StringReader(logRec));
					// 2013/09/03 kkato@PROSITE add end
					recList = csv.getLine();
					// 項目の長さチェックと切り詰め処理
					recList = chkIdsIpsField(recList);
					// ファイルINSERT
					// 2013/09/03 kkato@PROSITE mod start
					//try {
					sTableName=null;
					if(recList.length >= SUBTYPE_INDEX){
						// Subtypeにより出力テーブルを変更する。
						sTableName=ULOG_INSERT_SQL.replaceAll("%table_name%",LOG_URLFILTER_TABLE);
						if( sTableName != null){
							// 2013/04/01 kkato@PROSITE mod start
							//PreparedStatement stm = dbConnection.prepareStatement(sTableName);
							stm = dbConnection.prepareStatement(sTableName);
							// 2013/04/01 kkato@PROSITE mod end
							// SQLパラメータセット
							stm.clearParameters();
							// 20121201 kkato@PROSITE mod start
							//int max = recList.length;
							int max = URLFILTER_LENGTH;
							// 20121201 kkato@PROSITE mod end
							for (int i = 0; i < max; i++) {
								stm.setString(i + 1, recList[i]);
							}
							// SQL実行
							stm.executeUpdate();
							// 2013/04/01 kkato@PROSITE mod start
							//stm.close();
							// 2013/04/01 kkato@PROSITE mod end
						}
					}
				} catch (SQLException e) {
					// 2013/09/03 kkato@PROSITE add start
					//ロールバック
					dbConnection.rollback(sp1);
					log.error(msg.getProperty("EK151017") + "エラー " + rcnt + "行目：" + e.getMessage());
					// 2013/09/03 kkato@PROSITE add end
					e.printStackTrace();
					// INSERTでエラーとなった際には該当レコードをログへ出力
					String errMsg = msg.getProperty("EK151018");
					int max = recList.length;
					for (int i = 0; i < max; i++) {
						if(i == 0){
							errMsg = errMsg + recList[i];
						}else{
							errMsg = errMsg + "," + recList[i];
						}
					}
					log.error(errMsg);
					//}
					// 2013/09/03 kkato@PROSITE mod end
				}catch (IOException eIo) {
					// 2013/09/03 kkato@PROSITE add start
					//ロールバック
					dbConnection.rollback(sp1);
					// 2013/09/03 kkato@PROSITE add end
					eIo.printStackTrace();
					log.error(msg.getProperty("EK151019") + "CSVParseエラー " + cnt + "行目" );
					log.error(logRec);
					System.out.println(msg.getProperty("DK150018"));
					// 2013/09/03 kkato@PROSITE add start
				} catch (Exception e) {
					//ロールバック
					dbConnection.rollback(sp1);
					e.printStackTrace();
					log.error(msg.getProperty("EK151019") + "エラー " + rcnt + "行目：" + e.getMessage());
					System.out.println(msg.getProperty("DK150018"));
					// 2013/09/03 kkato@PROSITE add end
				}
// 2013/04/01 kkato@PROSITE add start
				if( rcnt % commitInterval == 0) {
					dbConnection.commit();
					//System.out.println("commit:" + rcnt);
				}
// 2013/04/01 kkato@PROSITE add end
			}// ファイル処理ループ（終端）
// 2013/04/01 kkato@PROSITE add start
			dbConnection.commit();
			dbConnection.setAutoCommit(true);
			stm.close();
// 2013/04/01 kkato@PROSITE add end
			return true;
		} catch (IOException eIo) {
			eIo.printStackTrace();
			log.error(msg.getProperty("EK151019") + eIo.getMessage());
			System.out.println(msg.getProperty("DK150018"));
			return false;
// 2013/04/01 kkato@PROSITE add start
		} catch (SQLException eSql) {
			eSql.printStackTrace();
			log.error(msg.getProperty("EK151019") + eSql.getMessage());
			System.out.println(msg.getProperty("DK150018"));
			return false;
// 2013/04/01 kkato@PROSITE add end
		}
	}
//2011/08/03 全体見直し end

	/**
	 * メソッド名 : トラフィックログ1レコードのフィールドチェック
	 * 機能概要 : 各フィールドごとのトリムおよびサイズチェックを行いサイズが長い場合は切り詰める
	 * @param args
	 * @return
	 * @see
	 */
	static String[] chkTrafficField(String[] recList) {

		int[] maxlength=
			{
			   MAX_FUTURE_USE1, MAX_RECEIVE_TIME, MAX_SERIAL_NUMBER, MAX_TYPE, MAX_SUB_TYPE, MAX_FUTURE_USE2,
               MAX_FUTURE_USE3, MAX_SOURCE_IP, MAX_DESTINATION_IP, MAX_NAT_SOURCE_IP, MAX_NAT_DESTINATION_IP,
               MAX_RULE_NAME, MAX_SOURCE_USER, MAX_DESTINATION_USER, MAX_APPLICATION, MAX_VIRTUAL_SYSTEM,
               MAX_SOURCE_ZONE, MAX_DESTINATION_ZONE, MAX_INGRESS_INTERFACE, MAX_EGRESS_INTERFACE, MAX_LOG_FOWERD_PROFILE,
               MAX_FUTURE_USE4, MAX_SESSION_ID, MAX_REPEAT_COUNT, MAX_SOURCE_PORT, MAX_DESTINATION_PORT,
               MAX_NAT_SOURCE_PORT, MAX_NAT_DESTINATION_PORT, MAX_FLAGS, MAX_PROTOCOL, MAX_ACTION, MAX_BYTES,
               MAX_FUTURE_USE5, MAX_FUTURE_USE6, MAX_PACKETS, MAX_START_TIME, MAX_ELAPSED_TIME, MAX_CATEGORY,
// 20121201 kkato@PROSITE mod start
//		                   MAX_FUTURE_USE7 };
               MAX_FUTURE_USE7,
               MAX_SEQUENCE_NUMBER, MAX_ACTION_FLAGS, MAX_SOURCE_LOCATION, MAX_DESTINATION_LOCATION,
               MAX_FUTURE_USE8, PACKETS_SENT, PACKETS_RECEIVED
               // UPDATED: PANOS7対応（2017.03 by Plum Systems Inc.)
               , MAX_END_REASON, MAX_DGH_1, MAX_DGH_2, MAX_DGH_3, MAX_DGH_4, MAX_VSYS_NAME, MAX_DEVICENAME, MAX_ACTION_SOURCE
           	   // --> PANOS6対応
            };
// 20121201 kkato@PROSITE mod end

		for (int idx = 0; idx < recList.length; idx++) {
			if(idx < maxlength.length) { // ログの項目数がmaxlengthの項目より多い場合は切り捨てる
				String tmp = recList[idx].trim();
				if (GenericValidator.maxLength(tmp, maxlength[idx]) == false) {
					recList[idx] = tmp.substring(0, maxlength[idx] - 1);
				}
			}
		}
		return recList;
	}

	/**
	 * メソッド名 : IDS/IPSログ1レコードのフィールドチェック
	 * 機能概要 : 各フィールドごとのトリムおよびサイズチェックを行いサイズが長い場合は切り詰める
	 * @param args
	 * @return
	 * @see
	 */
	static String[] chkIdsIpsField(String[] recList) {

		int[] maxlength=
			{
			   MAX_FUTURE_USE1, MAX_RECEIVE_TIME, MAX_SERIAL_NUMBER, MAX_TYPE, MAX_SUB_TYPE, MAX_FUTURE_USE2,
               MAX_FUTURE_USE3, MAX_SOURCE_IP, MAX_DESTINATION_IP, MAX_NAT_SOURCE_IP, MAX_NAT_DESTINATION_IP,
               MAX_RULE_NAME, MAX_SOURCE_USER, MAX_DESTINATION_USER, MAX_APPLICATION, MAX_VIRTUAL_SYSTEM,
               MAX_SOURCE_ZONE, MAX_DESTINATION_ZONE, MAX_INGRESS_INTERFACE, MAX_EGRESS_INTERFACE, MAX_LOG_FOWERD_PROFILE,
               MAX_FUTURE_USE4, MAX_SESSION_ID, MAX_REPEAT_COUNT, MAX_SOURCE_PORT, MAX_DESTINATION_PORT,
               MAX_NAT_SOURCE_PORT, MAX_NAT_DESTINATION_PORT, MAX_FLAGS, MAX_PROTOCOL, MAX_ACTION, MAX_MISCELLANEOUS,
// 20121201 kkato@PROSITE mod start
//		                   MAX_THREAT_ID, MAX_CATEGORY, MAX_SEVERITY, MAX_DIRECTION };
               MAX_THREAT_ID, MAX_CATEGORY, MAX_SEVERITY, MAX_DIRECTION,
               MAX_SEQUENCE_NUMBER, MAX_ACTION_FLAGS, MAX_SOURCE_LOCATION, MAX_DESTINATION_LOCATION,
               MAX_FUTURE_USE8, MAX_CONTENT_TYPE
               // UPDATED: PANOS7対応（2017.03 by Plum Systems Inc.)
               , MAX_PCAPID, MAX_FILEDIGEST, MAX_CLOUD, MAX_URL_INDEX, MAX_USER_AGENT, MAX_FILE_TYPE, MAX_XFORWARD,
               MAX_REFERER, MAX_SENDER, MAX_SUBJECT, MAX_RECEIPIENT, MAX_REPORTID,
               MAX_DGH_1, MAX_DGH_2, MAX_DGH_3, MAX_DGH_4, MAX_VSYS_NAME, MAX_DEVICENAME, MAX_FUTURE_USE5
           	   // --> PANOS7対応
            };
// 20121201 kkato@PROSITE mod end

		for (int idx = 0; idx < recList.length; idx++) {
			if(idx < maxlength.length) { // ログの項目数がmaxlengthの項目より多い場合は切り捨てる
				String tmp = recList[idx].trim();
				if (GenericValidator.maxLength(tmp, maxlength[idx]) == false) {
					recList[idx] = tmp.substring(0, maxlength[idx] - 1);
				}
			}
		}
		return recList;
	}

//2011/08/03 add start
	/**
	 * メソッド名 : replaceQuote
	 * 機能概要 : "(ダブルクォーテーション)で囲まれた項目に"がある場合、""に置き換える
	 * @param args
	 * @return
	 * @see
	 */
    static String replaceQuote(String src) {

        int stpo = src.indexOf('"');
    	if (stpo == -1) {
            return src;
        }
    	int edpo = src.lastIndexOf('"');
    	if (edpo == -1) {
            return src;
        }
    	if( stpo==edpo ){
            return src;
    	}
        // "(ダブルクォテーション）内の"を""に置換
        String buffer = src.substring(stpo+1, edpo);
        buffer = buffer.replaceAll("\"","\"\"");
        return src.substring(0,stpo+1) + buffer + src.substring(edpo);
    }
}
//2011/08/03 add end
