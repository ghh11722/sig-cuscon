/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコンバッチ マスター情報蓄積
 * ファイル名   : ConvNameSeq.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/05     matsshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.master.bussiness;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;
import jp.co.kddi.secgw.cuscon.batch.master.dao.*;

/**
 * クラス名 : ConvNameSeq
 * 機能概要 : カテゴリー、サブカテゴリー、テクノロジーののデータに対し、名称よりシーケンス番号を取得し、MAPに追加する。
 * 備考 :
 * @author matsshtt
 * @version 1.0 matsshtt
 *          Created 2010/03/05
 *          新規作成
 * @see
 */
public class ConvNameSeq {

	public static final String contxtFileName = "applicationContext.xml";
	private static final String daoName_SelCate = "SelectCategory";
	private static final String daoName_SelSubC = "SelectSubCategory";
	private static final String daoName_SelTech = "SelectTechnology";
	private static final String daoName_InsCate = "InsertCategory";
	private static final String daoName_InsSubC = "InsertSubCategory";
	private static final String daoName_InsTech = "InsertTechnology";


	private static final String mapKey_categorySeqNo = "category_seq_no";
	private static final String mapKey_subCategorySeqNo = "sub_category_seq_no";
	private static final String mapKey_technologySeqNo = "technology_seq_no";
	private static final String mapKey_seqNo = "seq_no";
	private static final String mapKey_category = "category";
	private static final String mapKey_technology = "technology";
	private static final String mapKey_sub_category = "sub_category"; 	//Category一覧用 (DBの項目名)
	private static final String mapKey_subcategory = "subcategory"; 	//inMapList用 (Palo AltoのXMLタグ )


	// ロガー
    Logger log = LogUtil.getCallerLogger();

    private SelectCategoryDao cdao;
    private SelectSubCategoryDao scdao;
    private SelectTechnologyDao tdao;
    private InsertCategoryDao InsCateDao;
    private InsertSubCategoryDao InsSubCDao;
    private InsertTechnologyDao InsTechDao;


	public ConvNameSeq(){
	    log.debug( LogUtil.getClassMethod() + " 開始");
	    // アプリケーションコンテキスト取得
	    ApplicationContext context = new ClassPathXmlApplicationContext(contxtFileName);

	    this.cdao = (SelectCategoryDao)context.getBean(daoName_SelCate);
	    this.scdao = (SelectSubCategoryDao)context.getBean(daoName_SelSubC);
	    this.tdao = (SelectTechnologyDao)context.getBean(daoName_SelTech);

		this.InsCateDao = (InsertCategoryDao)context.getBean(daoName_InsCate);
		this.InsSubCDao = (InsertSubCategoryDao)context.getBean(daoName_InsSubC);
		this.InsTechDao = (InsertTechnologyDao)context.getBean(daoName_InsTech);

    	log.debug( LogUtil.getClassMethod() + " 終了");
	}

	public void convert(List<HashMap<String, Object>> inMapList) throws DataAccessException {
	    log.debug( LogUtil.getClassMethod() + " 開始");

		List<HashMap<String, Object>> CategoryList = null;
		List<HashMap<String, Object>> SubCategoryList = null;
		List<HashMap<String, Object>> TechnologyList = null;

		//Category一覧取得
		try {
			CategoryList = cdao.select();
		} catch (DataAccessException e) {
	    	log.debug("Category一覧取得でDataAccessException");
        	throw e;
	    }

	  //SubCategory一覧取得
		try {
			SubCategoryList = scdao.select();
		} catch (DataAccessException e) {
	    	log.debug("SubCategory一覧取得でDataAccessException");
        	throw e;
	    }

		//Technology一覧取得
		try {
			TechnologyList = tdao.select();
		} catch (DataAccessException e) {
	    	log.debug("Technology一覧取得でDataAccessException");
        	throw e;
	    }

		for(int k=0; k < inMapList.size(); ++k) {
			HashMap<String, Object> tmpMap = inMapList.get(k);
	    	log.debug("名前=>SEQ_NO変換 Application="+inMapList.get(k).get("name"));

	    	// カテゴリー
	    	if(inMapList.get(k).containsKey(mapKey_category)) {
    			log.debug("Category_seq_no変換 Category="+inMapList.get(k).get(mapKey_category));

	    		// Category名が一致するデータをCategory一覧から探す
	    		for(int i = 0; i < CategoryList.size(); ++i) {
	    			log.debug("比較相手 ="+CategoryList.get(i).get(mapKey_category));
	    			// 一致するデータあり
	    			if(	CategoryList.get(i).get(mapKey_category).equals(inMapList.get(k).get(mapKey_category)) ) {
	    				//Category_seq_noを取得&登録
	    				log.debug("変換成功 Category_seq_no="+CategoryList.get(i).get(mapKey_seqNo));
	    				inMapList.get(k).put(mapKey_categorySeqNo , CategoryList.get(i).get(mapKey_seqNo) );
	    				break;
	    			}
	    		}
	    		// 既存のデータとカテゴリー名が一致しなかった場合
				if(!inMapList.get(k).containsKey(mapKey_categorySeqNo )) {
	    			log.debug("Category_seq_no変換 一致データなし");
					//カテゴリーを登録
					int cat_seq_no;
					try {
		    			log.debug("カテゴリー登録 Category="+inMapList.get(k).get(mapKey_category));
						cat_seq_no = InsCateDao.insert((String)inMapList.get(k).get(mapKey_category));
					} catch (DataAccessException e) {
				    	log.debug("Category登録でDataAccessException");
			        	throw e;
				    }

					// 採番されたSEQ_NOでCategory_seq_noを登録
    				inMapList.get(k).put(mapKey_categorySeqNo , cat_seq_no );

					//登録したカテゴリーを、Category一覧に追加
					HashMap cateMap = new HashMap();
					cateMap.put(mapKey_category, (String)inMapList.get(k).get(mapKey_category));
					cateMap.put(mapKey_seqNo, cat_seq_no);
					CategoryList.add(cateMap);
				}
	    	} else {
	    		// カテゴリーを含まないデータの場合、Category_seq_no=Nullで登録
		    	log.debug("Category_seq_noにnullを設定");
				inMapList.get(k).put(mapKey_categorySeqNo , null );
	    	}

	    	// サブカテゴリー
	    	if(inMapList.get(k).containsKey(mapKey_subcategory)) {
		    	log.debug("Sub_Category_seq_no変換 SubCategory="+inMapList.get(k).get(mapKey_subcategory));

	    		// SubCategory名が一致するデータをSubCategory一覧から探す
	    		for(int i = 0; i < SubCategoryList.size(); ++i) {
	    			log.debug("比較相手 ="+SubCategoryList.get(i).get(mapKey_sub_category));

	    			// 一致するデータあり
					if(	SubCategoryList.get(i).get(mapKey_sub_category).equals(inMapList.get(k).get(mapKey_subcategory)) ) {
	    				// SubCategory_seq_noを取得&登録
						log.debug("変換成功 Sub_Category_seq_no="+SubCategoryList.get(i).get(mapKey_seqNo));
						inMapList.get(k).put(mapKey_subCategorySeqNo, SubCategoryList.get(i).get(mapKey_seqNo) );
	    				break;
					}
	    		}

	    		// 既存のデータとサブカテゴリー名が一致しなかった場合
				if(!inMapList.get(k).containsKey(mapKey_subCategorySeqNo )) {
	    			log.debug("SubCategory_seq_no変換 一致データなし");
					//サブカテゴリーを登録
					int subc_seq_no;
					try {
		    			log.debug("サブカテゴリー登録 SubCategory="+inMapList.get(k).get(mapKey_subcategory));
						subc_seq_no = InsSubCDao.insert((String)inMapList.get(k).get(mapKey_subcategory));
					} catch (DataAccessException e) {
				    	log.debug("SubCategory登録でDataAccessException");
			        	throw e;
				    }

					// 採番されたSEQ_NOでCategory_seq_noを登録
    				inMapList.get(k).put(mapKey_subCategorySeqNo , subc_seq_no );

					//登録したサブカテゴリーを、SubCategory一覧に追加
					HashMap subcMap = new HashMap();
					subcMap.put(mapKey_sub_category, (String)inMapList.get(k).get(mapKey_subcategory));
					subcMap.put(mapKey_seqNo, subc_seq_no);
					SubCategoryList.add(subcMap);
				}
	    	} else {
	    		// サブカテゴリーを含まないデータの場合、SubCategory_seq_no=Nullで登録
		    	log.debug("SubCategory_seq_noにnullを設定");
				inMapList.get(k).put(mapKey_subCategorySeqNo , null );
	    	}

	    	// テクノロジー
	    	if(inMapList.get(k).containsKey(mapKey_technology)) {
		    	log.debug("Technology_seq_no変換 Technology="+inMapList.get(k).get(mapKey_technology));

	    		// Technology名が一致するデータをTechnology一覧から探す
				for(int i = 0; i < TechnologyList.size(); ++i) {
	    			log.debug("比較相手 ="+TechnologyList.get(i).get(mapKey_technology));

	    			// 一致するデータあり
					if(	TechnologyList.get(i).get(mapKey_technology).equals(inMapList.get(k).get(mapKey_technology)) ) {
	    				//Technology_seq_noを取得&登録
				    	log.debug("変換成功 Technology_seq_no="+TechnologyList.get(i).get(mapKey_seqNo));
						inMapList.get(k).put(mapKey_technologySeqNo, TechnologyList.get(i).get(mapKey_seqNo) );
				    	log.debug("test Technology_seq_no="+inMapList.get(k).get(mapKey_technologySeqNo));
	    				break;
					}
	    		}

	    		// 既存のデータとテクノロジ名が一致しなかった場合
				if(!inMapList.get(k).containsKey(mapKey_technologySeqNo )) {
	    			log.debug("Technology_seq_no変換 一致データなし");
					//テクノロジーを登録
					int tech_seq_no;
					try {
		    			log.debug("テクノロジー登録 Technology="+inMapList.get(k).get(mapKey_technology));
						tech_seq_no = InsTechDao.insert((String)inMapList.get(k).get(mapKey_technology));
					} catch (DataAccessException e) {
				    	log.debug("Technology登録でDataAccessException");
			        	throw e;
				    }

					// 採番されたSEQ_NOでTechnology_seq_noを登録
    				inMapList.get(k).put(mapKey_technologySeqNo , tech_seq_no );

					//登録したテクノロジーを、Technology一覧に追加
					HashMap techMap = new HashMap();
					techMap.put(mapKey_technology, (String)inMapList.get(k).get(mapKey_technology));
					techMap.put(mapKey_seqNo, tech_seq_no);
					TechnologyList.add(techMap);
				}
	    	} else {
	    		// テクノロジーを含まないデータの場合、Category_seq_no=Nullで登録
		    	log.debug("Technology_seq_noにnullを設定");
				inMapList.get(k).put(mapKey_technologySeqNo, null );
	    	}
		}
    	log.debug( LogUtil.getClassMethod() + " 終了");
	}

}
