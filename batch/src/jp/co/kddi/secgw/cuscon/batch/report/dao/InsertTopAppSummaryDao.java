/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : InsertTopAppSummaryDao.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/26     matsshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.report.dao;

import java.sql.Types;
import java.util.*;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


/**
 * クラス名：InsertTopAppSummaryDao<br>
 * 機能概要：出力データのDB登録クラスです。<br>
 * 備考：<br>
 * @author matsshtt
 * @version 1.0 matsshtt<br>
 *             Created 2010/02/28<br>
 *             新規作成
 *
 */
public class InsertTopAppSummaryDao {
	// ロガー
    private Logger log = LogUtil.getCallerLogger();
    /** データソース */
    private DriverManagerDataSource dataSource;
    // インプットMAPファイルのキー
    private static final String xmlTag_app = "app";
    private static final String xmlTag_riskOfApp = "risk-of-app";
    private static final String xmlTag_bytes = "bytes";
    private static final String xmlTag_sessions = "sessions";
    private static final String xmlTag_vsysID = "vsysid";
    private static final String xmlTag_targetDate = "target_date";

    /**
     * メソッド名：Appトップサマリレポート登録SP実行<br>
     * 機能概要：Appトップサマリレポート登録SPの実行<br>
     *
     * @param List<HashMap<String,Object>> in 登録するデータのリスト
     * @param String vsysID 登録するデータのVsysID
     * @param String targetDate 登録するデータのtargetdate
     */
    public void insert(List<HashMap<String,Object>> in,
    					String vsysID,
    					String targetDate) throws DataAccessException {

      	log.debug( LogUtil.getClassMethod() + " 開始");

    	TopAppSummary_InsertSP sproc = new TopAppSummary_InsertSP(this.dataSource);

    	// データの件数分ループ
    	int cnt = 0;
    	for (int i = 0; i < in.size(); i++) {
    		Map tmpMap =in.get(i);
    		log.debug("AppトップサマリレポートInsert roop count="+ i);

    		// データの欠損チェック
    		if(!tmpMap.containsKey(xmlTag_app)){
    			tmpMap.put(xmlTag_app, null);
     			log.debug("appをnullで補足しました");
    		}
    		if(!tmpMap.containsKey(xmlTag_riskOfApp)){
    			tmpMap.put(xmlTag_riskOfApp, null);
     			log.debug("riskOfAppをnullで補足しました");
    		}
    		if(!tmpMap.containsKey(xmlTag_bytes)){
    			tmpMap.put(xmlTag_bytes, null);
     			log.debug("bytesをnullで補足しました");
    		}
    		if(!tmpMap.containsKey(xmlTag_sessions)){
    			tmpMap.put(xmlTag_sessions, null);
      			log.debug("sessionsをnullで補足しました");
    		}

    		// VsysID と TargetDate を付加
    		tmpMap.put(xmlTag_vsysID, vsysID);
    		tmpMap.put(xmlTag_targetDate, targetDate);

    		// ストアド実行
    		try {
    			sproc.execute(tmpMap);
    			cnt += 1;
    		} catch(DataAccessException e) {
    			log.fatal(LogUtil.getErrorMSG("FK170014"),e);
    			throw e;
    		}

    		tmpMap.clear();
    	 }
    	log.info( LogUtil.getErrorMSG("IK170016") + cnt);

       	log.debug( LogUtil.getClassMethod() + " 終了");
    }

    /**
     * クラス名：Appトップサマリレポート登録内部クラス<br>
     * 機能概要：Appトップサマリレポート登録内部クラス<br>
     */
    private class TopAppSummary_InsertSP extends StoredProcedure {
        public static final String SP_NAME = "T_TopAppSummary_insert";


        /**
         * メソッド名：Appトップサマリレポート登録SP設定<br>
         * 機能概要：Appトップサマリレポート登録SPの設定<br>
         *
         * @param ds データソース
         */
        public TopAppSummary_InsertSP(DriverManagerDataSource ds) {

        	setDataSource(ds);
            setFunction(false);
            setSql(SP_NAME);
            declareParameter(new SqlParameter(xmlTag_vsysID, Types.VARCHAR));
            declareParameter(new SqlParameter(xmlTag_targetDate, Types.VARCHAR));
            declareParameter(new SqlParameter(xmlTag_app, Types.VARCHAR));
            declareParameter(new SqlParameter(xmlTag_riskOfApp, Types.VARCHAR));
            declareParameter(new SqlParameter(xmlTag_bytes, Types.VARCHAR));
            declareParameter(new SqlParameter(xmlTag_sessions, Types.VARCHAR));
            compile();

        }
    }

    /**
     * メソッド名：データソース設定<br>
     * 機能概要：データソースをセットする<br>
     * @param ds データソース
     */
    public void setdataSource(DriverManagerDataSource ds) {
        this.dataSource = ds;
    }


}

