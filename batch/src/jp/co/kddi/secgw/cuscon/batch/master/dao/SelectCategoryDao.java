/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコンバッチ
 * ファイル名   : SelectCategoryDao.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     matsshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.master.dao;

import java.util.*;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


/**
 * クラス名：SelectCategoryDao<br>
 * 機能概要：カテゴリマスタ検索クラスです。<br>
 * 備考：<br>
 * @author matsshtt
 * @version 1.0 matsshtt<br>
 *             Created 2010/03/01<br>
 *             新規作成
 */
public class SelectCategoryDao {
	// ロガー
    Logger log = LogUtil.getCallerLogger();
    /** データソース */
    private DriverManagerDataSource dataSource;

    /**
     * メソッド名：select<br>
     * 機能概要：select文を実行する<br>
     */
    public List select() {
	    log.debug( LogUtil.getClassMethod() + " 開始");

	    List results = null;
        final String sql = "select SEQ_NO, CATEGORY from m_category;";
        try {
        	JdbcTemplate jt = new JdbcTemplate(this.dataSource);
        	results = jt.queryForList(sql);
        } catch (DataAccessException e) {
         	log.fatal(LogUtil.getErrorMSG("FK160001"),e);
    		throw e;
        }

	    log.debug( LogUtil.getClassMethod() + " 終了");

        return results;
    }


    /**
     * メソッド名：データソース設定<br>
     * 機能概要：データソースをセットする<br>
     * @param ds データソース
     */
    public void setdataSource(DriverManagerDataSource ds) {
        this.dataSource = ds;
    }

}

