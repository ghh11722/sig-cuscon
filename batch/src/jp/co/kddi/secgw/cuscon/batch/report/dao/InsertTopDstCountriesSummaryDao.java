/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : InsertTopDstCountriesSummaryDao.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/26     matsshtt                初版作成
 *******************************************************************************
 */

package jp.co.kddi.secgw.cuscon.batch.report.dao;

import java.sql.Types;
import java.util.*;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
/**
 * クラス名：InsertTopDstCountriesSummaryDao<br>
 * 機能概要：出力データのDB登録クラスです。<br>
 * 備考：<br>
 * @author matsshtt
 * @version 1.0 matsshtt<br>
 *             Created 2010/02/28<br>
 *             新規作成
 *
 */
public class InsertTopDstCountriesSummaryDao {
	// ロガー
    private Logger log = LogUtil.getCallerLogger();
    /** データソース */
    private DriverManagerDataSource dataSource;
    // インプットMAPファイルのキー
    private static final String xmlTag_dstloc = "dstloc";
    private static final String xmlTag_bytes = "bytes";
    private static final String xmlTag_sessions = "sessions";
    private static final String xmlTag_vsysID = "vsysid";
    private static final String xmlTag_targetDate = "target_date";

    /**
     * メソッド名：DstCountriesトップサマリレポート登録SP実行<br>
     * 機能概要：DstCountriesトップサマリレポート登録SPの実行<br>
     *
     * @param List<HashMap<String,Object>> in 登録するデータのリスト
     * @param String vsysID 登録するデータのVsysID
     * @param String targetDate 登録するデータのtargetdate
     */
    public void insert(List<HashMap<String,Object>> in,
    					String vsysID,
    					String targetDate) throws DataAccessException{

      	log.debug( LogUtil.getClassMethod() + " 開始");

    	TopDstCountriesSummary_InsertSP sproc = new TopDstCountriesSummary_InsertSP(this.dataSource);

    	// データの件数分ループ
    	int cnt = 0;
    	for (int i = 0; i < in.size(); i++) {
    		Map tmpMap =in.get(i);
    		log.debug("宛先国トップサマリレポート Insert roop count="+ i);

    		// データの欠損チェック
    		if(!tmpMap.containsKey(xmlTag_dstloc)){
    			tmpMap.put(xmlTag_dstloc, null);
      			log.debug("dstlocをnullで補足しました");
    		}
    		if(!tmpMap.containsKey(xmlTag_bytes)){
    			tmpMap.put(xmlTag_bytes, null);
      			log.debug("bytesをnullで補足しました");
    		}
    		if(!tmpMap.containsKey(xmlTag_sessions)){
    			tmpMap.put(xmlTag_sessions, null);
      			log.debug("sessionsをnullで補足しました");
    		}

    		// VsysID と TargetDate を付加
    		tmpMap.put(xmlTag_vsysID, vsysID);
    		tmpMap.put(xmlTag_targetDate, targetDate);

    		// ストアド実行
    		try{
    			sproc.execute(tmpMap);
    			cnt += 1;
    		} catch (DataAccessException e) {
    			log.fatal(LogUtil.getErrorMSG("FK170014"), e);
    			throw e;
    		}

    		tmpMap.clear();
    	}
    	log.info( LogUtil.getErrorMSG("IK170019") + cnt);

    	log.debug( LogUtil.getClassMethod() + " 終了");
    }

    /**
     * クラス名：DstCountriesトップサマリレポート登録内部クラス<br>
     * 機能概要：DstCountriesトップサマリレポート登録内部クラス<br>
     */
    private class TopDstCountriesSummary_InsertSP extends StoredProcedure {
        public static final String SP_NAME = "T_TopDstCountriesSummary_Insert";


        /**
         * メソッド名：DstCountriesトップサマリレポート登録SP設定<br>
         * 機能概要：DstCountriesトップサマリレポート登録SPの設定<br>
         *
         * @param ds データソース
         */
        public TopDstCountriesSummary_InsertSP(DriverManagerDataSource ds) {
        	setDataSource(ds);
            setFunction(false);
            setSql(SP_NAME);
            declareParameter(new SqlParameter(xmlTag_vsysID, Types.VARCHAR));
            declareParameter(new SqlParameter(xmlTag_targetDate, Types.VARCHAR));
            declareParameter(new SqlParameter(xmlTag_dstloc, Types.VARCHAR));
            declareParameter(new SqlParameter(xmlTag_bytes, Types.VARCHAR));
            declareParameter(new SqlParameter(xmlTag_sessions, Types.VARCHAR));
            compile();
        }
    }

    /**
     * メソッド名：データソース設定<br>
     * 機能概要：データソースをセットする<br>
     * @param ds データソース
     */
    public void setdataSource(DriverManagerDataSource ds) {

        this.dataSource = ds;

    }

}