/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコンバッチ マスター情報蓄積
 * ファイル名	: MasterInfoChikuseki.java
 *
 * [変更履歴]
 * 日付 		  更新者				  内容
 * 2010/03/05	  matsshtt				  初版作成
 * 2011/05/19	  inoue@prosite			  Webフィルタカテゴリ取込追加
 * 2011/10/30	  inoue@prosite			  アプリケーションコンテナ取込追加
 * 2013/09/30     kkato@PROSITE           エラー時のリトライ処理追加
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.master;

import java.io.InputStream;
import java.util.*;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;
import jp.co.kddi.secgw.cuscon.batch.master.bussiness.*;
import jp.co.kddi.secgw.cuscon.batch.master.dao.*;

/**
 * クラス名 : MasterInfoChikuseki
 * 機能概要 : マスタ情報をPAより取得し
 * 、cusconDBに保存する
 * 備考 :
 * @author matsshtt
 * @version 1.0 matsshtt
 *			Created 2010/03/05
 *			新規作成
 * @version 1.1 inoue@prosite
 *			Created 2011/05/19
 *			Webフィルタカテゴリ取込追加
 * @version 1.2 inoue@prosite
 *			Created 2011/10/31
 *			アプリケーションコンテナ取込追加
 * @see
 */
public class MasterInfoChikuseki {

	public static final String contxtFileName = "applicationContext.xml";
	public static final String daoName_SelSys = "SelectSystemConstant";
	public static final String daoName_SelApl = "SelectApplications";
	public static final String daoName_UpdApl = "UpdateApplications";
	public static final String daoName_DelApl = "DeleteApplications";
	public static final String daoName_InsApl = "InsertApplications";
	public static final String daoName_SelSrv = "SelectServices";
	public static final String daoName_UpdSrv = "UpdateServices";
	public static final String daoName_DelSrv = "DeleteServices";
	public static final String daoName_InsSrv = "InsertServices";

	public static final String	APPLICATION = "application";
	public static final String	SERVICE = "service";
	public static final String SERVICE_SHARED = "service_shared";

	public static final String	mapKey_Name = "name";
	public static final String	mapKey_AplGrpLnk = "AplGrpLnk";
	public static final String	mapKey_AplLnk = "AplLnk";
	public static final String	mapKey_SrvGrpLnk = "SrvGrpLnk";
	public static final String	mapKey_SrvLnk = "SrvLnk";

// 20110519 inoue@PROSITE add start
	public static final String daoName_SelUrlCategory = "SelectUrlFilterCategory";
	public static final String daoName_InsUrlCategory = "InsertUrlFilterCategory";
// 20110519 inoue@PROSITE add end

// 2011/10/31 add start inoue@prosite
	public static final String APPLICATION_CONTAINER = "application-container";
	public static final String daoName_SelAplCont = "SelectApplicationContainer";
	public static final String daoName_DelAplCont = "DeleteApplicationContainer";
	public static final String daoName_InsAplCont = "InsertApplicationContainer";
// 2011/10/31 add start inoue@prosite

	public static void main(String[] arg) throws Exception {
		// ロガー
		Logger log = LogUtil.getCallerLogger();

		log.info( LogUtil.getClassMethod() + " 開始");

		ApplicationContext context = null;
		try {
			context = new ClassPathXmlApplicationContext(contxtFileName);
		} catch (BeansException e){
			log.fatal(LogUtil.getErrorMSG("FK160022"),e);
			System.exit(0);
		}

		// DBより、PA(Palo Alto)へのログイン情報を取得する
		SelectSystemConstantDao ssdao = (SelectSystemConstantDao)context.getBean(daoName_SelSys);
		List<HashMap> SystemConstantList = ssdao.select();
		if(SystemConstantList == null) {
			log.fatal(LogUtil.getErrorMSG("FK160003"));
			System.exit(0);
		}

		// 20130901 kkato@PROSITE add start
	  	Properties propm = new Properties();
		try {
			InputStream inStream = MasterInfoChikuseki.class.getClassLoader().getResourceAsStream("masterInfo.properties");
			 propm.load(inStream);
		} catch (Exception e) {
			log.fatal(LogUtil.getErrorMSG("FK170015"),e);
			System.exit(0);
		}
		int pa_retry_cnt = Integer.parseInt(propm.getProperty("pa_retry_cnt"));
		int pa_retry_wait = Integer.parseInt(propm.getProperty("pa_retry_wait_time"));
		 // リトライ回数カウンター
		int retryCnt = 0;
		// 20130901 kkato@PROSITE add end

		// PAより、アプリケーションマスタを取得する
// 20130901 kkato@PROSITE del start
//		GetMasterInfo gm_app = null;
//		try {
//			gm_app = new GetMasterInfo((HashMap)SystemConstantList.get(0));
//		} catch (Exception e1) {
//			log.fatal(LogUtil.getErrorMSG("FK160008"));
//			System.exit(0);
//		}
// 20130901 kkato@PROSITE del end

/////////////////////////////////////////////////////////////////////
// 2011/07/15 アプリケーションマスタ取得エラー時は以降も継続
/////////////////////////////////////////////////////////////////////
// 2011/0715 comment start inoue@prosite
//		List<HashMap<String, Object>> PAAplList = gm_app.getPAMaster(APPLICATION);
//		if(PAAplList == null) {
//			log.fatal(LogUtil.getErrorMSG("FK160008"));
//			System.exit(0);
//		}
//
//		// PAのアプリケーションマスタにはないアプリは、cusconDBより削除する
//		// ただし、「アプリケーショングループリンクTBL」と「アプリケーションリンクTBL」に参照されているものは除く
//		DeleteApplicationsDao ddao = (DeleteApplicationsDao)context.getBean(daoName_DelApl);
//		try{
//			ddao.delete(PAAplList);
//		} catch (DataAccessException e){
//			log.fatal(LogUtil.getErrorMSG("FK160021"));
//			System.exit(0);
//		}
//
//		// PAから取得したアプリケーションマスタのデータの中身を、cusconDB上のSEQ_NOに変換する
//		ConvNameSeq convNameSeq = new ConvNameSeq();
//		try {
//			convNameSeq.convert(PAAplList);
//		} catch (DataAccessException e){
//			log.fatal(LogUtil.getErrorMSG("FK160010"));
//			System.exit(0);
//		}
//
//		// cusconDBのアプリケーションマスタを取得する
//		SelectApplicationsDao adao = (SelectApplicationsDao)context.getBean(daoName_SelApl);
//		List<HashMap<String, Object>> CusAplList = adao.select();
//		if(CusAplList == null) {
//			log.fatal(LogUtil.getErrorMSG("FK160009"));
//			System.exit(0);
//		}
//
//		// PAから取得したアプリケーションマスタのデータを、cuscondbに登録済みと未登録に分ける
//		List PAAplList_UPD = new ArrayList();
//		for(int k=0; k < PAAplList.size(); ++k) {
//			log.debug("PAアプリ名="+(String)PAAplList.get(k).get(mapKey_Name));
//
//			for(int i=0; i < CusAplList.size(); ++i) {
//				log.debug("cusconアプリ名="+(String)CusAplList.get(i).get(mapKey_Name));
//
//				// アプリ名が同一の場合
//				if(((String)CusAplList.get(i).get(mapKey_Name)).equals((String)PAAplList.get(k).get(mapKey_Name)) ){
//					log.debug("アプリ名一致");
//
//					// PAのUpdate用アプリリストに追加
//					PAAplList_UPD.add(PAAplList.get(k));
//
//					// PAのアプリリスト(=Insert用)から除去
//					PAAplList.remove(k);
//					k = k -1;
//
//					break;
//				}
//			}
//		}
//
//		// 登録済みアプリケーションの更新
//		UpdateApplicationsDao udao = (UpdateApplicationsDao)context.getBean(daoName_UpdApl);
//		try{
//			udao.update(PAAplList_UPD);
//		} catch (DataAccessException e){
//			log.fatal(LogUtil.getErrorMSG("FK160011"));
//			System.exit(0);
//		}
//
//		// 未登録アプリケーションの挿入
//		InsertApplicationsDao idao = (InsertApplicationsDao)context.getBean(daoName_InsApl);
//		try{
//			idao.insert(PAAplList);
//		} catch (DataAccessException e){
//			log.fatal(LogUtil.getErrorMSG("FK160012"));
//			System.exit(0);
//		}
//
//		// 使用されなくなったアプリケーションマスタ関連TBL(カテゴリ、サブカテゴリ、テクノロジ)のデータを削除する
//		UpdateAplRelatedTbl mntapltblcls = new UpdateAplRelatedTbl();
//		try {
//			mntapltblcls.delete();
//		} catch (Exception e){
//			log.debug("アプリケーションマスタ関連TBLの削除失敗");
//			System.exit(0);
//		}
// 2011/07/15 comment end inoue@prosite

// 2011/07/15 add start inoue@prosite
		// 20130901 kkato@PROSITE del start
		//boolean appErrFlg = false;
		//List<HashMap<String, Object>> PAAplList = gm_app.getPAMaster(APPLICATION);
		//if(PAAplList == null) {
		//	log.fatal(LogUtil.getErrorMSG("FK160008"));
		//	appErrFlg = true;
		//}
		// 20130901 kkato@PROSITE del end

		// 20130901 kkato@PROSITE add start
		// PAより、アプリケーションマスタを取得する
		boolean appErrFlg = false;
		GetMasterInfo gm_app = null;
		List<HashMap<String, Object>> PAAplList = null;
		 // リトライ回数
		retryCnt = 0;
		// リトライ回数分繰り返す。
		while (true) {
			retryCnt++;
			try {
				gm_app = new GetMasterInfo((HashMap)SystemConstantList.get(0));
				PAAplList = gm_app.getPAMaster(APPLICATION);
				if(PAAplList == null) {
					throw new Exception();
				}
				break;
			} catch (Exception e) {
				// コマンド実行エラー
				if (retryCnt >= pa_retry_cnt){
					log.fatal(LogUtil.getErrorMSG("FK160008"),e);
					appErrFlg = true;
					break;
				}
				log.error(LogUtil.getErrorMSG("FK160008") + " リトライ:" + retryCnt,e);
				Thread.sleep(pa_retry_wait);
			}
		}
		// 20130901 kkato@PROSITE add end

		// PAより取得できた場合に限り、アプリケーションマスタの処理継続
		if( !appErrFlg ){
/*↓DEL:2012/02/16 T.Suzuki(NOP) 意図しない置き換わりを防ぐためアプリケーションマスタの削除処理を排除 */
//			// PAのアプリケーションマスタにはないアプリは、cusconDBより削除する
//			// ただし、「アプリケーショングループリンクTBL」と「アプリケーションリンクTBL」に参照されているものは除く
//			DeleteApplicationsDao ddao = (DeleteApplicationsDao)context.getBean(daoName_DelApl);
//			try{
//				ddao.delete(PAAplList);
//			} catch (DataAccessException e){
//				log.fatal(LogUtil.getErrorMSG("FK160021"));
//				System.exit(0);
//			}
/*↑DEL:2012/02/16 T.Suzuki(NOP) 意図しない置き換わりを防ぐためアプリケーションマスタの削除処理を排除 */
// 20130901 kkato@PROSITE mod start　エラー時も処理継続するように修正
			try {
				// PAから取得したアプリケーションマスタのデータの中身を、cusconDB上のSEQ_NOに変換する
				ConvNameSeq convNameSeq = new ConvNameSeq();
				try {
					convNameSeq.convert(PAAplList);
				} catch (DataAccessException e){
					log.fatal(LogUtil.getErrorMSG("FK160010"));
					//System.exit(0);
					throw new Exception();
				}

				// cusconDBのアプリケーションマスタを取得する
				SelectApplicationsDao adao = (SelectApplicationsDao)context.getBean(daoName_SelApl);
				List<HashMap<String, Object>> CusAplList = adao.select();
				if(CusAplList == null) {
					log.fatal(LogUtil.getErrorMSG("FK160009"));
					//System.exit(0);
					throw new Exception();
				}

				// PAから取得したアプリケーションマスタのデータを、cuscondbに登録済みと未登録に分ける
				List PAAplList_UPD = new ArrayList();
				for(int k=0; k < PAAplList.size(); ++k) {
					log.debug("PAアプリ名="+(String)PAAplList.get(k).get(mapKey_Name));

					for(int i=0; i < CusAplList.size(); ++i) {
						log.debug("cusconアプリ名="+(String)CusAplList.get(i).get(mapKey_Name));
						// アプリ名が同一の場合
						if(((String)CusAplList.get(i).get(mapKey_Name)).equals((String)PAAplList.get(k).get(mapKey_Name)) ){
							log.debug("アプリ名一致");
							// PAのUpdate用アプリリストに追加
							PAAplList_UPD.add(PAAplList.get(k));
							// PAのアプリリスト(=Insert用)から除去
							PAAplList.remove(k);
							k = k -1;
							break;
						}
					}
				}

				// 登録済みアプリケーションの更新
				UpdateApplicationsDao udao = (UpdateApplicationsDao)context.getBean(daoName_UpdApl);
				try{
					udao.update(PAAplList_UPD);
				} catch (DataAccessException e){
					log.fatal(LogUtil.getErrorMSG("FK160011"));
					//System.exit(0);
					throw new Exception();
				}

				// 未登録アプリケーションの挿入
				InsertApplicationsDao idao = (InsertApplicationsDao)context.getBean(daoName_InsApl);
				try{
					idao.insert(PAAplList);
				} catch (DataAccessException e){
					log.fatal(LogUtil.getErrorMSG("FK160012"));
					//System.exit(0);
					throw new Exception();
				}

				// 使用されなくなったアプリケーションマスタ関連TBL(カテゴリ、サブカテゴリ、テクノロジ)のデータを削除する
				UpdateAplRelatedTbl mntapltblcls = new UpdateAplRelatedTbl();
				try {
					mntapltblcls.delete();
				} catch (Exception e){
					log.debug("アプリケーションマスタ関連TBLの削除失敗");
					//System.exit(0);
					throw new Exception();
				}
			} catch (Exception e) {
				log.debug("アプリケーションマスタ処理でエラーの為 appErrFlg=true セット");
				appErrFlg = true;
			}
		}
// 20130901 kkato@PROSITE mod end
// 20130901 kkato@PROSITE add start
		// アプリケーションマスタの処理がエラーでない場合のみアプリケーションコンテナの取り込み
		if( !appErrFlg ){
			try {
				//アプリケーションコンテナの取り込み
				GetMasterInfo gm_app_cont= null;
				List<HashMap<String, Object>> PAAppContList = null;
				 // リトライ回数
				retryCnt = 0;
				// リトライ回数分繰り返す。
				while (true) {
					retryCnt++;
					try {
						gm_app_cont = new GetMasterInfo((HashMap)SystemConstantList.get(0));
						PAAppContList = gm_app_cont.getPAMaster(APPLICATION_CONTAINER);
						if(PAAppContList == null) {
							throw new Exception();
						}
						break;
					} catch (Exception e) {
						// コマンド実行エラー
						if (retryCnt >= pa_retry_cnt){
							log.fatal(LogUtil.getErrorMSG("FK160027"),e);
							throw new Exception();
						}
						log.error(LogUtil.getErrorMSG("FK160027") + " リトライ:" + retryCnt,e);
						Thread.sleep(pa_retry_wait);
					}
				}

				// cusconDBのアプリケーションコンテナマスタを取得する
				SelectApplicationContainerDao sacdao = (SelectApplicationContainerDao)context.getBean(daoName_SelAplCont);
				List<HashMap<String, Object>> CusAppContList = sacdao.select();
				if(CusAppContList == null) {
					log.fatal(LogUtil.getErrorMSG("FK160029"));
					//System.exit(0);
					throw new Exception();
				}

				// PAから取得したサービスのデータを、cuscondbに登録済みと未登録に分ける
				List PAAppContList_UPD = new ArrayList();
				for(int k=0; k < PAAppContList.size(); ++k) {
					log.debug("PAアプリケーションコンテナ名="+(String)PAAppContList.get(k).get(mapKey_Name));

					for(int i=0; i < CusAppContList.size(); ++i) {
						log.debug("cusconアプリケーションコンテナ名="+(String)CusAppContList.get(i).get(mapKey_Name));

						// サービス名が同一の場合
						if(((String)CusAppContList.get(i).get(mapKey_Name)).equals((String)PAAppContList.get(k).get(mapKey_Name)) ){
							log.debug("アプリケーションコンテナ名一致");

							// PAのUpdate用サービスリストに追加
							PAAppContList_UPD.add(PAAppContList.get(k));

							// PAのサービスリスト(=Insert用)から除去
							PAAppContList.remove(k);
							k = k -1;

							break;
						}
					}
				}

				// 未登録サービスの挿入
				InsertApplicationContainerDao iacdao = (InsertApplicationContainerDao)context.getBean(daoName_InsAplCont);
				try{
					iacdao.insert(PAAppContList);
				} catch (DataAccessException e){
					log.fatal(LogUtil.getErrorMSG("FK160030"));
					//System.exit(0);
					throw new Exception();
				}
			} catch (Exception e) {
				log.debug("アプリケーションコンテナ処理でエラー");
				appErrFlg = true;
			}
		}
// 20130901 kkato@PROSITE add end
// 20130901 kkato@PROSITE mod start　エラー時も処理継続するように修正
		// PAより、サービスマスタを取得する
		try{
			// Predefined Service
/*
			GetMasterInfo gm_srv= null;
			try {
				gm_srv = new GetMasterInfo((HashMap)SystemConstantList.get(0));
			} catch (Exception e1) {
				log.fatal(LogUtil.getErrorMSG("FK160008"));
				System.exit(0);
			}
			List<HashMap<String, Object>> PAServiceList = gm_srv.getPAMaster(SERVICE);
			if(PAServiceList == null) {
				log.fatal(LogUtil.getErrorMSG("FK160013"));
				System.exit(0);
			}

			// Shared Service
			GetMasterInfo gm_srv_srd= null;
			try {
				gm_srv_srd = new GetMasterInfo((HashMap)SystemConstantList.get(0));
			} catch (Exception e1) {
				log.fatal(LogUtil.getErrorMSG("FK160008"));
				System.exit(0);
			}
			List<HashMap<String, Object>> PAServiceListShared = gm_srv_srd.getPAMaster(SERVICE_SHARED);
			if(PAServiceListShared == null) {
				log.fatal(LogUtil.getErrorMSG("FK160023"));
				System.exit(0);
			}
*/
			GetMasterInfo gm_srv= null;
			List<HashMap<String, Object>> PAServiceList = null;
			 // リトライ回数
			retryCnt = 0;
			// リトライ回数分繰り返す。
			while (true) {
				retryCnt++;
				try {
					gm_srv = new GetMasterInfo((HashMap)SystemConstantList.get(0));
					PAServiceList = gm_srv.getPAMaster(SERVICE);
					if(PAServiceList == null) {
						throw new Exception();
					}
					break;
				} catch (Exception e) {
					// コマンド実行エラー
					if (retryCnt >= pa_retry_cnt){
						log.fatal(LogUtil.getErrorMSG("FK160013"),e);
						throw new Exception();
					}
					log.error(LogUtil.getErrorMSG("FK160013") + " リトライ:" + retryCnt,e);
					Thread.sleep(pa_retry_wait);
				}
			}
			// Shared Service
			GetMasterInfo gm_srv_srd= null;
			List<HashMap<String, Object>> PAServiceListShared = null;
			 // リトライ回数
			retryCnt = 0;
			// リトライ回数分繰り返す。
			while (true) {
				retryCnt++;
				try {
					gm_srv_srd = new GetMasterInfo((HashMap)SystemConstantList.get(0));
					PAServiceListShared = gm_srv_srd.getPAMaster(SERVICE_SHARED);
					if(PAServiceListShared == null) {
						throw new Exception();
					}
					break;
				} catch (Exception e) {
					// コマンド実行エラー
					if (retryCnt >= pa_retry_cnt){
						log.fatal(LogUtil.getErrorMSG("FK160013"),e);
						throw new Exception();
					}
					log.error(LogUtil.getErrorMSG("FK160013") + " リトライ:" + retryCnt,e);
					Thread.sleep(pa_retry_wait);
				}
			}
// 20130901 kkato@PROSITE mod end
			// 両サービスを連結
			PAServiceList.addAll(PAServiceListShared);

			// PAのサービスマスタにはないアプリは、cusconDBより削除する
			// ただし、「サービスグループリンクTBL」と「サービスリンクTBL」に参照されているものは除く
			DeleteServicesDao sddao = (DeleteServicesDao)context.getBean(daoName_DelSrv);
			try{
				sddao.delete(PAServiceList);
			} catch (DataAccessException e){
				log.fatal(LogUtil.getErrorMSG("FK160017"));
				//System.exit(0);
				throw new Exception();
			}


			// cusconDBのサービスマスタを取得する
			SelectServicesDao sdao = (SelectServicesDao)context.getBean(daoName_SelSrv);
			List<HashMap<String, Object>> CusServiceList = sdao.select();
// 2011/07/15 rep start inoue@prosite( 潜在バグと思われる )
//			if(CusAplList == null) {
			if(CusServiceList == null) {
// 2011/07/15 rep end inoue@prosite
				log.fatal(LogUtil.getErrorMSG("FK160014"));
				//System.exit(0);
				throw new Exception();
			}

			// PAから取得したサービスのデータを、cuscondbに登録済みと未登録に分ける
			List PAServiceList_UPD = new ArrayList();
			for(int k=0; k < PAServiceList.size(); ++k) {
				log.debug("PAサービス名="+(String)PAServiceList.get(k).get(mapKey_Name));

				for(int i=0; i < CusServiceList.size(); ++i) {
					log.debug("cusconサービス名="+(String)CusServiceList.get(i).get(mapKey_Name));

					// サービス名が同一の場合
					if(((String)CusServiceList.get(i).get(mapKey_Name)).equals((String)PAServiceList.get(k).get(mapKey_Name)) ){
						log.debug("サービス名一致");

						// PAのUpdate用サービスリストに追加
						PAServiceList_UPD.add(PAServiceList.get(k));

						// PAのサービスリスト(=Insert用)から除去
						PAServiceList.remove(k);
						k = k -1;

						break;
					}
				}
			}

			// 登録済みサービスの更新
			UpdateServicesDao sudao = (UpdateServicesDao)context.getBean(daoName_UpdSrv);
			try{
				sudao.update(PAServiceList_UPD);
			} catch (DataAccessException e){
				log.fatal(LogUtil.getErrorMSG("FK160015"));
				//System.exit(0);
				throw new Exception();
			}

			// 未登録サービスの挿入
			InsertServicesDao sidao = (InsertServicesDao)context.getBean(daoName_InsSrv);
			try{
				sidao.insert(PAServiceList);
			} catch (DataAccessException e){
				log.fatal(LogUtil.getErrorMSG("FK160016"));
				//System.exit(0);
				throw new Exception();
			}
		} catch (Exception e) {
			log.debug("サービスマスタ処理でエラー");
		}
//------------------------------------------------------------------------------------
// 2011/05/19 inoue@prosite add start - Webフィルタカテゴリの取り込み
	  	// urlcategory.properties設定ファイルよりcategoryリストの開始、終了を区別する文字列を取得
		try {
			Properties prop = new Properties();
			try {
				InputStream inStream = MasterInfoChikuseki.class.getClassLoader().getResourceAsStream("urlcategory.properties");
				 prop.load(inStream);
			} catch (Exception e) {
				log.fatal(LogUtil.getErrorMSG("FK170021"),e);
				//System.exit(0);
				throw new Exception();
			}
			String cmd_vsys = prop.getProperty("category_cmd_vsys");
			String cmd_urlprofile = prop.getProperty("category_cmd_urlprofile");
			String urlctgrst = prop.getProperty("category_start");
			String urlctgred = prop.getProperty("category_end");

			if(urlctgrst==null || urlctgred==null){
				log.fatal(LogUtil.getErrorMSG("FK170022"));
				//System.exit(0);
				throw new Exception();
			}
// 20130901 kkato@PROSITE mod start　エラー時も処理継続するように修正
			// PAより、Webフィルタカテゴリーマスタを取得する
/*
			GetMasterInfo gm_urlcategories = null;
			try {
				gm_urlcategories = new GetMasterInfo((HashMap)SystemConstantList.get(0));
			} catch (Exception e1) {
				log.fatal(LogUtil.getErrorMSG("FK160026"));
				System.exit(0);
			}

			List<HashMap<String, Object>> PAUrlFilterCategoriesList = gm_urlcategories.getPAMaster_UrlCategory(cmd_vsys, cmd_urlprofile, urlctgrst, urlctgred);
			if(PAUrlFilterCategoriesList == null) {
				log.fatal(LogUtil.getErrorMSG("FK160026"));
				System.exit(0);
			}
*/
			GetMasterInfo gm_urlcategories = null;
			List<HashMap<String, Object>> PAUrlFilterCategoriesList = null;
			 // リトライ回数
			retryCnt = 0;
			// リトライ回数分繰り返す。
			while (true) {
				retryCnt++;
				try {
					gm_urlcategories = new GetMasterInfo((HashMap)SystemConstantList.get(0));
					PAUrlFilterCategoriesList = gm_urlcategories.getPAMaster_UrlCategory(cmd_vsys, cmd_urlprofile, urlctgrst, urlctgred);
					if(PAUrlFilterCategoriesList == null) {
						throw new Exception();
					}
					break;
				} catch (Exception e) {
					// コマンド実行エラー
					if (retryCnt >= pa_retry_cnt){
						log.fatal(LogUtil.getErrorMSG("FK160026"),e);
						throw new Exception();
					}
					log.error(LogUtil.getErrorMSG("FK160026") + " リトライ:" + retryCnt,e);
					Thread.sleep(pa_retry_wait);
				}
			}
// 20130901 kkato@PROSITE mod end
			// cusconDBのWebフィルタカテゴリーマスタを取得する
			SelectUrlFilterCategoryDao sucdao = (SelectUrlFilterCategoryDao)context.getBean(daoName_SelUrlCategory);
			List<HashMap<String, Object>> CusUrlFilterCategoryList = sucdao.select();
			if(CusUrlFilterCategoryList == null) {
				log.fatal(LogUtil.getErrorMSG("FK160024"));
				//System.exit(0);
				throw new Exception();
			}
			// PAから取得したWebフィルタカテゴリのデータを、cuscondbに登録済みと未登録に分ける
			List PAUrlFilterCategoriesList_UPD = new ArrayList();
			for(int k=0; k < PAUrlFilterCategoriesList.size(); ++k) {
				log.debug("PA Webフィルタカテゴリ名="+(String)PAUrlFilterCategoriesList.get(k).get(mapKey_Name));

				for(int i=0; i < CusUrlFilterCategoryList.size(); ++i) {
					log.debug("cuscon Webフィルタカテゴリ名="+(String)CusUrlFilterCategoryList.get(i).get(mapKey_Name));

					// 名称が同一の場合
					if(((String)CusUrlFilterCategoryList.get(i).get(mapKey_Name)).equals((String)PAUrlFilterCategoriesList.get(k).get(mapKey_Name)) ){
						log.debug("名称一致");

						// PAのUpdate用サービスリストに追加
						PAUrlFilterCategoriesList_UPD.add(PAUrlFilterCategoriesList.get(k));

						// PAのサービスリスト(=Insert用)から除去
						PAUrlFilterCategoriesList.remove(k);
						k = k -1;

						break;
					}
				}
			}

			// 未登録Webフィルタカテゴリの挿入
			InsertUrlFilterCategoryDao iucdao = (InsertUrlFilterCategoryDao)context.getBean(daoName_InsUrlCategory);
			try{
				iucdao.insert(PAUrlFilterCategoriesList);
			} catch (DataAccessException e){
				log.fatal(LogUtil.getErrorMSG("FK160025"));
				//System.exit(0);
				throw new Exception();
			}
		} catch (Exception e) {
			log.debug("Webフィルタカテゴリ処理でエラー");
		}
// 2011/05/19 inoue@prosite add end - Webフィルタカテゴリの取り込み
//------------------------------------------------------------------------------------
// 20130901 kkato@PROSITE del start
//アプリケーションコンテナの取り込み処理は上へ移動
/*
// 2011/10/31 inoue@prosite add start - アプリケーションコンテナの取り込み
		GetMasterInfo gm_app_cont= null;
		try {
			gm_app_cont = new GetMasterInfo((HashMap)SystemConstantList.get(0));
		} catch (Exception e1) {
			log.fatal(LogUtil.getErrorMSG("FK160027"));
			System.exit(0);
		}
		List<HashMap<String, Object>> PAAppContList = gm_app_cont.getPAMaster(APPLICATION_CONTAINER);
		if(PAAppContList == null) {
			log.fatal(LogUtil.getErrorMSG("FK160027"));
			System.exit(0);
		}
*/
/*↓DEL:2012/02/16 T.Suzuki(NOP) アプリケーションコンテナマスタの削除処理を排除 */
//		// PAのアプリケーションコンテナマスタにはないものは、cusconDBより削除する
//		DeleteApplicationContainerDao dacdao = (DeleteApplicationContainerDao)context.getBean(daoName_DelAplCont);
//		try{
//			dacdao.delete(PAAppContList);
//		} catch (DataAccessException e){
//			log.fatal(LogUtil.getErrorMSG("FK160028"));
//			System.exit(0);
//		}
/*↑DEL:2012/02/16 T.Suzuki(NOP) アプリケーションマスタの削除処理を排除 */
/*
		// cusconDBのアプリケーションコンテナマスタを取得する
		SelectApplicationContainerDao sacdao = (SelectApplicationContainerDao)context.getBean(daoName_SelAplCont);
		List<HashMap<String, Object>> CusAppContList = sacdao.select();
		if(CusAppContList == null) {
			log.fatal(LogUtil.getErrorMSG("FK160029"));
			System.exit(0);
		}

		// PAから取得したサービスのデータを、cuscondbに登録済みと未登録に分ける
		List PAAppContList_UPD = new ArrayList();
		for(int k=0; k < PAAppContList.size(); ++k) {
			log.debug("PAアプリケーションコンテナ名="+(String)PAAppContList.get(k).get(mapKey_Name));

			for(int i=0; i < CusAppContList.size(); ++i) {
				log.debug("cusconアプリケーションコンテナ名="+(String)CusAppContList.get(i).get(mapKey_Name));

				// サービス名が同一の場合
				if(((String)CusAppContList.get(i).get(mapKey_Name)).equals((String)PAAppContList.get(k).get(mapKey_Name)) ){
					log.debug("アプリケーションコンテナ名一致");

					// PAのUpdate用サービスリストに追加
					PAAppContList_UPD.add(PAAppContList.get(k));

					// PAのサービスリスト(=Insert用)から除去
					PAAppContList.remove(k);
					k = k -1;

					break;
				}
			}
		}

		// 未登録サービスの挿入
		InsertApplicationContainerDao iacdao = (InsertApplicationContainerDao)context.getBean(daoName_InsAplCont);
		try{
			iacdao.insert(PAAppContList);
		} catch (DataAccessException e){
			log.fatal(LogUtil.getErrorMSG("FK160030"));
			System.exit(0);
		}
// 2011/10/31 inoue@prosite add end - アプリケーションコンテナの取り込み
*/
// 20130901 kkato@PROSITE del end
		log.info( LogUtil.getClassMethod() + " 終了\n");
		System.exit(0);
	}
}
