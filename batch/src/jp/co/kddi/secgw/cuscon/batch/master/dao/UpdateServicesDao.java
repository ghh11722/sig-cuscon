/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateServicesDao.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     matsshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.master.dao;

import java.sql.Types;
import java.util.*;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.BatchSqlUpdate;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


/**
 * クラス名：UpdateServicesDao<br>
 * 機能概要：サービスマスタの更新を行う。<br>
 * 備考：<br>
 * @author matsshtt
 * @version 1.0 matsshtt<br>
 *             Created 2010/03/01<br>
 *             新規作成
 *
 */
public class UpdateServicesDao {
	// ロガー
    Logger log = LogUtil.getCallerLogger();
    /** データソース */
    private DriverManagerDataSource dataSource;

    private static final String mapKey_name = "name";
    private static final String mapKey_protocol = "protocol";
    private static final String mapKey_port = "port";

    private static final String sql = "update m_services set protocol=?, port=? where name=?";

    /**
     * メソッド名：M_Servicesテーブルのデータ更新<br>
     * 機能概要：M_Servicesテーブルのデータを更新する<br>
     *
     * @param List<HashMap<String, Object>> PAServiceList_UPD 更新するデータ
     */
    public void update(List<HashMap<String, Object>> PAServiceList_UPD) throws DataAccessException{
	    log.debug( LogUtil.getClassMethod() + " 開始");

    	BatchSqlUpdate batchUpdater = new BatchSqlUpdate(this.dataSource, sql);
        batchUpdater.declareParameter(new SqlParameter(Types.VARCHAR));
        batchUpdater.declareParameter(new SqlParameter(Types.VARCHAR));
        batchUpdater.declareParameter(new SqlParameter(Types.VARCHAR));
        batchUpdater.compile();

  		// SQL実行
        try{
        	for(int i=0;i < PAServiceList_UPD.size(); ++i){
        		Map tmpMap = PAServiceList_UPD.get(i);
        		log.debug("M_Application更新 name="+tmpMap.get(mapKey_name));
        		Object param[] = {tmpMap.get(mapKey_protocol),
        				tmpMap.get(mapKey_port),
        				tmpMap.get(mapKey_name)};
        		batchUpdater.update(param);
        	}

            log.debug("M_Services更新 batchUpdater.flush");
        	batchUpdater.flush();

        } catch (DataAccessException e) {
        	log.fatal(LogUtil.getErrorMSG("FK160001"),e);
        	throw e;
        }

	    log.debug( LogUtil.getClassMethod() + " 終了");
    }

    /**
     * メソッド名：データソース設定<br>
     * 機能概要：データソースをセットする<br>
     * @param ds データソース
     */
    public void setdataSource(DriverManagerDataSource ds) {
	    log.debug( LogUtil.getClassMethod() + " 開始");
        this.dataSource = ds;
	    log.debug( LogUtil.getClassMethod() + " 終了");    }

}