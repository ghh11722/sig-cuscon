/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコンバッチ
 * ファイル名	: SelectSystemConstantDao.java
 *
 * [変更履歴]
 * 日付 		  更新者				  内容
 * 2010/03/01	  matsshtt				  初版作成
 * 2010/08/17	  T.Suzuki(NOP) 		  PAコマンドWait値対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.master.dao;

//import jp.co.denso.its.cnvalink.util.Log;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;


/**
 * クラス名：SelectSystemConstantDao<br>
 * 機能概要：システム定数マスタ検索クラスです。<br>
 * 備考：<br>
 * @author matsshtt
 * @version 1.0 matsshtt<br>
 *			   Created 2010/03/01<br>
 *			   新規作成
 * @version 1.1 M.Tsunashima@PLUMSYSTEMS
 *          Modified 2011/11/14
 *          Terasolunaバージョンアップ対応
 */
public class SelectSystemConstantDao {
	// ロガー
	Logger log = LogUtil.getCallerLogger();

	/** データソース */
	private DriverManagerDataSource dataSource;

	private static final String sqlParam_paAdrs = "pa_cli_address";
	private static final String sqlParam_paId = "pa_admin_login_id";
	private static final String sqlParam_paPwd= "pa_admin_login_pwd";
	private static final String sqlParam_paTimeout = "pa_cmd_timeout";
	private static final String sqlParam_paCmdWait = "pa_cmd_wait";

	/**
	 * メソッド名：select<br>
	 * 機能概要：select文を実行する<br>
	 */
	public List select() {
		log.debug( LogUtil.getClassMethod() + " 開始");

		final String sql = "select PA_CLI_ADDRESS,PA_ADMIN_LOGIN_ID,pgp_sym_decrypt(PA_ADMIN_LOGIN_PWD::bytea, 'kddi-secgw'::text) as PA_ADMIN_LOGIN_PWD,PA_CMD_TIMEOUT,PA_CMD_WAIT from m_systemconstant;";

		//2016/11/14 M.Tsunashima@PLUMSYSTEMS modified start
		//List<HashMap> results = null;
		List<Map<String,Object>> results = null;
		//2016/11/14 M.Tsunashima@PLUMSYSTEMS modified end
		try{
			JdbcTemplate jt = new JdbcTemplate(this.dataSource);
			results = jt.queryForList(sql);
		} catch (DataAccessException e) {
		 	log.fatal(LogUtil.getErrorMSG("FK160001"),e);
			return null;
		}

		// データチェック
		if(results.size() == 0 ||
	   		!results.get(0).containsKey(sqlParam_paAdrs) ||
	   		!results.get(0).containsKey(sqlParam_paId) ||
	   		!results.get(0).containsKey(sqlParam_paPwd) ||
			!results.get(0).containsKey(sqlParam_paTimeout) ||
			!results.get(0).containsKey(sqlParam_paCmdWait)) {
			log.fatal(LogUtil.getErrorMSG("FK160002"));
			return null;
		}

		log.debug( LogUtil.getClassMethod() + " 終了");
		return results;
	}

	/**
	 * メソッド名：データソース設定<br>
	 * 機能概要：データソースをセットする<br>
	 * @param ds データソース
	 */
	public void setdataSource(DriverManagerDataSource ds) {
		this.dataSource = ds;
	}


}

