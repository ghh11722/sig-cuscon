/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコンバッチ
 * ファイル名   : ReportInfoChikusekii.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/05     matsshtt                初版作成
 * 2012/12/01     kkato@PROSITE           PANOS4.1.x対応
 * 2013/09/01     kkato@PROSITE           エラー発生後に次データの処理を続行するように変更
 * 2016/12/10	  T.Yamazaki@Plum Systems PANOS7対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.report;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;

import jp.co.kddi.secgw.cuscon.batch.commons.CusconXMLAnalyze;
import jp.co.kddi.secgw.cuscon.batch.commons.CusconXmlApiExecute;
import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;
import jp.co.kddi.secgw.cuscon.batch.report.dao.InsertReportData;
import jp.co.kddi.secgw.cuscon.batch.report.dao.SelectManageCDao;
import jp.co.kddi.secgw.cuscon.batch.report.dao.SelectSystemConstantDao;

/**
 * クラス名 : ReportInfoChikusekii
 * 機能概要 : レポート情報をPAより取得し、cusconDBに保存する
 * 備考 :
 * @author matsshtt
 * @version 1.0 matsshtt
 *          Created 2010/03/05
 *          新規作成
 *			2.0 T.Yamazaki@Plum Systems Inc.
 *			Updated 2016/12/11
 *			PANOS7対応
 * @see
 */

public class ReportInfoChikuseki {

	public static final MessageFormat accessKeyUrlForm = new MessageFormat("?type=keygen&user={0}&password={1}");
	// 20121201 kkato@PROSITE mod start
	////public static final MessageFormat reportDataUrlForm = new MessageFormat("?type=report&reporttype={0}&reportname={1}&period={2}&topn={3}&key={4}");
	//public static final MessageFormat reportDataUrlForm = new MessageFormat("?type=report&reporttype={0}&reportname={1}&period={2}&topn={3}&key={4}&vsys={5}");
	// 20121201 kkato@PROSITE mod end

	// UPDATED: 2016.11 by Plum Systems Inc.
	// PANOS7対応（asyncパラメータ追加） -->
	public static final MessageFormat reportDataUrlForm = new MessageFormat("?type=report&async=no&reporttype={0}&reportname={1}&period={2}&topn={3}&key={4}&vsys={5}");
	// -->PANOS7対応（asyncパラメータ追加）

	public static final String dbClmn_url = "pa_xmlapi_url";
	public static final String dbClmn_login = "pa_login_id";
	public static final String dbClmn_pwd = "login_init_pwd";
	public static final String dbClmn_vsys = "vsys_id";
	public static final String xmlTag_key = "key";
	public static final String daoName_SelManage = "SelectManageC";
	public static final String daoName_SelSysCon = "SelectSystemConstant_Report";
	public static final String contxtFileName = "applicationContext.xml";
	public static final String propertyFileName = "report.properties";
	// 20121201 add start kkato@prosite
	public static final String daoName_SelSys = "SelectSystemConstant";
	// 20121201 add end kkato@prosite

	public static void main(String[] arg){
		// ロガー
	    Logger log = LogUtil.getCallerLogger();

      	log.info( LogUtil.getClassMethod() + " 開始");

      	// 設定ファイルを読み込み
      	Properties prop = new Properties();
    	try {
    		InputStream inStream = ReportInfoChikuseki.class.getClassLoader().getResourceAsStream(propertyFileName);
    		 prop.load(inStream);
    	} catch (Exception e) {
        	log.fatal(LogUtil.getErrorMSG("FK170016"),e);
        	return;
        }
		String reporttype = prop.getProperty("reporttype");
		String period = prop.getProperty("period");
		String topn = prop.getProperty("topn");
		String[] reptNameArry = (prop.getProperty("reportlist")).split(",");

		// 20121201 kkato@PROSITE mod start
		ApplicationContext context_sys = null;
		try {
			context_sys = new ClassPathXmlApplicationContext(contxtFileName);
		} catch (BeansException e){
			log.fatal(LogUtil.getErrorMSG("FK160022"),e);
			System.exit(0);
		}
		// DBより、PA(Palo Alto)へのログイン情報を取得する
		jp.co.kddi.secgw.cuscon.batch.master.dao.SelectSystemConstantDao ssdao =
			(jp.co.kddi.secgw.cuscon.batch.master.dao.SelectSystemConstantDao)context_sys.getBean(daoName_SelSys);
		List<HashMap> SystemConstantList = ssdao.select();
		if(SystemConstantList == null) {
			log.fatal(LogUtil.getErrorMSG("FK160003"));
			System.exit(0);
		}
		String adminuser = (String) SystemConstantList.get(0).get("pa_admin_login_id");
		String adminpassword = (String) SystemConstantList.get(0).get("pa_admin_login_pwd");
		try {
			adminuser = URLEncoder.encode(adminuser, "UTF-8");
			adminpassword = URLEncoder.encode(adminpassword, "UTF-8");
    	} catch (Exception e) {
			log.fatal(LogUtil.getErrorMSG("FK160003"),e);
			return;
		}
		// 20121201 kkato@PROSITE mod end

      	// PaloAltoへのuserとpasswordを取得（全Vsys分）
      	ApplicationContext context = null;
      	SelectManageCDao dao = null;
      	try{
      		context = new ClassPathXmlApplicationContext(contxtFileName);
      		dao = (SelectManageCDao)context.getBean(daoName_SelManage);
      	} catch (BeansException e){
        	log.fatal(LogUtil.getErrorMSG("FK170015"),e);
        	return;
      	}
        List<HashMap<String,Object>> loginIDs = dao.select();
        if(loginIDs == null || loginIDs.size() == 0) {
        	log.fatal(LogUtil.getErrorMSG("FK170001"));
        	return;
        }

        //PaloAltoへのURLを取得
        SelectSystemConstantDao scdao = (SelectSystemConstantDao)context.getBean(daoName_SelSysCon);
        List<HashMap<String,Object>>  SystemConstants = scdao.select();
        if(SystemConstants == null || SystemConstants.size() == 0 || !SystemConstants.get(0).containsKey(dbClmn_url)) {
        	log.fatal(LogUtil.getErrorMSG("FK170002"));
        	return;
        }
        String paloURL = (String)SystemConstants.get(0).get(dbClmn_url);

		// SSL用HHTTPクライアント
		CusconXmlApiExecute HttpsClient = new CusconXmlApiExecute();
		CusconXMLAnalyze xmlAnalyze = new CusconXMLAnalyze();

		// Report情報取得
		// vsys毎にループ
		for(int i=0; i < loginIDs.size(); ++i) {
			// 20130901 kkato@PROSITE add start
			try {
				// 20130901 kkato@PROSITE add end
				//PaloAltoのuserとpassword、及び対象のVsysIDを取り出す
				HashMap<String, Object> tmp = (HashMap)loginIDs.get(i);
				if(!tmp.containsKey(dbClmn_login) || !tmp.containsKey(dbClmn_pwd) || !tmp.containsKey(dbClmn_vsys)) {
					log.fatal(LogUtil.getErrorMSG("FK170003"));
					// 20130901 kkato@PROSITE mod start
					//return;
					continue;
					// 20130901 kkato@PROSITE mod end
				}

	        	log.debug("対象VSYS : "+tmp.get(dbClmn_vsys));

				String paloUser;
				try {
					paloUser = URLEncoder.encode(
							(String)tmp.get(dbClmn_login), "UTF-8");
				} catch (UnsupportedEncodingException e1) {
					log.fatal(LogUtil.getErrorMSG("FK170004"),e1);
					// 20130901 kkato@PROSITE mod start
					//return;
					continue;
					// 20130901 kkato@PROSITE mod end
				}
				String paloPass;
				try {
					paloPass = URLEncoder.encode(
							(String)tmp.get(dbClmn_pwd), "UTF-8");
				} catch (UnsupportedEncodingException e1) {
					log.fatal(LogUtil.getErrorMSG("FK170004"),e1);
					// 20130901 kkato@PROSITE mod start
					//return;
					continue;
					// 20130901 kkato@PROSITE mod end
				}
				String vsysID = (String)tmp.get(dbClmn_vsys);

		      	log.info(LogUtil.getErrorMSG("IK170012") + vsysID);

				// PaloAltoにアクセスキー生成をリクエスト
		    	// 20121201 kkato@PROSITE mod start
				//String accessKeyUrl = paloURL + accessKeyUrlForm.format( new Object[]{paloUser,paloPass});
		      	// 20130901 kkato@PROSITE mod start
				String accessKeyUrl = paloURL + accessKeyUrlForm.format( new Object[]{adminuser,adminpassword});
		      	// 20130901 kkato@PROSITE mod end

				log.debug(accessKeyUrl);

		    	// 20121201 kkato@PROSITE mod end
				InputStream inptStrmKey = null;
				try{
					inptStrmKey = HttpsClient.doProc(accessKeyUrl);
				} catch (Exception e) {
					log.fatal(LogUtil.getErrorMSG("FK170004"),e);
					// 20130901 kkato@PROSITE mod start
					//return;
					continue;
					// 20130901 kkato@PROSITE mod end
				}

				// レスポンスXMLを解析
				List<HashMap<String, Object>> listMapKey = null;
				try{
					listMapKey = xmlAnalyze.analyzeXML(inptStrmKey);
				} catch (Exception e) {
					log.fatal(LogUtil.getErrorMSG("FK170005")+inptStrmKey.toString(),e);
					// 20130901 kkato@PROSITE mod start
					//return;
					continue;
					// 20130901 kkato@PROSITE mod end
				}

				// アクセスキーを抽出
				if(listMapKey == null || listMapKey.size() == 0 || !listMapKey.get(0).containsKey(xmlTag_key)){
		        	log.fatal(LogUtil.getErrorMSG("FK170006")+paloUser+":"+paloPass);
					// 20130901 kkato@PROSITE mod start
					//return;
					continue;
					// 20130901 kkato@PROSITE mod end
				}
				String accessKey =  (String)(listMapKey.get(0)).get(xmlTag_key);

				// レポート種別毎にLoop
				for(int j=0; j < reptNameArry.length; ++j) {

			      	log.info(LogUtil.getErrorMSG("IK170013")+ vsysID+":"+reptNameArry[j]);

					InputStream inptStrmRept = null;
					List<HashMap<String, Object>> listMapRept = null;

					// PaloAltoよりレポートデータ取得
					try {
						// 20121201 kkato@PROSITE mod start
						//String reportDataUrl = paloURL
						//+ reportDataUrlForm.format( new Object[]{reporttype,reptNameArry[j],period,topn,accessKey});
						String reportDataUrl = paloURL + reportDataUrlForm.format(
								new Object[]{reporttype,reptNameArry[j],period,topn,accessKey,vsysID});
						// 20121201 kkato@PROSITE mod end

						log.debug(reportDataUrl);

						inptStrmRept = HttpsClient.doProc(reportDataUrl);
					} catch (Exception e) {
						log.fatal(LogUtil.getErrorMSG("FK170007"),e);
						// 20130901 kkato@PROSITE mod start
						//return;
						continue;
						// 20130901 kkato@PROSITE mod end

					}

					// レスポンスXMLを解析
					try {
						listMapRept = xmlAnalyze.analyzeXML(inptStrmRept);
					} catch (Exception e) {
						log.fatal(LogUtil.getErrorMSG("FK170008"),e);
						// 20130901 kkato@PROSITE mod start
						//return;
						continue;
						// 20130901 kkato@PROSITE mod end
					}
					if(listMapRept == null){
						// 20130901 kkato@PROSITE mod start
						log.fatal(LogUtil.getErrorMSG("FK170007"));
						// 20130901 kkato@PROSITE mod end
			        	log.debug("レスポンスXMLの解析に失敗");
						continue;
					}

					// DBにレポートデータを保存
					try {
						InsertReportData daoReprt = new InsertReportData();
						daoReprt.execute( listMapRept, vsysID);
					} catch (DataAccessException e) {
			        	log.debug("レポートデータ保存の際に、DataAccessExceptionが発生");
						// 20130901 kkato@PROSITE mod start
						log.fatal(LogUtil.getErrorMSG("FK170009"),e);
						//return;
						continue;
						// 20130901 kkato@PROSITE mod end
						// 20130901 kkato@PROSITE add start
					} catch (Exception e) {
						log.fatal(LogUtil.getErrorMSG("FK170009"),e);
						continue;
						// 20130901 kkato@PROSITE add end
					}


				}
			// 20130901 kkato@PROSITE add start
			} catch (Exception e) {
				log.fatal(LogUtil.getErrorMSG("FK170001"),e);
				continue;
			}
			// 20130901 kkato@PROSITE add end
		}

      	log.info( LogUtil.getClassMethod() + " 終了");
      	return;
	}

}
