/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコンバッチ
 * ファイル名   : SelectServicesDao.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/05/19     inoue@prosite           初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.master.dao;

import java.util.*;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


/**
 * クラス名：SelectUrlFilterCategoryDao<br>
 * 機能概要：カテゴリマスタ検索クラスです。<br>
 * 備考：<br>
 * @author inoue@prosite
 * @version 1.0 inoue@prosite<br>
 *             Created 2011/05/19<br>
 *             新規作成
 */
public class SelectUrlFilterCategoryDao {
	// ロガー
    Logger log = LogUtil.getCallerLogger();
    /** データソース */
    private DriverManagerDataSource dataSource;

    /**
     * メソッド名：select<br>
     * 機能概要：select文を実行する<br>
     */
    public List select() {
	    log.debug( LogUtil.getClassMethod() + " 開始");

	    final String sql = "select name from m_urlfilter_category;";
	    List results = null;
	    try{
	    	JdbcTemplate jt = new JdbcTemplate(this.dataSource);
	    	results = jt.queryForList(sql);
	    } catch (DataAccessException e) {
         	log.fatal(LogUtil.getErrorMSG("FK160001"),e);
	    	return null;
	    }

	    log.debug( LogUtil.getClassMethod() + " 終了");

        return results;
    }


    /**
     * メソッド名：データソース設定<br>
     * 機能概要：データソースをセットする<br>
     * @param ds データソース
     */
    public void setdataSource(DriverManagerDataSource ds) {
        this.dataSource = ds;
    }


}

