/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコンバッチ マスター情報蓄積
 * ファイル名   : MentAplRelatedTable.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/05     matsshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.master.bussiness;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;
import jp.co.kddi.secgw.cuscon.batch.master.dao.*;

/**
 * クラス名 : MentAplRelatedTable
 * 機能概要 : カテゴリ、サブカテゴリ、テクノロジーの３データについて、PAより取得したデータをもとにcusconDBを更新する
 * 備考 :
 * @author matsshtt
 * @version 1.0 matsshtt
 *          Created 2010/03/05
 *          新規作成
 * @see
 */
public class UpdateAplRelatedTbl {

	public static final String contxtFileName = "applicationContext.xml";
	private static final String daoName_DelCate = "DeleteCategory";
	private static final String daoName_DelSubC = "DeleteSubCategory";
	private static final String daoName_DelTech = "DeleteTechnology";

	private ApplicationContext context;

	// ロガー
    Logger log = LogUtil.getCallerLogger();

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 * @param
	 */
	public UpdateAplRelatedTbl() {
		this.context = new ClassPathXmlApplicationContext(contxtFileName);
	}

	public void delete() throws Exception{
	    log.info( LogUtil.getClassMethod() + " 開始");

		DeleteCategoryDao cDao = (DeleteCategoryDao)this.context.getBean(daoName_DelCate);
		DeleteSubCategoryDao scDao = (DeleteSubCategoryDao)this.context.getBean(daoName_DelSubC);
		DeleteTechnologyDao tDao = (DeleteTechnologyDao)this.context.getBean(daoName_DelTech);

		try {
			// 使用されていないカテゴリを、カテゴリTBLより削除する
			cDao.delete();

			// 使用されていないサブカテゴリを、サブカテゴリTBLより削除する
			scDao.delete();

			// 使用されていないテクノロジーを、テクノロジーTBLより削除する
			tDao.delete();

		} catch (Exception e) {
			throw e;
		}

	    log.info( LogUtil.getClassMethod() + " 終了");
	}

}
