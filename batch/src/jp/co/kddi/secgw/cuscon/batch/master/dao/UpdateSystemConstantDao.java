/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateSystemConstantDao.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2013/09/01     kkato@prosite			  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.master.dao;

import java.sql.Types;
import java.util.*;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.BatchSqlUpdate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * クラス名：UpdateSystemConstantDao<br>
 * 機能概要：M_SYSTEMCONSTANTの更新を行う。<br>
 * 備考：<br>
 *
 * @author kkato
 * @version 1.0 kkato@prosite<br>
 *          Created 2013/09/01<br>
 *          新規作成
 *
 */
public class UpdateSystemConstantDao {
	// ロガー
	Logger log = LogUtil.getCallerLogger();
	/** データソース */
	private DriverManagerDataSource dataSource;

	private static final String mapKey_UrlFiltering = "url-filtering-version";
	private static final String sql = "update M_SYSTEMCONSTANT set URL_DB_VERSION=?";

	/**
	 * メソッド名：m_systemconstantテーブルのデータ更新<br>
	 * 機能概要：m_systemconstantテーブルのデータを更新する<br>
	 *
	 * @param List
	 *            <HashMap<String, Object>> PAServiceList_UPD 更新するデータ
	 */
	public void update(HashMap<String, String> PASysteminfo_UPD)
			throws DataAccessException {
		log.debug(LogUtil.getClassMethod() + " 開始");

		BatchSqlUpdate batchUpdater = new BatchSqlUpdate(this.dataSource, sql);
		batchUpdater.declareParameter(new SqlParameter(Types.VARCHAR));
		batchUpdater.compile();

		// SQL実行
		try {
			log.debug("M_SYSTEMCONSTANT更新 name=" + PASysteminfo_UPD.get(mapKey_UrlFiltering));
			Object param[] = { PASysteminfo_UPD.get(mapKey_UrlFiltering) };
			batchUpdater.update(param);

			log.debug("M_SYSTEMCONSTANT更新 batchUpdater.flush");
			batchUpdater.flush();

		} catch (DataAccessException e) {
			log.fatal(LogUtil.getErrorMSG("FK160001"), e);
			throw e;
		}

		log.debug(LogUtil.getClassMethod() + " 終了");
	}

	/**
	 * メソッド名：データソース設定<br>
	 * 機能概要：データソースをセットする<br>
	 *
	 * @param ds
	 *            データソース
	 */
	public void setdataSource(DriverManagerDataSource ds) {
		log.debug(LogUtil.getClassMethod() + " 開始");
		this.dataSource = ds;
		log.debug(LogUtil.getClassMethod() + " 終了");
	}

}