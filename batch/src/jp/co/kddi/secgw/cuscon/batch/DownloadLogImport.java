/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : LogImport.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2011/06/10  inoue@prosite 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.File;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.*;

import java.sql.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

/**
 * クラス名 : DownloadLogImport
 * 機能概要 : ダウンロードログ登録バッチ処理メインクラス
 * 備考 :
 * @author inoue@prosite
 * @version 1.0 inoue@prosite
 *          Created 2011/06/10
 *          新規作成
 */
public class DownloadLogImport {

	// ログクラス
	private static Log log = LogFactory.getLog(DownloadLogImport.class);

	// メッセージプロパティ
	private static Properties msg = new Properties();

	// 動作設定取得用SQL
	private static final String SELECT_CONSTANT_SQL = "SELECT logsvr_ftp_addr,logsvr_ftp_login_id,pgp_sym_decrypt(logsvr_ftp_login_pwd::bytea, 'kddi-secgw'::text) as logsvr_ftp_login_pwd,logsvr_ftp_login_path,work_dir,download_logsvr_ftp_login_path, download_log_dir FROM m_systemconstant;";
	// 動作設定取得用フィールド名
	private static final String COLUMN_FTP_ADDR = "logsvr_ftp_addr";
	private static final String COLUMN_FTP_ID = "logsvr_ftp_login_id";
	private static final String COLUMN_FTP_PWD = "logsvr_ftp_login_pwd";
	private static final String COLUMN_DOWNLOAD_REMOTE_DIR = "download_logsvr_ftp_login_path";
	private static final String COLUMN_DOWNLOAD_LOCAL_DIR = "download_log_dir";
	// ログファイル名パターン
	private static final String DOWNLOAD_LOG_FILE_PATTERN = "^vsys\\d{1,3}_[0-9]{8}_.{3}.zip$";

	// ローカルディレクトリ名

	/**
	 * メソッド名 : メイン関数
	 * 機能概要 : ダウンロード用ログダウンロード処理
	 * @param args 起動パラメータ
	 */
	public static void main(String args[]) {



		String driverClassName = null; // JDBCドライバクラス名
		String url = null;             // DB接続URL
		String username = null;        // DB接続ユーザ名
		String password = null;        // DB接続パスワード
		int cnt = 0;                   // ループカウンタ
		String serverAddr = "";        // FTPサーバアドレス
		String login = "";             // FTPサーバアカウント
		String pass = "";              // FTPサーバパスワード
		String download_remoteDir = "";         // ダウンロードログ取得先ディレクトリ（サーバ側）
		String download_localBaseDir = "";      // ダウンロードログ配置先ベースディレクトリ（ローカル側）
		FtpCtr ftpObj = null;          // FTPオブジェクト

		// パラメータチェック
		if ((args == null) || (args.length != 2)) {
			//log.error("パラメータエラー");
			System.out.println("パラメータエラー：パラメータが正しく指定されていません。");
			System.exit(1); // 異常終了
		}

		// メッセージのプロパティファイルをロード
		try {
			msg.load(new FileInputStream(args[1]));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			System.out.println("パラメータエラー：メッセージプロパティファイルがオープンできません。");
			System.exit(1); // 異常終了
		} catch (IOException e1) {
			e1.printStackTrace();
			System.out.println("パラメータエラー：メッセージプロパティファイルがオープンできません。");
			System.exit(1); // 異常終了
		}

		System.out.println(msg.getProperty("DK150022"));
		log.info(msg.getProperty("IK150022"));

		// DB接続情報読み込み
		try {
			// XMLパース
			DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = dbfactory.newDocumentBuilder();
			Document doc = builder.parse(new File(args[0]));
			Element root = doc.getDocumentElement();
			NodeList node = root.getElementsByTagName("Resource");
			if (node.getLength() < 1) {
				log.fatal(msg.getProperty("FK151001"));
				System.out.println(msg.getProperty("DK150001"));
				System.exit(1); // 異常終了
			}

			// DB接続情報の取得
			NamedNodeMap nodeMap = node.item(0).getAttributes();
			driverClassName = nodeMap.getNamedItem("driverClassName").getNodeValue();
			url = nodeMap.getNamedItem("url").getNodeValue();
			username = nodeMap.getNamedItem("username").getNodeValue();
			password = nodeMap.getNamedItem("password").getNodeValue();

		} catch (Exception e) {
			e.printStackTrace();
			log.fatal(msg.getProperty("FK151002") + e.getMessage());
			System.out.println(msg.getProperty("DK150002"));
			System.exit(1); // 異常終了
		}
		log.debug("DB接続情報取得完了");

		// 動作設定取得
		try {
			// DB接続
			Class.forName(driverClassName);
			Properties info = new Properties();
			info.put("user", username);
			info.put("password", password);
			Connection dbConnection = DriverManager.getConnection(url, info);
			log.debug("DB接続 OK");

			PreparedStatement stm;

			// 動作設定取得（SQL実行）
			stm = dbConnection.prepareStatement(SELECT_CONSTANT_SQL);
			stm.clearParameters();
			ResultSet res = stm.executeQuery();
			res.next();
			serverAddr = res.getString(COLUMN_FTP_ADDR);
			login = res.getString(COLUMN_FTP_ID);
			pass = res.getString(COLUMN_FTP_PWD);
			download_remoteDir = res.getString(COLUMN_DOWNLOAD_REMOTE_DIR);
			download_localBaseDir = res.getString(COLUMN_DOWNLOAD_LOCAL_DIR);
			stm.close();
			log.debug("SQL実行 OK(動作設定取得)");

			// DB切断
			dbConnection.close();
			log.debug("DB切断 OK");

		} catch (Exception e) {
			e.printStackTrace();
			log.fatal(msg.getProperty("FK151003") + e.getMessage());
			System.out.println(msg.getProperty("DK150003"));
			System.exit(1); // 異常終了
		}
		log.debug("動作設定取得完了");

		// ログサーバからダウンロード用ログをダウンロード
		FileOutputStream os;
		String[] flistFtp = null;
		List<String> flist = new ArrayList<String>();
		// FTP接続
		try {
			ftpObj = new FtpCtr(serverAddr, login, pass, download_localBaseDir, download_remoteDir);
			if (ftpObj.connectFtp() == false) {
				log.fatal(msg.getProperty("FK151004"));
				System.out.println(msg.getProperty("DK150004"));
				System.exit(1); // 異常終了
			}
			log.debug("FTP接続 OK");

			// ダウンロード用ログファイル一覧取得
			flistFtp = ftpObj.listNames();
			if (flistFtp.length <= 0) {
				ftpObj.close();
				log.error(msg.getProperty("EK151005"));
				System.out.println(msg.getProperty("DK150005"));
				System.exit(0); // 準正常終了
			}
			log.debug("FTP ファイル一覧取得 OK");
		} catch (Exception e) {
			log.fatal(msg.getProperty("FK151006") + e.getMessage());
			System.out.println(msg.getProperty("DK150006"));
			System.exit(1); // 異常終了
		}

		// ダウンロード用ログファイルダウンロード
		try {
			int flistCnt = 0;
			for (cnt = 0; cnt < flistFtp.length; cnt++) {
				// ファイル受信
				String lFile = String.format("%s/%s", download_localBaseDir, flistFtp[cnt]);
				Pattern chkPattern = Pattern.compile(DOWNLOAD_LOG_FILE_PATTERN);
				Matcher matcher = chkPattern.matcher(flistFtp[cnt]);
				if (matcher.matches() == true) {
					// ローカルにDLし、作業用の配列へ追加
					os = new FileOutputStream(lFile); // ローカルファイル名
					ftpObj.retrieveFile(flistFtp[cnt], os); // サーバー側
					os.close();
					flist.add(flistFtp[cnt]);
					log.debug("FTP ファイルダウンロード (" + flistFtp[cnt] + ")");
					flistCnt++;
				}
			}
			// FTP切断処理
			ftpObj.close();
			log.debug("FTP切断 OK");
			if (flist.size() <= 0) {
				log.error(msg.getProperty("EK151007"));
				System.out.println(msg.getProperty("DK150007"));
				System.exit(0); // 準正常終了
			}
		} catch (Exception e) {
			ftpObj.close();
			log.fatal(msg.getProperty("FK151008") + e.getMessage());
			System.out.println(msg.getProperty("DK150008"));
			System.exit(1); // 異常終了
		}
		log.debug("ダウンロード用ログファイルダウンロード完了");

		// ファイル削除フェーズ（サーバー側のファイルを削除）
		try {
			// FTP接続
			if (ftpObj.connectFtp() == false) {
				log.fatal(msg.getProperty("FK151004"));
				System.out.println(msg.getProperty("DK150004"));
				System.exit(1); // 異常終了
			}
			log.debug("FTP接続 OK");

			// ログ削除（FTP）
			for (int i = 0; i < flist.size(); i++) {
				// ファイル受信
				try {
					// FTPサーバ側のファイル削除
					String sFile = flist.get(i);
					ftpObj.deleteFile(sFile);
					log.debug("リモートファイル削除 OK(" + sFile +")");

				} catch (IOException e) {
					e.printStackTrace();
				}// サーバー側
			}

			// FTP切断処理
			ftpObj.close();
			log.debug("FTP切断 OK");
		} catch (Exception e) {
			// 共通例外処理
			e.printStackTrace();
			log.fatal(msg.getProperty("FK151010") + e.getMessage());
			System.out.println(msg.getProperty("DK150010"));
			System.exit(1); // 異常終了
		}

		// 終了処理
		log.info(msg.getProperty("IK150023"));
		System.out.println(msg.getProperty("DK150023"));
		System.exit(0); // 正常終了
	}

}
