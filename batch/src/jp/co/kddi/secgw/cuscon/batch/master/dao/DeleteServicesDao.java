/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコンバッチ
 * ファイル名   : DeleteServicesDao.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     matsshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.master.dao;

import java.util.*;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


/**
 * クラス名：DeleteServicesDao<br>
 * 機能概要：M_Servicesテーブルのデータ削除クラスです。<br>
 * 備考：<br>
 * @author matsshtt
 * @version 1.0 matsshtt<br>
 *             Created 2010/03/01<br>
 *             新規作成
 *
 */
public class DeleteServicesDao {
	// ロガー
    Logger log = LogUtil.getCallerLogger();
    /** データソース */
    private DriverManagerDataSource dataSource;

    private static final String mapKey_names = "names";
    private static final String mapKey_name = "name";
	final String sql = "delete from m_services where name not in (:names) " +
	"and seq_no not in (select distinct LINK_SEQ_NO from T_serviceLink) " +
	"and seq_no not in (select distinct LINK_SEQ_NO from T_servicesGroupsLink)";

    /**
     * メソッド名：M_Servicesテーブルのデータ削除<br>
     * 機能概要：パラメータのサービスのリストに含まれないサービスデータを削除する。<br>
     *
     * @param List<HashMap<String, Object>> PAAplList 削除しないサービス
     */
    public void delete(List<HashMap<String, Object>> PAAplList) throws DataAccessException {
	    log.debug( LogUtil.getClassMethod() + " 開始");

	    // パラメータ作成
    	Map<String,ArrayList> params = new HashMap();
    	ArrayList tmpArray = new ArrayList();
    	for(int k=0; k < PAAplList.size(); ++k) {
    		tmpArray.add((String)PAAplList.get(k).get(mapKey_name));
    	}
    	params.put(mapKey_names, tmpArray);

    	NamedParameterJdbcTemplate jt = new NamedParameterJdbcTemplate(this.dataSource);

  		// SQL実行
    	try{
    		jt.update(sql, params);
    	} catch (DataAccessException e) {
    		log.fatal(LogUtil.getErrorMSG("FK160001"),e);
    		throw e;
    	}

	    log.debug( LogUtil.getClassMethod() + " 終了");
    }


    /**
     * メソッド名：データソース設定<br>
     * 機能概要：データソースをセットする<br>
     * @param ds データソース
     */
    public void setdataSource(DriverManagerDataSource ds) {
	    log.debug( LogUtil.getClassMethod() + " 開始");
        this.dataSource = ds;
	    log.debug( LogUtil.getClassMethod() + " 終了");    }

}

