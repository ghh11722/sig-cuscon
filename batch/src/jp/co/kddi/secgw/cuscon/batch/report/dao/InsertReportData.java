/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : InsertReportData.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/05     matsshtt                初版作成
 * 2012/12/01     kkato@PROSITE           PANOS4.1.x対応
 * 2013/11/19     kkato@PROSITE           XML解析不具合修正
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.report.dao;

import java.util.*;


import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;
import jp.co.kddi.secgw.cuscon.batch.report.dao.*;

/**
 * クラス名 : InsertReportData
 * 機能概要 : PAより取得したマスタ情報を引数として受け取り、cusconDBに保存する
 * 備考 :
 * @author matsshtt
 * @version 1.0 matsshtt
 *          Created 2010/03/05
 *          新規作成
 * @see
 */
public class InsertReportData {

	// レポート種別
    private static final String repName_TopApp = "Top applications";
    private static final String repName_TopThr = "Top threats";
    private static final String repName_TopDst = "Top destinations";
    private static final String repName_TopDstC = "Top destination countries";
    private static final String repName_TopSrc = "Top sources";
    // ストアドプロシージャ名
    private static final String SPName_TopApp = "InsertTopAppSummary";
    private static final String SPName_TopAtt = "InsertTopAttacksSummary";
    private static final String SPName_TopDst = "InsertTopDstSummary";
    private static final String SPName_TopDstC = "InsertTopDstCountriesSummary";
    private static final String SPName_TopSrc = "InsertTopSrcSummary";
    // コンテキストファイル名
    private static final String contextFileName = "applicationContext.xml";
    // XML Tag
    private static final String xmlTag_start = "start";
    private static final String xmlTag_Name = "name";

	public static void execute(List<HashMap<String,Object>> rept,	String vsysID) throws DataAccessException{
		// ロガー
	    Logger log = LogUtil.getCallerLogger();

	    log.info( LogUtil.getClassMethod() + " 開始");

		// レポートの「取得開始時間」、「レポート名」を取得
	    // 20121201 kkato@PROSITE mod start
	    // 20131119 kkato@PROSITE mod start
		String repStart = ((String)((HashMap)rept.get(0)).get(xmlTag_start)).substring(0,10);
		String repName = (String)((HashMap)rept.get(0)).get(xmlTag_Name);
		//String repStart = ((String)((HashMap)rept.get(1)).get(xmlTag_start)).substring(0,10);
		//String repName = (String)((HashMap)rept.get(1)).get(xmlTag_Name);
		//rept.remove(1);
	    // 20131119 kkato@PROSITE mod end
	    // 20121201 kkato@PROSITE mod end
		// レポートのデータ部分以外を削除
		rept.remove(0);

        try {
            // アプリケーションコンテキスト取得
        	ApplicationContext context = new ClassPathXmlApplicationContext(contextFileName);

        	// レポート種別毎のDAOクラスに振り分け
        	if(repName.equals(repName_TopApp)) {
        	    log.debug("レポート種別=Top applications");
        		InsertTopAppSummaryDao dao
        			= (InsertTopAppSummaryDao)context.getBean(SPName_TopApp);
        		dao.insert(rept, vsysID, repStart);
        	}
        	else if (repName.equals(repName_TopThr)) {
        	    log.debug("レポート種別=Top threats");
        		InsertTopAttacksSummaryDao dao
    				= (InsertTopAttacksSummaryDao)context.getBean(SPName_TopAtt);
        		dao.insert(rept, vsysID, repStart);
        	}
        	else if (repName.equals(repName_TopDst)) {
        	    log.debug("レポート種別=Top destinations");
        		InsertTopDstSummaryDao dao
    				= (InsertTopDstSummaryDao)context.getBean(SPName_TopDst);
        		dao.insert(rept, vsysID, repStart);
        	}
        	else if (repName.equals(repName_TopDstC)) {
        	    log.debug("レポート種別=Top destination countries");
        		InsertTopDstCountriesSummaryDao dao
    				= (InsertTopDstCountriesSummaryDao)context.getBean(SPName_TopDstC);
        		dao.insert(rept, vsysID, repStart);
        	}
        	else if (repName.equals(repName_TopSrc)) {
        	    log.debug("レポート種別=Top sources");
        		InsertTopSrcSummaryDao dao
    				= (InsertTopSrcSummaryDao)context.getBean(SPName_TopSrc);
        		dao.insert(rept, vsysID, repStart);
        	}
        	else {
              log.error("FK170010"+repName);
        	}

        } catch (DataAccessException e){
        	log.fatal(LogUtil.getErrorMSG("FK170009"));
            throw e;
        }

        log.info( LogUtil.getClassMethod() + " 終了");
	}

}
