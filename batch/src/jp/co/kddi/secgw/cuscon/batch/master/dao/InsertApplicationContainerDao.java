/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコンバッチ
 * ファイル名   : InsertApplicationsDao.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/10/31     inoue@prosite           初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.batch.master.dao;

import java.sql.Types;
import java.util.*;

import jp.co.kddi.secgw.cuscon.batch.commons.LogUtil;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.BatchSqlUpdate;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


/**
 * クラス名：InsertApplicationContainerDao<br>
 * 機能概要：アプリケーションコンテナマスタのデータのDB登録クラスです。<br>
 * 備考：<br>
 * @author inoue@prosite
 * @version 1.0 inoue@prosite<br>
 *             Created 2011/10/31<br>
 *             新規作成
 *
 */
public class InsertApplicationContainerDao {
	// ロガー
    Logger log = LogUtil.getCallerLogger();
    /** データソース */
    private DriverManagerDataSource dataSource;

    private static final String mapKey_name = "name";

    private static final String sql = "insert into m_applicationcontainer" +
    									"(name) " +
    									"values(?)";

    /**
     * メソッド名：M_Applicationsテーブルのデータ追加<br>
     * 機能概要：M_Applicationsテーブルにデータを追加する。<br>
     *
     * @param List<HashMap<String,Object>> PAAplList 登録するデータのリスト
     */
    public void insert(List<HashMap<String, Object>> PAAplList) throws DataAccessException{
	    log.debug( LogUtil.getClassMethod() + " 開始");

    	BatchSqlUpdate batchUpdater = new BatchSqlUpdate(this.dataSource, sql);
        batchUpdater.declareParameter(new SqlParameter(Types.VARCHAR));
        batchUpdater.compile();

  		// SQL実行
        try{
        	for(int i=0;i < PAAplList.size(); ++i){
        		Map tmpMap = PAAplList.get(i);
        		log.debug("M_Application挿入 name="+tmpMap.get(mapKey_name));
        		Object param[] = {tmpMap.get(mapKey_name)};
        		batchUpdater.update(param);
        	}

        	log.debug("M_ApplicationContainer挿入 batchUpdater.flush");
        	batchUpdater.flush();
        } catch (DataAccessException e) {
        	log.fatal(LogUtil.getErrorMSG("FK160001"),e);
        	throw e;
        }

	    log.debug( LogUtil.getClassMethod() + " 終了");
    }


    /**
     * メソッド名：データソース設定<br>
     * 機能概要：データソースをセットする<br>
     * @param ds データソース
     */
    public void setdataSource(DriverManagerDataSource ds) {
	    log.debug( LogUtil.getClassMethod() + " 開始");
        this.dataSource = ds;
	    log.debug( LogUtil.getClassMethod() + " 終了");
    }

}