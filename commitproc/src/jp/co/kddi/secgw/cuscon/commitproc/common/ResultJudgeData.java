/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ResultJudgeData.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/05     komakiys                初版作成
 * 2010/06/17     hiroyasu                コミット処理プロセス化対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.commitproc.common;

/**
 * クラス名 : ResultJudgeData
 * 機能概要 : 画面へエラーメッセージ出力するために使用するクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/05
 *          新規作成
 * @see
 */
public class ResultJudgeData {

	// 処理結果判定フラグ
	private int resultFlg = 0;

	// エラーメッセージor処理結果
	private String resultData = null;

	/**
	 * メソッド名 : resultFlgのGetterメソッド
	 * 機能概要 : resultFlgを取得する。
	 * @return resultFlg
	 */
	public int getResultFlg() {
		return resultFlg;
	}

	/**
	 * メソッド名 : resultFlgのSetterメソッド
	 * 機能概要 : resultFlgをセットする。
	 * @param resultFlg
	 */
	public void setResultFlg(int resultFlg) {
		this.resultFlg = resultFlg;
	}

	/**
	 * メソッド名 : resultDataのGetterメソッド
	 * 機能概要 : resultDataを取得する。
	 * @return resultData
	 */
	public String getResultData() {
		return resultData;
	}

	/**
	 * メソッド名 : resultDataのSetterメソッド
	 * 機能概要 : resultDataをセットする。
	 * @param resultData
	 */
	public void setResultData(String resultData) {
		this.resultData = resultData;
	}

}
