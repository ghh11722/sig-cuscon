/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : PolicyList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/19     komakiys         初版作成
 * 2010/06/17     hiroyasu         コミット処理プロセス化対応
 * 2011/06/10	  inoue@prosite    Webフィルタ,ウィルスチェック、アンチスパイウェアの追加対応
 * 2013/09/01     kkato@PROSITE    セキュリティルール有効/無効切替対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.commitproc.common;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.commitproc.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.SelectObjectList;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.SelectPolicyList;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.util.PropertyUtil;

/**
 * クラス名 : PolicyList
 * 機能概要 : セキュリティポリシー一覧を表示するための情報を取得する。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/19
 *          新規作成
 * @see
 */
public class PolicyList {

	/**
     * QueryDAO。
     * Springによりインスタンス生成され設定される。
     */
       // 区切り文字
	private static final String SPLIT = ",";

	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.commitproc.common.PolicyList");
    // メッセージクラス
    private static MessageAccessor messageAccessor = null;

    /**
     * メソッド名 : コンストラクタ
     * 機能概要 : -
     */
    public PolicyList(){
    	log.debug("PolicyListコンストラクタ2処理開始");
    	log.debug("PolicyListコンストラクタ2処理開始");
    }

	/**
	 * メソッド名 : getPolicylist
	 * 機能概要 : セキュリティポリシー一覧情報を取得する。
	 * @param vsysId Vsys-ID
	 * @param generationNo 世代番号
	 * @param processFlg 処理フラグ <br>0：ポリシー設定、1：コミット、2：ロールバック
	 * @param msgAcc メッセージアクセッサ
	 * @return List<Object> セキュリティポリシー一覧
	 * @throws Exception
	 */
	public List<SelectPolicyList> getPolicylist(
			QueryDAO queryDAO, CusconUVO uvo, int generationNo,
			int processFlg, MessageAccessor msgAcc) throws Exception {

		log.debug("getPolicylist処理開始 vsysId = " + uvo.getVsysId() +
				", generationNo = " + generationNo + ", processFlg = " + processFlg);
		messageAccessor = msgAcc;

		// 基本検索条件クラスにパラメータを設定する。
		InputBase base = new InputBase();
        base.setGenerationNo(generationNo);
        base.setVsysId(uvo.getVsysId());
        base.setModFlg(CusconConst.DEL_NUM);

        List<SelectPolicyList> rules;
        if(processFlg == CusconConst.POLICY) {
        	log.debug("ポリシー設定の場合");
        	base.setNonSubject(getHiddenObject());

        	try{
        		// セキュリティポリシー更新情報一覧を取得する。
        		rules =
        			queryDAO.executeForObjectList("CommonPolicyList-2", base);
        	} catch(Exception e) {
        		// DBアクセスエラー
        		Object[] objectgArray = {e.getMessage()};
        		log.fatal(messageAccessor.getMessage("FZ011032", objectgArray, uvo));
        		throw e;
        	}

        } else if (processFlg == CusconConst.COMMIT
        		|| (processFlg == CusconConst.ROLLBACK)) {
        	log.debug("コミット、ロールバックの場合");
        	try{
		        // セキュリティポリシー更新情報一覧を取得する。
		        rules = queryDAO.executeForObjectList("CommonPolicyList-1", base);
        	} catch(Exception e) {
        		// DBアクセスエラー
        		Object[] objectgArray = {e.getMessage()};
        		log.fatal(messageAccessor.getMessage("FZ011032", objectgArray, uvo));
        		throw e;
        	}
        } else {
        	log.debug("隠しルールのみ取得の場合");
        	base.setNonSubject(getHiddenObject());

        	try{
		        // セキュリティポリシー更新情報一覧を取得する。
		        rules = queryDAO.executeForObjectList("CommonPolicyList-3", base);
        	} catch(Exception e) {
        		// DBアクセスエラー
        		Object[] objectgArray = {e.getMessage()};
        		log.fatal(messageAccessor.getMessage("FZ011032", objectgArray, uvo));
        		throw e;
        	}
        }

        // 検索結果が0件
		if(rules.size() == 0) {
			log.debug("getPolicylist終了1:" + rules.size());
			return null;
		}
		List<SelectPolicyList> policyList;
        if(processFlg == CusconConst.HiddenRules) {
            policyList = editPolicyList2(rules);
        } else {
            policyList = editPolicyList(rules);
        }

        log.debug("getPolicylist処理終了2:policyList.size() = " + policyList.size());
		return policyList;
	}


	/**
	 * メソッド名 : editPolicyList
	 * 機能概要 : 受け取ったセキュリティポリシー一覧を編集し、
	 *            編集結果を返却する。<br>
	 *            同一行番号に複数のオブジェクトが紐付く場合は
	 *            各オブジェクトをリストに格納する。
	 * @param rules セキュリティポリシーリスト
	 * @return List<Object> 編集結果
	 */
	private List<SelectPolicyList> editPolicyList(List<SelectPolicyList> rules) {

		log.debug("editPolicyList処理開始:rules.size() = " + rules.size());
		// 結果格納リスト
		List<SelectPolicyList> resultPolicyList = new ArrayList<SelectPolicyList>();
		// 行番号
		int lineNo = 0;
		// セキュリティポリシーリスト
		SelectPolicyList securityPolicyList = null;
		// 出所ゾーン名リスト
		List<SelectObjectList> sourceZoneList = null;
		// オブジェクトリスト
		SelectObjectList obj = new SelectObjectList();
		// 宛先ゾーン名リスト
		List<SelectObjectList> destinationZoneList = null;
		// 出所アドレス名リスト
		List<SelectObjectList> sourceAddressList = null;
		// 宛先アドレス名リスト
		List<SelectObjectList> destinationAddressList = null;
		// アプリケーション名リスト
		List<SelectObjectList> applicationList = null;
		// サービス名リスト
		List<SelectObjectList> serviceList = null;

		for(int i=0; i<rules.size(); i++) {
			log.debug("ポリシー一覧情報存在");
			// 次の行番号が前の行番号と一致しない場合
			if(lineNo != rules.get(i).getLineNo()) {
				log.debug("行番号不一致:lineNo = " +
						lineNo + ", rules.get(i).getLineNo() = "
										+ rules.get(i).getLineNo());
				lineNo = rules.get(i).getLineNo();

				if(lineNo != 1) {
					log.debug("行番号1以外");
					securityPolicyList = new SelectPolicyList();
					// セキュリティポリシー一覧テーブルの情報をリストに設定する。
					securityPolicyList.setLineNo(lineNo - 1);
					securityPolicyList.setName(rules.get(i-1).getName());
					securityPolicyList.setDescription(rules.get(i-1).getDescription());
					securityPolicyList.setSchedulesSeqNo(rules.get(i-1).getSchedulesSeqNo());
					securityPolicyList.setServiceDefaultFlg(rules.get(i-1).getServiceDefaultFlg());
					securityPolicyList.setAction(rules.get(i-1).getAction());
					securityPolicyList.setIds_ips(rules.get(i-1).getIds_ips());
					securityPolicyList.setSchedule(rules.get(i-1).getSchedule());
//2011/06/10 add start inoue@prosite
					securityPolicyList.setUrlfilterDefaultFlg(rules.get(i-1).getUrlfilterDefaultFlg());
					securityPolicyList.setUrlfilterName(rules.get(i-1).getUrlfilterName());
					securityPolicyList.setVirusChecktFlg(rules.get(i-1).getVirusChecktFlg());
					securityPolicyList.setSpywareFlg(rules.get(i-1).getSpywareFlg());
//2011/06/10 add end inoue@prosite
					// 20130901 kkato@PROSITE add start
					securityPolicyList.setDisableFlg(rules.get(i-1).getDisableFlg());
					// 20130901 kkato@PROSITE add end
					// オブジェクトリストをセキュリティポリシーリストに追加する。
					securityPolicyList.setSourceZoneList(sourceZoneList);
					securityPolicyList.setDestinationZoneList(destinationZoneList);
					securityPolicyList.setSourceAddressList(sourceAddressList);
					securityPolicyList.setDestinationAddressList(destinationAddressList);
					securityPolicyList.setApplicationList(applicationList);
					securityPolicyList.setServiceList(serviceList);

					// 結果格納リストにセキュリティポリシーリストを追加する。
					resultPolicyList.add(securityPolicyList);
				}

				// 出所ゾーンリストを生成する。
				sourceZoneList = new ArrayList<SelectObjectList>();
				obj = new SelectObjectList();
				obj.setName(rules.get(i).getSourceZone());
				// 出所ゾーンを追加する。
				sourceZoneList.add(obj);

				// 宛先ゾーンリストを生成する。
				destinationZoneList = new ArrayList<SelectObjectList>();
				obj = new SelectObjectList();
				obj.setName(rules.get(i).getDestinationZone());
				// 宛先ゾーンを追加する。
				destinationZoneList.add(obj);

				// 出所アドレスリストを生成する。
				sourceAddressList = new ArrayList<SelectObjectList>();
				obj = new SelectObjectList();
				obj.setName(rules.get(i).getSourceAddress());
				obj.setObjectType(rules.get(i).getSourceAddressType());
				// 出所アドレスを追加する。
				sourceAddressList.add(obj);

				// 宛先アドレスリストを生成する。
				destinationAddressList = new ArrayList<SelectObjectList>();
				obj = new SelectObjectList();
				obj.setName(rules.get(i).getDestinationAddress());
				obj.setObjectType(rules.get(i).getDestinationAddressType());
				// 宛先アドレスを追加する。
				destinationAddressList.add(obj);

				// アプリケーションリストを生成する。
				applicationList = new ArrayList<SelectObjectList>();
				obj = new SelectObjectList();
				obj.setName(rules.get(i).getApplication());
				obj.setObjectType(rules.get(i).getApplicationType());
				// アプリケーションを追加する。
				applicationList.add(obj);

				// サービスリストを生成する。
				serviceList = new ArrayList<SelectObjectList>();
				obj = new SelectObjectList();
				obj.setName(rules.get(i).getService());
				obj.setObjectType(rules.get(i).getServiceType());
				// サービスを追加する。
				serviceList.add(obj);

			} else {
				log.debug("行番号一致:lineNo = " +
						lineNo + ", rules.get(i).getLineNo() = "
										+ rules.get(i).getLineNo());
				// 出所ゾーン名存在チェックを行う。
				if(checkExistList(sourceZoneList, rules.get(i).getSourceZone())) {
					log.debug("出所ゾーンリストに同一名が存在しない");
					obj = new SelectObjectList();
					obj.setName(rules.get(i).getSourceZone());
					// 出所ゾーンを追加する。
					sourceZoneList.add(obj);
				}

				// 宛先ゾーン名存在チェックを行う。
				if(checkExistList(destinationZoneList, rules.get(i).getDestinationZone())) {
					log.debug("宛先ゾーンリストに同一名が存在しない");
					obj = new SelectObjectList();
					obj.setName(rules.get(i).getDestinationZone());
					// 宛先ゾーン名を追加する。
					destinationZoneList.add(obj);
				}

				// 出所アドレス名存在チェックを行う。
				if(checkExistList(sourceAddressList, rules.get(i).getSourceAddress())) {
					log.debug("出所アドレスリストに同一名が存在しない");
					obj = new SelectObjectList();
					obj.setName(rules.get(i).getSourceAddress());
					obj.setObjectType(rules.get(i).getSourceAddressType());
					// 出所アドレス名を追加する。
					sourceAddressList.add(obj);
				}

				// 宛先アドレス名存在チェックを行う。
				if(checkExistList(destinationAddressList, rules.get(i).getDestinationAddress())) {
					log.debug("宛先アドレスリストに同一名が存在しない");
					obj = new SelectObjectList();
					obj.setName(rules.get(i).getDestinationAddress());
					obj.setObjectType(rules.get(i).getDestinationAddressType());
					// 出所アドレス名を追加する。
					destinationAddressList.add(obj);
				}

				// アプリケーション名存在チェックを行う。
				if(checkExistList(applicationList, rules.get(i).getApplication())) {
					log.debug("アプリケーションリストに同一名が存在しない");
					obj = new SelectObjectList();
					obj.setName(rules.get(i).getApplication());
					obj.setObjectType(rules.get(i).getApplicationType());
					// アプリケーション名を追加する。
					applicationList.add(obj);
				}

				// サービス名存在チェックを行う。
				if(checkExistList(serviceList, rules.get(i).getService())) {
					log.debug("サービスリストに同一名が存在しない");
					obj = new SelectObjectList();
					obj.setName(rules.get(i).getService());
					obj.setObjectType(rules.get(i).getServiceType());
					// サービス名を追加する。
					serviceList.add(obj);
				}
			}
		}

		// 最後の要素を登録する。
		securityPolicyList = new SelectPolicyList();
		// セキュリティポリシー一覧テーブルの情報をリストに設定する。
		securityPolicyList.setLineNo(lineNo);
		securityPolicyList.setName(rules.get(rules.size()-1).getName());
		securityPolicyList.setDescription(rules.get(rules.size()-1).getDescription());
		securityPolicyList.setSchedulesSeqNo(rules.get(rules.size()-1).getSchedulesSeqNo());
		securityPolicyList.setServiceDefaultFlg(rules.get(rules.size()-1).getServiceDefaultFlg());
		securityPolicyList.setAction(rules.get(rules.size()-1).getAction());
		securityPolicyList.setIds_ips(rules.get(rules.size()-1).getIds_ips());
		securityPolicyList.setSchedule(rules.get(rules.size()-1).getSchedule());
//2011/06/10 add start inoue@prosite
		securityPolicyList.setUrlfilterDefaultFlg(rules.get(rules.size()-1).getUrlfilterDefaultFlg());
		securityPolicyList.setUrlfilterName(rules.get(rules.size()-1).getUrlfilterName());
		securityPolicyList.setVirusChecktFlg(rules.get(rules.size()-1).getVirusChecktFlg());
		securityPolicyList.setSpywareFlg(rules.get(rules.size()-1).getSpywareFlg());
//2011/06/10 add end inoue@prosite
		// 20130901 kkato@PROSITE add start
		securityPolicyList.setDisableFlg(rules.get(rules.size()-1).getDisableFlg());
		// 20130901 kkato@PROSITE add end
		// オブジェクトリストをセキュリティポリシーリストに追加する。
		securityPolicyList.setSourceZoneList(sourceZoneList);
		securityPolicyList.setDestinationZoneList(destinationZoneList);
		securityPolicyList.setSourceAddressList(sourceAddressList);
		securityPolicyList.setDestinationAddressList(destinationAddressList);
		securityPolicyList.setApplicationList(applicationList);
		securityPolicyList.setServiceList(serviceList);

		// 結果格納リストにセキュリティポリシーリストを追加する。
		resultPolicyList.add(securityPolicyList);
		log.debug("editPolicyList処理終了:resultPolicyList() = " + resultPolicyList.size());
	return resultPolicyList;

	}

	/**
	 * メソッド名 : editPolicyList2
	 * 機能概要 : 受け取ったセキュリティポリシー一覧を編集し、
	 *            編集結果を返却する。<br>
	 *            同一行番号に複数のオブジェクトが紐付く場合は
	 *            各オブジェクトをリストに格納する。(隠しルール専用)
	 * @param rules セキュリティポリシーリスト
	 * @return List<Object> 編集結果
	 */
	private List<SelectPolicyList> editPolicyList2(List<SelectPolicyList> rules) {

		log.debug("editPolicyList処理開始:rules.size() = " + rules.size());
		// 結果格納リスト
		List<SelectPolicyList> resultPolicyList = new ArrayList<SelectPolicyList>();
		// 行番号
		int lineNo = 0;
		// セキュリティポリシーリスト
		SelectPolicyList securityPolicyList = null;
		// 出所ゾーン名リスト
		List<SelectObjectList> sourceZoneList = null;
		// オブジェクトリスト
		SelectObjectList obj = new SelectObjectList();
		// 宛先ゾーン名リスト
		List<SelectObjectList> destinationZoneList = null;
		// 出所アドレス名リスト
		List<SelectObjectList> sourceAddressList = null;
		// 宛先アドレス名リスト
		List<SelectObjectList> destinationAddressList = null;
		// アプリケーション名リスト
		List<SelectObjectList> applicationList = null;
		// サービス名リスト
		List<SelectObjectList> serviceList = null;

		// 先頭行数
		int Line1 = rules.get(0).getLineNo();

		for(int i=0; i<rules.size(); i++) {
			log.debug("ポリシー一覧情報存在");
			// 次の行番号が前の行番号と一致しない場合
			if(lineNo != rules.get(i).getLineNo()) {
				log.debug("行番号不一致:lineNo = " +
						lineNo + ", rules.get(i).getLineNo() = "
										+ rules.get(i).getLineNo());
				lineNo = rules.get(i).getLineNo();

				if(lineNo != Line1) {
					log.debug("行番号1以外");
					securityPolicyList = new SelectPolicyList();
					// セキュリティポリシー一覧テーブルの情報をリストに設定する。
					securityPolicyList.setLineNo(lineNo - 1);
					securityPolicyList.setName(rules.get(i-1).getName());
					securityPolicyList.setDescription(rules.get(i-1).getDescription());
					securityPolicyList.setSchedulesSeqNo(rules.get(i-1).getSchedulesSeqNo());
					securityPolicyList.setServiceDefaultFlg(rules.get(i-1).getServiceDefaultFlg());
					securityPolicyList.setAction(rules.get(i-1).getAction());
					securityPolicyList.setIds_ips(rules.get(i-1).getIds_ips());
					securityPolicyList.setSchedule(rules.get(i-1).getSchedule());
//2011/06/10 add start inoue@prosite
					securityPolicyList.setUrlfilterDefaultFlg(rules.get(i-1).getUrlfilterDefaultFlg());
					securityPolicyList.setUrlfilterName(rules.get(i-1).getUrlfilterName());
					securityPolicyList.setVirusChecktFlg(rules.get(i-1).getVirusChecktFlg());
					securityPolicyList.setSpywareFlg(rules.get(i-1).getSpywareFlg());
//2011/06/10 add end inoue@prosite
					// 20130901 kkato@PROSITE add start
					securityPolicyList.setDisableFlg(rules.get(i-1).getDisableFlg());
					// 20130901 kkato@PROSITE add end
					// オブジェクトリストをセキュリティポリシーリストに追加する。
					securityPolicyList.setSourceZoneList(sourceZoneList);
					securityPolicyList.setDestinationZoneList(destinationZoneList);
					securityPolicyList.setSourceAddressList(sourceAddressList);
					securityPolicyList.setDestinationAddressList(destinationAddressList);
					securityPolicyList.setApplicationList(applicationList);
					securityPolicyList.setServiceList(serviceList);

					// 結果格納リストにセキュリティポリシーリストを追加する。
					resultPolicyList.add(securityPolicyList);
				}

				// 出所ゾーンリストを生成する。
				sourceZoneList = new ArrayList<SelectObjectList>();
				obj = new SelectObjectList();
				obj.setName(rules.get(i).getSourceZone());
				// 出所ゾーンを追加する。
				sourceZoneList.add(obj);

				// 宛先ゾーンリストを生成する。
				destinationZoneList = new ArrayList<SelectObjectList>();
				obj = new SelectObjectList();
				obj.setName(rules.get(i).getDestinationZone());
				// 宛先ゾーンを追加する。
				destinationZoneList.add(obj);

				// 出所アドレスリストを生成する。
				sourceAddressList = new ArrayList<SelectObjectList>();
				obj = new SelectObjectList();
				obj.setName(rules.get(i).getSourceAddress());
				obj.setObjectType(rules.get(i).getSourceAddressType());
				// 出所アドレスを追加する。
				sourceAddressList.add(obj);

				// 宛先アドレスリストを生成する。
				destinationAddressList = new ArrayList<SelectObjectList>();
				obj = new SelectObjectList();
				obj.setName(rules.get(i).getDestinationAddress());
				obj.setObjectType(rules.get(i).getDestinationAddressType());
				// 宛先アドレスを追加する。
				destinationAddressList.add(obj);

				// アプリケーションリストを生成する。
				applicationList = new ArrayList<SelectObjectList>();
				obj = new SelectObjectList();
				obj.setName(rules.get(i).getApplication());
				obj.setObjectType(rules.get(i).getApplicationType());
				// アプリケーションを追加する。
				applicationList.add(obj);

				// サービスリストを生成する。
				serviceList = new ArrayList<SelectObjectList>();
				obj = new SelectObjectList();
				obj.setName(rules.get(i).getService());
				obj.setObjectType(rules.get(i).getServiceType());
				// サービスを追加する。
				serviceList.add(obj);

			} else {
				log.debug("行番号一致:lineNo = " +
						lineNo + ", rules.get(i).getLineNo() = "
										+ rules.get(i).getLineNo());
				// 出所ゾーン名存在チェックを行う。
				if(checkExistList(sourceZoneList, rules.get(i).getSourceZone())) {
					log.debug("出所ゾーンリストに同一名が存在しない");
					obj = new SelectObjectList();
					obj.setName(rules.get(i).getSourceZone());
					// 出所ゾーンを追加する。
					sourceZoneList.add(obj);
				}

				// 宛先ゾーン名存在チェックを行う。
				if(checkExistList(destinationZoneList, rules.get(i).getDestinationZone())) {
					log.debug("宛先ゾーンリストに同一名が存在しない");
					obj = new SelectObjectList();
					obj.setName(rules.get(i).getDestinationZone());
					// 宛先ゾーン名を追加する。
					destinationZoneList.add(obj);
				}

				// 出所アドレス名存在チェックを行う。
				if(checkExistList(sourceAddressList, rules.get(i).getSourceAddress())) {
					log.debug("出所アドレスリストに同一名が存在しない");
					obj = new SelectObjectList();
					obj.setName(rules.get(i).getSourceAddress());
					obj.setObjectType(rules.get(i).getSourceAddressType());
					// 出所アドレス名を追加する。
					sourceAddressList.add(obj);
				}

				// 宛先アドレス名存在チェックを行う。
				if(checkExistList(destinationAddressList, rules.get(i).getDestinationAddress())) {
					log.debug("宛先アドレスリストに同一名が存在しない");
					obj = new SelectObjectList();
					obj.setName(rules.get(i).getDestinationAddress());
					obj.setObjectType(rules.get(i).getDestinationAddressType());
					// 出所アドレス名を追加する。
					destinationAddressList.add(obj);
				}

				// アプリケーション名存在チェックを行う。
				if(checkExistList(applicationList, rules.get(i).getApplication())) {
					log.debug("アプリケーションリストに同一名が存在しない");
					obj = new SelectObjectList();
					obj.setName(rules.get(i).getApplication());
					obj.setObjectType(rules.get(i).getApplicationType());
					// アプリケーション名を追加する。
					applicationList.add(obj);
				}

				// サービス名存在チェックを行う。
				if(checkExistList(serviceList, rules.get(i).getService())) {
					log.debug("サービスリストに同一名が存在しない");
					obj = new SelectObjectList();
					obj.setName(rules.get(i).getService());
					obj.setObjectType(rules.get(i).getServiceType());
					// サービス名を追加する。
					serviceList.add(obj);
				}
			}
		}

		// 最後の要素を登録する。
		securityPolicyList = new SelectPolicyList();
		// セキュリティポリシー一覧テーブルの情報をリストに設定する。
		securityPolicyList.setLineNo(lineNo);
		securityPolicyList.setName(rules.get(rules.size()-1).getName());
		securityPolicyList.setDescription(rules.get(rules.size()-1).getDescription());
		securityPolicyList.setSchedulesSeqNo(rules.get(rules.size()-1).getSchedulesSeqNo());
		securityPolicyList.setServiceDefaultFlg(rules.get(rules.size()-1).getServiceDefaultFlg());
		securityPolicyList.setAction(rules.get(rules.size()-1).getAction());
		securityPolicyList.setIds_ips(rules.get(rules.size()-1).getIds_ips());
		securityPolicyList.setSchedule(rules.get(rules.size()-1).getSchedule());
//2011/06/10 add start inoue@prosite
		securityPolicyList.setUrlfilterDefaultFlg(rules.get(rules.size()-1).getUrlfilterDefaultFlg());
		securityPolicyList.setUrlfilterName(rules.get(rules.size()-1).getUrlfilterName());
		securityPolicyList.setVirusChecktFlg(rules.get(rules.size()-1).getVirusChecktFlg());
		securityPolicyList.setSpywareFlg(rules.get(rules.size()-1).getSpywareFlg());
//2011/06/10 add end inoue@prosite
		// 20130901 kkato@PROSITE add start
		securityPolicyList.setDisableFlg(rules.get(rules.size()-1).getDisableFlg());
		// 20130901 kkato@PROSITE add end
		// オブジェクトリストをセキュリティポリシーリストに追加する。
		securityPolicyList.setSourceZoneList(sourceZoneList);
		securityPolicyList.setDestinationZoneList(destinationZoneList);
		securityPolicyList.setSourceAddressList(sourceAddressList);
		securityPolicyList.setDestinationAddressList(destinationAddressList);
		securityPolicyList.setApplicationList(applicationList);
		securityPolicyList.setServiceList(serviceList);

		// 結果格納リストにセキュリティポリシーリストを追加する。
		resultPolicyList.add(securityPolicyList);
		log.debug("editPolicyList処理終了:resultPolicyList() = " + resultPolicyList.size());
		return resultPolicyList;

	}
	/**
	 * メソッド名 : checkExistList
	 * 機能概要 : インプットのobjListにstrが存在しているかのチェックを行う。<br>
	 *            存在しない場合：true、存在する場合：false
	 * @param objList オブジェクトリスト
	 * @param str オブジェクト名
	 * @return boolean チェック結果
	 */
	private boolean checkExistList(List<SelectObjectList> objList, String str) {
		log.debug("checkExistList処理開始:objList =" + objList.size() + ", str = "+ str);
		for(int i=0; i<objList.size(); i++) {
			log.debug("checkExistList処理開始:objList =" + objList.size() + ", str = "+ str);
			// objListの中にstrが存在していない場合
			if(objList.get(i).getName().equals(str)) {
				log.debug("checkExistList処理終了:false");
				return false;
			}
		}
		log.debug("checkExistList処理終了:true");
		return true;
	}

	public List<String> getHiddenObject() {
		log.debug("getHiddenObject処理開始");
		// 隠しポリシー名を取得
        String nonSubject =
        	PropertyUtil.getProperty("fwsetting.common.nosubectpolicy");
        log.debug("隠しポリシー名:" + nonSubject);

        String[] splitNonSubject  = nonSubject.split(SPLIT);
        List<String> objectList = new ArrayList<String>();

        // 隠しポリシー名をリストに格納する。
        for(int i=0; i<splitNonSubject.length; i++) {
        	log.debug("隠しポリシー存在:splitNonSubject.length = " + splitNonSubject.length);
        	objectList.add(splitNonSubject[i]);
        }
        log.debug("getHiddenObject処理終了:objectList.size = " + objectList.size());
        return objectList;
	}
}
