/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : LinkInfo.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/19     morisou                 初版作成
 * 2010/06/17     hiroyasu                コミット処理プロセス化対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.commitproc.common.vo;

import java.io.Serializable;
import java.util.List;

/**
 * クラス名 : LinkInfo
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/19
 *          新規作成
 * @see
 */
public class LinkInfo  implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 1921171906239848903L;
	// グループオブジェクトのSEQ_NO
	private int seqNo = 0;
	// グループオブジェクト名
	private String name = null;
	// OBJECT_TYPE
	private int objectType = 0;
	// LINK_SEQ_NO
	private int linkSeqNo = 0;
	// リンク対象名
	private String linkName = null;
	// リンク対象名リスト
	private List<String> linkNameList = null;
	// 世代番号
	private int generationNo = 0;
	// VsysID
	private String vsysId = null;

	/**
	 * メソッド名 : seqNoのGetterメソッド
	 * 機能概要 : seqNoを取得する。
	 * @return seqNo
	 */
	public int getSeqNo() {
		return seqNo;
	}
	/**
	 * メソッド名 : seqNoのSetterメソッド
	 * 機能概要 : seqNoをセットする。
	 * @param seqNo
	 */
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : objectTypeのGetterメソッド
	 * 機能概要 : objectTypeを取得する。
	 * @return objectType
	 */
	public int getObjectType() {
		return objectType;
	}
	/**
	 * メソッド名 : objectTypeのSetterメソッド
	 * 機能概要 : objectTypeをセットする。
	 * @param objectType
	 */
	public void setObjectType(int objectType) {
		this.objectType = objectType;
	}
	/**
	 * メソッド名 : linkSeqNoのGetterメソッド
	 * 機能概要 : linkSeqNoを取得する。
	 * @return linkSeqNo
	 */
	public int getLinkSeqNo() {
		return linkSeqNo;
	}
	/**
	 * メソッド名 : linkSeqNoのSetterメソッド
	 * 機能概要 : linkSeqNoをセットする。
	 * @param linkSeqNo
	 */
	public void setLinkSeqNo(int linkSeqNo) {
		this.linkSeqNo = linkSeqNo;
	}
	/**
	 * メソッド名 : linkNameのGetterメソッド
	 * 機能概要 : linkNameを取得する。
	 * @return linkName
	 */
	public String getLinkName() {
		return linkName;
	}
	/**
	 * メソッド名 : linkNameのSetterメソッド
	 * 機能概要 : linkNameをセットする。
	 * @param linkName
	 */
	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}
	/**
	 * メソッド名 : linkNameListのGetterメソッド
	 * 機能概要 : linkNameListを取得する。
	 * @return linkNameList
	 */
	public List<String> getLinkNameList() {
		return linkNameList;
	}
	/**
	 * メソッド名 : linkNameListのSetterメソッド
	 * 機能概要 : linkNameListをセットする。
	 * @param linkNameList
	 */
	public void setLinkNameList(List<String> linkNameList) {
		this.linkNameList = linkNameList;
	}
	/**
	 * メソッド名 : generationNoのGetterメソッド
	 * 機能概要 : generationNoを取得する。
	 * @return generationNo
	 */
	public int getGenerationNo() {
		return generationNo;
	}
	/**
	 * メソッド名 : generationNoのSetterメソッド
	 * 機能概要 : generationNoをセットする。
	 * @param generationNo
	 */
	public void setGenerationNo(int generationNo) {
		this.generationNo = generationNo;
	}
	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得する。
	 * @return vsysId
	 */
	public String getVsysId() {
		return vsysId;
	}
	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdをセットする。
	 * @param vsysId
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}
}
