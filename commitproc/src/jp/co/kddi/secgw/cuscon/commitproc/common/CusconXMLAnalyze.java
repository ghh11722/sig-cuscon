/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CusconXMLAnalyze.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/24     komakiys                初版作成
 * 2010/06/17     hiroyasu                コミット処理プロセス化対応
 * 2012/12/01     kato@PROSITE            PANOS4.1.x対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.commitproc.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * クラス名 : CusconXMLAnalyze
 * 機能概要 :
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/24
 *          新規作成
 * @see
 */
public class CusconXMLAnalyze {

	// 1つのオブジェクト単位のデータ
	private HashMap<String, Object> hash = null;
	// 解析結果格納用リスト
	private List<HashMap<String, Object>> result = null;
	// メンバリスト
	List<String> memList = null;
	// 子要素名
	private String name = null;

	// 処理で判定を行うタグ
	private static final String ERROR = "error";
	private static final String RESULT = "result";
	private static final String KEY = "key";
	private static final String MEMBER = "member";
	private static final String LINE = "line";
	private static final String ENTRY= "entry";
	private static final String NON_ERROR = "No such node";
	// 20121201 kkato@PROSITE add start
	private static final String SERVICE = "service";		// サービスオブジェクト取得用
    private static final String POLICY = "rulebase/security/rules";	// ポリシー取得用
    private static final String PROFILE_SETTING = "profile-setting";	// ポリシー取得用(profile-setting)
    private static final String URL_FILTERING = "url-filtering";	// ポリシー取得用(url-filtering)
	// 20121201 kkato@PROSITE add end

	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.commitproc.common.CusconXMLAnalyze");

    // メッセージクラス
    private static MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 */
	public CusconXMLAnalyze() {
		log.debug("CusconXMLAnalyzeコンストラクタ処理開始");
		log.debug("CusconXMLAnalyzeコンストラクタ処理終了");
	}

	// 20121201 kkato@PROSITE add start
	/**
	 * メソッド名 : analyzeXML_objects
	 * 機能概要 : XMLを解析し、解析結果を返却する。
	 * @param xml
	 * @return List 解析結果
	 * @throws Exception
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> analyzeXML_objects(
			InputStream xml, CusconUVO uvo, String objName, MessageAccessor msgAcc
			) throws Exception {

		log.debug("analyzeXML_objects処理開始");
		messageAccessor = msgAcc;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		// イテレータ
		Iterator iterator;
		Map.Entry obj;

		// XMLをパース
		Document doc = null;

		try {
			builder = factory.newDocumentBuilder();
			doc = builder.parse(xml);
		} catch (Exception e) {
			// XML解析エラー
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("EZ001002", objectgArray, uvo));
			throw e;
		}

		// ルート要素の取得する。
		Element element = doc.getDocumentElement();

	    // 子要素を含んだノードを列挙
		//NodeList list = element.getChildNodes();

		// ルートの子要素名を取得する。
		//String nodeName = list.item(0).getNodeName();

	    hash = new HashMap<String, Object>();
	    result = new ArrayList<HashMap<String, Object>>();
	    memList = new ArrayList<String>();
	    // エラーメッセージ格納
	    String error = null;

		// エラーの場合、エラーメッセージを出力し、nullを返却する。
	    if(element.getAttributes().item(0) != null &&
	    		element.getAttributes().item(0).getNodeValue().equals(ERROR)) {
	    	log.debug("XML-API取得エラー");
	    	// エラーメッセージを取得する。
	    	analyzeChildNode(element.getChildNodes());
	    	iterator = result.get(0).entrySet().iterator();
	    	obj = (Map.Entry)iterator.next();

	    	// エラーメッセージ出力
	    	for(int i=0; i<result.size(); i++) {
	    		log.debug("取得エラー内容取得:" + i);
	 			iterator = result.get(i).entrySet().iterator();
	 			while (iterator.hasNext()) {
	 				log.debug("ハッシュマップ:");
	 				obj = (Map.Entry)iterator.next();
	 				if(obj.getKey().equals(LINE) && obj.getValue().equals(NON_ERROR)){

	 					String msg = obj.getValue().toString();
	 			    	log.debug("PAに対象データが存在しない:" + msg);
	 				} else {
	 					log.debug("key:" + obj.getKey() + ", value = " + obj.getValue());
	 					error = obj.getValue().toString();
	 			    	// エラーメッセージ出力
	 			    	Object[] objectgArray = {error};
	 					log.error(messageAccessor.getMessage("EZ001003", objectgArray, uvo));
	 				}
	 			}
		 	}
	    	return null;
	    }

	    // ポリシー/オブジェクトの場合
		NodeList listentry = element.getElementsByTagName(ENTRY);
		//ENTRY要素の走査
		for (int i = 0; i < listentry.getLength(); i++) {
			Node childNode = listentry.item(i);

			// 子要素の属性を取得する。
			NamedNodeMap attrMap = childNode.getAttributes();
			// 属性の数を取得
			int attrs = attrMap.getLength();
			// 属性の数分繰り返す。
		    for (int iAttr=0; iAttr<attrs; iAttr++) {
		    	log.debug("属性:" + iAttr);
		    	// 属性ノードを取得する。
		    	Node attr = attrMap.item(iAttr);
		    	// 子要素をキー、属性を値としてハッシュマップに格納する。
		        hash.put(attr.getNodeName(), attr.getNodeValue());
		    }

			memList = new ArrayList<String>();
			String memName = ENTRY;
	        NodeList children = childNode.getChildNodes();

			for (int j = 0; j < children.getLength(); j++) {
				Node child = children.item(j);
				if (child.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
					if (child.getNodeName().equals(MEMBER)) {
						// メンバーリストに子要素の値を追加する。
						memList.add(child.getFirstChild().getNodeValue());
					} else {
						hash.put(child.getNodeName(), child.getFirstChild().getNodeValue());
						//孫要素がMEMBERの場合追加する
						NodeList grandchildren = children.item(j).getChildNodes();
						for (int k = 0; k < grandchildren.getLength(); k++) {
							Node grandchild = grandchildren.item(k);
							if (grandchild.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
								if (objName == SERVICE) {
									// サービスはポート番号をセットする
									analyzePort(grandchild);
									hash.put(child.getNodeName().toString(), grandchild.getNodeName());
								} else if (objName == POLICY && child.getNodeName().equals(PROFILE_SETTING)) {
									// ポリシーはprofileをセットする
									analyzeProfile(grandchild);
									hash.put(child.getNodeName().toString(), grandchild.getNodeName());
								} else {
									// メンバーリストに子要素の値を追加する。
									memList.add(grandchild.getFirstChild().getNodeValue());
									memName = child.getNodeName();
								}
							}
						}
						// 孫要素名をキー、メンバーリストを値としてハッシュマップに格納する。
						if (memList.size() > 0) {
							hash.put(memName, memList);
							memList = new ArrayList<String>();
							memName = ENTRY;
						}
					}
				}
			}
			// 子要素名をキー、メンバーリストを値としてハッシュマップに格納する。
			if (memList.size() > 0) {
				hash.put(memName, memList);
			}
			log.debug("結果格納:" +  i);
			result.add(hash);
			hash = new HashMap<String, Object>();
		}

	    log.debug("analyzeXML_objects処理終了:result.size() = " + result.size());
		return result;
	  }
	/**
	 * メソッド名 : analyzePort
	 * 機能概要 : portノードを解析する。
	 * @param analyzePort
	 */
	private void analyzePort(Node childNode) {
		log.debug("analyzePort処理開始");
		NodeList nodelist1 = childNode.getChildNodes();

		for (int i = 0; i < nodelist1.getLength(); i++) {
			Node child1 = nodelist1.item(i);
			if (child1.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
				hash.put(child1.getNodeName(), child1.getFirstChild().getNodeValue());
			}
		}
		log.debug("analyzePort処理終了");
		return ;
	  }
	/**
	 * メソッド名 : analyzeProfile
	 * 機能概要 : Profileノードを解析する。
	 * @param analyzeProfile
	 */
	private void analyzeProfile(Node childNode) {
		log.debug("analyzeProfile処理開始");
		memList = new ArrayList<String>();
		String memName = URL_FILTERING;
		NodeList nodelist1 = childNode.getChildNodes();

		for (int i = 0; i < nodelist1.getLength(); i++) {
			Node child1 = nodelist1.item(i);
			NodeList nodelist2 = child1.getChildNodes();
			for (int j = 0; j < nodelist2.getLength(); j++) {
				Node child2 = nodelist2.item(j);
				if (child2.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
					if (child1.getNodeName().equals(memName)) {
						// メンバーリストに子要素の値を追加する。
						memList.add(child2.getFirstChild().getNodeValue());
					} else {
						hash.put(child1.getNodeName(), child2.getFirstChild().getNodeValue());
					}
				}
			}
			// 子要素名をキー、メンバーリストを値としてハッシュマップに格納する。
			if (memList.size() > 0) {
				hash.put(memName, memList);
				memList = new ArrayList<String>();
			}
		}
		log.debug("analyzeProfile処理終了");
		return ;
	  }
	// 20121201 kkato@PROSITE add end

	/**
	 * メソッド名 : analyzeXML
	 * 機能概要 : XMLを解析し、解析結果を返却する。
	 * @param xml
	 * @return List 解析結果
	 * @throws Exception
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> analyzeXML(InputStream xml,
			CusconUVO uvo, MessageAccessor msgAcc) throws Exception {

		log.debug("analyzeXML処理開始");
		messageAccessor = msgAcc;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		// イテレータ
		Iterator iterator;
		Map.Entry obj;

		// XMLをパース
		Document doc = null;

		try {
			builder = factory.newDocumentBuilder();
			doc = builder.parse(xml);
		} catch (Exception e) {
			// XML解析エラー
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("EZ001002", objectgArray, uvo));
			throw e;
		}

		// ルート要素の取得する。
		Element element = doc.getDocumentElement();

	    // 子要素を含んだノードを列挙
		NodeList list = element.getChildNodes();

		// ルートの子要素名を取得する。
		String nodeName = list.item(0).getNodeName();

	    hash = new HashMap<String, Object>();
	    result = new ArrayList<HashMap<String, Object>>();
	    memList = new ArrayList<String>();
	    // エラーメッセージ格納
	    String error = null;

	    // エラーの場合、エラーメッセージを出力し、nullを返却する。
	    if(element.getAttributes().item(0) != null &&
	    		element.getAttributes().item(0).getNodeValue().equals(ERROR)) {
	    	log.debug("XML-API取得エラー");
	    	// エラーメッセージを取得する。
	    	analyzeChildNode(element.getChildNodes());
	    	iterator = result.get(0).entrySet().iterator();
	    	obj = (Map.Entry)iterator.next();

	    	// エラーメッセージ出力
	    	for(int i=0; i<result.size(); i++) {
	    		log.debug("取得エラー内容取得:" + i);
	 			iterator = result.get(i).entrySet().iterator();
	 			while (iterator.hasNext()) {
	 				log.debug("ハッシュマップ:");
	 				obj = (Map.Entry)iterator.next();
	 				if(obj.getKey().equals(LINE) && obj.getValue().equals(NON_ERROR)){

	 					String msg = obj.getValue().toString();
	 			    	log.debug("PAに対象データが存在しない:" + msg);
	 				} else {
	 					log.debug("key:" + obj.getKey() + ", value = " + obj.getValue());
	 					error = obj.getValue().toString();
	 			    	// エラーメッセージ出力
	 			    	Object[] objectgArray = {error};
	 					log.error(messageAccessor.getMessage("EZ001003", objectgArray, uvo));
	 				}
	 			}
		 	}

	    	return null;
	    }
	    // アクセスキー生成、ポリシー/オブジェクトの場合
	    if (nodeName.equals(RESULT)) {
	    	log.debug("アクセスキー生成orポリシー/オブジェクト:nodeName = " + nodeName);
	        // 子要素を取得する。
	        NodeList childNodes = list.item(0).getChildNodes();

	        // アクセスキー生成の場合
	        if(childNodes.item(0).getNodeName().equals(KEY)) {
	        	log.debug("アクセスキー生成:nodeName = " + childNodes.item(0).getNodeName());
	        	analyzeChildNode(element.getChildNodes());
	        }
	        // ポリシー名/オブジェクト名の要素を読み飛ばす。
        	NodeList grandChildNodes = childNodes.item(0).getChildNodes();
        	analyzeChildNode(grandChildNodes);

	    // レポートの場合
	    } else {
	    	log.debug("レポート:nodeName = " + nodeName);
	    	// 子要素を取得する。
	    	NodeList childNodes = list.item(0).getChildNodes();

	    	// 子要素の属性を取得する。
			NamedNodeMap attribute =list.item(0).getAttributes();
			// 属性の数分繰り返す。
		    for (int iAttr=0; iAttr<attribute.getLength(); iAttr++) {
		    	log.debug("属性:" + iAttr);
		    	// 属性ノードを取得する。
		    	Node attr = attribute.item(iAttr);
		    	// 子要素をキー、属性を値としてハッシュマップに格納する。
		        hash.put(attr.getNodeName(), attr.getNodeValue());
		    }
		    result.add(hash);
		    hash = new HashMap<String, Object>();

		    analyzeChildNode(childNodes);
	    }

	    log.debug("analyzeXML処理終了:result.size() = " + result.size());
		return result;
	  }

	/**
	 * メソッド名 : analyzeChildNode
	 * 機能概要 : （再帰処理）インプットの子ノードを解析する。
	 * @param childNodes
	 */
	private void analyzeChildNode(NodeList childNodes) {

		log.debug("analyzeChildNode処理開始");
		// 20121201 kkato@PROSITE add start
		//String CRLF = System.getProperty("line.separator");
		String tmpStr;
		// 20121201 kkato@PROSITE add end
		// メンバ数カウント
		int countMem;
		// 子要素格納用
		Node childNode;
		Node tmpNode;
		// 子要素名
		String childName;

		// childNodesの子要素分繰り返す。
		for (int i=0;(childNode = childNodes.item(i))!=null; i++) {
			log.debug("childNodes:" + i);

			// 子要素名を取得する。
			childName = childNode.getNodeName();
			countMem = 0;

			// 子要素名がmemberの数をカウントする。
			for(int j=0; (tmpNode=childNodes.item(j))!=null; j++) {
				log.debug("memberの数をカウント:" + j);
				if(childName.equals(MEMBER)) {
					log.debug("member:" + childName);
					countMem++;
				}
			}

			// 子要素の属性を取得する。
			NamedNodeMap attrMap = childNode.getAttributes();

			// 子要素の属性がnull出ない場合
			if (attrMap!=null) {
				log.debug("属性がnullでない");
				// 属性の数を取得
				int attrs = attrMap.getLength();

				// 属性の数分繰り返す。
			    for (int iAttr=0; iAttr<attrs; iAttr++) {
			    	log.debug("属性:" + iAttr);
			    	// 属性ノードを取得する。
			    	Node attr = attrMap.item(iAttr);
			    	// 子要素をキー、属性を値としてハッシュマップに格納する。
			        hash.put(attr.getNodeName(), attr.getNodeValue());
			    }
			}

			// 子要素の種別がELEMENT_NODE(要素)の場合
			if (childNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
				log.debug("要素種別:" + childNode.getNodeType());
				// 孫要素がnullでない場合
				if(childNode.getFirstChild() != null) {
					log.debug("孫要素がnullでない");
					// 孫要素の値がnullの場合
					if(childNode.getFirstChild().getNodeValue() == null) {
						log.debug("孫要素の値がnull");
						// ハッシュマップのキーに子要素名を設定する。
						name = childName;
					} else {
						log.debug("孫要素がnullでない");
						// 20121201 kkato@PROSITE mod start
						//改行のみは対象外
						tmpStr = (String)childNode.getFirstChild().getNodeValue().trim();
						if(tmpStr.length() == 0) {
						//if(childName.equals(MEMBER)) {
						} else if(childName.equals(MEMBER)) {
						// 子要素名がmemberの場合
						// 20121201 kkato@PROSITE mod end
							log.debug("子要素名がmember:childName = " + childName);
							// メンバーリストに子要素の値を追加する。
							memList.add(childNode.getFirstChild().getNodeValue());

							// member要素がカウントしたメンバ数と一致する場合
							if(memList.size() == countMem) {
								log.debug("member要素全取得:memList.size() = " + memList.size());
								// 子要素名をキー、メンバーリストを値としてハッシュマップに格納する。
								hash.put(name, memList);
								memList = new ArrayList<String>();
							}
						} else if(childName.equals("port")) {
							log.debug("子要素名がport:name = " + name);
							// 子要素名をキー、子要素の値を値としてハッシュマップに格納する。
							hash.put(name, childNode.getFirstChild().getNodeValue());
						} else {
							log.debug("子要素名がmember,protocolでない:childName = " + childName);
							// 子要素名をキー、子要素の値を値としてハッシュマップに格納する。
							hash.put(childName, childNode.getFirstChild().getNodeValue());
						}
					}
				}

				// 子要素が存在する場合
				if(childNode.hasChildNodes()) {
					log.debug("子要素が存在するため再帰処理を行う。");
					// 再帰呼び出し
					analyzeChildNode(childNode.getChildNodes());
				}
				// 子要素名がentry、result(アクセスキー生成)、msg(エラー時)の場合、
				// ハッシュマップを結果格納リストに追加する。
				if(childName.equals(ENTRY) | childName.equals(RESULT) | childName.equals(LINE)) {
					log.debug("結果格納:" +  i);
					result.add(hash);
					hash = new HashMap<String, Object>();
				}
			}
		}
		log.debug("analyzeChildNode処理終了:result.size() = " + result.size());
	  }

}