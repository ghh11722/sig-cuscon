/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名	: UpdatePA.java
 *
 * [変更履歴]
 * 日付 		  更新者				  内容
 * 2010/02/19	  komakiys		   初版作成
 * 2010/06/17	  hiroyasu		   コミット処理プロセス化対応
 * 2011/06/10	  inoue@prosite    Webフィルタの追加対応
 * 2011/09/24	  inoue@prosite    Paloコマンド投入エラー対応
 * 2013/09/01     kkato@PROSITE    アドレスOBJのFQDN対応
 * 2013/09/01     kkato@PROSITE    セキュリティルール有効/無効切替対応
 * 2014/01/10     kamide@prosite   エラー後にrunning-configのロード処理するように修正
 * 2014/03/19     kkato@prosite    running-configロード処理のリトライ追加
 * 2016/12/10	  T.Yamazaki@Plum  PANOS7対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.commitproc.common;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.commitproc.common.vo.Addresses;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.CommandPara;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.FilterInfo;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.LinkInfo;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.Schedules;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.SelectObjectList;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.SelectSystemConstantList;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.SelectUpdatePA;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.Services;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.UrlFilterCategories;
//2011/06/10 add start inoue@prosite
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.UrlFilters;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.util.PropertyUtil;
//2011/06/10 add end inoue@prosite
/**
 * クラス名 : UpdatePA
 * 機能概要 : PAの設定全削除、及びDBの指定世代にて<br>
 *			  PAの設定を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *			Created 2010/02/19
 *			新規作成
 *			2.0 T.Yamazaki@Plum Systems Inc.
 *			Updated 2016/12/10
 *			Paloalto V7対応
 * @see
 */
public class UpdatePA {

	// コマンドタイムアウト時間
	private int timeout = 0;
	// 改行(コマンド付加用)
	private String CHANGE_LINE ="\n";
	// ダブルクォーテーション(コマンド付加用)
	private String QUOT ="\"";
	// コンフィグモードコマンド
	private static final String CONFIG_COM =
		PropertyUtil.getProperty("fwsetting.common.configure");
	// コマンドエラー表示
	private static final String COM_ERROR =
		PropertyUtil.getProperty("fwsetting.common.commanderror");
	// 設定/削除コマンドエラー確認(正規表現)
	private static final String CHECK_COM =
		PropertyUtil.getProperty("fwsetting.common.checkset");
	// 設定/削除コマンドエラー確認箇所
	private static final int CHECK_NUM =Integer.parseInt(
				PropertyUtil.getProperty("fwsetting.common.checksetnum"));
	// ポリシー/オブジェクト削除コマンド
	private static final String DEL_COM =
		PropertyUtil.getProperty("fwsetting.common.del");
	// ポリシー/オブジェクト設定コマンド
	private static final String SET_COM =
		PropertyUtil.getProperty("fwsetting.common.set");
	// 日時フォーマット
	private static final String DATE_FORMAT =
		PropertyUtil.getProperty("fwsetting.common.dateformat");
	// コミット応答確認(正規表現)
	private static final String COMMIT_CHECK =
		PropertyUtil.getProperty("fwsetting.common.commitcheck");
	// コミットコマンドエラー確認対象場所
	private static final int COMMIT_CHECKNUM = Integer.parseInt(
		PropertyUtil.getProperty("fwsetting.common.commitchecknum"));
	// コマンド待ち時間
	private static int WAIT_COMMAND = 0;
	// コミット成功メッセージ
	private static final String COMMIT_SUCCESS =
		PropertyUtil.getProperty("fwsetting.common.commitsuccess");
	// deleteコマンド実行数
	private static final int DEL_COM_NUM =Integer.parseInt(
				PropertyUtil.getProperty("fwsetting.common.commitDelNum"));
	// AllowLogForwardingProfile名
	private static final String ALLOW_LOG_FORWARD_PROFILE =
		PropertyUtil.getProperty("fwsetting.common.allowlogprofile");
	// DenyLogForwardingProfile名
	private static final String DENY_LOG_FORWARD_PROFILE =
		PropertyUtil.getProperty("fwsetting.common.denylogprofile");

	// アドレスコマンド
	private static final String ADDRESS = " address ";
	// アドレスグループコマンド
	private static final String ADDRESS_GRP = " address-group ";
	// アプリケーションフィルタコマンド
	private static final String APPLI_FIL = " application-filter ";
	// アプリケーショングループコマンド
	private static final String APPLI_GRP = " application-group ";
	// サービスコマンド
	private static final String SERVICE = " service ";
	// サービスグループコマンド
	private static final String SERVICE_GRP = " service-group ";
	// スケジュールコマンド
	private static final String SCHEDULE = " schedule ";
	// コマンド用ブランク
	private static final String SP = " ";
	private static final String DESCRIPTION = " description ";

	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.commitproc.common.UpdatePA");
	// メッセージクラス
	private static MessageAccessor messageAccessor = null;

//2011/06/10 add start inoue@prosite
	// Webフィルタコマンド
	private static final String URLFILTER = " profiles url-filtering ";
	private static final String URLFILTER_ACTION_ALERT = "alert";
	private static final String URLFILTER_ACTION_ALLOW = "allow";
	private static final String URLFILTER_ACTION_BLOCK = "block";
	private static final String URLFILTER_ACTION_CONTINUE = "continue";
	private static final String URLFILTER_ACTION_OVERRIDE = "override";
	private static final String SPACE = " ";
	private static final String URLFILTER_ACTION_ON_LiCENSE_EXPIRATION = " license-expired ";
	private static final String URLFILTER_DYNAMIC_URL = " dynamic-url ";
//2011/06/10 add end inoue@prosite

//2011/09/24 add start inoue@prosite
	// コマンドinvalid
	private static final String COMMAND_INVALID =
		PropertyUtil.getProperty("fwsetting.common.command_invalid");
	// コマンドunknown
	private static final String COMMAND_UNKNOWN =
		PropertyUtil.getProperty("fwsetting.common.command_unknown");
	// コマンドserver error
	private static final String COMMAND_SERVER_ERROR =
		PropertyUtil.getProperty("fwsetting.common.command_server_error");
	// running-configのロードコマンド
	private static final String LOAD_RUNNING_CONFIG =
		PropertyUtil.getProperty("fwsetting.common.running_config");
	// running-configのロード成功メッセージ
	private static final String LOAD_RUNNING_CONFIG_SUCCESS =
		PropertyUtil.getProperty("fwsetting.common.running_configsuccess");
//2011/09/24 add end inoue@prosite
//2014/03/19 add start kkato@prosite
	// running-configのロード(REVERT)リトライ回数
	private static final int REVERT_RETRY_CNT = Integer.parseInt(
		PropertyUtil.getProperty("fwsetting.common.revert_retry_cnt"));
	// running-configのロード(REVERT)リトライ待機時間(ミリ秒)
	private static final int REVERT_RETRY_WAIT_TIME = Integer.parseInt(
		PropertyUtil.getProperty("fwsetting.common.revert_retry_wait_time"));
//2014/03/19 add end   kkato@prosite

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 * @param cusconUVO
	 * @param queryDAO
	 */
	public UpdatePA() {
		log.debug("UpdatePAコンストラクタ1処理開始");
		log.debug("UpdatePAコンストラクタ1処理終了");
	}

	/**
	 * メソッド名 : updatePAProcess
	 * 機能概要 : 現在のPAの設定を全て削除し、<br>
	 *			  generationNoの世代の情報にてPAを更新する。
	 * @param policyList PA設定対象ポリシー一覧情報
	 * @param vsysId VsysID
	 * @param generationNo 世代番号
	 * @param loginId PAログインID
	 * @param password PAパスワード
	 * @param queryDAO QueryDAO
	 * @param uvo CusconUVO
	 * @param msgAcc メッセージアクセッサ
	 * @return commit_flg commit判定フラグ
	 * @throws Exception
	 */
	public void updatePAProcess(ResultJudgeData resultJudge, List<SelectPolicyList> policyList,
			String vsysId, int generationNo, String loginId, String password,
			QueryDAO queryDAO, CusconUVO uvo, MessageAccessor msgAcc) throws Exception{

		log.debug("updatePAProcess処理開始");
		messageAccessor = msgAcc;

		// PA接続情報を取得する。
		SelectSystemConstantList connectionInfo =
									getConnectionInfo(
									queryDAO, uvo, loginId, password);
		// 参照・削除コマンド用セッション生成
		CommandPara delcompara = new CommandPara();
		delcompara.setConnectionInfo(connectionInfo);
		delcompara.setUvo(uvo);
		// コマンドタイムアウト時間
		timeout = connectionInfo.getTimeout() * 60 * 1000;
		delcompara.setTimeout(timeout);
		delcompara.setWaitCommand(WAIT_COMMAND);

		//2014/01/10 start kamide@prosite
		CommandExecute delcommand = null;
		try {
			delcommand = new CommandExecute(delcompara, msgAcc);
		} catch (Exception e) {
			// PA接続エラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FZ011035", objectgArray, uvo));
			throw e;
		}
		//2014/01/10 end kamide@prosite

		// 設定ファイルより読み出す項目のログ出力
		if (log.isDebugEnabled()) {
			log.debug("---- 設定値 ------------------------------------------------");
			log.debug("コンフィグモードコマンド:" + CONFIG_COM);
			log.debug("コマンドエラー表示:" + COM_ERROR);
			log.debug("設定/削除コマンドエラー確認(正規表現):" + CHECK_COM);
			log.debug("設定/削除コマンドエラー確認箇所:" + CHECK_NUM);
			log.debug("ポリシー/オブジェクト削除コマンド:" + DEL_COM);
			log.debug("ポリシー/オブジェクト設定コマンド:" + SET_COM);
			log.debug("日時フォーマット:" + DATE_FORMAT);
			log.debug("コミット応答確認(正規表現):" + COMMIT_CHECK);
			log.debug("コミットコマンドエラー確認対象場所:" + COMMIT_CHECKNUM);
			log.debug("コミット成功メッセージ:" + COMMIT_SUCCESS);
			log.debug("------------------------------------------------------------");
		}

		// 削除用オブジェクト名リスト取得処理
		List<List<SelectUpdatePA>> delObjNameList =
									getObjectNameList(vsysId, queryDAO, uvo);

		//2014/12/05 thasegawa@prosite add start
		CommandExecute command = null;
		CommandPara compara = null;
		//2014/12/05 thasegawa@prosite add end

		//2014/01/10 start kamide@prosite
		try {
			// PA設定削除処理
			deletePA(delObjNameList, delcommand, delcompara, uvo);
			delcommand.disconnect(delcompara);

			// PA関連処理を全てtry~catchの対象に変更
			/* 2014/12/05 thasegawa@prosite del
			} catch (Exception e) {
				// 途中で接続が切れたなどの理由で例外が飛んできた場合
				Object[] objectArray = {e.getClass().getName()+": "+e.getMessage()};
				log.error(messageAccessor.getMessage("FK160019", objectArray, uvo));
				//2014/03/19 mod start kkato@prosite
				//REVERT(running-configロード)するように切断処理にtry-catch追加
				try {
					// ユーザでの接続を一旦、切断
					delcommand.disconnect(delcompara);
				} catch (Exception e2) {
					log.debug("delcommand.disconnect:例外発生");
				}
				//2014/03/19 mod end   kkato@prosite
				// configureモードの場合、running-configのロードコマンド実行
				log.info("Commandの投入に失敗した為、running-configに戻します。");
				String loadResult = executeLoad_runnning_config(queryDAO, uvo, msgAcc);
				resultJudge.setResultFlg(3);
				log.debug("updatePAProcess処理終了:flg=3");
				return;
			}
			 */

		//2014/01/10 end kamide@prosite

			// 設定用オブジェクトリスト取得処理
			SelectUpdatePA setObjNameList =
										getObjectList(vsysId, generationNo, queryDAO, uvo);

			// 設定コマンド用セッション作成
			//CommandPara compara = new CommandPara();
			compara = new CommandPara();
			compara.setConnectionInfo(connectionInfo);
			compara.setUvo(uvo);
			compara.setTimeout(timeout);
			compara.setWaitCommand(WAIT_COMMAND);
			//CommandExecute command = new CommandExecute(compara, msgAcc);
			command = new CommandExecute(compara, msgAcc);

			// PA設定処理
			//2011/09/24 rep start inoue@prosite
			//registPA(setObjNameList, policyList, command, compara, uvo);
			//2014/01/10 start kamide@prosite
			boolean rst = false;
			try {
				rst = registPA(setObjNameList, policyList, command, compara, uvo);
			} catch (Exception e) {
				// 途中で接続が切れたなどの理由で例外が飛んできた場合
				Object[] objectArray = {e.getClass().getName()+": "+e.getMessage()};
				log.error(messageAccessor.getMessage("FK160019", objectArray, uvo));
				rst = false;
			}
			//2014/01/10 end kamide@prosite
			if (!rst) {
				//2014/03/19 mod start kkato@prosite
				//REVERT(running-configロード)するように切断処理にtry-catch追加
				try {
					// ユーザでの接続を一旦、切断
					command.disconnect(compara);
				} catch (Exception e2) {
					log.debug("command.disconnect:例外発生");
				}
				//2014/03/19 mod end   kkato@prosite
				// configureモードの場合、running-configのロードコマンド実行
				log.info("Commandの投入に失敗した為、running-configに戻します。");
				String loadResult = executeLoad_runnning_config(queryDAO, uvo, msgAcc);
				resultJudge.setResultFlg(3);
				log.debug("updatePAProcess処理終了:flg=3");
				return;
			}
			//2011/09/24 rep end inoue@prosite

			//2014/01/10 start kamide@prosite
			String commitResult;

			/* 2014/12/05 thasegawa@prosite del
			try {
			*/

			// コミットコマンド実行
			commitResult = executeCommit(command, compara, uvo);
			command.disconnect(compara);

			/* 2014/12/05 thasegawa@prosite del
			} catch (Exception e) {
				// 途中で接続が切れたなどの理由で例外が飛んできた場合
				Object[] objectArray = {e.getClass().getName()+": "+e.getMessage()};
				log.error(messageAccessor.getMessage("FK160019", objectArray, uvo));
				//2014/03/19 mod start kkato@prosite
				//REVERT(running-configロード)するように切断処理にtry-catch追加
				try {
					// ユーザでの接続を一旦、切断
					command.disconnect(compara);
				} catch (Exception e2) {
					log.debug("command.disconnect:例外発生");
				}
				//2014/03/19 mod end   kkato@prosite
				// configureモードの場合、running-configのロードコマンド実行
				log.info("Commandの投入に失敗した為、running-configに戻します。");
				String loadResult = executeLoad_runnning_config(queryDAO, uvo, msgAcc);
				resultJudge.setResultFlg(3);
				log.debug("updatePAProcess処理終了:flg=3");
				return;
			}
			 */

			//2014/01/10 end kamide@prosite

//	  	String[] commitResultList = commitResult.split(COMMIT_CHECK);
//	  	if (commitResultList.length < COMMIT_CHECKNUM + 1) {
//			// コマンド実行エラー
//			Object[] objectgArray = {commitResult};
//			log.error(messageAccessor.getMessage("EZ011001", objectgArray, uvo));
//			throw new Exception();
//	  	}
//
//	  	// コミットが失敗の場合
//	  	if(commitResultList[COMMIT_CHECKNUM].indexOf(COMMIT_SUCCESS) == -1) {
//			// コミット失敗
//			Object[] objectgArray = {commitResultList[COMMIT_CHECKNUM]};
//			log.error(messageAccessor.getMessage("EZ011002", objectgArray, uvo));
//			resultJudge.setResultFlg(3);
//			log.debug("updatePAProcess処理終了:flg=3");
//
//			return;
//	  	}

			// コミットが失敗の場合
			if(commitResult.indexOf(COMMIT_SUCCESS) == -1) {
				// コミット失敗
				Object[] objectgArray = {commitResult};
				log.error(messageAccessor.getMessage("EZ011002", objectgArray, uvo));
				//2011/09/24 add start inoue@prosite
				// configureモードの場合、running-configのロードコマンド実行
				log.info("Commandの投入に失敗した為、running-configに戻します。");
				String loadResult = executeLoad_runnning_config(queryDAO, uvo, msgAcc);
				//2011/09/24 add end inoue@prosite
				resultJudge.setResultFlg(3);
				log.debug("updatePAProcess処理終了:flg=3");

				return;
			}

		// 2014/12/05 thasegawa@prosite add start
		} catch (Exception e) {
			// 途中で接続が切れたなどの理由で例外が飛んできた場合
			Object[] objectArray = {e.getClass().getName()+": "+e.getMessage()};
			log.error(messageAccessor.getMessage("FK160019", objectArray, uvo));
			//REVERT(running-configロード)するように切断処理にtry-catch追加
			try {
				// ユーザでの接続を一旦、切断
				delcommand.disconnect(delcompara);
			} catch (Exception e2) {
				log.debug("delcommand.disconnect:例外発生");
			}
			try {
				// ユーザでの接続を一旦、切断
				command.disconnect(compara);
			} catch (Exception e2) {
				log.debug("command.disconnect:例外発生");
			}
			// configureモードの場合、running-configのロードコマンド実行
			log.info("Commandの投入に失敗した為、running-configに戻します。");
			String loadResult = executeLoad_runnning_config(queryDAO, uvo, msgAcc);
			resultJudge.setResultFlg(3);
			log.debug("updatePAProcess処理終了:flg=3");
			return;
		}
		// 2014/12/05 thasegawa@prosite add end

		Date date = new Date();

		// 時刻フォーマットを定義する。
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);

		// 現在時刻を返却する。
		dateFormat.format(date);

		// 結果を格納する。
		resultJudge.setResultFlg(0);
		// 現在時刻を返却する。
		resultJudge.setResultData(dateFormat.format(date));

		log.debug("updatePAProcess処理終了:flg=2,updateTime = " +
													dateFormat.format(date));
		return;
	}

	/**
	 * メソッド名 : getConnectionInfo
	 * 機能概要 : DBよりPA接続情報を取得し、返却する。
	 * @param loginId PAログインID
	 * @param password PAパスワード
	 * @return SelectSystemConstantList PA接続情報
	 * @throws Exception
	 */
	private SelectSystemConstantList getConnectionInfo(QueryDAO queryDAO, CusconUVO uvo,
									String loginId, String password) throws Exception {
		log.debug("getConnectionInfo処理開始: loginId = " +
									loginId + ",password = " + password);

		// PAのホスト、ユーザID、パスワードを取得する。
		// 復号化鍵
		String key = "kddi-secgw";
		// PA接続情報
		List<SelectSystemConstantList> connectionList = null;

		try {
			// PA接続情報を取得する。
			connectionList =
				queryDAO.executeForObjectList("CommonM_SystemConstant-1", key);
		} catch(Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FZ011033", objectgArray, uvo));
			throw e;
		}

		// 接続情報を戻り値に格納する。
		connectionList.get(0).setLoginId(loginId);
		connectionList.get(0).setPassword(password);
		WAIT_COMMAND = connectionList.get(0).getCmdWait();
		log.debug("getConnectionInfo処理終了");
		return connectionList.get(0);
	}

	/**
	 * メソッド名 : getObjectNameList
	 * 機能概要 : 各オブジェクトテーブルをvsysIdで検索し、<br>
	 *			  オブジェクト名リストを作成し、リストを返却する。
	 * @param vsysId VsysID
	 * @return List<CODE><</CODE>List<CODE><</CODE>SelectUpdatePA<CODE>></CODE><CODE>></CODE> 各オブジェクト名リストのリスト
	 * @throws Exception
	 */
	private List<List<SelectUpdatePA>> getObjectNameList(
			String vsysId, QueryDAO queryDAO, CusconUVO uvo) throws Exception {
		log.debug("getObjectNameList処理開始 : vsysId = " + vsysId);
		List<List<SelectUpdatePA>> objNameList =
						new ArrayList<List<SelectUpdatePA>>();

		// 基本検索条件クラスにパラメータを設定する。
		InputBase base = new InputBase();
		base.setGenerationNo(0);
		base.setVsysId(vsysId);

		// アドレスグループオブジェクト
		List<SelectUpdatePA> adrGrpNameList = null;
		// アプリケーショングループオブジェクト
		List<SelectUpdatePA> appGrpNameList = null;
		// サービスグループオブジェクト
		List<SelectUpdatePA> srvGrpNameList = null;
		// アドレスオブジェクト
		List<SelectUpdatePA> adrNameList = null;
		// アプリケーションフィルタオブジェクト
		List<SelectUpdatePA> appFilNameList = null;
		// サービスオブジェクト
		List<SelectUpdatePA> srvNameList = null;
		// スケジュールオブジェクト
		List<SelectUpdatePA> scheNameList = null;
//2011.06.10 add start inoue@prosite
		// Webフィルタオブジェクト
		List<SelectUpdatePA> urlFilterNameList = null;
//2011.06.10 add end inoue@prosite

		try {
			// アドレスグループオブジェクトを取得する。
			adrGrpNameList = queryDAO.executeForObjectList(
										"CommonT_AddressGroups-2", base);

			// アプリケーショングループオブジェクト名を取得する。
			appGrpNameList = queryDAO.executeForObjectList(
									"CommonT_ApplicationGroups-2", base);

			// サービスグループオブジェクト名を取得する。
			srvGrpNameList = queryDAO.executeForObjectList(
										"CommonT_ServicesGroups-2", base);

			// アドレスオブジェクトを取得する。
			adrNameList = queryDAO.executeForObjectList(
											"CommonT_Addresses-2", base);

			// アプリケーションフィルタオブジェクト名を取得する。
			appFilNameList = queryDAO.executeForObjectList(
									"CommonT_ApplicationFilters-2", base);

			// サービスオブジェクト名を取得する。
			srvNameList = queryDAO.executeForObjectList(
												"CommonT_Services-2", base);

			// スケジュールを取得する。
			scheNameList = queryDAO.executeForObjectList(
											"CommonT_Schedules-2", base);

//2011.06.10 add start inoue@prosite
			// Webフィルタオブジェクトを取得する。
			urlFilterNameList = queryDAO.executeForObjectList(
											"GetObjectOldName-7", base);
//2011.06.10 add end inoue@prosite
		} catch(Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FZ011034", objectgArray, uvo));
			throw e;
		}

		// アドレスグループオブジェクトを追加する。
		objNameList.add(adrGrpNameList);
		// アプリケーショングループオブジェクトを追加する。
		objNameList.add(appGrpNameList);
		// サービスグループオブジェクトを追加する。
		objNameList.add(srvGrpNameList);
		// アドレスオブジェクトを追加する。
		objNameList.add(adrNameList);
		// アプリケーションフィルタオブジェクトを追加する。
		objNameList.add(appFilNameList);
		// サービスオブジェクトを追加する。
		objNameList.add(srvNameList);
		// スケジュールを追加する。
		objNameList.add(scheNameList);
//2011.06.10 add start inoue@prosite
		// Webフィルタオブジェクトを追加する。
		objNameList.add(urlFilterNameList);
//2011.06.10 add end inoue@prosite

		log.debug("getObjectNameList処理終了");
		return objNameList;
	}


	/**
	 * メソッド名 : deletePA
	 * 機能概要 :現在のPAの設定を全て削除する。
	 * @param objNameList オブジェクト名リスト
	 * @throws Exception
	 */
	private void deletePA(
				List<List<SelectUpdatePA>> objNameList,
				CommandExecute command,
				CommandPara compara,
				CusconUVO uvo) throws Exception {

		log.debug("deletePA処理開始:objNameList.size() = " + objNameList.size());

		// ポリシー削除コマンド
		String DEL_POLICY = " rulebase security ";

		// 設定コマンド生成用
		String delComConnect;
		// コマンドリスト
		List<String> comList = new ArrayList<String>();


		/*********************************
		 *	ポリシー削除処理
		 *********************************/
		 // コンフィグモードコマンド設定
		comList.add(CONFIG_COM);

		// ポリシー全削除コマンド設定
		delComConnect = DEL_COM  + DEL_POLICY + CHANGE_LINE;
		comList.add(delComConnect);

		// ポリシー削除コマンドログ出力
		Object[] objectgArray = {delComConnect};
		log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));

		// コマンドを実行する。
		command.getCommandResponce(compara, comList);

		/*********************************
		 *	オブジェクト削除処理
		 *********************************/
		// オブジェクトの削除
		deleteObject(objNameList, command, compara, uvo);

		log.debug("deletePA処理終了3");
		return;
	}


	/**
	 * メソッド名 : deleteObject
	 * 機能概要 : 対象オブジェクトの削除を行う。
	 * @param objNameList 削除対象オブジェクト名リスト
	 * @throws Exception
	 */
	private void deleteObject(
				List<List<SelectUpdatePA>> objNameList,
				CommandExecute command,
				CommandPara compara,
				CusconUVO uvo) throws Exception {

		log.debug("deleteObject処理開始");
		// 設定コマンド生成用
		String delComConnect;
		// 削除コマンド実行用
		String delCom;

		// コマンドリスト
		List<String> comList = new ArrayList<String>();
		// 実行コマンドリスト
		List<String> exeComList = new ArrayList<String>();

		/*
		 	objFlg:
			0：アドレスグループオブジェクト
			1：アプリケーショングループオブジェクト
			2：サービスグループオブジェクト
			3：アドレスオブジェクト
			4：アプリケーションフィルタオブジェクト
			5：サービスオブジェクト
			6：スケジュールオブジェクト
//2011/06/10 add start inoue@prosite
			7：Webフィルタオブジェクト
//2011/06/10 add end inoue@prosite
		*/
		for(int objFlg=0; objFlg<objNameList.size(); objFlg++) {
			log.debug("各オブジェクトの処理 - " + objFlg);
			List<SelectUpdatePA> processList = objNameList.get(objFlg);

			// アドレスグループの場合
			if(objFlg == 0) {
				log.debug("アドレスグループ：" + objFlg);
				delComConnect = DEL_COM + ADDRESS_GRP;

			// アプリケーショングループの場合
			} else if(objFlg == 1) {
				log.debug("アプリケーショングループ：" + objFlg);
				delComConnect = DEL_COM + APPLI_GRP;

			// サービスグループの場合
			} else if(objFlg == 2){
				log.debug("サービスグループ：" + objFlg);
				delComConnect = DEL_COM + SERVICE_GRP;

			// アドレスの場合
			} else if(objFlg == 3) {
				log.debug("アドレス：" + objFlg);
				delComConnect = DEL_COM + ADDRESS;

			// アプリケーションフィルタの場合
			} else if(objFlg == 4){
				log.debug("アプリケーションフィルタ：" + objFlg);
				delComConnect = DEL_COM + APPLI_FIL;

			// サービスの場合
			} else if(objFlg == 5){
				log.debug("サービス：" + objFlg);
				delComConnect = DEL_COM + SERVICE;

//2011/06/10 rep start inoue@prosite
//			// スケジュールの場合
//			} else {
//				log.debug("スケジュール：" + objFlg);
//				delComConnect =DEL_COM + SCHEDULE;
//
//			}
			// スケジュールの場合
			} else if(objFlg == 6){
				log.debug("スケジュール：" + objFlg);
				delComConnect =DEL_COM + SCHEDULE;
			// Webフィルタの場合
			} else {
				log.debug("Webフィルタ：" + objFlg);
				delComConnect =DEL_COM + URLFILTER;

			}
//2011/06/10 rep end inoue@prosite

			log.debug("=====================================================================");
			for(int n=0; n<processList.size(); n++) {
				log.debug("  " + n + " : " + processList.get(n).getName());
			}
			log.debug("=====================================================================");

			if(objFlg==0 || objFlg==1 || objFlg==2) {
				log.debug("オブジェクト削除:" + objFlg);
				for(int j=0; j<processList.size(); j++) {
					log.debug("削除処理中:" + j);

					for(int k=0; k<processList.size(); k++) {
						log.debug("グループオブジェクトコマンド生成:" + k);
						if(!processList.get(j).getName().equals(processList.get(k).getName())) {

							// UPDATED: 2016.11 by Plum Systems Inc.
							// PANOS7対応 -->
							// 削除対象の設定（対象グループによって変更）
							if(objFlg == 0){
								// アドレスグループの場合
								delCom = delComConnect + QUOT +
										processList.get(j).getName() + QUOT + " static " +
										QUOT + processList.get(k).getName() + QUOT;
							} else if(objFlg == 1 || objFlg == 2) {
								// アプリケーショングループ or サービスグループの場合
								delCom = delComConnect + QUOT +
										processList.get(j).getName() + QUOT + " members " +
										QUOT + processList.get(k).getName() + QUOT;
							} else {
								// その他の場合
								delCom = delComConnect + QUOT +
										processList.get(j).getName() + QUOT + " " +
										QUOT + processList.get(k).getName() + QUOT;
							}
							// --> PANOS7対応

							// ポリシー削除コマンドログ出力
							Object[] objectgArray = {delCom};
							log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));
							log.debug("ポリシー削除コマンド : " + delCom);

							delCom = delCom + CHANGE_LINE;
							comList.add(delCom);
						}
					}
				}
			}

			// オブジェクト名にて削除コマンド生成
			for(int j=0; j<processList.size(); j++) {
				// 削除対象の設定
				delCom = delComConnect + QUOT +
						processList.get(j).getName() + QUOT;

				// ポリシー削除コマンドログ出力
				Object[] objectgArray = {delCom};
				log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));
				log.debug("ポリシー削除コマンド : " + delCom);

				delCom = delCom + CHANGE_LINE;
				comList.add(delCom);
			}
		}

		// コンフィグモードコマンド設定
		exeComList.add(CONFIG_COM);
		// 削除数カウンター
		int delCounter = 0;

		// 削除コマンドを指定回数実行する。
		for(int i=0; i<comList.size(); i++) {
			log.debug("削除コマンド生成 : " + i);
			delCounter++ ;
			exeComList.add(comList.get(i));
			if(delCounter == DEL_COM_NUM) {
				log.debug("削除コマンド実行 : " + i);
				// コマンドを実行する。
				command.getCommandResponce(compara, exeComList);

				exeComList = new ArrayList<String>();
				// コンフィグモードコマンド設定
				exeComList.add(CONFIG_COM);
				delCounter = 0;
			}
		}

//2011/06/10 rep start inoue@prosite コメントロジック意味不明（多分configureのみの場合か？）
//		if(exeComList.size() != 2) {
//			log.debug("削除コマンド実行(最後)");
//			command.getCommandResponce(compara, exeComList);
//		}
		if(exeComList.size() != 1) {
			log.debug("削除コマンド実行(最後)");
			command.getCommandResponce(compara, exeComList);
		}
//2011/06/10 rep end inoue@prosite
		log.debug("deleteObject処理終了2:true");

		return;
	}

	/**
	 * メソッド名 : getObjectList
	 * 機能概要 :各オブジェクトテーブルをvsysId、generationNoで検索し、<br>
	 *			 各オブジェクトリストのリストを作成し、リストを返却する。
	 * @param vsysId VsysID
	 * @param generationNo 世代番号
	 * @return List<CODE><</CODE>List<CODE><</CODE>Object<CODE>></CODE><CODE>></CODE> 各オブジェクトリストのリスト
	 * @throws Exception
	 */
	private SelectUpdatePA getObjectList(String vsysId, int generationNo,
			QueryDAO queryDAO, CusconUVO uvo) throws Exception{

		log.debug("getObjectList処理開始, vsysId = " + vsysId +
										", generationNo = " + generationNo);
		// 結果を格納するリスト
		SelectUpdatePA objList = new SelectUpdatePA();

		// 基本検索条件クラスにパラメータを設定する。
		InputBase base = new InputBase();
		base.setGenerationNo(generationNo);
		base.setVsysId(vsysId);
		base.setModFlg(3);

		// アドレスオブジェクト情報リスト
		List<Addresses> adrNameList = null;
		// アドレスグループオブジェクト情報リスト
		List<LinkInfo> adrGrpNameList = null;
		// アプリケーションフィルタオブジェクト情報リスト
		List<FilterInfo> appFilNameList = null;
		// アプリケーショングループオブジェクト情報リスト
		List<LinkInfo> appGrpNameList = null;
		// サービスオブジェクト情報リスト
		List<Services> srvNameList = null;
		// サービスグループオブジェクト情報リスト
		List<LinkInfo> srvGrpNameList = null;
		// スケジュール情報リスト
		List<Schedules> scheNameList = null;
//2011/06/10 add start inoue@prosite
		// Webフィルタ情報リスト
		List<UrlFilters> urlFilterNameList = null;
//2011/06/10 add end inoue@prosite

		try {
			// アドレスオブジェクト情報リストを取得する。
			adrNameList = queryDAO.executeForObjectList(
											"CommonT_Addresses-5", base);

			// アドレスグループオブジェクト情報リストを取得する。
			adrGrpNameList = queryDAO.executeForObjectList(
											"AddressGroupsBLogic-1", base);

			// アプリケーションフィルタオブジェクト情報リストを取得する。
			appFilNameList = queryDAO.executeForObjectList(
										"ApplicationFiltersBLogic-1", base);

			// アプリケーショングループオブジェクト情報リストを取得する。
			appGrpNameList = queryDAO.executeForObjectList(
										"ApplicationGroupsBLogic-1", base);

			// サービスオブジェクト情報リストを取得する。
			srvNameList = queryDAO.executeForObjectList(
												"CommonT_Services-6", base);

			// サービスグループオブジェクト情報リストを取得する。
			srvGrpNameList = queryDAO.executeForObjectList(
											"ServiceGroupsBLogic-15", base);

			// スケジュール情報リストを取得する。
			scheNameList = queryDAO.executeForObjectList(
												"CommonT_Schedules-4", base);
//2011/06/10 add start inoue@prosite
			// Webフィルタフラグ=1(ON)時の場合に限り、Webフィルタオブジェクトを取得
			if( uvo.getUrlFilteringFlg()==1 ){
				// Webフィルタオブジェクト情報リストを取得する。
				urlFilterNameList = queryDAO.executeForObjectList(
													"GetT_UrlFilters", base);
				// Webフィルタカテゴリ情報リストを取得する。
				for (int i=0; i<urlFilterNameList.size(); i++){
					base.setUrlfilter_seqno(urlFilterNameList.get(i).getSeqNo());
					List<UrlFilterCategories> urlFilterCategoryList =
						queryDAO.executeForObjectList("GetT_UrlFilter_Categories", base);
					urlFilterNameList.get(i).setUrlfiltercategories(urlFilterCategoryList);
				}
			}
//2011/06/10 add end inoue@prosite
		} catch(Exception e) {

			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FZ011034", objectgArray, uvo));
			throw e;
		}


		// アドレスオブジェクト情報リストを追加する。
		objList.setAddress(adrNameList);
		// アドレスグループオブジェクト情報リストを追加する。
		objList.setAddressGroups(adrGrpNameList);
		// アプリケーションフィルタオブジェクト情報リストを追加する。
		objList.setApplicationFilters(appFilNameList);
		// アプリケーショングループオブジェクト情報リストを追加する。
		objList.setApplicationGroups(appGrpNameList);
		// サービスオブジェクト情報リストを追加する。
		objList.setServices(srvNameList);
		// サービスグループオブジェクト情報リストを追加する。
		objList.setServiceGroups(srvGrpNameList);
		// スケジュール情報リストを追加する。
		objList.setSchedules(scheNameList);
//2011/06/10 add start inoue@prosite
		// Webフィルタ情報リストを追加する。
		objList.setUrlFilters(urlFilterNameList);
//2011/06/10 add end inoue@prosite

		log.debug("getObjectList処理終了");
		return objList;
	}


	/**
	 * メソッド名 : registPA
	 * 機能概要 : objList、policyListの情報をPAに設定する。
	 * @param objList 設定対象オブジェクト情報リスト
	 * @param policyList 設定対象ポリシー情報リスト
	 * @throws Exception
	 */
	private boolean registPA(SelectUpdatePA objList,
					List<SelectPolicyList> policyList,
					CommandExecute command, CommandPara compara, CusconUVO uvo) throws Exception {

		log.debug("registPA処理開始:policyList.size() = " + policyList.size());
		// カテゴリコマンド
		String CATEGORY = " category ";
		// サブカテゴリコマンド
		String SUBCATEGORY = " subcategory ";
		// テクノロジコマンド
		String TECHNOLOGY = " technology ";
		// 脅威度コマンド
		String RISK = " risk ";
		// プロトコルコマンド
		String PROTOCOL = " protocol ";
		// ポートコマンド
		String PORT = " port ";
		// dailyコマンド
		String DAILY = " recurring daily ";
		// weeklyコマンド
		String WEEKLY = " recurring weekly ";
		// non-recurringコマンド
		String NON_RECURRING = " non-recurring ";
		// sundayコマンド
		String SUNDAY = " sunday ";
		// mondayコマンド
		String MONDAY = " monday ";
		// tuesdayコマンド
		String TUESDAY = " tuesday ";
		// wednesdayコマンド
		String WEDNESDAY = " wednesday ";
		// thursdayコマンド
		String THURSDAY = " thursday ";
		// fridayコマンド
		String FRIDAY = " friday ";
		// saturdayコマンド
		String SATURDAY = " saturday ";
		// -コマンド
		String HYPHEN = "-";
		// @コマンド
		String AT = "@";
		// セキュリティポリシーコマンド
		String SECURITY = " rulebase security rules ";
		// fromコマンド
		String FROM = " from ";
		// toコマンド
		String TO = " to ";
		// sourceコマンド
		String SOURCE = " source ";
		// destinationコマンド
		String DESTINATION = " destination ";
		// applicationコマンド
		String APPLICATION = " application ";
		// actionコマンド
		String ACTION = " action ";
		// profile-settingコマンド
		String PROFILE = " profile-setting profiles vulnerability ";
		// 脆弱性の保護フラグ
		String DEFAULT = "default";
		// スケジュール
		String NONE = "none";
		// ログエンド
		String LOG_END = " log-end yes ";
		// ログフォワード設定
		String LOG_SETTING = " log-setting ";
		// Action_Allow
		String ACTION_ALLOW = "allow";
		// Action_Deny
		String ACTION_DENY = "deny";
//2011/06/10 add start inoue@prosite
		String URLFILTER_PROFILE = " profile-setting profiles url-filtering ";
		String VIRUS_PROFILE = " profile-setting profiles virus ";
		String SPYWARE_PROFILE = " profile-setting profiles spyware ";
		String ALLOW_LIST = " allow-list ";
		String BLOCK_LIST = " block-list ";
//2011/06/10 add end inoue@prosite
		// 20130901 kkato@PROSITE add start
		String DISABLE_PROFILE = " disabled ";
		// 20130901 kkato@PROSITE add end

		// 設定コマンド生成用
		String setComConnect;
		// コマンドリスト
		List<String> comList;
		// アドレスタイプ
		String type;
		// コマンドレスポンス
		String response;
//2011/09/24 add start inoue@prosite
		Boolean bRst;
//2011/09/24 add end inoue@prosite
		/**************************************************
		 *	アドレスオブジェクト情報リスト
		 **************************************************/
		List<Addresses> addressList = objList.getAddress();

		// アドレスオブジェクト設定処理
		for(int i=0; i<addressList.size(); i++) {

			log.debug("アドレス設定:" + i);
			// コマンドリスト
			comList = new ArrayList<String>();
			 // コンフィグモードコマンド設定
			comList.add(CONFIG_COM);

			// アドレス種別により設定するコマンドを判定する。
			if(addressList.get(i).getType().equals(CusconConst.ADDRESS_IP)){
				log.debug("アドレス種別:IPアドレス");
				type = CusconConst.ADDRESS_IP;
// 20130901 kkato@PROSITE add start
			} else if(addressList.get(i).getType().equals(CusconConst.ADDRESS_FQDN)){
				log.debug("アドレス種別:FQDN");
				type = CusconConst.ADDRESS_FQDN;
// 20130901 kkato@PROSITE add end
			} else {
				log.debug("アドレス種別:範囲");
				type = CusconConst.ADDRESS_RANGE;
			}
			// 設定対象オブジェクトの設定
			setComConnect = SET_COM + ADDRESS + QUOT +
				addressList.get(i).getName() + QUOT + SP  + type  + SP + QUOT +
						addressList.get(i).getAddress() + QUOT + CHANGE_LINE;
			comList.add(setComConnect);

			// 設定コマンドログ出力
			Object[] objectgArray = {setComConnect};
			log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));

			// コマンドを実行し、レスポンスを取得する。
//2011/09/24 rep start inoue@prosite
			//command.getCommandResponce(compara, comList);
			response = command.getCommandResponce(compara, comList);
			// コマンドが失敗の場合( invalid or unknown or server error)
			if(response.indexOf(COMMAND_INVALID) != -1 ||
				response.indexOf(COMMAND_UNKNOWN) != -1 ||
				response.indexOf(COMMAND_SERVER_ERROR) != -1) {
				Object[] objectRes = {response};
				log.fatal(messageAccessor.getMessage("FZ011041", objectRes, uvo));
				return false;
			}
//2011/09/24 rep end inoue@prosite

		}

		/**************************************************
		 *	アドレスグループオブジェクト情報リスト
		 **************************************************/
		List<LinkInfo> addressGrpList = objList.getAddressGroups();
		// アドレスグループオブジェクト設定処理
//2011/09/24 rep start inoue@prosite
		//setGroupOject(addressGrpList, command, compara, uvo, 1);
		bRst = setGroupOject(addressGrpList, command, compara, uvo, 1);
		if (!bRst){
			return false;
		}
//2011/09/24 rep end inoue@prosite

		/**************************************************
		 *	アプリケーションフィルタオブジェクト情報リスト
		 **************************************************/
		List<FilterInfo> appliFilList = objList.getApplicationFilters();

		// アプリケーションフィルタオブジェクト設定処理
		for(int i=0; i<appliFilList.size(); i++) {
			log.debug("アプリケーションフィルタ設定:" + i);
			// コマンドリスト
			comList = new ArrayList<String>();
			 // コンフィグモードコマンド設定
			comList.add(CONFIG_COM);

			// 設定対象オブジェクトの設定
			setComConnect = SET_COM + APPLI_FIL + QUOT +
									appliFilList.get(i).getName() + QUOT;

			// カテゴリが存在する場合
			if(appliFilList.get(i).getCategory() != null) {
				log.debug("カテゴリ存在");
				setComConnect = setComConnect + CATEGORY + QUOT +
								appliFilList.get(i).getCategory() + QUOT;
			}
			// サブカテゴリが存在する場合
			if(appliFilList.get(i).getSubCategory() != null) {
				log.debug("サブカテゴリ存在");
				setComConnect = setComConnect + SUBCATEGORY + QUOT +
								appliFilList.get(i).getSubCategory() + QUOT;
			}
			// テクノロジが存在する場合
			if(appliFilList.get(i).getTechnology() != null) {
				log.debug("テクノロジ存在");
				setComConnect = setComConnect + TECHNOLOGY + QUOT +
								appliFilList.get(i).getTechnology() + QUOT;
			}
			// リスクが存在する場合
			if(appliFilList.get(i).getRisk() != 0) {
				log.debug("リスク存在");
				setComConnect = setComConnect + RISK + QUOT +
								appliFilList.get(i).getRisk() + QUOT;
			}
			// 最後に改行コードを付加する。
			setComConnect = setComConnect + CHANGE_LINE;

			comList.add(setComConnect);

			// 設定コマンドログ出力
			Object[] objectgArray = {setComConnect};
			log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));

			// コマンドを実行し、設定する。
//2011/09/24 rep start inoue@prosite
			//command.getCommandResponce(compara, comList);
			response = command.getCommandResponce(compara, comList);
			// コマンドが失敗の場合( invalid or unknown or server error)
			if(response.indexOf(COMMAND_INVALID) != -1 ||
				response.indexOf(COMMAND_UNKNOWN) != -1 ||
				response.indexOf(COMMAND_SERVER_ERROR) != -1) {
				Object[] objectRes = {response};
				log.fatal(messageAccessor.getMessage("FZ011041", objectRes, uvo));
				return false;
			}
//2011/09/24 rep end inoue@prosite

		}

		/**************************************************
		 *	アプリケーショングループオブジェクト情報リスト
		 **************************************************/
		List<LinkInfo> appliGrpList = objList.getApplicationGroups();
		// アドレスグループオブジェクト設定処理
//2011/09/24 rep start inoue@prosite
		//setGroupOject(appliGrpList, command, compara, uvo, 2);
		bRst=setGroupOject(appliGrpList, command, compara, uvo, 2);
		if (!bRst){
			return false;
		}
//2011/09/24 rep end inoue@prosite

		/**************************************************
		 *	サービスオブジェクト情報リスト
		 **************************************************/
		List<Services> serviceList = objList.getServices();

		// サービスオブジェクト設定処理
		for(int i=0; i<serviceList.size(); i++) {
			log.debug("サービス設定:" + i);
			// コマンドリスト
			comList = new ArrayList<String>();
			 // コンフィグモードコマンド設定
			comList.add(CONFIG_COM);

			// 設定対象オブジェクトの設定
			setComConnect = SET_COM + SERVICE + QUOT +
				serviceList.get(i).getName() + QUOT + PROTOCOL + QUOT +
					serviceList.get(i).getProtocol() + QUOT + PORT + QUOT +
						serviceList.get(i).getPort() + QUOT + CHANGE_LINE;
			comList.add(setComConnect);

			// 設定コマンドログ出力
			Object[] objectgArray = {setComConnect};
			log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));

			// コマンドを実行し、設定する。
//2011/09/24 rep start inoue@prosite
			//command.getCommandResponce(compara, comList);
			response = command.getCommandResponce(compara, comList);
			// コマンドが失敗の場合( invalid or unknown or server error)
			if(response.indexOf(COMMAND_INVALID) != -1 ||
				response.indexOf(COMMAND_UNKNOWN) != -1 ||
				response.indexOf(COMMAND_SERVER_ERROR) != -1) {
				Object[] objectRes = {response};
				log.fatal(messageAccessor.getMessage("FZ011041", objectRes, uvo));
				return false;
			}
//2011/09/24 rep end inoue@prosite
		}

		/**************************************************
		 *	サービスグループオブジェクト情報リスト
		 **************************************************/
		List<LinkInfo> srvGrpList = objList.getServiceGroups();
		// サービスグループオブジェクト設定処理
//2011/09/24 rep start inoue@prosite
		//setGroupOject(srvGrpList, command, compara, uvo, 3);
		bRst = setGroupOject(srvGrpList, command, compara, uvo, 3);
		if (!bRst){
			return false;
		}
//2011/09/24 rep end inoue@prosite

		/**************************************************
		 *	スケジュールオブジェクト情報リスト
		 **************************************************/
		List<Schedules> scheduleList = objList.getSchedules();
		List<Schedules> tmpScheduleList = new ArrayList<Schedules>();
		Schedules schedule = new Schedules();
		int count = 0;
		int recurrence = 0;

		if(scheduleList.size() != 0) {
			log.debug("スケジュールが0件でない場合:");
			recurrence = scheduleList.get(0).getRecurrence();
		}
		// スケジュール(デイリー、指定日時)の同一名称の件数をカウントする）
		for(int i=0; i<scheduleList.size(); i++) {
			log.debug("同一名称の件数カウント処理:" + i);
			// ウィークリーでない場合
			if(scheduleList.get(i).getRecurrence() != 1) {
				log.debug("ウィークリー以外");
				recurrence = scheduleList.get(i).getRecurrence();
				// 名前が一致
				if(i != scheduleList.size() - 1 &&
						scheduleList.get(i).getName().equals(
									scheduleList.get(i+1).getName())) {
					count++;
					log.debug("名称一致:" + count);
				} else {
					log.debug("名称不一致:" + scheduleList.get(i).getName() + ", " +
												recurrence + ", " + count);
					count++;
					// スケジュール名を格納する。
					schedule.setName(scheduleList.get(i).getName());
					// 周期を格納
					schedule.setRecurrence(recurrence);
					// 件数を格納する。
					schedule.setSeqNo(count);
					tmpScheduleList.add(schedule);
					schedule = new Schedules();

					count = 0;
				}
			}
		}
		int tmpNum = 0;
		// スケジュールオブジェクト設定処理
		for(int i=0; i<scheduleList.size(); i++) {
			log.debug("スケジュール設定:" + i);
			// コマンドリスト
			comList = new ArrayList<String>();
			 // コンフィグモードコマンド設定
			comList.add(CONFIG_COM);
			String setScheName = "";
			// 設定対象オブジェクトの設定
			setComConnect = SET_COM + SCHEDULE + QUOT +
										scheduleList.get(i).getName() + QUOT;
			// Dailyの場合
			if(scheduleList.get(i).getRecurrence() == CusconConst.DAILY) {
				log.debug("Dailyの場合");

				for(int j=0 ; j<tmpScheduleList.size(); j++) {
					log.debug("tmp分繰り返す。:" + j);

					if(tmpScheduleList.get(j).getRecurrence() == 0) {
						log.debug("デイリー");
						if(scheduleList.get(i).getName().equals(
											tmpScheduleList.get(j).getName())) {
							log.debug("名称一致:" + scheduleList.get(i).getName());
							// 同一名称件数分繰り返す。
							for(int k=i; k<i+tmpScheduleList.get(j).getSeqNo(); k++) {
								log.debug("時間件数:" + k);
								// オブジェクトの1つ目の時間
								if(k==i) {
									log.debug("時間結合1回目");
									setScheName =  "[ " + scheduleList.get(k).getStartTime() + "-" +
											scheduleList.get(k).getEndTime() + " ";
								} else {
									log.debug("時間結合" + (k+1) + "回目");
									setScheName =  setScheName + scheduleList.get(k).getStartTime() + "-" +
											scheduleList.get(k).getEndTime() + " ";
								}

								// 最後の時間の場合、閉じ括弧をつける。
								if(k+1==i+tmpScheduleList.get(j).getSeqNo()) {
									log.debug("閉じ括弧付加:" + k);
									setScheName = setScheName + "]";
									tmpNum = tmpScheduleList.get(j).getSeqNo() - 1;
								}
							}
						}
					}
				}
				// scheduleListのインデックスに同一名称件数をプラスする。
				i = i + tmpNum;

				setComConnect = setComConnect + DAILY + setScheName;

			// Weeklyの場合
			} else if(scheduleList.get(i).getRecurrence() ==
													CusconConst.WEEKLY) {
				log.debug("Weeklyの場合");
				setComConnect = setComConnect + WEEKLY;

				// 日曜の場合
				if(scheduleList.get(i).getDayOfWeek() == CusconConst.SUN) {
					log.debug("日曜の場合");
					setComConnect = setComConnect + SUNDAY;
				// 月曜の場合
				} else if(scheduleList.get(i).getDayOfWeek() ==
														CusconConst.MON) {
					log.debug("月曜の場合");
					setComConnect = setComConnect + MONDAY;
				// 火曜の場合
				} else if(scheduleList.get(i).getDayOfWeek() ==
														CusconConst.TUE) {
					log.debug("火曜の場合");
					setComConnect = setComConnect + TUESDAY;
				// 水曜の場合
				} else if(scheduleList.get(i).getDayOfWeek() ==
														CusconConst.WED) {
					log.debug("水曜の場合");
					setComConnect = setComConnect + WEDNESDAY;
				// 木曜の場合
				} else if(scheduleList.get(i).getDayOfWeek() ==
														CusconConst.THU) {
					log.debug("木曜の場合");
					setComConnect = setComConnect + THURSDAY;
				// 金曜の場合
				} else if(scheduleList.get(i).getDayOfWeek() ==
														CusconConst.FRI) {
					log.debug("金曜の場合");
					setComConnect = setComConnect + FRIDAY;
				// 土曜の場合
				} else {
					log.debug("土曜の場合");
					setComConnect = setComConnect + SATURDAY;
				}

				setComConnect = setComConnect +
					scheduleList.get(i).getStartTime() + HYPHEN  +
										scheduleList.get(i).getEndTime();

			// Non-Recurringの場合
			} else {
				log.debug("Non-Recurring");

				for(int j=0 ; j<tmpScheduleList.size(); j++) {
					log.debug("tmp分繰り返す。:" + j);

					if(tmpScheduleList.get(j).getRecurrence() == 2) {
						log.debug("指定日時");
						if(scheduleList.get(i).getName().equals(
											tmpScheduleList.get(j).getName())) {
							log.debug("名称一致:" + scheduleList.get(i).getName());
							// 同一名称件数分繰り返す。
							for(int k=i; k<i+tmpScheduleList.get(j).getSeqNo(); k++) {
								log.debug("時間件数:" + k);
								// オブジェクトの1つ目の時間
								if(k==i) {
									log.debug("時間結合1回目");
									setScheName =  "[ " + scheduleList.get(k).getStartDate() +
									AT + scheduleList.get(k).getStartTime() + HYPHEN +
									scheduleList.get(k).getEndDate() + AT + scheduleList.get(k).getEndTime() + " ";
								} else {
									log.debug("時間結合" + (k+1) + "回目");
									setScheName =  setScheName + scheduleList.get(k).getStartDate() +
									AT + scheduleList.get(k).getStartTime() + HYPHEN +
									scheduleList.get(k).getEndDate() + AT + scheduleList.get(k).getEndTime() + " ";
								}

								// 最後の時間の場合、閉じ括弧をつける。
								if(k+1==i+tmpScheduleList.get(j).getSeqNo()) {
									log.debug("閉じ括弧付加:" + k);
									setScheName = setScheName + "]";
									tmpNum = tmpScheduleList.get(j).getSeqNo() - 1;
								}
							}
						}
					}
				}
				// scheduleListのインデックスに同一名称件数をプラスする。
				i = i + tmpNum;
				setComConnect = setComConnect + NON_RECURRING + setScheName;
			}

			// 最後に改行コードを付加する。
			setComConnect = setComConnect + CHANGE_LINE;

			comList.add(setComConnect);

			// 設定コマンドログ出力
			Object[] objectgArray = {setComConnect};
			log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));

			// コマンドを実行し、設定する。
//2011/09/24 rep start inoue@prosite
			//command.getCommandResponce(compara, comList);
			response = command.getCommandResponce(compara, comList);
			// コマンドが失敗の場合( invalid or unknown or server error)
			if(response.indexOf(COMMAND_INVALID) != -1 ||
				response.indexOf(COMMAND_UNKNOWN) != -1 ||
				response.indexOf(COMMAND_SERVER_ERROR) != -1) {
				Object[] objectRes = {response};
				log.fatal(messageAccessor.getMessage("FZ011041", objectRes, uvo));
				return false;
			}
//2011/09/24 rep end inoue@prosite
		}

//2011/06/10 add start inoue@prosite
		/**************************************************
		 *	Webフィルタオブジェクト情報リスト
		 **************************************************/
		if( uvo.getUrlFilteringFlg()==1 ){
			List<UrlFilters> urlfilterList = objList.getUrlFilters();
			// Webフィルタオブジェクト設定処理
			for(int i=0; i<urlfilterList.size(); i++) {
				log.debug("Webフィルタ設定:" + i);
				// コマンドリスト
				comList = new ArrayList<String>();

				// コンフィグモードコマンド設定
				comList.add(CONFIG_COM);

				// 設定対象オブジェクトの設定
				String commonSet = SET_COM  + URLFILTER + QUOT + urlfilterList.get(i).getName() + QUOT;
				setComConnect = commonSet;

				// description
				if( urlfilterList.get(i).getComment() != null){
					if( urlfilterList.get(i).getComment().length() != 0 ){
						setComConnect = setComConnect + DESCRIPTION  + QUOT + urlfilterList.get(i).getComment() + QUOT;
					}
				}
				// Action on License Expiration(プロパティファイルの値を設定）未設定時はAllow
			    String license = PropertyUtil.getProperty("fwsetting.common.urlfilter_action_on_license_expiration");
			    if(license==null){
			    	license=URLFILTER_ACTION_ALLOW;
			    }
				setComConnect = setComConnect + URLFILTER_ACTION_ON_LiCENSE_EXPIRATION + license;

				// Dynamic URL(プロパティファイルの値を設定）
			    String dynamic = PropertyUtil.getProperty("fwsetting.common.urlfilter_dynamic-url");
				setComConnect = setComConnect + URLFILTER_DYNAMIC_URL + dynamic;

				// action block site list
				setComConnect = setComConnect + ACTION + urlfilterList.get(i).getActionBlockListSite();
				setComConnect += CHANGE_LINE;
				comList.add(setComConnect);
				//ここまでで1つ

				//block list site
				String sendbuf = "";
				String CRLF = "\n";
			    String proLimitlength = PropertyUtil.getProperty("fwsetting.common.urlfilter_list_max_buffer");
			    Integer lim = Integer.parseInt(proLimitlength);
			    lim = lim - commonSet.length() - BLOCK_LIST.length() - 4;

				// block list site
				String blockListSite = urlfilterList.get(i).getBlockListSite();
				if(blockListSite != null){
					if( blockListSite.length() != 0 ){
						setComConnect = "";
					    String[] blockListSiteArray = blockListSite.split(CRLF, 0);
						for (int j = 0; j < blockListSiteArray.length; j++) {
							String lists = blockListSiteArray[j].replaceAll("\r", "");
							lists = QUOT + lists + QUOT;
							if( lim < (setComConnect.length() + lists.length()) ){
								// 最後の" "をカット
								setComConnect = setComConnect.substring(0, setComConnect.length()-1);
								sendbuf = commonSet + BLOCK_LIST + "[ " + setComConnect + " ]" + CHANGE_LINE;
								comList.add(sendbuf);
								setComConnect = "";
							}
							setComConnect = setComConnect + lists +  " ";
						}
						// 最後の" "をカット
						setComConnect = setComConnect.substring(0, setComConnect.length()-1);
						sendbuf = commonSet + BLOCK_LIST + "[ " + setComConnect + " ]" + CHANGE_LINE;
						comList.add(sendbuf);
					}
				}

				// allow list site
				String allowListSite = urlfilterList.get(i).getAllowListSite();
				if(allowListSite != null){
					if( allowListSite.length() != 0 ){
						setComConnect = "";
					    String[] allowListSiteArray = allowListSite.split(CRLF, 0);
					    for (int j = 0; j < allowListSiteArray.length; j++) {
					    	String lists = allowListSiteArray[j].replaceAll("\r", "");
						    lists = QUOT + lists + QUOT;
							if( lim < (setComConnect.length() + lists.length()) ){
								// 最後の" "をカット
								setComConnect = setComConnect.substring(0, setComConnect.length()-1);
								sendbuf = commonSet + ALLOW_LIST + "[ " + setComConnect + " ]" + CHANGE_LINE;
								comList.add(sendbuf);
								setComConnect = "";
							}
							setComConnect = setComConnect + lists +  " ";
						}
						// をカット
						setComConnect = setComConnect.substring(0, setComConnect.length()-1);
						sendbuf = commonSet +  ALLOW_LIST + "[ " + setComConnect + " ]" + CHANGE_LINE;
						comList.add(sendbuf);
					}
				}

				// URLフィルタカテゴリの設定
				String alertctgr = "";
				String allowctgr = "";
				String blockctgr = "";
				String contctgr = "";
				String overctgr = "";
				List<UrlFilterCategories> urlFilterCategoryList = urlfilterList.get(i).getUrlfiltercategories();
				for (int j = 0; j < urlFilterCategoryList.size(); j++) {
					if( urlFilterCategoryList.get(j).getAction().equalsIgnoreCase(URLFILTER_ACTION_ALERT)){
						alertctgr = alertctgr + QUOT + urlFilterCategoryList.get(j).getName() + QUOT + " ";
					}
					if( urlFilterCategoryList.get(j).getAction().equalsIgnoreCase(URLFILTER_ACTION_ALLOW)){
						allowctgr = allowctgr + QUOT + urlFilterCategoryList.get(j).getName() + QUOT + " ";
					}
					if( urlFilterCategoryList.get(j).getAction().equalsIgnoreCase(URLFILTER_ACTION_BLOCK)){
						blockctgr = blockctgr + QUOT + urlFilterCategoryList.get(j).getName() + QUOT + " ";
					}
					if( urlFilterCategoryList.get(j).getAction().equalsIgnoreCase(URLFILTER_ACTION_CONTINUE)){
						contctgr = contctgr + QUOT + urlFilterCategoryList.get(j).getName() + QUOT + " ";
					}
					if( urlFilterCategoryList.get(j).getAction().equalsIgnoreCase(URLFILTER_ACTION_OVERRIDE)){
						overctgr = overctgr + QUOT + urlFilterCategoryList.get(j).getName() + QUOT + " ";
					}
				}
				setComConnect = "";
				if( alertctgr.length() != 0 || allowctgr.length() != 0 ||
					blockctgr.length() != 0 || contctgr.length() != 0 || overctgr.length() != 0){
					setComConnect = commonSet;
				}
				if (alertctgr.length() != 0 ){
					setComConnect = setComConnect + SP  + URLFILTER_ACTION_ALERT  + " [ " + alertctgr.substring(0, alertctgr.length()-1) + " ]";
				}
				if (allowctgr.length() != 0 ){
					setComConnect = setComConnect + SP  + URLFILTER_ACTION_ALLOW  + " [ " + allowctgr.substring(0, allowctgr.length()-1) + " ]";
				}
				if (blockctgr.length() != 0 ){
					setComConnect = setComConnect + SP  + URLFILTER_ACTION_BLOCK  + " [ " + blockctgr.substring(0, blockctgr.length()-1) + " ]";
				}
				if (contctgr.length() != 0 ){
					setComConnect = setComConnect + SP + URLFILTER_ACTION_CONTINUE  + " [ " + contctgr.substring(0, contctgr.length()-1) + " ]";
				}
				if (overctgr.length() != 0 ){
					setComConnect = setComConnect + SP + URLFILTER_ACTION_OVERRIDE  + " [ " + overctgr.substring(0, overctgr.length()-1) + " ]";
				}
				if( setComConnect.length() != 0  ){
					setComConnect += CHANGE_LINE;
					comList.add(setComConnect);
				}
				// 設定コマンドログ出力
				for (int j = 0; j < comList.size(); j++) {
					Object[] objectgArray = {comList.get(j)};
					log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));
				}
				// コマンドを実行し、設定する。
//2011/09/24 rep start inoue@prosite
				//command.getCommandResponce(compara, comList);
				response = command.getCommandResponce(compara, comList);
				// コマンドが失敗の場合( invalid or unknown or server error)
				if(response.indexOf(COMMAND_INVALID) != -1 ||
					response.indexOf(COMMAND_UNKNOWN) != -1 ||
					response.indexOf(COMMAND_SERVER_ERROR) != -1) {
					Object[] objectRes = {response};
					log.fatal(messageAccessor.getMessage("FZ011041", objectRes, uvo));
					return false;
				}
//2011/09/24 rep end inoue@prosite
			}
		}
//2011/06/10 add start inoue@prosite

		/*********************************
		 *	ポリシー設定処理
		 *********************************/
		// 出所ゾーン名リスト
		List<SelectObjectList> srcZoneList;
		// 宛先ゾーン名リスト
		List<SelectObjectList> dstZoneList;
		// 出所アドレス名リスト
		List<SelectObjectList> srcAddressList;
		// 宛先アドレス名リスト
		List<SelectObjectList> dstAddressList;
		// アプリケーション名リスト
		List<SelectObjectList> appliList;
		// サービス名リスト
		List<SelectObjectList> srvList;

		// ポリシー設定処理
		for(int i=0; i<policyList.size(); i++) {
			log.debug("ポリシー設定:" + i);
			// コマンド一時生成用
			String tmpSetCom;
			// コマンドリスト
			comList = new ArrayList<String>();
			 // コンフィグモードコマンド設定
			comList.add(CONFIG_COM);

			srcZoneList = policyList.get(i).getSourceZoneList();
			dstZoneList = policyList.get(i).getDestinationZoneList();
			srcAddressList = policyList.get(i).getSourceAddressList();
			dstAddressList = policyList.get(i).getDestinationAddressList();
			appliList = policyList.get(i).getApplicationList();
			srvList = policyList.get(i).getServiceList();

			// 設定対象オブジェクトの設定
			tmpSetCom = SET_COM + SECURITY + QUOT + policyList.get(i).getName() + QUOT;

			// 設定コマンド1回目
			setComConnect = tmpSetCom + FROM + QUOT + srcZoneList.get(0).getName() + QUOT +
				TO + QUOT + dstZoneList.get(0).getName() + QUOT  +
				SOURCE + QUOT + srcAddressList.get(0).getName() + QUOT +
				DESTINATION + QUOT + dstAddressList.get(0).getName() + QUOT +
				APPLICATION + QUOT + appliList.get(0).getName() + QUOT +
				SERVICE + QUOT + srvList.get(0).getName() + QUOT +
				ACTION + QUOT + policyList.get(i).getAction() + QUOT;

			// 備考は設定がある場合のみ、コマンド発行
			if(policyList.get(i).getDescription() != null) {
				log.debug("備考設定付加");
				setComConnect = setComConnect + DESCRIPTION + QUOT + policyList.get(i).getDescription() + QUOT;
			}
			// スケジュールはnoneでない場合のみ、コマンド発行
			if(!policyList.get(i).getSchedule().equals(NONE)) {
				log.debug("スケジュール設定付加");
				setComConnect = setComConnect + SCHEDULE + QUOT + policyList.get(i).getSchedule() + QUOT;
			}

			// LogFowardProfileを設定する
			if(policyList.get(i).getAction().equals(ACTION_ALLOW)) {
				log.debug("AllowLogForwardingProfile設定付加");
				setComConnect = setComConnect + LOG_SETTING + ALLOW_LOG_FORWARD_PROFILE;
			}
			else if(policyList.get(i).getAction().equals(ACTION_DENY)) {
				log.debug("DenyLogForwardingProfile設定付加");
				setComConnect = setComConnect + LOG_SETTING + DENY_LOG_FORWARD_PROFILE;
			}

			// ログエンドコマンドを設定する。
			setComConnect = setComConnect + LOG_END;

			// 脆弱性の保護フラグはdefaultの場合のみ、コマンド発行
			if(policyList.get(i).getIds_ips().equals(DEFAULT)) {
				log.debug("脆弱性の保護フラグ設定付加");
				setComConnect = setComConnect + PROFILE + QUOT + policyList.get(i).getIds_ips() + QUOT + CHANGE_LINE;
			} else {
				// 最後に改行コードを付加する。
				setComConnect = setComConnect + CHANGE_LINE;
			}

			// コマンド実行
			comList.add(setComConnect);

//2011/06/10 add start inoue@prosite
			// WebフィルタがONの時に限り、profileを設定
			if( uvo.getUrlFilteringFlg()==1 ){
				log.debug("profile-setting urlfilter設定付加");
				// urlfilter-profile
				if(policyList.get(i).getUrlfilterDefaultFlg()==1){
					setComConnect = tmpSetCom + URLFILTER_PROFILE + policyList.get(i).getUrlfilterName();
					setComConnect = setComConnect + CHANGE_LINE;
					// コマンド実行
					comList.add(setComConnect);
				}
			}
			// virusプロファイルを設定する
			if(policyList.get(i).getVirusChecktFlg() == 1) {
				log.debug("profile-setting virus設定付加");
				// virus-profile
				String profileVirus = PropertyUtil.getProperty("fwsetting.common.profile_virus");
				if(profileVirus == null){
					log.debug("ApplicationResurces.propertiesにfwsetting.common.profile_virus未設定");
				}else{
					setComConnect = tmpSetCom + VIRUS_PROFILE + profileVirus;
					setComConnect = setComConnect + CHANGE_LINE;
					// コマンド実行
					comList.add(setComConnect);
				}
			}
			// spywareプロファイルを設定する
			if(policyList.get(i).getSpywareFlg() == 1) {
				String profileSpyware = PropertyUtil.getProperty("fwsetting.common.profile_spyware");
				if(profileSpyware == null){
					log.debug("ApplicationResurces.propertiesにfwsetting.common.profile_spyware未設定");
				}else{
					log.debug("profile-setting spyware設定付加");
					setComConnect = tmpSetCom + SPYWARE_PROFILE + profileSpyware;
					setComConnect = setComConnect + CHANGE_LINE;
					// コマンド実行
					comList.add(setComConnect);
				}
			}
//2011/06/10 add end inoue@prosite
			// 20130901 kkato@PROSITE add start
			// disabledプロファイルを設定する
			if(policyList.get(i).getDisableFlg() == 1) {
				String profileDisable = PropertyUtil.getProperty("fwsetting.common.profile_disable");
				if(profileDisable == null){
					log.debug("ApplicationResurces.propertiesにfwsetting.common.profile_disable未設定");
				}else{
					log.debug("profile-setting disable設定付加");
					setComConnect = tmpSetCom + DISABLE_PROFILE + profileDisable;
					setComConnect = setComConnect + CHANGE_LINE;
					// コマンド実行
					comList.add(setComConnect);
				}
			}
			// 20130901 kkato@PROSITE add end

//2011/06/10 rep start inoue@prosite
//			// 設定コマンドログ出力
//			Object[] objectgArray = {setComConnect};
//			log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));
			for(int j=0; j<comList.size(); j++) {
				// 設定コマンドログ出力
				Object[] objectgArray = {comList.get(j)};
				log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));
			}
//2011/06/10 rep end inoue@prosite
			// コマンドを実行し、設定する。
//2011/09/24 rep start inoue@prosite
			//command.getCommandResponce(compara, comList);
			response = command.getCommandResponce(compara, comList);
			// コマンドが失敗の場合( invalid or unknown or server error)
			if(response.indexOf(COMMAND_INVALID) != -1 ||
				response.indexOf(COMMAND_UNKNOWN) != -1 ||
				response.indexOf(COMMAND_SERVER_ERROR) != -1) {
				Object[] objectRes = {response};
				log.fatal(messageAccessor.getMessage("FZ011041", objectRes, uvo));
				return false;
			}
//2011/09/24 rep end inoue@prosite

			// 複数設定が存在する場合
			// 出所ゾーンリストの設定
			setComConnect = tmpSetCom + FROM + QUOT;
//2011/09/24 rep start inoue@prosite
			//setSecurityRule(srcZoneList, setComConnect, command, compara, uvo);
			bRst = setSecurityRule(srcZoneList, setComConnect, command, compara, uvo);
			if (!bRst){
				return false;
			}
//2011/09/24 rep end inoue@prosite

			// 宛先ゾーンリストの設定
			setComConnect = tmpSetCom + TO + QUOT;
//2011/09/24 rep start inoue@prosite
			//setSecurityRule(dstZoneList, setComConnect, command, compara, uvo);
			bRst = setSecurityRule(dstZoneList, setComConnect, command, compara, uvo);
			if (!bRst){
				return false;
			}
//2011/09/24 rep end inoue@prosite

			// 出所アドレス名リストの設定
			setComConnect = tmpSetCom + SOURCE + QUOT;
//2011/09/24 rep start inoue@prosite
			//setSecurityRule(srcAddressList, setComConnect, command, compara, uvo);
			bRst = setSecurityRule(srcAddressList, setComConnect, command, compara, uvo);
			if (!bRst){
				return false;
			}
//2011/09/24 rep end inoue@prosite

			// 宛先アドレス名リストの設定
			setComConnect = tmpSetCom + DESTINATION + QUOT;
//2011/09/24 rep start inoue@prosite
			//setSecurityRule(dstAddressList, setComConnect, command, compara, uvo);
			bRst = setSecurityRule(dstAddressList, setComConnect, command, compara, uvo);
			if (!bRst){
				return false;
			}
//2011/09/24 rep end inoue@prosite

			// アプリケーション名リストの設定
			setComConnect = tmpSetCom + APPLICATION + QUOT;
//2011/09/24 rep start inoue@prosite
			//setSecurityRule(appliList, setComConnect, command, compara, uvo);
			bRst = setSecurityRule(appliList, setComConnect, command, compara, uvo);
			if (!bRst){
				return false;
			}
//2011/09/24 rep end inoue@prosite

			// サービス名リストの設定
			setComConnect = tmpSetCom + SERVICE + QUOT;
//2011/09/24 rep start inoue@prosite
			//setSecurityRule(srvList, setComConnect, command, compara, uvo);
			bRst = setSecurityRule(srvList, setComConnect, command, compara, uvo);
			if (!bRst){
				return false;
			}
//2011/09/24 rep end inoue@prosite

		}

		log.debug("registPA処理終了:true");
		return true;
	}


	/**
	 * メソッド名 :setGroupOject
	 * 機能概要 :グループオブジェクトの設定を行う。
	 * @param objList 設定対象オブジェクト情報リスト
	 * @param processFlg プロセスフラグ
	 * @throws Exception
	 */
	private boolean setGroupOject(List<LinkInfo> objList,
			CommandExecute command, CommandPara compara, CusconUVO uvo, int processFlg)
	throws Exception {
		log.debug("setGroupOject処理開始:objList.size() = " +
						objList.size() + ", processFlg = " + processFlg);
		// レスポンス返却用
		String response;
		// レスポンス解析用
		String checkSetObj;
		// コマンド一時生成用
		String tmpSetCom;
		// 設定コマンド生成用
		String setComConnect;
		// ループ制御フラグ
		boolean roopFlg = true;
		// コマンドリスト
		List<String> comList;
		// コマンドタイプ
		String comType;
		// 設定処理に使用するためのリスト
		List<LinkInfo> tmp_list = objList;


		// アドレスグループオブジェクト
		if(processFlg == 1) {
			log.debug("アドレスグループ設定");
			comType = ADDRESS_GRP;
		// アプリケーショングループオブジェクト
		} else if(processFlg == 2) {
			log.debug("アプリケーショングループ設定");
			comType = APPLI_GRP;
		// サービスグループオブジェクト
		} else {
			log.debug("サービスグループ設定");
			comType = SERVICE_GRP;
		}

		// 全てのグループオブジェクトの設定が完了するまで処理を繰り返す。
		while(roopFlg) {
			log.debug("グループオブジェクト設定:" + roopFlg);
			for(int i=0; i<objList.size(); i++) {
				log.debug("設定中:" + i);
				// コマンドリスト
				comList = new ArrayList<String>();
				 // コンフィグモードコマンド設定
				comList.add(CONFIG_COM);

				// 対象の設定
				setComConnect = "\n";

				// UPDATED: 2016.11 by Plum Systems Inc.
				// PANOS7対応 -->
				if(comType == ADDRESS_GRP){
					tmpSetCom = SET_COM + comType + QUOT +
						objList.get(i).getName() + QUOT + SP + "static " + QUOT +
										objList.get(i).getLinkName() + QUOT;
				} else if(comType == APPLI_GRP || comType == SERVICE_GRP) {
					tmpSetCom = SET_COM + comType + QUOT +
							objList.get(i).getName() + QUOT + SP + "members " + QUOT +
											objList.get(i).getLinkName() + QUOT;
				} else {
					tmpSetCom = SET_COM + comType + QUOT +
						objList.get(i).getName() + QUOT + SP + QUOT +
										objList.get(i).getLinkName() + QUOT;
				}
				setComConnect = tmpSetCom + CHANGE_LINE;
				// --> PANOS7対応

				// 設定コマンドログ出力
				Object[] objectgArray = {setComConnect};
				log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));

				comList.add(setComConnect);

				// コマンドを実行し、レスポンスを取得する。
				response = command.getCommandResponce(compara, comList);

				checkSetObj = tmpSetCom + CHECK_COM;

//2011/09/24 add start inoue@prosite
				// コマンドが失敗の場合( invalid or unknown or server error)
				if(response.indexOf(COMMAND_INVALID) != -1 ||
					response.indexOf(COMMAND_UNKNOWN) != -1 ||
					response.indexOf(COMMAND_SERVER_ERROR) != -1) {
					Object[] objectRes = {response};
					log.fatal(messageAccessor.getMessage("FZ011041", objectRes, uvo));
					return false;
				}
//2011/09/24 add end inoue@prosite

				// レスポンスを分割する。
// レスポンス判定変更時コメントアウト開始位置
//				String[] split_res = response.split(checkSetObj);
//				if (split_res.length < CHECK_NUM + 1) {
//					// コマンド実行エラー
//					Object[] objectgArray1 = {response};
//					log.error(messageAccessor.getMessage("EZ011001", objectgArray1, uvo));
//
//					throw new Exception();
//				}
//
//				// 設定コマンド成功の場合
//				if(split_res[CHECK_NUM].indexOf(COM_ERROR)==-1){
// レスポンス判定変更時コメントアウト終了位置
// レスポンス判定変更時有効化
				if(response.indexOf(COM_ERROR)==-1){
					log.debug("設定コマンド成功");
					// リストから要素を削除する。
					tmp_list.remove(i);
				}
			}

			// リストの要素が存在しない場合、ループを抜ける。
			if(tmp_list.size() == 0) {
				log.debug("処理対象オブジェクト設定完了：false");
				roopFlg = false;
			}
			objList = tmp_list;
		}

		log.debug("setGroupOject処理終了2:true");
		return true;
	}

	/**
	 * メソッド名 : setSecurityRule
	 * 機能概要 : objListのデータ件数分ポリシー情報を設定する。
	 * @param objList 設定対象オブジェクト情報リスト
	 * @param command 実行コマンド
	 * @param num データ件数判定
	 * @throws Exception
	 */
//2011/09/24 rep start inoue@prosite 型変更
//	private void setSecurityRule(List<SelectObjectList> objList,
	private boolean setSecurityRule(List<SelectObjectList> objList,
					String commandConnect, CommandExecute command,
					CommandPara compara, CusconUVO uvo) throws Exception{

		log.debug("setSecurityRule処理開始:objList.size() = " +
				objList.size() + ", commandConnect = " + commandConnect);
		// コマンドリスト
		List<String> comList;

		// オブジェクトリストのデータがnum件以上の場合のみ実行
		for(int i=1; i<objList.size(); i++) {
			log.debug("オブジェクト設定:" + i);
			// コマンドリスト
			comList = new ArrayList<String>();
			 // コンフィグモードコマンド設定
			comList.add(CONFIG_COM);

			String tmpcommandConnect = commandConnect + objList.get(i).getName() + QUOT + CHANGE_LINE;

			// 実行コマンド設定
			comList.add(tmpcommandConnect);

			// 設定コマンドログ出力
			Object[] objectgArray = {tmpcommandConnect};
			log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));
			// コマンド実行
			String response = command.getCommandResponce(compara, comList);
//2011/09/24 add start inoue@prosite
			// コマンドが失敗の場合( invalid or unknown or server error)
			if(response.indexOf(COMMAND_INVALID) != -1 ||
				response.indexOf(COMMAND_UNKNOWN) != -1 ||
				response.indexOf(COMMAND_SERVER_ERROR) != -1) {
				Object[] objectRes = {response};
				log.fatal(messageAccessor.getMessage("FZ011041", objectRes, uvo));
				return false;
			}
//2011/09/24 add end inoue@prosite
		}

		log.debug("setSecurityRule処理終了2:true");

		//2011/09/24 add start inoue@prosite
		return true;
		//2011/09/24 add end inoue@prosite

	}

	/**
	 * メソッド名 : executeCommit
	 * 機能概要 : コミットコマンドを実行する。
	 * @throws Exception
	 */
	private String executeCommit(
			CommandExecute command, CommandPara compara, CusconUVO uvo) throws Exception {

		log.debug("executeCommit処理開始");
		String commitCom = PropertyUtil.getProperty("fwsetting.common.commit");

		log.debug("コミットコマンド:" + commitCom);

		// コマンドリスト
		List<String> comList = new ArrayList<String>();

		// コンフィグモードコマンド設定
		comList.add(CONFIG_COM);
		// コミットコマンド設定
		comList.add(commitCom);

		// コミットコマンドログ出力
		Object[] objectgArray = {commitCom};
		log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));

		// コマンド実行
		String commiReslt = command.getCommandResponce(compara, comList);

		// commit完了ログ出力
		log.info("コマンド実行完了" + commiReslt);

		log.debug("executeCommit処理終了");

		return commiReslt;
	}

//2011/09/24 add start inoue@prosite
	/**
	 * メソッド名 : executeLoad_runnning_config
	 * 機能概要 : running-configのロードコマンドを実行する。
	 * @throws Exception
	 */
	private String executeLoad_runnning_config(
			QueryDAO queryDAO, CusconUVO uvo, MessageAccessor msgAcc) throws Exception {

		log.debug("load running-config開始");
		log.debug("コマンド:" + LOAD_RUNNING_CONFIG);

		//2014/03/19 add start kkato@prosite
		int retryCount;
		retryCount = 0;
		String Result;
		//リトライ処理ループ
		while(true) {
			retryCount++;
			Result = "";
			try {
				//2014/03/19 add end   kkato@prosite
				// 設定コマンド用セッション作成 running-config時はadminで接続
				// PA接続情報を取得する。
				SelectSystemConstantList connectionInfo = getConnectionInfo_Admin(queryDAO, uvo);
				CommandPara compara = new CommandPara();
				compara.setConnectionInfo(connectionInfo);
				compara.setUvo(uvo);
				compara.setTimeout(timeout);
				compara.setWaitCommand(WAIT_COMMAND);
				CommandExecute command = new CommandExecute(compara, msgAcc);

				// コマンドリスト
				List<String> comList = new ArrayList<String>();

				// コンフィグモードコマンド設定
				comList.add(CONFIG_COM);

				// コマンド設定
				comList.add(LOAD_RUNNING_CONFIG);

				// コマンドログ出力
				Object[] objectgArray = {LOAD_RUNNING_CONFIG};
				log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));

				// コマンド実行
				Result = command.getCommandResponce(compara, comList);

				// load完了ログ出力
				log.debug("コマンド実行完了" + Result);

				// loadが失敗の場合
				if(Result.indexOf(LOAD_RUNNING_CONFIG_SUCCESS) == -1) {
					//2014/03/19 mod start kkato@prosite
					log.error("running-configのロードに失敗しました。" + Result);
					//例外を起こしてリトライ処理する
					throw new Exception();
					//2014/03/19 add end   kkato@prosite
				}else{
					log.info("running-configのロードに成功しました。");
				}
				command.disconnect(compara);

				log.debug("load running-config終了");

				//2014/03/19 add start kkato@prosite
				break;
			} catch (Exception e) {
				if (retryCount >= REVERT_RETRY_CNT){
					log.fatal("running-configのロード リトライオーバー");
					break;
				}
				log.info("running-configのロード リトライ:" + retryCount,e);
				Thread.sleep(REVERT_RETRY_WAIT_TIME);
			}
		}
		//2014/03/19 add end   kkato@prosite

		return Result;
	}

	/**
	 * メソッド名 : getConnectionInfo
	 * 機能概要 : DBよりPA接続情報を取得し、返却する。
	 * @return SelectSystemConstantList PA接続情報
	 * @throws Exception
	 */
	private SelectSystemConstantList getConnectionInfo_Admin(QueryDAO queryDAO, CusconUVO uvo) throws Exception {

		// PAのホスト、ユーザID、パスワードを取得する。
		// 復号化鍵
		String key = "kddi-secgw";
		// PA接続情報
		List<SelectSystemConstantList> connectionList = null;

		try {
			// PA接続情報を取得する。
			connectionList =
				queryDAO.executeForObjectList("CommonM_SystemConstant-1", key);
		} catch(Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FZ011033", objectgArray, uvo));
			throw e;
		}

		// 接続情報を戻り値に格納する。
		//connectionList.get(0).setLoginId(loginId);
		//connectionList.get(0).setPassword(password);
		WAIT_COMMAND = connectionList.get(0).getCmdWait();
		log.debug("getConnectionInfo処理終了");
		return connectionList.get(0);
	}
//2011/09/24 add end inoue@prosite

}


