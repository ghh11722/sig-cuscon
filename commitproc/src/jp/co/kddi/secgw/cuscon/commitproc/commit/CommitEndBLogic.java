/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名	: CommitEnd.java
 *
 * [変更履歴]
 * 日付 		  更新者				  内容
 * 2010/02/19	  komakiys		    初版作成
 * 2010/06/17	  hiroyasu		    コミット処理プロセス化対応
 * 2011/06/10	  inoue@prosite     Webフィルタの追加対応
 * 2016/11/21	  T.Yamazaki@Plum   Terasolunaアップデート/SSH対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.commitproc.commit;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionStatus;

import jp.co.kddi.secgw.cuscon.commitproc.common.CreateCusconUVO;
import jp.co.kddi.secgw.cuscon.commitproc.common.CusconConst;
import jp.co.kddi.secgw.cuscon.commitproc.common.CusconFWDelete;
import jp.co.kddi.secgw.cuscon.commitproc.common.CusconFWSet;
import jp.co.kddi.secgw.cuscon.commitproc.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.commitproc.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.commitproc.common.PolicyList;
import jp.co.kddi.secgw.cuscon.commitproc.common.ResultJudgeData;
import jp.co.kddi.secgw.cuscon.commitproc.common.TimeoutException;
import jp.co.kddi.secgw.cuscon.commitproc.common.UpdateCommitFlg;
import jp.co.kddi.secgw.cuscon.commitproc.common.UpdateCommitStatus;
import jp.co.kddi.secgw.cuscon.commitproc.common.UpdatePA;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.AddressGroups;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.ApplicationGroups;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.CommitStatus;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.SecurityRules;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.ServiceGroups;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.util.PropertyUtil;

/**
 * クラス名 : CommitEndBLogic
 * 機能概要 : コミット処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *			Created 2010/02/19
 *			新規作成
 *			2.0 T.Yamazaki@Plum Systems Inc.
 *			Updated 2016/11/21
 *			Terasolunaアップデート/SSH対応
 * @see
 */
public class CommitEndBLogic {

	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.commitproc.commit.CommitEndBLogic");
	// メッセージクラス
	private static MessageAccessor messageAccessor = null;

  	private static ApplicationContext context = null;
  	private static DataSourceTransactionManager manager = null;
  	private static CusconUVO uvo = null;
	// QueryDAO
	private static QueryDAO queryDAO;
	// UpdateDAO
	private static UpdateDAO updateDAO;

	public static final String contxtFileName = "applicationContext.xml";

	// 日時フォーマット
	private static final String DATE_FORMAT =
		PropertyUtil.getProperty("fwsetting.common.dateformat");

	/**
	 * メソッド名 :main
	 * 機能概要 : コミット処理を行う
	 * @param param CommitEndの入力クラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public static void main(String[] args) {

		log.debug("----------CommitEndBLogic(2016/12ビルド版) 処理開始--------------------");
		log.debug("CommitEndBLogic処理開始");

		log.debug("指定された引数は、" + args.length + "個です");
		if (args.length != 4) {
			log.fatal("引数が不正です。");
			System.exit(0);
		}

		log.debug("引数表示");
		for (int i=0; i<args.length; ++i) {
			log.debug(args[i]);
		}

		// 引数をCusconUVOに設定する
		uvo = new CusconUVO();
		try {
			uvo.setLoginId(URLDecoder.decode(args[0], "UTF-8"));
			uvo.setVsysId(URLDecoder.decode(args[1], "UTF-8"));
			uvo.setGrantFlagStr(URLDecoder.decode(args[2], "UTF-8"));
		} catch (UnsupportedEncodingException e3) {
			log.fatal("引数が不正です。");
			System.exit(0);
		}

		try {
	  		context = new ClassPathXmlApplicationContext(contxtFileName);
	  		manager = (DataSourceTransactionManager)context.getBean("transactionManager");
		} catch (Exception e) {
			log.fatal(uvo.getGrantFlagStr() + " " + uvo.getLoginId() +
					" " + uvo.getVsysId() + " " +
					"アプリケーションコンテキストの読み込みに失敗しました。"
					+ e.getMessage());
			System.exit(0);
		}
		// コミット処理結果格納用エリア
		CommitStatus result = new CommitStatus();

		try{
			// CusconUVOを作成する。
			CreateCusconUVO cv = new CreateCusconUVO();
			cv.setCusconUVO(queryDAO, uvo, messageAccessor);
			result.setVsysId(uvo.getVsysId());

//2011/06/10 add start inoue@prosite
			// WebフィルタがOFFの時、全てのWebフィルタ関連のデータを削除し、
			// セキュリティポリシーのURL適用フラグも更新する。
			if( uvo.getUrlFilteringFlg()==0 ){
				DeleteUrlFilterData(uvo);
			}
//2011/06/10 add end inoue@prosite

			// 0世代のポリシー一覧情報を取得する。
			PolicyList policy = new PolicyList();
			List<SelectPolicyList> policyList =
				policy.getPolicylist(
						queryDAO, uvo,
						CusconConst.ZERO_GENE, CusconConst.COMMIT, messageAccessor);

			UpdatePA update = new UpdatePA();

			// 処理結果格納用
			ResultJudgeData resultJudge = new ResultJudgeData();

			update.updatePAProcess(resultJudge, policyList,
						uvo.getVsysId(),
						CusconConst.ZERO_GENE, uvo.getPaLoginId(),
						uvo.getPaPasswd(), queryDAO, uvo, messageAccessor);

			// コミットコマンド失敗の場合
			if(resultJudge.getResultFlg() == 3) {
				log.debug("コミットコマンド失敗");

				// コミットコマンド失敗メッセージ出力
				result.setMessage(messageAccessor.getMessage("DK070003", null));
				// ステータスを設定する。
				result.setStatus(CusconConst.FAiILURE);
				// 終了時刻を設定する。
				Date date = new Date();
				SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
				result.setEndTime(dateFormat.format(date));
				// コミットステータス確認テーブルを更新する。
				UpdateCommitStatus ucs = new UpdateCommitStatus();
				ucs.setCommitStatus(updateDAO, result, uvo, messageAccessor);

				// コミット中フラグOFF
				UpdateCommitFlg ucf = new UpdateCommitFlg();
				ucf.updatePAProcessFlg(CusconConst.COMMIT_OFF, queryDAO);

				System.exit(0);
			}

			// DB更新管理
			updateFWManage(resultJudge);

			// コミット成功のメッセージ格納
			result.setMessage(messageAccessor.getMessage("DK070004", null));
			// ステータスを設定する。
			result.setStatus(CusconConst.SUCCESS);
			// 終了時刻を設定する。
			Date date = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
			result.setEndTime(dateFormat.format(date));
			// コミットステータス確認テーブルを更新する。
			UpdateCommitStatus ucs = new UpdateCommitStatus();
			ucs.setCommitStatus(updateDAO, result, uvo, messageAccessor);

			// コミット中フラグOFF
			UpdateCommitFlg ucf = new UpdateCommitFlg();
			ucf.updatePAProcessFlg(CusconConst.COMMIT_OFF, queryDAO);

		} catch(TimeoutException e) {
			// コマンドタイムアウトメッセージ出力
			log.debug("コマンドタイムアウト");
			result.setMessage(messageAccessor.getMessage("DK070003", null));
			result.setStatus(CusconConst.FAiILURE);
			// 終了時刻を設定する。
			Date date = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
			result.setEndTime(dateFormat.format(date));
			// コミットステータス確認テーブルを更新する。
			UpdateCommitStatus ucs = new UpdateCommitStatus();
			try {
				ucs.setCommitStatus(updateDAO, result, uvo, messageAccessor);
			} catch (Exception e2) {
				// DBアクセスエラー
				Object[] objectgArray = {e2.getMessage()};
				log.fatal(messageAccessor.getMessage("FK180002", objectgArray, uvo));
			}

			// コミット中フラグOFF
		   	UpdateCommitFlg ucf = new UpdateCommitFlg();
   			try {
   				ucf.updatePAProcessFlg(CusconConst.COMMIT_OFF, queryDAO);
   			} catch (Exception e1) {
   				Object[] objectgArray = {e1.getMessage()};
   				log.error(messageAccessor.getMessage("EK071003", objectgArray, uvo));
			}
			System.exit(0);

		} catch(Exception e) {
			// コミット処理失敗ログ出力
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("EK071001", objectgArray, uvo));
			result.setMessage(messageAccessor.getMessage("DK070003", null));
			result.setStatus(CusconConst.FAiILURE);
			// 終了時刻を設定する。
			Date date = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
			result.setEndTime(dateFormat.format(date));
			// コミットステータス確認テーブルを更新する。
			UpdateCommitStatus ucs = new UpdateCommitStatus();
			try {
				ucs.setCommitStatus(updateDAO, result, uvo, messageAccessor);
			} catch (Exception e2) {
				// DBアクセスエラー
				Object[] objectgArray1 = {e2.getMessage()};
				log.fatal(messageAccessor.getMessage("FK180002", objectgArray1, uvo));
			}

			// コミット中フラグOFF
		   	UpdateCommitFlg ucf = new UpdateCommitFlg();
   			try {
   				ucf.updatePAProcessFlg(CusconConst.COMMIT_OFF, queryDAO);
   			} catch (Exception e1) {
   				Object[] objectgArray1 = {e1.getMessage()};
   				log.error(messageAccessor.getMessage("EK071003", objectgArray1, uvo));
   			}
			System.exit(0);
		}

		log.debug("CommitEndBLogic処理終了");
		System.exit(0);
	}

	/**
	 * メソッド名 : updateFWManage
	 * 機能概要 : ポリシー/オブジェクトテーブルをコミット済情報で更新し、<br>
	 *			  世代入替を実施する。
	 * @param resultJudge 処理結果
	 * @throws Exception
	 */
	private static void updateFWManage(ResultJudgeData resultJudge) throws Exception {
		log.debug("updateFWManage処理開始");

		// トランザクションスタート
		TransactionStatus ts = manager.getTransaction(null);

		try {
			// CusconFWSetクラスを生成する。
			CusconFWSet fwset = new CusconFWSet();

			fwset.setFWInfo(updateDAO, queryDAO, uvo, messageAccessor);

			// CusconFWDeleteクラスを生成する。
			CusconFWDelete fwdel = new CusconFWDelete();

			// DBの3世代情報を削除する。
			fwdel.deleteFWInfo(CusconConst.THREE_GENE, updateDAO, queryDAO, uvo, messageAccessor);

			// ポリシー/オブジェクトの世代番号を2⇒3に更新する。
			updateGenerationNo(uvo.getVsysId(), CusconConst.TWO_GENE, uvo);

			// ポリシー/オブジェクトの世代番号を1⇒2に更新する。
			updateGenerationNo(uvo.getVsysId(), CusconConst.ONE_GENE, uvo);

			// ポリシー/オブジェクトの世代番号を0のレコードを世代番号1としてコピーする。
			copyObjectInfo(uvo.getVsysId(), resultJudge.getResultData(), uvo);
		} catch (Exception e) {
			manager.rollback(ts);
			log.debug("updateFWManage rollback 終了");
			throw e;
		}
		manager.commit(ts);
		log.debug("updateFWManage処理終了");
	}

	/**
	 * メソッド名 : updateGenerationNo
	 * 機能概要 : ポリシー/オブジェクトテーブルの入力世代番号のレコードを<br>
	 *			  入力世代番号+1に更新する。
	 * @param vsysId Vsys-ID
	 * @param generationNo 世代番号
	 * @throws Exception
	 */
	private static void updateGenerationNo(String vsysId, int generationNo, CusconUVO uvo) throws Exception {

		log.debug("updateGenerationNo処理開始:vsysId = " + vsysId +
									", generationNo = " + generationNo);
		InputBase base = new InputBase();

		base.setVsysId(vsysId);
		base.setGenerationNo(generationNo);
		base.setNewGenerationNo(generationNo + 1);

		// SQL-ID生成用
		String sqlId;

		// ポリシー/オブジェクトテーブルを更新する。
		for(int i=1; i<9; i++) {
			sqlId = "CommitEndBLogic-" + i;
			log.debug("SQL発行:" + sqlId);
			// 世代番号0のレコードを世代番号1にコピーする。
			try {
				updateDAO.execute(sqlId, base);

			} catch(Exception e) {
				// DBアクセスエラー
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FK071002", objectgArray, uvo));
				throw e;
			}
		}

//2011/06/10 add start inoue@prosite
		// Webフィルタオブジェクト
		sqlId = "UpdateGenarationT_UrlFilters";
		log.debug("SQL発行:" + sqlId);
		// 世代番号の更新
		try {
			updateDAO.execute(sqlId, base);

		} catch(Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK071002", objectgArray, uvo));
			throw e;
		}

		// Webフィルタカテゴリ
		sqlId = "UpdateGenarationT_UrlFilter_Categories";
		log.debug("SQL発行:" + sqlId);
		// 世代番号の更新
		try {
			updateDAO.execute(sqlId, base);

		} catch(Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK071002", objectgArray, uvo));
			throw e;
		}
//2011/06/10 add end inoue@prosite

		log.debug("updateGenerationNo処理終了");
	}

	/**
	 * メソッド名 : copyObjectInfo
	 * 機能概要 : 世代番号0のレコードを世代番号1にコピーする。
	 * @param vsysId Vsys-ID
	 * @param updateTime コミット日時
	 * @throws Exception
	 */
	private static void copyObjectInfo(String vsysId, String updateTime, CusconUVO uvo) throws Exception {

		log.debug("copyObjectInfo処理開始:vsysId = " + vsysId +
				", generationNo = " + updateTime);

		InputBase base = new InputBase();

		base.setVsysId(vsysId);
		base.setGenerationNo(CusconConst.ONE_GENE);
		base.setModFlg(CusconConst.NONE_NUM);

		SecurityRules rules = new SecurityRules();
		rules.setVsysId(vsysId);
		rules.setGenerationNo(CusconConst.ZERO_GENE);
		rules.setNewGenerationNo(CusconConst.ONE_GENE);
		rules.setUpdateTime(updateTime);

		// SQL-ID生成用
		String sqlId;
		// 1世代のポリシー情報を取得する。
		List<SecurityRules> security;

		try {
			// セキュリティポリシー一覧テーブルの世代番号0のレコードを世代番号1にコピーする。
			updateDAO.execute("CommitEndBLogic-9", rules);

			rules.setGenerationNo(CusconConst.ONE_GENE);
			// 1世代のポリシー情報を取得する。
			security =
				queryDAO.executeForObjectList("CommonT_SecurityRules-1", base);
		} catch(Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK071001", objectgArray, uvo));
			throw e;
		}

		rules.setGenerationNo(CusconConst.ZERO_GENE);
		base.setGenerationNo(CusconConst.ZERO_GENE);
		base.setNewGenerationNo(CusconConst.ONE_GENE);

		// セキュリティポリシー一覧テーブル以外のデータをコピーする。
		for(int i=10; i<18; i++) {

			sqlId = "CommitEndBLogic-" + i;
			log.debug("SQL発行:" + sqlId);
			try {
				// 世代番号0のレコードを世代番号1にコピーする。
				updateDAO.execute(sqlId, base);

			} catch(Exception e) {
				// DBアクセスエラー
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FK071003", objectgArray, uvo));
				throw e;
			}

		}
		try {
			// フィルタ登録
			updateDAO.execute("CommitEndBLogic-59", base);
		} catch(Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK071003", objectgArray, uvo));
			throw e;
		}
//2011/06/10 add start inoue@prosite
		try {
			// Webフィルタオブジェクト登録
			updateDAO.execute("CopyGenerationT_UrlFilters", base);
			// Webフィルタカテゴリ登録
			updateDAO.execute("CopyGenerationT_UrlFilter_Categories", base);
			base.setGenerationNo(CusconConst.ONE_GENE);
			log.debug("WebフィルタカテゴリのURLFILTER_SEQNOの更新");
			updateDAO.execute("UpdateT_UrlFilter_Categories_UrlFiltersSeqNo", base);
			log.debug("WebフィルタオブジェクトのORG_SEQNOに0をセット");
			updateDAO.execute("UpdateT_UrlFilters_OrgSeqNo", base);
			// 以降の処理に問題がないように条件の世代を0に戻す。
			base.setGenerationNo(CusconConst.ZERO_GENE);
		} catch(Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK071003", objectgArray, uvo));
			throw e;
		}
//2011/06/10 add end inoue@prosite

		List<Integer> link = null;
		List<Integer> grpLink = null;
		List<String> localAddress = null;

		// ポリシー情報コピー
		for(int i=0; i<security.size(); i++) {

			try {
				/**************************************************
				 *	出所アドレスリンクDB登録
				 **************************************************/
				log.debug("出所アドレスリンク登録:" + i);
				rules.setSeqNo(security.get(i).getSeqNo());
				rules.setName(security.get(i).getName());

				log.debug("アドレスとのリンク登録:" + i);
				link = queryDAO.executeForObjectList("CommitEndBLogic-19", rules);

				if(link != null) {
					for(int j=0; j<link.size(); j++) {
						log.debug("アドレスとのリンク登録:" + j);
						rules.setLinkNo(link.get(j));
						updateDAO.execute("CommitEndBLogic-20", rules);
					}
				}

				grpLink = queryDAO.executeForObjectList("CommitEndBLogic-21", rules);

				if(grpLink != null) {
					for(int j=0; j<grpLink.size(); j++) {
						log.debug("アドレスグループとのリンク登録:" + j);
						rules.setLinkNo(grpLink.get(j));
						updateDAO.execute("CommitEndBLogic-22", rules);
					}
				}

				localAddress =	queryDAO.executeForObjectList("CommitEndBLogic-23", rules);

				if(localAddress != null) {
					log.debug("ローカル出所アドレス件数:" + localAddress.size());
					// ローカル出所アドレス分繰り返す。
					for(int j=0; j<localAddress.size(); j++) {
						log.debug("ローカル出所アドレス存在:" + j);
						Integer srcSeqNo = queryDAO.executeForObject(
								"LocalSourceAddressSeq-1", null, java.lang.Integer.class);

						rules.setLinkNo(srcSeqNo);
						rules.setLocalAddress(localAddress.get(j));
						updateDAO.execute("CommitEndBLogic-24", rules);

						log.debug("ローカル出所アドレスとのリンク登録:" + j);
						updateDAO.execute("CommitEndBLogic-25", rules);
					}
				}

				/**************************************************
				 *	宛先アドレスリンクDB登録
				 **************************************************/
				log.debug("宛先アドレスリンク登録:" + i);

				link = queryDAO.executeForObjectList("CommitEndBLogic-26", rules);

				if(link != null) {
					for(int j=0; j<link.size(); j++) {
						log.debug("アドレスとのリンク登録:" + j);
						rules.setLinkNo(link.get(j));
						updateDAO.execute("CommitEndBLogic-27", rules);
					}
				}

				grpLink = queryDAO.executeForObjectList("CommitEndBLogic-28", rules);
				if(grpLink != null) {
					for(int j=0; j<grpLink.size(); j++) {
						log.debug("アドレスグループとのリンク登録:" + j);
						rules.setLinkNo(grpLink.get(j));
						updateDAO.execute("CommitEndBLogic-29", rules);
					}
				}


				localAddress =	queryDAO.executeForObjectList("CommitEndBLogic-30", rules);

				if(localAddress != null) {
					log.debug("ローカル宛先アドレス件数:" + localAddress.size());
					// ローカル宛先アドレス分繰り返す。
					for(int j=0; j<localAddress.size(); j++) {
						log.debug("ローカル宛先アドレス存在:" + j);
						Integer dstSeqNo = queryDAO.executeForObject(
								"LocalDestinationAddress-1", null, java.lang.Integer.class);

						rules.setLinkNo(dstSeqNo);
						rules.setLocalAddress(localAddress.get(j));
						updateDAO.execute("CommitEndBLogic-31", rules);

						log.debug("ローカル宛先アドレスとのリンク登録:" + i);
						updateDAO.execute("CommitEndBLogic-32", rules);
					}
				}

				/**************************************************
				 *	アプリケーションリンクDB登録
				 **************************************************/
				log.debug("アプリリンク登録:" + i);
				// アプリマスタ登録
				updateDAO.execute("CommitEndBLogic-33", rules);

				grpLink = queryDAO.executeForObjectList("CommitEndBLogic-34", rules);
				if(grpLink != null) {
					for(int j=0; j<grpLink.size(); j++) {
						log.debug("アプリグループとのリンク登録:" + j);
						rules.setLinkNo(grpLink.get(j));
						updateDAO.execute("CommitEndBLogic-35", rules);
					}
				}

				link = queryDAO.executeForObjectList("CommitEndBLogic-36", rules);

				if(link != null) {
					for(int j=0; j<link.size(); j++) {

						log.debug("アプリフィルタとのリンク登録:" + j);
						rules.setLinkNo(link.get(j));
						updateDAO.execute("CommitEndBLogic-37", rules);
					}
				}

				/**************************************************
				 *	サービスリンクDB登録
				 **************************************************/
				log.debug("サービスリンク登録:" + i);
				// サービスマスタ登録
				updateDAO.execute("CommitEndBLogic-38", rules);

				link = queryDAO.executeForObjectList("CommitEndBLogic-39", rules);

				if(link != null) {
					for(int j=0; j<link.size(); j++) {
						log.debug("サービスとのリンク登録:" + j);
						rules.setLinkNo(link.get(j));
						updateDAO.execute("CommitEndBLogic-40", rules);
					}
				}

				grpLink = queryDAO.executeForObjectList("CommitEndBLogic-41", rules);
				if(grpLink != null) {
					for(int j=0; j<grpLink.size(); j++) {
						log.debug("サービスグループとのリンク登録:" + j);
						rules.setLinkNo(grpLink.get(j));
						updateDAO.execute("CommitEndBLogic-42", rules);
					}
				}

				/**************************************************
				 * スケジュールリンクDB更新
				 **************************************************/
				log.debug("1世代スケジュールリンク更新処理");
				if(security.get(i).getSchedulesSeqNo() != null) {
					log.debug("1世代スケジュールリンク設定あり");
					Integer scheduleLink = queryDAO.executeForObject(
								"CommitEndBLogic-43", rules, java.lang.Integer.class);
					rules.setModFlg(CusconConst.MOD_NUM);
					rules.setLinkNo(scheduleLink);
					updateDAO.execute("CommitEndBLogic-44", rules);
				}

//2011/06/10 add start inoue@prosite
				/**************************************************
				 *	WebフィルタリンクDB登録
				 **************************************************/
				log.debug("Webフィルタリンク登録:" + i);
				rules.setSeqNo(security.get(i).getSeqNo());
				rules.setName(security.get(i).getName());

				log.debug("Webフィルタとのリンク登録:" + i);
				link = queryDAO.executeForObjectList("GetT_UrlFilters_SeqNo", rules);

				if(link != null) {
					for(int j=0; j<link.size(); j++) {
						log.debug("Webフィルタとのリンク登録:" + j);
						rules.setLinkNo(link.get(j));
						updateDAO.execute("MakeT_UrlFilterLink_GenerationCopy", rules);
					}
				}
//2011/06/10 add end inoue@prosite

			} catch(Exception e) {
				// DBアクセスエラー
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FK071003", objectgArray, uvo));
				throw e;
			}
		}

		try {
			// オブジェクト情報コピー
			/**************************************************
			 * アドレスグループDB登録
			 **************************************************/
			base.setGenerationNo(CusconConst.ONE_GENE);
			// 1世代のアドレスグループ情報を取得する。
			List<AddressGroups> addressGroups =
				queryDAO.executeForObjectList("CommonT_AddressGroups-1", base);

			for(int i=0; i<addressGroups.size(); i++) {
				log.debug("アドレスグループリンクの登録:" + i);
				rules.setSeqNo(addressGroups.get(i).getSeqNo());
				rules.setName(addressGroups.get(i).getName());

				link = queryDAO.executeForObjectList("CommitEndBLogic-45", rules);

				if(link != null) {
					for(int j=0; j<link.size(); j++) {
						log.debug("アドレスとのリンク登録:" + j);
						rules.setLinkNo(link.get(j));
						updateDAO.execute("CommitEndBLogic-46", rules);
					}
				}

				grpLink = queryDAO.executeForObjectList("CommitEndBLogic-47", rules);
				if(grpLink != null) {
					for(int j=0; j<grpLink.size(); j++) {
						log.debug("アドレスグループとのリンク登録:" + j);
						rules.setLinkNo(grpLink.get(j));
						updateDAO.execute("CommitEndBLogic-48", rules);
					}
				}
			}

			/**************************************************
			 * アプリグループDB登録
			 **************************************************/
			base.setGenerationNo(CusconConst.ONE_GENE);
			// 1世代のアドレスグループ情報を取得する。
			List<ApplicationGroups> applicationGroups =
				queryDAO.executeForObjectList("CommonT_ApplicationGroups-1", base);
			// アプリマスタ登録
			updateDAO.execute("CommitEndBLogic-49", rules);

			for(int i=0; i<applicationGroups.size(); i++) {
				log.debug("アプリグループリンクの登録:" + i);
				rules.setSeqNo(applicationGroups.get(i).getSeqNo());
				rules.setName(applicationGroups.get(i).getName());

				grpLink = queryDAO.executeForObjectList("CommitEndBLogic-50", rules);
				if(grpLink != null) {
					for(int j=0; j<grpLink.size(); j++) {
						log.debug("アプリグループとのリンク登録:" + j);
						rules.setLinkNo(grpLink.get(j));
						updateDAO.execute("CommitEndBLogic-51", rules);
					}
				}

				link = queryDAO.executeForObjectList("CommitEndBLogic-52", rules);
				if(link != null) {
					for(int j=0; j<link.size(); j++) {
						log.debug("アプリフィルタとのリンク登録:" + j);
						rules.setLinkNo(link.get(j));
						updateDAO.execute("CommitEndBLogic-53", rules);
					}
				}
			}

			/**************************************************
			 * サービスグループDB登録
			 **************************************************/
			base.setGenerationNo(CusconConst.ONE_GENE);
			// 1世代のアドレスグループ情報を取得する。
			List<ServiceGroups> serviceGroups =
				queryDAO.executeForObjectList("CommonT_ServicesGroups-1", base);
			// サービスマスタ登録
			updateDAO.execute("CommitEndBLogic-54", rules);

			for(int i=0; i<serviceGroups.size(); i++) {
				log.debug("サービスグループリンクの登録:" + i);
				rules.setSeqNo(serviceGroups.get(i).getSeqNo());
				rules.setName(serviceGroups.get(i).getName());

				link = queryDAO.executeForObjectList("CommitEndBLogic-55", rules);
				if(link != null) {
					for(int j=0; j<link.size(); j++) {
						log.debug("サービスとのリンク登録:" + j);
						rules.setLinkNo(link.get(j));
						updateDAO.execute("CommitEndBLogic-56", rules);
					}
				}

				grpLink = queryDAO.executeForObjectList("CommitEndBLogic-57", rules);
				if(grpLink != null) {
					for(int j=0; j<grpLink.size(); j++) {
						log.debug("サービスグループとのリンク登録:" + j);
						rules.setLinkNo(grpLink.get(j));
						updateDAO.execute("CommitEndBLogic-58", rules);
					}
				}
			}

			/**************************************************
			 * フィルタリンクDB登録
			 **************************************************/

			// アプリケーションフィルタカテゴリリンク
			updateDAO.execute("CommitEndBLogic-60", base);
			// アプリケーションフィルタサブカテゴリリンク
			updateDAO.execute("CommitEndBLogic-61", base);
			// アプリケーションフィルタテクノロジリンク
			updateDAO.execute("CommitEndBLogic-62", base);
			// アプリケーションフィルタリスクリンク
			updateDAO.execute("CommitEndBLogic-63", base);

			/**************************************************
			 * スケジュールリンクDB登録
			 **************************************************/
			// ディリーリンク
			updateDAO.execute("CommitEndBLogic-64", base);
			// ウィークリーリンク
			updateDAO.execute("CommitEndBLogic-65", base);
			// 臨時リンク
			updateDAO.execute("CommitEndBLogic-66", base);
		} catch(Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK071003", objectgArray, uvo));
			throw e;
		}

		log.debug("copyObjectInfo処理終了");
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		CommitEndBLogic.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : updateDAOのSetterメソッド
	 * 機能概要 : updateDAOをセットする。
	 * @param updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		CommitEndBLogic.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}
	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		CommitEndBLogic.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
//2011/06/10 add start inoue@prosite
	/**
	 * メソッド名 : DeleteUrlFilterData
	 * 機能概要 : Webフィルタ関連のデータを削除すると共に
	 * 			  ポリシーテーブルのURLフィルタ適用フラグを0(none)に更新する
	 * @param CusconUVO uvo
	 * @throws Exception
	 */
	private static void DeleteUrlFilterData(CusconUVO uvo) throws Exception {

		log.debug("DeleteUrlFilterData処理開始:vsysId = " + uvo.getVsysId() );
		InputBase base = new InputBase();

		base.setVsysId(uvo.getVsysId());

		try {
			// Webフィルタオブジェクトに紐付くWebフィルタカテゴリを削除する。（VsysID指定）
			updateDAO.execute("DeleteT_UrlFilterCategories-vsysID", base);
			// Webフィルタオブジェクトを削除する。（VsysID指定）
			updateDAO.execute("DeleteT_UrlFilters-vsysID", base);
			// セキュリティポリシー一覧に紐付くWebフィルタリンクを削除する。（VsysID指定）
			updateDAO.execute("DeleteT_UrlFilterLink-vsysID", base);
			// セキュリティポリシーのURLフィルタ適用フラグを0に更新。
			updateDAO.execute("UpDeleteT_SecurityRules-UrlFilterDefaultFlg", base);
		} catch(Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK071002", objectgArray, uvo));
			throw e;
		}
		log.debug("DeleteUrlFilterData処理終了");
	}
//2011/06/10 add end inoue@prosite
}
