/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectSchedules.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/10     inoue@prosite    初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.commitproc.common.vo;

import java.io.Serializable;

/**
 * クラス名 : UrlFilters
 * 機能概要 : Webフィルタカテゴリデータクラス
 * 備考 :
 * @author inoue@prosite
 * @version 1.0 inoue@prosite
 *          Created 2011/06/10
 *          新規作成
 * @see
 */
public class UrlFilterCategories implements Serializable {

	private static final long serialVersionUID = 84203614083433616L;
	// シーケンス番号
	private int seqNo = 0;
	// Vsys-ID
	private String vsysId = null;
	// WebフィルタオブジェクトのSEQNO
	private int urlfiltersSeqno = 0;
	// WebフィルタカテゴリマスタのSEQNO
	private int urlcategorySeqno = 0;
	// 世代番号
	private  int generationNo = 0;
	// アクション
	private  String action = null;
	// カテゴリ名（カテゴリマスタの名称）
	private String name = null;
	// リンクリスト(urlfilter_categoriesテーブル作成時使用）
	private String[] linkNameList = null;

	/**
	 * メソッド名 : seqNoのGetterメソッド
	 * 機能概要 : seqNoを取得する。
	 * @return seqNo
	 */
	public int getSeqNo() {
		return seqNo;
	}
	/**
	 * メソッド名 : seqNoのSetterメソッド
	 * 機能概要 : seqNoをセットする。
	 * @param seqNo
	 */
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}

	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得する。
	 * @return vsysId
	 */
	public String getVsysId() {
		return vsysId;
	}
	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdをセットする。
	 * @param vsysId
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}

	/**
	 * メソッド名 : urlfiltersSeqnoのGetterメソッド
	 * 機能概要 : urlfiltersSeqnoを取得する。
	 * @return urlfiltersSeqno
	 */
	public int getUrlfiltersSeqno() {
		return urlfiltersSeqno;
	}
	/**
	 * メソッド名 : urlfiltersSeqnoのSetterメソッド
	 * 機能概要 : urlfiltersSeqnoをセットする。
	 * @param urlfiltersSeqno
	 */
	public void setUrlfiltersSeqno(int urlfiltersSeqno) {
		this.urlfiltersSeqno = urlfiltersSeqno;
	}

	/**
	 * メソッド名 : urlcategorySeqnoのGetterメソッド
	 * 機能概要 : urlcategorySeqnoを取得する。
	 * @return urlcategorySeqno
	 */
	public int getUrlcategorySeqno() {
		return urlcategorySeqno;
	}
	/**
	 * メソッド名 : urlcategorySeqnoのSetterメソッド
	 * 機能概要 : urlcategorySeqnoをセットする。
	 * @param urlcategorySeqno
	 */
	public void setUrlcategorySeqno(int urlcategorySeqno) {
		this.urlcategorySeqno = urlcategorySeqno;
	}

	/**
	 * メソッド名 : generationNoのGetterメソッド
	 * 機能概要 : generationNoを取得する。
	 * @return generationNo
	 */
	public int getGenerationNo() {
		return generationNo;
	}
	/**
	 * メソッド名 : generationNoのSetterメソッド
	 * 機能概要 : generationNoをセットする。
	 * @param generationNo
	 */
	public void setGenerationNo(int generationNo) {
		this.generationNo = generationNo;
	}

	/**
	 * メソッド名 : actionのGetterメソッド
	 * 機能概要 : actionを取得する。
	 * @return action
	 */
	public String getAction() {
		return action;
	}
	/**
	 * メソッド名 : actionのSetterメソッド
	 * 機能概要 : actionをセットする。
	 * @param action
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * メソッド名 : serialversionuidのGetterメソッド
	 * 機能概要 : serialversionuidを取得する。
	 * @return serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : linkNameListのGetterメソッド
	 * 機能概要 : linkNameListを取得する。
	 * @return linkNameList
	 */
	public String[] getLinkNameList() {
		return linkNameList;
	}
	/**
	 * メソッド名 : linkNameListのSetterメソッド
	 * 機能概要 : linkNameListをセットする。
	 * @param linkNameList
	 */
	public void setLinkNameList(String[] linkNameList) {
		this.linkNameList = linkNameList;
	}
}
