/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CommitEnd.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/06/17     hiroyasu         初版作成
 * 2011/06/10     inoue@prosite    Webフィルタフラグの追加
*******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.commitproc.common;

import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.commitproc.common.vo.CustomerManage;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.InputBase;
import jp.terasoluna.fw.dao.QueryDAO;

public class CreateCusconUVO {

	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.commitproc.common.CreateCusconUVO");
    // メッセージクラス
    private static MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : setCusconUVO
	 * 機能概要 : 企業管理者テーブルを検索してCusconUVO設定処理を行う。
	 * @param queryDAO
	 * @param uvo
	 * @param msgAcc
	 * @throws Exception
	 */
	public void setCusconUVO(QueryDAO queryDAO, CusconUVO uvo,
								MessageAccessor msgAcc) throws Exception {

		log.debug("setCusconUVO処理開始:vsysId = " + uvo.getVsysId());
		messageAccessor = msgAcc;

		InputBase base = new InputBase();
		base.setVsysId(uvo.getVsysId());
		base.setKey("kddi-secgw");

		List<CustomerManage> cmList = null;

        try {
			// PA接続情報を取得する。
			cmList = queryDAO.executeForObjectList(
										"CommonT_Manage_C-1", base);
        } catch (Exception e) {
        	// DBアクセスエラー
        	Object[] objectgArray = {e.getMessage()};
        	log.fatal(messageAccessor.getMessage("FC011004", objectgArray, uvo));
        	throw e;
        }
		// PAログインID設定
        uvo.setPaLoginId(cmList.get(0).getPaLoginId());
		// PAログインパスワード設定
        uvo.setPaPasswd(cmList.get(0).getLoginInitPwd());
//2011/06/10 add start inoue@prosite
		// Webフィルタフラグ
        uvo.setUrlFilteringFlg(cmList.get(0).getUrlFilteringFlg());
//2011/06/10 add end inoue@prosite
	}
}
