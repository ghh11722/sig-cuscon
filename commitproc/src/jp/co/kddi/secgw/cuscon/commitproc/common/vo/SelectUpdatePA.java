/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectUpdatePA.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/20     komakiys                初版作成
 * 2010/06/17     hiroyasu                コミット処理プロセス化対応
 * 2011/06/10     inoue@prosite           Webフィルタオブジェクト対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.commitproc.common.vo;

import java.io.Serializable;
import java.util.List;

/**
 * クラス名 : SelectUpdatePA
 * 機能概要 :
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/20
 *          新規作成
 * @see
 */
public class SelectUpdatePA implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = -5537520067315449584L;

	// オブジェクト名
	private String name;

	// アドレス情報
	private List<Addresses> address = null;

	// アドレスグループ情報
	private List<LinkInfo> addressGroups = null;

	// アプリケーショングループ情報
	private List<LinkInfo> applicationGroups = null;

	// アプリケーションフィルタ情報
	private List<FilterInfo> applicationFilters = null;

	// サービス情報
	private List<Services> services = null;

	// サービスグループ情報
	private List<LinkInfo> serviceGroups = null;

	// スケジュール情報
	private List<Schedules> schedules = null;

//2011.06.10 add start inoue@prosite
	// Webフィルタ情報
	private List<UrlFilters> urlfilters = null;
//2011.06.10 add end inoue@prosite


	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * メソッド名 : addressのGetterメソッド
	 * 機能概要 : addressを取得する。
	 * @return address
	 */
	public List<Addresses> getAddress() {
		return address;
	}

	/**
	 * メソッド名 : addressのSetterメソッド
	 * 機能概要 : addressをセットする。
	 * @param address
	 */
	public void setAddress(List<Addresses> address) {
		this.address = address;
	}

	/**
	 * メソッド名 : addressGroupsのGetterメソッド
	 * 機能概要 : addressGroupsを取得する。
	 * @return addressGroups
	 */
	public List<LinkInfo> getAddressGroups() {
		return addressGroups;
	}

	/**
	 * メソッド名 : addressGroupsのSetterメソッド
	 * 機能概要 : addressGroupsをセットする。
	 * @param addressGroups
	 */
	public void setAddressGroups(List<LinkInfo> addressGroups) {
		this.addressGroups = addressGroups;
	}

	/**
	 * メソッド名 : applicationGroupsのGetterメソッド
	 * 機能概要 : applicationGroupsを取得する。
	 * @return applicationGroups
	 */
	public List<LinkInfo> getApplicationGroups() {
		return applicationGroups;
	}

	/**
	 * メソッド名 : applicationGroupsのSetterメソッド
	 * 機能概要 : applicationGroupsをセットする。
	 * @param applicationGroups
	 */
	public void setApplicationGroups(List<LinkInfo> applicationGroups) {
		this.applicationGroups = applicationGroups;
	}

	/**
	 * メソッド名 : applicationFiltersのGetterメソッド
	 * 機能概要 : applicationFiltersを取得する。
	 * @return applicationFilters
	 */
	public List<FilterInfo> getApplicationFilters() {
		return applicationFilters;
	}

	/**
	 * メソッド名 : applicationFiltersのSetterメソッド
	 * 機能概要 : applicationFiltersをセットする。
	 * @param applicationFilters
	 */
	public void setApplicationFilters(List<FilterInfo> applicationFilters) {
		this.applicationFilters = applicationFilters;
	}

	/**
	 * メソッド名 : servicesのGetterメソッド
	 * 機能概要 : servicesを取得する。
	 * @return services
	 */
	public List<Services> getServices() {
		return services;
	}

	/**
	 * メソッド名 : servicesのSetterメソッド
	 * 機能概要 : servicesをセットする。
	 * @param services
	 */
	public void setServices(List<Services> services) {
		this.services = services;
	}

	/**
	 * メソッド名 : serviceGroupsのGetterメソッド
	 * 機能概要 : serviceGroupsを取得する。
	 * @return serviceGroups
	 */
	public List<LinkInfo> getServiceGroups() {
		return serviceGroups;
	}

	/**
	 * メソッド名 : serviceGroupsのSetterメソッド
	 * 機能概要 : serviceGroupsをセットする。
	 * @param serviceGroups
	 */
	public void setServiceGroups(List<LinkInfo> serviceGroups) {
		this.serviceGroups = serviceGroups;
	}

	/**
	 * メソッド名 : schedulesのGetterメソッド
	 * 機能概要 : schedulesを取得する。
	 * @return schedules
	 */
	public List<Schedules> getSchedules() {
		return schedules;
	}

	/**
	 * メソッド名 : schedulesのSetterメソッド
	 * 機能概要 : schedulesをセットする。
	 * @param schedules
	 */
	public void setSchedules(List<Schedules> schedules) {
		this.schedules = schedules;
	}

//2011.06.10 add start inoue@prosite
	/**
	 * メソッド名 : urlfiltersのSetterメソッド
	 * 機能概要 : urlfiltersをセットする。
	 * @param urlfilters
	 */
	public void setUrlFilters(List<UrlFilters> urlfilters) {
		this.urlfilters = urlfilters;
	}

	/**
	 * メソッド名 : urlfiltersのGetterメソッド
	 * 機能概要 : urlfiltersを取得する。
	 * @return urlfilters
	 */
	public List<UrlFilters> getUrlFilters() {
		return urlfilters;
	}
//2011.06.10 add end inoue@prosite

}
