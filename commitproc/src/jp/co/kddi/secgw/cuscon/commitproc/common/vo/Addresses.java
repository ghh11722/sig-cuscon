/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : Addresses.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/17     morisou          初版作成
 * 2010/06/17     hiroyasu         コミット処理プロセス化対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.commitproc.common.vo;

import java.io.Serializable;

/**
 * クラス名 : Addresses
 * 機能概要 : アドレスオブジェクトデータクラス
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/17
 *          新規作成
 * @see
 */
public class Addresses implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 2979457829852504062L;
	// シーケンス番号
	private int seqNo = 0;
	// Vsys-ID
	private String vsysId = null;
	// オブジェクト名
	private String name = null;
	// 世代番号
	private  int generationNo = 0;
	// タイプ（"ip-netmask"又は"ip-range"）
	private  String type = null;
	// アドレス（IPアドレス(IPv4/6)又は範囲）
	private  String address = null;
	// 変更フラグ
	private  int modFlg = 0;

	/**
	 * メソッド名 : seqNoのGetterメソッド
	 * 機能概要 : seqNoを取得する。
	 * @return seqNo
	 */
	public int getSeqNo() {
		return seqNo;
	}
	/**
	 * メソッド名 : seqNoのSetterメソッド
	 * 機能概要 : seqNoをセットする。
	 * @param seqNo
	 */
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得する。
	 * @return vsysId
	 */
	public String getVsysId() {
		return vsysId;
	}
	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdをセットする。
	 * @param vsysId
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : generationNoのGetterメソッド
	 * 機能概要 : generationNoを取得する。
	 * @return generationNo
	 */
	public int getGenerationNo() {
		return generationNo;
	}
	/**
	 * メソッド名 : generationNoのSetterメソッド
	 * 機能概要 : generationNoをセットする。
	 * @param generationNo
	 */
	public void setGenerationNo(int generationNo) {
		this.generationNo = generationNo;
	}
	/**
	 * メソッド名 : typeのGetterメソッド
	 * 機能概要 : typeを取得する。
	 * @return type
	 */
	public String getType() {
		return type;
	}
	/**
	 * メソッド名 : typeのSetterメソッド
	 * 機能概要 : typeをセットする。
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * メソッド名 : addressのGetterメソッド
	 * 機能概要 : addressを取得する。
	 * @return address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * メソッド名 : addressのSetterメソッド
	 * 機能概要 : addressをセットする。
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * メソッド名 : modFlgのGetterメソッド
	 * 機能概要 : modFlgを取得する。
	 * @return modFlg
	 */
	public int getModFlg() {
		return modFlg;
	}
	/**
	 * メソッド名 : modFlgのSetterメソッド
	 * 機能概要 : modFlgをセットする。
	 * @param modFlg
	 */
	public void setModFlg(int modFlg) {
		this.modFlg = modFlg;
	}
}
