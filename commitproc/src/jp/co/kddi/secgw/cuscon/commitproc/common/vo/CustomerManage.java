/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CustomerManage.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/06/17     hiroyasu                初版作成
 * 2011/06/10     inoue@prosite           Webフィルタフラグの追加
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.commitproc.common.vo;

import java.io.Serializable;

/**
 * クラス名 : CustomerManage
 * 機能概要 : 企業管理者レコード定義
 * 備考 :
 * @author hiroyasu
 * @version 1.0 hiroyasu
 *          Created 2010/06/17
 *          新規作成
 */
public class CustomerManage implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 0L;
	// FireWall-ID
	private String firewallId = null;
	// ログインID
	private String loginId = null;
	// パスワード
	private String loginPwd = null;
	// 初期パスワード
	private String loginInitPwd = null;
	// PAログインID
	private String paLoginId = null;
	// Vsys-ID
	private String vsysId = null;
	// 初期パスワード状態フラグ
	private int initPwdFlg = 0;
//2011/06/10 add start inoue@prosite
	// Webフィルタフラグ(0:OFF 1:ON)
	private int urlFilteringFlg = 0;
//2011/06/10 add end inoue@prosite

	/**
	 * メソッド名 : firewallIdのGetterメソッド
	 * 機能概要 : firewallIdを取得する。
	 * @return firewallId FireWall-ID
	 */
	public String getFirewallId() {
		return firewallId;
	}

	/**
	 * メソッド名 : firewallIdのSetterメソッド
	 * 機能概要 : firewallIdをセットする。
	 * @param firewallId FireWall-ID
	 */
	public void setFirewallId(String firewallId) {
		this.firewallId = firewallId;
	}

	/**
	 * メソッド名 : loginIdのGetterメソッド
	 * 機能概要 : loginIdを取得する。
	 * @return loginId ログインID
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * メソッド名 : loginIdのSetterメソッド
	 * 機能概要 : loginIdをセットする。
	 * @param loginId ログインID
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * メソッド名 : loginPwdのGetterメソッド
	 * 機能概要 : loginPwdを取得する。
	 * @return loginPwd パスワード
	 */
	public String getLoginPwd() {
		return loginPwd;
	}

	/**
	 * メソッド名 : loginPwdのSetterメソッド
	 * 機能概要 : loginPwdをセットする。
	 * @param loginPwd パスワード
	 */
	public void setLoginPwd(String loginPwd) {
		this.loginPwd = loginPwd;
	}

	/**
	 * メソッド名 : loginInitPwdのGetterメソッド
	 * 機能概要 : loginInitPwdを取得する。
	 * @return loginInitPwd 初期パスワード
	 */
	public String getLoginInitPwd() {
		return loginInitPwd;
	}

	/**
	 * メソッド名 : loginInitPwdのSetterメソッド
	 * 機能概要 : loginInitPwdをセットする。
	 * @param loginInitPwd 初期パスワード
	 */
	public void setLoginInitPwd(String loginInitPwd) {
		this.loginInitPwd = loginInitPwd;
	}

	/**
	 * メソッド名 : paLoginIdのGetterメソッド
	 * 機能概要 : paLoginIdを取得する。
	 * @return paLoginId PAログインID
	 */
	public String getPaLoginId() {
		return paLoginId;
	}

	/**
	 * メソッド名 : paLoginIdのSetterメソッド
	 * 機能概要 : paLoginIdをセットする。
	 * @param paLoginId PAログインID
	 */
	public void setPaLoginId(String paLoginId) {
		this.paLoginId = paLoginId;
	}

	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得する。
	 * @return vsysId Vsys-ID
	 */
	public String getVsysId() {
		return vsysId;
	}

	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdをセットする。
	 * @param vsysId Vsys-ID
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}

	/**
	 * メソッド名 : initPwdFlgのGetterメソッド
	 * 機能概要 : initPwdFlgを取得する。
	 * @return initPwdFlg 初期パスワード状態フラグ
	 */
	public int getInitPwdFlg() {
		return initPwdFlg;
	}

	/**
	 * メソッド名 : initPwdFlgのSetterメソッド
	 * 機能概要 : initPwdFlgをセットする。
	 * @param initPwdFlg 初期パスワード状態フラグ
	 */
	public void setInitPwdFlg(int initPwdFlg) {
		this.initPwdFlg = initPwdFlg;
	}

//2011/06/10 add start inoue@prosite
	/**
	 * メソッド名 : urlFilteringFlgのGetterメソッド
	 * 機能概要 : urlFilteringFlgを取得する。
	 * @return urlFilteringFlg 初期パスワード状態フラグ
	 */
	public int getUrlFilteringFlg() {
		return urlFilteringFlg;
	}

	/**
	 * メソッド名 : urlFilteringFlgのSetterメソッド
	 * 機能概要 : urlFilteringFlgをセットする。
	 * @param urlFilteringFlg 初期パスワード状態フラグ
	 */
	public void setUrlFilteringFlg(int urlFilteringFlg) {
		this.urlFilteringFlg = urlFilteringFlg;
	}
//2011/06/10 add end inoue@prosite
}
