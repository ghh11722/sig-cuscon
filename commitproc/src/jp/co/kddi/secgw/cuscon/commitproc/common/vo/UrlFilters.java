/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectSchedules.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/10     inoue@prosite    初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.commitproc.common.vo;

import java.io.Serializable;
import java.util.List;

/**
 * クラス名 : UrlFilters
 * 機能概要 : Webフィルタオブジェクトデータクラス
 * 備考 :
 * @author inoue@prosite
 * @version 1.0 inoue@prosite
 *          Created 2011/06/10
 *          新規作成
 * @see
 */
public class UrlFilters implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = -7566259065093305065L;
	// シーケンス番号
	private int seqNo = 0;
	// Vsys-ID
	private String vsysId = null;
	// オブジェクト名
	private String name = null;
	// 説明
	private String comment = null;
	// 世代番号
	private  int generationNo = 0;
	// ブロックリストサイト
	private String blockListSite = null;
	// ブロックリストサイト数
	private int blockListSiteCnt = 0;
	// ブロックサイト動作
	private String actionBlockListSite = null;
	// アローリストサイト
	private String allowListSite = null;
	// アローリストサイト数
	private int allowListSiteCnt = 0;
	// 変更フラグ
	private  int modFlg = 0;
	// Webフィルタカテゴリ情報
	private List<UrlFilterCategories> urlfiltercategories = null;
	// オリジナルのSEQ_NO
	private int org_seqno = 0;

	/**
	 * メソッド名 : seqNoのGetterメソッド
	 * 機能概要 : seqNoを取得する。
	 * @return seqNo
	 */
	public int getSeqNo() {
		return seqNo;
	}
	/**
	 * メソッド名 : seqNoのSetterメソッド
	 * 機能概要 : seqNoをセットする。
	 * @param seqNo
	 */
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得する。
	 * @return vsysId
	 */
	public String getVsysId() {
		return vsysId;
	}
	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdをセットする。
	 * @param vsysId
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : commentのGetterメソッド
	 * 機能概要 : commentを取得する。
	 * @return comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * メソッド名 : commentのSetterメソッド
	 * 機能概要 : commentをセットする。
	 * @param comment
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	/**
	 * メソッド名 : generationNoのGetterメソッド
	 * 機能概要 : generationNoを取得する。
	 * @return generationNo
	 */
	public int getGenerationNo() {
		return generationNo;
	}
	/**
	 * メソッド名 : generationNoのSetterメソッド
	 * 機能概要 : generationNoをセットする。
	 * @param generationNo
	 */
	public void setGenerationNo(int generationNo) {
		this.generationNo = generationNo;
	}

	/**
	 * メソッド名 : blockListSiteのGetterメソッド
	 * 機能概要 : blockListSiteを取得する。
	 * @return blockListSite
	 */
	public String getBlockListSite() {
		return blockListSite;
	}
	/**
	 * メソッド名 : blockListSiteのSetterメソッド
	 * 機能概要 : blockListSiteをセットする。
	 * @param blockListSite
	 */
	public void setBlockListSite(String blockListSite) {
		this.blockListSite = blockListSite;
	}
	/**
	 * メソッド名 : blockListSiteCntのGetterメソッド
	 * 機能概要 : blockListSiteCntを取得する。
	 * @return blockListSiteCnt
	 */
	public int getBlockListSiteCnt() {
		return blockListSiteCnt;
	}
	/**
	 * メソッド名 : blockListSiteCntのSetterメソッド
	 * 機能概要 : blockListSiteCntをセットする。
	 * @param blockListSiteCnt
	 */
	public void setBlockListSiteCnt(int blockListSiteCnt) {
		this.blockListSiteCnt = blockListSiteCnt;
	}
	/**
	 * メソッド名 : actionBlockListSiteのGetterメソッド
	 * 機能概要 : actionBlockListSiteを取得する。
	 * @return actionBlockListSite
	 */
	public String getActionBlockListSite() {
		return actionBlockListSite;
	}
	/**
	 * メソッド名 : actionBlockListSiteのSetterメソッド
	 * 機能概要 : actionBlockListSiteをセットする。
	 * @param actionBlockListSite
	 */
	public void setActionBlockListSite(String actionBlockListSite) {
		this.actionBlockListSite = actionBlockListSite;
	}
	/**
	 * メソッド名 : allowListSiteのGetterメソッド
	 * 機能概要 : allowListSiteを取得する。
	 * @return allowListSite
	 */
	public String getAllowListSite() {
		return allowListSite;
	}
	/**
	 * メソッド名 : allowListSiteのSetterメソッド
	 * 機能概要 : allowListSiteをセットする。
	 * @param allowListSite
	 */
	public void setAllowListSite(String allowListSite) {
		this.allowListSite = allowListSite;
	}
	/**
	 * メソッド名 : allowListSiteCntのGetterメソッド
	 * 機能概要 : allowListSiteCntを取得する。
	 * @return allowListSiteCnt
	 */
	public int getAllowListSiteCnt() {
		return allowListSiteCnt;
	}
	/**
	 * メソッド名 : allowListSiteCntのSetterメソッド
	 * 機能概要 : allowListSiteCntをセットする。
	 * @param allowListSiteCnt
	 */
	public void setAllowListSiteCnt(int allowListSiteCnt) {
		this.allowListSiteCnt = allowListSiteCnt;
	}
	/**
	 * メソッド名 : modFlgのGetterメソッド
	 * 機能概要 : modFlgを取得する。
	 * @return modFlg
	 */
	public int getModFlg() {
		return modFlg;
	}
	/**
	 * メソッド名 : modFlgのSetterメソッド
	 * 機能概要 : modFlgをセットする。
	 * @param modFlg
	 */
	public void setModFlg(int modFlg) {
		this.modFlg = modFlg;
	}
	/**
	 * メソッド名 : urlfiltercategoriesのGetterメソッド
	 * 機能概要 : urlfiltercategoriesを取得する。
	 * @return urlfiltercategories
	 */
	public List<UrlFilterCategories> getUrlfiltercategories() {
		return urlfiltercategories;
	}
	/**
	 * メソッド名 : urlfiltercategoriesのSetterメソッド
	 * 機能概要 : urlfiltercategoriesをセットする。
	 * @param urlfiltercategories
	 */
	public void setUrlfiltercategories(List<UrlFilterCategories> urlfiltercategories) {
		this.urlfiltercategories = urlfiltercategories;
	}
	/**
	 * メソッド名 : org_seqnoのGetterメソッド
	 * 機能概要 : org_seqnoを取得する。
	 * @return org_seqno
	 */
	public int getOrg_seqno() {
		return org_seqno;
	}
	/**
	 * メソッド名 : org_seqnoのSetterメソッド
	 * 機能概要 : org_seqnoをセットする。
	 * @param org_seqno
	 */
	public void setOrg_seqno(int org_seqno) {
		this.org_seqno = org_seqno;
	}


}
