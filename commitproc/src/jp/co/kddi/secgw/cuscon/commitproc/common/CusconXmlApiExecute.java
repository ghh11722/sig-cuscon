/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CusconXmlApiExecute.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/24     komakiys                初版作成
 * 2010/06/17     hiroyasu                コミット処理プロセス化対応
 * 2019/09/10     Plum Systems Inc.       TLS1.2対応（JDK8移行）
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.commitproc.common;

import java.io.InputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;
import org.apache.log4j.Logger;

/**
 * クラス名 : CusconXmlApiExecute
 * 機能概要 : XML-APIの実行を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/24
 *          新規作成
 *          2.0 T.Yamazaki@Plum Systems Inc.
 *          Updated 2019/09/10
 *          TLS1.2対応
 * @see
 */
public class CusconXmlApiExecute {

	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.commitproc.common.CusconXmlApiExecute");

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 */
	public CusconXmlApiExecute() {
		log.debug("CusconXmlApiExecuteコンストラクタ2処理開始");
		log.debug("CusconXmlApiExecuteコンストラクタ2処理終了");
	}

	/**
	 * メソッド名 : initSSLContext
	 * 機能概要 :
	 * @return SSLContext
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 */
	public static SSLContext initSSLContext() throws NoSuchAlgorithmException, KeyManagementException {
		log.info("initSSLContext処理開始（TLSv1.2対応版）");

		SSLContext ssl = SSLContext.getInstance("TLSv1.2");

		X509TrustManager[] tm = new X509TrustManager[] {
			new X509TrustManager() {

				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] arg0, String arg) {

				}

				public void checkServerTrusted(X509Certificate[] arg0, String arg) {

				}
			}
		};

		ssl.init(null, tm, null);

		HttpsURLConnection.setDefaultHostnameVerifier(
			new HostnameVerifier() {
				public boolean verify(String host1, SSLSession session) {
					log.debug(host1 + " に " + session.getProtocol() + " で接続しています。");
					return true;
				}
			}
		);

		HttpsURLConnection.setDefaultSSLSocketFactory(ssl.getSocketFactory());
		log.debug("initSSLContext処理終了");
		return ssl;
	}

	/**
	 * メソッド名 : doProc
	 * 機能概要 :
	 * @param urls
	 * @return InputStream
	 * @throws Exception
	 */
	public InputStream doProc(String urls, CusconUVO uvo, MessageAccessor messageAccessor) throws Exception {

		log.debug("doProc処理開始:urls = " + urls);
		InputStream input = null;
		try {
			initSSLContext();
			URL url = new URL(urls);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			input = conn.getInputStream();

		} catch (Exception e) {

			// PA接続失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FZ001001", objectgArray, uvo));
			throw e;
		}
		log.debug("doProc処理終了");

	    return input;
	}

}
