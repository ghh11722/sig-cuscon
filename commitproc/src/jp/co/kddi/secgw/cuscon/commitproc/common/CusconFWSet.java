/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CusconFWSet.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/23     komakiys                初版作成
 * 2010/06/17     hiroyasu                コミット処理プロセス化対応
 * 2011/06/10	  inoue@prosite           Webフィルタの追加対応
 * 2012/12/01     kato@PROSITE            PANOS4.1.x対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.commitproc.common;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.commitproc.common.vo.AddressGroups;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.Addresses;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.ApplicationFilters;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.ApplicationGroups;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.DailySchedulesLink;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.LinkInfo;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.NonRecurringSchedulesLink;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.ObjectOldName;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.Schedules;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.SecurityRules;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.ServiceGroups;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.Services;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.WeeklySchedulesLink;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.util.PropertyUtil;

//2011/06/10 add start inoue@prosite
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.UrlFilters;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.UrlFilterCategories;
//2011/06/10 add end inoue@prosite

/**
 * クラス名 : CusconFWSet
 * 機能概要 : FW設定情報DB登録を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/23
 *          新規作成
 * @see
 */
public class CusconFWSet {

    // XML-API実行クラス
	private CusconXmlApiExecute xmlExe = null;
    // XML-API解析クラス
	private CusconXMLAnalyze xmlAna = null;

	// URL結合文字
	private static final String AND = "&";
	// URL使用文字
	private static final String PAR = "]";
	// URL使用文字
	private static final String SLASH = "/";
	// 文字列分活用
	private static final String PHY = "-";
	private static final String AT = "@";
	// ダブルクォーテーション(コマンド付加用)
    private static String QUOT ="\"";

	 private Map.Entry obj;
     // イテレータ
     private Iterator iterator;

     // メッセージクラス
     private static MessageAccessor messageAccessor = null;

     private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.commitproc.common.CusconFWDelete");

//2011/06/10 add start inoue@prosite
     // カンマ
     private static String COMMA =",";
//2011/06/10 add end inoue@prosite

// 20121201 kkato@PROSITE add start
     // XML解析用
     private static final String PORT = "port";
     private static final String PROTOCOL = "protocol";
// 20121201 kkato@PROSITE afd end

     /**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 */
	public CusconFWSet() {
		log.debug("CusconFWSetコンストラクタ2処理開始");
		log.debug("CusconFWSetコンストラクタ2処理終了");
	}



	/**
	 * メソッド名 : setFWInfo
	 * 機能概要 : 指定VsysIDについてFW設定情報DB登録<br>
	 *            (Palo設定情報取得/0世代の削除登録)を行う。
	 * @param updateDAO
	 * @param queryDAO
	 * @param uvo
	 * @throws Exception
	 */
	public void setFWInfo(UpdateDAO updateDAO,
						QueryDAO queryDAO, CusconUVO uvo,
						MessageAccessor msgAcc) throws Exception {

		log.debug("setFWInfo処理開始:vsysId = " + uvo.getVsysId());
		// メッセージクラス設定
		messageAccessor = msgAcc;

		// CusconFWDeleteクラス
		CusconFWDelete fwdel = null;

		// 要求種別
		String TYPE =
	    	PropertyUtil.getProperty("common.xmlgettype");
		// URL(ホスト名前)
		String URL1 =
	    	PropertyUtil.getProperty("common.xmlurl1");
		// URL(ホスト名後)
		String URL2 =
	    	PropertyUtil.getProperty("common.xmlurl2");
		// アクション
		String ACTION =
	    	PropertyUtil.getProperty("common.xmlaction");
		// アクセスキ－設定
		String KEY =
	    	PropertyUtil.getProperty("common.xmlkey");
		// パス
		String XPATH =
	    	PropertyUtil.getProperty("common.xmlxpath");

		if(log.isDebugEnabled()) {
		      log.debug("要求種別:" + TYPE);
		      log.debug("URL(ホスト名前):" + URL1);
		      log.debug("URL(ホスト名後):" + URL2);
		      log.debug("アクション:" + ACTION);
		      log.debug("アクセスキ－設定:" + KEY);
		      log.debug("パス:" + XPATH);
		}

		// アクセスキー
		String accessKey;
		// XML-APIのURL生成用
        String xmlApiConnect;

        // XML-API実行クラスを生成する。
		xmlExe = new CusconXmlApiExecute();
	    // XML-API解析クラスを生成する。
		xmlAna = new CusconXMLAnalyze();
		// CusconFWDeleteクラスを生成する。
		fwdel = new CusconFWDelete();

		// DBの0世代情報を削除する。
		fwdel.deleteFWInfo(CusconConst.ZERO_GENE, updateDAO, queryDAO, uvo, msgAcc);

		CusconAccessKeyCreate access = new CusconAccessKeyCreate();

		// アクセスキーの生成を行う。
		accessKey = access.getAccessKey(null, null, queryDAO, uvo, msgAcc);

		// アクセスキー取得失敗
		if(accessKey == null) {
			// アクセスキー取得失敗ログ出力
			log.error(messageAccessor.getMessage("EZ001001", null, uvo));

			throw new Exception();
		}

		// XML-API FW要求生成URL作成
		xmlApiConnect = URL1 + access.getHost() + URL2  +
				TYPE + AND + ACTION + AND + KEY + accessKey + AND +
					XPATH + QUOT +  uvo.getVsysId() + QUOT + PAR;

		// アドレスオブジェクトの取得＆及びDB登録
		setAddresses(updateDAO, uvo, xmlApiConnect);

		// アドレスグループオブジェクトの取得＆及びDB登録
		setAddressGroups(updateDAO, uvo, xmlApiConnect);

		// アプリケーションフィルタオブジェクトの取得＆及びDB登録
		setApplicationFilter(updateDAO, uvo, xmlApiConnect);

		// アプリケーショングループオブジェクトの取得＆及びDB登録
		setApplicationGroups(updateDAO, uvo, xmlApiConnect);

		// サービスオブジェクトの取得＆及びDB登録
		setServices(updateDAO, uvo, xmlApiConnect);

		// サービスグループオブジェクトの取得＆及びDB登録
		setServiceGroups(updateDAO, uvo, xmlApiConnect);

		// スケジュールオブジェクトの取得＆及びDB登録
		setSchedules(updateDAO, uvo, xmlApiConnect);

//2011/06/10 add start inoue@prosite
		// Webフィルタオブジェクトの取得＆及びDB登録
		setUrlFilters(updateDAO, queryDAO, uvo, xmlApiConnect);
//2011/06/10 add end inoue@prosite

		// ポリシーの取得＆及びDB登録
		setPolicyInfo(updateDAO, queryDAO, uvo, xmlApiConnect);

		log.debug("setFWInfo処理終了");

	}

	/**
	 * メソッド名 : setAddresses
	 * 機能概要 : XML-APIによるアドレスオブジェクト取得、及びDB登録を行う。
	 * @param vsysId Vsys-ID
	 * @param xmlApiConnect XML-API実行URL
	 * @throws Exception
	 */
	private void setAddresses(UpdateDAO updateDAO, CusconUVO uvo, String xmlApiConnect) throws Exception {

		log.debug("setAddresses処理開始:vsysId = " + uvo.getVsysId() +
								", xmlApiConnect = " + xmlApiConnect);
		// アドレスオブジェクト取得用
        String ADDRESS = "address";

        // アドレスオブジェクトデータクラス
        Addresses address = new Addresses();
        // オブジェクト旧名データクラス
        ObjectOldName objectOldName = new ObjectOldName();

        // XML解析レスポンス
        List<HashMap<String, Object>> objAnalyse;
        // XML-API取得
        String objConnect;

		// 20121201 kkato@PROSITE mod start
		//objConnect = xmlApiConnect + SLASH + ADDRESS;
		// XML解析を行う
		//objAnalyse = getXmlResult(objConnect, uvo);
		objAnalyse = getXmlResult(xmlApiConnect, uvo, ADDRESS);
		// 20121201 kkato@PROSITE mod end

		// アドレスの基本登録情報を設定する。
		address.setVsysId(uvo.getVsysId());
		address.setModFlg(CusconConst.NONE_NUM);
		address.setGenerationNo(CusconConst.ZERO_GENE);
		objectOldName.setVsysId(uvo.getVsysId());
		objectOldName.setObjectType(CusconConst.Address);

		if(objAnalyse != null) {
			log.debug("PAにアドレスが存在");
			// XML解析レスポンスのデータ分繰り返す。
			for(int i=0; i<objAnalyse.size(); i++) {
				log.debug("XMLレスポンス解析:" + i);
				iterator = objAnalyse.get(i).entrySet().iterator();

				// ハッシュマップのデータ分繰り返す。
				while (iterator.hasNext()) {
					log.debug("ハッシュマップ");
					obj = (Map.Entry)iterator.next();
					// キーがnameの場合
					if(obj.getKey().equals(CusconConst.NAME)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						address.setName(obj.getValue().toString());
						objectOldName.setName(obj.getValue().toString());
					// キーがname以外の場合
					} else {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						address.setType(obj.getKey().toString());
						address.setAddress(obj.getValue().toString());
					}
				}

				try {
					// データをDBに登録する。
					updateDAO.execute("AddressesBLogic-5", address);
					updateDAO.execute("CommonT_ObjectOldName-1", objectOldName);
				} catch (Exception e) {
					// DBアクセスエラー
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage("FZ001004", objectgArray, uvo));
					throw e;
				}
			}
		}

		log.debug("setAddresses処理終了");
	}

	/**
	 * メソッド名 : setAddressGroups
	 * 機能概要 : XML-APIによるアドレスグループオブジェクト取得、及びDB登録を行う。
	 * @param vsysId Vsys-ID
	 * @param xmlApiConnect XML-API実行URL
	 * @throws Exception
	 */
	private void setAddressGroups(UpdateDAO updateDAO, CusconUVO uvo, String xmlApiConnect) throws Exception {

		log.debug("setAddressGroups処理開始:vsysId = " + uvo.getVsysId() +
				", xmlApiConnect = " + xmlApiConnect);
		// アドレスグループオブジェクト取得用
        String ADDRESS_GRP = "address-group";

        // アドレスグループオブジェクトデータクラス
        AddressGroups addressGrp = new AddressGroups();
        // オブジェクト旧名データクラス
        ObjectOldName objectOldName = new ObjectOldName();

        // XML解析レスポンス
        List<HashMap<String, Object>> objAnalyse;
        // XML-API取得
        String objConnect;

        // 20121201 kkato@PROSITE mod start
		//objConnect = xmlApiConnect + SLASH + ADDRESS_GRP;
		// XML解析を行う
		//objAnalyse = getXmlResult(objConnect, uvo);
		objAnalyse = getXmlResult(xmlApiConnect, uvo, ADDRESS_GRP);
	    // 20121201 kkato@PROSITE mod end

		// アドレスグループの基本登録情報を設定する。
		addressGrp.setVsysId(uvo.getVsysId());
		addressGrp.setModFlg(CusconConst.NONE_NUM);
		addressGrp.setGenerationNo(CusconConst.ZERO_GENE);
		objectOldName.setVsysId(uvo.getVsysId());
		objectOldName.setObjectType(CusconConst.AddressGrp);

		if(objAnalyse != null) {
			log.debug("PAにアドレスグループが存在");
			try {
				List<LinkInfo> objLinkList = new ArrayList<LinkInfo>();
				// XML解析レスポンスのデータ分繰り返す。
				for(int i=0; i<objAnalyse.size(); i++) {
					log.debug("XMLレスポンス解析:" + i);
					iterator = objAnalyse.get(i).entrySet().iterator();
			        // リンクデータクラス
			        LinkInfo objLink = new LinkInfo();
					// リンクの基本登録情報を設定する。
					objLink.setVsysId(uvo.getVsysId());
					objLink.setGenerationNo(CusconConst.ZERO_GENE);

					// ハッシュマップのデータ分繰り返す。
					while (iterator.hasNext()) {
						log.debug("ハッシュマップ");
						obj = (Map.Entry)iterator.next();
						// キーがnameの場合
						if(obj.getKey().equals(CusconConst.NAME)) {
							log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
							addressGrp.setName(obj.getValue().toString());
							objectOldName.setName(obj.getValue().toString());
							objLink.setName(obj.getValue().toString());
							objLink.setName(obj.getValue().toString());
						} else {
							log.debug("key:" + obj.getKey());
							objLink.setLinkNameList((List<String>)obj.getValue());
						}
					}
					// アドレスグループオブジェクトをDBに登録する。
					updateDAO.execute("CommonT_AddressGroups-5", addressGrp);
					updateDAO.execute("CommonT_ObjectOldName-1", objectOldName);
					// アドレスグループリンクリスト追加
					objLinkList.add(objLink);
				}
				for(int i=0; i<objLinkList.size(); i++) {
					log.debug("アドレスグループリンク登録" + i);
					// アドレスグループリンクをDBに登録する。
					updateDAO.execute("CommonT_AddressGroupsLink-2", objLinkList.get(i));
				}
			} catch(Exception e) {
				// DBアクセスエラー
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FZ001005", objectgArray, uvo));
				throw e;
			}
		}

		log.debug("setAddressGroups処理終了");
	}

	/**
	 * メソッド名 : setApplicationFilter
	 * 機能概要 : XML-APIによるアプリケーションフィルタオブジェクト取得、及びDB登録を行う。
	 * @param vsysId Vsys-ID
	 * @param xmlApiConnect XML-API実行URL
	 * @throws Exception
	 */
	private void setApplicationFilter(UpdateDAO updateDAO, CusconUVO uvo, String xmlApiConnect) throws Exception {

		log.debug("setApplicationFilter処理開始:vsysId = " + uvo.getVsysId() +
				", xmlApiConnect = " + xmlApiConnect);
		// アプリケーションフィルタオブジェクト取得用
        String APPLI_FIL = "application-filter";

        // アプリケーションフィルタオブジェクトデータクラス
        ApplicationFilters appliFil = new ApplicationFilters();
        // リンクデータクラス
        LinkInfo categoryLink = new LinkInfo();
        LinkInfo subCategoryLink = new LinkInfo();
        LinkInfo technologyLink = new LinkInfo();
        LinkInfo riskLink = new LinkInfo();
        // オブジェクト旧名データクラス
        ObjectOldName objectOldName = new ObjectOldName();

        // XML解析レスポンス
        List<HashMap<String, Object>> objAnalyse;
        // XML-API取得
        String objConnect;

        // 20121201 kkato@PROSITE mod start
        //objConnect = xmlApiConnect + SLASH + APPLI_FIL;
        // XML解析を行う
		//objAnalyse = getXmlResult(objConnect, uvo);
		objAnalyse = getXmlResult(xmlApiConnect, uvo, APPLI_FIL);
		// 20121201 kkato@PROSITE mod end

		// アプリケーションフィルタの基本登録情報を設定する。
		appliFil.setVsysId(uvo.getVsysId());
		appliFil.setModFlg(CusconConst.NONE_NUM);
		appliFil.setGenerationNo(CusconConst.ZERO_GENE);
		objectOldName.setVsysId(uvo.getVsysId());
		objectOldName.setObjectType(CusconConst.ApplFlt);

		if(objAnalyse != null) {
			log.debug("PAにアプリフィルタが存在");
			// XML解析レスポンスのデータ分繰り返す。
			for(int i=0; i<objAnalyse.size(); i++) {
				log.debug("XMLレスポンス解析:" + i);
				iterator = objAnalyse.get(i).entrySet().iterator();
				categoryLink = new LinkInfo();
				subCategoryLink = new LinkInfo();
				technologyLink = new LinkInfo();
				riskLink = new LinkInfo();

				// ハッシュマップのデータ分繰り返す。
				while (iterator.hasNext()) {
					log.debug("ハッシュマップ");
					obj = (Map.Entry)iterator.next();
					// キーがnameの場合
					if(obj.getKey().equals(CusconConst.NAME)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						appliFil.setName(obj.getValue().toString());
						objectOldName.setName(obj.getValue().toString());
						categoryLink.setName(obj.getValue().toString());
						subCategoryLink.setName(obj.getValue().toString());
						technologyLink.setName(obj.getValue().toString());
						riskLink.setName(obj.getValue().toString());
					// キーがcategoryの場合
					} else if(obj.getKey().equals(CusconConst.CATEGORY)){
						log.debug("key:" + obj.getKey());
						categoryLink.setLinkNameList((List<String>)obj.getValue());
					// キーがsubcategoryの場合
						log.debug("key:" + obj.getKey());
					} else if(obj.getKey().equals(CusconConst.SUBCATEGORY)){
						log.debug("key:" + obj.getKey());
						subCategoryLink.setLinkNameList(
										(List<String>)obj.getValue());
					// キーがtechnologyの場合
					} else if(obj.getKey().equals(CusconConst.TECHNOLOGY)){
						log.debug("key:" + obj.getKey());
						technologyLink.setLinkNameList(
										(List<String>)obj.getValue());
					// キーがriskの場合
					} else if(obj.getKey().equals(CusconConst.RISK)) {
						log.debug("key:" + obj.getKey());
						riskLink.setLinkNameList((List<String>)obj.getValue());
					} else {
						log.debug("その他のデータ");
					}
				}

				try {
					// データをDBに登録する。
					updateDAO.execute("CommonT_ApplicationFilters-5", appliFil);
					updateDAO.execute("CommonT_ObjectOldName-1", objectOldName);
				} catch (Exception e) {
					// DBアクセスエラー
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage(
											"FZ001006", objectgArray, uvo));
					throw e;
				}

				// カテゴリリンクが存在する場合
				if(categoryLink.getLinkNameList() != null) {
					log.debug("登録対象カテゴリが存在");
					// カテゴリリンクをDBに登録する。
					for(int j=0; j<categoryLink.getLinkNameList().size(); j++) {
						log.debug("カテゴリリンクDB登録:" + i);

						// アプリケーションカテゴリリンクの基本登録情報を設定する。
						categoryLink.setVsysId(uvo.getVsysId());
						categoryLink.setGenerationNo(CusconConst.ZERO_GENE);
						categoryLink.setLinkName(
								categoryLink.getLinkNameList().get(j));
						try {
							updateDAO.execute(
									"CommonT_AplFltCategoryLink-2", categoryLink);
						} catch (Exception e) {
							// DBアクセスエラー
							Object[] objectgArray = {e.getMessage()};
							log.fatal(messageAccessor.getMessage(
												"FZ001006", objectgArray, uvo));
							throw e;
						}
					}
				}

				// サブカテゴリリンクが存在する場合
				if(subCategoryLink.getLinkNameList() != null) {
					log.debug("登録対象サブカテゴリが存在");
					// サブカテゴリリンクをDBに登録する。
					for(int j=0; j<subCategoryLink.getLinkNameList().size(); j++) {
						log.debug("サブカテゴリリンクDB登録:" + i);

						// アプリケーションサブカテゴリリンクの基本登録情報を設定する。
						subCategoryLink.setVsysId(uvo.getVsysId());
						subCategoryLink.setGenerationNo(CusconConst.ZERO_GENE);
						subCategoryLink.setLinkName(
								subCategoryLink.getLinkNameList().get(j));
						try {
							updateDAO.execute(
								"CommonT_AplFltSubCategoryLink-2", subCategoryLink);
						} catch (Exception e) {
							// DBアクセスエラー
							Object[] objectgArray = {e.getMessage()};
							log.fatal(messageAccessor.getMessage(
												"FZ001006", objectgArray, uvo));
							throw e;
						}
					}
				}

				// テクノロジリンクが存在する場合
				if(technologyLink.getLinkNameList() != null) {
					log.debug("登録対象テクノロジが存在");
					// テクノロジリンクをDBに登録する。
					for(int j=0; j<technologyLink.getLinkNameList().size(); j++) {
						log.debug("テクノロジリンクDB登録:" + i);

						// アプリケーションテクノロジリンクの基本登録情報を設定する。
						technologyLink.setVsysId(uvo.getVsysId());
						technologyLink.setGenerationNo(CusconConst.ZERO_GENE);
						technologyLink.setLinkName(
								technologyLink.getLinkNameList().get(j));
						try {
							updateDAO.execute(
									"CommonT_AplFltTechnologyLink-2", technologyLink);
						} catch (Exception e) {
							// DBアクセスエラー
							Object[] objectgArray = {e.getMessage()};
							log.fatal(messageAccessor.getMessage(
											"FZ001006", objectgArray, uvo));
							throw e;
						}
					}
				}

				// リスクリンクが存在する場合
				if(riskLink.getLinkNameList() != null) {
					log.debug("登録対象リスクが存在");
					// リスクリンクをDBに登録する。
					for(int j=0; j<riskLink.getLinkNameList().size(); j++) {
						log.debug("リスクリンクDB登録:" + i);

						// アプリケーションリスクリンクの基本登録情報を設定する。
						riskLink.setVsysId(uvo.getVsysId());
						riskLink.setGenerationNo(CusconConst.ZERO_GENE);
						riskLink.setLinkName(riskLink.getLinkNameList().get(j));
						try {
							updateDAO.execute("CommonT_AplFltRiskLink-2", riskLink);
						} catch (Exception e) {
							// DBアクセスエラー
							Object[] objectgArray = {e.getMessage()};
							log.fatal(messageAccessor.getMessage(
											"FZ001006", objectgArray, uvo));
							throw e;
						}
					}
				}
			}
		}
		log.debug("setApplicationFilter処理終了");
	}

	/**
	 * メソッド名 : setApplicationGroups
	 * 機能概要 : XML-APIによるアプリケーショングループオブジェクト取得、及びDB登録を行う。
	 * @param vsysId Vsys-ID
	 * @param xmlApiConnect XML-API実行URL
	 * @throws Exception
	 */
	private void setApplicationGroups(UpdateDAO updateDAO, CusconUVO uvo, String xmlApiConnect) throws Exception {

		log.debug("setApplicationGroups処理開始:vsysId = " + uvo.getVsysId() +
				", xmlApiConnect = " + xmlApiConnect);
		// アプリケーショングループオブジェクト取得用
        String APPLI_GRP = "application-group";

        // アプリケーショングループオブジェクトデータクラス
        ApplicationGroups appliGrp = new ApplicationGroups();
        // オブジェクト旧名データクラス
        ObjectOldName objectOldName = new ObjectOldName();

        // XML解析レスポンス
        List<HashMap<String, Object>> objAnalyse;
        // XML-API取得
        String objConnect;

		// 20121201 kkato@PROSITE mod start
		//objConnect = xmlApiConnect + SLASH + APPLI_GRP;
		// XML解析を行う
		//objAnalyse = getXmlResult(objConnect, uvo);
		objAnalyse = getXmlResult(xmlApiConnect, uvo, APPLI_GRP);
		// 20121201 kkato@PROSITE mod end

		// アプリケーショングループリンクの基本登録情報を設定する。
		appliGrp.setVsysId(uvo.getVsysId());
		appliGrp.setModFlg(CusconConst.NONE_NUM);
		appliGrp.setGenerationNo(CusconConst.ZERO_GENE);
		objectOldName.setVsysId(uvo.getVsysId());
		objectOldName.setObjectType(CusconConst.ApplGrp);

		if(objAnalyse != null) {
			log.debug("PAにアプリグループが存在");
			try {
				List<LinkInfo> objLinkList = new ArrayList<LinkInfo>();
				// XML解析レスポンスのデータ分繰り返す。
				for(int i=0; i<objAnalyse.size(); i++) {
					log.debug("XMLレスポンス解析:" + i);
					iterator = objAnalyse.get(i).entrySet().iterator();
			        // リンクデータクラス
			        LinkInfo objLink = new LinkInfo();
					// リンクの基本登録情報を設定する。
					objLink.setVsysId(uvo.getVsysId());
					objLink.setGenerationNo(CusconConst.ZERO_GENE);

					// ハッシュマップのデータ分繰り返す。
					while (iterator.hasNext()) {
						log.debug("ハッシュマップ");
						obj = (Map.Entry)iterator.next();
						// キーがnameの場合
						if(obj.getKey().equals(CusconConst.NAME)) {
							log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
							appliGrp.setName(obj.getValue().toString());
							objectOldName.setName(obj.getValue().toString());
							objLink.setName(obj.getValue().toString());
						} else {
							log.debug("key:" + obj.getKey());
							objLink.setLinkNameList((List<String>)obj.getValue());
						}
					}
					// アプリケーショングループオブジェクトをDBに登録する。
					updateDAO.execute("CommonT_ApplicationGroups-5", appliGrp);
					updateDAO.execute("CommonT_ObjectOldName-1", objectOldName);
					objLinkList.add(objLink);
				}
				for (int i = 0; i < objLinkList.size(); i++) {
					// アプリケーショングループリンクをDBに登録する。
					log.debug("グループリンク登録");
					updateDAO.execute("CommonT_ApplicationGroupsLink-2", objLinkList.get(i));
				}
			} catch (Exception e) {
				// DBアクセスエラー
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FZ001007", objectgArray, uvo));
				throw e;
			}
		}
		log.debug("setApplicationGroups処理終了");
	}

	/**
	 * メソッド名 : setServices
	 * 機能概要 : XML-APIによるサービスオブジェクト取得、及びDB登録を行う。
	 * @param vsysId Vsys-ID
	 * @param xmlApiConnect XML-API実行URL
	 * @throws Exception
	 */
	private void setServices(UpdateDAO updateDAO, CusconUVO uvo, String xmlApiConnect) throws Exception {

		log.debug("setServices処理開始:vsysId = " + uvo.getVsysId() +
									", xmlApiConnect = " + xmlApiConnect);
		 // サービスオブジェクト取得用
        String SERVICE = "service";

        // サービスオブジェクトデータクラス
        Services service = new Services();
        // オブジェクト旧名データクラス
        ObjectOldName objectOldName = new ObjectOldName();

        // XML解析レスポンス
        List<HashMap<String, Object>> objAnalyse;
        // XML-API取得
        String objConnect;

		// 20121201 kkato@PROSITE mod start
		//objConnect = xmlApiConnect + SLASH + SERVICE;
		// XML解析を行う
		//objAnalyse = getXmlResult(objConnect, uvo);
		objAnalyse = getXmlResult(xmlApiConnect, uvo, SERVICE);
		// 20121201 kkato@PROSITE mod end

		// サービスの基本登録情報を設定する。
		service.setVsysId(uvo.getVsysId());
		service.setModFlg(CusconConst.NONE_NUM);
		service.setGenerationNo(CusconConst.ZERO_GENE);
		objectOldName.setVsysId(uvo.getVsysId());
		objectOldName.setObjectType(CusconConst.Service);

		if(objAnalyse != null) {
			log.debug("PAにサービスが存在");
			// XML解析レスポンスのデータ分繰り返す。
			for(int i=0; i<objAnalyse.size(); i++) {
				log.debug("XMLレスポンス解析:" + i);
				iterator = objAnalyse.get(i).entrySet().iterator();

				// ハッシュマップのデータ分繰り返す。
				while (iterator.hasNext()) {
					log.debug("ハッシュマップ");
					obj = (Map.Entry)iterator.next();
					// キーがnameの場合
					if(obj.getKey().equals(CusconConst.NAME)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						service.setName(obj.getValue().toString());
						objectOldName.setName(obj.getValue().toString());
// 20121201 kkato@PROSITE mod start
//					// キーがtcpの場合
//					} else if(obj.getKey().equals(CusconConst.TCP)){
//						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
//						service.setProtocol(obj.getKey().toString());
//						service.setPort(obj.getValue().toString());
//					// キーがudpの場合
//					} else {
//						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
//						service.setProtocol(obj.getKey().toString());
//						service.setPort(obj.getValue().toString());
					// キーがPROTCOLの場合
					} else if(obj.getKey().equals(PROTOCOL)){
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						service.setProtocol(obj.getValue().toString());
					// キーがPORTの場合
					} else if(obj.getKey().equals(PORT)){
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						service.setPort(obj.getValue().toString());
// 20121201 kkato@PROSITE mod end
					}
				}
				try {
					// サービスオブジェクトをDBに登録する。
					updateDAO.execute("ServicesBLogic-6", service);
					updateDAO.execute("CommonT_ObjectOldName-1", objectOldName);
				} catch(Exception e) {
					// DBアクセスエラー
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage("FZ001008", objectgArray, uvo));
					throw e;
				}
			}
		}
		log.debug("setServices処理終了");

	}

	/**
	 * メソッド名 : setServiceGroups
	 * 機能概要 : XML-APIによるサービスグループオブジェクト取得、及びDB登録を行う。
	 * @param vsysId Vsys-ID
	 * @param xmlApiConnect XML-API実行URL
	 * @throws Exception
	 */
	private void setServiceGroups(UpdateDAO updateDAO, CusconUVO uvo, String xmlApiConnect) throws Exception {

		log.debug("setServiceGroups処理開始:vsysId = " + uvo.getVsysId() +
									", xmlApiConnect = " + xmlApiConnect);
		// サービスグループオブジェクト取得用
        String SERVICE_GRP = "service-group";
        // サービスグループオブジェクトデータクラス
        ServiceGroups serviceGrp = new ServiceGroups();
        // オブジェクト旧名データクラス
        ObjectOldName objectOldName = new ObjectOldName();

        // XML解析レスポンス
        List<HashMap<String, Object>> objAnalyse;
        // XML-API取得
        String objConnect;

		// 20121201 kkato@PROSITE mod start
		//objConnect = xmlApiConnect + SLASH + SERVICE_GRP;
		// XML解析を行う
		//objAnalyse = getXmlResult(objConnect, uvo);
		objAnalyse = getXmlResult(xmlApiConnect, uvo, SERVICE_GRP);
		// 20121201 kkato@PROSITE mod end

		// サービスグループの基本登録情報を設定する。
		serviceGrp.setVsysId(uvo.getVsysId());
		serviceGrp.setModFlg(CusconConst.NONE_NUM);
		serviceGrp.setGenerationNo(CusconConst.ZERO_GENE);
		objectOldName.setVsysId(uvo.getVsysId());
		objectOldName.setObjectType(CusconConst.ServiceGrp);

		if(objAnalyse != null) {
			log.debug("PAにサービスグループが存在");
			List<LinkInfo> objLinkList = new ArrayList<LinkInfo>();
			try {
				// XML解析レスポンスのデータ分繰り返す。
				for(int i=0; i<objAnalyse.size(); i++) {
					log.debug("XMLレスポンス解析:" + i);
					iterator = objAnalyse.get(i).entrySet().iterator();
			        // リンクデータクラス
			        LinkInfo objLink = new LinkInfo();
					// リンクの基本登録情報を設定する。
					objLink.setVsysId(uvo.getVsysId());
					objLink.setGenerationNo(CusconConst.ZERO_GENE);

					// ハッシュマップのデータ分繰り返す。
					while (iterator.hasNext()) {
						log.debug("ハッシュマップ");
						obj = (Map.Entry)iterator.next();
						// キーがnameの場合
						if(obj.getKey().equals(CusconConst.NAME)) {
							log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
							serviceGrp.setName(obj.getValue().toString());
							objectOldName.setName(obj.getValue().toString());
							objLink.setName(obj.getValue().toString());
							// キーがentryの場合
						} else {
							log.debug("key:" + obj.getKey());
							objLink.setLinkNameList((List<String>)obj.getValue());
						}
					}
					// サービスグループオブジェクトをDBに登録する。
					updateDAO.execute("CommonT_ServicesGroups-5", serviceGrp);
					updateDAO.execute("CommonT_ObjectOldName-1", objectOldName);
					objLinkList.add(objLink);
				}
				for (int i =0; i < objLinkList.size(); i++) {
					log.debug("グループリンクDB登録");
					// サービスグループリンクをDBに登録する。
					updateDAO.execute("CommonT_ServicesGroupsLink-2", objLinkList.get(i));
				}
			} catch (Exception e) {
					// DBアクセスエラー
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage("FZ001009", objectgArray, uvo));
					throw e;
			}
		}
		log.debug("setServiceGroups処理終了");
	}

	/**
	 * メソッド名 : setSchedules
	 * 機能概要 : XML-APIによるスケジュールオブジェクト取得、及びDB登録を行う。
	 * @param vsysId Vsys-ID
	 * @param xmlApiConnect XML-API実行URL
	 * @throws Exception
	 */
	private void setSchedules(UpdateDAO updateDAO, CusconUVO uvo, String xmlApiConnect) throws Exception {

		log.debug("setSchedules処理開始:vsysId = " + uvo.getVsysId() +
								", xmlApiConnect = " + xmlApiConnect);
		// スケジュールオブジェクト取得用
        String SCHEDULE = "schedule";

        // スケジュールオブジェクトデータクラス
        Schedules schedule = new Schedules();
        // スケジュールリンククラス
        DailySchedulesLink dailySchedule = new DailySchedulesLink();
        WeeklySchedulesLink weeklySchedule = new WeeklySchedulesLink();
        NonRecurringSchedulesLink nonRecSchedule =
        								new NonRecurringSchedulesLink();
        // オブジェクト旧名データクラス
        ObjectOldName objectOldName = new ObjectOldName();

        // XML解析レスポンス
        List<HashMap<String, Object>> objAnalyse;
        // XML-API取得
        String objConnect;

        // スケジュール一時時刻分割用
        List<String> tmpList;
        // スケジュール開始時刻分割用
		List<String> startTimeList;
		// スケジュール終了時刻分割用
		List<String> endTimeList;
		 // スケジュール開始日時分割用
		List<String> startDateList;
		// スケジュール終了日時分割用
		List<String> endDateList;
		// ウィークリー処理一時用
		List<WeeklySchedulesLink> tmpWeeklyList = null;

		// 20121201 kkato@PROSITE mod start
		//objConnect = xmlApiConnect + SLASH + SCHEDULE;
		// XML解析を行う
		//objAnalyse = getXmlResult(objConnect, uvo);
		objAnalyse = getXmlResult(xmlApiConnect, uvo, SCHEDULE);
		// 20121201 kkato@PROSITE mod end

		// スケジュールの基本登録情報を設定する。
		schedule.setVsysId(uvo.getVsysId());
		schedule.setModFlg(CusconConst.NONE_NUM);
		schedule.setGenerationNo(CusconConst.ZERO_GENE);
		objectOldName.setVsysId(uvo.getVsysId());
		objectOldName.setObjectType(CusconConst.Schedule);

		if(objAnalyse != null) {
			log.debug("PAにスケジュールが存在");
			// XML解析レスポンスのデータ分繰り返す。
			for(int i=0; i<objAnalyse.size(); i++) {
				log.debug("XMLレスポンス解析:" + i);
				iterator = objAnalyse.get(i).entrySet().iterator();
				tmpWeeklyList = new ArrayList<WeeklySchedulesLink>();
				dailySchedule = new DailySchedulesLink();
				nonRecSchedule = new NonRecurringSchedulesLink();
				// ハッシュマップのデータ分繰り返す。
				while (iterator.hasNext()) {
					log.debug("ハッシュマップ");
					obj = (Map.Entry)iterator.next();

					weeklySchedule = new WeeklySchedulesLink();
					// キーがnameの場合
					if(obj.getKey().equals(CusconConst.NAME)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						schedule.setName(obj.getValue().toString());
						objectOldName.setName(obj.getValue().toString());
						dailySchedule.setName(obj.getValue().toString());
						weeklySchedule.setName(obj.getValue().toString());
						nonRecSchedule.setName(obj.getValue().toString());
					// キーがdailyの場合
					} else if(obj.getKey().equals(CusconConst.DAILY_STR)) {
						log.debug("key:" + obj.getKey());
						schedule.setRecurrence(CusconConst.DAILY);

						tmpList = (List<String>)obj.getValue();
						startTimeList = new ArrayList<String>();
						endTimeList = new ArrayList<String>();

						// スケジュール時刻リスト分繰り返す。
						for(int j=0; j<tmpList.size(); j++) {
							log.debug("時刻リスト:" + j);
							// 開始時刻、終了時刻に分割する。
							String[] split_Str = tmpList.get(j).split(PHY);
							startTimeList.add(split_Str[0]);
							endTimeList.add(split_Str[1]);
						}
						// 開始時刻、終了時刻を格納する。
						dailySchedule.setStartTimeList(startTimeList);
						dailySchedule.setEndTimeList(endTimeList);

					// キーがnon-recurringの場合
					} else if(obj.getKey().equals(CusconConst.NONRECURRING_STR)) {
						log.debug("key:" + obj.getKey());
						schedule.setRecurrence(CusconConst.NONRECURRING);

						tmpList = (List<String>)obj.getValue();
						startDateList = new ArrayList<String>();
						startTimeList = new ArrayList<String>();
						endDateList = new ArrayList<String>();
						endTimeList = new ArrayList<String>();

						// スケジュール時刻リスト分繰り返す。
						for(int j=0; j<tmpList.size(); j++) {
							log.debug("時刻リスト:" + j);
							// 開始日時、開始時刻、終了日時、終了時刻に分割する。
							String[] splitAtStr = tmpList.get(j).split(AT);
							String[] split_Str = splitAtStr[1].split(PHY);

							startDateList.add(splitAtStr[0]);
							startTimeList.add(split_Str[0]);
							endDateList.add(split_Str[1]);
							endTimeList.add(splitAtStr[2]);
						}
						// 開始日時、開始時刻、終了日時、終了時刻を格納する。
						nonRecSchedule.setStartDateList(startDateList);
						nonRecSchedule.setStartTimeList(startTimeList);
						nonRecSchedule.setEndDateList(endDateList);
						nonRecSchedule.setEndTimeList(endTimeList);

					// キーが上記以外の場合
					} else {
						log.debug("key:" + obj.getKey());
						schedule.setRecurrence(1);
						// キーがsundayの場合
						if(obj.getKey().equals(CusconConst.SUN_STR)) {
							log.debug("key:" + obj.getKey());
							setWeeklySchedule(weeklySchedule, obj, CusconConst.SUN);
							tmpWeeklyList.add(weeklySchedule);
						// キーがmondayの場合
						} else if(obj.getKey().equals(CusconConst.MON_STR)) {
							log.debug("key:" + obj.getKey());
							setWeeklySchedule(weeklySchedule, obj, CusconConst.MON);
							tmpWeeklyList.add(weeklySchedule);
						// キーがtuesdayの場合
						} else if(obj.getKey().equals(CusconConst.TUE_STR)) {
							log.debug("key:" + obj.getKey());
							setWeeklySchedule(weeklySchedule, obj, CusconConst.TUE);
							tmpWeeklyList.add(weeklySchedule);
						// キーがwednesdayの場合
						} else if(obj.getKey().equals(CusconConst.WED_STR)) {
							log.debug("key:" + obj.getKey());
							setWeeklySchedule(weeklySchedule, obj, CusconConst.WED);
							tmpWeeklyList.add(weeklySchedule);
						// キーがthursdayの場合
						} else if(obj.getKey().equals(CusconConst.THU_STR)) {
							log.debug("key:" + obj.getKey());
							setWeeklySchedule(weeklySchedule, obj, CusconConst.THU);
							tmpWeeklyList.add(weeklySchedule);
						// キーがfridayの場合
						} else if(obj.getKey().equals(CusconConst.FRI_STR)) {
							log.debug("key:" + obj.getKey());
							setWeeklySchedule(weeklySchedule, obj, CusconConst.FRI);
							tmpWeeklyList.add(weeklySchedule);
						// キーがsaturdayの場合
						} else {
							log.debug("key:" + obj.getKey());
							setWeeklySchedule(weeklySchedule, obj, CusconConst.SAT);
							tmpWeeklyList.add(weeklySchedule);
						}
					}
				}

				try {
					// スケジュールオブジェクトをDBに登録する。
					updateDAO.execute("CommonT_Schedules-6", schedule);
					updateDAO.execute("CommonT_ObjectOldName-1", objectOldName);
				} catch(Exception e) {
					// DBアクセスエラー
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage(
											"FZ001010", objectgArray, uvo));
					throw e;
				}

				// デイリースケジュールが存在する場合
				if(dailySchedule.getStartTimeList() != null) {
					log.debug("デイリースケジュールデータ存在");

					// デイリースケジュールクリンクをDBに登録する。
					for(int j=0; j<dailySchedule.getStartTimeList().size(); j++) {

						log.debug("デイリースケジュールをDB登録" + j);
						// デイリーの基本登録情報を設定する。
						dailySchedule.setVsysId(uvo.getVsysId());
						dailySchedule.setGenerationNo(CusconConst.ZERO_GENE);
						dailySchedule.setStartTime(
								dailySchedule.getStartTimeList().get(j));
						dailySchedule.setEndTime(
								dailySchedule.getEndTimeList().get(j));
						try {
							updateDAO.execute(
								"CommonT_DailySchedulesLink-2", dailySchedule);
						} catch (Exception e) {
							// DBアクセスエラー
							Object[] objectgArray = {e.getMessage()};
							log.fatal(messageAccessor.getMessage(
											"FZ001010", objectgArray, uvo));
							throw e;
						}
					}
				}
				// ウィークリースケジュールクリンクをDBに登録する。
				for(int j=0; j<tmpWeeklyList.size(); j++) {
					log.debug("ウィークリースケジュールをDB登録" + j);
					for(int k=0; k<
						tmpWeeklyList.get(j).getStartTimeList().size(); k++) {

						log.debug("ウィークリースケジュールデータループ");

						WeeklySchedulesLink weeklySchedule2 = new WeeklySchedulesLink();
						// ウィークリーの基本登録情報を設定する。
						weeklySchedule2.setDayOfWeek(
								tmpWeeklyList.get(j).getDayOfWeek());
						weeklySchedule2.setStartTime(
								tmpWeeklyList.get(j).getStartTimeList().get(k));
						weeklySchedule2.setEndTime(
								tmpWeeklyList.get(j).getEndTimeList().get(k));
						weeklySchedule2.setName(schedule.getName());
						weeklySchedule2.setVsysId(uvo.getVsysId());
						weeklySchedule2.setGenerationNo(CusconConst.ZERO_GENE);

						try {
							updateDAO.execute(
									"CommonT_WeeklySchedulesLink-2", weeklySchedule2);
						} catch (Exception e) {
							// DBアクセスエラー
							Object[] objectgArray = {e.getMessage()};
							log.fatal(messageAccessor.getMessage("FZ001010", objectgArray, uvo));
							throw e;
						}
					}
				}

				// 臨時スケジュールが存在する場合
				if(nonRecSchedule.getStartTimeList() != null) {
					log.debug("臨時スケジュールデータ存在");

					// 臨時スケジュールクリンクをDBに登録する。
					for(int j=0; j<nonRecSchedule.getStartTimeList().size(); j++) {

						log.debug("臨時スケジュールをDB登録" + j);
						// 臨時の基本登録情報を設定する。
						nonRecSchedule.setVsysId(uvo.getVsysId());
						nonRecSchedule.setGenerationNo(CusconConst.ZERO_GENE);
						nonRecSchedule.setStartDate(
								nonRecSchedule.getStartDateList().get(j));
						nonRecSchedule.setStartTime(
								nonRecSchedule.getStartTimeList().get(j));
						nonRecSchedule.setEndDate(
								nonRecSchedule.getEndDateList().get(j));
						nonRecSchedule.setEndTime(
								nonRecSchedule.getEndTimeList().get(j));
						try {
							updateDAO.execute(
									"CommonT_NonRecurringSchedulesLink-2", nonRecSchedule);
						} catch(Exception e) {
							// DBアクセスエラー
							Object[] objectgArray = {e.getMessage()};
							log.fatal(messageAccessor.getMessage("FZ001010", objectgArray, uvo));
							throw e;
						}
					}
				}
			}
		}
		log.debug("setSchedules処理終了");

	}
	/**
	 * メソッド名 : setWeeklySchedule
	 * 機能概要 :
	 * @param weeklySchedule ウィークリーデータクラス
	 * @param obj ウィークリーデータクラスにデータを設定する。
	 * @param week 曜日
	 * @return WeeklySchedulesLink ウィークリーデータクラス
	 */
	private void setWeeklySchedule(
				WeeklySchedulesLink weeklySchedule, Map.Entry obj, int week) {

		log.debug("setWeeklySchedule処理開始:week = " + week);

		List<String> tmpList = (List<String>)obj.getValue();
		List<String> startTimeList = new ArrayList<String>();
		List<String> endTimeList = new ArrayList<String>();

		// スケジュール時刻リスト分繰り返す。
		for(int i=0; i<tmpList.size(); i++) {
			log.debug("時刻リスト" + i);
			// 開始時刻、終了時刻に分割する。
			String[] split_Str = tmpList.get(i).split(PHY);
			startTimeList.add(split_Str[0]);
			endTimeList.add(split_Str[1]);
		}
		// 曜日、開始時刻、終了時刻を格納する。
		weeklySchedule.setDayOfWeek(week);
		weeklySchedule.setStartTimeList(startTimeList);
		weeklySchedule.setEndTimeList(endTimeList);

		log.debug("setWeeklySchedule処理終了");
	}

	/**
	 * メソッド名 : setPolicyInfo
	 * 機能概要 : XML-APIによるポリシー取得、及びDB登録を行う。
	 * @param vsysId Vsys-ID
	 * @param xmlApiConnect XML-API実行URL
	 * @throws Exception
	 */
	private void setPolicyInfo(UpdateDAO updateDAO, QueryDAO queryDAO,
					CusconUVO uvo, String xmlApiConnect) throws Exception {

		log.debug("setPolicyInfo処理開始:vsysId = " + uvo.getVsysId() +
								", xmlApiConnect = " + xmlApiConnect);
		// ポリシー取得用
        String POLICY = "rulebase/security/rules";
        String DESCRIPTION = "description";
        String SERVICE = "service";
        String ACTION = "action";
        String ALLOW = "allow";
        String VULNERABILITY = "vulnerability";
        String SCHEDULE = "schedule";
        String FROM = "from";
        String TO = "to";
        String SOURCE = "source";
        String DESTINATION = "destination";
        String APPLICATION = "application";
//2011/06/10 add start inoue@prosite
        String URLFILTERING = "url-filtering";
        String VIRUS = "virus";
        String SPYWARE = "spyware";
//2011/06/10 add end inoue@prosite
     // 20130901 kkato@PROSITE add start
        String DISABLD = "disabled";
     // 20130901 kkato@PROSITE add end

		 // ポリシーデータクラス
        SecurityRules policy = new SecurityRules();
        LinkInfo srcZoneLink = new LinkInfo();
        LinkInfo dstZoneLink = new LinkInfo();
        LinkInfo srcAddressLink = new LinkInfo();
        LinkInfo dstAddressLink = new LinkInfo();
        LinkInfo appliLink = new LinkInfo();
        LinkInfo serviceLink = new LinkInfo();
//2011/06/10 add start inoue@prosite
        LinkInfo urlfilterLink = new LinkInfo();
//2011/06/10 add end inoue@prosite

        SecurityRules base = new SecurityRules();

        // XML解析レスポンス
        List<HashMap<String, Object>> objAnalyse;
        // XML-API取得
        String objConnect;
        // ポリシー情報を取得する。
		List<SecurityRules> security;

        // 一時格納用リスト
        List<String> tmpList;

        // 20121201 kkato@PROSITE mod start
		//objConnect = xmlApiConnect + SLASH + POLICY;
		// XML解析を行う
		//objAnalyse = getXmlResult(objConnect, uvo);
		objAnalyse = getXmlResult(xmlApiConnect, uvo, POLICY);
		// 20121201 kkato@PROSITE mod end

		// ポリシーの基本登録情報を設定する。
		policy.setVsysId(uvo.getVsysId());
		policy.setModFlg(CusconConst.DEL_NUM);
		policy.setGenerationNo(CusconConst.ZERO_GENE);

		base.setVsysId(uvo.getVsysId());
		base.setModFlg(CusconConst.DEL_NUM);
		base.setGenerationNo(CusconConst.ZERO_GENE);

		// 出所ゾーンリンクの基本登録情報を設定する。
		srcZoneLink.setVsysId(uvo.getVsysId());
		srcZoneLink.setGenerationNo(CusconConst.ZERO_GENE);

		// 宛先ゾーンリンクの基本登録情報を設定する。
		dstZoneLink.setVsysId(uvo.getVsysId());
		dstZoneLink.setGenerationNo(CusconConst.ZERO_GENE);

		// 出所アドレスリンクの基本登録情報を設定する。
		srcAddressLink.setVsysId(uvo.getVsysId());
		srcAddressLink.setGenerationNo(CusconConst.ZERO_GENE);

		// 宛先アドレスリンクの基本登録情報を設定する。
		dstAddressLink.setVsysId(uvo.getVsysId());
		dstAddressLink.setGenerationNo(CusconConst.ZERO_GENE);

		// サービスリンクの基本登録情報を設定する。
		serviceLink.setVsysId(uvo.getVsysId());
		serviceLink.setGenerationNo(CusconConst.ZERO_GENE);

		// アプリケーションリンクの基本登録情報を設定する。
		appliLink.setVsysId(uvo.getVsysId());
		appliLink.setGenerationNo(CusconConst.ZERO_GENE);

//2011/06/10 add start inoue@prosite
		// アプリケーションリンクの基本登録情報を設定する。
		urlfilterLink.setVsysId(uvo.getVsysId());
		urlfilterLink.setGenerationNo(CusconConst.ZERO_GENE);
//2011/06/10 add end inoue@prosite
		if(objAnalyse != null) {
			log.debug("PAにポリシーが存在");
			// XML解析レスポンスのデータ分繰り返す。
			for(int i=0; i<objAnalyse.size(); i++) {
				log.debug("XMLレスポンス解析:" + i);
				iterator = objAnalyse.get(i).entrySet().iterator();
				policy = new SecurityRules();
				policy.setVsysId(uvo.getVsysId());
				// ハッシュマップのデータ分繰り返す。
				while (iterator.hasNext()) {
					log.debug("ハッシュマップ");
					obj = (Map.Entry)iterator.next();
					// キーがnameの場合
					if(obj.getKey().equals(CusconConst.NAME)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						// ルール名を設定する。
						policy.setName(obj.getValue().toString());
						srcZoneLink.setName(obj.getValue().toString());
						dstZoneLink.setName(obj.getValue().toString());
						srcAddressLink.setName(obj.getValue().toString());
						dstAddressLink.setName(obj.getValue().toString());
						appliLink.setName(obj.getValue().toString());
						serviceLink.setName(obj.getValue().toString());
//2011/06/10 add start inoue@prosite
						urlfilterLink.setName(obj.getValue().toString());
//2011/06/10 add end inoue@prosite
						// 行数を設定する。
						policy.setLineNo(i+1);

					// キーがdescriptionの場合
					} else if(obj.getKey().equals(DESCRIPTION)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						// 備考を設定する。
						policy.setDescription(obj.getValue().toString());

					// キーがserviceの場合
					} else if(obj.getKey().equals(SERVICE)) {
						log.debug("key:" + obj.getKey());
						tmpList = (List<String>)obj.getValue();
						// サービスを設定する。
						serviceLink.setLinkNameList((List<String>)obj.getValue());
						// anyの場合
						if(tmpList.get(0).equals(CusconConst.ANY)) {
							log.debug("tmpList.get(0) = " + tmpList.get(0));
							policy.setServiceDefaultFlg(0);

						// application-defaultの場合
						} else if(tmpList.get(0).equals(
								CusconConst.APPLICATION_DEFAULT)) {
							log.debug("tmpList.get(0) = " + tmpList.get(0));
							policy.setServiceDefaultFlg(1);

						// selectの場合
						} else {
							log.debug("tmpList.get(0) = " + tmpList.get(0));
							policy.setServiceDefaultFlg(2);
						}

					// キーがactionの場合
					} else if(obj.getKey().equals(ACTION)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						// allowの場合
						if(obj.getValue().equals(ALLOW)) {
							log.debug("obj.getValue() = " + obj.getValue());
							policy.setAction(0);

						// denyの場合
						} else {
							log.debug("obj.getValue() = " + obj.getValue());
							policy.setAction(1);
						}

					// キーがvulnerabilityの場合
					} else if(obj.getKey().equals(VULNERABILITY)) {
						log.debug("key:" + obj.getKey());
						// defaultの場合しか存在しない。
						policy.setIds_ips(1);

					// キーがscheduleの場合
					} else if(obj.getKey().equals(SCHEDULE)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						// スケジュール名を設定する。
						policy.setScheduleName(obj.getValue().toString());
					// キーがfromの場合
					} else if(obj.getKey().equals(FROM)) {
						log.debug("key:" + obj.getKey());
						// 出所ゾーン名を設定する。
						srcZoneLink.setLinkNameList((List<String>)obj.getValue());
					// キーがtoの場合
					} else if(obj.getKey().equals(TO)) {
						log.debug("key:" + obj.getKey());
						// 宛先ゾーン名を設定する。
						dstZoneLink.setLinkNameList((List<String>)obj.getValue());
					// キーがsourceの場合
					} else if(obj.getKey().equals(SOURCE)) {
						log.debug("key:" + obj.getKey());
						// 出所アドレス名を設定する。
						srcAddressLink.setLinkNameList(
								(List<String>)obj.getValue());
					// キーがdestinationの場合
					} else if(obj.getKey().equals(DESTINATION)) {
						log.debug("key:" + obj.getKey());
						// 宛先アドレス名を設定する。
						dstAddressLink.setLinkNameList(
								(List<String>)obj.getValue());
					// キーがapplicationの場合
					} else if(obj.getKey().equals(APPLICATION)) {
						log.debug("key:" + obj.getKey());
						// アプリケーションを設定する。
						appliLink.setLinkNameList((List<String>)obj.getValue());
//2011/06/10 add start inoue@prosite
					// 各々キーが存在すれば各々のフラグに1をセット
					// キーがurl-filteringの場合
					} else if(obj.getKey().equals(URLFILTERING)) {
						log.debug("key:" + obj.getKey());
						policy.setUrlfilterDefaultFlg(1);
						urlfilterLink.setLinkNameList((List<String>)obj.getValue());
						// キーがurl-filteringの場合
					} else if(obj.getKey().equals(VIRUS)) {
						log.debug("key:" + obj.getKey());
						policy.setVirusCheckFlg(1);
						// キーがurl-filteringの場合
					} else if(obj.getKey().equals(SPYWARE)) {
						log.debug("key:" + obj.getKey());
						policy.setSpywareFlg(1);
//2011/06/10 add end inoue@prosite
					// 20130901 kkato@PROSITE add start
					} else if(obj.getKey().equals(DISABLD)) {
						log.debug("key:" + obj.getKey());
						policy.setDisableFlg(1);
					// 20130901 kkato@PROSITE add end
					// キーがserviceの場合
					} else {
						log.debug("key:" + obj.getKey());
					}
				}
				try {
					// ポリシー情報をDBに登録する。
					updateDAO.execute("CommonT_SecurityRules-4", policy);
					// ポリシー情報のスケジュールリンク番号を更新する。
					updateDAO.execute("CommonT_SecurityRules-5", policy);
					// ポリシー情報を取得する。
					base.setName(policy.getName());
					security =
						queryDAO.executeForObjectList("MovePolicy-2", base);
				} catch(Exception e) {
					// DBアクセスエラー
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage(
												"FZ001011", objectgArray, uvo));
					throw e;
				}

				/**************************************************
				 *  出所ゾーンリンクDB登録
				 **************************************************/
				// 出所ゾーン名リストの1つ目がany以外場合のみ登録処理を行う。
				if(!srcZoneLink.getLinkNameList().get(0).equals(CusconConst.ANY)) {
					log.debug("出所ゾーンリンクDB登録");
					try {
						updateDAO.execute("CommonT_SourceZoneLink-2", srcZoneLink);
					} catch(Exception e) {
						// DBアクセスエラー
						Object[] objectgArray = {e.getMessage()};
						log.fatal(messageAccessor.getMessage(
											"FZ001011", objectgArray, uvo));
						throw e;
					}
				}

				/**************************************************
				 *  宛先ゾーンリンクDB登録
				 **************************************************/
				// 宛先ゾーン名リストの1つ目がany以外場合のみ登録処理を行う。
				if(!dstZoneLink.getLinkNameList().get(0).equals(CusconConst.ANY)) {
					log.debug("宛先ゾーンリンクDB登録");
					try {
						updateDAO.execute(
								"CommonT_DestinationZoneLink-2", dstZoneLink);
					} catch(Exception e) {
						// DBアクセスエラー
						Object[] objectgArray = {e.getMessage()};
						log.fatal(messageAccessor.getMessage(
											"FZ001011", objectgArray, uvo));
						throw e;
					}
				}

				/**************************************************
				 *  出所アドレスリンク、ローカル出所アドレスDB登録
				 **************************************************/
				// 処理結果
				int rec = 0;
				// 出所アドレス名リストの1つ目がany以外場合のみ登録処理を行う。
				if(!srcAddressLink.getLinkNameList().get(0).
												equals(CusconConst.ANY)) {
					log.debug("出所アドレスリンクDB登録");
					// 取得したアドレス名リスト分繰り返す。
					for(int j = 0;
							j<srcAddressLink.getLinkNameList().size(); j++) {

						log.debug("登録中:" + j);
						srcAddressLink.setLinkName(
								srcAddressLink.getLinkNameList().get(j));
						try {
							rec = updateDAO.execute(
								"CommonT_SourceAddressLink-2", srcAddressLink);
						} catch(Exception e) {
							// DBアクセスエラー
							Object[] objectgArray = {e.getMessage()};
							log.fatal(messageAccessor.getMessage(
											"FZ001011", objectgArray, uvo));
							throw e;
						}

						// 処理結果が0件の場合
						if(rec == 0) {
							log.debug("ローカル出所アドレスDB登録");
							try {
								// ローカル出所アドレスのシーケンス番号を払い出す。
								Integer linkSeqNo = queryDAO.executeForObject(
									"LocalSourceAddressSeq-1", null,
													java.lang.Integer.class);

								srcAddressLink.setLinkSeqNo(linkSeqNo);

								// ローカル出所アドレスにDB登録を行う。
								updateDAO.execute(
									"CommonT_LocalSourceAddress-2", srcAddressLink);

								// 出所アドレスリンクに登録したローカル出所アドレス
								// とのリンクを登録する。
								srcAddressLink.setSeqNo(security.get(0).getSeqNo());
								updateDAO.execute(
									"CommonT_SourceAddressLink-3", srcAddressLink);
							} catch(Exception e) {
								// DBアクセスエラー
								Object[] objectgArray = {e.getMessage()};
								log.fatal(messageAccessor.getMessage(
												"FZ001011", objectgArray, uvo));
								throw e;
							}
						}
					}
				}
				/**************************************************
				 *  宛先アドレスリンク、ローカル宛先アドレスDB登録
				 **************************************************/
				// リンクシーケンス番号取得用
				Integer linkSeqNo = 0;

				// 宛先アドレス名リストの1つ目がany以外場合のみ登録処理を行う。
				if(!dstAddressLink.getLinkNameList().get(0).
												equals(CusconConst.ANY)) {
					log.debug("宛先アドレスリンクDB登録");
					// 取得したアドレス名リスト分繰り返す。
					for(int j = 0;
							j<dstAddressLink.getLinkNameList().size(); j++) {

						log.debug("登録中:" + j);
						dstAddressLink.setLinkName(
							dstAddressLink.getLinkNameList().get(j));
						try {
							rec = updateDAO.execute(
								"CommonT_DestinationAddressLink-2", dstAddressLink);
						} catch(Exception e) {
							// DBアクセスエラー
							Object[] objectgArray = {e.getMessage()};
							log.fatal(messageAccessor.getMessage(
												"FZ001011", objectgArray, uvo));
							throw e;
						}

						// 処理結果が0件の場合
						if(rec == 0) {
							log.debug("ローカル宛先アドレスDB登録");
							// ローカル宛先アドレスのシーケンス番号を払い出す。
							try {
								linkSeqNo = queryDAO.executeForObject(
									"LocalDestinationAddress-1", null,
													java.lang.Integer.class);
							} catch(Exception e) {
								// DBアクセスエラー
								Object[] objectgArray = {e.getMessage()};
								log.fatal(messageAccessor.getMessage(
												"FZ001011", objectgArray, uvo));
								throw e;
							}

							dstAddressLink.setLinkSeqNo(linkSeqNo);
							try {
								// ローカル宛先アドレスにDB登録を行う。
								updateDAO.execute(
										"CommonT_LocalDestinationAddress-2",
											dstAddressLink);

								// 宛先アドレスリンクに登録したローカル宛先アドレス
								// とのリンクを登録する。
								dstAddressLink.setSeqNo(security.get(0).getSeqNo());
								updateDAO.execute(
										"CommonT_DestinationAddressLink-3",
											dstAddressLink);
							} catch(Exception e) {
								// DBアクセスエラー
								Object[] objectgArray = {e.getMessage()};
								log.fatal(messageAccessor.getMessage(
												"FZ001011", objectgArray, uvo));
								throw e;
							}
						}
					}
				}

				/**************************************************
				 *  サービスリンクDB登録
				 **************************************************/
				// サービス適用フラグが2(select)の場合のみ登録処理を行う。
				if(policy.getServiceDefaultFlg() == 2) {
					log.debug("サービスリンクDB登録");

					try {
						updateDAO.execute("CommonT_ServiceLink-2", serviceLink);
					} catch(Exception e) {
						// DBアクセスエラー
						Object[] objectgArray = {e.getMessage()};
						log.fatal(messageAccessor.getMessage(
												"FZ001011", objectgArray, uvo));
						throw e;
					}
				}

				/**************************************************
				 *  アプリケーションリンクDB登録
				 **************************************************/
				// アプリケーション名リストの1つ目がany以外場合のみ登録処理を行う。
				if(!appliLink.getLinkNameList().get(0).equals(CusconConst.ANY)) {
					log.debug("アプリケーションリンクDB登録");
					try {
						updateDAO.execute("CommonT_ApplicationLink-2", appliLink);
					} catch(Exception e) {
						// DBアクセスエラー
						Object[] objectgArray = {e.getMessage()};
						log.fatal(messageAccessor.getMessage(
												"FZ001011", objectgArray, uvo));
						throw e;
					}
				}

//2011/06/10 add start inoue@prosite
				/**************************************************
				 *  WebフィルタリンクDB登録MakeT_UrlFilterLink
				 **************************************************/
				// Webフィルタ適用フラグが1(select)の場合のみ登録処理を行う。
				if(policy.getUrlfilterDefaultFlg() == 1) {
					log.debug("WebフィルタリンクDB登録");
					try {
						updateDAO.execute("MakeT_UrlFilterLink", urlfilterLink);
					} catch(Exception e) {
						// DBアクセスエラー
						Object[] objectgArray = {e.getMessage()};
						log.fatal(messageAccessor.getMessage(
												"FZ001011", objectgArray, uvo));
						throw e;
					}
				}
//2011/06/10 add end inoue@prosite
			}
		}
		log.debug("setPolicyInfo処理終了");
	}

	/**
	 * メソッド名 : getXmlResult
	 * 機能概要 : XML-APIを実行し、レスポンスを解析し結果を返却する。
	 * @param xmlApiConnect XML-APIのURL
	 * @param objName オブジェクト名称
	 * @return List<CODE><</CODE>HashMap<CODE><</CODE>String, Object<CODE>></CODE><CODE>></CODE> XML解析結果
	 * @throws Exception
	 */
// 20121201 kkato@PROSITE mod start
//	private List<HashMap<String, Object>> getXmlResult(String objConnect, CusconUVO uvo) throws Exception {
	private List<HashMap<String, Object>> getXmlResult(String xmlApiConnect, CusconUVO uvo, String objName
			) throws Exception {
// 20121201 kkato@PROSITE mod end

		log.debug("getXmlResult処理開始");
		 // XML-APIレスポンス
        InputStream objRes;
        // XML解析レスポンス
        List<HashMap<String, Object>> objAnalyse;

// 20121201 kkato@PROSITE add start
	     // アドレスオブジェクト取得用
	     String ADDRESS = "address";
	     // アドレスグループオブジェクト取得用
	     String ADDRESS_GRP = "address-group";
	     // アプリケーションフィルタオブジェクト取得用
	     String APPLI_FIL = "application-filter";
	     // アプリケーショングループオブジェクト取得用
	     String APPLI_GRP = "application-group";
	     // サービスオブジェクト取得用
	     String SERVICE = "service";
	     // サービスグループオブジェクト取得用
	     String SERVICE_GRP = "service-group";
	     // スケジュールオブジェクト取得用
	     String SCHEDULE = "schedule";
	     // ポリシー取得用
	     String POLICY = "rulebase/security/rules";
	     // Webフィルタオブジェクト取得用
	     String URLFILTER = "profiles/url-filtering";
//20121201 kkato@PROSITE afd end

		// XML-APIにてFW要求を行う。
		// 20121201 kkato@PROSITE mod start
        String objConnect = xmlApiConnect + SLASH + objName;
		objRes = xmlExe.doProc(objConnect, uvo, messageAccessor);
		// XML解析を行う
		//objAnalyse = xmlAna.analyzeXML(objRes, uvo, messageAccessor);
		if (objName == ADDRESS) {
			objAnalyse = xmlAna.analyzeXML_objects(objRes, uvo, objName, messageAccessor);
		} else if (objName == ADDRESS_GRP) {
				objAnalyse = xmlAna.analyzeXML_objects(objRes, uvo, objName, messageAccessor);
		} else if (objName == APPLI_FIL) {
			objAnalyse = xmlAna.analyzeXML_objects(objRes, uvo, objName, messageAccessor);
		} else if (objName == APPLI_GRP) {
			objAnalyse = xmlAna.analyzeXML_objects(objRes, uvo, objName, messageAccessor);
		} else if (objName == SERVICE) {
			objAnalyse = xmlAna.analyzeXML_objects(objRes, uvo, objName, messageAccessor);
		} else if (objName == SERVICE_GRP) {
			objAnalyse = xmlAna.analyzeXML_objects(objRes, uvo, objName, messageAccessor);
		} else if (objName == POLICY) {
			objAnalyse = xmlAna.analyzeXML_objects(objRes, uvo, objName, messageAccessor);
		} else if (objName == URLFILTER) {
			objAnalyse = xmlAna.analyzeXML_objects(objRes, uvo, objName, messageAccessor);
		} else {
			objAnalyse = xmlAna.analyzeXML(objRes, uvo, messageAccessor);
		}
		// 20121201 kkato@PROSITE mod end

		if(objAnalyse == null) {
			log.debug("XML-APIコマンドが不正:null");
			return null;
		}
		log.debug("getXmlResult処理終了:objAnalyse.size() = " + objAnalyse.size());
		return objAnalyse;
	}
//2011/06/10 add start inoue@prosite
	/**
	 * メソッド名 : setUrlFilters
	 * 機能概要 : XML-APIによるWebフィルタオブジェクト取得、及びDB登録を行う。
	 * @param updateDAO
	 * @param queryDAO
	 * @param uvo
	 * @param xmlApiConnect XML-API実行URL
	 * @throws Exception
	 */
	private void setUrlFilters(UpdateDAO updateDAO, QueryDAO queryDAO, CusconUVO uvo, String xmlApiConnect) throws Exception {

		log.debug("setUrlFilters処理開始:vsysId = " + uvo.getVsysId() +
								", xmlApiConnect = " + xmlApiConnect);
		// Webフィルタオブジェクト取得用
        String URLFILTER = "profiles/url-filtering";

        // 条件クラス
        InputBase base = new InputBase();
        // Webフィルタオブジェクトデータクラス
        UrlFilters urlfilters = new UrlFilters();
        // Webフィルタカテゴリデータクラス
        UrlFilterCategories urlfiltercategory = new UrlFilterCategories();
        // オブジェクト旧名データクラス
        ObjectOldName objectOldName = new ObjectOldName();

        // XML解析レスポンス
        List<HashMap<String, Object>> objAnalyse;
        // XML-API取得
        String objConnect;

		// 20121201 kkato@PROSITE mod start
		//objConnect = xmlApiConnect + SLASH + URLFILTER;
		// XML解析を行う
		//objAnalyse = getXmlResult(objConnect, uvo);
		objAnalyse = getXmlResult(xmlApiConnect, uvo, URLFILTER);
		// 20121201 kkato@PROSITE mod end

		// Baseの基本登録情報を設定する。
		base.setVsysId(uvo.getVsysId());
		base.setGenerationNo(CusconConst.ZERO_GENE);

		// Webフィルタの基本登録情報を設定する。
		urlfilters.setVsysId(uvo.getVsysId());
		urlfilters.setModFlg(CusconConst.NONE_NUM);
		urlfilters.setGenerationNo(CusconConst.ZERO_GENE);
		// Webフィルタカテゴリの基本登録情報を設定する。
		urlfiltercategory.setVsysId(uvo.getVsysId());
		urlfiltercategory.setGenerationNo(CusconConst.ZERO_GENE);

		objectOldName.setVsysId(uvo.getVsysId());
		objectOldName.setObjectType(CusconConst.UrlFilter);

		if(objAnalyse != null) {
			log.debug("PAにWebフィルタが存在");
			// XML解析レスポンスのデータ分繰り返す。
			for(int i=0; i<objAnalyse.size(); i++) {

				//WebフィルタオブジェクトとWebフィルタカテゴリのリンク付けの設定
				urlfilters.setOrg_seqno(i+1);
				urlfiltercategory.setUrlfiltersSeqno(i+1);

				//WebフィルタオブジェクトとWebフィルタカテゴリのリンク付けの設定
				urlfilters.setBlockListSite(null);
				urlfilters.setBlockListSiteCnt(0);
				urlfilters.setAllowListSite(null);
				urlfilters.setAllowListSiteCnt(0);

//2011/10/06 add start inoue@prosite(説明に他のWebフィルターの文字が入る件)
				urlfilters.setComment(null);
//2011/10/06 add end inoue@prosite(説明に他のWebフィルターの文字が入る件)

				log.debug("XMLレスポンス解析:" + i);
				iterator = objAnalyse.get(i).entrySet().iterator();
				// ハッシュマップのデータ分繰り返す。
				while (iterator.hasNext()) {
					log.debug("ハッシュマップ");
					obj = (Map.Entry)iterator.next();
					// オブジェクト名(name)
					if(obj.getKey().equals(CusconConst.NAME)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						urlfilters.setName(obj.getValue().toString());
						objectOldName.setName(obj.getValue().toString());
					}
					// 説明(description)
					if(obj.getKey().equals(CusconConst.DESCRIPTION)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						urlfilters.setComment(obj.getValue().toString());
					}
					// ブロックリストアクション(block-list-site)
					if(obj.getKey().equals(CusconConst.ACTION)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						urlfilters.setActionBlockListSite(obj.getValue().toString());
					}
					// ブロックリスト(block-list-site) 戻り値形式"[yahoo.cp.jp, google.com]"
					if(obj.getKey().equals(CusconConst.BLOCKLIST)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						String str = obj.getValue().toString();
						int num = getStrNum(str, COMMA) + 1;
						urlfilters.setBlockListSiteCnt(num);
						// 先頭、最後の1文字を除く( "[", "]"を除く )
						str = str.substring(1, str.length()-1);
						str = str.replaceAll(", ","\n");
						urlfilters.setBlockListSite(str);
					}
					// 許可リスト(allow-list-site) 戻り値形式"[yahoo.cp.jp, google.com]"
					if(obj.getKey().equals(CusconConst.ALLOWLIST)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						String str = obj.getValue().toString();
						int num = getStrNum(str, COMMA) + 1;
						urlfilters.setAllowListSiteCnt(num);
						// 先頭、最後の1文字を除く( "[", "]"を除く )
						str = str.substring(1, str.length()-1);
						str = str.replaceAll(", ","\n");
						urlfilters.setAllowListSite(str);
					}
					// カテゴリ(allow)
					if(obj.getKey().equals(CusconConst.ALLOW)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						setUrlFilterCategories(updateDAO, queryDAO, uvo, urlfiltercategory, CusconConst.ALLOW, obj.getValue().toString());
					}
					// カテゴリ(alert)
					if(obj.getKey().equals(CusconConst.ALERT)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						setUrlFilterCategories(updateDAO, queryDAO, uvo, urlfiltercategory, CusconConst.ALERT, obj.getValue().toString());
					}
					// カテゴリ(block)
					if(obj.getKey().equals(CusconConst.BLOCK)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						setUrlFilterCategories(updateDAO, queryDAO, uvo, urlfiltercategory, CusconConst.BLOCK, obj.getValue().toString());
					}
					// カテゴリ(continue)
					if(obj.getKey().equals(CusconConst.CONTINUE)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						setUrlFilterCategories(updateDAO, queryDAO, uvo, urlfiltercategory, CusconConst.CONTINUE, obj.getValue().toString());
					}
					// カテゴリ(override)
					if(obj.getKey().equals(CusconConst.OVERRIDE)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						setUrlFilterCategories(updateDAO, queryDAO, uvo, urlfiltercategory, CusconConst.OVERRIDE, obj.getValue().toString());
					// キーがname以外の場合
					}
				}

				try {
					// データをDBに登録する。
					log.debug("Webフィルタオブジェクトを作成");
					updateDAO.execute("MakeT_UrlFilters", urlfilters);
					log.debug("WebフィルタカテゴリのURLFILTER_SEQNOの更新");
					updateDAO.execute("UpdateT_UrlFilter_Categories_UrlFiltersSeqNo", base);
					log.debug("WebフィルタオブジェクトのORG_SEQNOに0をセット");
					updateDAO.execute("UpdateT_UrlFilters_OrgSeqNo", base);
					updateDAO.execute("CommonT_ObjectOldName-1", objectOldName);
				} catch (Exception e) {
					// DBアクセスエラー
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage("FZ001012", objectgArray, uvo));
					throw e;
				}
			}
		}

		log.debug("setUrlFilters処理終了");
	}

	/**
	 * メソッド名 : setUrlFilterCategories
	 * 機能概要 : XML-APIによるWebフィルタカテゴリDB登録を行う。
	 * @param updateDAO
	 * @param queryDAO
	 * @param uvo
	 * @param urlfiltercategoryクラス
	 * @param action(action)
	 * @param catetories(カテゴリ名)
	 * @throws Exception
	 */
	private void setUrlFilterCategories(UpdateDAO updateDAO, QueryDAO queryDAO, CusconUVO uvo,
			UrlFilterCategories urlfiltercategory, String action, String categories) throws Exception {

		log.debug("setUrlFilterCategories処理開始:vsysId = " + uvo.getVsysId() +
								", action = " + action +
								", categories = " + categories);
		try {
			// 先頭、最後の1文字を除く( "[", "]"を除く )
			String str = categories.substring(1, categories.length()-1);
			String[] ctgr = str.split(", ", -1);

			urlfiltercategory.setLinkNameList(ctgr);
			if(str != null) {
				log.debug("Webフィルタカテゴリを作成:");
				urlfiltercategory.setAction(action);
				updateDAO.execute("MakeT_UrlFilter_Categories", urlfiltercategory);
			}
		} catch (Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FZ001013", objectgArray, uvo));
			throw e;
		}
		log.debug("setUrlFilterCategories処理終了");
	}

	/**
	 * メソッド名 : getStrNum
	 * 機能概要 : 対象文字列内に検索文字列が何個含まれているか
	 * @param orgstr	: 検索対象文字列
	 * @param searchstr	: 検索文字列
	 * @throws Exception
	 */
	private int getStrNum(String orgstr, String searchstr) throws Exception {
		int cnt = 0;
		int targetSize = 0;
		int replacedSize = 0;

		try{
			targetSize = orgstr.length();
			replacedSize = orgstr.replace(searchstr,"").length();
			cnt = (targetSize-replacedSize)/searchstr.length();
		}catch(Exception e){
			throw e;
		}
		return cnt;
	}
//2011/06/10 add end inoue@prosite
}
