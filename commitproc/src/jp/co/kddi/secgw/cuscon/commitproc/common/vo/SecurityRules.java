/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SecurityRules.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/17     komakiys         初版作成
 * 2010/06/17     hiroyasu         コミット処理プロセス化対応
 * 2011/06/10     inoue@prosite    Webフィルタの追加対応
 * 2013/09/01     kkato@PROSITE    セキュリティルール有効/無効切替対応
*******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.commitproc.common.vo;

import java.io.Serializable;
import java.util.List;

/**
 * クラス名 : SecurityRules
 * 機能概要 : セキュリティポリシー一覧データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/17
 *          新規作成
 * @see
 */
public class SecurityRules implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = -2280096718131621846L;

	// シーケンス番号
	private int seqNo = 0;
	// Vsys-ID
	private String vsysId = null;
	// ルール名
	private String name = null;
	// 世代番号
	private int generationNo = 0;
	// 行番号
	private int lineNo = 0;
	// 備考
	private String description = null;
	// サービス適用フラグ
	private int serviceDefaultFlg = 0;
	// ルール適用フラグ
	private int action = 0;
	// 脆弱性の保護フラグ
	private int ids_ips = 0;
	// スケジュールオブジェクトリンク
	private String schedulesSeqNo = null;
	// 変更フラグ
	private int modFlg = 0;
	// コミット日時
	private String updateTime = null;
	// スケジュール名
	private String scheduleName = null;
	// 世代番号(新)
	private int newGenerationNo = 0;
	// コミット時間+世代番号
	private String GeneUpdateStr = null;
	// 隠しルール名リスト
	private List<String> nonSubject = null;
	// 更新行番号
	private int updateLineNo = 0;
	// 編集後ルール名
	private String modName = null;
	private int linkNo = 0;
	private String localAddress = null;

//2011/06/10 add start inoue@prosite
	private int urlfilterDefaultFlg = 0;
	private int virusCheckFlg = 0;
	private int spywareFlg = 0;
//2011/06/10 add end inoue@prosite
	// 20130901 kkato@PROSITE add start
	private int disableFlg = 0;
	// 20130901 kkato@PROSITE add end



	/**
	 * メソッド名 : localAddressのGetterメソッド
	 * 機能概要 : localAddressを取得する。
	 * @return localAddress
	 */
	public String getLocalAddress() {
		return localAddress;
	}

	/**
	 * メソッド名 : localAddressのSetterメソッド
	 * 機能概要 : localAddressをセットする。
	 * @param localAddress
	 */
	public void setLocalAddress(String localAddress) {
		this.localAddress = localAddress;
	}

	/**
	 * メソッド名 : linkNoのGetterメソッド
	 * 機能概要 : linkNoを取得する。
	 * @return linkNo
	 */
	public int getLinkNo() {
		return linkNo;
	}

	/**
	 * メソッド名 : linkNoのSetterメソッド
	 * 機能概要 : linkNoをセットする。
	 * @param linkNo
	 */
	public void setLinkNo(int linkNo) {
		this.linkNo = linkNo;
	}

	/**
	 * メソッド名 : seqNoのGetterメソッド
	 * 機能概要 : seqNoを取得する。
	 * @return seqNo
	 */
	public int getSeqNo() {
		return seqNo;
	}

	/**
	 * メソッド名 : seqNoのSetterメソッド
	 * 機能概要 : seqNoをセットする。
	 * @param seqNo
	 */
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}

	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得する。
	 * @return vsysId
	 */
	public String getVsysId() {
		return vsysId;
	}

	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdをセットする。
	 * @param vsysId
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * メソッド名 : generationNoのGetterメソッド
	 * 機能概要 : generationNoを取得する。
	 * @return generationNo
	 */
	public int getGenerationNo() {
		return generationNo;
	}

	/**
	 * メソッド名 : generationNoのSetterメソッド
	 * 機能概要 : generationNoをセットする。
	 * @param generationNo
	 */
	public void setGenerationNo(int generationNo) {
		this.generationNo = generationNo;
	}

	/**
	 * メソッド名 : linkNoのGetterメソッド
	 * 機能概要 : linkNoを取得する。
	 * @return linkNo
	 */
	public int getLineNo() {
		return lineNo;
	}

	/**
	 * メソッド名 : linkNoのSetterメソッド
	 * 機能概要 : linkNoをセットする。
	 * @param linkNo
	 */
	public void setLineNo(int linkNo) {
		this.lineNo = linkNo;
	}

	/**
	 * メソッド名 : descriptionのGetterメソッド
	 * 機能概要 : descriptionを取得する。
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * メソッド名 : descriptionのSetterメソッド
	 * 機能概要 : descriptionをセットする。
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * メソッド名 : serviceDefaultFlgのGetterメソッド
	 * 機能概要 : serviceDefaultFlgを取得する。
	 * @return serviceDefaultFlg
	 */
	public int getServiceDefaultFlg() {
		return serviceDefaultFlg;
	}

	/**
	 * メソッド名 : serviceDefaultFlgのSetterメソッド
	 * 機能概要 : serviceDefaultFlgをセットする。
	 * @param serviceDefaultFlg
	 */
	public void setServiceDefaultFlg(int serviceDefaultFlg) {
		this.serviceDefaultFlg = serviceDefaultFlg;
	}

	/**
	 * メソッド名 : actionのGetterメソッド
	 * 機能概要 : actionを取得する。
	 * @return action
	 */
	public int getAction() {
		return action;
	}

	/**
	 * メソッド名 : actionのSetterメソッド
	 * 機能概要 : actionをセットする。
	 * @param action
	 */
	public void setAction(int action) {
		this.action = action;
	}

	/**
	 * メソッド名 : ids_ipsのGetterメソッド
	 * 機能概要 : ids_ipsを取得する。
	 * @return ids_ips
	 */
	public int getIds_ips() {
		return ids_ips;
	}

	/**
	 * メソッド名 : ids_ipsのSetterメソッド
	 * 機能概要 : ids_ipsをセットする。
	 * @param idsIps
	 */
	public void setIds_ips(int idsIps) {
		this.ids_ips = idsIps;
	}

	/**
	 * メソッド名 : schedulesSeqNoのGetterメソッド
	 * 機能概要 : schedulesSeqNoを取得する。
	 * @return schedulesSeqNo
	 */
	public String getSchedulesSeqNo() {
		return schedulesSeqNo;
	}

	/**
	 * メソッド名 : schedulesSeqNoのSetterメソッド
	 * 機能概要 : schedulesSeqNoをセットする。
	 * @param schedulesSeqNo
	 */
	public void setSchedulesSeqNo(String schedulesSeqNo) {
		this.schedulesSeqNo = schedulesSeqNo;
	}

	/**
	 * メソッド名 : modFlgのGetterメソッド
	 * 機能概要 : modFlgを取得する。
	 * @return modFlg
	 */
	public int getModFlg() {
		return modFlg;
	}

	/**
	 * メソッド名 : modFlgのSetterメソッド
	 * 機能概要 : modFlgをセットする。
	 * @param modFlg
	 */
	public void setModFlg(int modFlg) {
		this.modFlg = modFlg;
	}

	/**
	 * メソッド名 : updateTimeのGetterメソッド
	 * 機能概要 : updateTimeを取得する。
	 * @return updateTime
	 */
	public String getUpdateTime() {
		return updateTime;
	}

	/**
	 * メソッド名 : updateTimeのSetterメソッド
	 * 機能概要 : updateTimeをセットする。
	 * @param updateTime
	 */
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * メソッド名 : scheduleNameのGetterメソッド
	 * 機能概要 : scheduleNameを取得する。
	 * @return scheduleName
	 */
	public String getScheduleName() {
		return scheduleName;
	}

	/**
	 * メソッド名 : scheduleNameのSetterメソッド
	 * 機能概要 : scheduleNameをセットする。
	 * @param scheduleName
	 */
	public void setScheduleName(String scheduleName) {
		this.scheduleName = scheduleName;
	}

	/**
	 * メソッド名 : newGenerationNoのGetterメソッド
	 * 機能概要 : newGenerationNoを取得する。
	 * @return newGenerationNo
	 */
	public int getNewGenerationNo() {
		return newGenerationNo;
	}

	/**
	 * メソッド名 : newGenerationNoのSetterメソッド
	 * 機能概要 : newGenerationNoをセットする。
	 * @param newGenerationNo
	 */
	public void setNewGenerationNo(int newGenerationNo) {
		this.newGenerationNo = newGenerationNo;
	}

	/**
	 * メソッド名 : geneUpdateStrのGetterメソッド
	 * 機能概要 : geneUpdateStrを取得する。
	 * @return geneUpdateStr
	 */
	public String getGeneUpdateStr() {
		return GeneUpdateStr;
	}

	/**
	 * メソッド名 : geneUpdateStrのSetterメソッド
	 * 機能概要 : geneUpdateStrをセットする。
	 * @param geneUpdateStr
	 */
	public void setGeneUpdateStr(String geneUpdateStr) {
		GeneUpdateStr = geneUpdateStr;
	}

	/**
	 * メソッド名 : nonSubjectのGetterメソッド
	 * 機能概要 : nonSubjectを取得する。
	 * @return nonSubject
	 */
	public List<String> getNonSubject() {
		return nonSubject;
	}

	/**
	 * メソッド名 : nonSubjectのSetterメソッド
	 * 機能概要 : nonSubjectをセットする。
	 * @param nonSubject
	 */
	public void setNonSubject(List<String> nonSubject) {
		this.nonSubject = nonSubject;
	}

	/**
	 * メソッド名 : updateLinkNoのGetterメソッド
	 * 機能概要 : updateLinkNoを取得する。
	 * @return updateLinkNo
	 */
	public int getUpdateLineNo() {
		return updateLineNo;
	}

	/**
	 * メソッド名 : updateLinkNoのSetterメソッド
	 * 機能概要 : updateLinkNoをセットする。
	 * @param updateLinkNo
	 */
	public void setUpdateLineNo(int updateLineNo) {
		this.updateLineNo = updateLineNo;
	}

	/**
	 * メソッド名 : modNameのGetterメソッド
	 * 機能概要 : modNameを取得する。
	 * @return modName
	 */
	public String getModName() {
		return modName;
	}

	/**
	 * メソッド名 : modNameのSetterメソッド
	 * 機能概要 : modNameをセットする。
	 * @param modName
	 */
	public void setModName(String modName) {
		this.modName = modName;
	}
//2011/06/10 add start inoue@prosite
	/**
	 * メソッド名 : urlfilterDefaultFlgのGetterメソッド
	 * 機能概要 : urlfilterDefaultFlgを取得する。
	 * @return urlfilterDefaultFlg
	 */
	public int getUrlfilterDefaultFlg() {
		return urlfilterDefaultFlg;
	}

	/**
	 * メソッド名 : urlfilterDefaultFlgのSetterメソッド
	 * 機能概要 : urlfilterDefaultFlgをセットする。
	 * @param urlfilterDefaultFlg
	 */
	public void setUrlfilterDefaultFlg(int urlfilterDefaultFlg) {
		this.urlfilterDefaultFlg = urlfilterDefaultFlg;
	}
	/**
	 * メソッド名 : virusCheckFlgのGetterメソッド
	 * 機能概要 : virusCheckFlgを取得する。
	 * @return virusCheckFlg
	 */
	public int getVirusCheckFlg() {
		return virusCheckFlg;
	}
	/**
	 * メソッド名 : virusCheckFlgのSetterメソッド
	 * 機能概要 : virusCheckFlgをセットする。
	 * @param virusCheckFlg
	 */
	public void setVirusCheckFlg(int virusCheckFlg) {
		this.virusCheckFlg = virusCheckFlg;
	}
	/**
	 * メソッド名 : spywareFlgのGetterメソッド
	 * 機能概要 : spywareFlgを取得する。
	 * @return spywareFlg
	 */
	public int getSpywareFlg() {
		return spywareFlg;
	}

	/**
	 * メソッド名 : spywareFlgのSetterメソッド
	 * 機能概要 : spywareFlgをセットする。
	 * @param spywareFlg
	 */
	public void setSpywareFlg(int spywareFlg) {
		this.spywareFlg = spywareFlg;
	}
//2011/06/10 add end inoue@prosite

	// 20130901 kkato@PROSITE add start
	/**
	 * メソッド名 : disableFlgのGetterメソッド
	 * 機能概要 : disableFlgを取得する。
	 * @return disableFlg
	 */
	public int getDisableFlg() {
		return disableFlg;
	}

	/**
	 * メソッド名 : disableFlgのSetterメソッド
	 * 機能概要 : disableFlgをセットする。
	 * @param disableFlg
	 */
	public void setDisableFlg(int disableFlg) {
		this.disableFlg = disableFlg;
	}
	// 20130901 kkato@PROSITE add end
}
