/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : NonRecurringSchedulesLink.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/18     komakiys         初版作成
 * 2010/06/17     hiroyasu         コミット処理プロセス化対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.commitproc.common.vo;

import java.io.Serializable;
import java.util.List;

/**
 * クラス名 : NonRecurringSchedulesLink
 * 機能概要 : 臨時スケジュールクリンクデータクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/18
 *          新規作成
 * @see
 */
public class NonRecurringSchedulesLink implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 6887374074243767968L;

	// スケジュールオブジェクトのSeqNoとのリンク
	private int schedulesSeqNo = 0;
	// 開始日付
	private String startDate = null;
	// 開始時刻
	private String startTime = null;
	// 終了日付
	private String endDate = null;
	// 終了時刻
	private String endTime = null;
	// 開始日付リスト
	private List<String> startDateList = null;
	// 開始時刻リスト
	private List<String> startTimeList = null;
	// 終了日付リスト
	private List<String> endDateList = null;
	// 終了時刻リスト
	private List<String> endTimeList = null;
	// スケジュールオブジェクト名
	private String name = null;
	// Vsys-ID
	private String vsysId = null;
	// 世代番号
	private int generationNo = 0;

	/**
	 * メソッド名 : schedulesSeqNoのGetterメソッド
	 * 機能概要 : schedulesSeqNoを取得する。
	 * @return schedulesSeqNo
	 */
	public int getSchedulesSeqNo() {
		return schedulesSeqNo;
	}
	/**
	 * メソッド名 : schedulesSeqNoのSetterメソッド
	 * 機能概要 : schedulesSeqNoをセットする。
	 * @param schedulesSeqNo
	 */
	public void setSchedulesSeqNo(int schedulesSeqNo) {
		this.schedulesSeqNo = schedulesSeqNo;
	}
	/**
	 * メソッド名 : startDateのGetterメソッド
	 * 機能概要 : startDateを取得する。
	 * @return startDate
	 */
	public String getStartDate() {
		return startDate;
	}
	/**
	 * メソッド名 : startDateのSetterメソッド
	 * 機能概要 : startDateをセットする。
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	/**
	 * メソッド名 : startTimeのGetterメソッド
	 * 機能概要 : startTimeを取得する。
	 * @return startTime
	 */
	public String getStartTime() {
		return startTime;
	}
	/**
	 * メソッド名 : startTimeのSetterメソッド
	 * 機能概要 : startTimeをセットする。
	 * @param startTime
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	/**
	 * メソッド名 : endDateのGetterメソッド
	 * 機能概要 : endDateを取得する。
	 * @return endDate
	 */
	public String getEndDate() {
		return endDate;
	}
	/**
	 * メソッド名 : endDateのSetterメソッド
	 * 機能概要 : endDateをセットする。
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	/**
	 * メソッド名 : endTimeのGetterメソッド
	 * 機能概要 : endTimeを取得する。
	 * @return endTime
	 */
	public String getEndTime() {
		return endTime;
	}
	/**
	 * メソッド名 : endTimeのSetterメソッド
	 * 機能概要 : endTimeをセットする。
	 * @param endTime
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	/**
	 * メソッド名 : startDateListのGetterメソッド
	 * 機能概要 : startDateListを取得する。
	 * @return startDateList
	 */
	public List<String> getStartDateList() {
		return startDateList;
	}
	/**
	 * メソッド名 : startDateListのSetterメソッド
	 * 機能概要 : startDateListをセットする。
	 * @param startDateList
	 */
	public void setStartDateList(List<String> startDateList) {
		this.startDateList = startDateList;
	}
	/**
	 * メソッド名 : startTimeListのGetterメソッド
	 * 機能概要 : startTimeListを取得する。
	 * @return startTimeList
	 */
	public List<String> getStartTimeList() {
		return startTimeList;
	}
	/**
	 * メソッド名 : startTimeListのSetterメソッド
	 * 機能概要 : startTimeListをセットする。
	 * @param startTimeList
	 */
	public void setStartTimeList(List<String> startTimeList) {
		this.startTimeList = startTimeList;
	}
	/**
	 * メソッド名 : endDateListのGetterメソッド
	 * 機能概要 : endDateListを取得する。
	 * @return endDateList
	 */
	public List<String> getEndDateList() {
		return endDateList;
	}
	/**
	 * メソッド名 : endDateListのSetterメソッド
	 * 機能概要 : endDateListをセットする。
	 * @param endDateList
	 */
	public void setEndDateList(List<String> endDateList) {
		this.endDateList = endDateList;
	}
	/**
	 * メソッド名 : endTimeListのGetterメソッド
	 * 機能概要 : endTimeListを取得する。
	 * @return endTimeList
	 */
	public List<String> getEndTimeList() {
		return endTimeList;
	}
	/**
	 * メソッド名 : endTimeListのSetterメソッド
	 * 機能概要 : endTimeListをセットする。
	 * @param endTimeList
	 */
	public void setEndTimeList(List<String> endTimeList) {
		this.endTimeList = endTimeList;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得する。
	 * @return vsysId
	 */
	public String getVsysId() {
		return vsysId;
	}
	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdをセットする。
	 * @param vsysId
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}
	/**
	 * メソッド名 : generationNoのGetterメソッド
	 * 機能概要 : generationNoを取得する。
	 * @return generationNo
	 */
	public int getGenerationNo() {
		return generationNo;
	}
	/**
	 * メソッド名 : generationNoのSetterメソッド
	 * 機能概要 : generationNoをセットする。
	 * @param generationNo
	 */
	public void setGenerationNo(int generationNo) {
		this.generationNo = generationNo;
	}
}
