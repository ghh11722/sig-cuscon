/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CusconFWDelete.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/23     komakiys                初版作成
 * 2010/06/17     hiroyasu                コミット処理プロセス化対応
 * 2011/06/10	  inoue@prosite           Webフィルタの追加対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.commitproc.common;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.commitproc.common.vo.InputBase;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;

/**
 * クラス名 : CusconFWDelete
 * 機能概要 : FW設定情報DB削除処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/23
 *          新規作成
 * @see
 */
public class CusconFWDelete {

    private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.commitproc.common.CusconFWDelete");

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 */
	public CusconFWDelete() {
		log.debug("CusconFWDeleteコンストラクタ2処理開始");
		log.debug("CusconFWDeleteコンストラクタ2処理終了");
	}


	/**
	 * メソッド名 : deleteFWInfo
	 * 機能概要 : 指定VsysID/指定世代についてFW設定情報DB削除処理を行う。
	 * @param generationNo 世代番号
	 * @param updateDAO
	 * @param queryDAO
	 * @param uvo
	 * @throws Exception
	 */
	public void deleteFWInfo(int generationNo, UpdateDAO updateDAO,
							QueryDAO queryDAO, CusconUVO uvo,
							MessageAccessor messageAccessor) throws Exception {

		log.debug("deleteFWInfo処理開始:vsysId = " +
				uvo.getVsysId() + ", generationNo = " + generationNo);
		InputBase base = new InputBase();

		// 引数情報を検索条件に設定する。
		base.setVsysId(uvo.getVsysId());
		base.setGenerationNo(generationNo);

		/***********************************************************
		 *  ポリシー情報削除処理
		 ***********************************************************/
		try {
			// ローカル出所アドレステーブルのデータを削除する。
			updateDAO.execute("CommonT_LocalSourceAddress-1", base);

			// ローカル宛先アドレステーブルのデータを削除する。
			updateDAO.execute("CommonT_LocalDestinationAddress-1", base);

			// 出所ゾーンリンクのデータを削除する。
			updateDAO.execute("CommonT_SourceZoneLink-1", base);

			// 宛先ゾーンリンクのデータを削除する。
			updateDAO.execute("CommonT_DestinationZoneLink-1", base);

			// 出所アドレスリンクのデータを削除する。
			updateDAO.execute("CommonT_SourceAddressLink-1", base);

			// 宛先アドレスリンクのデータを削除する。
			updateDAO.execute("CommonT_DestinationAddressLink-1", base);

			// アプリケーションリンクのデータを削除する。
			updateDAO.execute("CommonT_ApplicationLink-1", base);

			// サービスリンクのデータを削除する。
			updateDAO.execute("CommonT_ServiceLink-1", base);

//2011/06/10 add start inoue@prosite
			// Webフィルタリンクのデータを削除する。
			updateDAO.execute("DeleteT_UrlFilterLink-1", base);
//2011/06/10 add end inoue@prosite

			// セキュリティポリシー一覧のデータを削除する。
			updateDAO.execute("CommonT_SecurityRules-3", base);

		/***********************************************************
		 *  オブジェクト情報削除処理
		 ***********************************************************/

			// アドレスグループリンクのデータを削除する。
			updateDAO.execute("CommonT_AddressGroupsLink-1", base);

			// アドレスグループオブジェクトのデータを削除する。
			updateDAO.execute("CommonT_AddressGroups-4", base);

			// アドレスオブジェクトのデータを削除する。
			updateDAO.execute("CommonT_Addresses-4", base);

			// アプリケーショングループリンクのデータを削除する。
			updateDAO.execute("CommonT_ApplicationGroupsLink-1", base);

			// アプリケーショングループオブジェクトのデータを削除する。
			updateDAO.execute("CommonT_ApplicationGroups-4", base);

			// アプリケーションフィルタカテゴリリンクのデータを削除する。
			updateDAO.execute("CommonT_AplFltCategoryLink-1", base);

			// アプリケーションフィルタサブカテゴリリンクのデータを削除する。
			updateDAO.execute("CommonT_AplFltSubCategoryLink-1", base);

			// アプリケーションフィルタテクノロジリンクのデータを削除する。
			updateDAO.execute("CommonT_AplFltTechnologyLink-1", base);

			// アプリケーションフィルタリスクリンクのデータを削除する。
			updateDAO.execute("CommonT_AplFltRiskLink-1", base);

			// アプリケーションフィルタオブジェクトのデータを削除する。
			updateDAO.execute("CommonT_ApplicationFilters-4", base);

			// サービスグループリンクのデータを削除する。
			updateDAO.execute("CommonT_ServicesGroupsLink-1", base);

			// サービスグループオブジェクトのデータを削除する。
			updateDAO.execute("CommonT_ServicesGroups-4", base);

			// サービスオブジェクトのデータを削除する。
			updateDAO.execute("CommonT_Services-4", base);

			// デイリースケジュールクリンクのデータを削除する。
			updateDAO.execute("CommonT_DailySchedulesLink-1", base);

			// ウィークリースケジュールクリンクのデータを削除する。
			updateDAO.execute("CommonT_WeeklySchedulesLink-1", base);

			// 臨時スケジュールクリンクのデータを削除する。
			updateDAO.execute("CommonT_NonRecurringSchedulesLink-1", base);

			// スケジュールオブジェクトのデータを削除する。
			updateDAO.execute("CommonT_Schedules-5", base);

			if (base.getGenerationNo() == CusconConst.ZERO_GENE) {
				// オブジェクト旧名のデータを削除する。
				updateDAO.execute("CommonT_ObjectOldName-2", base);
			}
//2011/06/10 add start inoue@prosite
			// Webフィルタカテゴリのデータを削除する。
			updateDAO.execute("DeleteT_UrlFilterCategories-1", base);
			// Webフィルタオブジェクトのデータを削除する。
			updateDAO.execute("DeleteT_UrlFilters-1", base);
//2011/06/10 add end inoue@prosite
		} catch (Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FZ001003", objectgArray, uvo));
			throw e;
		}

		log.debug("deleteFWInfo処理終了");
	}

}
