/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : FilterInfo.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/20     morisou                 初版作成
 * 2010/06/17     hiroyasu                コミット処理プロセス化対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.commitproc.common.vo;

import java.io.Serializable;

/**
 * クラス名 : FilterInfo
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/20
 *          新規作成
 * @see
 */
public class FilterInfo implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 2021867338217261594L;
	// アプリケーションフィルタのSEQ_NO
	private int seqNo = 0;
	// アプリケーションフィルタ名
	private String name = null;
	// カテゴリ名
	private String category = null;
	// サブカテゴリ名
	private String subCategory = null;
	// テクノロジ名
	private String technology = null;
	// リスク
	private int risk = 0;

	/**
	 * メソッド名 : seqNoのGetterメソッド
	 * 機能概要 : seqNoを取得する。
	 * @return seqNo
	 */
	public int getSeqNo() {
		return seqNo;
	}
	/**
	 * メソッド名 : seqNoのSetterメソッド
	 * 機能概要 : seqNoをセットする。
	 * @param seqNo
	 */
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : categoryのGetterメソッド
	 * 機能概要 : categoryを取得する。
	 * @return category
	 */
	public String getCategory() {
		return category;
	}
	/**
	 * メソッド名 : categoryのSetterメソッド
	 * 機能概要 : categoryをセットする。
	 * @param category
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	 * メソッド名 : subCategoryのGetterメソッド
	 * 機能概要 : subCategoryを取得する。
	 * @return subCategory
	 */
	public String getSubCategory() {
		return subCategory;
	}
	/**
	 * メソッド名 : subCategoryのSetterメソッド
	 * 機能概要 : subCategoryをセットする。
	 * @param subCategory
	 */
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}
	/**
	 * メソッド名 : technologyのGetterメソッド
	 * 機能概要 : technologyを取得する。
	 * @return technology
	 */
	public String getTechnology() {
		return technology;
	}
	/**
	 * メソッド名 : technologyのSetterメソッド
	 * 機能概要 : technologyをセットする。
	 * @param technology
	 */
	public void setTechnology(String technology) {
		this.technology = technology;
	}
	/**
	 * メソッド名 : riskのGetterメソッド
	 * 機能概要 : riskを取得する。
	 * @return risk
	 */
	public int getRisk() {
		return risk;
	}
	/**
	 * メソッド名 : riskのSetterメソッド
	 * 機能概要 : riskをセットする。
	 * @param risk
	 */
	public void setRisk(int risk) {
		this.risk = risk;
	}
}
