/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : InputBase.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/18     komakiys         初版作成
 * 2010/06/17     hiroyasu         コミット処理プロセス化対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.commitproc.common.vo;

import java.io.Serializable;
import java.util.List;

/**
 * クラス名 : InputBase
 * 機能概要 : 基本検索条件クラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/18
 *          新規作成
 * @see
 */
public class InputBase implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = -4943411648259685640L;

	// Vsys-ID
	private String vsysId = null;
	// 世代番号
	private int generationNo = 0;
	// 変更フラグ
	private int modFlg = 0;
	// 世代番号(新)
	private int newGenerationNo = 0;
	// 検索対象外ポリシー
	private List<String> nonSubject = null;
	// key
	private String key = null;
//2011/06/10 add start inoue@prosite
	private int urlfilter_seqno = 0;
//2011/06/10 add end inoue@prosite

	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得する。
	 * @return vsysId
	 */
	public String getVsysId() {
		return vsysId;
	}
	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdをセットする。
	 * @param vsysId
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}
	/**
	 * メソッド名 : generationNoのGetterメソッド
	 * 機能概要 : generationNoを取得する。
	 * @return generationNo
	 */
	public int getGenerationNo() {
		return generationNo;
	}
	/**
	 * メソッド名 : generationNoのSetterメソッド
	 * 機能概要 : generationNoをセットする。
	 * @param generationNo
	 */
	public void setGenerationNo(int generationNo) {
		this.generationNo = generationNo;
	}
	/**
	 * メソッド名 : modFlgのGetterメソッド
	 * 機能概要 : modFlgを取得する。
	 * @return modFlg
	 */
	public int getModFlg() {
		return modFlg;
	}
	/**
	 * メソッド名 : modFlgのSetterメソッド
	 * 機能概要 : modFlgをセットする。
	 * @param modFlg
	 */
	public void setModFlg(int modFlg) {
		this.modFlg = modFlg;
	}
	/**
	 * メソッド名 : newGenerationNoのGetterメソッド
	 * 機能概要 : newGenerationNoを取得する。
	 * @return newGenerationNo
	 */
	public int getNewGenerationNo() {
		return newGenerationNo;
	}
	/**
	 * メソッド名 : newGenerationNoのSetterメソッド
	 * 機能概要 : newGenerationNoをセットする。
	 * @param newGenerationNo
	 */
	public void setNewGenerationNo(int newGenerationNo) {
		this.newGenerationNo = newGenerationNo;
	}
	/**
	 * メソッド名 : nonSubjectのGetterメソッド
	 * 機能概要 : nonSubjectを取得する。
	 * @return nonSubject
	 */
	public List<String> getNonSubject() {
		return nonSubject;
	}
	/**
	 * メソッド名 : nonSubjectのSetterメソッド
	 * 機能概要 : nonSubjectをセットする。
	 * @param nonSubject
	 */
	public void setNonSubject(List<String> nonSubject) {
		this.nonSubject = nonSubject;
	}
	/**
	 * @param key セットする key
	 */
	public void setKey(String key) {
		this.key = key;
	}
	/**
	 * @return key
	 */
	public String getKey() {
		return key;
	}
//2011/06/10 add start inoue@prosite
	/**
	 * @param urlfilter_seqno セットする urlfilter_seqno
	 */
	public void setUrlfilter_seqno(int urlfilter_seqno) {
		this.urlfilter_seqno = urlfilter_seqno;
	}
	/**
	 * @return urlfilter_seqno
	 */
	public int getUrlfilter_seqno() {
		return urlfilter_seqno;
	}
//2011/06/10 add end inoue@prosite
}
