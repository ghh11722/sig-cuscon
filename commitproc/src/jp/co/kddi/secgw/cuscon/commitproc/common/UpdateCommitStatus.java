/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CommitEnd.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/06/17     hiroyasu         初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.commitproc.common;

import jp.co.kddi.secgw.cuscon.commitproc.common.vo.CommitStatus;
import jp.terasoluna.fw.dao.UpdateDAO;

import org.apache.log4j.Logger;

public class UpdateCommitStatus {
	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.commitproc.common.UpdateCommitStatus");

    /**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 */
	public UpdateCommitStatus() {
	}

	/**
	 * メソッド名 : setCommitStatus
	 * 機能概要 : Commitステータス確認テーブルを更新する。
	 * @param updateDAO
	 * @param commitStatus
	 * @param uvo
	 * @param messageAccessor
	 * @throws Exception
	 */
	public void setCommitStatus(UpdateDAO updateDAO,
					CommitStatus commitStatus, CusconUVO uvo,
					MessageAccessor messageAccessor) throws Exception {

		log.debug("setCommitStatus処理開始");

		try {
			// データをDBに登録する。
			updateDAO.execute("CommonT_CommitStatus-1", commitStatus);
		} catch (Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK180002", objectgArray, uvo));
			throw e;
		}
		log.debug("setCommitStatus処理終了");
	}
}
