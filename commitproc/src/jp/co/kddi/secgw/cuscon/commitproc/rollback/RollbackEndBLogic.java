/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名	: RollbackEndBLogic.java
 *
 * [変更履歴]
 * 日付 		  更新者				  内容
 * 2010/03/01	  komakiys				  初版作成
 * 2010/06/17	  hiroyasu				  コミット処理プロセス化対応
 * 2016/11/21	  T.Yamazaki@Plum		  Terasolunaアップデート/SSH対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.commitproc.rollback;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionStatus;

import jp.co.kddi.secgw.cuscon.commitproc.common.CreateCusconUVO;
import jp.co.kddi.secgw.cuscon.commitproc.common.CusconConst;
import jp.co.kddi.secgw.cuscon.commitproc.common.CusconFWSet;
import jp.co.kddi.secgw.cuscon.commitproc.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.commitproc.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.commitproc.common.PolicyList;
import jp.co.kddi.secgw.cuscon.commitproc.common.ResultJudgeData;
import jp.co.kddi.secgw.cuscon.commitproc.common.TimeoutException;
import jp.co.kddi.secgw.cuscon.commitproc.common.UpdateCommitFlg;
import jp.co.kddi.secgw.cuscon.commitproc.common.UpdateCommitStatus;
import jp.co.kddi.secgw.cuscon.commitproc.common.UpdatePA;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.CommitStatus;
import jp.co.kddi.secgw.cuscon.commitproc.common.vo.SelectPolicyList;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.util.PropertyUtil;

/**
 * クラス名 : RollbackEndBLogic
 * 機能概要 :ロールバック処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *			Created 2010/03/01
 *			新規作成
 *			2.0 T.Yamazaki@Plum Systems Inc.
 *			Updated 2016/11/21
 *			Terasolunaアップデート/SSH対応
 * @see
 */
public class RollbackEndBLogic {

	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.commitproc.rollback.RollbackEndBLogic");
	// メッセージクラス
	private static MessageAccessor messageAccessor = null;

  	private static ApplicationContext context = null;
  	private static DataSourceTransactionManager manager = null;
  	private static CusconUVO uvo = null;
  	private static int geneNo = 0;
	// QueryDAO
	private static QueryDAO queryDAO;
	// UpdateDAO
	private static UpdateDAO updateDAO;

	public static final String contxtFileName = "applicationContext.xml";
	// 日時フォーマット
	private static final String DATE_FORMAT =
		PropertyUtil.getProperty("fwsetting.common.dateformat");

	/**
	 * メソッド名 :main
	 * 機能概要 : UpdatePA、CusconFWSetを呼び出し、選択世代についてロールバック処理を行う。
	 * @param param RollbackEndの入力クラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public static void main(String[] args) {

		log.debug("----------RollbackEndBLogic(2016/12ビルド版) 処理開始--------------------");
		log.debug("RollbackEndBLogic処理開始");

		log.debug("指定された引数は、" + args.length + "個です");
		if (args.length != 4) {
			log.fatal("引数が不正です。");
			System.exit(0);
		}

		log.debug("引数表示");
		for (int i=0; i<args.length; ++i) {
			log.debug(args[i]);
		}

		// 引数をCusconUVOに設定する
		uvo = new CusconUVO();
		try {
			uvo.setLoginId(URLDecoder.decode(args[0], "UTF-8"));
			uvo.setVsysId(URLDecoder.decode(args[1], "UTF-8"));
			uvo.setGrantFlagStr(URLDecoder.decode(args[2], "UTF-8"));
		} catch (UnsupportedEncodingException e3) {
			log.fatal("引数が不正です。");
			System.exit(0);
		}

		// 世代番号を設定する
		try {
			geneNo = Integer.parseInt(args[3]);
		} catch (Exception e) {
			log.fatal(uvo.getGrantFlagStr() + " " + uvo.getLoginId() +
					" " + uvo.getVsysId() + " " +
					"世代番号の数値変換に失敗しました。"
					+ e.getMessage());
		}

		if (1 > geneNo || 3 < geneNo) {
			log.fatal(uvo.getGrantFlagStr() + " " + uvo.getLoginId() +
					" " + uvo.getVsysId() + " " +
					"世代番号が不正です。");
		}

		try {
	  		context = new ClassPathXmlApplicationContext(contxtFileName);
	  		manager = (DataSourceTransactionManager)context.getBean("transactionManager");
		} catch (Exception e) {
			log.fatal(uvo.getGrantFlagStr() + " " + uvo.getLoginId() +
					" " + uvo.getVsysId() + " " +
					"アプリケーションコンテキストの読み込みに失敗しました。"
					+ e.getMessage());
			System.exit(0);
		}

		// コミット処理結果格納用エリア
		CommitStatus result = new CommitStatus();

		try{
			// CusconUVOを作成する。
			CreateCusconUVO cv = new CreateCusconUVO();
			cv.setCusconUVO(queryDAO, uvo, messageAccessor);
			result.setVsysId(uvo.getVsysId());

			// 指定世代のポリシー一覧情報を取得する。
			PolicyList policy = new PolicyList();
			List<SelectPolicyList> policyList =
				policy.getPolicylist(
						queryDAO, uvo,
						geneNo, CusconConst.COMMIT, messageAccessor);

			// UpdatePAクラスの生成
			UpdatePA update = new UpdatePA();
			// 処理結果格納用
			ResultJudgeData resultJudge = new ResultJudgeData();

			update.updatePAProcess(resultJudge, policyList,
							uvo.getVsysId(),
							geneNo, uvo.getPaLoginId(),
							uvo.getPaPasswd(), queryDAO, uvo, messageAccessor);

			// ロールバックコマンド失敗の場合
			if(resultJudge.getResultFlg() == 3) {
				log.error("ロールバックコマンド失敗");
				log.error(resultJudge.getResultData());

				// ロールバックコマンド失敗メッセージ出力
				result.setMessage(messageAccessor.getMessage("DK070007", null));
				// ステータスを設定する。
				result.setStatus(CusconConst.FAiILURE);
				// 終了時刻を設定する。
				Date date = new Date();
				SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
				result.setEndTime(dateFormat.format(date));
				// コミットステータス確認テーブルを更新する。
				UpdateCommitStatus ucs = new UpdateCommitStatus();
				ucs.setCommitStatus(updateDAO, result, uvo, messageAccessor);

				// コミット中フラグOFF
				UpdateCommitFlg ucf = new UpdateCommitFlg();
				ucf.updatePAProcessFlg(CusconConst.COMMIT_OFF, queryDAO);

				System.exit(0);
			}

			// DB更新管理
			updateFWManage();

			// ロールバック成功のメッセージ格納
			result.setMessage(messageAccessor.getMessage("DK070008", null));
			// ステータスを設定する。
			result.setStatus(CusconConst.SUCCESS);
			// 終了時刻を設定する。
			Date date = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
			result.setEndTime(dateFormat.format(date));
			// コミットステータス確認テーブルを更新する。
			UpdateCommitStatus ucs = new UpdateCommitStatus();
			ucs.setCommitStatus(updateDAO, result, uvo, messageAccessor);

			// コミット中フラグOFF
			UpdateCommitFlg ucf = new UpdateCommitFlg();
			ucf.updatePAProcessFlg(CusconConst.COMMIT_OFF, queryDAO);

		} catch(TimeoutException e) {
			// コマンドタイムアウトメッセージ出力
			log.debug("コマンドタイムアウト");
			result.setMessage(messageAccessor.getMessage("DK070007", null));
			result.setStatus(CusconConst.FAiILURE);
			// 終了時刻を設定する。
			Date date = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
			result.setEndTime(dateFormat.format(date));
			// コミットステータス確認テーブルを更新する。
			UpdateCommitStatus ucs = new UpdateCommitStatus();
			try {
				ucs.setCommitStatus(updateDAO, result, uvo, messageAccessor);
			} catch (Exception e2) {
				// DBアクセスエラー
				Object[] objectgArray = {e2.getMessage()};
				log.fatal(messageAccessor.getMessage("FK180002", objectgArray, uvo));
			}

			// コミット中フラグOFF
		   	UpdateCommitFlg ucf = new UpdateCommitFlg();
   			try {
   				ucf.updatePAProcessFlg(CusconConst.COMMIT_OFF, queryDAO);
   			} catch (Exception e1) {
   				Object[] objectgArray = {e1.getMessage()};
   				log.error(messageAccessor.getMessage("EK071003", objectgArray, uvo));
   			}
			System.exit(0);

		} catch (Exception e) {
			// ロールバック処理失敗ログ出力
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("EK071002", objectgArray, uvo));
			result.setMessage(messageAccessor.getMessage("DK070007", null));
			result.setStatus(CusconConst.FAiILURE);
			// 終了時刻を設定する。
			Date date = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
			result.setEndTime(dateFormat.format(date));
			// コミットステータス確認テーブルを更新する。
			UpdateCommitStatus ucs = new UpdateCommitStatus();
			try {
				ucs.setCommitStatus(updateDAO, result, uvo, messageAccessor);
			} catch (Exception e2) {
				// DBアクセスエラー
				Object[] objectgArray1 = {e2.getMessage()};
				log.fatal(messageAccessor.getMessage("FK180002", objectgArray1, uvo));
			}

			// コミット中フラグOFF
		   	UpdateCommitFlg ucf = new UpdateCommitFlg();
   			try {
   				ucf.updatePAProcessFlg(CusconConst.COMMIT_OFF, queryDAO);
   			} catch (Exception e1) {
   				Object[] objectgArray1 = {e1.getMessage()};
   				log.error(messageAccessor.getMessage("EK071003", objectgArray1, uvo));
   			}
			System.exit(0);
		}

		log.debug("RollbackEndBLogic処理終了");
		System.exit(0);
	}

	/**
	 * メソッド名 : updateFWManage
	 * 機能概要 : ポリシー/オブジェクトテーブルをコミット済情報で更新する。
	 * @throws Exception
	 */
	private static void updateFWManage() throws Exception {
		log.debug("updateFWManage処理開始");

		// トランザクションスタート
		TransactionStatus ts = manager.getTransaction(null);

		try {
			// CusconFWSetクラスを生成する。
			CusconFWSet fwset = new CusconFWSet();

			fwset.setFWInfo(updateDAO, queryDAO, uvo, messageAccessor);
		} catch (Exception e) {
			manager.rollback(ts);
			log.debug("updateFWManage rollback 終了");
			throw e;
		}
		manager.commit(ts);

		log.debug("updateFWManage処理終了");
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		RollbackEndBLogic.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : updateDAOのSetterメソッド
	 * 機能概要 : updateDAOをセットする。
	 * @param updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		RollbackEndBLogic.updateDAO = updateDAO;
	}
	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		RollbackEndBLogic.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
