#!/bin/bash

# 環境変数設定
# ※psqlコマンドが実行できるように環境変数を設定する。
#export PATH="/usr/kerberos/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/jdk/bin:/usr/local/pgsql/bin:/home/postgres/bin"
export PATH="/opt/powergres/bin:/usr/kerberos/sbin:/usr/kerberos/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin"

# 変数設定
db_name="cuscondb"               # データベース名
db_user="postgres"               # PostgreSQL接続ユーザ
db_pswd="#=gIHT%u"              # PostgreSQL接続パスワード
vsysid="$1"                      # 第1引数 VsysID
startDate="$2"                   # 第2引数 検索条件（ログ日付 開始日）
endDate="$3"                     # 第3引数 検索条件（ログ日付 終了日）
csv_file="$4"                    # 第4引数 出力CSVファイルパス名
zip_file="$5"                    # 第5引数 出力ZIPファイルパス名
sqlPatern="$6"                   # 第6引数 実行するSQL
subtypeFlg="$7"                  # 第7引数 サブタイプフラグ(1:vulnera,2:url,3,virus,4:spyware 20110525 ktakenaka@PROSITE add
extension=".csv"

csv_file_tmp="$csv_file"".tmp"   # CSVテンポラリファイルパス名

#csv_head="/usr/local/cuscon/bin//idslog_head.dat" # CSVヘッダファイルパス 20110525 ktakenaka@PROSITE del

# 20110525 ktakenaka@PROSITE add start
# SQL COLUMN for "vulnera,virus,spyware"
COLUMN_THREAT=""
COLUMN_THREAT="$COLUMN_THREAT   RECEIVE_TIME      AS reciveTime, "
COLUMN_THREAT="$COLUMN_THREAT   TYPE              AS type, "
COLUMN_THREAT="$COLUMN_THREAT   SOURCE_IP         AS sourceIp, "
COLUMN_THREAT="$COLUMN_THREAT   DESTINATION_IP    AS destIp, "
COLUMN_THREAT="$COLUMN_THREAT   RULE_NAME         AS ruleName, "
COLUMN_THREAT="$COLUMN_THREAT   APPLICATION       AS application, "
COLUMN_THREAT="$COLUMN_THREAT   SOURCE_ZONE       AS sourceZone, "
COLUMN_THREAT="$COLUMN_THREAT   DESTINATION_ZONE  AS destinationZone, "
COLUMN_THREAT="$COLUMN_THREAT   SOURCE_PORT       AS sourcePort, "
COLUMN_THREAT="$COLUMN_THREAT   DESTINATION_PORT  AS destinationPort, "
COLUMN_THREAT="$COLUMN_THREAT   ACTION            AS action "

# SQL COLUMN for "url"
COLUMN_URL=""
COLUMN_URL="$COLUMN_URL   RECEIVE_TIME      AS reciveTime, "
COLUMN_URL="$COLUMN_URL   CATEGORY          AS category, "
COLUMN_URL="$COLUMN_URL   MISCELLANEOUS     AS miscellaneous, "
COLUMN_URL="$COLUMN_URL   SOURCE_ZONE       AS sourceZone, "
COLUMN_URL="$COLUMN_URL   DESTINATION_ZONE  AS destinationZone, "
COLUMN_URL="$COLUMN_URL   SOURCE_IP         AS sourceIp, "
COLUMN_URL="$COLUMN_URL   DESTINATION_IP    AS destIp, "
COLUMN_URL="$COLUMN_URL   SOURCE_PORT       AS sourcePort, "
COLUMN_URL="$COLUMN_URL   DESTINATION_PORT  AS destinationPort, "
COLUMN_URL="$COLUMN_URL   APPLICATION       AS application, "
COLUMN_URL="$COLUMN_URL   ACTION            AS action "

# 列名,テーブル名,CSVヘッダファイルパスを設定
if test $subtypeFlg = '1' ; then
    COLUMN=$COLUMN_THREAT
    TABLE_NAME=" T_LogIdsIps ";
    csv_head="/usr/local/cuscon/bin//idslog_head.dat"
elif test $subtypeFlg = '2' ; then
    COLUMN=$COLUMN_URL
    TABLE_NAME=" T_LogUrlFilter ";
    csv_head="/usr/local/cuscon/bin//urllog_head.dat"
elif test $subtypeFlg = '3' ; then
    COLUMN=$COLUMN_THREAT
    TABLE_NAME=" T_LogVirusCheck ";
    csv_head="/usr/local/cuscon/bin//idslog_head.dat"
elif test $subtypeFlg = '4' ; then
    COLUMN=$COLUMN_THREAT
    TABLE_NAME=" T_LogSpyware ";
    csv_head="/usr/local/cuscon/bin//idslog_head.dat"
else
  exit 1
fi
# 20110602 ktakenaka@PROSITE add end

iLogSQL3=""
iLogSQL3="$iLogSQL3 SELECT "
# 20110603 ktakenaka@PROSITE del start
#iLogSQL3="$iLogSQL3   RECEIVE_TIME      AS reciveTime, "
#iLogSQL3="$iLogSQL3   TYPE              AS type, "
#iLogSQL3="$iLogSQL3   SOURCE_IP         AS sourceIp, "
#iLogSQL3="$iLogSQL3   DESTINATION_IP    AS destIp, "
#iLogSQL3="$iLogSQL3   RULE_NAME         AS ruleName, "
#iLogSQL3="$iLogSQL3   APPLICATION       AS application, "
#iLogSQL3="$iLogSQL3   SOURCE_ZONE       AS sourceZone, "
#iLogSQL3="$iLogSQL3   DESTINATION_ZONE  AS destinationZone, "
#iLogSQL3="$iLogSQL3   SOURCE_PORT       AS sourcePort, "
#iLogSQL3="$iLogSQL3   DESTINATION_PORT  AS destinationPort, "
#iLogSQL3="$iLogSQL3   ACTION            AS action "
# 20110603 ktakenaka@PROSITE del end
iLogSQL3=$iLogSQL3$COLUMN             # 20110603 ktakenaka@PROSITE add
iLogSQL3="$iLogSQL3 FROM "
#iLogSQL3="$iLogSQL3   T_LogIdsIps "    20110603 ktakenaka@PROSITE del
iLogSQL3=$iLogSQL3$TABLE_NAME         # 20110604 ktakenaka@PROSITE add
iLogSQL3="$iLogSQL3 WHERE "
iLogSQL3="$iLogSQL3       VIRTUAL_SYSTEM = '$vsysid' "
#iLogSQL3="$iLogSQL3   AND ( to_date(RECEIVE_TIME, 'YYYY/MM/DD') >= to_date('$startDate', 'YYYY/MM/DD') "
#iLogSQL3="$iLogSQL3   AND   to_date('$endDate',   'YYYY/MM/DD') >= to_date(RECEIVE_TIME, 'YYYY/MM/DD') ) "
iLogSQL3="$iLogSQL3   AND ( RECEIVE_TIME >= '$startDate' AND '$endDate' >= RECEIVE_TIME ) "
#iLogSQL3="$iLogSQL3 ORDER BY to_timestamp(RECEIVE_TIME, 'YYYY/MM/DD HH24:MI:SS') ASC "
iLogSQL3="$iLogSQL3 ORDER BY RECEIVE_TIME ASC "
iLogSQL3="$iLogSQL3 ; "

iLogSQL4=""
iLogSQL4="$iLogSQL4 SELECT "
# 20110603 ktakenaka@PROSITE del start
#iLogSQL4="$iLogSQL4   RECEIVE_TIME      AS reciveTime, "
#iLogSQL4="$iLogSQL4   TYPE              AS type, "
#iLogSQL4="$iLogSQL4   SOURCE_IP         AS sourceIp, "
#iLogSQL4="$iLogSQL4   DESTINATION_IP    AS destIp, "
#iLogSQL4="$iLogSQL4   RULE_NAME         AS ruleName, "
#iLogSQL4="$iLogSQL4   APPLICATION       AS application, "
#iLogSQL4="$iLogSQL4   SOURCE_ZONE       AS sourceZone, "
#iLogSQL4="$iLogSQL4   DESTINATION_ZONE  AS destinationZone, "
#iLogSQL4="$iLogSQL4   SOURCE_PORT       AS sourcePort, "
#iLogSQL4="$iLogSQL4   DESTINATION_PORT  AS destinationPort, "
#iLogSQL4="$iLogSQL4   ACTION            AS action "
# 20110603 ktakenaka@PROSITE del end
iLogSQL4=$iLogSQL4$COLUMN             # 20110603 ktakenaka@PROSITE add
iLogSQL4="$iLogSQL4 FROM "
#iLogSQL4="$iLogSQL4   T_LogIdsIps "    20110604 ktakenaka@PROSITE del
iLogSQL4=$iLogSQL4$TABLE_NAME         # 20110604 ktakenaka@PROSITE add
iLogSQL4="$iLogSQL4 WHERE "
iLogSQL4="$iLogSQL4       VIRTUAL_SYSTEM = '$vsysid' "
#iLogSQL4="$iLogSQL4   AND ( to_date(RECEIVE_TIME, 'YYYY/MM/DD') >= to_date('$startDate', 'YYYY/MM/DD') ) "
iLogSQL4="$iLogSQL4   AND ( RECEIVE_TIME >= '$startDate' ) "
#iLogSQL4="$iLogSQL4 ORDER BY to_timestamp(RECEIVE_TIME, 'YYYY/MM/DD HH24:MI:SS') ASC "
iLogSQL4="$iLogSQL4 ORDER BY RECEIVE_TIME ASC "
iLogSQL4="$iLogSQL4 ; "


iLogSQL5=""
iLogSQL5="$iLogSQL5 SELECT "
# 20110603 ktakenaka@PROSITE del start
#iLogSQL5="$iLogSQL5   RECEIVE_TIME      AS reciveTime, "
#iLogSQL5="$iLogSQL5   TYPE              AS type, "
#iLogSQL5="$iLogSQL5   SOURCE_IP         AS sourceIp, "
#iLogSQL5="$iLogSQL5   DESTINATION_IP    AS destIp, "
#iLogSQL5="$iLogSQL5   RULE_NAME         AS ruleName, "
#iLogSQL5="$iLogSQL5   APPLICATION       AS application, "
#iLogSQL5="$iLogSQL5   SOURCE_ZONE       AS sourceZone, "
#iLogSQL5="$iLogSQL5   DESTINATION_ZONE  AS destinationZone, "
#iLogSQL5="$iLogSQL5   SOURCE_PORT       AS sourcePort, "
#iLogSQL5="$iLogSQL5   DESTINATION_PORT  AS destinationPort, "
#iLogSQL5="$iLogSQL5   ACTION            AS action "
# 20110603 ktakenaka@PROSITE del end
iLogSQL5=$iLogSQL5$COLUMN             # 20110604 ktakenaka@PROSITE add
iLogSQL5="$iLogSQL5 FROM "
#iLogSQL5="$iLogSQL5   T_LogIdsIps "    20110603 ktakenaka@PROSITE del
iLogSQL5=$iLogSQL5$TABLE_NAME         # 20110604 ktakenaka@PROSITE add
iLogSQL5="$iLogSQL5 WHERE "
iLogSQL5="$iLogSQL5       VIRTUAL_SYSTEM = '$vsysid' "
#iLogSQL5="$iLogSQL5   AND ( to_date('$endDate', 'YYYY/MM/DD') >= to_date(RECEIVE_TIME, 'YYYY/MM/DD') ) "
iLogSQL5="$iLogSQL5   AND ( '$endDate' >= RECEIVE_TIME ) "
#iLogSQL5="$iLogSQL5 ORDER BY to_timestamp(RECEIVE_TIME, 'YYYY/MM/DD HH24:MI:SS') ASC "
iLogSQL5="$iLogSQL5 ORDER BY RECEIVE_TIME ASC "
iLogSQL5="$iLogSQL5 ; "


iLogSQL6=""
iLogSQL6="$iLogSQL6 SELECT "
# 20110603 ktakenaka@PROSITE del start
#iLogSQL6="$iLogSQL6   RECEIVE_TIME      AS reciveTime, "
#iLogSQL6="$iLogSQL6   TYPE              AS type, "
#iLogSQL6="$iLogSQL6   SOURCE_IP         AS sourceIp, "
#iLogSQL6="$iLogSQL6   DESTINATION_IP    AS destIp, "
#iLogSQL6="$iLogSQL6   RULE_NAME         AS ruleName, "
#iLogSQL6="$iLogSQL6   APPLICATION       AS application, "
#iLogSQL6="$iLogSQL6   SOURCE_ZONE       AS sourceZone, "
#iLogSQL6="$iLogSQL6   DESTINATION_ZONE  AS destinationZone, "
#iLogSQL6="$iLogSQL6   SOURCE_PORT       AS sourcePort, "
#iLogSQL6="$iLogSQL6   DESTINATION_PORT  AS destinationPort, "
#iLogSQL6="$iLogSQL6   ACTION            AS action "
# 20110603 ktakenaka@PROSITE del end
iLogSQL6=$iLogSQL6$COLUMN             # 20110604 ktakenaka@PROSITE add
iLogSQL6="$iLogSQL6 FROM "
#iLogSQL6="$iLogSQL6   T_LogIdsIps "     20110624 ktakenaka@PROSITE del
iLogSQL6=$iLogSQL6$TABLE_NAME         # 20110604 ktakenaka@PROSITE add
iLogSQL6="$iLogSQL6 WHERE "
iLogSQL6="$iLogSQL6       VIRTUAL_SYSTEM = '$vsysid' "
#iLogSQL6="$iLogSQL6 ORDER BY to_timestamp(RECEIVE_TIME, 'YYYY/MM/DD HH24:MI:SS') ASC "
iLogSQL6="$iLogSQL6 ORDER BY RECEIVE_TIME ASC "
iLogSQL6="$iLogSQL6 ; "

# $sqlPatern から実行するSQLを決定
if test $sqlPatern = 'IdsLogBLogic-3' ; then
  sql=$iLogSQL3
fi
if test $sqlPatern = 'IdsLogBLogic-4' ; then
  sql=$iLogSQL4
fi
if test $sqlPatern = 'IdsLogBLogic-5' ; then
  sql=$iLogSQL5
fi
if test $sqlPatern = 'IdsLogBLogic-6' ; then
  sql=$iLogSQL6
fi

# SQL実行
# ヘッダなし
PGPASSWORD="$db_pswd" nice -n 1 psql -d "$db_name" -U "$db_user" --set FETCH_COUNT=10000 -A -F , -t -o "$csv_file_tmp" -c "$sql"

#SQL実行結果のチェック（テンポラリファイルのサイズで判断）
tmp_size=`wc -c $csv_file_tmp | awk '{print $1}'`
if test $tmp_size -eq 0 ; then
  exit 1
fi
##############################
# 20110620 PROSITE add start(65,000行越え対応)
##############################
tmp_fname=${csv_file_tmp##*/}
rst_path=${csv_file_tmp%/*}
rst_fname=${csv_file##*/}
rst_fname_without_extension=${rst_fname%.*}

cd $rst_path
split -l 65000 $tmp_fname
CNT=1
for FILE in x??; do
	cat $csv_head $FILE > $FILE.tmp
	mv $FILE.tmp $rst_fname_without_extension`expr 00$CNT`.csv
	CNT=`expr $CNT + 1`
done
##############################
# 20110620 PROSITE add end(65,000行越え対応)
##############################
# ZIP圧縮
zip -l -j "$zip_file" *.csv
if test $? -ne 0 ; then
  exit 2
fi

exit 0

