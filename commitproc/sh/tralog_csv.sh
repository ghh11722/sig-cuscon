#!/bin/bash

# 環境変数設定
# ※psqlコマンドが実行できるように環境変数を設定する。
#export PATH="/usr/kerberos/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/jdk/bin:/usr/local/pgsql/bin:/home/postgres/bin"
export PATH="/opt/powergres/bin:/usr/kerberos/sbin:/usr/kerberos/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin"

# 変数設定
db_name="cuscondb"              # データベース名
db_user="postgres"              # PostgreSQL接続ユーザ
db_pswd="#=gIHT%u"              # PostgreSQL接続パスワード
vsysid="$1"                     # 第1引数 VsysID
startDate="$2"                  # 第2引数 検索条件（ログ日付 開始日）
endDate="$3"                    # 第3引数 検索条件（ログ日付 終了日）
csv_file="$4"                   # 第4引数 出力CSVファイルパス名
zip_file="$5"                   # 第5引数 出力ZIPファイルパス名
sqlPatern="$6"                  # 第6引数 実行するSQL
csv_file_tmp="$csv_file"".tmp"  # CSVテンポラリファイルパス名
csv_head="/usr/local/cuscon/bin//tralog_head.dat" # CSVヘッダファイルパス名

tLogSQL3=""
tLogSQL3="$tLogSQL3 SELECT "
tLogSQL3="$tLogSQL3     RECEIVE_TIME      AS reciveTime, "
tLogSQL3="$tLogSQL3     TYPE              AS type, "
tLogSQL3="$tLogSQL3     SOURCE_IP         AS sourceIp, "
tLogSQL3="$tLogSQL3     DESTINATION_IP    AS destIp, "
tLogSQL3="$tLogSQL3     RULE_NAME         AS ruleName, "
tLogSQL3="$tLogSQL3     APPLICATION       AS application, "
tLogSQL3="$tLogSQL3     SOURCE_ZONE       AS sourceZone, "
tLogSQL3="$tLogSQL3     DESTINATION_ZONE  AS destinationZone, "
tLogSQL3="$tLogSQL3     SOURCE_PORT       AS sourcePort, "
tLogSQL3="$tLogSQL3     DESTINATION_PORT  AS destinationPort, "
tLogSQL3="$tLogSQL3     PROTOCOL          AS protocol, "
tLogSQL3="$tLogSQL3     ACTION            AS action, "
tLogSQL3="$tLogSQL3     BYTES             AS bytes "
tLogSQL3="$tLogSQL3 FROM "
tLogSQL3="$tLogSQL3     T_LogTraffic "
tLogSQL3="$tLogSQL3 WHERE "
tLogSQL3="$tLogSQL3           VIRTUAL_SYSTEM = '$vsysid' "
#tLogSQL3="$tLogSQL3     AND ( to_date(RECEIVE_TIME, 'YYYY/MM/DD') >= to_date('$startDate', 'YYYY/MM/DD') "
#tLogSQL3="$tLogSQL3     AND   to_date('$endDate',   'YYYY/MM/DD') >= to_date(RECEIVE_TIME, 'YYYY/MM/DD') ) "
tLogSQL3="$tLogSQL3     AND ( RECEIVE_TIME >= '$startDate' AND '$endDate' >= RECEIVE_TIME ) "
#tLogSQL3="$tLogSQL3 ORDER BY to_timestamp(RECEIVE_TIME, 'YYYY/MM/DD HH24:MI:SS') ASC "
tLogSQL3="$tLogSQL3 ORDER BY RECEIVE_TIME ASC "
tLogSQL3="$tLogSQL3 ; "

tLogSQL4=""
tLogSQL4="$tLogSQL4 SELECT "
tLogSQL4="$tLogSQL4     RECEIVE_TIME      AS reciveTime, "
tLogSQL4="$tLogSQL4     TYPE              AS type, "
tLogSQL4="$tLogSQL4     SOURCE_IP         AS sourceIp, "
tLogSQL4="$tLogSQL4     DESTINATION_IP    AS destIp, "
tLogSQL4="$tLogSQL4     RULE_NAME         AS ruleName, "
tLogSQL4="$tLogSQL4     APPLICATION       AS application, "
tLogSQL4="$tLogSQL4     SOURCE_ZONE       AS sourceZone, "
tLogSQL4="$tLogSQL4     DESTINATION_ZONE  AS destinationZone, "
tLogSQL4="$tLogSQL4     SOURCE_PORT       AS sourcePort, "
tLogSQL4="$tLogSQL4     DESTINATION_PORT  AS destinationPort, "
tLogSQL4="$tLogSQL4     PROTOCOL          AS protocol, "
tLogSQL4="$tLogSQL4     ACTION            AS action, "
tLogSQL4="$tLogSQL4     BYTES             AS bytes "
tLogSQL4="$tLogSQL4 FROM "
tLogSQL4="$tLogSQL4     T_LogTraffic "
tLogSQL4="$tLogSQL4 WHERE "
tLogSQL4="$tLogSQL4         VIRTUAL_SYSTEM = '$vsysid' "
#tLogSQL4="$tLogSQL4     AND ( to_date(RECEIVE_TIME, 'YYYY/MM/DD') >= to_date('$startDate', 'YYYY/MM/DD') ) "
tLogSQL4="$tLogSQL4     AND ( RECEIVE_TIME >= '$startDate' ) "
#tLogSQL4="$tLogSQL4 ORDER BY to_timestamp(RECEIVE_TIME, 'YYYY/MM/DD HH24:MI:SS') ASC "
tLogSQL4="$tLogSQL4 ORDER BY RECEIVE_TIME ASC "
tLogSQL4="$tLogSQL4 ; "

tLogSQL5=""
tLogSQL5="$tLogSQL5 SELECT "
tLogSQL5="$tLogSQL5     RECEIVE_TIME      AS reciveTime, "
tLogSQL5="$tLogSQL5     TYPE              AS type, "
tLogSQL5="$tLogSQL5     SOURCE_IP         AS sourceIp, "
tLogSQL5="$tLogSQL5     DESTINATION_IP    AS destIp, "
tLogSQL5="$tLogSQL5     RULE_NAME         AS ruleName, "
tLogSQL5="$tLogSQL5     APPLICATION       AS application, "
tLogSQL5="$tLogSQL5     SOURCE_ZONE       AS sourceZone, "
tLogSQL5="$tLogSQL5     DESTINATION_ZONE  AS destinationZone, "
tLogSQL5="$tLogSQL5     SOURCE_PORT       AS sourcePort, "
tLogSQL5="$tLogSQL5     DESTINATION_PORT  AS destinationPort, "
tLogSQL5="$tLogSQL5     PROTOCOL          AS protocol, "
tLogSQL5="$tLogSQL5     ACTION            AS action, "
tLogSQL5="$tLogSQL5     BYTES             AS bytes "
tLogSQL5="$tLogSQL5 FROM "
tLogSQL5="$tLogSQL5     T_LogTraffic "
tLogSQL5="$tLogSQL5 WHERE "
tLogSQL5="$tLogSQL5         VIRTUAL_SYSTEM = '$vsysid' "
#tLogSQL5="$tLogSQL5     AND ( to_date('$endDate', 'YYYY/MM/DD') >= to_date(RECEIVE_TIME, 'YYYY/MM/DD') ) "
tLogSQL5="$tLogSQL5     AND ( '$endDate' >= RECEIVE_TIME ) "
#tLogSQL5="$tLogSQL5 ORDER BY to_timestamp(RECEIVE_TIME, 'YYYY/MM/DD HH24:MI:SS') ASC "
tLogSQL5="$tLogSQL5 ORDER BY RECEIVE_TIME ASC "
tLogSQL5="$tLogSQL5 ; "

tLogSQL6=""
tLogSQL6="$tLogSQL6 SELECT "
tLogSQL6="$tLogSQL6     RECEIVE_TIME      AS reciveTime, "
tLogSQL6="$tLogSQL6     TYPE              AS type, "
tLogSQL6="$tLogSQL6     SOURCE_IP         AS sourceIp, "
tLogSQL6="$tLogSQL6     DESTINATION_IP    AS destIp, "
tLogSQL6="$tLogSQL6     RULE_NAME         AS ruleName, "
tLogSQL6="$tLogSQL6     APPLICATION       AS application, "
tLogSQL6="$tLogSQL6     SOURCE_ZONE       AS sourceZone, "
tLogSQL6="$tLogSQL6     DESTINATION_ZONE  AS destinationZone, "
tLogSQL6="$tLogSQL6     SOURCE_PORT       AS sourcePort, "
tLogSQL6="$tLogSQL6     DESTINATION_PORT  AS destinationPort, "
tLogSQL6="$tLogSQL6     PROTOCOL          AS protocol, "
tLogSQL6="$tLogSQL6     ACTION            AS action, "
tLogSQL6="$tLogSQL6     BYTES             AS bytes "
tLogSQL6="$tLogSQL6 FROM "
tLogSQL6="$tLogSQL6     T_LogTraffic "
tLogSQL6="$tLogSQL6 WHERE "
tLogSQL6="$tLogSQL6         VIRTUAL_SYSTEM = '$vsysid' "
#tLogSQL6="$tLogSQL6 ORDER BY to_timestamp(RECEIVE_TIME, 'YYYY/MM/DD HH24:MI:SS') ASC "
tLogSQL6="$tLogSQL6 ORDER BY RECEIVE_TIME ASC "
tLogSQL6="$tLogSQL6 ; "

# $sqlPatern から実行するSQLを決定
if test $sqlPatern = 'TrafficLogBLogic-3' ; then
  sql=$tLogSQL3
fi
if test $sqlPatern = 'TrafficLogBLogic-4' ; then
  sql=$tLogSQL4
fi
if test $sqlPatern = 'TrafficLogBLogic-5' ; then
  sql=$tLogSQL5
fi
if test $sqlPatern = 'TrafficLogBLogic-6' ; then
  sql=$tLogSQL6
fi

# SQL実行
# ヘッダなし
PGPASSWORD="$db_pswd" nice -n 1 psql -d "$db_name" -U "$db_user" --set FETCH_COUNT=5000 -A -F , -t -o "$csv_file_tmp" -c "$sql"

#SQL実行結果のチェック（テンポラリファイルのサイズで判断）
tmp_size=`wc -c $csv_file_tmp | awk '{print $1}'`
if test $tmp_size -eq 0 ; then
  exit 1
fi
##############################
# 20110620 PROSITE add start(65,000行越え対応)
##############################
tmp_fname=${csv_file_tmp##*/}
rst_path=${csv_file_tmp%/*}
rst_fname=${csv_file##*/}
rst_fname_without_extension=${rst_fname%.*}

cd $rst_path
split -l 65000 $tmp_fname
CNT=1
for FILE in x??; do
	cat $csv_head $FILE > $FILE.tmp
	mv $FILE.tmp $rst_fname_without_extension`expr 00$CNT`.csv
	CNT=`expr $CNT + 1`
done
##############################
# 20110620 PROSITE add end(65,000行越え対応)
##############################
# ZIP圧縮
zip -l -j "$zip_file" *.csv

if test $? -ne 0 ; then
  exit 2
fi

exit 0
