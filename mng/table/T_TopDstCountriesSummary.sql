--DROP TABLE T_TopDstCountriesSummary;

CREATE TABLE T_TopDstCountriesSummary (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    TARGET_DATE timestamp without time zone NOT NULL,
    DSTLOC varchar(128),
    BYTES varchar(32),
    SESSIONS varchar(32)
);

ALTER TABLE ONLY T_TopDstCountriesSummary
    ADD CONSTRAINT T_TopDstCountriesSummary_pkey1 PRIMARY KEY (SEQ_NO);

CREATE INDEX i_idx1_T_TopDstCountriesSummary ON T_TopDstCountriesSummary USING btree (VSYS_ID, TARGET_DATE);
