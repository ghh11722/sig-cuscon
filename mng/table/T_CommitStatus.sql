SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_CommitStatus;

CREATE TABLE T_CommitStatus (
    VSYS_ID varchar(20) NOT NULL,
    PROCESSING_KIND numeric(1,0) NOT NULL,
    STATUS numeric(1,0) NOT NULL,
    MESSAGE text,
    START_TIME timestamp without time zone NOT NULL,
    END_TIME timestamp without time zone
);

ALTER TABLE cuscondb.T_CommitStatus OWNER TO cuscondb;

ALTER TABLE ONLY T_CommitStatus
    ADD CONSTRAINT T_CommitStatus_pkey1 PRIMARY KEY (VSYS_ID);
