SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE M_Technology;

CREATE TABLE M_Technology (
    SEQ_NO serial NOT NULL,
    TECHNOLOGY varchar(64) NOT NULL
);

ALTER TABLE cuscondb.M_Technology OWNER TO cuscondb;

ALTER TABLE ONLY M_Technology
    ADD CONSTRAINT M_Technology_pkey1 PRIMARY KEY (SEQ_NO);
