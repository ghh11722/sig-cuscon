SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_WeeklySchedulesLink;

CREATE TABLE T_WeeklySchedulesLink (
    SCHEDULES_SEQ_NO integer NOT NULL references T_Schedules(SEQ_NO),
    DAY_OF_WEEK numeric(1,0) NOT NULL,
    START_TIME time NOT NULL,
    END_TIME time NOT NULL
);

ALTER TABLE cuscondb.T_WeeklySchedulesLink OWNER TO cuscondb;

ALTER TABLE ONLY T_WeeklySchedulesLink
    ADD CONSTRAINT T_WeeklySchedulesLink_pkey1 PRIMARY KEY (SCHEDULES_SEQ_NO, DAY_OF_WEEK, START_TIME);
