SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE M_Zones CASCADE;
--DROP INDEX i_uniq_m_zones;

CREATE TABLE M_Zones (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    ZONE varchar(15) NOT NULL
);

ALTER TABLE cuscondb.M_Zones OWNER TO cuscondb;

ALTER TABLE ONLY M_Zones
    ADD CONSTRAINT M_Zones_pkey1 PRIMARY KEY (SEQ_NO);

CREATE UNIQUE INDEX i_uniq_M_Zones ON M_Zones USING btree (VSYS_ID, ZONE);
