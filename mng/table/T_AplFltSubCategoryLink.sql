SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_AplFltSubCategoryLink;

CREATE TABLE T_AplFltSubCategoryLink (
    APL_FLT_SEQ_NO integer NOT NULL references T_ApplicationFilters(SEQ_NO),
    SUB_CATEGORY_SEQ_NO integer NOT NULL references M_SubCategory(SEQ_NO)
);

ALTER TABLE cuscondb.T_AplFltSubCategoryLink OWNER TO cuscondb;

ALTER TABLE ONLY T_AplFltSubCategoryLink
    ADD CONSTRAINT T_AplFltSubCategoryLink_pkey1 PRIMARY KEY (APL_FLT_SEQ_NO, SUB_CATEGORY_SEQ_NO);
