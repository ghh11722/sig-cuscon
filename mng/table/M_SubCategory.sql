SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE M_SubCategory;

CREATE TABLE M_SubCategory (
    SEQ_NO serial NOT NULL,
    SUB_CATEGORY varchar(64) NOT NULL
);

ALTER TABLE cuscondb.M_SubCategory OWNER TO cuscondb;

ALTER TABLE ONLY M_SubCategory
    ADD CONSTRAINT M_SubCategory_pkey1 PRIMARY KEY (SEQ_NO);
