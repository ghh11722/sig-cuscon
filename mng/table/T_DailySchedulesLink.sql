SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_DailySchedulesLink;

CREATE TABLE T_DailySchedulesLink (
    SCHEDULES_SEQ_NO integer NOT NULL references T_Schedules(SEQ_NO),
    START_TIME time NOT NULL,
    END_TIME time NOT NULL
);

ALTER TABLE cuscondb.T_DailySchedulesLink OWNER TO cuscondb;

ALTER TABLE ONLY T_DailySchedulesLink
    ADD CONSTRAINT T_DailySchedulesLink_pkey1 PRIMARY KEY (SCHEDULES_SEQ_NO, START_TIME);
