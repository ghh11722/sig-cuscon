SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_AplFltCategoryLink;

CREATE TABLE T_AplFltCategoryLink (
    APL_FLT_SEQ_NO integer NOT NULL references T_ApplicationFilters(SEQ_NO),
    CATEGORY_SEQ_NO integer NOT NULL references M_Category(SEQ_NO)
);

ALTER TABLE cuscondb.T_AplFltCategoryLink OWNER TO cuscondb;

ALTER TABLE ONLY T_AplFltCategoryLink
    ADD CONSTRAINT T_AplFltCategoryLink_pkey1 PRIMARY KEY (APL_FLT_SEQ_NO, CATEGORY_SEQ_NO);
