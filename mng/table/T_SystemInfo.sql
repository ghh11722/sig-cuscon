SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_SystemInfo;

CREATE TABLE T_SystemInfo (
    MODE numeric (1,0) NOT NULL,
    INFOMATION_MSG	text NOT NULL
);

ALTER TABLE cuscondb.T_SystemInfo OWNER TO cuscondb;
