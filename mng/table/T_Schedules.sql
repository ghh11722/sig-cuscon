SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_Schedules;
--DROP INDEX i_uniq_T_Schedules;

CREATE TABLE T_Schedules (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    NAME varchar(31) NOT NULL,
    GENERATION_NO numeric(1,0) NOT NULL DEFAULT 0,
    RECURRENCE numeric(1,0) NOT NULL,
    MOD_FLG numeric(1,0) NOT NULL DEFAULT 0
);

ALTER TABLE cuscondb.T_Schedules OWNER TO cuscondb;

ALTER TABLE ONLY T_Schedules
    ADD CONSTRAINT T_Schedules_pkey1 PRIMARY KEY (SEQ_NO);

CREATE UNIQUE INDEX i_uniq_T_Schedules ON T_Schedules USING btree (VSYS_ID, NAME, GENERATION_NO);
