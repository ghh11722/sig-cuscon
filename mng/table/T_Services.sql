SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_Services;
--DROP INDEX i_uniq_T_Services;

CREATE TABLE T_Services (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    NAME varchar(31) NOT NULL,
    GENERATION_NO numeric(1,0) NOT NULL DEFAULT 0,
    PROTOCOL char(3) NOT NULL,
    PORT varchar(64) NOT NULL,
    MOD_FLG numeric(1,0) NOT NULL DEFAULT 0
);

ALTER TABLE cuscondb.T_Services OWNER TO cuscondb;

ALTER TABLE ONLY T_Services
    ADD CONSTRAINT T_Services_pkey1 PRIMARY KEY (SEQ_NO);

CREATE UNIQUE INDEX i_uniq_T_Services ON T_Services USING btree (VSYS_ID, NAME, GENERATION_NO);
