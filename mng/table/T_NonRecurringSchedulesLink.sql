SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_NonRecurringSchedulesLink;

CREATE TABLE T_NonRecurringSchedulesLink (
    SCHEDULES_SEQ_NO integer NOT NULL references T_Schedules(SEQ_NO),
    START_DATE date NOT NULL,
    START_TIME time NOT NULL,
    END_DATE date NOT NULL,
    END_TIME time NOT NULL
);

ALTER TABLE cuscondb.T_NonRecurringSchedulesLink OWNER TO cuscondb;

ALTER TABLE ONLY T_NonRecurringSchedulesLink
    ADD CONSTRAINT T_NonRecurringSchedulesLink_pkey1 PRIMARY KEY (SCHEDULES_SEQ_NO, STRAT_DATE, START_TIME);
