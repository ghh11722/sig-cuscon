SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_AplFltTechnologyLink;

CREATE TABLE T_AplFltTechnologyLink (
    APL_FLT_SEQ_NO integer NOT NULL references T_ApplicationFilters(SEQ_NO),
    TECHNOLOGY_SEQ_NO integer NOT NULL references M_Technology(SEQ_NO)
);

ALTER TABLE cuscondb.T_AplFltTechnologyLink OWNER TO cuscondb;

ALTER TABLE ONLY T_AplFltTechnologyLink
    ADD CONSTRAINT T_AplFltTechnologyLink_pkey1 PRIMARY KEY (APL_FLT_SEQ_NO, TECHNOLOGY_SEQ_NO);
