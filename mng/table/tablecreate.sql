SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE M_SystemConstant;

CREATE TABLE M_SystemConstant (
PA_CLI_ADDRESS varchar(48) NOT NULL,
PA_XMLAPI_URL varchar(256) NOT NULL,
PA_ADMIN_LOGIN_ID varchar(20) NOT NULL,
PA_ADMIN_LOGIN_PWD text NOT NULL,
FW_RULE_LIMIT integer NOT NULL,
OBJ_ADDR_LIMIT integer NOT NULL,
OBJ_ADDR_GRP_LIMIT integer NOT NULL,
OBJ_ADDR_GRP_MEMBER_LIMIT integer NOT NULL,
OBJ_SERVICE_LIMIT integer NOT NULL,
OBJ_SERVICE_GRP_LIMIT integer NOT NULL,
OBJ_SERVICE_GRP_MEMBER_LIMIT integer NOT NULL,
OBJ_APP_GRP_LIMIT integer NOT NULL,
OBJ_APP_GRP_MEMBER_LIMIT integer NOT NULL,
OBJ_APP_FILTER_LIMIT integer NOT NULL,
OBJ_SCHE_LIMIT integer NOT NULL,
LOGIN_PWD_EXPIRY numeric(3,0) NOT NULL,
LOGIN_FAIL_RESET_TIME numeric(4,0) NOT NULL,
LOCKOUT_TIME numeric(4,0) NOT NULL,
LOCKOUT_CNT numeric(2,0) NOT NULL,
LOGSVR_FTP_ADDR varchar(128) NOT NULL,
LOGSVR_FTP_LOGIN_ID varchar(128) NOT NULL,
LOGSVR_FTP_LOGIN_PWD varchar(128) NOT NULL,
LOGSVR_FTP_LOGIN_PATH varchar(256) NOT NULL,
WORK_DIR varchar(256) NOT NULL,
LOG_EXPIRE_DATE numeric(3,0) NOT NULL,
COMMIT_EXCL_WAIT_TIME numeric(4,0) NOT NULL,
COMMIT_EXCL_RETRY_CNT integer NOT NULL,
PA_CMD_TIMEOUT numeric(4,0) NOT NULL
);

ALTER TABLE cuscondb.M_SystemConstant OWNER TO cuscondb;
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE M_Category;

CREATE TABLE M_Category (
    SEQ_NO serial NOT NULL,
    CATEGORY varchar(64) NOT NULL
);

ALTER TABLE cuscondb.M_Category OWNER TO cuscondb;

ALTER TABLE ONLY M_Category
    ADD CONSTRAINT M_Category_pkey1 PRIMARY KEY (SEQ_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE M_Messages;

CREATE TABLE M_Messages (
    KEY	varchar(8) NOT NULL,
    MESSAGE	text NOT NULL
);

ALTER TABLE cuscondb.M_Messages OWNER TO cuscondb;

ALTER TABLE ONLY M_Messages
    ADD CONSTRAINT M_Messages_pkey1 PRIMARY KEY (KEY);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE M_Services;
--DROP INDEX i_uniq_M_Services;

CREATE TABLE M_Services (
    SEQ_NO serial NOT NULL,
    NAME varchar(31) NOT NULL,
    PROTOCOL char(3) NOT NULL,
    PORT varchar(64) NOT NULL
);

ALTER TABLE cuscondb.M_Services OWNER TO cuscondb;

ALTER TABLE ONLY M_Services
    ADD CONSTRAINT M_Services_pkey1 PRIMARY KEY (SEQ_NO);

CREATE UNIQUE INDEX i_uniq_M_Services ON M_Services USING btree (NAME);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE M_SubCategory;

CREATE TABLE M_SubCategory (
    SEQ_NO serial NOT NULL,
    SUB_CATEGORY varchar(64) NOT NULL
);

ALTER TABLE cuscondb.M_SubCategory OWNER TO cuscondb;

ALTER TABLE ONLY M_SubCategory
    ADD CONSTRAINT M_SubCategory_pkey1 PRIMARY KEY (SEQ_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE M_Technology;

CREATE TABLE M_Technology (
    SEQ_NO serial NOT NULL,
    TECHNOLOGY varchar(64) NOT NULL
);

ALTER TABLE cuscondb.M_Technology OWNER TO cuscondb;

ALTER TABLE ONLY M_Technology
    ADD CONSTRAINT M_Technology_pkey1 PRIMARY KEY (SEQ_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE M_Applications;
--DROP INDEX i_uniq_M_Applications;

CREATE TABLE M_Applications (
    SEQ_NO serial NOT NULL,
    NAME varchar(31) NOT NULL,
    CATEGORY_SEQ_NO integer references M_Category(SEQ_NO),
    SUB_CATEGORY_SEQ_NO integer references M_SubCategory(SEQ_NO),
    RISK numeric(1,0),
    TECHNOLOGY_SEQ_NO integer references M_Technology(SEQ_NO)
);

ALTER TABLE cuscondb.M_Applications OWNER TO cuscondb;

ALTER TABLE ONLY M_Applications
    ADD CONSTRAINT M_Applications_pkey1 PRIMARY KEY (SEQ_NO);

CREATE UNIQUE INDEX i_uniq_M_Applications ON M_Applications USING btree (NAME);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE M_Zones CASCADE;
--DROP INDEX i_uniq_m_zones;

CREATE TABLE M_Zones (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    ZONE varchar(15) NOT NULL
);

ALTER TABLE cuscondb.M_Zones OWNER TO cuscondb;

ALTER TABLE ONLY M_Zones
    ADD CONSTRAINT M_Zones_pkey1 PRIMARY KEY (SEQ_NO);

CREATE UNIQUE INDEX i_uniq_M_Zones ON M_Zones USING btree (VSYS_ID, ZONE);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_AddressGroups;
--DROP INDEX i_uniq_T_AddressGroups;

CREATE TABLE T_AddressGroups (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    NAME varchar(31) NOT NULL,
    GENERATION_NO numeric(1,0) NOT NULL DEFAULT 0,
    MOD_FLG numeric(1,0) NOT NULL DEFAULT 0
);

ALTER TABLE cuscondb.T_AddressGroups OWNER TO cuscondb;

ALTER TABLE ONLY T_AddressGroups
    ADD CONSTRAINT T_AddressGroups_pkey1 PRIMARY KEY (SEQ_NO);

CREATE UNIQUE INDEX i_uniq_T_AddressGroups ON T_AddressGroups USING btree (VSYS_ID, NAME, GENERATION_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_AddressGroupsLink;

CREATE TABLE T_AddressGroupsLink (
    ADR_GRP_SEQ_NO integer NOT NULL references T_AddressGroups(SEQ_NO),
    OBJECT_TYPE numeric(1,0) NOT NULL,
    LINK_SEQ_NO integer NOT NULL
);

ALTER TABLE cuscondb.T_AddressGroupsLink OWNER TO cuscondb;

ALTER TABLE ONLY T_AddressGroupsLink
    ADD CONSTRAINT T_AddressGroupsLink_pkey1 PRIMARY KEY (ADR_GRP_SEQ_NO, OBJECT_TYPE, LINK_SEQ_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_Addresses;
--DROP INDEX i_uniq_T_Addresses;

CREATE TABLE T_Addresses (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    NAME varchar(31) NOT NULL,
    GENERATION_NO numeric(1,0) NOT NULL DEFAULT 0,
    TYPE varchar(10) NOT NULL,
    ADDRESS varchar(87) NOT NULL,
    MOD_FLG numeric(1,0) NOT NULL DEFAULT 0
);

ALTER TABLE cuscondb.T_Addresses OWNER TO cuscondb;

ALTER TABLE ONLY T_Addresses
    ADD CONSTRAINT T_Addresses_pkey1 PRIMARY KEY (SEQ_NO);

CREATE UNIQUE INDEX i_uniq_T_Addresses ON T_Addresses USING btree (VSYS_ID, NAME, GENERATION_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_ApplicationFilters;
--DROP INDEX i_uniq_T_ApplicationFilters;

CREATE TABLE T_ApplicationFilters (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    NAME varchar(31) NOT NULL,
    GENERATION_NO numeric(1,0) NOT NULL DEFAULT 0,
    MOD_FLG numeric(1,0) NOT NULL DEFAULT 0
);

ALTER TABLE cuscondb.T_ApplicationFilters OWNER TO cuscondb;

ALTER TABLE ONLY T_ApplicationFilters
    ADD CONSTRAINT T_ApplicationFilters_pkey1 PRIMARY KEY (SEQ_NO);

CREATE UNIQUE INDEX i_uniq_T_ApplicationFilters ON T_ApplicationFilters USING btree (VSYS_ID, NAME, GENERATION_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_AplFltCategoryLink;

CREATE TABLE T_AplFltCategoryLink (
    APL_FLT_SEQ_NO integer NOT NULL references T_ApplicationFilters(SEQ_NO),
    CATEGORY_SEQ_NO integer NOT NULL references M_Category(SEQ_NO)
);

ALTER TABLE cuscondb.T_AplFltCategoryLink OWNER TO cuscondb;

ALTER TABLE ONLY T_AplFltCategoryLink
    ADD CONSTRAINT T_AplFltCategoryLink_pkey1 PRIMARY KEY (APL_FLT_SEQ_NO, CATEGORY_SEQ_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_AplFltRiskLink;

CREATE TABLE T_AplFltRiskLink (
    APL_FLT_SEQ_NO integer NOT NULL references T_ApplicationFilters(SEQ_NO),
    RISK numeric(1,0) NOT NULL
);

ALTER TABLE cuscondb.T_AplFltRiskLink OWNER TO cuscondb;

ALTER TABLE ONLY T_AplFltRiskLink
    ADD CONSTRAINT T_AplFltRiskLink_pkey1 PRIMARY KEY (APL_FLT_SEQ_NO, RISK);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_AplFltSubCategoryLink;

CREATE TABLE T_AplFltSubCategoryLink (
    APL_FLT_SEQ_NO integer NOT NULL references T_ApplicationFilters(SEQ_NO),
    SUB_CATEGORY_SEQ_NO integer NOT NULL references M_SubCategory(SEQ_NO)
);

ALTER TABLE cuscondb.T_AplFltSubCategoryLink OWNER TO cuscondb;

ALTER TABLE ONLY T_AplFltSubCategoryLink
    ADD CONSTRAINT T_AplFltSubCategoryLink_pkey1 PRIMARY KEY (APL_FLT_SEQ_NO, SUB_CATEGORY_SEQ_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_AplFltTechnologyLink;

CREATE TABLE T_AplFltTechnologyLink (
    APL_FLT_SEQ_NO integer NOT NULL references T_ApplicationFilters(SEQ_NO),
    TECHNOLOGY_SEQ_NO integer NOT NULL references M_Technology(SEQ_NO)
);

ALTER TABLE cuscondb.T_AplFltTechnologyLink OWNER TO cuscondb;

ALTER TABLE ONLY T_AplFltTechnologyLink
    ADD CONSTRAINT T_AplFltTechnologyLink_pkey1 PRIMARY KEY (APL_FLT_SEQ_NO, TECHNOLOGY_SEQ_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_ApplicationGroups;
--DROP INDEX i_uniq_T_ApplicationGroups;

CREATE TABLE T_ApplicationGroups (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    NAME varchar(31) NOT NULL,
    GENERATION_NO numeric(1,0) NOT NULL DEFAULT 0,
    MOD_FLG numeric(1,0) NOT NULL DEFAULT 0
);

ALTER TABLE cuscondb.T_ApplicationGroups OWNER TO cuscondb;

ALTER TABLE ONLY T_ApplicationGroups
    ADD CONSTRAINT T_ApplicationGroups_pkey1 PRIMARY KEY (SEQ_NO);

CREATE UNIQUE INDEX i_uniq_T_ApplicationGroups ON T_ApplicationGroups USING btree (VSYS_ID, NAME, GENERATION_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_ApplicationGroupsLink;

CREATE TABLE T_ApplicationGroupsLink (
    APL_GRP_SEQ_NO integer NOT NULL references T_ApplicationGroups(SEQ_NO),
    OBJECT_TYPE numeric(1,0) NOT NULL,
    LINK_SEQ_NO integer NOT NULL
);

ALTER TABLE cuscondb.T_ApplicationGroupsLink OWNER TO cuscondb;

ALTER TABLE ONLY T_ApplicationGroupsLink
    ADD CONSTRAINT T_ApplicationGroupsLink_pkey1 PRIMARY KEY (APL_GRP_SEQ_NO, OBJECT_TYPE, LINK_SEQ_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_Schedules;
--DROP INDEX i_uniq_T_Schedules;

CREATE TABLE T_Schedules (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    NAME varchar(31) NOT NULL,
    GENERATION_NO numeric(1,0) NOT NULL DEFAULT 0,
    RECURRENCE numeric(1,0) NOT NULL,
    MOD_FLG numeric(1,0) NOT NULL DEFAULT 0
);

ALTER TABLE cuscondb.T_Schedules OWNER TO cuscondb;

ALTER TABLE ONLY T_Schedules
    ADD CONSTRAINT T_Schedules_pkey1 PRIMARY KEY (SEQ_NO);

CREATE UNIQUE INDEX i_uniq_T_Schedules ON T_Schedules USING btree (VSYS_ID, NAME, GENERATION_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_SecurityRules;
--DROP INDEX i_uniq_T_SecurityRules;

CREATE TABLE T_SecurityRules (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    NAME varchar(31) NOT NULL,
    GENERATION_NO numeric(1,0) NOT NULL DEFAULT 0,
    LINE_NO smallint NOT NULL,
    DESCRIPTION varchar(255),
    SERVICE_DEFAULT_FLG numeric(1,0) NOT NULL DEFAULT 0,
    ACTION numeric(1,0) NOT NULL DEFAULT 0,
    IDS_IPS numeric(1,0) NOT NULL DEFAULT 0,
    SCHEDULES_SEQ_NO integer references T_Schedules(SEQ_NO),
    MOD_FLG numeric(1,0) NOT NULL DEFAULT 0,
    UPDATE_TIME timestamp without time zone
);

ALTER TABLE cuscondb.T_SecurityRules OWNER TO cuscondb;

ALTER TABLE ONLY T_SecurityRules
    ADD CONSTRAINT T_SecurityRules_pkey1 PRIMARY KEY (SEQ_NO);

CREATE UNIQUE INDEX i_uniq_T_SecurityRules ON T_SecurityRules USING btree (VSYS_ID, NAME, GENERATION_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_ApplicationLink;

CREATE TABLE T_ApplicationLink (
    SEC_RLS_SEQ_NO integer NOT NULL references T_SecurityRules(SEQ_NO),
    OBJECT_TYPE numeric(1,0) NOT NULL,
    LINK_SEQ_NO integer NOT NULL
);

ALTER TABLE cuscondb.T_ApplicationLink OWNER TO cuscondb;

ALTER TABLE ONLY T_ApplicationLink
    ADD CONSTRAINT T_ApplicationLink_pkey1 PRIMARY KEY (SEC_RLS_SEQ_NO, OBJECT_TYPE, LINK_SEQ_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_DailySchedulesLink;

CREATE TABLE T_DailySchedulesLink (
    SCHEDULES_SEQ_NO integer NOT NULL references T_Schedules(SEQ_NO),
    START_TIME time NOT NULL,
    END_TIME time NOT NULL
);

ALTER TABLE cuscondb.T_DailySchedulesLink OWNER TO cuscondb;

ALTER TABLE ONLY T_DailySchedulesLink
    ADD CONSTRAINT T_DailySchedulesLink_pkey1 PRIMARY KEY (SCHEDULES_SEQ_NO, START_TIME);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_DestinationAddressLink;

CREATE TABLE T_DestinationAddressLink (
    SEC_RLS_SEQ_NO integer NOT NULL references T_SecurityRules(SEQ_NO),
    OBJECT_TYPE numeric(1,0) NOT NULL,
    LINK_SEQ_NO integer NOT NULL
);

ALTER TABLE cuscondb.T_DestinationAddressLink OWNER TO cuscondb;

ALTER TABLE ONLY T_DestinationAddressLink
    ADD CONSTRAINT T_DestinationAddressLink_pkey1 PRIMARY KEY (SEC_RLS_SEQ_NO, OBJECT_TYPE, LINK_SEQ_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_DestinationZoneLink;

CREATE TABLE T_DestinationZoneLink (
    SEC_RLS_SEQ_NO integer NOT NULL references T_SecurityRules(SEQ_NO),
    ZONE_LINK_SEQ_NO integer NOT NULL references M_Zones(SEQ_NO)
);

ALTER TABLE cuscondb.T_DestinationZoneLink OWNER TO cuscondb;

ALTER TABLE ONLY T_DestinationZoneLink
    ADD CONSTRAINT T_DestinationZoneLink_pkey1 PRIMARY KEY (SEC_RLS_SEQ_NO, ZONE_LINK_SEQ_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_LocalDestinationAddress;

CREATE TABLE T_LocalDestinationAddress (
    SEQ_NO serial NOT NULL,
    ADDRESS varchar(43) NOT NULL
);

ALTER TABLE cuscondb.T_LocalDestinationAddress OWNER TO cuscondb;

ALTER TABLE ONLY T_LocalDestinationAddress
    ADD CONSTRAINT T_LocalDestinationAddress_pkey1 PRIMARY KEY (SEQ_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_LocalSourceAddress;

CREATE TABLE T_LocalSourceAddress (
    SEQ_NO serial NOT NULL,
    ADDRESS varchar(43) NOT NULL
);

ALTER TABLE cuscondb.T_LocalSourceAddress OWNER TO cuscondb;

ALTER TABLE ONLY T_LocalSourceAddress
    ADD CONSTRAINT T_LocalSourceAddress_pkey1 PRIMARY KEY (SEQ_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_NonRecurringSchedulesLink;

CREATE TABLE T_NonRecurringSchedulesLink (
    SCHEDULES_SEQ_NO integer NOT NULL references T_Schedules(SEQ_NO),
    START_DATE date NOT NULL,
    START_TIME time NOT NULL,
    END_DATE date NOT NULL,
    END_TIME time NOT NULL
);

ALTER TABLE cuscondb.T_NonRecurringSchedulesLink OWNER TO cuscondb;

ALTER TABLE ONLY T_NonRecurringSchedulesLink
    ADD CONSTRAINT T_NonRecurringSchedulesLink_pkey1 PRIMARY KEY (SCHEDULES_SEQ_NO, START_DATE, START_TIME);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_ServiceLink;

CREATE TABLE T_ServiceLink (
    SEC_RLS_SEQ_NO integer NOT NULL references T_SecurityRules(SEQ_NO),
    OBJECT_TYPE numeric(1,0) NOT NULL,
    LINK_SEQ_NO integer NOT NULL
);

ALTER TABLE cuscondb.T_ServiceLink OWNER TO cuscondb;

ALTER TABLE ONLY T_ServiceLink
    ADD CONSTRAINT T_ServiceLink_pkey1 PRIMARY KEY (SEC_RLS_SEQ_NO, OBJECT_TYPE, LINK_SEQ_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_Services;
--DROP INDEX i_uniq_T_Services;

CREATE TABLE T_Services (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    NAME varchar(31) NOT NULL,
    GENERATION_NO numeric(1,0) NOT NULL DEFAULT 0,
    PROTOCOL char(3) NOT NULL,
    PORT varchar(64) NOT NULL,
    MOD_FLG numeric(1,0) NOT NULL DEFAULT 0
);

ALTER TABLE cuscondb.T_Services OWNER TO cuscondb;

ALTER TABLE ONLY T_Services
    ADD CONSTRAINT T_Services_pkey1 PRIMARY KEY (SEQ_NO);

CREATE UNIQUE INDEX i_uniq_T_Services ON T_Services USING btree (VSYS_ID, NAME, GENERATION_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_ServicesGroups;
--DROP INDEX i_uniq_T_ServicesGroups;

CREATE TABLE T_ServicesGroups (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    NAME varchar(31) NOT NULL,
    GENERATION_NO numeric(1,0) NOT NULL DEFAULT 0,
    MOD_FLG numeric(1,0) NOT NULL DEFAULT 0
);

ALTER TABLE cuscondb.T_ServicesGroups OWNER TO cuscondb;

ALTER TABLE ONLY T_ServicesGroups
    ADD CONSTRAINT T_ServicesGroups_pkey1 PRIMARY KEY (SEQ_NO);

CREATE UNIQUE INDEX i_uniq_T_ServicesGroups ON T_ServicesGroups USING btree (VSYS_ID, NAME, GENERATION_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_ServicesGroupsLink;

CREATE TABLE T_ServicesGroupsLink (
    SRV_GRP_SEQ_NO integer NOT NULL references T_ServicesGroups(SEQ_NO),
    OBJECT_TYPE numeric(1,0) NOT NULL,
    LINK_SEQ_NO integer NOT NULL
);

ALTER TABLE cuscondb.T_ServicesGroupsLink OWNER TO cuscondb;

ALTER TABLE ONLY T_ServicesGroupsLink
    ADD CONSTRAINT T_ServicesGroupsLink_pkey1 PRIMARY KEY (SRV_GRP_SEQ_NO, OBJECT_TYPE, LINK_SEQ_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_SourceAddressLink;

CREATE TABLE T_SourceAddressLink (
    SEC_RLS_SEQ_NO integer NOT NULL references T_SecurityRules(SEQ_NO),
    OBJECT_TYPE numeric(1,0) NOT NULL,
    LINK_SEQ_NO integer NOT NULL
);

ALTER TABLE cuscondb.T_SourceAddressLink OWNER TO cuscondb;

ALTER TABLE ONLY T_SourceAddressLink
    ADD CONSTRAINT T_SourceAddressLink_pkey1 PRIMARY KEY (SEC_RLS_SEQ_NO, OBJECT_TYPE, LINK_SEQ_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_SourceZoneLink;

CREATE TABLE T_SourceZoneLink (
    SEC_RLS_SEQ_NO integer NOT NULL references T_SecurityRules(SEQ_NO),
    ZONE_LINK_SEQ_NO integer NOT NULL references M_Zones(SEQ_NO)
);

ALTER TABLE cuscondb.T_SourceZoneLink OWNER TO cuscondb;

ALTER TABLE ONLY T_SourceZoneLink
    ADD CONSTRAINT T_SourceZoneLink_pkey1 PRIMARY KEY (SEC_RLS_SEQ_NO, ZONE_LINK_SEQ_NO);
SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_TopAppSummary;

CREATE TABLE T_TopAppSummary (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    TARGET_DATE timestamp without time zone NOT NULL,
    APP varchar(64),
    RISK_OF_APP varchar(1),
    BYTES varchar(32),
    SESSIONS varchar(32)
);

ALTER TABLE cuscondb.T_TopAppSummary OWNER TO cuscondb;

ALTER TABLE ONLY T_TopAppSummary
    ADD CONSTRAINT T_TopAppSummary_pkey1 PRIMARY KEY (SEQ_NO);

SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_TopAttacksSummary;

CREATE TABLE T_TopAttacksSummary (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    TARGET_DATE timestamp without time zone NOT NULL,
    TID varchar(7),
    SEVERITY_OF_THREATID varchar(1),
    THREATID varchar(128),
    THREAT_SUB_TYPE varchar(32),
    COUNT varchar(32)
);

ALTER TABLE cuscondb.T_TopAttacksSummary OWNER TO cuscondb;

ALTER TABLE ONLY T_TopAttacksSummary
    ADD CONSTRAINT T_TopAttacksSummary_pkey1 PRIMARY KEY (SEQ_NO);

SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_TopDstSummary;

CREATE TABLE T_TopDstSummary (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    TARGET_DATE timestamp without time zone NOT NULL,
    DST varchar(48),
    RESOLVED_DST varchar(128),
    DST_USER varchar(64),
    BYTES varchar(32),
    SESSIONS varchar(32)
);

ALTER TABLE cuscondb.T_TopDstSummary OWNER TO cuscondb;

ALTER TABLE ONLY T_TopDstSummary
    ADD CONSTRAINT T_TopDstSummary_pkey1 PRIMARY KEY (SEQ_NO);

SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_TopDstCountriesSummary;

CREATE TABLE T_TopDstCountriesSummary (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    TARGET_DATE timestamp without time zone NOT NULL,
    DSTLOC varchar(128),
    BYTES varchar(32),
    SESSIONS varchar(32)
);

ALTER TABLE cuscondb.T_TopDstCountriesSummary OWNER TO cuscondb;

ALTER TABLE ONLY T_TopDstCountriesSummary
    ADD CONSTRAINT T_TopDstCountriesSummary_pkey1 PRIMARY KEY (SEQ_NO);

SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_TopSrcSummary;

CREATE TABLE T_TopSrcSummary (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    TARGET_DATE timestamp without time zone NOT NULL,
    SRC varchar(48),
    RESOLVED_SRC varchar(128),
    SRC_USER varchar(64),
    BYTES varchar(32),
    SESSIONS varchar(32)
);

ALTER TABLE cuscondb.T_TopSrcSummary OWNER TO cuscondb;

ALTER TABLE ONLY T_TopSrcSummary
    ADD CONSTRAINT T_TopSrcSummary_pkey1 PRIMARY KEY (SEQ_NO);

SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_WeeklySchedulesLink;

CREATE TABLE T_WeeklySchedulesLink (
    SCHEDULES_SEQ_NO integer NOT NULL references T_Schedules(SEQ_NO),
    DAY_OF_WEEK numeric(1,0) NOT NULL,
    START_TIME time NOT NULL,
    END_TIME time NOT NULL
);

ALTER TABLE cuscondb.T_WeeklySchedulesLink OWNER TO cuscondb;

ALTER TABLE ONLY T_WeeklySchedulesLink
    ADD CONSTRAINT T_WeeklySchedulesLink_pkey1 PRIMARY KEY (SCHEDULES_SEQ_NO, DAY_OF_WEEK, START_TIME);

SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_ObjectOldName;

CREATE TABLE T_ObjectOldName (
    VSYS_ID varchar(20) NOT NULL,
    OBJECT_TYPE numeric(1,0) NOT NULL,
    NAME varchar(31) NOT NULL
);

ALTER TABLE cuscondb.T_ObjectOldName OWNER TO cuscondb;

ALTER TABLE ONLY T_ObjectOldName
    ADD CONSTRAINT T_ObjectOldName_pkey1 PRIMARY KEY (VSYS_ID, OBJECT_TYPE, NAME);
