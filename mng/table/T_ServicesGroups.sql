SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_ServicesGroups;
--DROP INDEX i_uniq_T_ServicesGroups;

CREATE TABLE T_ServicesGroups (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    NAME varchar(31) NOT NULL,
    GENERATION_NO numeric(1,0) NOT NULL DEFAULT 0,
    MOD_FLG numeric(1,0) NOT NULL DEFAULT 0
);

ALTER TABLE cuscondb.T_ServicesGroups OWNER TO cuscondb;

ALTER TABLE ONLY T_ServicesGroups
    ADD CONSTRAINT T_ServicesGroups_pkey1 PRIMARY KEY (SEQ_NO);

CREATE UNIQUE INDEX i_uniq_T_ServicesGroups ON T_ServicesGroups USING btree (VSYS_ID, NAME, GENERATION_NO);
