SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_TopAppSummary;

CREATE TABLE T_TopAppSummary (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    TARGET_DATE timestamp without time zone NOT NULL,
    APP varchar(64),
    RISK_OF_APP varchar(1),
    BYTES varchar(32),
    SESSIONS varchar(32)
);

ALTER TABLE cuscondb.T_TopAppSummary OWNER TO cuscondb;

ALTER TABLE ONLY T_TopAppSummary
    ADD CONSTRAINT T_TopAppSummary_pkey1 PRIMARY KEY (SEQ_NO);

CREATE INDEX i_idx1_T_TopAppSummary ON T_TopAppSummary USING btree (VSYS_ID, TARGET_DATE);