SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_Addresses;
--DROP INDEX i_uniq_T_Addresses;

CREATE TABLE T_Addresses (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    NAME varchar(31) NOT NULL,
    GENERATION_NO numeric(1,0) NOT NULL DEFAULT 0,
    TYPE varchar(10) NOT NULL,
    ADDRESS varchar(87) NOT NULL,
    MOD_FLG numeric(1,0) NOT NULL DEFAULT 0
);

ALTER TABLE cuscondb.T_Addresses OWNER TO cuscondb;

ALTER TABLE ONLY T_Addresses
    ADD CONSTRAINT T_Addresses_pkey1 PRIMARY KEY (SEQ_NO);

CREATE UNIQUE INDEX i_uniq_T_Addresses ON T_Addresses USING btree (VSYS_ID, NAME, GENERATION_NO);
