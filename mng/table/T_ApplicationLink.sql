SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_ApplicationLink;

CREATE TABLE T_ApplicationLink (
    SEC_RLS_SEQ_NO integer NOT NULL references T_SecurityRules(SEQ_NO),
    OBJECT_TYPE numeric(1,0) NOT NULL,
    LINK_SEQ_NO integer NOT NULL
);

ALTER TABLE cuscondb.T_ApplicationLink OWNER TO cuscondb;

ALTER TABLE ONLY T_ApplicationLink
    ADD CONSTRAINT T_ApplicationLink_pkey1 PRIMARY KEY (SEC_RLS_SEQ_NO, OBJECT_TYPE, LINK_SEQ_NO);
