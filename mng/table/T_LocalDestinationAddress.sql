SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_LocalDestinationAddress;

CREATE TABLE T_LocalDestinationAddress (
    SEQ_NO serial NOT NULL,
    ADDRESS varchar(43) NOT NULL
);

ALTER TABLE cuscondb.T_LocalDestinationAddress OWNER TO cuscondb;

ALTER TABLE ONLY T_LocalDestinationAddress
    ADD CONSTRAINT T_LocalDestinationAddress_pkey1 PRIMARY KEY (SEQ_NO);
