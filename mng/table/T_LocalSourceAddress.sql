SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_LocalSourceAddress;

CREATE TABLE T_LocalSourceAddress (
    SEQ_NO serial NOT NULL,
    ADDRESS varchar(43) NOT NULL
);

ALTER TABLE cuscondb.T_LocalSourceAddress OWNER TO cuscondb;

ALTER TABLE ONLY T_LocalSourceAddress
    ADD CONSTRAINT T_LocalSourceAddress_pkey1 PRIMARY KEY (SEQ_NO);
