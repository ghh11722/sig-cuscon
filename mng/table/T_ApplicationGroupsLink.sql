SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_ApplicationGroupsLink;

CREATE TABLE T_ApplicationGroupsLink (
    APL_GRP_SEQ_NO integer NOT NULL references T_ApplicationGroups(SEQ_NO),
    OBJECT_TYPE numeric(1,0) NOT NULL,
    LINK_SEQ_NO integer NOT NULL
);

ALTER TABLE cuscondb.T_ApplicationGroupsLink OWNER TO cuscondb;

ALTER TABLE ONLY T_ApplicationGroupsLink
    ADD CONSTRAINT T_ApplicationGroupsLink_pkey1 PRIMARY KEY (APL_GRP_SEQ_NO, OBJECT_TYPE, LINK_SEQ_NO);
