SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_TopDstSummary;

CREATE TABLE T_TopDstSummary (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    TARGET_DATE timestamp without time zone NOT NULL,
    DST varchar(48),
    RESOLVED_DST varchar(128),
    DST_USER varchar(64),
    BYTES varchar(32),
    SESSIONS varchar(32)
);

ALTER TABLE cuscondb.T_TopDstSummary OWNER TO cuscondb;

ALTER TABLE ONLY T_TopDstSummary
    ADD CONSTRAINT T_TopDstSummary_pkey1 PRIMARY KEY (SEQ_NO);

CREATE INDEX i_idx1_T_TopDstSummary ON T_TopDstSummary USING btree (VSYS_ID, TARGET_DATE);
