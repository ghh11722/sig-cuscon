SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_ServicesGroupsLink;

CREATE TABLE T_ServicesGroupsLink (
    SRV_GRP_SEQ_NO integer NOT NULL references T_ServicesGroups(SEQ_NO),
    OBJECT_TYPE numeric(1,0) NOT NULL,
    LINK_SEQ_NO integer NOT NULL
);

ALTER TABLE cuscondb.T_ServicesGroupsLink OWNER TO cuscondb;

ALTER TABLE ONLY T_ServicesGroupsLink
    ADD CONSTRAINT T_ServicesGroupsLink_pkey1 PRIMARY KEY (SRV_GRP_SEQ_NO, OBJECT_TYPE, LINK_SEQ_NO);
