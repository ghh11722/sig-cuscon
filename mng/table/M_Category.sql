SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE M_Category;

CREATE TABLE M_Category (
    SEQ_NO serial NOT NULL,
    CATEGORY varchar(64) NOT NULL
);

ALTER TABLE cuscondb.M_Category OWNER TO cuscondb;

ALTER TABLE ONLY M_Category
    ADD CONSTRAINT M_Category_pkey1 PRIMARY KEY (SEQ_NO);
