SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_TopSrcCountriesSummary;

CREATE TABLE T_TopSrcCountriesSummary (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    TARGET_DATE timestamp without time zone NOT NULL,
    SRCLOC varchar(128),
    BYTES varchar(32),
    SESSIONS varchar(32)
);

ALTER TABLE cuscondb.T_TopSrcCountriesSummary OWNER TO cuscondb;

ALTER TABLE ONLY T_TopSrcCountriesSummary
    ADD CONSTRAINT T_TopSrcCountriesSummary_pkey1 PRIMARY KEY (SEQ_NO);

CREATE INDEX i_idx1_T_TopSrcCountriesSummary ON T_TopSrcCountriesSummary USING btree (VSYS_ID, TARGET_DATE);
