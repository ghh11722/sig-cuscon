SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_DestinationZoneLink;

CREATE TABLE T_DestinationZoneLink (
    SEC_RLS_SEQ_NO integer NOT NULL references T_SecurityRules(SEQ_NO),
    ZONE_LINK_SEQ_NO integer NOT NULL references M_Zones(SEQ_NO)
);

ALTER TABLE cuscondb.T_DestinationZoneLink OWNER TO cuscondb;

ALTER TABLE ONLY T_DestinationZoneLink
    ADD CONSTRAINT T_DestinationZoneLink_pkey1 PRIMARY KEY (SEC_RLS_SEQ_NO, ZONE_LINK_SEQ_NO);
