CREATE OR REPLACE FUNCTION T_CommitProcessing_update(IN integer, OUT integer) RETURNS integer AS $$
DECLARE
	input_flg ALIAS FOR $1;
	myrec ALIAS FOR $2;
	dbFlg integer;
BEGIN
	myrec := 0;
	IF input_flg = 0 THEN
		UPDATE T_CommitProcessing SET commitProcessing_flg = input_flg;
	ELSE
		SELECT commitprocessing_flg into dbFlg from T_CommitProcessing;
		IF dbFlg = 1 THEN
	    	myrec = dbFlg;
	    	return;
		END IF;
		UPDATE T_CommitProcessing SET commitProcessing_flg = input_flg;
	END IF;
	return;
END;
$$ LANGUAGE 'plpgsql' VOLATILE;