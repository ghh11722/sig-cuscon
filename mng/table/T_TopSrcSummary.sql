SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_TopSrcSummary;

CREATE TABLE T_TopSrcSummary (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    TARGET_DATE timestamp without time zone NOT NULL,
    SRC varchar(48),
    RESOLVED_SRC varchar(128),
    SRC_USER varchar(64),
    BYTES varchar(32),
    SESSIONS varchar(32)
);

ALTER TABLE cuscondb.T_TopSrcSummary OWNER TO cuscondb;

ALTER TABLE ONLY T_TopSrcSummary
    ADD CONSTRAINT T_TopSrcSummary_pkey1 PRIMARY KEY (SEQ_NO);

CREATE INDEX i_idx1_T_TopSrcSummary ON T_TopSrcSummary USING btree (VSYS_ID, TARGET_DATE);
