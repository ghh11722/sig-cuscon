SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE M_Services;
--DROP INDEX i_uniq_M_Services;

CREATE TABLE M_Services (
    SEQ_NO serial NOT NULL,
    NAME varchar(31) NOT NULL,
    PROTOCOL char(3) NOT NULL,
    PORT varchar(64) NOT NULL
);

ALTER TABLE cuscondb.M_Services OWNER TO cuscondb;

ALTER TABLE ONLY M_Services
    ADD CONSTRAINT M_Services_pkey1 PRIMARY KEY (SEQ_NO);

CREATE UNIQUE INDEX i_uniq_M_Services ON M_Services USING btree (NAME);
