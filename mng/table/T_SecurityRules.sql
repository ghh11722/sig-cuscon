SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_SecurityRules;
--DROP INDEX i_uniq_T_SecurityRules;

CREATE TABLE T_SecurityRules (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    NAME varchar(31) NOT NULL,
    GENERATION_NO numeric(1,0) NOT NULL DEFAULT 0,
    LINE_NO smallint NOT NULL,
    DESCRIPTION varchar(255),
    SERVICE_DEFAULT_FLG numeric(1,0) NOT NULL DEFAULT 0,
    ACTION numeric(1,0) NOT NULL DEFAULT 0,
    IDS_IPS numeric(1,0) NOT NULL DEFAULT 0,
    SCHEDULES_SEQ_NO integer references T_Schedules(SEQ_NO),
    MOD_FLG numeric(1,0) NOT NULL DEFAULT 0,
    UPDATE_TIME timestamp without time zone
);

ALTER TABLE cuscondb.T_SecurityRules OWNER TO cuscondb;

ALTER TABLE ONLY T_SecurityRules
    ADD CONSTRAINT T_SecurityRules_pkey1 PRIMARY KEY (SEQ_NO);

CREATE UNIQUE INDEX i_uniq_T_SecurityRules ON T_SecurityRules USING btree (VSYS_ID, NAME, GENERATION_NO);
