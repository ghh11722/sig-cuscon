SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_AplFltRiskLink;

CREATE TABLE T_AplFltRiskLink (
    APL_FLT_SEQ_NO integer NOT NULL references T_ApplicationFilters(SEQ_NO),
    RISK numeric(1,0) NOT NULL
);

ALTER TABLE cuscondb.T_AplFltRiskLink OWNER TO cuscondb;

ALTER TABLE ONLY T_AplFltRiskLink
    ADD CONSTRAINT T_AplFltRiskLink_pkey1 PRIMARY KEY (APL_FLT_SEQ_NO, RISK);
