SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_SourceZoneLink;

CREATE TABLE T_SourceZoneLink (
    SEC_RLS_SEQ_NO integer NOT NULL references T_SecurityRules(SEQ_NO),
    ZONE_LINK_SEQ_NO integer NOT NULL references M_Zones(SEQ_NO)
);

ALTER TABLE cuscondb.T_SourceZoneLink OWNER TO cuscondb;

ALTER TABLE ONLY T_SourceZoneLink
    ADD CONSTRAINT T_SourceZoneLink_pkey1 PRIMARY KEY (SEC_RLS_SEQ_NO, ZONE_LINK_SEQ_NO);
