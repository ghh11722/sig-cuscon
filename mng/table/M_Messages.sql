SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE M_Messages;

CREATE TABLE M_Messages (
    KEY	varchar(8) NOT NULL,
    MESSAGE	text NOT NULL
);

ALTER TABLE cuscondb.M_Messages OWNER TO cuscondb;

ALTER TABLE ONLY M_Messages
    ADD CONSTRAINT M_Messages_pkey1 PRIMARY KEY (KEY);
