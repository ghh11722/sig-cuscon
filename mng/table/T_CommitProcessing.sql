SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

-- DROP TABLE T_CommitProcessing;

CREATE TABLE T_CommitProcessing
(
  COMMITPROCESSING_FLG numeric(1) NOT NULL DEFAULT 0
)

ALTER TABLE T_CommitProcessing OWNER TO cuscondb;
