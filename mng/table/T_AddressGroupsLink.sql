SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_AddressGroupsLink;

CREATE TABLE T_AddressGroupsLink (
    ADR_GRP_SEQ_NO integer NOT NULL references T_AddressGroups(SEQ_NO),
    OBJECT_TYPE numeric(1,0) NOT NULL,
    LINK_SEQ_NO integer NOT NULL
);

ALTER TABLE cuscondb.T_AddressGroupsLink OWNER TO cuscondb;

ALTER TABLE ONLY T_AddressGroupsLink
    ADD CONSTRAINT T_AddressGroupsLink_pkey1 PRIMARY KEY (ADR_GRP_SEQ_NO, OBJECT_TYPE, LINK_SEQ_NO);
