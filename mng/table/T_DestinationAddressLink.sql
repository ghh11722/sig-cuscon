SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_DestinationAddressLink;

CREATE TABLE T_DestinationAddressLink (
    SEC_RLS_SEQ_NO integer NOT NULL references T_SecurityRules(SEQ_NO),
    OBJECT_TYPE numeric(1,0) NOT NULL,
    LINK_SEQ_NO integer NOT NULL
);

ALTER TABLE cuscondb.T_DestinationAddressLink OWNER TO cuscondb;

ALTER TABLE ONLY T_DestinationAddressLink
    ADD CONSTRAINT T_DestinationAddressLink_pkey1 PRIMARY KEY (SEC_RLS_SEQ_NO, OBJECT_TYPE, LINK_SEQ_NO);
