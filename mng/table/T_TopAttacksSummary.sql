SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_TopAttacksSummary;

CREATE TABLE T_TopAttacksSummary (
    SEQ_NO serial NOT NULL,
    VSYS_ID varchar(20) NOT NULL,
    TARGET_DATE timestamp without time zone NOT NULL,
    TID varchar(7),
    SEVERITY_OF_THREATID varchar(1),
    THREATID varchar(128),
    THREAT_SUB_TYPE varchar(32),
    COUNT varchar(32)
);

ALTER TABLE cuscondb.T_TopAttacksSummary OWNER TO cuscondb;

ALTER TABLE ONLY T_TopAttacksSummary
    ADD CONSTRAINT T_TopAttacksSummary_pkey1 PRIMARY KEY (SEQ_NO);

CREATE INDEX i_idx1_T_TopAttacksSummary ON T_TopAttacksSummary USING btree (VSYS_ID, TARGET_DATE);
