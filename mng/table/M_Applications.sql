SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE M_Applications;
--DROP INDEX i_uniq_M_Applications;

CREATE TABLE M_Applications (
    SEQ_NO serial NOT NULL,
    NAME varchar(31) NOT NULL,
    CATEGORY_SEQ_NO integer references M_Category(SEQ_NO),
    SUB_CATEGORY_SEQ_NO integer references M_SubCategory(SEQ_NO),
    RISK numeric(1,0),
    TECHNOLOGY_SEQ_NO integer references M_Technology(SEQ_NO)
);

ALTER TABLE cuscondb.M_Applications OWNER TO cuscondb;

ALTER TABLE ONLY M_Applications
    ADD CONSTRAINT M_Applications_pkey1 PRIMARY KEY (SEQ_NO);

CREATE UNIQUE INDEX i_uniq_M_Applications ON M_Applications USING btree (NAME);
