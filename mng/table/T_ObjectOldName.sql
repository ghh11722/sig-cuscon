SET client_encoding = 'UNICODE';
SET check_function_bodies = false;
SET search_path = cuscondb, pg_catalog;

--DROP TABLE T_ObjectOldName;

CREATE TABLE T_ObjectOldName (
    VSYS_ID varchar(20) NOT NULL,
    OBJECT_TYPE numeric(1,0) NOT NULL,
    NAME varchar(31) NOT NULL
);

ALTER TABLE cuscondb.T_ObjectOldName OWNER TO cuscondb;

ALTER TABLE ONLY T_ObjectOldName
    ADD CONSTRAINT T_ObjectOldName_pkey1 PRIMARY KEY (VSYS_ID, OBJECT_TYPE, NAME);
