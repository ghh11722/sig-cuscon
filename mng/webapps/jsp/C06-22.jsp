<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お客様企業メイン" />
<bean:define id="titleScreenName" scope="page" value="ロールバック" />
<bean:define id="screenName" scope="page" value="Webフィルタ参照" />
<bean:define id="screenID" scope="page" value="C06-22" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<script>
	var urlNum = 0;
	var urlFilLst = new Array();
</script>
<body>
	<div id="content">
		<div id="editSourcezone">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ロールバック &raquo; Webフィルタ参照</p>

			<h3>Webフィルタ参照</h3>
			<ts:form action="/customer/fwsetting/rollback/RollbackBL" method="post" >
				<bean:size id="urlFilterSize" name="_RollbackForm" property="urlFilterList" />
				<div id="anyOrSelect">
					<ul>
						<logic:equal name="_RollbackForm" property="urlfilterDefaultFlg" value="0">
							<li><input name="selectRadio" type="radio" disabled="disabled" checked="checked"/>none</li>
							<li><input name="selectRadio" type="radio" disabled="disabled"/>選択する</li>
						</logic:equal>
						<logic:equal name="_RollbackForm" property="urlfilterDefaultFlg" value="1">
							<li><input name="selectRadio" type="radio" disabled="disabled"/>none</li>
							<li><input name="selectRadio" type="radio" disabled="disabled" checked="checked"/>選択する</li>
						</logic:equal>
					</ul>
				</div>
				<div id="available">
					<h4>&raquo; オブジェクトの選択</h4>
					<ul>
						<logic:iterate id="urlListBean" name="_RollbackForm" property="urlFilterList"  indexId="index">
							<li>
								<input name="urlRadio" type="radio"  disabled="disabled" />
								<bean:write name="urlListBean" property="name"/><br/>
							</li>
							<script type="text/javascript">
								{
									urlFilLst[urlNum] = "<bean:write name='urlListBean' property='name'/>";
									urlNum++;
								}
							</script>
						</logic:iterate>

						<logic:iterate id="urlListBean" name="_RollbackForm" property="urlFilterList"  indexId="index">
							<script type="text/javascript">
								{
									for(i=0; i<urlFilLst.length; i++) {
										if(urlFilLst[i] == "<bean:write name='_RollbackForm' property='urlFilterName'/>") {
											document._RollbackForm.urlRadio[i].checked=true;
										}
									}
								}
							</script>
						</logic:iterate>
					</ul>
				</div>
				<ul id="button">
					<li><ts:submit value="Cancel" /></li>
				</ul>
			</ts:form>
		<!-- editSourcezone -->
		</div>
	<!-- end #content --></div>
</body>

<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
