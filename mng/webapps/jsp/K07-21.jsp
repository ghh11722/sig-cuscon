<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お客様企業メイン" />
<bean:define id="titleScreenName" scope="page" value="ロールバック" />
<bean:define id="screenName" scope="page" value="コミットステータス確認" />
<bean:define id="screenID" scope="page" value="K07-21" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-03.jspf"%>
<body>
	<div id="content">
		<div id="commit">
		<p id="pankuzu">Top &raquo; FW設定機能 &raquo; コミットステータス確認</p>

		<h3>コミットステータス確認</h3>
		<div style="height:620px;">
		<div align="left">Commit/Rollback Operation</div>
		<table width="50%">
			<tr>
				<td>
					<table>
						<!--<caption  align="left">Commit/Rollback Operation</caption>-->
						<thead>
							<tr class="odd">
								<th abbr="Operation">Operation</th>
								<th abbr="Commit">
								<logic:equal name="_CommitStatusForm" property="processingKind" value="0">
									コミット
								</logic:equal>
								<logic:equal name="_CommitStatusForm" property="processingKind" value="1">
									ロールバック
								</logic:equal>
								<logic:equal name="_CommitStatusForm" property="processingKind" value="999">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</logic:equal>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr id="tr1">
								<th abbr="Starttime">受付時間</th>
								<th abbr="StarttimeContent"><bean:write name="_CommitStatusForm" property="startTime"/></th>
							</tr>
							<tr id="tr2">
								<th abbr="Status">処理状態</th>
								<th abbr="Processing">
								<logic:equal name="_CommitStatusForm" property="status" value="0">
									コミット成功
								</logic:equal>
								<logic:equal name="_CommitStatusForm" property="status" value="1">
									コミット失敗
								</logic:equal>
								<logic:equal name="_CommitStatusForm" property="status" value="2">
									コミット中
								</logic:equal>
								<logic:equal name="_CommitStatusForm" property="status" value="999">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</logic:equal>
								</th>
							</tr>
							<tr id="tr3">
								<th abbr="Details">メッセージ</th>
								<th abbr="DetailsContent"><bean:write name="_CommitStatusForm" property="message"/>
								</th>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</table>
		</div>
		<!-- end commit --></div>
		<!-- InstanceEndEditable -->
	<!-- end #content --></div>

	<div id="footer">
		<address>Copyright &copy; 2010 KDDI CORPORATION. All RIGHTS RESERVED.</address>
	</div>
	<!-- end #footer -->
<!-- end #container -->
</body>
<!-- InstanceEnd -->

<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
