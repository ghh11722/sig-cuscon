<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="ロールバック" />
<bean:define id="screenName" scope="page" value="ロールバック完了" />
<bean:define id="screenID" scope="page" value="C06-09" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<body>
	<div id="content">
		<div id="commit">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ロールバック</p>
			<h3>ロールバック</h3>
			<div id="body2">
				<div style="height:620px;">
					<div align="left">Rollback Operation</div>
					<table width="50%">
						<tr>
							<td>
								<table class="processing">
									<!--<caption align="left">Rollback Operation</caption>-->
								<thead>
									<tr class="odd">
										<th abbr="Operation">Operation</th>
									<th abbr="Commit">ロールバック</th>
									</tr>
								</thead>
								<tbody>
									<tr id="tr1">
										<th abbr="Starttime">受付時間</th>
										<th abbr="StarttimeContent"><bean:write name="_RollbackForm" property="startTime"/>
										</th>
									</tr>
									 <tr id="tr2">
										<th abbr="Status">処理状態</th>
										<th abbr="Processing"><bean:write name="_RollbackForm" property="acceptStatus"/>
										</th>
									</tr>
									 <tr id="tr3">
										<th abbr="Details">メッセージ</th>
										<th abbr="DetailsContent"><bean:write name="_RollbackForm" property="message"/>
										</th>
									</tr>
								</tbody>
								</table>
							</td>
						</tr>
					</table>
			</div>
			<div align="left">
				<br />
				<br />
				</div>
		</div>
		<!-- end commit -->
		</div>
	<!-- end #content --></div>
</body>
<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
