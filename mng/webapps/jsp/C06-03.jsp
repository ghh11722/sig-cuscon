<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="コミット" />
<bean:define id="screenName" scope="page" value="コミット処理中" />
<bean:define id="screenID" scope="page" value="C06-03" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<!-- begin #sidebar -->
<head>
<script language="JavaScript">
	<!--

		function NEXT() {
			if(document.getElementById) {
				var elem = document.getElementById("processingDialog");
				elem.style.visibility = 'visible';
			}
			document.commitForm.action = "<html:rewrite action="/customer/fwsetting/commit/CommitEndBL" />";
	        document.commitForm.submit();
		}

	//-->
</script>
</head>

<form action="#" name="form1">
	<div id="sidebarCustomer">
		<p id="login">Login: <bean:write name="USER_VALUE_OBJECT" property="loginId" scope="session" /></p>

	</div>
</form>
<!-- end #sidebar -->
<body onload="setTimeout('NEXT()',0)">
	<div id="content">
		<div id="commit">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; コミット</p>
			<h3>コミット</h3>
				<form name="commitForm" method="post">
					<div id="body2">
						<div style="height:620px;">
							<div align="left">Commit Operation</div>
							<table width="50%">
							    <tr>
							    	<td>
										<table class="processing">
										 	<!--<caption align="left">Commit Operation</caption>-->
											<thead>
												<tr class="odd">
													<th abbr="Operation">Operation</th>
													<th abbr="Commit">コミット</th>

												</tr>
											</thead>
											<tbody>
											 	<tr id="tr1">
													<th abbr="Status">Status</th>
													<th abbr="Processing">処理中</th>
												</tr>
											 	 <tr id="tr2">

													<th abbr="Details">Details</th>
													<th abbr="DetailsContent">処理が完了するまで数分かかる場合があります。</th>
												</tr>
											</tbody>
										</table>
							    	</td>
								</tr>
							</table>
							<div id="processingDialog" >
								<div id="loading" >NowProcessing...</div>
							</div>
						</div>
					</div>
				</form>
		</div><!-- end commit -->
	</div><!-- end content -->
</body>
<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
