<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="404 Not Found" />
<bean:define id="titleScreenName" scope="page" value="404 Not Found画面" />
<bean:define id="screenName" scope="page" value="404 Not Found" />
<bean:define id="screenID" scope="page" value="E_404" />
<bean:define id="cssCommonFileName" scope="page" value="/css/base.css" />
<bean:define id="cssExtendFileName" scope="page" value="/css/extend.css" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true" lang="true">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>KDDI Secure Gateway Service - Customer Control System -</title>
<bean:page id="requestObj" property="request"/>
<bean:define id="contextPath" name="requestObj" property="contextPath" />
<link href="<bean:write name='contextPath' /><bean:write name="cssCommonFileName" scope="page"/>" rel="stylesheet" type="text/css" />
<link href="<bean:write name='contextPath' /><bean:write name="cssExtendFileName" scope="page"/>" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
	<div id="exception">
		<h1>KDDI セキュリティGWシステム</h1>
		<h2>404 Not Found</h2>
	</div>
</div>
</body>
</html:html>
