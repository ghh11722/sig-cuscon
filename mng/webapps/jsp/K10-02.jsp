<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="FW接続情報管理機能" />
<bean:define id="titleScreenName" scope="page" value="顧客企業情報登録確認画面" />
<bean:define id="screenName" scope="page" value="顧客企業情報登録確認" />
<bean:define id="screenID" scope="page" value="K10-02" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true" lang="true">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-02.jspf"%>

<div id="content">
	<div id="blockCAdddel">
	<ts:form action="/admin/fwconnect/entryBL"  method="post">
	<h3>お客様設定 追加／削除</h3>
	<table>
	<thead>
		<tr class="odd">
			<th>操作</th>
			<th>VLAN</th>
			<th>GWアドレス(仮想IP)</th>
			<th>GWアドレス(ACT側実IP)</th>
			<th>GWアドレス(SBY側実IP)</th>
			<th>GWアドレス(VIP)</th>
			<th>PATアドレス</th>
			<th>Firewall-SYS</th>
			<th>Firewall-ID</th>
			<th>ログインID</th>
			<th>ログイン初期パスワード</th>
			<th>Vsys-ID</th>
			<th>PAログインID</th>
			<th>状態フラグ</th>
		</tr>
	</thead>
<logic:notEmpty name="uploadCSV">
	<tbody>
<logic:iterate id="userBean" name="uploadCSV" indexId="idx" scope="session" >
		<tr <%= idx % 2 == 0 ? "" : "class='odd'" %> >
			<td><bean:write name="userBean" property="opeFlg"/></td>
			<td><bean:write name="userBean" property="vlan"/></td>
			<td><bean:write name="userBean" property="gwAddressVirtual"/></td>
			<td><bean:write name="userBean" property="gwAddressAct"/></td>
			<td><bean:write name="userBean" property="gwAddressSby"/></td>
			<td><bean:write name="userBean" property="gwAddressVip"/></td>
			<td><bean:write name="userBean" property="patAddress"/></td>
			<td><bean:write name="userBean" property="firewallSys"/></td>
			<td><bean:write name="userBean" property="firewallId"/></td>
			<td><bean:write name="userBean" property="loginId"/></td>
			<td><bean:write name="userBean" property="initPassword"/></td>
			<td><bean:write name="userBean" property="vsysId"/></td>
			<td><bean:write name="userBean" property="paLoginId"/></td>
			<td><bean:write name="userBean" property="stateFlg"/></td>
		</tr>
</logic:iterate>
	</tbody>
</logic:notEmpty>
	</table>
<logic:notEmpty name="uploadCSV">
	<p>上記の内容で設定してもよろしいですか？</p>
	<ul id="button">
		<li><input type="submit" name="confirm" value="OK" /></li>
		<li><input type="submit" name="confirm" value="Cancel" /></li>
	</ul>
</logic:notEmpty>
	</ts:form>
	</div>
</div>

<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
