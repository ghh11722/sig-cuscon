<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="メイン画面" />
<bean:define id="screenName" scope="page" value="企業管理者メイン" />
<bean:define id="screenID" scope="page" value="K06-43" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-03.jspf"%>
<style type="text/css">
	pre.objName {min-width:150px; overflow:hidden; text-align: left; word-wrap:break-word}
	pre.siteList {min-width:180px; overflow:hidden; text-align: left; word-wrap:break-word}
	pre.category {min-width:100px; overflow:hidden; text-align: left; word-wrap:break-word;}
</style>
<%--
	pre.objName {width:150px; overflow:hidden; text-align: left;}
	pre.siteList {width:180px; overflow:hidden; text-align: left;}
	pre.category {width:100px; overflow:hidden; text-align: left; word-wrap:normal;}
 --%>
<script>
	<!--
		var submit_flag = 0;

		function okClick() {
			if( submit_flag == 1 ){ return; }
			submit_flag = 1;
			document.urlFilterForm.action = "<html:rewrite action="/admin/fwsetting/objectsetting/DelUrlFiltersBL" />";
		    document.urlFilterForm.submit();
		}

		function cancelClick() {
			if( submit_flag == 1 ){ return; }
			submit_flag = 1;
			document.urlFilterForm.action = "<html:rewrite action="/admin/fwsetting/objectsetting/UrlFiltersBL" />";
		    document.urlFilterForm.submit();
		}
	-->
</script>
<body>
	<div id="content"><!-- InstanceBeginEditable name="content" -->
		<div id="objectUrlFilters">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; オブジェクト設定 &raquo; Webフィルタ設定 &raquo; Webフィルタ削除</p>
			<h3>Webフィルタ削除</h3>
			<form action="" method="post" name="urlFilterForm">
				<table>
					<thead>
						<tr class="odd">
							<th scope="col">オブジェクト名</th>
							<th scope="col">ブロック</br>リスト</th>
							<th scope="col">アクション</th>
							<th scope="col">許可</br>リスト</th>
							<th scope="col">許可</br>カテゴリ</th>
							<th scope="col">ブロック</br>カテゴリ</th>
							<th scope="col">オーバーライド</br>カテゴリ</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<logic:notEmpty name="_UrlFiltersForm" property="urlFilterList">
							<tbody>
								<logic:iterate id="userBean" name="_UrlFiltersForm" property="urlFilterList" indexId="index">
									<tr <%=index % 2 == 0 ? "" : "class='odd'"%>>
										<td><pre class="objName"><bean:write name="userBean" property="name" /></pre></td>
										<td><pre class="siteList"><bean:write name="userBean" property="blockListSite" /></pre></td>
										<td><bean:write name="userBean" property="actionBlockListSiteWamei" /></td>
										<td><pre class="siteList"><bean:write name="userBean" property="allowListSite" /></pre></td>
										<td><pre class="category"><bean:write name="userBean" property="alertCategory" /></pre></td>
										<td><pre class="category"><bean:write name="userBean" property="blockCategory" /></pre></td>
										<td><pre class="category"><bean:write name="userBean" property="continueCategory" /></pre></td>
									</tr>
								</logic:iterate>
							</tbody>
							</logic:notEmpty>
						</tr>
					</tbody>
				</table>
				<p>上記の設定を削除してもよろしいですか？</p>
				<ul id="button">
					<li><input type="button" value="OK" onclick="okClick()" /></li>
					<li><input type="button" value="Cancel" onclick="cancelClick()"/></li>
				</ul>
			</form>
		<!-- #objectServices --></div>
	<!-- InstanceEndEditable -->
	<!-- end #content --></div>
</body>
<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
