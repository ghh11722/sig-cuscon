<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="DMZ設定" />
<bean:define id="titleScreenName" scope="page" value="顧客企業メイン画面" />
<bean:define id="screenName" scope="page" value="顧客企業メイン" />
<bean:define id="screenID" scope="page" value="K03-02" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-01.jspf"%>
<script>
	var submit_flag = 0;
	function updateURLFiltering() {
		if( submit_flag == 1 ){ return; }

		var msg = "";
		if(document._AdminMainForm.UrlFilteringRadio[0].checked) {
			msg="ON";
		} else {
			msg="OFF";
		}
		var res = confirm("Webフィルタサービスを" + msg + "します。よろしいですか？");
		if( res == true ) {
			submit_flag = 1;
			if(document._AdminMainForm.UrlFilteringRadio[0].checked) {
		    	document._AdminMainForm.radio.value = 1;
			} else {
				document._AdminMainForm.radio.value = 0;
			}
		    document._AdminMainForm.submit();
		}
	}
	function cancelClick() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document._AdminMainForm.action = "<html:rewrite action="/admin/mainBL" />";
	    document._AdminMainForm.submit();
	}
</script>
<body>
	<div id="content">
		<div id="editIpsIds">
			<p id="pankuzu">Top &raquo; Webフィルタ設定</p>
			<h3>Webフィルタ設定</h3>

			<ts:form action="/admin/UpdateURLFilteringBL" method="post" >

				<div id="useOrUnuse">
					<ul>
						<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
							<logic:equal name="_AdminMainForm" property="editUrlFilteringFlg" value="1">
								<li><input name="UrlFilteringRadio" type="radio" disabled="disabled" checked="checked"/>○：利用する</li>
								<li><input name="UrlFilteringRadio" type="radio" disabled="disabled"/>×：利用しない</li>
							</logic:equal>
							<logic:equal name="_AdminMainForm" property="editUrlFilteringFlg" value="0">
								<li><input name="UrlFilteringRadio" type="radio" disabled="disabled"/>○：利用する</li>
								<li><input name="UrlFilteringRadio" type="radio" disabled="disabled" checked="checked"/>×：利用しない</li>
							</logic:equal>
						</logic:notMatch>
						<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
							<logic:equal name="_AdminMainForm" property="editUrlFilteringFlg" value="1">
								<li><input name="UrlFilteringRadio" type="radio"  checked="checked"/>○：利用する</li>
								<li><input name="UrlFilteringRadio" type="radio" />×：利用しない</li>
							</logic:equal>
							<logic:equal name="_AdminMainForm" property="editUrlFilteringFlg" value="0">
								<li><input name="UrlFilteringRadio" type="radio" />○：利用する</li>
								<li><input name="UrlFilteringRadio" type="radio"  checked="checked"/>×：利用しない</li>
							</logic:equal>
						</logic:match>
					</ul>

				</div>
				<ul id="button">
					<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<li><input type="button" value="OK" disabled="disabled"/></li>
					</logic:notMatch>
					<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<li><input type="button" value="OK"  onclick="updateURLFiltering()" /></li>
					</logic:match>
					<li><input type="button" value="Cancel" onclick="cancelClick()"/></li>
				</ul>
				<input type="hidden" name="radio" value="0"/>
				<input type="hidden" name="firewallId" value="<bean:write name="_AdminMainForm" property="firewallId"/>"/>
			</ts:form>
		</div>
		<!-- end editIpsIds -->

	<!-- end #content --></div>

</body>

<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
