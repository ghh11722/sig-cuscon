<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="権限チェック" />
<bean:define id="titleScreenName" scope="page" value="権限チェックエラー画面" />
<bean:define id="screenName" scope="page" value="権限チェック" />
<bean:define id="screenID" scope="page" value="E-AUTHORIZATION" />
<bean:define id="cssCommonFileName" scope="page" value="/css/base.css" />
<bean:define id="cssExtendFileName" scope="page" value="/css/extend.css" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true" lang="true">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Customer Control System</title>
<bean:page id="requestObj" property="request"/>
<bean:define id="contextPath" name="requestObj" property="contextPath" />
<link href="<bean:write name='contextPath' /><bean:write name="cssCommonFileName" scope="page"/>" rel="stylesheet" type="text/css" />
<link href="<bean:write name='contextPath' /><bean:write name="cssExtendFileName" scope="page"/>" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="wrapper">
	<div id="exception">
		<h1>Customer Control System</h1>
		<form action="<bean:write name='contextPath' />/exception-logout.do">
		<h2>このページは現在のログインIDでは使用できません。</h2>
		<h2><input type="submit" value="OK" /></h2>
		</form>
	</div>
</div>
</body>
</html:html>
