<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="アカウントロック解除機能" />
<bean:define id="titleScreenName" scope="page" value="アカウントロック解除画面" />
<bean:define id="screenName" scope="page" value="アカウントロック解除" />
<bean:define id="screenID" scope="page" value="0001" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true" lang="true">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-01.jspf"%>

<script language="JavaScript" type="text/JavaScript">
<!--
	function listOver( id_tr, id_check  ){
		if(document.getElementById) {
			document.getElementById(id_tr).className = "mouseover";
			if( document.getElementById(id_check).checked == true ){
				document.getElementById(id_tr).className = "selected";
			}
		}
	}
	function listOut( id_tr, id_check, style_class ){
		if(document.getElementById) {
			document.getElementById(id_tr).className = style_class;
			if( document.getElementById(id_check).checked == true ){
				document.getElementById(id_tr).className = "selected";
			}
		}
	}
	function listClick( id_tr, id_check, style_class ){
		if(document.getElementById) {
			if( document.getElementById(id_check).checked == true ){
				document.getElementById(id_check).checked = false;
				document.getElementById(id_tr).className = style_class;
			}
			else{
				document.getElementById(id_check).checked = true;
				document.getElementById(id_tr).className = "selected";
			}
		}
	}
	function CheckAll(sw){
		if(document.getElementById) {
			var obj = document.getElementById("id_customers");
			if(obj == null ) return;
			var TRs = obj.getElementsByTagName("tr");
			var check = false;
			if( sw == 1 ){ check = true; }
			if( document._UnlockAccountForm.targetLoginId.length) {
				for(i = 0; i < document._UnlockAccountForm.targetLoginId.length; i++){
					document._UnlockAccountForm.targetLoginId[i].checked = check;
					if( check == true ){
						TRs[i].className = "selected";
					}
					else {
						if( i % 2 ) { TRs[i].className = "odd"; }
						else        { TRs[i].className = ""; }
					}
				}
			}
			else {
				document._UnlockAccountForm.targetLoginId.checked = check;
				if( check == true ){
					TRs[0].className = "selected";
				}
				else {
					TRs[0].className = "";
				}
			}
		}
	}
	function CheckBoxAll(){
		if(document.getElementById) {
			if( document.getElementById("all_checkbox").checked == true ){
				CheckAll(1);
			}
			else{
				CheckAll(0);
			}
		}
	}
//-->
</script>

<div id="content">
	<div id="accountLock">
		<h3>アカウントロック管理</h3>
		<ts:form action="/admin/account/unlockBL" >
		<p>アカウントロックされているアカウント一覧</p>
		<table>
			<thead>
				<tr class="odd">
					<th><input name="all_checkbox" id="all_checkbox" type="checkbox" onclick="CheckBoxAll();" /></th>
					<th scope="col" abbr="User ID">ログインID</th>
					<th scope="col" abbr="Firewall ID">ファイアウォールID</th>
					<th scope="col" abbr="Virtual System">ヴァーチャルシステム(VSYS)</th>
					<th scope="col" abbr="Last Login Date">最終ログイン日時</th>
				</tr>
			</thead>
<logic:notEmpty name="_UnlockAccountForm" property="lock_accounts">
			<tbody id="id_customers">
<logic:iterate id="userBean" name="_UnlockAccountForm" property="lock_accounts" indexId="idx">
				<tr id="id_tr<bean:write name="idx"/>"
					<%= idx % 2 == 0 ? "" : "class='odd'" %>
					onmouseover="listOver('id_tr<bean:write name="idx"/>','id_check<bean:write name="idx"/>');"
					onmouseout="listOut('id_tr<bean:write name="idx"/>','id_check<bean:write name="idx"/>','<%= idx % 2 == 0 ? "" : "odd" %>');"
					onclick="listClick('id_tr<bean:write name="idx"/>','id_check<bean:write name="idx"/>','<%= idx % 2 == 0 ? "" : "odd" %>');">
					<td>
						<input type="checkbox"
						       id="id_check<bean:write name="idx"/>" name="targetLoginId"
						       value="<bean:write name="userBean" property="loginId"/>"
						       onclick="listClick('id_tr<bean:write name="idx"/>','id_check<bean:write name="idx"/>');" />
					</td>
					<td><bean:write name="userBean" property="loginId" /></td>
					<td><bean:write name="userBean" property="firewallId" /></td>
					<td><bean:write name="userBean" property="vsysId" /></td>
					<td><bean:write name="userBean" property="lastLoginDate" /></td>
				</tr>
</logic:iterate>
			</tbody>
</logic:notEmpty>
		</table>
<logic:notEmpty name="_UnlockAccountForm" property="lock_accounts">
		<ul id="button">
			<li><ts:submit value="ロックを解除する" /></li>
			<li><input type="button" value="Cancel" onclick="document.location='<bean:write name='contextPath' />/admin/mainBL.do'" /></li>
		</ul>
</logic:notEmpty>
		</ts:form>
	</div>
</div>
<logic:equal name="_UnlockAccountForm" property="resultmsg" value="success">
<script language="JavaScript" type="text/JavaScript">
<!--
alert('<ts:messages id="message" message="true"><bean:write name="message"/></ts:messages>');
//-->
</script>
</logic:equal>

<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
