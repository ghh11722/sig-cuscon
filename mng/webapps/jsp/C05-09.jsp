<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="メイン画面" />
<bean:define id="screenName" scope="page" value="企業管理者メイン" />
<bean:define id="screenID" scope="page" value="C05-09" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<head>
	<script type="text/javascript">
	<!--
	var submit_flag = 0;
	var chkNum = 0;
	var chkbox = 0;
	var indexList = new Array();
	var lastIndex = 0;
	function lookDisplayType(){
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		indexList = new Array();

		chkNum = 0;

		var tbl = document.getElementById("selectTBL");

		var selectIndex = 0;

		if (chkbox == 1 && document.subForm.checkboxNum.checked) {
			selectIndex = 0
			indexList.push(selectIndex);
		} else if (chkbox > 1) {
			for (i = 1; i < chkbox + 1; i++) {
				if(document.subForm.checkboxNum[i-1].checked) {
					chkNum++;
					selectIndex = i-1;
					// indexリストに詰める
					indexList.push(selectIndex);
				}
			}
		}

		var display = "<bean:write name="_ApplicationGroupsForm" property="displayType"/>";

		if (display == "登録") {
			addData4Regist();
		} else {
			addDate4Update();
		}
	}

	function lookDisplayType4Cancel(){

		var display = "<bean:write name="_ApplicationGroupsForm" property="displayType"/>";

		if (display == "登録") {
			document.subForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/AddApplicationGroupBL" />";
        	document.subForm.submit();
		} else {
			document.subForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/EditApplicationGroupBL" />";
        	document.subForm.submit();
		}
	}

	function addData4Regist(){
		var str = "";
		for(i=0;i<indexList.length;i++) {
			if(i==0) {
				str = indexList[i];
			} else {
				str = str + "," + indexList[i];
			}
		}
		document.subForm.index.value = str;
		document.subForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/AddFiltersBL" />";
       	document.subForm.submit();
	}

	function addDate4Update(){
		var str = "";

		for(i=0;i<indexList.length;i++) {
			if(i==0) {
				str = indexList[i];
			} else {
				str = str + "," + indexList[i];
			}
		}
		document.subForm.index.value = str;
		document.subForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/AddFiltersBL" />";
       	document.subForm.submit();
	}

	function listClick( id_tr , index ){
		if(document.getElementById) {
			var obj = document.getElementById("id_customers");
			var TRs = obj.getElementsByTagName("tr");
			if( lastIndex % 2 ) {
				TRs[lastIndex].className = "odd";
			} else {
				TRs[lastIndex].className = "";
			}
			document.getElementById(id_tr).className = "selected";
			lastIndex = index;
		}
	}
	function onCancel(kbn){
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		if(kbn == "add"){
			document.subForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/AddApplicationGroupBL" />";
		} else {
			document.subForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/EditApplicationGroupBL" />";
		}
       	document.subForm.submit();
	}
	-->
	</script>
</head>
<body onunload="unLoadWindow()">
	<div id="content">
		<div id="editFilters">
			<logic:match name="_ApplicationGroupsForm" property="displayType" value="登録">
				<p id="pankuzu">Top &raquo; FW設定機能 &raquo; オブジェクト設定 &raquo; アプリケーショングループ設定 &raquo; アプリケーショングループ追加 &raquo; フィルター登録</p>
			</logic:match>
			<logic:notMatch name="_ApplicationGroupsForm" property="displayType" value="登録">
				<p id="pankuzu">Top &raquo; FW設定機能 &raquo; オブジェクト設定 &raquo; アプリケーショングループ設定 &raquo; アプリケーショングループ編集 &raquo; フィルター登録</p>
			</logic:notMatch>
			<h3>フィルター登録</h3>
			<form action="/customer/fwsetting/objectsetting/AppolicationGroupsBL" name="subForm" method="post" >
				<table id="selectTBL">
				<thead>
					<tr class="odd">
						<th scope="col">&nbsp;</th>
						<th scope="col">フィルター名</th>
						<th scope="col">カテゴリー</th>
						<th scope="col">サブカテゴリー</th>
						<th scope="col">テクノロジー</th>
						<th scope="col">リスク</th>
					</tr>
				</thead>
				<tbody id="id_customers">
					<logic:iterate id="userBean" name="_ApplicationGroupsForm" property="filterList4Add" indexId="index">
						<tr id="id_tr<bean:write name="index"/>" <%= index % 2 == 0 ? "" : "class='odd'" %>
							    onclick="listClick('id_tr<bean:write name="index"/>',<%= index %>);" >
							<td>
								<logic:notMatch name="userBean"  property="chkFlg" value="true" >
									<input type="checkbox" name="checkboxNum"/>
								</logic:notMatch>
								<logic:match name="userBean"  property="chkFlg" value="true" >
									<input type="checkbox" checked="checked" name="checkboxNum"/>
								</logic:match>
								<script type="text/javascript">
									{
										chkbox++;
									}
								</script>
							</td>
							<td>
								<bean:write name="userBean" property="name"/>
							</td>
							<td>
								<logic:iterate id="categoryBean" name="userBean" property="categoryList" >
									<bean:write name="categoryBean" property="name"/>
								</logic:iterate>
							</td>
							<td>
								<logic:iterate id="subCategoryBean" name="userBean" property="subCategoryList" >
									<bean:write name="subCategoryBean" property="name"/>
								</logic:iterate>
							</td>
							<td>
								<logic:iterate id="technologyBean" name="userBean" property="technologyList" >
									<bean:write name="technologyBean" property="name"/>
								</logic:iterate>
							</td>
							<td>
								<logic:iterate id="riskBean" name="userBean" property="riskList" >
									<bean:write name="riskBean" property="name"/>
								</logic:iterate>
							</td>
						</tr>
					</logic:iterate>
				</tbody>
				</table>
				<ul id="button">
					<li><input type="button" value="OK" onclick="lookDisplayType()" /></li>
					<logic:match name="_ApplicationGroupsForm" property="displayType" value="登録">
						<li><input type="button" value="Cancel" onclick="onCancel('add')"/></li>
					</logic:match>
					<logic:notMatch name="_ApplicationGroupsForm" property="displayType" value="登録">
						<li><input type="button" value="Cancel" onclick="onCancel('edt')"/></li>
					</logic:notMatch>
				<!-- end #button --></ul>
				<input type="hidden" name="displayType" value="<bean:write name="_ApplicationGroupsForm" property="displayType"/>"/>
				<input type="hidden" name="index"/>
				<input type="hidden" name="name" value="<bean:write name="_ApplicationGroupsForm" property="name"/>"/>
				<input type="hidden" id="oldName" name="oldName" value="<bean:write name="_ApplicationGroupsForm" property="oldName"/>"/>
			</form>
		<!-- end #editFilters -->
		</div>
	<!-- end #content --></div>
</body>

<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
