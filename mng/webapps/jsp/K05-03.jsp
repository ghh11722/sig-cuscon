<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お客様企業メイン" />
<bean:define id="titleScreenName" scope="page" value="ポリシー一覧" />
<bean:define id="screenName" scope="page" value="Name編集" />
<bean:define id="screenID" scope="page" value="K05-03" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-03.jspf"%>
<head>
	<script type="text/javascript">
	<!--
	var submit_flag = 0;
	function KeyEvent(){
		if(event.keyCode == 13){event.returnValue = false;}
	}

	function okClick() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document._PolicyForm.submit();
	}
	function cancelClick() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document._PolicyForm.action = "<html:rewrite action="/admin/fwsetting/policysetting/PolicyBL" />";
	    document._PolicyForm.submit();
	}
	-->
	</script>
</head>

<body>
	<div id="content">
	<div id="editName">
		<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ポリシー設定 &raquo; ポリシー名設定</p>
			<h3>ポリシー名設定</h3>
			<ts:form action="/admin/fwsetting/policysetting/UpdatePolicyBL"  method="post" >
				<dl>
					<dt>ポリシー名</dt>
					<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<dd><input name="modName" value="<bean:write name="_PolicyForm" property="name" />" disabled="disabled"/></dd>
					</logic:notMatch>
					<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<dd><input name="modName" value="<bean:write name="_PolicyForm" property="name"/>" onkeydown="KeyEvent()"/></dd>
					</logic:match>
				</dl>
				<ul id="button">
					<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<li><input type="button" value="OK" disabled="disabled"/></li>
					</logic:notMatch>
					<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<li><input type="button" value="OK" onclick="okClick()"/></li>
					</logic:match>
					<li><input type="button" value="Cancel" onclick="cancelClick()"/></li>
				</ul>
				<input type="hidden" name="name" value="<bean:write name="_PolicyForm" property="name"/>"/>
			</ts:form>
		<!-- end editName -->
	</div>
	<!-- end #editName -->

	<!-- end #content --></div>
</body>

<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
