<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/struts-nested" prefix="nested" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="メイン画面" />
<bean:define id="screenName" scope="page" value="企業管理者メイン" />
<bean:define id="screenID" scope="page" value="C05-08" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<head>
	<script type="text/javascript">
	<!--
		var submit_flag = 0;
		function addApl(index){

			var selectApl = document.getElementById("aplTBL").rows[index].cells[1].innerHTML;

		    var tableId = 'selectTBL';
		    var targetTable = document.getElementById(tableId);
		    var rowCount    = targetTable.rows.length;
		    var colCount    = targetTable.rows[0].cells.length;

		    var tbl = document.getElementById("selectTBL");
		    var flg = 0;
		    for (i = 1; i < tbl.rows.length; i = i +1) {
				if (tbl.rows[i].cells[1].innerHTML == selectApl) {
					flg++;
				}
			}

		    if (flg == 0) {
			    var row = targetTable.insertRow(rowCount);

			    var cell1 = row.insertCell(-1);
			    var cell2 = row.insertCell(-1);
			    var cell3 = row.insertCell(-1);

			    var button = document.createElement("input");
			    button.type = "button";
			    button.value = "削除";

			    button.onclick = function(event) {

			        var tbl = document.getElementById("selectTBL");
			        deleteRow(this.parentNode.parentNode.rowIndex);
			    };

		    	cell1.appendChild(button);
		    	cell2.appendChild(document.createTextNode(selectApl));
		    	cell3.appendChild(document.createTextNode("アプリケーション"));
		    }
		}

		function deleteRow(index){

  			var tbl = document.getElementById("selectTBL");

			tbl.deleteRow(index);
		}

		function jumpAddFilter() {
			document.getElementById('displayType').value = "更新";
			getSelectApl();
			document.aplForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/ChoiceFiltersBL" />";
	        document.aplForm.submit();
		}

		function clickOk(){
			if( submit_flag == 1 ){ return; }
			submit_flag = 1;
			var selectApl = new Array();
			var selectType = new Array();

			var tbl = document.getElementById("selectTBL");

			for (i = 1; i < tbl.rows.length; i = i +1) {

				var selectAplStr = document.getElementById("selectTBL").rows[i].cells[1].innerHTML;
				var selectTypeStr = document.getElementById("selectTBL").rows[i].cells[2].innerHTML;

				selectApl[i-1] = selectAplStr;
				selectType[i-1] = selectTypeStr;

				make_hidden('selectAplList', selectApl[i-1], 'aplForm');
				make_hidden('selectTypeList', selectType[i-1], 'aplForm');
			}
			if(tbl.rows.length == 1) {

				make_hidden('selectAplList', "null", 'aplForm');
				make_hidden('selectTypeList', "null", 'aplForm');
			}

			document.getElementById('displayType').value = "";

			document.aplForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/UpdateApplicationGroupBL" />";
	        document.aplForm.submit();
		}

		function clickCancel(){
			if( submit_flag == 1 ){ return; }
			submit_flag = 1;

			document.getElementById('displayType').value = "";

			document.getElementById('objectName').value = "";

			document.getElementById('searchWord').value = "";

			document.aplForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/ApplicationGroupsBL" />";
	        document.aplForm.submit();
		}

		function searchData(){
			if( submit_flag == 1 ){ return; }
			submit_flag = 1;
			var selectApl = new Array();
			var selectType = new Array();

			var tbl = document.getElementById("selectTBL");
			if(tbl == null) {
				submit_flag = 0;
				return;
			}
			for (i = 1; i < tbl.rows.length; i = i +1) {

				var selectAplStr = document.getElementById("selectTBL").rows[i].cells[1].innerHTML;
				var selectTypeStr = document.getElementById("selectTBL").rows[i].cells[2].innerHTML;

				selectApl[i-1] = selectAplStr;
				selectType[i-1] = selectTypeStr;

				make_hidden('selectAplList', selectApl[i-1], 'aplForm');
				make_hidden('selectTypeList', selectType[i-1], 'aplForm');
			}

			document.aplForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/Search4GroupBL" />";
	        document.aplForm.submit();
			}

		function make_hidden( name, value, formname ){
		    var q = document.createElement('input');
		    q.type = 'hidden';
		    q.name = name;
		    q.value = value;

			if (formname){document.forms[formname].appendChild(q);
			} else{ document.forms[0].appendChild(q);
			}
		}

		function change(value){
	      	if(event.keyCode == 13){
	      		if( submit_flag == 1 ){ return; }
				submit_flag = 1;
				var selectApl = new Array();
				var selectType = new Array();

				var tbl = document.getElementById("selectTBL");
				if(tbl == null) {
					submit_flag = 0;
					return;
				}
				for (i = 1; i < tbl.rows.length; i = i +1) {

					var selectAplStr = document.getElementById("selectTBL").rows[i].cells[1].innerHTML;
					var selectTypeStr = document.getElementById("selectTBL").rows[i].cells[2].innerHTML;

					selectApl[i-1] = selectAplStr;
					selectType[i-1] = selectTypeStr;

					make_hidden('selectAplList', selectApl[i-1], 'aplForm');
					make_hidden('selectTypeList', selectType[i-1], 'aplForm');
				}

				document.aplForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/Search4GroupBL" />";
		        document.aplForm.submit();
		    }
		}

		function clearFilter() {
			if( submit_flag == 1 ){ return; }
			submit_flag = 1;
			var selectApl = new Array();
			var selectType = new Array();

			var tbl = document.getElementById("selectTBL");
			if(tbl == null) {
				submit_flag = 0;
				return;
			}
			for (i = 1; i < tbl.rows.length; i = i +1) {

				var selectAplStr = document.getElementById("selectTBL").rows[i].cells[1].innerHTML;
				var selectTypeStr = document.getElementById("selectTBL").rows[i].cells[2].innerHTML;

				selectApl[i-1] = selectAplStr;
				selectType[i-1] = selectTypeStr;

				make_hidden('selectAplList', selectApl[i-1], 'aplForm');
				make_hidden('selectTypeList', selectType[i-1], 'aplForm');
			}
			document.getElementById('searchWord').value = "";

			document.aplForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/Clear4GroupBL" />";
	        document.aplForm.submit();
		}

		function getSelectApl() {
			var selectApl = new Array();
			var selectType = new Array();

			var tbl = document.getElementById("selectTBL");

			for (i = 1; i < tbl.rows.length; i = i +1) {

				var selectAplStr = document.getElementById("selectTBL").rows[i].cells[1].innerHTML;
				var selectTypeStr = document.getElementById("selectTBL").rows[i].cells[2].innerHTML;

				selectApl[i-1] = selectAplStr;
				selectType[i-1] = selectTypeStr;


				make_hidden('selectAplList', selectApl[i-1], 'aplForm');
				make_hidden('selectTypeList', selectType[i-1], 'aplForm');
			}
			if(tbl.rows.length == 1) {
				make_hidden('selectAplList', "null", 'aplForm');
				make_hidden('selectTypeList', "null", 'aplForm');
			}
		}

	 -->
</script>
</head>
<body onunload="unLoadWindow()">
	<div id="content">
		<div id="blockApplication">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; オブジェクト設定 &raquo; アプリケーショングループ設定 &raquo; アプリケーショングループ編集</p>
			<h3>アプリケーショングループ編集</h3>
			<form action="/customer/fwsetting/objectsetting/AppolicationGroupsBL" name="aplForm" method="post" >
				<p>オブジェクト名:
				<input name="name" id="objectName" value="<bean:write name="_ApplicationGroupsForm" property="name"/>"/></p>
				<div id="box1">
					<p id="filters"><a href="javascript:jumpAddFilter()">フィルターを設定する</a></p>
				<!-- end box1 --></div>
				<div id="box2">
					<p id="search">検索：<input name="searchWord" id="searchWord" value="<bean:write name="_ApplicationGroupsForm" property="searchWord"/>"  onkeypress="change(this.value)"/></p>
					<p id="filterReset"><html:button property="reset" value="フィルターの解除" onclick="clearFilter()"/></p>
				<!-- end box2 --></div>
				<div id="applicationeditFilter">

	<bean:define id="filterbean" name="_ApplicationGroupsForm" property="filterList" />
	<div id="categoly">
			<h4>カテゴリー</h4>
			<div id="categolyList">
			<ul>
				<logic:iterate id="categoryList" name="filterbean" property="categoryList" indexId="index" >
					<li>
						<html:checkbox name="categoryList" property="chkFlg" indexed="true"	onclick="javascript:searchData()" />
						<nested:hidden property="chkFlg" value="false"/>
						<bean:write name="categoryList" property="name"/>
						<html:hidden name="categoryList" property="name" indexed="true"/>
						<html:hidden name="categoryList" property="seqNo" indexed="true"/>
					</li>
				</logic:iterate>
			</ul>
			<!-- end #categoryList --></div>
			<!-- #category --></div>
			<div id="subcategory">
			<h4>サブカテゴリー</h4>
			<div id="subcategoryList">
			<ul>
				<logic:iterate id="subCategoryList" name="filterbean" property="subCategoryList" indexId="index" >
					<li>
						<html:checkbox name="subCategoryList" property="chkFlg" indexed="true" onclick="javascript:searchData()" />
						<nested:hidden property="chkFlg" value="false"/>
						<bean:write name="subCategoryList" property="name"/>
						<html:hidden name="subCategoryList" property="name" indexed="true"/>
						<html:hidden name="subCategoryList" property="seqNo" indexed="true"/>
					</li>
				</logic:iterate>
			</ul>
			<!-- end #subcategoryList --></div>
			<!-- #subcategoly --></div>
			<div id="technology">
			<h4>テクノロジー</h4>
			<div id="technologyList">
			<ul>
				<logic:iterate id="technologyList" name="filterbean" property="technologyList" indexId="index" >
					<li>
						<html:checkbox name="technologyList" property="chkFlg" indexed="true" onclick="javascript:searchData()" />
						<nested:hidden property="chkFlg" value="false"/>
						<bean:write name="technologyList" property="name"/>
						<html:hidden name="technologyList" property="name" indexed="true"/>
						<html:hidden name="technologyList" property="seqNo" indexed="true"/>
					</li>
				</logic:iterate>
			</ul>
			<!-- end #technology --></div>
			<!-- #technology --></div>
			<div id="risk">
			<h4>リスク</h4>
			<div id="riskList">
				<ul>
					<logic:iterate id="riskList" name="filterbean" property="riskList" indexId="index" >
						<li>
							<html:checkbox name="riskList" property="chkFlg" indexed="true"	onclick="javascript:searchData()" />
							<nested:hidden property="chkFlg" value="false"/>
							<bean:write name="riskList" property="name"/>
							<html:hidden name="riskList" property="name" indexed="true"/>
						</li>
					</logic:iterate>
				</ul>
		<!-- end #riskList --></div>
		<!-- #risk --></div>
	<!-- end #applicationeditFilter --></div>

			<div id="filterResult">
				<table id="aplTBL">
					<thead>
						<tr class="odd">
							<th scope="col">&nbsp;</th>
							<th scope="col">アプリケーション名</th>
							<th scope="col">カテゴリー</th>
							<th scope="col">サブカテゴリー</th>
							<th scope="col">テクノロジー</th>
							<th scope="col">リスク</th>
						</tr>
					</thead>
					<tbody>
						<logic:iterate id="applicationBean" name="_ApplicationGroupsForm" property="applicationList"  indexId="index">
							<tr <%=index % 2 == 0 ? "" : "class='odd'"%>>
								<td><input type="button" value="追加" onclick="addApl(this.parentNode.parentNode.rowIndex)"/></td>
								<td><bean:write name="applicationBean" property="name"/></td>
								<td><bean:write name="applicationBean" property="category"/></td>
								<td><bean:write name="applicationBean" property="subCategory"/></td>
								<td><bean:write name="applicationBean" property="technology"/></td>
								<td><bean:write name="applicationBean" property="risk"/></td>
							</tr>
						</logic:iterate>
					</tbody>
				</table>
				<!-- #filterResult --></div>

				<div id="result"><br />
					<p>選択中のアプリケーション</p>
				<table id="selectTBL">
					<thead>
						<tr class="odd">
							<th scope="col"></th>
							<th scope="col">アプリケーション名</th>
							<th scope="col">タイプ</th>
						</tr>
						</thead>
						<logic:notEmpty name="_ApplicationGroupsForm" property="selectedAplList">
							<tbody>
								<logic:iterate id="applicationBean" name="_ApplicationGroupsForm" property="selectedAplList"  indexId="index">
									<tr <%=index % 2 == 0 ? "" : "class='odd'"%>>
										<td><input type="button" value="削除" onclick="deleteRow(this.parentNode.parentNode.rowIndex)"/></td>
										<td><bean:write name="applicationBean" property="name"/></td>
										<td><bean:write name="applicationBean" property="type"/></td>
									</tr>
								</logic:iterate>
							</tbody>
						</logic:notEmpty>
				</table>
				<!-- end #result--></div>

			<ul id="button">
				<li><input type="button" value="OK" onclick="clickOk()" /></li>
				<li><input type="button" value="Cancel" onclick="clickCancel()" /></li>
				</ul>
				<input type="hidden" name="addName" value="<bean:write name="_ApplicationGroupsForm" property="name"/>"/>
				<input type="hidden" id="displayType" name="displayType" value="<bean:write name="_ApplicationGroupsForm" property="displayType"/>"/>
				<input type="hidden" id="oldName" name="oldName" value="<bean:write name="_ApplicationGroupsForm" property="oldName"/>"/>
				<br />
			</form>
			<!-- end #blockApplication -->
		</div>
	<!-- end #content --></div>
</body>
<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
