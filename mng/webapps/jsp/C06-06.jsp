<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="ロールバック" />
<bean:define id="screenName" scope="page" value="ロールバック" />
<bean:define id="screenID" scope="page" value="C06-06" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<head>
	<script type="text/javascript">
	var lastIndex = 0;
	var submit_flag = 0;
	function jump() {

		document.geneForm.selectGeneNo.value =document.geneForm.name.options[document.geneForm.name.selectedIndex].value;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/rollback/RollbackBL" />";
        document.geneForm.submit();
	}

	function jumpName(index) {
		document.geneForm.selectGeneNo.value =document.geneForm.name.options[document.geneForm.name.selectedIndex].value;
		document.geneForm.index.value =index;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/rollback/ShowPolicyBL" />";
        document.geneForm.submit();
	}

	function jumpAppli(index) {
		document.geneForm.selectGeneNo.value =document.geneForm.name.options[document.geneForm.name.selectedIndex].value;
		document.geneForm.index.value =index;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/rollback/ShowApplicationBL" />";
        document.geneForm.submit();
	}

	function jumpSource(index) {
		document.geneForm.selectGeneNo.value =document.geneForm.name.options[document.geneForm.name.selectedIndex].value;
		document.geneForm.index.value =index;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/rollback/ShowSourceZoneBL" />";
        document.geneForm.submit();
	}

	function jumpDestination(index) {
		document.geneForm.selectGeneNo.value =document.geneForm.name.options[document.geneForm.name.selectedIndex].value;
		document.geneForm.index.value =index;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/rollback/ShowDestinationZoneBL" />";
        document.geneForm.submit();
	}

	function jumpSrcAddress(index) {
		document.geneForm.selectGeneNo.value =document.geneForm.name.options[document.geneForm.name.selectedIndex].value;
		document.geneForm.index.value =index;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/rollback/ShowSourceAddressBL" />";
        document.geneForm.submit();
	}

	function jumpDstAddress(index) {
		document.geneForm.selectGeneNo.value =document.geneForm.name.options[document.geneForm.name.selectedIndex].value;
		document.geneForm.index.value =index;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/rollback/ShowDestinationAddressBL" />";
        document.geneForm.submit();
	}

	function jumpService(index) {
		document.geneForm.selectGeneNo.value =document.geneForm.name.options[document.geneForm.name.selectedIndex].value;
		document.geneForm.index.value =index;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/rollback/ShowServiceBL" />";
        document.geneForm.submit();
	}

	function jumpIPS_IDS(index) {
		document.geneForm.selectGeneNo.value =document.geneForm.name.options[document.geneForm.name.selectedIndex].value;
		document.geneForm.index.value =index;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/rollback/ShowIpsIdsBL" />";
        document.geneForm.submit();
	}

	function jumpAction(index) {
		document.geneForm.selectGeneNo.value =document.geneForm.name.options[document.geneForm.name.selectedIndex].value;
		document.geneForm.index.value =index;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/rollback/ShowActionBL" />";
        document.geneForm.submit();
	}

	<%-- 20110506 ktakenaka@PROSITE add start --%>
	function jumpUrlFilter(index) {
		document.geneForm.selectGeneNo.value =document.geneForm.name.options[document.geneForm.name.selectedIndex].value;
		document.geneForm.index.value =index;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/rollback/ShowUrlFilterBL" />";
        document.geneForm.submit();
	}

	function jumpVirusCheck(index) {
		document.geneForm.selectGeneNo.value =document.geneForm.name.options[document.geneForm.name.selectedIndex].value;
		document.geneForm.index.value =index;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/rollback/ShowVirusCheckBL" />";
        document.geneForm.submit();
	}

	function jumpSpyware(index) {
		document.geneForm.selectGeneNo.value =document.geneForm.name.options[document.geneForm.name.selectedIndex].value;
		document.geneForm.index.value =index;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/rollback/ShowSpywareBL" />";
        document.geneForm.submit();
	}
	<%-- 20110506 ktakenaka@PROSITE add end --%>
	<%-- 20130901 add start kkato@PROSITE --%>
	function jumpDisable(index) {
		document.geneForm.selectGeneNo.value =document.geneForm.name.options[document.geneForm.name.selectedIndex].value;
		document.geneForm.index.value =index;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/rollback/ShowDisableBL" />";
        document.geneForm.submit();
	}
	<%-- 20130901 add end   kkato@PROSITE --%>

	function popup() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		wd = window.open(
		       "RollbackPopupSCR.do",
		       "childWin",
		       "width=400,height=100",
		       "resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no"
		   );
		wd.focus();
	}

	function unLoadWindow()
	{
		if( wd != null ) {
			wd.close();
			wd = null;
		}
	}

	function listClick( id_tr , index ){
		if(document.getElementById) {
			var obj = document.getElementById("id_customers");
			var TRs = obj.getElementsByTagName("tr");
			if( lastIndex % 2 ) {
				TRs[lastIndex].className = "odd";
			} else {
				TRs[lastIndex].className = "";
			}
			document.getElementById(id_tr).className = "selected";
			lastIndex = index;
		}
	}
	-->
	</script>
</head>
<body onunload="unLoadWindow()">
	<div id="content"><!-- InstanceBeginEditable name="content" -->
		<div id="blockRollback">
		<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ロールバック</p>
		<h3>ロールバック</h3>
			<form name="geneForm" method="post" >

				<p>ロールバック対象の設定を選択してください。

					<bean:define id="seGeneNo" name="_RollbackForm" property="selectGeneNo"/>

					<html:select property="name" value="<%= seGeneNo.toString() %>" onchange="jump()">
						<logic:iterate id="geneBean" name="_RollbackForm" property="generationList">
							<bean:define id="geneNo" name="geneBean" property="generationNo"/>
							<html:option value="<%= geneNo.toString() %>"> <bean:write name="geneBean" property="geneUpdateStr"/></html:option>
						</logic:iterate>
					</html:select>
				</p>


				<input type="hidden" name="selectGeneNo" value="0"/>
				<input type="hidden" name="index"/>
				<table>
					<thead>
						<tr class="odd">
							<th scope="col" abbr="No">No</th>
							<th scope="col" abbr="Name">ポリシー名</th>
<%-- 20110622 ktakenaka@PROSITE del
							<th scope="col" abbr="Source Zone">通信元ゾーン</th>
							<th scope="col" abbr="Destination Zone">通信先ゾーン</th>
							<th scope="col" abbr="Source Address">通信元アドレス</th>
							<th scope="col" abbr="Destination Address">通信先アドレス</th>
--%>
							<th scope="col" abbr="Source Zone">通信元<br/>ゾーン</th>
							<th scope="col" abbr="Destination Zone">通信先<br/>ゾーン</th>
							<th scope="col" abbr="Source Address">通信元<br/>アドレス</th>
							<th scope="col" abbr="Destination Address">通信先<br/>アドレス</th>
							<th scope="col" abbr="Applications">アプリケーション</th>
<%-- 20110506 ktakenaka@PROSITE add start --%>
							<logic:match name="USER_VALUE_OBJECT"  property="urlFilteringFlg" value="1" >
								<th scope="col" abbr="URLFiltering">Web<br/>フィルター</th>
							</logic:match>
							<th scope="col" abbr="Virus Check">Webウィルス<br/>チェック</th>
							<th scope="col" abbr="AntiSpyware">スパイウェア<br/>チェック</th>
<%-- 20110506 ktakenaka@PROSITE add end --%>
							<th scope="col" abbr="Service">サービス</th>
							<th scope="col" abbr="IPS/IDS">IPS/<br/>IDS</th>
							<th scope="col" abbr="Action">アクション</th>
<%-- 20130901 add start kkato@PROSITE --%>
							<th scope="col" abbr="Disable">有効/無効</th>
<%-- 20130901 add end   kkato@PROSITE --%>
						</tr>
					</thead>
					<tbody id="id_customers">
						<logic:iterate id="policyBean" name="_RollbackForm" property="policyList"  indexId="index">
							<tr id="id_tr<bean:write name="index"/>" <%= index % 2 == 0 ? "" : "class='odd'" %>
							    onclick="listClick('id_tr<bean:write name="index"/>',<%= index %>);" >
								<td><bean:write name="policyBean" property="lineNo"/></td>
								<td><a href="javascript:jumpName(<%= index %>)"><bean:write name="policyBean" property="name"/></a></td>
									<td>
										<logic:iterate id="srcZoneBean" name="policyBean" property="sourceZoneList">
											<logic:notEqual name="srcZoneBean" property="name" value="any">
												<a href="javascript:jumpSource(<%= index %>)"><bean:write name="srcZoneBean" property="name"/></a>
											</logic:notEqual>
											<logic:equal name="srcZoneBean" property="name" value="any">
												<bean:write name="srcZoneBean" property="name"/>
											</logic:equal>
										</logic:iterate>
									</td>
									<td>
										<logic:iterate id="dstZoneBean" name="policyBean" property="destinationZoneList" >
											<logic:notEqual name="dstZoneBean" property="name" value="any">
												<a href="javascript:jumpDestination(<%= index %>)"><bean:write name="dstZoneBean" property="name"/></a>
											</logic:notEqual>
											<logic:equal name="dstZoneBean" property="name" value="any">
												<bean:write name="dstZoneBean" property="name"/>
											</logic:equal>
										</logic:iterate>
									</td>
									<td>
										<logic:iterate id="srcAddressBean" name="policyBean" property="sourceAddressList">
											<logic:notEqual name="srcAddressBean" property="name" value="any">
												<a href="javascript:jumpSrcAddress(<%= index %>)"><bean:write name="srcAddressBean" property="name"/></a>
											</logic:notEqual>
											<logic:equal name="srcAddressBean" property="name" value="any">
												<bean:write name="srcAddressBean" property="name"/>
											</logic:equal>
										</logic:iterate>
									</td>
									<td>
										<logic:iterate id="dstAddressBean" name="policyBean" property="destinationAddressList">
											<logic:notEqual name="dstAddressBean" property="name" value="any">
												<a href="javascript:jumpDstAddress(<%= index %>)"><bean:write name="dstAddressBean" property="name"/></a>
											</logic:notEqual>
											<logic:equal name="dstAddressBean" property="name" value="any">
												<bean:write name="dstAddressBean" property="name"/>
											</logic:equal>
										</logic:iterate>
									</td>
									<td>
										<logic:iterate id="appliBean" name="policyBean" property="applicationList">
											<logic:notEqual name="appliBean" property="name" value="any">
												<a href="javascript:jumpAppli(<%= index %>)"><bean:write name="appliBean" property="name"/></a>
											</logic:notEqual>
											<logic:equal name="appliBean" property="name" value="any">
												<bean:write name="appliBean" property="name"/>
											</logic:equal>
											</logic:iterate>
									</td>
<%-- 20110506 ktakenaka@PROSITE add start --%>
<%-- 20130901 kato@PROSITE mod start --%>
									<logic:match name="USER_VALUE_OBJECT"  property="urlFilteringFlg" value="1" >
									<td>
										<a href="javascript:jumpUrlFilter(<%= index %>)"><bean:write name="policyBean" property="urlFilterName"/></a>
									</td>
									</logic:match>
<%-- 20130901 kato@PROSITE mod end --%>
									<td>
										<logic:equal name="policyBean" property="virusCheckFlg" value="default">
											<a href="javascript:jumpVirusCheck(<%= index %>)">○</a>
										</logic:equal>
										<logic:equal name="policyBean" property="virusCheckFlg" value="none">
											<a href="javascript:jumpVirusCheck(<%= index %>)">×</a>
										</logic:equal>
									</td>
									<td>
										<logic:equal name="policyBean" property="spywareFlg" value="default">
											<a href="javascript:jumpSpyware(<%= index %>)">○</a>
										</logic:equal>
										<logic:equal name="policyBean" property="spywareFlg" value="none">
											<a href="javascript:jumpSpyware(<%= index %>)">×</a>
										</logic:equal>
									</td>
<%-- 20110506 ktakenaka@PROSITE add end --%>
									<td>
										<logic:iterate id="serviceBean" name="policyBean" property="serviceList">

											<logic:notEqual name="serviceBean" property="name" value="any">
												<logic:notEqual name="serviceBean" property="name" value="application-default">
													<a href="javascript:jumpService(<%= index %>)"><bean:write name="serviceBean" property="name"/></a>
												</logic:notEqual>
											</logic:notEqual>
											<logic:equal name="serviceBean" property="name" value="any">
												<bean:write name="serviceBean" property="name"/>
											</logic:equal>
											<logic:equal name="serviceBean" property="name" value="application-default">
												<bean:write name="serviceBean" property="name"/>
											</logic:equal>
										</logic:iterate>
									</td>
									<td>
										<logic:equal name="policyBean" property="ids_ips" value="default">
											<a href="javascript:jumpIPS_IDS(<%= index %>)">○</a>
										</logic:equal>
										<logic:equal name="policyBean" property="ids_ips" value="none">
											<a href="javascript:jumpIPS_IDS(<%= index %>)">×</a>
										</logic:equal>
									</td>
								<td><a href="javascript:jumpAction(<%= index %>)"><bean:write name="policyBean" property="action"/></a></td>
<%-- 20130901 add start kkato@PROSITE --%>
									<td><a href="javascript:jumpDisable(<%= index %>)"><bean:write name="policyBean" property="disableFlg"/></a></td>
<%-- 20130901 add end   kkato@PROSITE --%>
							</tr>
						</logic:iterate>
					</tbody>
				</table>
				<ul id="button">
					<li><input type="button" value="ロールバックの実行" onclick="popup()" /></li>
				</ul>
			</form>


		<!-- end blockRollback --></div>
	<!-- InstanceEndEditable -->
	<!-- end #content --></div>


</body>

<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
