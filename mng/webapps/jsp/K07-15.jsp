<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お客様企業メイン" />
<bean:define id="titleScreenName" scope="page" value="ロールバック" />
<bean:define id="screenName" scope="page" value="Destination Address参照" />
<bean:define id="screenID" scope="page" value="K07-15" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-03.jspf"%>
<script>
	var mstNum = 0;
	var addressMstList = new Array();
</script>
<body>
	<div id="content">
		<div id="editDstaddress">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ロールバック &raquo; 通信先アドレス参照</p>
			<h3>通信先アドレス参照</h3>
			<ts:form action="/admin/fwsetting/rollback/RollbackBL" method="post" >
				<div id="anyOrSelect">
					<ul>
						<bean:size id="addressSize" name="_RollbackForm" property="addressMasterList" />
						<logic:equal name="_RollbackForm" property="addressName" value="any">
							<li><input name="AddressRadio" type="radio" checked="checked" disabled ="disabled"/>any</li>
							<li><input name="AddressRadio" type="radio" disabled ="disabled"/>選択する</li>
						</logic:equal>
						<logic:notEqual name="_RollbackForm" property="addressName" value="any">
							<li><input name="AddressRadio" type="radio" disabled ="disabled"/>any</li>
							<li><input name="AddressRadio" type="radio" checked="checked" disabled ="disabled"/>選択する</li>
						</logic:notEqual>

					</ul>
				</div>
				<div id="available">
					<h4>&raquo; オブジェクトの選択</h4>
					<ul id="addressUl">
							<logic:iterate id="addressMstBean" name="_RollbackForm" property="addressMasterList"  indexId="index">
								<li>
									<input name="checkAddress" type="checkbox" disabled ="disabled"/>
									<logic:equal name="addressMstBean" property="objectType" value="0">
										<img alt="" src="../../../img/group_off.png" width="13" height="13"/>
									</logic:equal>
									<logic:equal name="addressMstBean" property="objectType" value="1">
										<img alt="" src="../../../img/group_on.png" width="13" height="13"/>
									</logic:equal>
									<bean:write name="addressMstBean"  property="name"/><br/>
								</li>
									<script type="text/javascript">
											{
												addressMstList[mstNum] = "<bean:write name='addressMstBean'  property='name'/>";
												mstNum++;
											}
									</script>
							</logic:iterate>
							<logic:iterate id="addressBean" name="_RollbackForm" property="addressList"  indexId="index">
								<logic:equal name="addressBean" property="objectType" value="2">
									<li>
										<input name="checkAddAddress" type="checkbox" checked="checked" disabled ="disabled"/>
										<img alt="" src="../../../img/group_off.png" width="13" height="13"/>
										<bean:write name="addressBean"  property="name"/><br/>
									</li>
								</logic:equal>
							</logic:iterate>
							<logic:iterate id="addressBean" name="_RollbackForm" property="addressList"  indexId="index">
								<script type="text/javascript">
									{
										for(i=0; i<addressMstList.length; i++) {
											if(addressMstList[i] == "<bean:write name='addressBean'  property='name'/>") {
												document._RollbackForm.checkAddress[i].checked=true;
											}
										}
									}
								</script>
							</logic:iterate>
					</ul>
				<!-- end #available --></div>
				<ul id="button">
					<li><ts:submit value="Cancel" /></li>
				</ul>
			</ts:form>
			<!-- editDstaddress -->
		</div>
	<!-- end #content --></div>
</body>

<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
