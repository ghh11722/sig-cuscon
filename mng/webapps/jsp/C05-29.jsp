<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>
<%@ taglib uri="/struts-nested" prefix="nested" %>

<bean:define id="titleUsecaseName" scope="page" value="XXXXX" />
<bean:define id="titleScreenName" scope="page" value="XXXXX" />
<bean:define id="screenName" scope="page" value="XXXXX" />
<bean:define id="screenID" scope="page" value="C05-29" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>

<head>
	<script type="text/javascript">
	<!--
	var submit_flag = 0;
	document.onkeydown = KeyEvent;
	function KeyEvent(e){
	    var pressKey=event.keyCode;
	    if(pressKey == 13) {
		return false;
	    }
	}

	function searchData(){
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/EDT_SearchBL" />";
        document.geneForm.submit();

	}

	function updateData(){
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/UpdateApplicationFilterBL" />";
        document.geneForm.submit();
	}

	function clearData(){
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/EDT_ClearBL" />";
        document.geneForm.submit();
	}
	function cancelClick() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/ApplicationFiltersBL" />";
	    document.geneForm.submit();
	}
	-->
	</script>
</head>
<body>
	<div id="content">
	<!-- InstanceBeginEditable name="content" -->
		<div id="objectApplicationfilters">
		<p id="pankuzu">Top &raquo; FW設定機能 &raquo; オブジェクト設定 &raquo; アプリケーションフィルター設定 &raquo; アプリケーションフィルター編集</p>
		<h3>アプリケーションフィルター編集</h3>
		<form name="geneForm" action="" method="post">
			<p>オブジェクト名: <html:text name="_ApplicationFilterForm" property="name" /></p>
			<html:hidden name="_ApplicationFilterForm" property="newName" />
			<html:hidden name="_ApplicationFilterForm" property="oldName" />

			<p id="filterReset"><html:button property="reset" value="フィルターの解除" onclick="javascript:clearData()" /></p>
			<div id="applicationeditFilter">
			<bean:define id="filterbean" name="_ApplicationFilterForm" property="filterList" />
			<div id="categoly">
			<h4>カテゴリー</h4>
			<div id="categolyList">
			<ul>
				<logic:iterate id="categoryList" name="filterbean" property="categoryList" indexId="index" >
					<li>
						<html:checkbox name="categoryList" property="chkFlg" indexed="true"	onclick="javascript:searchData()" />
						<bean:write name="categoryList" property="name"/>
						<html:hidden name="categoryList" property="name" indexed="true"/>
						<html:hidden name="categoryList" property="seqNo" indexed="true"/>
					</li>
				</logic:iterate>
			</ul>
			<!-- end #categoryList --></div>
			<!-- #category --></div>
			<div id="subcategory">
			<h4>サブカテゴリー</h4>
			<div id="subcategoryList">
			<ul>
				<logic:iterate id="subCategoryList" name="filterbean" property="subCategoryList" indexId="index" >
					<li>
						<html:checkbox name="subCategoryList" property="chkFlg" indexed="true" onclick="javascript:searchData()" />
						<bean:write name="subCategoryList" property="name"/>
						<html:hidden name="subCategoryList" property="name" indexed="true"/>
						<html:hidden name="subCategoryList" property="seqNo" indexed="true"/>
					</li>
				</logic:iterate>
			</ul>
			<!-- end #subcategoryList --></div>
			<!-- #subcategoly --></div>
			<div id="technology">
			<h4>テクノロジー</h4>
			<div id="technologyList">
			<ul>
				<logic:iterate id="technologyList" name="filterbean" property="technologyList" indexId="index" >
					<li>
						<html:checkbox name="technologyList" property="chkFlg" indexed="true" onclick="javascript:searchData()" />
						<bean:write name="technologyList" property="name"/>
						<html:hidden name="technologyList" property="name" indexed="true"/>
						<html:hidden name="technologyList" property="seqNo" indexed="true"/>
					</li>
				</logic:iterate>
			</ul>
			<!-- end #technology --></div>
			<!-- #technology --></div>
			<div id="risk">
			<h4>リスク</h4>
			<div id="riskList">
				<ul>
					<logic:iterate id="riskList" name="filterbean" property="riskList" indexId="index" >
						<li>
							<html:checkbox name="riskList" property="chkFlg" indexed="true"	onclick="javascript:searchData()" />
							<bean:write name="riskList" property="name"/>
							<html:hidden name="riskList" property="name" indexed="true"/>
						</li>
					</logic:iterate>
				</ul>
			<!-- end #riskList --></div>
			<!-- #risk --></div>
			<!-- end #applicationeditFilter --></div>
			<div id="filterResult">
				<table>
				<thead>
					<tr class="odd">
						<th scope="col">アプリケーション名</th>
						<th scope="col">カテゴリー</th>
						<th scope="col">サブカテゴリー</th>
						<th scope="col">テクノロジー</th>
						<th scope="col">リスク</th>
					</tr>
				</thead>
				<tbody>
					<logic:notEmpty name="_ApplicationFilterForm"  property="applicationList" >
						<logic:iterate id="applicationBean" name="_ApplicationFilterForm" property="applicationList"  indexId="index">
							<tr <%=index % 2 == 0 ? "" : "class='odd'"%>>
								<td><bean:write name="applicationBean" property="name"/></td>
								<td><bean:write name="applicationBean" property="category"/></td>
								<td><bean:write name="applicationBean" property="subCategory"/></td>
								<td><bean:write name="applicationBean" property="technology"/></td>
								<td><bean:write name="applicationBean" property="risk"/></td>
							</tr>
						</logic:iterate>
					</logic:notEmpty>
				</tbody>
				</table>
			<!-- #filterResult --></div>
			<ul id="button">
				<li><html:button property="ok" value="OK" onclick="javascript:updateData()"/></li>
				<li><html:button property="cancel" value="Cancel" onclick="cancelClick()"/></li>
			</ul>
		</form>
		<!-- end #objectApplicationgroup --></div>
	<!-- InstanceEndEditable -->
	<!-- end #content --></div>
</body>
<%@ include file="C-FOOTER-01.jspf"%>
<!-- InstanceEnd --></html:html>
