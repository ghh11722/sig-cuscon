<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="FW接続情報管理機能" />
<bean:define id="titleScreenName" scope="page" value="メイン画面" />
<bean:define id="screenName" scope="page" value="KDDI管理者メイン" />
<bean:define id="screenID" scope="page" value="K03-01" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true" lang="true">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-01.jspf"%>

<script language="JavaScript" type="text/JavaScript">
<!--
	function listOver( id_tr, id_radio  ){
		if(document.getElementById) {
			document.getElementById(id_tr).className = "mouseover";
			if( document.getElementById(id_radio).checked == true ){
				document.getElementById(id_tr).className = "selected";
			}
		}
	}
	function listOut( id_tr, id_radio, style_class ){
		if(document.getElementById) {
			document.getElementById(id_tr).className = style_class;
			if( document.getElementById(id_radio).checked == true ){
				document.getElementById(id_tr).className = "selected";
			}
		}
	}
	function listClick( id_tr, id_radio ){
		if(document.getElementById) {
			// リスト再描画
			var obj = document.getElementById("id_customers");
			var TRs = obj.getElementsByTagName("tr");
			for (i=0; i<TRs.length; i++){
				if( i % 2 ) { TRs[i].className = "odd"; }
				else        { TRs[i].className = ""; }
			}
			// 選択項目の色変更
			document.getElementById(id_radio).checked = true;
			document.getElementById(id_tr).className = "selected";
		}
	}
	var submit_flag = 0;
	function submitForm() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;

		if(document.getElementById) {
			var elem = document.getElementById("progressDialog");
			elem.style.visibility = 'visible';
			elem = document.getElementById("loadingDialog");
			elem.style.visibility = 'visible';
			elem.style.width = document.documentElement.scrollWidth || document.body.scrollWidth;;
			elem.style.height = document.documentElement.scrollHeight || document.body.scrollHeight;
		}
		document._AdminMainForm.submit();
	}
	<%-- 20110510 ktakenaka@PROSITE add start --%>
	function jumpURLFiltering(selectedIndex, flg, id_loginid) {
		document._AdminMainForm.selectIndex.value = selectedIndex;
		document._AdminMainForm.action = "<html:rewrite action="/admin/EditURLFilteringBL" />";
		submitForm();
	}
	function jumpDmz(selectedIndex, flg) {
		document._AdminMainForm.selectIndex.value = selectedIndex;
		document._AdminMainForm.action = "<html:rewrite action="/admin/EditDmzBL" />";
		submitForm();
	}
	<%-- 20110510 ktakenaka@PROSITE add end --%>
//-->
</script>

<div id="content">
	<div id="top">
	<h3>お客様一覧</h3>
	<ts:form action="/admin/customer_choiceBL" method="post">
	<table class="list">
		<thead>
			<tr class="odd">
				<th scope="col" abbr="Check"><label></label></th>
				<th scope="col" abbr="User ID">ログインID</th>
				<th scope="col" abbr="Password">パスワード</th>
				<th scope="col" abbr="Firewall ID">ファイアウォール ID</th>
				<th scope="col" abbr="Virtual System">ヴァーチャルシステム(VSYS)</th>
<%-- 20110511 ktakenaka@PROSITE add start --%>
				<th scope="col" abbr="URL Filtering">Webフィルタ</th>
				<th scope="col" abbr="DMZ">DMZ</th>
<%-- 20110511 ktakenaka@PROSITE add end --%>
				<th scope="col" abbr="Last Login Date">最終ログイン日時</th>
				<th scope="col" abbr="Last Commit Date">最終コミット日時</th>
			</tr>
		</thead>
<logic:notEmpty name="_AdminMainForm" property="customers">
		<tbody id="id_customers">
<logic:iterate id="userBean" name="_AdminMainForm" property="customers" indexId="idx">
			<tr id="id_tr<bean:write name="idx"/>" <%= idx % 2 == 0 ? "" : "class='odd'" %>
			    onmouseover="listOver('id_tr<bean:write name="idx"/>','id_radio<bean:write name="idx"/>');"
			    onmouseout="listOut('id_tr<bean:write name="idx"/>','id_radio<bean:write name="idx"/>','<%= idx % 2 == 0 ? "" : "odd" %>');"
			    onclick="listClick('id_tr<bean:write name="idx"/>','id_radio<bean:write name="idx"/>');" >
				<td>
					<input type="radio"
					       id="id_radio<bean:write name="idx"/>"
					       name="customerLoginId"
					       value="<bean:write name="userBean" property="loginId"/>"
					       onclick="listClick('id_tr<bean:write name="idx"/>','id_radio<bean:write name="idx"/>');" />
				</td>
				<td><bean:write name="userBean" property="loginId"/></td>
				<td><bean:write name="userBean" property="loginPwd"/></td>
				<td><bean:write name="userBean" property="firewallId"/></td>
				<td><bean:write name="userBean" property="vsysId"/></td>
<%-- 20110511 ktakenaka@PROSITE add start --%>
				<td>
					<logic:equal name="userBean" property="urlFilteringFlg" value="1">
						<a href="javascript:jumpURLFiltering(<%= idx %>, 1)">○</a>
					</logic:equal>
					<logic:equal name="userBean" property="urlFilteringFlg" value="0">
						<a href="javascript:jumpURLFiltering(<%= idx %>, 0) ")>×</a>
					</logic:equal>
				</td>
				<td>
					<logic:equal name="userBean" property="dmzFlg" value="1">
						<a href="javascript:jumpDmz(<%= idx %>, 1)">○</a>
					</logic:equal>
					<logic:equal name="userBean" property="dmzFlg" value="0">
						<a href="javascript:jumpDmz(<%= idx %>, 0)">×</a>
					</logic:equal>
				</td>

<%-- 20110511 ktakenaka@PROSITE add end --%>
				<td><bean:write name="userBean" property="lastLoginDate"/></td>
				<td><bean:write name="userBean" property="lastCommitDate"/></td>
			</tr>
</logic:iterate>
		</tbody>
</logic:notEmpty>
	</table>
<logic:notEmpty name="_AdminMainForm" property="customers">
	<p>選択したお客様のFW設定画面にログインします。</p>
	<ul id="button">
		<li><html:button property="Login" value="ログインする" onclick="submitForm()" /></li>
	</ul>
</logic:notEmpty>
	<input type="hidden" name="selectIndex" value="0"/>
	</ts:form>
	</div>
	<div id="progressDialog" >
		<div id="loading" >NowLoading...</div>
	</div>
</div>
<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
