<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="メイン画面" />
<bean:define id="screenName" scope="page" value="企業管理者メイン" />
<bean:define id="screenID" scope="page" value="C05-20" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<script>
	<!--
		var submit_flag = 0;

		function okClick() {
			if( submit_flag == 1 ){ return; }
			submit_flag = 1;
			document.serviceForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/DelServiceGroupsBL" />";
		    document.serviceForm.submit();
		}

		function cancelClick() {
			if( submit_flag == 1 ){ return; }
			submit_flag = 1;
			document.serviceForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/ServiceGroupsBL" />";
		    document.serviceForm.submit();
		}
	-->
</script>
<body onunload="unLoadWindow()">
	<div id="content">
		<div id="objectServices">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; オブジェクト設定 &raquo; サービスグループ設定 &raquo; サービスグループ削除</p>
			<h3>サービスグループ削除</h3>
			<form action="" method="post" name="serviceForm">
				<table>
					<thead>
						<tr class="odd">
							<th scope="col">オブジェクト名</th>
							<th scope="col">メンバー数</th>
							<th scope="col">サービス</th>
						</tr>
					</thead>
					<logic:notEmpty name="_ServiceGroupsForm" property="serviceList">
					<tbody>
						<logic:iterate id="userBean" name="_ServiceGroupsForm" property="serviceList" indexId="idx">
						<tr <%=idx % 2 == 0 ? "" : "class='odd'"%>>
							<td><bean:write name="userBean" property="name" /></td>
							<td><bean:write name="userBean" property="members" /></td>
							<td><bean:write name="userBean" property="services" /></td>
						</tr>
						</logic:iterate>
					</tbody>
					</logic:notEmpty>
				</table>
				<p>上記の設定を削除してもよろしいですか？</p>
				<ul id="button">
					<li><input type="button" value="OK" onclick="okClick()" /></li>
					<li><input type="button" value="Cancel" onclick="cancelClick()"/></li>
				</ul>
			</form>
		<!-- end objectServices --></div>
	<!-- InstanceEndEditable -->
	<!-- end #content --></div>
</body>

<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
