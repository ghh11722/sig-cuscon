<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お客様企業メイン" />
<bean:define id="titleScreenName" scope="page" value="ポリシー一覧" />
<bean:define id="screenName" scope="page" value="ポリシー削除確認" />
<bean:define id="screenID" scope="page" value="K05-02" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-03.jspf"%>
<script>
	<!--
		var submit_flag = 0;

		function okClick() {
			if( submit_flag == 1 ){ return; }
			submit_flag = 1;
		    document._PolicyForm.submit();
		}

		function cancelClick() {
			if( submit_flag == 1 ){ return; }
			submit_flag = 1;
			document._PolicyForm.action = "<html:rewrite action="/admin/fwsetting/policysetting/PolicyBL" />";
		    document._PolicyForm.submit();
		}
	-->
</script>
<body>
	<div id="content">
		<div id="delPolicy">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ポリシー設定 &raquo; ポリシー削除</p>
			<h3>ポリシー削除</h3>
			<ts:form action="/admin/fwsetting/policysetting/DelPolicyBL" method="post">

				<table>
					<thead>
						<tr class="odd">
							<th scope="col" abbr="No">No</th>
								<th scope="col" abbr="Name">ポリシー名</th>
<%-- 20110622 ktakenaka@PROSITE del
								<th scope="col" abbr="Source Zone">通信元ゾーン</th>
								<th scope="col" abbr="Destination Zone">通信先ゾーン</th>
								<th scope="col" abbr="Source Address">通信元アドレス</th>
								<th scope="col" abbr="Destination Address">通信先アドレス</th>
--%>
								<th scope="col" abbr="Source Zone">通信元<br/>ゾーン</th>
								<th scope="col" abbr="Destination Zone">通信先<br/>ゾーン</th>
								<th scope="col" abbr="Source Address">通信元<br/>アドレス</th>
								<th scope="col" abbr="Destination Address">通信先<br/>アドレス</th>
								<th scope="col" abbr="Applications">アプリケーション</th>
<%-- 20110620 ktakenaka@PROSITE add end --%>
								<logic:match name="USER_VALUE_OBJECT"  property="urlFilteringFlg" value="1" >
									<th scope="col" abbr="URL Filtering">Web<br/>フィルタ</th>
								</logic:match>
								<th scope="col" abbr="Virus Check">Webウィルス<br/>チェック</th>
								<th scope="col" abbr="Anti Spyware">スパイウェア<br/>チェック</th>
<%-- 20110620 ktakenaka@PROSITE add end --%>
								<th scope="col" abbr="Service">サービス</th>
								<th scope="col" abbr="IPS/IDS">IPS/IDS</th>
								<th scope="col" abbr="Action">アクション</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<bean:define id="policyBean" name="_PolicyForm" property="selectPolicy"/>
							<td><bean:write name="policyBean" property="lineNo"/></td>
							<td><bean:write name="policyBean" property="name"/></td>
							<bean:define id="policyName" name="policyBean" property="name" />

							<html:hidden name="_PolicyForm" property="name" value="<%= policyName.toString() %>"></html:hidden>
							<td>
								<logic:iterate id="srcZoneBean" name="policyBean" property="sourceZoneList">
									<bean:write name="srcZoneBean" property="name"/>
								</logic:iterate>
							</td>
							<td>
								<logic:iterate id="dstZoneBean" name="policyBean" property="destinationZoneList" >
										<bean:write name="dstZoneBean" property="name"/>
								</logic:iterate>
							</td>
							<td>
								<logic:iterate id="srcAddressBean" name="policyBean" property="sourceAddressList">
									<bean:write name="srcAddressBean" property="name"/>
								</logic:iterate>
							</td>
							<td>
								<logic:iterate id="dstAddressBean" name="policyBean" property="destinationAddressList">
									<bean:write name="dstAddressBean" property="name"/>
								</logic:iterate>
							</td>
							<td>
								<logic:iterate id="appliBean" name="policyBean" property="applicationList">
									<bean:write name="appliBean" property="name"/>
								</logic:iterate>
							</td>
<%-- 20110620 ktakenaka@PROSITE add start --%>
<%-- 20170322 UPDATE Yamada@Plum-systems.inc add start --%>
							<logic:match name="USER_VALUE_OBJECT"  property="urlFilteringFlg" value="1" >
							<td><bean:write name="policyBean" property="urlFilterName"/></td>
							</logic:match>
<%-- 20170322 UPDATE Yamada@Plum-systems.inc add end --%>
							<td>
								<logic:equal name="policyBean" property="virusCheckFlg" value="default">○</logic:equal>
								<logic:equal name="policyBean" property="virusCheckFlg" value="none">×</logic:equal>
							</td>
							<td>
								<logic:equal name="policyBean" property="spywareFlg" value="default">○</logic:equal>
								<logic:equal name="policyBean" property="spywareFlg" value="none">×</logic:equal>
							</td>
<%-- 20110620 ktakenaka@PROSITE add end --%>
							<td>
								<logic:iterate id="serviceBean" name="policyBean" property="serviceList">
									<bean:write name="serviceBean" property="name"/>
								</logic:iterate>
							</td>
							<td>
								<logic:equal name="policyBean" property="ids_ips" value="default">
									○
								</logic:equal>
								<logic:equal name="policyBean" property="ids_ips" value="none">
									×
								</logic:equal>
							</td>
							<td><bean:write name="policyBean" property="action"/></td>
						</tr>
					</tbody>
				</table>
				<p>上記のポリシー設定を削除してもよろしいですか？</p>
				<ul id="button">
					<li><input type="button" value="OK" onclick="okClick()"/></li>
					<li><input type="button" value="Cancel" onclick="cancelClick()"/></li>
				</ul>
			</ts:form>
		<!-- end #delPolicy -->
		</div>
	<!-- end #content --></div>
</body>

<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
