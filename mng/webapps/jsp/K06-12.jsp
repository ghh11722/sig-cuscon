<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>
<%@ taglib uri="/struts-nested" prefix="nested" %>

<bean:define id="titleUsecaseName" scope="page" value="XXXXX" />
<bean:define id="titleScreenName" scope="page" value="XXXXX" />
<bean:define id="screenName" scope="page" value="xxxxx" />
<bean:define id="screenID" scope="page" value="K06-12" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-03.jspf"%>
<head>
	<script type="text/javascript">
	<!--

	var chkNum = 0;
	var indexList = new Array();

	function getCheck(obj,index){
		if (obj.checked) {
			chkNum++;
			indexList.push(index);
		} else {
			chkNum--;
			for(i=0;i<indexList.length;i++) {
				if(indexList[i]==index) {
					indexList.splice(i, 1);
					i--;
				}
			}
		}
	}

	function checkDel(){
		var str = "";
		if(chkNum!=0) {
			for(i=0;i<indexList.length;i++) {
				if(i==0) {
					str = indexList[i];
				} else {
					str = str + "," + indexList[i];
				}
			}
			document.geneForm.checkIndex.value = str;
			document.geneForm.action = "<html:rewrite action="/admin/fwsetting/objectsetting/ApplicationFiltersConfirmBL" />";
	        document.geneForm.submit();
		}
	}

	function addData() {
		document.geneForm.action = "<html:rewrite action="/admin/fwsetting/objectsetting/AddApplicationFiltersBL" />";
        document.geneForm.submit();

	}
	var lastIndex = 0;
	function listClick( id_tr , index ){
		if(document.getElementById) {
			var obj = document.getElementById("id_customers");
			var TRs = obj.getElementsByTagName("tr");
			if( lastIndex % 2 ) {
				TRs[lastIndex].className = "odd";
			} else {
				TRs[lastIndex].className = "";
			}
			document.getElementById(id_tr).className = "selected";
			lastIndex = index;
		}
	}
	-->
	</script>
</head>
<body>
	<div id="content">
		<div id="objectApplicationfilters">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; オブジェクト設定 &raquo; アプリケーションフィルター設定</p>
			<h3>アプリケーションフィルター設定</h3>
			<form name="geneForm" method="post">
				<ul id="button">
					<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<li><html:button property="add" value="オブジェクトの追加" disabled="true"/></li>
						<li><html:button property="delete" value="オブジェクトの削除" disabled="true"/></li>
					</logic:notMatch>
					<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<li><html:button property="add" value="オブジェクトの追加" onclick="javascript:addData()"/></li>
						<li><html:button property="delete" value="オブジェクトの削除" onclick="javascript:checkDel()" /></li>
					</logic:match>
				</ul>
				<input type="hidden" name="checkIndex" />
				<input type="hidden" name="name"  value=""/>
				<table>
					<thead>
						<tr class="odd">
							<th scope="col">&nbsp;</th>
							<th scope="col">オブジェクト名</th>
							<th scope="col">カテゴリー</th>
							<th scope="col">サブカテゴリー</th>
							<th scope="col">テクノロジー</th>
							<th scope="col">リスク</th>
						</tr>
					</thead>
					<logic:notEmpty name="_ApplicationFilterForm" property="applicationFilterList">
						<tbody id="id_customers">
						<logic:iterate id="userBean" name="_ApplicationFilterForm" property="applicationFilterList" indexId="index" >
							<tr id="id_tr<bean:write name="index"/>" <%= index % 2 == 0 ? "" : "class='odd'" %>
							    onclick="listClick('id_tr<bean:write name="index"/>',<%= index %>);" >
								<td>
									<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
										<input type="checkbox" disabled="disabled" />
									</logic:notMatch>
									<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
										<input type="checkbox" onclick="getCheck(this, <%= index %>)"/>
									</logic:match>
								</td>
								<td>
									<bean:define id="name" name="userBean" property="name" type="java.lang.String"/>
									<html:link action="/admin/fwsetting/objectsetting/EditApplicationFilterBL" paramId="name" paramName="name">
										<bean:write name="userBean" property="name"/>
									</html:link>
									<html:hidden name="userBean" property="name"/>
								</td>
								<td>
									<logic:iterate id="categoryList" name="userBean" property="categoryList" >
										<bean:write name="categoryList" property="name"/>
										<html:hidden name="categoryList" property="name" indexed="true"/>
									</logic:iterate>
								</td>
								<td>
									<logic:iterate id="subCategoryList" name="userBean" property="subCategoryList" >
										<bean:write name="subCategoryList" property="name"/>
										<html:hidden name="subCategoryList" property="name" indexed="true"/>
									</logic:iterate>
								</td>
								<td>
									<logic:iterate id="technologyList" name="userBean" property="technologyList" >
										<bean:write name="technologyList" property="name"/>
										<html:hidden name="technologyList" property="name" indexed="true"/>
									</logic:iterate>
								</td>
								<td>
									<logic:iterate id="riskList" name="userBean" property="riskList" >
										<bean:write name="riskList" property="name"/>
										<html:hidden name="riskList" property="name" indexed="true"/>
									</logic:iterate>
								</td>
							</tr>
						</logic:iterate>
						</tbody>
					</logic:notEmpty>
				</table>
				<ul id="button">
					<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<li><html:button property="add" value="オブジェクトの追加" disabled="true"/></li>
						<li><html:button property="delete" value="オブジェクトの削除" disabled="true"/></li>
					</logic:notMatch>
					<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<li><html:button property="add" value="オブジェクトの追加" onclick="javascript:addData()"/></li>
						<li><html:button property="delete" value="オブジェクトの削除" onclick="javascript:checkDel()" /></li>
					</logic:match>
				</ul>
				<logic:notEmpty name="_ApplicationFilterForm" property="message">
					<script type="text/javascript">
					{
						alert("<bean:write name="_ApplicationFilterForm" property="message"/>");
						document._ScheduleForm.message.value = "";
					}
					</script>
				</logic:notEmpty>
			</form>
	<!-- end objectApplicationfilters --></div>
	<!-- end #content --></div>
</body>
<%@ include file="K-FOOTER-01.jspf"%>
<!-- InstanceEnd --></html:html>
