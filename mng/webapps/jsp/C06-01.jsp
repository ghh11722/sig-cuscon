<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="コミット" />
<bean:define id="screenName" scope="page" value="コミット" />
<bean:define id="screenID" scope="page" value="C06-01" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<head>
	<script type="text/javascript">
	<!--
		var submit_flag = 0;
		function popup() {
			if( submit_flag == 1 ){ return; }
			submit_flag = 1;
			wd = window.open(
			       "CommitPopupSCR.do",
			       "childWin",
			        "width=400,height=120",
					"resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no"
			   );
			wd.focus();
		}

		function unLoadWindow() {
			if( wd != null ) {
				wd.close();
				wd = null;
			}
		}
	-->
	</script>
</head>
<body onunload="unLoadWindow()">
	<div id="content">
		<div id="commit">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; コミット</p>
			<h3>コミット</h3>
			<form action="" method="post">
				<ul id="commitCheck">

					<bean:size id="rulesSize" name="_CommitForm" property="rules"/>
					<logic:notEqual name="rulesSize" value="0">
						<li>
							<h4>Security Rules</h4>
								<dl>
									<logic:iterate id="userBean" name="_CommitForm" property="rules">
										<dt><bean:write name="userBean" property="name"/></dt><dd><bean:write name="userBean" property="modName"/></dd>
									</logic:iterate>
								</dl>
						</li>
					</logic:notEqual>

					<bean:size id="addressSize" name="_CommitForm" property="addresses"/>
					<logic:notEqual name="addressSize" value="0">
					<li>
						<h4>Addresses</h4>
						<dl>
							<logic:iterate id="userBean" name="_CommitForm" property="addresses">
								<dt><bean:write name="userBean" property="name"/></dt><dd><bean:write name="userBean" property="modName"/></dd>
							</logic:iterate>
						</dl>
					</li>
					</logic:notEqual>

					<bean:size id="addressGroupsSize" name="_CommitForm" property="addressGroups"/>
					<logic:notEqual name="addressGroupsSize" value="0">
						<li>
							<h4>Address Groups</h4>
							<dl>
								<logic:iterate id="userBean" name="_CommitForm" property="addressGroups">
										<dt><bean:write name="userBean" property="name"/></dt><dd><bean:write name="userBean" property="modName"/></dd>
								</logic:iterate>
							</dl>
						</li>
					</logic:notEqual>

					<bean:size id="applicationGroupsSize" name="_CommitForm" property="applicationGroups"/>
					<logic:notEqual name="applicationGroupsSize" value="0">
						<li>
							<h4>Application Groups</h4>
							<dl>
								<logic:iterate id="userBean" name="_CommitForm" property="applicationGroups">
										<dt><bean:write name="userBean" property="name"/></dt><dd><bean:write name="userBean" property="modName"/></dd>
								</logic:iterate>
							</dl>
						</li>
					</logic:notEqual>

					<bean:size id="applicationFiltersSize" name="_CommitForm" property="applicationFilters"/>
					<logic:notEqual name="applicationFiltersSize" value="0">
						<li>
							<h4>Application Filters</h4>
							<dl>
								<logic:iterate id="userBean" name="_CommitForm" property="applicationFilters">
										<dt><bean:write name="userBean" property="name"/></dt><dd><bean:write name="userBean" property="modName"/></dd>
								</logic:iterate>
							</dl>
						</li>
					</logic:notEqual>

					<bean:size id="servicesSize" name="_CommitForm" property="services"/>
					<logic:notEqual name="servicesSize" value="0">
						<li>
							<h4>Services</h4>
							<dl>
								<logic:iterate id="userBean" name="_CommitForm" property="services">
										<dt><bean:write name="userBean" property="name"/></dt><dd><bean:write name="userBean" property="modName"/></dd>
								</logic:iterate>
							</dl>
						</li>
					</logic:notEqual>

					<bean:size id="serviceGroupsSize" name="_CommitForm" property="serviceGroups"/>
					<logic:notEqual name="serviceGroupsSize" value="0">
						<li>
							<h4>Service Groups</h4>
							<dl>
								<logic:iterate id="userBean" name="_CommitForm" property="serviceGroups">
										<dt><bean:write name="userBean" property="name"/></dt><dd><bean:write name="userBean" property="modName"/></dd>
								</logic:iterate>
							</dl>
						</li>
					</logic:notEqual>

					<bean:size id="schedulesSize" name="_CommitForm" property="schedules"/>
					<logic:notEqual name="schedulesSize" value="0">
						<li>
							<h4>Schedules</h4>
							<dl>
								<logic:iterate id="userBean" name="_CommitForm" property="schedules">
										<dt><bean:write name="userBean" property="name"/></dt><dd><bean:write name="userBean" property="modName"/></dd>
								</logic:iterate>
							</dl>
						</li>
					</logic:notEqual>

<%-- 20110506 ktakenaka@PROSITE add start --%>
					<bean:size id="urlFiltersSize" name="_CommitForm" property="urlFilters"/>
					<logic:notEqual name="urlFiltersSize" value="0">
						<li>
							<h4>Web Filter</h4>
							<dl>
								<logic:iterate id="userBean" name="_CommitForm" property="urlFilters">
										<dt><bean:write name="userBean" property="name"/></dt><dd><bean:write name="userBean" property="modName"/></dd>
								</logic:iterate>
							</dl>
						</li>
					</logic:notEqual>
<%-- 20110506 ktakenaka@PROSITE add end --%>

				</ul>
				<ul id="button">
					<li><input type="button" value="コミットの実行" onclick="popup()" /></li>
				</ul>
			</form>
	<!-- end commit --></div>
	<!-- end content --></div>
</body>

<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
