<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="ロールバック" />
<bean:define id="screenName" scope="page" value="ロールバック対象なしエラー画面" />
<bean:define id="screenID" scope="page" value="C06-10" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<body>
	<div id="content"><!-- InstanceBeginEditable name="content" -->
		<div id="commit">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ロールバック</p>
			<h3>ロールバック</h3>
			<div style="height:620px;">
				<h4><ts:messages id="message" message="true"><bean:write name="message"/></ts:messages></h4>
			</div>
	<!-- end commit --></div>
	<!-- InstanceEndEditable -->
	<!-- end #content --></div>
</body>
<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
