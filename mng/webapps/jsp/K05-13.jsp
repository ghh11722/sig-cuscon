<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お客様企業メイン" />
<bean:define id="titleScreenName" scope="page" value="ポリシー一覧" />
<bean:define id="screenName" scope="page" value="IDS/IPS編集" />
<bean:define id="screenID" scope="page" value="K05-13" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-03.jspf"%>
<script>
	var submit_flag = 0;
	function updateIDS_IPS() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		if(document._PolicyForm.IDS_IPS[0].checked) {
	    	document._PolicyForm.radio.value = 1;
		} else {
			document._PolicyForm.radio.value = 0;
		}

	    document._PolicyForm.submit();
	}
	function cancelClick() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document._PolicyForm.action = "<html:rewrite action="/admin/fwsetting/policysetting/PolicyBL" />";
	    document._PolicyForm.submit();
	}
</script>
<body>
	<div id="content">
		<div id="editIpsIds">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ポリシー設定 &raquo; IPS/IDS設定</p>

			<h3>IPS/IDS設定</h3>
			<ts:form action="/admin/fwsetting/policysetting/UpdateIdsIpsBL" method="post" >
				<div id="useOrUnuse">
					<ul>
						<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
							<logic:equal name="_PolicyForm" property="ids_ips" value="default">
								<li><input name="IDS_IPS" type="radio"  checked="checked" disabled="disabled"/>○：利用する</li>
								<li><input name="IDS_IPS" type="radio" disabled="disabled"/>×：利用しない</li>
							</logic:equal>
							<logic:equal name="_PolicyForm" property="ids_ips" value="none">
								<li><input name="IDS_IPS" type="radio" disabled="disabled"/>○：利用する</li>
								<li><input name="IDS_IPS" type="radio"  checked="checked" disabled="disabled"/>×：利用しない</li>
							</logic:equal>
						</logic:notMatch>
						<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
							<logic:equal name="_PolicyForm" property="ids_ips" value="default">
								<li><input name="IDS_IPS" type="radio"  checked="checked"/>○：利用する</li>
								<li><input name="IDS_IPS" type="radio" />×：利用しない</li>
							</logic:equal>
							<logic:equal name="_PolicyForm" property="ids_ips" value="none">
								<li><input name="IDS_IPS" type="radio" />○：利用する</li>
								<li><input name="IDS_IPS" type="radio"  checked="checked"/>×：利用しない</li>
							</logic:equal>
						</logic:match>
					</ul>

				</div>
				<ul id="button">
					<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<li><input type="button" value="OK" disabled="disabled"/></li>
					</logic:notMatch>
					<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<li><input type="button" value="OK"  onclick="updateIDS_IPS()" /></li>
					</logic:match>
					<li><input type="button" value="Cancel" onclick="cancelClick()"/></li>
				</ul>
				<input type="hidden" name="name" value="<bean:write name="_PolicyForm" property="name"/>"/>
				<input type="hidden" name="radio" value="0"/>
			</ts:form>
		</div>
		<!-- end editIpsIds -->

	<!-- end #content --></div>

</body>

<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
