<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お客様企業メイン" />
<bean:define id="titleScreenName" scope="page" value="ロールバック" />
<bean:define id="screenName" scope="page" value="Application参照" />
<bean:define id="screenID" scope="page" value="C06-16" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<body onunload="unLoadWindow()">
	<div id="content">
		<div id="blockApplication">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ポリシー設定 &raquo; ロールバック &raquo; アプリケーション参照</p>
			<h3>アプリケーション参照</h3>
			<form action="" method="post">
				<div id="body2">
					<div style="height:250px; overflow-y:scroll;">
						<div align="left">選択中のアプリケーション</div>
						<table width="100%" summary="メイン画面の背景テーブル">
							<tr>
								<td>
									<!--<caption align="left">選択中のアプリケーション</caption>-->

									<table class="list" summary="顧客企業一覧テーブル">
										<thead>
											<tr class="odd">
												<th abbr="Name">アプリケーション名</th>
												<th abbr="Category">カテゴリー</th>
												<th abbr="Subcategory">サブカテゴリー</th>
												<th abbr="Technorogy">テクノロジー</th>
												<th abbr="Risk">リスク</th>
											</tr>
										</thead>
										<tbody>
											<logic:iterate id="AppliBean" name="_RollbackForm" property="appliList" indexId="index">
												<tr <%=index % 2 == 0 ? "" : "class='odd'"%>>
													<td><bean:write name="AppliBean" property="name"/></td>
													<td><bean:write name="AppliBean" property="category"/></td>
													<td><bean:write name="AppliBean" property="subCategory"/></td>
													<td><bean:write name="AppliBean" property="technology"/></td>
													<td><bean:write name="AppliBean" property="risk"/></td>
												</tr>
											</logic:iterate>
										</tbody>
									</table>
				    			</td>
							</tr>
						</table>
					</div>
					<br/>
					<br/>
					<div style="height:100px;  overflow-y:scroll;">
						<div align="left">選択中のフィルター</div>
						<table width="100%" summary="メイン画面の背景テーブル">
							<tr>
								<td>
									<!--<caption Align="left">選択中のフィルター</caption>-->

									<table class="list" summary="顧客企業一覧テーブル">
										<thead>
											<tr class="odd">
												<th abbr="Name">フィルター名</th>
												<th abbr="Categories">カテゴリー</th>
												<th abbr="Subcategories">サブカテゴリー</th>
												<th abbr="Technologies">テクノロジー</th>
												<th abbr="Risks">リスク</th>

											</tr>
										</thead>
										<tbody>

											<logic:iterate id="AppliFilBean" name="_RollbackForm" property="appliFilList" indexId="index">
												<tr <%=index % 2 == 0 ? "" : "class='odd'"%>>
													<td><bean:write name="AppliFilBean" property="name"/></td>
													<td>
														<logic:iterate id="CategoryBean" name="AppliFilBean" property="categoryList">
															<bean:write name="CategoryBean" property="name"/>
														</logic:iterate>
													</td>
													<td>
														<logic:iterate id="SubCategoryBean" name="AppliFilBean" property="subCategoryList">
															<bean:write name="SubCategoryBean" property="name"/>
														</logic:iterate>
													</td>
													<td>
														<logic:iterate id="TechnologyBean" name="AppliFilBean" property="technologyList">
															<bean:write name="TechnologyBean" property="name"/>
														</logic:iterate>
													</td>
													<td>
														<logic:iterate id="RiskBean" name="AppliFilBean" property="riskList">
															<bean:write name="RiskBean" property="objectType"/>
														</logic:iterate>
													</td>
												</tr>
											</logic:iterate>
										</tbody>
									</table>
								</td>
							</tr>
						</table>
					</div>
					<br/>
					<br/>

					<div style="height:200px; overflow-y:scroll;">
						<div align="left">選択中のグループ</div>
						<table width="100%" summary="メイン画面の背景テーブル">
							<td>
								<!--<caption align="left">選択中のグループ</caption>-->
									<table class="list" summary="顧客企業一覧テーブル">
										<thead>
											<tr class="odd">
												<th abbr="Name">グループ名</th>
												<th abbr="Members">メンバー数</th>
												<th abbr="Applications/Filters/Groups">アプリケーション・フィルター</th>
											</tr>
										</thead>
										<tbody>
											<logic:iterate id="AppliGrpBean" name="_RollbackForm" property="appliGrpList" indexId="index">
												<tr <%=index % 2 == 0 ? "" : "class='odd'"%>>
													<td><bean:write name="AppliGrpBean" property="name"/></td>
													<td><bean:write name="AppliGrpBean" property="members"/></td>
													<td><bean:write name="AppliGrpBean" property="applications"/></td>
												</tr>
											</logic:iterate>
										</tbody>
									</table>
								</td>
						</table>
					</div>

					<div align="left">
						<input type="button" value="Cancel" onclick="top.location.href='RollbackBL.do'" />
					</div>
				</div>
			</form>
	<!-- end #blockApplication --></div>
	<!-- end content --></div>
</body>

<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
