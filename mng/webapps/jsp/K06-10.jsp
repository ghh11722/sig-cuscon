<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="メイン画面" />
<bean:define id="screenName" scope="page" value="企業管理者メイン" />
<bean:define id="screenID" scope="page" value="K06-10" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-03.jspf"%>
<head>
	<script type="text/javascript">
	<!--
	var submit_flag = 0;
	var chkNum = 0;
	var chkbox = 0;
	var indexList = new Array();

	function lookDisplayType(){
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		indexList = new Array();

		chkNum = 0;

		var tbl = document.getElementById("selectTBL");

		var selectIndex = 0;

		if (chkbox == 1 && document.subForm.checkboxNum.checked) {
			selectIndex = 0
			indexList.push(selectIndex);
		} else if (chkbox > 1) {

			for (i = 1; i < chkbox + 1; i++) {
				if(document.subForm.checkboxNum[i-1].checked) {
					chkNum++;
					selectIndex = i-1;

					indexList.push(selectIndex);
				}
			}
		}

		var display = "<bean:write name="_ApplicationGroupsForm" property="displayType"/>";

		if (display == "登録") {
			addData4Regist();
		} else {
			addDate4Update();
		}
	}

	function lookDisplayType4Cancel(){
		var display = "<bean:write name="_ApplicationGroupsForm" property="displayType"/>";

		if (display == "登録") {
			document.subForm.action = "<html:rewrite action="/admin/fwsetting/objectsetting/AddApplicationGroupBL" />";
        	document.subForm.submit();
		} else {
			document.subForm.action = "<html:rewrite action="/admin/fwsetting/objectsetting/UpdateApplicationGroupBL" />";
        	document.subForm.submit();
		}
	}

	function addData4Regist(){
		var str = "";
		for(i=0;i<indexList.length;i++) {
			if(i==0) {
				str = indexList[i];
			} else {
				str = str + "," + indexList[i];
			}
		}
		document.subForm.index.value = str;
		document.subForm.action = "<html:rewrite action="/admin/fwsetting/objectsetting/AddGroupsBL" />";
       	document.subForm.submit();
	}

	function addDate4Update(){
		var str = "";
		for(i=0;i<indexList.length;i++) {
			if(i==0) {
				str = indexList[i];
			} else {
				str = str + "," + indexList[i];
			}
		}
		document.subForm.index.value = str;
		document.subForm.action = "<html:rewrite action="/admin/fwsetting/objectsetting/AddGroupsBL" />";
       	document.subForm.submit();
	}
	var lastIndex = 0;
	function listClick( id_tr , index ){
		if(document.getElementById) {
			var obj = document.getElementById("id_customers");
			var TRs = obj.getElementsByTagName("tr");
			if( lastIndex % 2 ) {
				TRs[lastIndex].className = "odd";
			} else {
				TRs[lastIndex].className = "";
			}
			document.getElementById(id_tr).className = "selected";
			lastIndex = index;
		}
	}
	function onCancel(kbn){
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		if(kbn == "add"){
			document.subForm.action = "<html:rewrite action="/admin/fwsetting/objectsetting/AddApplicationGroupBL" />";
		} else {
			document.subForm.action = "<html:rewrite action="/admin/fwsetting/objectsetting/EditApplicationGroupBL" />";
		}
       	document.subForm.submit();
	}
	-->
	</script>
</head>
<body onunload="unLoadWindow()">
	<div id="content">
		<div id="editGroups">
			<logic:match name="_ApplicationGroupsForm" property="displayType" value="登録">
				<p id="pankuzu">Top &raquo; FW設定機能 &raquo; オブジェクト設定 &raquo; アプリケーショングループ設定 &raquo; アプリケーショングループ追加 &raquo; グループ登録</p>
			</logic:match>
			<logic:notMatch name="_ApplicationGroupsForm" property="displayType" value="登録">
				<p id="pankuzu">Top &raquo; FW設定機能 &raquo; オブジェクト設定 &raquo; アプリケーショングループ設定 &raquo; アプリケーショングループ編集 &raquo; グループ登録</p>
			</logic:notMatch>
			<h3>グループ登録</h3>
			<form action="/admin/fwsetting/objectsetting/AppolicationGroupsBL" name="subForm" method="post" >
				<table id="selectTBL">
				<thead>
					<tr class="odd">
						<th scope="col">&nbsp;</th>
						<th scope="col">オブジェクト名</th>
						<th scope="col">メンバー数</th>
						<th scope="col">アプリケーション・フィルター</th>
					</tr>
				</thead>
				<logic:notEmpty name="_ApplicationGroupsForm" property="groupList4Add">
				<tbody id="id_customers">
					<logic:iterate id="userBean" name="_ApplicationGroupsForm" property="groupList4Add" indexId="index">
						<tr id="id_tr<bean:write name="index"/>" <%= index % 2 == 0 ? "" : "class='odd'" %>
							    onclick="listClick('id_tr<bean:write name="index"/>',<%= index %>);" >
							<td>
								<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
									<logic:notMatch name="userBean"  property="chkFlg" value="true" >
										<input type="checkbox"  name="checkboxNum" disabled="disabled" />
									</logic:notMatch>
									<logic:match name="userBean"  property="chkFlg" value="true" >
										<input type="checkbox" name="checkboxNum" disabled="disabled" checked="checked"/>
									</logic:match>
								</logic:notMatch>
								<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
									<logic:notMatch name="userBean"  property="chkFlg" value="true" >
										<input type="checkbox" name="checkboxNum"/>
									</logic:notMatch>
									<logic:match name="userBean"  property="chkFlg" value="true" >
										<input type="checkbox" name="checkboxNum" checked="checked"/>
									</logic:match>
								</logic:match>
								<script type="text/javascript">
									{
										chkbox++;
									}
								</script>
							</td>
							<td><bean:write name="userBean" property="name" /></td>
							<td><bean:write name="userBean" property="members" /></td>
							<td><bean:write name="userBean" property="applications" /></td>
						</tr>
					</logic:iterate>
				</tbody>
				</logic:notEmpty>
				</table>
				<ul id="button">
					<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<li><input type="button" value="OK" disabled="disabled"/></li>
					</logic:notMatch>
					<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<li><input type="button" value="OK" onclick="lookDisplayType()" /></li>
					</logic:match>
					<logic:match name="_ApplicationGroupsForm" property="displayType" value="登録">
						<li><input type="button" value="Cancel" onclick="onCancel('add')"/></li>
					</logic:match>
					<logic:notMatch name="_ApplicationGroupsForm" property="displayType" value="登録">
						<li><input type="button" value="Cancel" onclick="onCancel('edt')"/></li>
					</logic:notMatch>
				<!-- end #button --></ul>
				<input type="hidden" name="displayType" value="<bean:write name="_ApplicationGroupsForm" property="displayType"/>"/>
				<input type="hidden" name="index"/>
				<input type="hidden" name="name" value="<bean:write name="_ApplicationGroupsForm" property="name"/>"/>
				<input type="hidden" id="oldName" name="oldName" value="<bean:write name="_ApplicationGroupsForm" property="oldName"/>"/>
			</form>
		<!-- end #editGroups -->
		</div>
	<!-- end #content --></div>
</body>

<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
