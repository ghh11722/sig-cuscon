<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="お知らせメッセージ登録確認画面" />
<bean:define id="screenName" scope="page" value="お知らせメッセージ登録確認" />
<bean:define id="screenID" scope="page" value="K14-02" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true" lang="true">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-02.jspf"%>

<div id="content">
	<div id="blockOshirase">
		<h3>お知らせ登録</h3>

		<ts:form action="/admin/infomation/entryBL"  method="post">

		<h4>変更前</h4>
		<div class="oshiraseNow">
			<pre><bean:write filter="false" name="_InfomationEntryForm" property="infomation"/></pre>
		</div>

		<h4>プレビュー表示</h4>
		<div class="oshiraseNow">
			<pre><bean:write filter="false" name="_InfomationEntryForm" property="newInfomation"/></pre>
		</div>

		<p>上記の内容で反映しますか？</p>

		<ul id="button">
			<li><input type="submit" name="confirm" value="OK" /></li>
			<li><input type="submit" name="confirm" value="Cancel" /></li>
		</ul>
		</ts:form>
	</div>
</div>

<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
