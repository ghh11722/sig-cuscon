<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="パスワード初期化機能" />
<bean:define id="titleScreenName" scope="page" value="パスワード初期化画面" />
<bean:define id="screenName" scope="page" value="パスワード初期化" />
<bean:define id="screenID" scope="page" value="0001" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true" lang="true">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-01.jspf"%>

<script language="JavaScript" type="text/JavaScript">
<!--
	function listOver( id_tr, id_radio  ){
		if(document.getElementById) {
			document.getElementById(id_tr).className = "mouseover";
			if( document.getElementById(id_radio).checked == true ){
				document.getElementById(id_tr).className = "selected";
			}
		}
	}
	function listOut( id_tr, id_radio, style_class ){
		if(document.getElementById) {
			document.getElementById(id_tr).className = style_class;
			if( document.getElementById(id_radio).checked == true ){
				document.getElementById(id_tr).className = "selected";
			}
		}
	}
	function listClick( id_tr, id_radio ){
		if(document.getElementById) {
			// リスト再描画
			var obj = document.getElementById("id_customers");
			var TRs = obj.getElementsByTagName("tr");
			for (i=0; i<TRs.length; i++){
				if( i % 2 ) { TRs[i].className = "odd"; }
				else        { TRs[i].className = ""; }
			}
			// 選択項目の色変更
			document.getElementById(id_radio).checked = true;
			document.getElementById(id_tr).className = "selected";
		}
	}
//-->
</script>

<div id="content">
	<div id="blockCPassChange">
		<ts:form action="/admin/password/init_confirmBL"  method="post">
		<h3>お客様管理者パスワード初期化</h3>
		<table class="list">
		<thead>
			<tr>
				<th scope="col">&nbsp;</th>
				<th scope="col">ログインID</th>
				<th scope="col">ファイアウォールID</th>
				<th scope="col">ヴァーチャルシステム(VSYS)</th>
				<th scope="col">最終変更日時</th>
			</tr>
		</thead>
<logic:notEmpty name="_InitPasswordForm" property="accounts">
		<tbody id="id_customers">
<logic:iterate id="userBean" name="_InitPasswordForm" property="accounts" indexId="idx">
			<tr id="id_tr<bean:write name="idx"/>" <%= idx % 2 == 0 ? "" : "class='odd'" %>
			    onmouseover="listOver('id_tr<bean:write name="idx"/>','id_radio<bean:write name="idx"/>');"
			    onmouseout="listOut('id_tr<bean:write name="idx"/>','id_radio<bean:write name="idx"/>','<%= idx % 2 == 0 ? "" : "odd" %>');"
			    onclick="listClick('id_tr<bean:write name="idx"/>','id_radio<bean:write name="idx"/>');" >
				<td>
					<input type="radio"
					       id="id_radio<bean:write name="idx"/>"
					       name="passInitID"
					       value="<bean:write name="userBean" property="loginId"/>"
					       <bean:write name="userBean" property="status"/>
					       onclick="listClick('id_tr<bean:write name="idx"/>','id_radio<bean:write name="idx"/>');" />
				</td>
				<td><bean:write name="userBean" property="loginId"/></td>
				<td><bean:write name="userBean" property="firewallId"/></td>
				<td><bean:write name="userBean" property="vsysId"/></td>
				<td><bean:write name="userBean" property="last_login_date"/></td>
			</tr>
</logic:iterate>
		</tbody>
</logic:notEmpty>
		</table>
<logic:notEmpty name="_InitPasswordForm" property="accounts">
		<ul id="button">
			<li><ts:submit value="初期化を行う" /></li>
			<li><input type="button" value="Cancel" onclick="document.location='<bean:write name='contextPath' />/admin/mainBL.do'" /></li>
		</ul>
</logic:notEmpty>
		</ts:form>
	</div>
</div>

<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
