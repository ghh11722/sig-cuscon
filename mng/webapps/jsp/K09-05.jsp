<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="FWレポーティング機能" />
<bean:define id="titleScreenName" scope="page" value="レポート" />
<bean:define id="screenName" scope="page" value="top-dst-summary" />
<bean:define id="screenID" scope="page" value="K09-05" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-03.jspf"%>
<script>

	function jump() {
		document._ReportForm.submit();
	}

</script>

<div id="content">
		<div id="blockReport">
			<p id="pankuzu">Top &raquo; レポート閲覧 &raquo; 通信先アドレス</p>

			<h3>通信先アドレス</h3>
			<ts:form action="/admin/report/TopDstSummaryBL" method="post" >
				<p id="summary">- 通信先アドレスごとのアクセス集計(上位50件)</p>

				<logic:notEmpty name="_ReportForm" property="dstTargetDate">
					<bean:define id="date" name="_ReportForm" property="dstTargetDate"/>
					<select name="dstTargetDate" onchange="jump()">
							<logic:iterate id="dateBean" name="_ReportForm" property="targetDateList" indexId="index">
								<logic:equal name="dateBean" property="targetDate" value="<%= date.toString() %>" >
									<option value="<bean:write name="dateBean" property="targetDate"/>"  selected="selected"> <bean:write name="dateBean" property="targetDate"/></option>
								</logic:equal>
								<logic:notEqual name="dateBean" property="targetDate" value="<%= date.toString() %>">
									<option value="<bean:write name="dateBean" property="targetDate"/>" > <bean:write name="dateBean" property="targetDate"/></option>
								</logic:notEqual>
							</logic:iterate>
					</select>
				</logic:notEmpty>
				<logic:empty name="_ReportForm" property="dstTargetDate">
					<select name="dstTargetDate" onchange="jump()">
						<option>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
					</select>
				</logic:empty>
				<p>：レポート取得対象を選択してください。</p>
				<table>
					<thead>
						<tr class="odd">
							<th scope="col">通信先アドレス<!--dst--></th>
							<th scope="col">通信先(ホスト名)<!--resolved-dst--></th>
							<th scope="col">通信先ユーザ<!--dstuser--></th>
							<th scope="col">転送量(byte)<!--bytes--></th>
							<th scope="col">セッション数<!--sessions--></th>
						</tr>
					</thead>
					<logic:notEmpty name="_ReportForm" property="topDstSummaryList">
						<tbody>
							 <logic:iterate id="dstBean" name="_ReportForm" property="topDstSummaryList" indexId="index">
							   <tr <%=index % 2 == 0 ? "" : "class='odd'"%>>
							        <td><bean:write name="dstBean" property="dst"/></td>
							        <td><bean:write name="dstBean" property="resolvedDst"/></td>
							        <td><bean:write name="dstBean" property="dstUser"/></td>
							        <td><bean:write name="dstBean" property="bytes"/></td>
							        <td><bean:write name="dstBean" property="sessions"/></td>
						    	</tr>
						    </logic:iterate>
						</tbody>
					</logic:notEmpty>
				</table>
			</ts:form>
		<!-- end blockReport -->
		</div>
	<!-- end #content --></div>
<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
