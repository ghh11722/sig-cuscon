<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お客様企業メイン" />
<bean:define id="titleScreenName" scope="page" value="ポリシー一覧" />
<bean:define id="screenName" scope="page" value="Destination Zone編集" />
<bean:define id="screenID" scope="page" value="K05-05" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-03.jspf"%>
<script>
	var submit_flag = 0;
	var chkNum = 0;
	var indexList = new Array();
	var zoneMstList = new Array();
	var zoneNum = 0;

	function radioClick(size) {
		if(document._PolicyForm.ZoneRadio[0].checked) {
			if(size == 1) {
				document._PolicyForm.checkZone.disabled = true;
			} else {
				for(i=0; i<size; i++) {
					document._PolicyForm.checkZone[i].disabled = true;
				}
			}
		} else {
			if(size == 1) {
				document._PolicyForm.checkZone.disabled = false;
			} else {

				for(i=0; i<size; i++) {
					document._PolicyForm.checkZone[i].disabled = false;
				}
			}
		}
	}

	function getCheck(obj,index){
		if (obj.checked) {
			chkNum++;
			indexList.push(index);
		} else {
			chkNum--;
			for(i=0;i<indexList.length;i++) {
				if(indexList[i]==index) {
					indexList.splice(i, 1);
					i--;
				}
			}
		}
	}

	function getCheckList(){
		if( submit_flag == 1 ){ return; }
		var str = "";
		var res = true;

		if(document._PolicyForm.ZoneRadio[0].checked && document._PolicyForm.otherZoneName.value=="any"){
			res = confirm("通信元ゾーンの設定がanyに設定されています。このまま設定しますか？");
		}
		if( res == true ){
			submit_flag = 1;

	        for(i =0; i<indexList.length; i++) {
	            if(i==0) {
					str = indexList[i];
	            } else {
	            	str = str + "," + indexList[i];
	            }
	        }
	        document._PolicyForm.checkList.value = str;

			if(document._PolicyForm.ZoneRadio[0].checked) {
	        	document._PolicyForm.radio.value = 1;
			} else {
				document._PolicyForm.radio.value = 2;
			}

	        document._PolicyForm.submit();
		}
	}

	function cancelClick() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document._PolicyForm.action = "<html:rewrite action="/admin/fwsetting/policysetting/PolicyBL" />";
	    document._PolicyForm.submit();
	}
</script>
<body>
	<div id="content">
		<div id="editSourcezone">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ポリシー設定 &raquo; 通信先ゾーン設定</p>

			<h3>通信先ゾーン設定</h3>
			<ts:form action="/admin/fwsetting/policysetting/UpdateDestinationZoneBL" method="post" >
				<bean:size id="zoneSize" name="_PolicyForm" property="zoneMasterList" />
				<div id="anyOrSelect">
					<ul>
						<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
							<logic:equal name="_PolicyForm" property="zoneName" value="any">
								<li><input name="ZoneRadio" type="radio" checked="checked" onclick="radioClick(<bean:write name="zoneSize" />)"/>any</li>
								<li><input name="ZoneRadio" type="radio" onclick="radioClick(<bean:write name="zoneSize" />)"/>選択する</li>
							</logic:equal>
							<logic:notEqual name="_PolicyForm" property="zoneName" value="any">
								<li><input name="ZoneRadio" type="radio" onclick="radioClick(<bean:write name="zoneSize" />)"/>any</li>
								<li><input name="ZoneRadio" type="radio" checked="checked" onclick="radioClick(<bean:write name="zoneSize" />)"/>選択する</li>
							</logic:notEqual>
						</logic:match>
						<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
							<logic:equal name="_PolicyForm" property="zoneName" value="any">
								<li><input name="ZoneRadio" type="radio" checked="checked" disabled="disabled"/>any</li>
								<li><input name="ZoneRadio" type="radio" disabled="disabled"/>選択する</li>
							</logic:equal>
							<logic:notEqual name="_PolicyForm" property="zoneName" value="any">
								<li><input name="ZoneRadio" type="radio" disabled="disabled"/>any</li>
								<li><input name="ZoneRadio" type="radio" checked="checked" disabled="disabled"/>選択する</li>
							</logic:notEqual>
						</logic:notMatch>
					</ul>
				<!-- end anyOrSelect --></div>
				<div id="available">
					<h4>&raquo; オブジェクトの選択</h4>
					<ul>
						<logic:iterate id="zoneMstBean" name="_PolicyForm" property="zoneMasterList"  indexId="index">
							<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
								<li><input name="checkZone" type="checkbox" disabled="disabled" /><bean:write name="zoneMstBean" /><br/></li>
							</logic:notMatch>
							<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
								<logic:equal name="_PolicyForm" property="zoneName" value="any">
									<li><input name="checkZone" type="checkbox"  onclick="getCheck(this, <%= index %>)" disabled="disabled"/><bean:write name="zoneMstBean" /><br/></li>
								</logic:equal>
								<logic:notEqual name="_PolicyForm" property="zoneName" value="any">
									<li><input name="checkZone" type="checkbox"  onclick="getCheck(this, <%= index %>)" /><bean:write name="zoneMstBean" /><br/></li>
								</logic:notEqual>
							</logic:match>
								<script type="text/javascript">
										{
											zoneMstList[zoneNum] = "<bean:write name='zoneMstBean'/>";
											zoneNum++;
										}
									</script>
						</logic:iterate>
						<logic:iterate id="zoneBean" name="_PolicyForm" property="zoneList"  indexId="index">
							<script type="text/javascript">
										{
											if(zoneMstList.length == 1) {
												if(zoneMstList[0] == "<bean:write name='zoneBean'  property='name'/>") {
													document._PolicyForm.checkZone.checked=true;
													indexList[chkNum] = 0;
													chkNum++;
												}
											} else {
												for(i=0; i<zoneMstList.length; i++) {
													if(zoneMstList[i] == "<bean:write name='zoneBean'  property='name'/>") {
														document._PolicyForm.checkZone[i].checked=true;
														indexList[chkNum] = i;
														chkNum++;
													}
												}
											}
										}
									</script>
						</logic:iterate>
					</ul>
				<!-- end #available --></div>

				<ul id="button">
					<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<li><input type="button" value="OK" disabled="disabled"/></li>
					</logic:notMatch>
					<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<li><input type="button" value="OK" onclick="getCheckList()"/></li>
					</logic:match>
					<li><input type="button" value="Cancel" onclick="cancelClick()"/></li>
				</ul>
				<input type="hidden" name="name" value="<bean:write name="_PolicyForm" property="name"/>"/>
				<input type="hidden" name="radio" value="0"/>
				<input type="hidden" name="checkList" />
				<input type="hidden" name="otherZoneName" value="<bean:write name="_PolicyForm" property="otherZoneName"/>"/>
			</ts:form>
		<!-- editSourcezone -->
		</div>
	<!-- end #content --></div>
</body>

<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
