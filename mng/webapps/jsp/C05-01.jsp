<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="メイン画面" />
<bean:define id="screenName" scope="page" value="企業管理者メイン" />
<bean:define id="screenID" scope="page" value="C05-01" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<head>
	<script type="text/javascript">
	<!--

	var chkNum = 0;
	var indexList = new Array();
	var lastIndex = 0;
	function jumpAppli(selectedIndex) {
		document.geneForm.index.value =selectedIndex;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/EditAddressBL" />";
        document.geneForm.submit();
	}

	function getCheck(obj,index){
		if (obj.checked) {
			chkNum++;
			indexList.push(index);
		} else {
			chkNum--;
			for(i=0;i<indexList.length;i++) {
				if(indexList[i]==index) {
					indexList.splice(i, 1);
					i--;
				}
			}
		}
	}

	function checkDel(){
		var str = "";
		if(chkNum!=0) {
			for(i=0;i<indexList.length;i++) {
				if(i==0) {
					str = indexList[i];
				} else {
					str = str + "," + indexList[i];
				}
			}
			document.geneForm.checkIndex.value = str;
			document.geneForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/AddressesConfirmBL" />";
	        document.geneForm.submit();
		}
	}

	function listClick( id_tr , index ){
		if(document.getElementById) {
			var obj = document.getElementById("id_customers");
			var TRs = obj.getElementsByTagName("tr");
			if( lastIndex % 2 ) {
				TRs[lastIndex].className = "odd";
			} else {
				TRs[lastIndex].className = "";
			}
			document.getElementById(id_tr).className = "selected";
			lastIndex = index;
		}
	}
	-->
	</script>
</head>
<body onunload="unLoadWindow()">
	<div id="content">
		<div id="objectAddress">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; オブジェクト設定 &raquo; アドレス設定</p>
			<h3>アドレス設定</h3>
			<form name="geneForm" method="post">
				<ul id="button">
					<li><input type="button" value="オブジェクトの追加" onclick="document.location='<bean:write name='contextPath' />/customer/fwsetting/objectsetting/AddAddressBL.do'" /></li>
					<li><input type="button" value="オブジェクトの削除" onclick="checkDel()" /></li>
				</ul>
				<input type="hidden" name="index" />
				<input type="hidden" name="checkIndex" />
				<table>
					<thead>
						<tr class="odd">
							<th scope="col">&nbsp;</th>
							<th scope="col">オブジェクト名</th>
							<th scope="col">タイプ</th>
							<th scope="col">アドレス</th>
						</tr>
					</thead>
					<logic:notEmpty name="_AddressesForm" property="addressList">
					<tbody id="id_customers">
						<logic:iterate id="userBean" name="_AddressesForm" property="addressList" indexId="index">
							<tr id="id_tr<bean:write name="index"/>" <%= index % 2 == 0 ? "" : "class='odd'" %>
							    onclick="listClick('id_tr<bean:write name="index"/>',<%= index %>);" >
	 							<td><input name="a" type="checkbox" onclick="getCheck(this, <%= index %>)" /></td>
								<td><a href="javascript:jumpAppli(<%= index %>)"><bean:write name="userBean" property="name"/></a></td>
								<td><bean:write name="userBean" property="type" /></td>
								<td><bean:write name="userBean" property="address" /></td>
							</tr>
						</logic:iterate>
					</tbody>
					</logic:notEmpty>
				</table>
				<ul id="button">
					<li><input type="button" value="オブジェクトの追加" onclick="document.location='<bean:write name='contextPath' />/customer/fwsetting/objectsetting/AddAddressBL.do'" /></li>
					<li><input type="button" value="オブジェクトの削除" onclick="checkDel()" /></li>
				</ul>

				<logic:notEmpty name="_AddressesForm" property="message">
					<script type="text/javascript">
					{
						alert("<bean:write name="_AddressesForm" property="message"/>");
					}
					</script>
				</logic:notEmpty>
			</form>
	<!-- end objectAddress --></div>
	<!-- end content --></div>
</body>
<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
