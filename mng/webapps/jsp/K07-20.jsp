<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お客様企業メイン" />
<bean:define id="titleScreenName" scope="page" value="ロールバック" />
<bean:define id="screenName" scope="page" value="Schedule参照" />
<bean:define id="screenID" scope="page" value="K07-20" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-03.jspf"%>
<body>
	<div id="content">
		<div id="editSchedule">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ロールバック &raquo; スケジュール参照</p>
			<h3>スケジュール参照</h3>
			<ts:form action="/admin/fwsetting/rollback/RollbackBL" method="post" >
				<table>
					<thead>
						<tr class="odd">
							<th scope="col">&nbsp;</th>
							<th scope="col">オブジェクト名</th>
							<th scope="col">周期</th>
							<th scope="col">スケジュール</th>
						</tr>
					</thead>
					<tbody>
						<logic:iterate id="editscheBean" name="_RollbackForm" property="scheduleList"  indexId="index">
							<bean:define id="scheName" name="_RollbackForm" property="scheduleName" />
							<logic:equal name="editscheBean" property="name" value="<%= scheName.toString() %>">
								<tr <%=index % 2 == 0 ? "" : "class='odd'"%>>
									<td><input name="scheRadio" type="radio" checked="checked"  disabled ="disabled"></input></td>
									<td><bean:write name="editscheBean" property="name"/></td>
									<td><bean:write name="editscheBean" property="recurrenceStr"/></td>
									<logic:notEmpty name="editscheBean" property="timeList">
										<td>
											<logic:iterate id="timeBean" name="editscheBean" property="timeList" >
												<bean:write name="timeBean" /><br/>
									</logic:iterate>
										</td>
									</logic:notEmpty>
									<logic:empty name="editscheBean" property="timeList">
										<td><br/></td>
									</logic:empty>
								</tr>
							</logic:equal>
							<logic:notEqual name="editscheBean" property="name" value="<%= scheName.toString() %>">
								<tr <%=index % 2 == 0 ? "" : "class='odd'"%>>
									<td><input name="scheRadio" type="radio" disabled ="disabled"></input></td>
									<td><bean:write name="editscheBean" property="name"/></td>
									<td><bean:write name="editscheBean" property="recurrenceStr"/></td>
									<logic:notEmpty name="editscheBean" property="timeList">
										<td>
											<logic:iterate id="timeBean" name="editscheBean" property="timeList" >
												<bean:write name="timeBean" /><br/>
											</logic:iterate>
										</td>
									</logic:notEmpty>
									<logic:empty name="editscheBean" property="timeList">
										<td><br/></td>
									</logic:empty>
								</tr>
							</logic:notEqual>
						</logic:iterate>
					</tbody>
				</table>
				<ul id="button">
					<li><ts:submit value="Cancel" /></li>
				</ul>
			</ts:form>
		<!-- end editScheule -->
		</div>
	<!-- end #content --></div>
</body>

<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
