<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="スケジュール機能" />
<bean:define id="titleScreenName" scope="page" value="オブジェクト設定画面" />
<bean:define id="screenName" scope="page" value="FW設定機能" />
<bean:define id="screenID" scope="page" value="C05-2５" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<script>
	<!--
		var submit_flag = 0;

		function okClick() {
			if( submit_flag == 1 ){ return; }
			submit_flag = 1;
			document.scheduleForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/DelSchedulesBL" />";
		    document.scheduleForm.submit();
		}

		function cancelClick() {
			if( submit_flag == 1 ){ return; }
			submit_flag = 1;
			document.scheduleForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/SchedulesBL" />";
		    document.scheduleForm.submit();
		}
	-->
</script>
<body>
	<div id="content"><!-- InstanceBeginEditable name="content" -->
		<div id="objectSchedules">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; オブジェクト設定 &raquo; スケジュール設定</p>

			<h3>スケジュール削除</h3>
			<form action="" method="post" name="scheduleForm">
				<table>
					<thead>
						<tr class="odd">
							<th scope="col">オブジェクト名</th>
							<th scope="col">周期</th>

							<th scope="col">時間</th>
						</tr>
					</thead>
					<tbody>
						<logic:iterate id="scheduleBean" name="_ScheduleForm" property="scheduleList"  indexId="index">
							<tr>
								<td><bean:write name="scheduleBean" property="name"/></td>

								<td><bean:write name="scheduleBean" property="recurrenceStr"/></td>
								<td>
									<logic:iterate id="timeBean" name="scheduleBean" property="timeList">
										<bean:write name="timeBean" /><br/>
									</logic:iterate>
								</td>
							</tr>
						</logic:iterate>
					</tbody>
				</table>
				<p>上記の設定を削除してもよろしいですか？</p>
				<ul id="button">
					<li><input type="button" value="OK"  onclick= "okClick()"/></li>
					<li><input type="button" value="Cancel" onclick="cancelClick()"/></li>
				</ul>
			</form>
		<!-- end #objectSchedules --></div>
	<!-- InstanceEndEditable -->
	<!-- end #content --></div>
</body>

<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
