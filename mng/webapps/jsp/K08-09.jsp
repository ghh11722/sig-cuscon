<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>
<%@ taglib uri="/cuscon-struts" prefix="cs" %>

<bean:define id="titleUsecaseName" scope="page" value="コンフィグ管理" />
<bean:define id="titleScreenName" scope="page" value="FWコンフィグ設定表示画面" />
<bean:define id="screenName" scope="page" value="FWコンフィグ設定ダウンロード" />
<bean:define id="screenID" scope="page" value="0001" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true" lang="true">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-03.jspf"%>

<div id="content">
	<div id="blockLog">
	<p id="pankuzu">Top &raquo; コンフィグ管理 &raquo; コンフィグダウンロード</p>
	<h3><bean:write name="screenName" scope="page" /></h3>
		<div id="operationBox1">
			<div style="height:620px;">
				<h4><ts:messages id="message" message="true"><bean:write name="message"/></ts:messages></h4>
			</div>
		</div>
	</div>
</div>

<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
