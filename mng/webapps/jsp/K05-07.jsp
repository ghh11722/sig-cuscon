<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お客様企業メイン" />
<bean:define id="titleScreenName" scope="page" value="ポリシー一覧" />
<bean:define id="screenName" scope="page" value="Destination Address編集" />
<bean:define id="screenID" scope="page" value="K05-07" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-03.jspf"%>
<script>
	var submit_flag = 0;
	var chkNum = 0;
	var indexList = new Array();
	var addIndexList = new Array();
	var mstNum = 0;
	var addressMstList = new Array();
	var addNum = 0;
	var addIndex = 0;
	var addList = new Array();

	function radioClick(size) {
		if(document._PolicyForm.AddressRadio[0].checked) {
			if(size == 1) {
				document._PolicyForm.checkAddress.disabled = true;
			} else {
				for(i=0; i<size; i++) {
					document._PolicyForm.checkAddress[i].disabled = true;
				}
			}
			if(addIndex == 1) {
				document._PolicyForm.checkAddAddress.disabled = true;
			} else {
				for(i=0; i<addIndex; i++) {
					document._PolicyForm.checkAddAddress[i].disabled = true;
				}
			}
			document._PolicyForm.addressText.disabled = true;
		} else {
			if(size == 1) {
				document._PolicyForm.checkAddress.disabled = false;
			} else {
				for(i=0; i<size; i++) {
					document._PolicyForm.checkAddress[i].disabled = false;
				}
			}

			if(addIndex == 1) {
				document._PolicyForm.checkAddAddress.disabled = false;
			} else {
				for(i=0; i<addIndex; i++) {
					document._PolicyForm.checkAddAddress[i].disabled = false;
				}
			}
			document._PolicyForm.addressText.disabled = false;
		}
	}

	function getCheck(obj,index){
		if (obj.checked) {
			chkNum++;
			indexList.push(index);
		} else {
			chkNum--;
			for(i=0;i<indexList.length;i++) {
				if(indexList[i]==index) {
					indexList.splice(i, 1);
					i--;
				}
			}
		}
	}

	function getAddCheck(obj, index) {
		if (obj.checked) {
			addNum++;
			addIndexList.push(index);
		} else {
			addNum--;
			for(i=0;i<addIndexList.length;i++) {
				if(addIndexList[i]==index) {
					addIndexList.splice(i, 1);
					i--;
				}
			}
		}
	}

	function getCheckList(){
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;

		var address = document._PolicyForm.addressText.value;

		if(address.length != 0) {

            //var LF = "\r\n";
            //var splitOut = address.split(LF);

            /* for support Firefox */
            var addressTmp = address.replace(/\r/g,'');
            var LF = "\n";
            var splitOut = addressTmp.split(LF);

			for(i=0;i<splitOut.length;i++) {

				if(splitOut[i].trim().length != 1 && splitOut[i].trim().length != 0) {
					addList[addIndex] = splitOut[i].trim();
					addIndexList[addNum] = addIndex;

					addNum++;
					addIndex++;
				}
			}
		}

		var str = "";
		var addStr = "";
	    for(i =0; i<indexList.length; i++) {
	        if(i==0) {
				str = indexList[i];
	        } else {
	        	str = str + "," + indexList[i];
	        }
	    }
	    document._PolicyForm.checkList.value = str;
		if(document._PolicyForm.AddressRadio[0].checked) {
	    	document._PolicyForm.radio.value = 1;
		} else {
			document._PolicyForm.radio.value = 2;
		}

		for(i =0; i<addIndexList.length; i++) {
			 if(i==0) {
				 addStr = addList[addIndexList[i]];
	         } else {
	        	 addStr = addStr + "," + addList[addIndexList[i]];
	         }

			 document._PolicyForm.addCheckList.value = addStr;
		}
	    document._PolicyForm.submit();
	}

	String.prototype.trim = function() {
		return unescape(escape(this).replace(/^(%u3000|%20|%09)+|(%u3000|%20|%09)+$/g, ""));
	}

	function cancelClick() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document._PolicyForm.action = "<html:rewrite action="/admin/fwsetting/policysetting/PolicyBL" />";
	    document._PolicyForm.submit();
	}

</script>
<body>
	<div id="content">
		<div id="editDstaddress">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ポリシー設定 &raquo; 通信先アドレス設定</p>
			<h3>通信先アドレス設定</h3>
			<ts:form action="/admin/fwsetting/policysetting/UpdateDestinationAddressBL" method="post" >
				<bean:size id="addressSize" name="_PolicyForm" property="addressMasterList" />
				<div id="anyOrSelect">
					<ul>
					<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<logic:equal name="_PolicyForm" property="addressName" value="any">
							<li><input name="AddressRadio" type="radio" checked="checked" disabled="disabled"/>any</li>
							<li><input name="AddressRadio" type="radio" disabled="disabled"/>選択する</li>
						</logic:equal>
						<logic:notEqual name="_PolicyForm" property="addressName" value="any">
							<li><input name="AddressRadio" type="radio" disabled="disabled"/>any</li>
							<li><input name="AddressRadio" type="radio" checked="checked" disabled="disabled"/>選択する</li>
						</logic:notEqual>
					</logic:notMatch>
					<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<logic:equal name="_PolicyForm" property="addressName" value="any">
							<li><input name="AddressRadio" type="radio" checked="checked" onclick="radioClick(<bean:write name="addressSize" />)"/>any</li>
							<li><input name="AddressRadio" type="radio" onclick="radioClick(<bean:write name="addressSize" />)"/>選択する</li>
						</logic:equal>
						<logic:notEqual name="_PolicyForm" property="addressName" value="any">
							<li><input name="AddressRadio" type="radio" onclick="radioClick(<bean:write name="addressSize" />)"/>any</li>
							<li><input name="AddressRadio" type="radio" checked="checked" onclick="radioClick(<bean:write name="addressSize" />)"/>選択する</li>
						</logic:notEqual>
					</logic:match>

					</ul>
				</div>
				<div id="available">
					<h4>&raquo; オブジェクトの選択</h4>
					<ul id="addressUl">
						<logic:iterate id="addressMstBean" name="_PolicyForm" property="addressMasterList"  indexId="index">
							<li>
								<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
									<input name="checkAddress" type="checkbox" disabled="disabled"/>
								</logic:notMatch>
								<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
									<logic:equal name="_PolicyForm" property="addressName" value="any">
										<input name="checkAddress" type="checkbox"  onclick="getCheck(this, <%= index %>)" disabled="disabled"/>
									</logic:equal>
									<logic:notEqual name="_PolicyForm" property="addressName" value="any">
										<input name="checkAddress" type="checkbox"  onclick="getCheck(this, <%= index %>)" />
									</logic:notEqual>
								</logic:match>
								<logic:equal name="addressMstBean" property="objectType" value="0">
									<img alt="" src="../../../img/group_off.png" width="13" height="13"/>
								</logic:equal>
								<logic:equal name="addressMstBean" property="objectType" value="1">
									<img alt="" src="../../../img/group_on.png" width="13" height="13"/>
								</logic:equal>
								<bean:write name="addressMstBean"  property="name"/><br/>
							</li>
								<script type="text/javascript">
										{

											addressMstList[mstNum] = "<bean:write name='addressMstBean'  property='name'/>";
											mstNum++;
										}
								</script>
						</logic:iterate>
						<logic:notEqual name="_PolicyForm" property="addressName" value="any">
							<logic:iterate id="addressBean" name="_PolicyForm" property="addressList"  indexId="index">
								<logic:equal name="addressBean" property="objectType" value="2">
									<li>
										<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
											<input name="checkAddAddress" type="checkbox" checked="checked" disabled="disabled"/>
										</logic:notMatch>
										<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
											<input name="checkAddAddress" type="checkbox"  onclick="getAddCheck(this, <%= index %>)" checked="checked"/>
										</logic:match>
										<img alt="" src="../../../img/group_off.png" width="13" height="13"/>
										<bean:write name="addressBean"  property="name"/><br/>
									</li>
										<script type="text/javascript">
										{

											addList[addNum] = "<bean:write name='addressBean'  property='name'/>";
											addIndexList[addNum] = addNum;
											addNum++;
											addIndex++;
										}
										</script>
								</logic:equal>
							</logic:iterate>
							<logic:iterate id="addressBean" name="_PolicyForm" property="addressList"  indexId="index">
								<script type="text/javascript">
									{
										for(i=0; i<addressMstList.length; i++) {
											if(addressMstList[i] == "<bean:write name='addressBean'  property='name'/>") {
												if(addressMstList.length == 1) {
													document._PolicyForm.checkAddress.checked=true;
												} else {
													document._PolicyForm.checkAddress[i].checked=true;
												}
												indexList[chkNum] = i;
												chkNum++;
											}
										}
									}
								</script>
							</logic:iterate>
						</logic:notEqual>
					</ul>
				<!-- end #available --></div>
				<div id="additionalAddress">
					<h4>追加アドレス</h4>
					<dl>
						<dt>
							<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
								<textarea name="addressText" cols="40" rows="5" disabled="disabled"></textarea><br />
							</logic:notMatch>
							<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
								<logic:notEqual name="_PolicyForm" property="addressName" value="any">
									<textarea name="addressText" cols="40" rows="5"></textarea><br />
								</logic:notEqual>
								<logic:equal name="_PolicyForm" property="addressName" value="any">
									<textarea name="addressText" cols="40" rows="5" disabled="disabled"></textarea><br />
								</logic:equal>
							</logic:match>
						</dt>
						<dd>
							追加するIPアドレスを一行に一つの形式で記入してください。<br />
							(e.g. 「192.168.80.150」 or 「192.168.80.0/24」)</dd>
					</dl>
				<!-- end #additionalAddress --></div>

				<ul id="button">
					<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<li><input type="button" value="OK" disabled="disabled"/></li>
					</logic:notMatch>
					<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<li><input type="button" value="OK" onclick="getCheckList()"/></li>
					</logic:match>
					<li><input type="button" value="Cancel" onclick="cancelClick()"/></li>
				</ul>
				<input type="hidden" name="name" value="<bean:write name="_PolicyForm" property="name"/>"/>
				<input type="hidden" name="radio" value="0"/>
				<input type="hidden" name="checkList" />
				<input type="hidden" name="addCheckList" />
			</ts:form>
			<!-- editDstaddress -->
		</div>
	<!-- end #content --></div>
</body>

<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
