<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="ポリシー設定機能" />
<bean:define id="titleScreenName" scope="page" value="ポリシー設定画面" />
<bean:define id="screenName" scope="page" value="ポリシー設定一覧" />
<bean:define id="screenID" scope="page" value="C04-01" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<script>
	var lastIndex = 0;
	function radioIndex(chkOBJ) {
		document._PolicyForm.selectIndex.value = chkOBJ.value;
	}

	function movePolicy(num) {
		if(num==1) {
			document._PolicyForm.moveType.value = document._PolicyForm.move.value;
		} else {
			document._PolicyForm.moveType.value = document._PolicyForm.move2.value;
		}
		document._PolicyForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/MovePolicyBL" />";
        document._PolicyForm.submit();
	}

	function jumpName(index) {
		document._PolicyForm.selectIndex.value = index;
		document._PolicyForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/EditPolicyBL" />";
        document._PolicyForm.submit();
	}

	function jumpSource(index) {
		document._PolicyForm.selectIndex.value = index;
		document._PolicyForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/EditSourceZoneBL" />";
        document._PolicyForm.submit();
	}

	function jumpDestination(index) {
		document._PolicyForm.selectIndex.value = index;
		document._PolicyForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/EditDestinationZoneBL" />";
        document._PolicyForm.submit();
	}

	function jumpSrcAddress(index) {
		document._PolicyForm.selectIndex.value = index;
		document._PolicyForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/EditSourceAddressBL" />";
        document._PolicyForm.submit();
	}

	function jumpDstAddress(index) {
		document._PolicyForm.selectIndex.value = index;
		document._PolicyForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/EditDestinationAddressBL" />";
        document._PolicyForm.submit();
	}

	function jumpAppli(index) {
		document._PolicyForm.selectIndex.value = index;
		document._PolicyForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/EditApplicationBL" />";
        document._PolicyForm.submit();
	}

	function jumpAction(index) {
		document._PolicyForm.selectIndex.value = index;
		document._PolicyForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/EditActionBL" />";
        document._PolicyForm.submit();
	}

	function jumpIDS_IPS(index) {
		document._PolicyForm.selectIndex.value = index;
		document._PolicyForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/EditIdsIpsBL" />";
        document._PolicyForm.submit();
	}

	function jumpService(index) {
		document._PolicyForm.selectIndex.value = index;
		document._PolicyForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/EditServiceBL" />";
        document._PolicyForm.submit();
	}

	<%-- 20110511 ktakenaka@PROSITE add start --%>
	function jumpUrlFilter(index) {
		document._PolicyForm.selectIndex.value = index;
		document._PolicyForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/EditUrlFilterBL" />";
        document._PolicyForm.submit();
	}

	function jumpVirusCheck(index) {
		document._PolicyForm.selectIndex.value = index;
		document._PolicyForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/EditVirusCheckBL" />";
        document._PolicyForm.submit();
	}

	function jumpSpyware(index) {
		document._PolicyForm.selectIndex.value = index;
		document._PolicyForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/EditSpywareBL" />";
        document._PolicyForm.submit();
	}
	<%-- 20110511 ktakenaka@PROSITE add end --%>
	<%-- 20130901 add start kkato@PROSITE --%>
	function jumpDisable(index) {
		document._PolicyForm.selectIndex.value = index;
		document._PolicyForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/EditDisableBL" />";
        document._PolicyForm.submit();
	}
	<%-- 20130901 add end   kkato@PROSITE --%>

	function listOver( id_tr, index  ){
		if(document.getElementById) {
			document.getElementById(id_tr).className = "mouseover";
			var obj = document.getElementById("id_customers");
			var TRs = obj.getElementsByTagName("tr");
			if(TRs.length == 1) {
				document.getElementById(id_tr).className = "selected";
			} else {
				if( document._PolicyForm.index[index].checked == true ){
					document.getElementById(id_tr).className = "selected";
				}
			}
		}
	}
	function listOut( id_tr, index, style_class ){
		if(document.getElementById) {
			document.getElementById(id_tr).className = style_class;
			var obj = document.getElementById("id_customers");
			var TRs = obj.getElementsByTagName("tr");
			if(TRs.length == 1) {
				document.getElementById(id_tr).className = "selected";
			} else {
				if( document._PolicyForm.index[index].checked == true ){
					document.getElementById(id_tr).className = "selected";
				}
			}
		}
	}
	function listClick( id_tr , index){
		if(document.getElementById) {
			var obj = document.getElementById("id_customers");
			var TRs = obj.getElementsByTagName("tr");

			if( lastIndex % 2 ) {
				TRs[lastIndex].className = "odd";
			} else {
				TRs[lastIndex].className = "";
			}
			if(TRs.length == 1) {
				document._PolicyForm.index.checked = true;
			} else {
				document._PolicyForm.index[index].checked = true;
			}

			document.getElementById(id_tr).className = "selected";
			lastIndex = index;
		}
	}
</script>
<body>
	<div id="content">
		<div id="blockPolicy">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ポリシー設定</p>
			<h3>ポリシー設定</h3>

			<ts:form action="/customer/fwsetting/policysetting/PolicyConfirmBL" method="post" >

				<div id="operationBox1">
					ポリシーの移動：
					<logic:notEmpty name="_PolicyForm" property="policyList">
						<select name="move" onchange="movePolicy(1)">
							<option value="">選択してください。</option>
							<option value="0">一番上へ移動</option>
							<option value="1">一番下へ移動</option>
							<option value="2">一つ上へ移動</option>
							<option value="3">一つ下へ移動</option>
						</select>
					</logic:notEmpty>
					<logic:empty name="_PolicyForm" property="policyList">
						<select name="move3" >
							<option value="">選択してください。</option>
							<option value="0">一番上へ移動</option>
							<option value="1">一番下へ移動</option>
							<option value="2">一つ上へ移動</option>
							<option value="3">一つ下へ移動</option>
						</select>
					</logic:empty>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="button" value="ポリシーの追加" onclick="document.location='<bean:write name='contextPath' />/customer/fwsetting/policysetting/AddPolicyBL.do'" />
					&nbsp;&nbsp;

					<logic:notEmpty name="_PolicyForm" property="policyList">
						<ts:submit value="ポリシーの削除" />
					</logic:notEmpty>
					<logic:empty name="_PolicyForm" property="policyList">
						<input type="button" value="ポリシーの削除"></input>
					</logic:empty>

				<!-- end #movePolicy --></div>
				<table>
					<thead>

						<tr class="odd">
							<th scope="col"></th>
							<th scope="col" abbr="No">No.</th>
							<th scope="col" abbr="Name">ポリシー名</th>
<%-- 20110511 ktakenaka@PROSITE del
							<th scope="col" abbr="Source Zone">通信元ゾーン</th>
							<th scope="col" abbr="Destination Zone">通信先ゾーン</th>
							<th scope="col" abbr="Source Address">通信元アドレス</th>
							<th scope="col" abbr="Destination Address">通信先アドレス</th>
							<th scope="col" abbr="Applications">アプリケーション</th>
--%>
<%-- 20110511 ktakenaka@PROSITE add start --%>
							<th scope="col" abbr="Source Zone">通信元<br/>ゾーン</th>
							<th scope="col" abbr="Destination Zone">通信先<br/>ゾーン</th>
							<th scope="col" abbr="Source Address">通信元<br/>アドレス</th>
							<th scope="col" abbr="Destination Address">通信先<br/>アドレス</th>
							<th scope="col" abbr="Applications">アプリケーション</th>
							<logic:match name="USER_VALUE_OBJECT"  property="urlFilteringFlg" value="1" >
								<th scope="col" abbr="URL Filtering">Web<br/>フィルタ</th>
							</logic:match>
							<th scope="col" abbr="Antivirus">Webウィルス<br/>チェック</th>
							<th scope="col" abbr="Anti Spyware">スパイウェア<br/>チェック</th>
<%-- 20110511 ktakenaka@PROSITE add end --%>
							<th scope="col" abbr="Service">サービス</th>
							<th scope="col" abbr="IPS/IDS">IPS/<br/>IDS</th>
							<th scope="col" abbr="Action">アクション</th>
<%-- 20130901 add start kkato@PROSITE --%>
							<th scope="col" abbr="Disable">有効/無効</th>
<%-- 20130901 add end   kkato@PROSITE --%>
						</tr>
					</thead>
					<tbody id="id_customers">
						<logic:notEmpty name="_PolicyForm" property="policyList">
							<logic:iterate id="policyBean" name="_PolicyForm" property="policyList"  indexId="indexId">
								<tr id="id_tr<bean:write name="indexId"/>" <%= indexId % 2 == 0 ? "" : "class='odd'" %>
							     onmouseover="listOver('id_tr<bean:write name="indexId"/>',<%= indexId %>)"
								    onmouseout="listOut('id_tr<bean:write name="indexId"/>',<%= indexId %>,'<%= indexId % 2 == 0 ? "" : "odd" %>')"
								    onclick="listClick('id_tr<bean:write name="indexId"/>', <%= indexId %>);" >
									<bean:define id="clickIndex" name="policyBean" property="lineNo"/>
									<td><html:radio name="_PolicyForm" property="index" value="<%= indexId.toString() %>" onclick="radioIndex(this)"></html:radio></td>
									<bean:define id="poliNo" name="policyBean" property="lineNo"/>
									<td><bean:write name="policyBean" property="lineNo"/></td>
									<td><a href="javascript:jumpName(<%= indexId %>)"><bean:write name="policyBean" property="name"/></a></td>
									<td>
										<logic:iterate id="srcZoneBean" name="policyBean" property="sourceZoneList">
											<a href="javascript:jumpSource(<%= indexId %>)"><bean:write name="srcZoneBean" property="name"/></a>
										</logic:iterate>
									</td>
									<td>
										<logic:iterate id="dstZoneBean" name="policyBean" property="destinationZoneList" >
											<a href="javascript:jumpDestination(<%= indexId %>)"><bean:write name="dstZoneBean" property="name"/></a>
										</logic:iterate>
									</td>
					 				<td>
										<logic:iterate id="srcAddressBean" name="policyBean" property="sourceAddressList">
											<a href="javascript:jumpSrcAddress(<%= indexId %>)"><bean:write name="srcAddressBean" property="name"/></a>
										</logic:iterate>
									</td>
									<td>
										<logic:iterate id="dstAddressBean" name="policyBean" property="destinationAddressList">
											<a href="javascript:jumpDstAddress(<%= indexId %>)"><bean:write name="dstAddressBean" property="name"/></a>
										</logic:iterate>
									</td>
									<td>
										<logic:iterate id="appliBean" name="policyBean" property="applicationList">
											<a href="javascript:jumpAppli(<%= indexId %>)"><bean:write name="appliBean" property="name"/></a>
										</logic:iterate>
									</td>
<%-- 20110511 ktakenaka@PROSITE add start --%>
									<%-- WebフィルタONの場合 --%>
									<logic:match name="USER_VALUE_OBJECT"  property="urlFilteringFlg" value="1" >
										<td>
												<a href="javascript:jumpUrlFilter(<%= indexId %>)"><bean:write name="policyBean" property="urlFilterName"/></a>
										</td>
									</logic:match>
									<td>
										<logic:equal name="policyBean" property="virusCheckFlg" value="default">
											<a href="javascript:jumpVirusCheck(<%= indexId %>)">○</a>
										</logic:equal>
										<logic:equal name="policyBean" property="virusCheckFlg" value="none">
											<a href="javascript:jumpVirusCheck(<%= indexId %>)">×</a>
										</logic:equal>
									</td>
									<td>
										<logic:equal name="policyBean" property="spywareFlg" value="default">
											<a href="javascript:jumpSpyware(<%= indexId %>)">○</a>
										</logic:equal>
										<logic:equal name="policyBean" property="spywareFlg" value="none">
											<a href="javascript:jumpSpyware(<%= indexId %>)">×</a>
										</logic:equal>
									</td>
<%-- 20110511 ktakenaka@PROSITE add end --%>
									<td>
										<logic:iterate id="serviceBean" name="policyBean" property="serviceList">
											<a href="javascript:jumpService(<%= indexId %>)"><bean:write name="serviceBean" property="name"/></a>
										</logic:iterate>
									</td>
									<td>
										<logic:equal name="policyBean" property="ids_ips" value="default">
											<a href="javascript:jumpIDS_IPS(<%= indexId %>)">○</a>
										</logic:equal>
										<logic:equal name="policyBean" property="ids_ips" value="none">
											<a href="javascript:jumpIDS_IPS(<%= indexId %>)">×</a>
										</logic:equal>
									</td>
									<td><a href="javascript:jumpAction(<%= indexId %>)"><bean:write name="policyBean" property="action"/></a></td>
<%-- 20130901 add start kkato@PROSITE --%>
									<td><a href="javascript:jumpDisable(<%= indexId %>)"><bean:write name="policyBean" property="disableFlg"/></a></td>
<%-- 20130901 add end   kkato@PROSITE --%>
								</tr>
							</logic:iterate>
						</logic:notEmpty>
					</tbody>
				</table>
				<div id="operationBox1">
					ポリシーの移動：
					<logic:notEmpty name="_PolicyForm" property="policyList">
						<select name="move2" onchange="movePolicy(2)">
							<option value="">選択してください。</option>
							<option value="0">一番上へ移動</option>
							<option value="1">一番下へ移動</option>
							<option value="2">一つ上へ移動</option>
							<option value="3">一つ下へ移動</option>
						</select>
					</logic:notEmpty>
					<logic:empty name="_PolicyForm" property="policyList">
						<select name="move4" >
							<option value="">選択してください。</option>
							<option value="0">一番上へ移動</option>
							<option value="1">一番下へ移動</option>
							<option value="2">一つ上へ移動</option>
							<option value="3">一つ下へ移動</option>
						</select>
					</logic:empty>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="button" value="ポリシーの追加" onclick="document.location='<bean:write name='contextPath' />/customer/fwsetting/policysetting/AddPolicyBL.do'" />
					&nbsp;&nbsp;
					<logic:notEmpty name="_PolicyForm" property="policyList">
						<ts:submit value="ポリシーの削除" />
					</logic:notEmpty>
					<logic:empty name="_PolicyForm" property="policyList">
						<input type="button" value="ポリシーの削除"></input>
					</logic:empty>
				<!-- end #movePolicy --></div>
				<input type="hidden" name="selectIndex" value="0"/>
				<input type="hidden" name="moveType"/>

		</ts:form>
			<!-- end blockPolicy -->
		</div>
	<!-- end #content --></div>

</body>

<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
