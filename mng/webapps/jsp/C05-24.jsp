<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="スケジュール機能" />
<bean:define id="titleScreenName" scope="page" value="オブジェクト設定画面" />
<bean:define id="screenName" scope="page" value="FW設定機能" />
<bean:define id="screenID" scope="page" value="C05-24" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<script type="text/javascript">
<!--
// チェックボックス数
	var chkNum = 0;
	var list = new Array();
	var submit_flag = 0;
	function addTimes(){

		if(document._ScheduleForm.startDate.value != "" &&
				document._ScheduleForm.endDate.value != "" &&
					document._ScheduleForm.startTime.value != "" &&
						 document._ScheduleForm.endTime.value != "") {
			chkNum++;

			var out = document._ScheduleForm.startDate.value + "@" + document._ScheduleForm.startTime.value + "-" + document._ScheduleForm.endDate.value + "@" + document._ScheduleForm.endTime.value;
			var chkObj = document.createElement('input');
			var textObj = document.createTextNode(out);
			var brObj = document.createElement("br");

			chkObj.type="checkbox";
			chkObj.id="add"+chkNum;
			chkObj.value=chkNum;
			document._ScheduleForm.times.appendChild(chkObj);
			document._ScheduleForm.times.appendChild(textObj);
			document._ScheduleForm.times.appendChild(brObj);
			document._ScheduleForm.startTime.value = "";
			document._ScheduleForm.endTime.value = "";
			document._ScheduleForm.startDate.value = "";
			document._ScheduleForm.endDate.value = "";
		}
	}

	function delTimes(){

		var elem = document._ScheduleForm.times;
		var counter = 0;

		for (i = elem.childNodes.length-1; i >= 0; i--) {
			if(elem.childNodes[i].nodeName == "INPUT"){
				if(elem.childNodes[i].checked){
					elem.removeChild(elem.childNodes[i+2]);
					elem.removeChild(elem.childNodes[i+1]);
					elem.removeChild(elem.childNodes[i]);
					chkNum--;
					counter++;
				}
			}
		}
	}

	function jump(){
		document._ScheduleForm.selectRecurrence.value = document._ScheduleForm.recurrence.options[document._ScheduleForm.recurrence.selectedIndex].value;
		document._ScheduleForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/ChangeAddReccurenceBL" />";
        document._ScheduleForm.submit();
	}
	function regist(){
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		var timeArray = new Array();
		var timeArray2 = new Array();
		var startTimeStr = new Array();
		var startDateStr = new Array();
		var endDateStr = new Array();
		var endTimeStr = new Array();

		var str = "";
		var j = 0;
		var elem = document._ScheduleForm.times;
		for (i = elem.childNodes.length-1; i >= 0; i--) {
			if(elem.childNodes[i].nodeName == "INPUT"){
				timeArray = elem.childNodes[i+1].nodeValue.split("-");
				timeArray2 = timeArray[0].split("@");
				startDateStr[j] = timeArray2[0];
				startTimeStr[j] = timeArray2[1];
				timeArray2 = timeArray[1].split("@");
				endDateStr[j] = timeArray2[0];
				endTimeStr[j] = timeArray2[1];
				j++;
			}
		}

		for(i =0; i<startDateStr.length; i++) {
			make_hidden('startDateList[]', startDateStr[i], '_ScheduleForm');
			make_hidden('endDateList[]', endDateStr[i], '_ScheduleForm');
			make_hidden('startTimeList[]', startTimeStr[i], '_ScheduleForm');
			make_hidden('endTimeList[]', endTimeStr[i], '_ScheduleForm');
		}

        document._ScheduleForm.submit();

	}

	function make_hidden( name, value, formname ){
	    var q = document.createElement('input');
	    q.type = 'hidden';
	    q.name = name;
	    q.value = value;
		if (formname){ document.forms[formname].appendChild(q); }
	    else{ document.forms[0].appendChild(q); }
	}
	function cancelClick() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document._ScheduleForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/SchedulesBL" />";
	    document._ScheduleForm.submit();
	}
	// -->

</script>
<script type="text/javascript">
	<!--
		var localja = true;
		var jscalendarMonthName = new Array("1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月");
		var jscalendarDayName = new Array("日","月","火","水","木","金","土");
		var jscalendarTodayString = "今日は";
		var jscalendarThemePrefix = "BlueStyle";
		var jscalendarImgDir = "<bean:write name='contextPath' />/img/calendar/BlueStyle/";
	-->
</script>
<script src="<bean:write name='contextPath' />/js/calendar/InputCalendar2.js" type=text/javascript></script>
<script type=text/javascript>
		<!--
			jscalendarInit();
		-->
</script>
<link href="<bean:write name='contextPath' />/css/calendar/BlueStyleInputCalendar.css" type=text/css rel=stylesheet />
<body>
	<div id="content"><!-- InstanceBeginEditable name="content" -->
		<div id="objectSchedules">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; オブジェクト設定 &raquo; スケジュール設定 &raquo; スケジュール追加</p>
			<h3>スケジュール追加</h3>
			<ts:form action="/customer/fwsetting/objectsetting/RegistSchedulesNonRecurringBL"  method="post" >
				<dl>
					<dt>オブジェクト名</dt>
					<dd><html:text name="_ScheduleForm" property="name" /></dd>
					<dt>周期</dt>
					<dd>
						<select name="recurrence" onchange="jump()">
						<option value="0">デイリー</option>
						<option value="1">ウィークリー</option>
						<option value="2" selected="selected">指定日時</option>
						</select>
					</dd>

					<dt>開始日: </dt>
					<dd><input type="text" name="startDate" onclick="jscalendarPopUpCalendar(this,this,'yyyy/MM/dd');" readonly="readonly"/> </dd>
					<dt>開始時刻: </dt>
					<dd><input type="text" name="startTime"/> (00:00 to 23:59)</dd>
					<dt>終了日: </dt>
					<dd><input type="text" name="endDate" onclick="jscalendarPopUpCalendar(this,this,'yyyy/MM/dd');" readonly="readonly"/> </dd>
					<dt>終了時刻: </dt>
					<dd><input type="text" name="endTime"/> (00:00 to 23:59)<br /><input type="button" value="スケジュールを追加" onclick="addTimes()"/></dd>
					<dt>設定日時</dt>
					<dd>
						<textarea name="times" cols="" rows="5" readonly="readonly"></textarea>
						<br /><input type="button" value="スケジュールの削除" onclick="delTimes()"/>
					</dd>
				</dl>

				<ul id="button">
					<li><input type="button" value="OK"  onclick= "regist()"/></li>
					<li><input type="button" value="Cancel" onclick="cancelClick()"/></li>
				</ul>
				<input type="hidden" name="selectRecurrence" />
			</ts:form>


		<!-- end #objectSchedules --></div>
	<!-- InstanceEndEditable -->
	<!-- end #content --></div>

</body>

<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
