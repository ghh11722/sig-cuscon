<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お客様企業メイン" />
<bean:define id="titleScreenName" scope="page" value="ポリシー一覧" />
<bean:define id="screenName" scope="page" value="Service編集" />
<bean:define id="screenID" scope="page" value="C04-11" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<script>
	var submit_flag = 0;
	var chkNum = 0;
	var indexList = new Array();
	var srvMstList = new Array();
	var mstNum = 0;

	function radioClick(size) {
		if(document._PolicyForm.ServiceRadio[0].checked || document._PolicyForm.ServiceRadio[1].checked) {
			if(size == 1) {
				document._PolicyForm.checkSrv.disabled = true;
			} else {
				for(i=0; i<size; i++) {
					document._PolicyForm.checkSrv[i].disabled = true;
				}
			}
		} else {
			if(size == 1) {
				document._PolicyForm.checkSrv.disabled = false;
			} else {

				for(i=0; i<size; i++) {
					document._PolicyForm.checkSrv[i].disabled = false;
				}
			}
		}
	}

	function getCheck(obj,index){
		if (obj.checked) {
			chkNum++;
			indexList.push(index);
		} else {
			chkNum--;
			for(i=0;i<indexList.length;i++) {
				if(indexList[i]==index) {
					indexList.splice(i, 1);
					i--;
				}
			}
		}
	}

	function getCheckList(){
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		var str = "";
        for(i =0; i<indexList.length; i++) {
            if(i==0) {
				str = indexList[i];
            } else {
            	str = str + "," + indexList[i];
            }
        }
        document._PolicyForm.checkList.value = str;

		if(document._PolicyForm.ServiceRadio[0].checked) {
        	document._PolicyForm.radio.value = 0;
		} else if(document._PolicyForm.ServiceRadio[1].checked) {
			document._PolicyForm.radio.value = 1;
		} else {
			document._PolicyForm.radio.value = 2;
		}

        document._PolicyForm.submit();
	}

	function cancelClick() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document._PolicyForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/PolicyBL" />";
	    document._PolicyForm.submit();
	}
</script>
<body>
	<div id="content">
		<div id="editService">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ポリシー設定 &raquo; サービス設定</p>

			<h3>サービス設定</h3>
			<ts:form action="/customer/fwsetting/policysetting/UpdateServiceBL" method="post" >
				<bean:size id="serviceSize" name="_PolicyForm" property="serviceMasterList" />
				<div id="anyOrSelect">
					<ul>
						<logic:equal name="_PolicyForm" property="serviceName" value="any">
							<li><input name="ServiceRadio" type="radio" checked="checked" onclick="radioClick(<bean:write name="serviceSize" />)"/>any</li>
							<li><input name="ServiceRadio" type="radio" onclick="radioClick(<bean:write name="serviceSize" />)"/>application-default</li>
							<li><input name="ServiceRadio" type="radio" onclick="radioClick(<bean:write name="serviceSize" />)"/>選択する</li>
						</logic:equal>
						<logic:equal name="_PolicyForm" property="serviceName" value="application-default">
							<li><input name="ServiceRadio" type="radio" onclick="radioClick(<bean:write name="serviceSize" />)"/>any</li>
							<li><input name="ServiceRadio" type="radio" checked="checked" onclick="radioClick(<bean:write name="serviceSize" />)"/>application-default</li>
							<li><input name="ServiceRadio" type="radio" onclick="radioClick(<bean:write name="serviceSize" />)"/>選択する</li>
						</logic:equal>
						<logic:notEqual name="_PolicyForm" property="serviceName" value="any">
							<logic:notEqual name="_PolicyForm" property="serviceName" value="application-default">
								<li><input name="ServiceRadio" type="radio" onclick="radioClick(<bean:write name="serviceSize" />)"/>any</li>
								<li><input name="ServiceRadio" type="radio" onclick="radioClick(<bean:write name="serviceSize" />)"/>application-default</li>
								<li><input name="ServiceRadio" type="radio" checked="checked" onclick="radioClick(<bean:write name="serviceSize" />)"/>選択する</li>
							</logic:notEqual>
						</logic:notEqual>
					</ul>
				</div>

			<div id="available">
				<h4>オブジェクトの選択</h4>
					<ul>
						<logic:iterate id="serviceMstBean" name="_PolicyForm" property="serviceMasterList"  indexId="index">
							<li>
								<logic:equal name="_PolicyForm" property="serviceName" value="any">
									<input name="checkSrv" type="checkbox"  onclick="getCheck(this, <%= index %>)" disabled="disabled"/>
								</logic:equal>
								<logic:equal name="_PolicyForm" property="serviceName" value="application-default">
									<input name="checkSrv" type="checkbox"  onclick="getCheck(this, <%= index %>)" disabled="disabled"/>
								</logic:equal>
								<logic:notEqual name="_PolicyForm" property="serviceName" value="any">
									<logic:notEqual name="_PolicyForm" property="serviceName" value="application-default">
										<input name="checkSrv" type="checkbox"  onclick="getCheck(this, <%= index %>)"/>
									</logic:notEqual>
								</logic:notEqual>
								<logic:equal name="serviceMstBean" property="objectType" value="0">
									<img alt="" src="../../../img/group_off.png" width="13" height="13"/>
								</logic:equal>
								<logic:equal name="serviceMstBean" property="objectType" value="1">
									<img alt="" src="../../../img/group_off.png" width="13" height="13"/>
								</logic:equal>
								<logic:equal name="serviceMstBean" property="objectType" value="2">
									<img alt="" src="../../../img/group_on.png" width="13" height="13"/>
								</logic:equal>
								<bean:write name="serviceMstBean"  property="name"/><br/>
							</li>
							<script type="text/javascript">
								{
									srvMstList[mstNum] = "<bean:write name='serviceMstBean'  property='name'/>";
									mstNum++;
								}
							</script>
						</logic:iterate>
						<logic:iterate id="serviceBean" name="_PolicyForm" property="serviceList"  indexId="index">
							<script type="text/javascript">
								{
									if(srvMstList.length == 1) {
										if(srvMstList[0] == "<bean:write name='serviceBean'  property='name'/>") {
											document._PolicyForm.checkSrv.checked=true;
											indexList[chkNum] = 0;
											chkNum++;
										}
									} else {
										for(i=0; i<srvMstList.length; i++) {
											if(srvMstList[i] == "<bean:write name='serviceBean'  property='name'/>") {
												document._PolicyForm.checkSrv[i].checked=true;
												indexList[chkNum] = i;
												chkNum++;
											}
										}
									}
								}
							</script>
						</logic:iterate>
					</ul>
				<!-- end #available --></div>

				<ul id="button">
					<li><input type="button" value="OK" onclick="getCheckList()"/></li>
					<li><input type="button" value="Cancel" onclick="cancelClick()"/></li>
				</ul>
				<input type="hidden" name="name" value="<bean:write name="_PolicyForm" property="name"/>"/>
				<input type="hidden" name="radio" value="0"/>
				<input type="hidden" name="checkList" />
			</ts:form>
		<!-- end editService -->
		</div>
	<!-- end #content --></div>

</body>

<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
