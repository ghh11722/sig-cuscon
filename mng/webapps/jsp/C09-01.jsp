<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<%@ page import="jp.co.kddi.secgw.cuscon.common.CusconUVO" %>

<bean:define id="titleUsecaseName" scope="page" value="パスワード変更機能" />
<bean:define id="titleScreenName" scope="page" value="パスワード変更画面" />
<bean:define id="screenName" scope="page" value="パスワード変更" />
<bean:define id="screenID" scope="page" value="0001" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true" lang="true">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
  <%
	CusconUVO uvo = (CusconUVO)session.getAttribute( "USER_VALUE_OBJECT" );
  %>

<script type="text/javascript">
<!--
function clearForm() {
	   document.getElementsByName('old_password')[0].value = '';
	   document.getElementsByName('new_password')[0].value = '';
	   document.getElementsByName('password_check')[0].value = '';
}
// -->
</script>

<div id="content">
	<div id="passwordChange">
		<p id="pankuzu">Top &raquo; パスワード管理 &raquo; パスワード変更</p>
		<h3><bean:write name="screenName" scope="page"/></h3>
		<ts:form action="/customer/password/changeBL" >
		<dl id="customerPasspwordchange">
			<dt>ログイン ID</dt>
			<dd><%= uvo.getLoginId() %></dd>
			<dt>ファイアウォールID</dt>
			<dd><bean:write name="_PasswordChangeForm" property="firewallId" /></dd>
			<dt>現在のパスワード</dt>
			<dd><html:password property="old_password" maxlength="14" size="25" /></dd>
			<dt>変更後のパスワード</dt>
			<dd><html:password property="new_password" maxlength="14" size="25" /></dd>
			<dt>変更後のパスワード(再入力)</dt>
			<dd><html:password property="password_check" maxlength="14" size="25" /></dd>
		</dl>
		<ul id="button">
			<li><ts:submit value="OK" /></li>
			<li><html:button property="reset" value="Clear" onclick="clearForm()" /></li>
		</ul>
		</ts:form>
	</div>
</div>
<logic:equal name="_PasswordChangeForm" property="resultmsg" value="success">
<script language="JavaScript" type="text/JavaScript">
<!--
alert('<ts:messages id="message" message="true"><bean:write name="message"/></ts:messages>');
//-->
</script>
</logic:equal>
<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
