<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="メイン画面" />
<bean:define id="screenName" scope="page" value="企業管理者メイン" />
<bean:define id="screenID" scope="page" value="C05-41" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<script type="text/javascript">
	<!--
	// ブロック時動作セレクトボックス生成用
	var blkActNum = 0;

	// 全カテゴリ適用セレクトボックス生成用
	var allCtgrActNum = 0;

	// Webフィルタカテゴリセレクトボックス生成用
	var actionValueLst = new Array();
	var actionTextLst = new Array();
	var valNum = 0;
	var txtNum = 0;

	// 変更前カテゴリアクション
	var oldActionList = new Array();
	var oldActNum = 0;

	// 選択したアクションを全てのカテゴリに適用
	function selectAllActionCtgrClick() {
		<%-- 20120827 kkato@PROSITE add start --%>
		if( confirm("選択したアクションを全てのカテゴリに適用します。よろしいですか？") == false ) return;
		<%-- 20120827 kkato@PROSITE add end --%>
		var selValue = document.geneForm.selectAllCategoryAction.options[document.geneForm.selectAllCategoryAction.options.selectedIndex].value;
		for(var i=0; i<document.geneForm.selectCategoryAction.length; i++) {
			for(var j=0; j<document.geneForm.selectCategoryAction[i].options.length; j++) {
				if(selValue == document.geneForm.selectCategoryAction[i].options[j].value) {
					document.geneForm.selectCategoryAction[i].options.selectedIndex = j;
					break;
				}
			}
		}
	}

	var submit_flag = 0;
	function clickOk()
	{
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;

		var str = "";
		var selObj = document.geneForm.selectCategoryAction;
		for(i=0;i<selObj.length;i++) {
			if(i==0) {
				str = selObj[i].options[selObj[i].options.selectedIndex].value;
			} else {
				str = str + "," + selObj[i].options[selObj[i].options.selectedIndex].value;
			}
		}
		document.geneForm.selectActionList.value = str;

		document.geneForm.actionBlockListSite.value = document.geneForm.selectActionBlockListSite.options[document.geneForm.selectActionBlockListSite.options.selectedIndex].value;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/UpdateUrlFilterBL" />";
        document.geneForm.submit();
	}
	function cancelClick() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/UrlFiltersBL" />";
	    document.geneForm.submit();
	}
	-->
</script>
<body onunload="unLoadWindow()">
	<div id="content">
		<div id="objectUrlFilters">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; オブジェクト設定 &raquo; Webフィルタ設定 &raquo; Webフィルタ編集</p>
			<h3>Webフィルタ編集</h3>
			<form action="" name="geneForm" method="post">
				<input type="hidden" name="oldUrlFilterName" value="<bean:write name="_UrlFiltersForm" property="name"/>"/>
				<input type="hidden" name="actionBlockListSite" />
				<input type="hidden" name="selectActionList" />
				<table border="1" align="left" width="100%">
					<tr>
						<td width="40%">
							<table>
								<tr>
									<td><p align="left">オブジェクト名</p></td>
									<td><p align="left"><input name="newUrlFilterName" size="30"  value="<bean:write name="_UrlFiltersForm" property="name"/>"/></p></td>
								</tr>
								<tr>
									<td><p align="left">説明</p></td>
									<td><p align="left"><input type="text" name="comment" size="40" value="<bean:write name="_UrlFiltersForm" property="comment"/>" /></p></td>
								</tr>
							</table>
							<table>
								<tr>
									<td style="text-align: left;">
										<h4><p align="left">ブロックリスト</p></h4>
										<p align="left"><textarea name="newBlockListSite" rows="10" cols="60" ><bean:write name="_UrlFiltersForm" property="blockListSite"/></textarea></p>
										<br/>
										<p align="left">アクション</p>
										<SELECT NAME="selectActionBlockListSite" align="left">
										</SELECT>
										<!-- 動作セレクトボックスを動的に生成 -->
										<logic:notEmpty name="_UrlFiltersForm" property="blockActionList">
											<logic:iterate id="blkActBean" name="_UrlFiltersForm" property="blockActionList" indexId="index" >
												<script type="text/javascript">
													{
														document.geneForm.selectActionBlockListSite.length++;
														document.geneForm.selectActionBlockListSite.options[blkActNum].value ="<bean:write name='blkActBean' property='value'/>";
														document.geneForm.selectActionBlockListSite.options[blkActNum].text  ="<bean:write name='blkActBean' property='text'/>";
														blkActNum++;
													}
												</script>
											</logic:iterate>
										</logic:notEmpty>
										<script type="text/javascript">
											{
												for(var j=0; document.geneForm.selectActionBlockListSite.options.length; j++) {
													if(document.geneForm.selectActionBlockListSite.options[j].value
														== "<bean:write name='_UrlFiltersForm' property='actionBlockListSite'/>") {
														document.geneForm.selectActionBlockListSite.options.selectedIndex = j;
														break;
													}
												}
											}
										</script>
									</td>
								</tr>
							</table>
							<table>
								<tr>
									<td>
										<h4><p align="left">許可リスト</p></h4>
										<p align="left"><textarea name="newAllowListSite" rows="10" cols="60" ><bean:write name="_UrlFiltersForm" property="allowListSite"/></textarea></p>
									</td>
								</tr>
							</table>
							<%-- 20130901 kkato@PROSITE del
							<table>
								<tr><td>
<p align="left"><font sise=-1>＜ブロックリスト および 許可リスト 入力時のご注意事項＞<br>
・文字列には「"」を含めないでください。また、URLの「http[s]://」部分は、必ず省略してください。入力しても正しく動作できません。<br>
・複数のサイトを入力する場合には、1サイト毎に「Enter」で改行してください。</font></p>
								</td></tr>
							</table>
							--%>
						</td>
						<td width="60%" valign="top">
							<div id="urlAllChangeCategory">
								<p>選択したアクションを全てのカテゴリに適用</p>
								<%-- 20120827 kkato@PROSITE del
								<SELECT NAME="selectAllCategoryAction" onChange="selectAllActionCtgrClick()">
								--%>
								<%-- 20120827 kkato@PROSITE add start --%>
								<SELECT NAME="selectAllCategoryAction" onChange="">
								<%-- 20120827 kkato@PROSITE add end --%>
									<!--
									<OPTION VALUE="select">--select--</OPTION>
									<OPTION VALUE="alert">alert</OPTION>
									<OPTION VALUE="continue">continue</OPTION>
									<OPTION VALUE="block">block</OPTION>
									 -->
								</SELECT>
								<!-- 全カテゴリ適用セレクトボックスを動的に生成 START -->
								<script type="text/javascript">
									{
										document.geneForm.selectAllCategoryAction.length++;
										document.geneForm.selectAllCategoryAction.options[allCtgrActNum].value ="";
										document.geneForm.selectAllCategoryAction.options[allCtgrActNum].text  ="--select--";
										allCtgrActNum++;
									}
								</script>
								<logic:notEmpty name="_UrlFiltersForm" property="categoryActionList">
									<logic:iterate id="allCtgrActBean" name="_UrlFiltersForm" property="categoryActionList" indexId="index" >
										<script type="text/javascript">
											{
												document.geneForm.selectAllCategoryAction.length++;
												document.geneForm.selectAllCategoryAction.options[allCtgrActNum].value ="<bean:write name='allCtgrActBean' property='value'/>";
												document.geneForm.selectAllCategoryAction.options[allCtgrActNum].text  ="<bean:write name='allCtgrActBean' property='text'/>";
												allCtgrActNum++;
											}
										</script>
									</logic:iterate>
								</logic:notEmpty>
								<%-- 20120827 kkato@PROSITE add start --%>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" value="OK" onclick="selectAllActionCtgrClick()" />
								<%-- 20120827 kkato@PROSITE add end --%>
							</div>
							<br/>
							<div id="urlFilteringCategory" style="height:550px;width:440px; overflow-y:auto;">
								<table>
									<thead>
										<tr class="odd">
											<th scope="col">カテゴリー</th>
											<th scope="col">アクション</th>
										</tr>
									</thead>
									<tbody id="id_customers">
										<logic:notEmpty name="_UrlFiltersForm" property="categoryList">
											<logic:iterate id="categoryBean" name="_UrlFiltersForm" property="categoryList" indexId="index" >
												<tr id="id_tr<bean:write name="index"/>" <%= index % 2 == 0 ? "" : "class='odd'" %>
							    					onclick="listClick('id_tr<bean:write name="index"/>',<%= index %>);" >
													<td><bean:write name="categoryBean" property="name" /></td>
													<td>
															<SELECT NAME="selectCategoryAction">
															<!--
															<OPTION VALUE="alert">alert</OPTION>
															<OPTION VALUE="continue">continue</OPTION>
															<OPTION VALUE="block">block</OPTION>
															 -->
														</SELECT>
														<script type="text/javascript">
															{
																oldActionList[oldActNum]  ="<bean:write name='categoryBean' property='action' />";
																oldActNum++;
															}
														</script>
													</td>
												</tr>
											</logic:iterate>
											<!-- カテゴリアクションセレクトボックス用の値とテキストを取得  -->
											<logic:notEmpty name="_UrlFiltersForm" property="categoryActionList">
												<logic:iterate id="ctgrActBean" name="_UrlFiltersForm" property="categoryActionList">
													<script type="text/javascript">
														{
															actionValueLst[valNum]  ="<bean:write name='ctgrActBean' property='value'/>";
															actionTextLst[txtNum]  ="<bean:write name='ctgrActBean' property='text'/>";
															valNum++;
															txtNum++;
														}
													</script>
												</logic:iterate>
											</logic:notEmpty>

											<!-- カテゴリアクションセレクトボックスを動的に生成 -->
											<script type="text/javascript">
												{
													for(var i=0; i<document.geneForm.selectCategoryAction.length; i++) {
														var selObj = document.geneForm.selectCategoryAction[i];
														selObj.length = 0;
												        for( var j=0; j < actionValueLst.length; j++) {
												            selObj.length++;
												            selObj.options[ selObj.length - 1].value = actionValueLst[j];
												            selObj.options[ selObj.length - 1].text  = actionTextLst[j];
												        }
													}

													// 選択状態を設定
													for(var i=0; i<oldActionList.length; i++) {
														for(var j=0; j<document.geneForm.selectCategoryAction[i].options.length; j++) {
															if(oldActionList[i] == document.geneForm.selectCategoryAction[i].options[j].value) {
																document.geneForm.selectCategoryAction[i].options.selectedIndex = j;
																break;
															}
														}
													}
												}
											</script>
										</logic:notEmpty>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
				</table>

				<ul id="button">
					<li><input type="button" value="OK" onclick="clickOk()" /></li>
					<li><input type="button" value="Cancel" onclick="cancelClick()"/></li>
				</ul>
 			</form>
		</div>
	</div>
</body>
<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
