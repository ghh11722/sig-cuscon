<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="メイン画面" />
<bean:define id="screenName" scope="page" value="企業管理者メイン" />
<bean:define id="screenID" scope="page" value="C05-026" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>

<head>
	<script type="text/javascript">
	<!--
	var submit_flag = 0;
	function checkAddress()
	{
		document.geneForm.type.value ="ip-netmask";
	}

	function checkRange()
	{
		document.geneForm.type.value ="ip-range";
	}

	function checkFqdn()
	{
		document.geneForm.type.value ="fqdn";
	}

	function clickOk()
	{
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		if( document.geneForm.type.value =="ip-netmask" )
		{
			document.geneForm.address.value = document.geneForm.input_Address.value;
		}else if( document.geneForm.type.value =="ip-range" )
		{
			document.geneForm.address.value = document.geneForm.input_Range.value;
		}else if( document.geneForm.type.value =="fqdn" )
		{
			document.geneForm.address.value = document.geneForm.input_Fqdn.value;
		}
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/UpdateAddressBL" />";
        document.geneForm.submit();
	}

	function cancelClick() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/AddressesBL" />";
	    document.geneForm.submit();
	}
	-->
	</script>
</head>
<body>
	<div id="content"><!-- InstanceBeginEditable name="content" -->
		<div id="objectAddress">
		<p id="pankuzu">Top &raquo; FW設定機能 &raquo; オブジェクト設定 &raquo; アドレス設定 &raquo; アドレス編集</p>
		<h3>アドレス編集</h3>
		<form action="" name="geneForm" method="post">
			<input type="hidden" name="oldAddressName" value="<bean:write name="_AddressesForm" property="name"/>"/>
			<input type="hidden" name="address" value=""/>
			<dl>
				<dt>オブジェクト名</dt>
				<dd><input name="newAddressName" value="<bean:write name="_AddressesForm" property="name"/>"/></dd>

				<logic:equal name="_AddressesForm" property="type" value="ip-netmask">
					<dt><input name="Address/Range" type="radio" onclick="checkAddress()" value="check_Address" checked="checked"/>IPアドレス</dt>
					<dd><input name="input_Address" value="<bean:write name="_AddressesForm" property="address"/>"/><br />
				(e.g. 「192.168.80.150」 or 「192.168.80.0/24」)</dd>
					<input type="hidden" name="type" value="ip-netmask"/>
				</logic:equal>

				<logic:equal name="_AddressesForm" property="type" value="ip-range">
					<dt><input name="Address/Range" type="radio" onclick="checkAddress()" value="check_Address"/>IPアドレス</dt>
					<dd><input name="input_Address" value=""/><br />
				(e.g. 「192.168.80.150」 or 「192.168.80.0/24」)</dd>
					<input type="hidden" name="type" value="ip-range"/>
				</logic:equal>
				<logic:equal name="_AddressesForm" property="type" value="fqdn">
					<dt><input name="Address/Range" type="radio" onclick="checkAddress()" value="check_Address"/>IPアドレス</dt>
					<dd><input name="input_Address" value=""/><br />
				(e.g. 「192.168.80.150」 or 「192.168.80.0/24」)</dd>
					<input type="hidden" name="type" value="fqdn"/>
				</logic:equal>

				<logic:equal name="_AddressesForm" property="type" value="ip-netmask">
					<dt><input name="Address/Range" type="radio" onclick="checkRange()" value="check_Range"/>IPレンジ</dt>
					<dd><input name="input_Range" value="" size="50"/><br />
				(e.g. 「192.168.80.10-192.168.80.20」)</dd>
				</logic:equal>

				<logic:equal name="_AddressesForm" property="type" value="ip-range">
					<dt><input name="Address/Range" type="radio" onclick="checkRange()" value="check_Range" checked="checked"/>IPレンジ</dt>
					<dd><input name="input_Range" value="<bean:write name="_AddressesForm" property="address"/>" size="50"/><br />
				(e.g. 「192.168.80.10-192.168.80.20」)</dd>
				</logic:equal>
				<logic:equal name="_AddressesForm" property="type" value="fqdn">
					<dt><input name="Address/Range" type="radio" onclick="checkRange()" value="check_Range"/>IPレンジ</dt>
					<dd><input name="input_Range" value="" size="50"/><br />
				(e.g. 「192.168.80.10-192.168.80.20」)</dd>
				</logic:equal>
				<logic:equal name="_AddressesForm" property="type" value="ip-netmask">
					<dt><input name="Address/Range" type="radio" onclick="checkFqdn()" value="check_Fqdn"/>FQDN</dt>
					<dd><input name="input_Fqdn" value="" size="50"/><br />
				(e.g. 「www.kddi.com」)</dd>
				</logic:equal>

				<logic:equal name="_AddressesForm" property="type" value="ip-range">
					<dt><input name="Address/Range" type="radio" onclick="checkFqdn()" value="check_Fqdn"/>FQDN</dt>
					<dd><input name="input_Fqdn" value="" size="50"/><br />
				(e.g. 「www.kddi.com」)</dd>
				</logic:equal>

				<logic:equal name="_AddressesForm" property="type" value="fqdn">
					<dt><input name="Address/Range" type="radio" onclick="checkFqdn()" value="check_Fqdn" checked="checked"/>FQDN</dt>
					<dd><input name="input_Fqdn" value="<bean:write name="_AddressesForm" property="address"/>" size="50"/><br />
				(e.g. 「www.kddi.com」)</dd>
				</logic:equal>

			</dl>
			<ul id="button">
				<li><input type="button" value="OK" onclick="clickOk()" /></li>
				<li><input type="button" value="Cancel" onclick="cancelClick()"/></li>
			</ul>
		</form>
		<!-- #objectAddress --></div>
	<!-- InstanceEndEditable -->
	<!-- end #content --></div>
</body>
<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
