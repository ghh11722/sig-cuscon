<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="メイン画面" />
<bean:define id="screenName" scope="page" value="企業管理者メイン" />
<bean:define id="screenID" scope="page" value="C03-01" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true" lang="true">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>

<div id="content">
	<div id="top">
	<div id="information">
		<h3>お知らせ</h3>
		<pre><bean:write filter="false" name="_CustomerMainForm" property="infomation"/></pre>
	</div>
	<div id="manual">
		<h3>マニュアルダウンロード</h3>
		<ul>
			<li>&raquo; <a target="blank" href="<bean:write name='contextPath' />/manual/AttentionNote.pdf">ご利用にあたって</a></li>
			<li>&raquo; <a target="blank" href="<bean:write name='contextPath' />/manual/EasyOperationManual.pdf">簡易操作マニュアル</a></li>
			<li>&raquo; <a target="blank" href="<bean:write name='contextPath' />/manual/OperationManual.pdf">操作マニュアル</a></li>
			<li>&raquo; <a target="blank" href="<bean:write name='contextPath' />/manual/VSystemManual.pdf">仮想システムマニュアル</a></li>
			<li>&raquo; <a target="blank" href="<bean:write name='contextPath' />/manual/WebFilterManual.pdf">Webフィルタマニュアル</a></li>
		</ul>
	</div>
<%-- 20130901 add start kkato@PROSITE --%>
	<logic:notEmpty name="_CustomerMainForm"  property="urlDbVersion">
	<p>URL-DBバージョン:&nbsp;<bean:write name="_CustomerMainForm" property="urlDbVersion"/></p>
	</logic:notEmpty>
<%-- 20130901 add end   kkato@PROSITE --%>
	</div>
</div>

<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
