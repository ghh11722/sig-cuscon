<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お客様企業メイン" />
<bean:define id="titleScreenName" scope="page" value="ポリシー一覧" />
<bean:define id="screenName" scope="page" value="Webフィルタ編集" />
<bean:define id="screenID" scope="page" value="C04-17" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<script>
	var submit_flag = 0;
	var selectedIndex = -1;
	var urlNum = 0;
	var urlFilLst = new Array();

	// Webフィルタオブジェクトラジオボタンの使用可否設定
	function setUrlRadioDisabled(size) {
		if(document._PolicyForm.selectRadio[0].checked) {
    		if(size == 1) {
				document._PolicyForm.urlRadio.disabled = true;
			} else {
				for(i=0; i<size; i++) {
					document._PolicyForm.urlRadio[i].disabled = true;
				}
			}
		} else {
			if(size == 1) {
				document._PolicyForm.urlRadio.disabled = false;
			} else {
				for(i=0; i<size; i++) {
					document._PolicyForm.urlRadio[i].disabled = false;
				}
			}
		}
	}

	// Webフィルタ　ラジオボタン選択時
	function objectRadioClick(index){
		selectedIndex = index;
	}

	// OKボタン押下時
	function okClick(){
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		if(document._PolicyForm.selectRadio[0].checked) {
			document._PolicyForm.radio.value = 1;
		} else {
			document._PolicyForm.radio.value = 2;
		}
		document._PolicyForm.urlFilterSelectedIndex.value = selectedIndex;
	    document._PolicyForm.submit();
	}

	// Cancelボタン押下時
	function cancelClick() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document._PolicyForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/PolicyBL" />";
	    document._PolicyForm.submit();
	}
</script>
<body>
	<div id="content">
		<div id="editUrlFilter">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ポリシー設定 &raquo; Webフィルタ設定</p>

			<h3>Webフィルタ設定</h3>
			<ts:form action="/customer/fwsetting/policysetting/UpdateUrlFilterBL" method="post" >
				<bean:size id="urlFilterSize" name="_PolicyForm" property="urlFilterList" />
				<div id="anyOrSelect">
					<ul>
						<logic:equal name="_PolicyForm" property="urlfilterDefaultFlg" value="0">
							<li><input name="selectRadio" type="radio" checked="checked" onclick="setUrlRadioDisabled(<bean:write name="urlFilterSize" />)" />none</li>
							<li><input name="selectRadio" type="radio" onclick="setUrlRadioDisabled(<bean:write name="urlFilterSize" />)" />選択する</li>
						</logic:equal>
						<logic:equal name="_PolicyForm" property="urlfilterDefaultFlg" value="1">
							<li><input name="selectRadio" type="radio" onclick="setUrlRadioDisabled(<bean:write name="urlFilterSize" />)"/>none</li>
							<li><input name="selectRadio" type="radio" checked="checked" onclick="setUrlRadioDisabled(<bean:write name="urlFilterSize" />)"/>選択する</li>
						</logic:equal>
					</ul>
				</div><!-- end anyOrSelect -->
				<div id="available">
					<h4>&raquo; オブジェクトの選択</h4>
					<ul>
						<logic:iterate id="urlListBean" name="_PolicyForm" property="urlFilterList"  indexId="index">
							<li>
								<input name="urlRadio" type="radio"  onclick="objectRadioClick(<%= index %>)" />
								<bean:write name="urlListBean" property="name"/><br/>
							</li>
							<script type="text/javascript">
								{
									urlFilLst[urlNum] = "<bean:write name='urlListBean' property='name'/>";
									urlNum++;
								}
							</script>
						</logic:iterate>

							<!-- Webフィルタオブジェクトラジオボタンの使用可否設定 -->
						<script type="text/javascript">
							{
								setUrlRadioDisabled(<bean:write name="urlFilterSize" />);
							}
						</script>

							<!-- Webフィルタオブジェクトラジオボタンをチェック -->
						<logic:iterate id="urlListBean" name="_PolicyForm" property="urlFilterList"  indexId="index">
							<script type="text/javascript">
								{
									for(i=0; i<urlFilLst.length; i++) {
										if(urlFilLst[i] == "<bean:write name='_PolicyForm' property='urlFilterName'/>") {
											if(urlFilLst.length == 1) {
												document._PolicyForm.urlRadio.checked = true;
											} else {
												document._PolicyForm.urlRadio[i].checked=true;
											}
											selectedIndex = i;
										}
									}
								}
							</script>
						</logic:iterate>
					</ul>
				<!-- end #available --></div>

				<ul id="button">
					<li><input type="button" value="OK" onclick="okClick()"/></li>
					<li><input type="button" value="Cancel" onclick="cancelClick()"/></li>
				</ul>
				<input type="hidden" name="name" value="<bean:write name="_PolicyForm" property="name"/>"/>
				<input type="hidden" name="radio" value="<bean:write name="_PolicyForm" property="urlfilterDefaultFlg"/>"/>
				<input type="hidden" name="urlFilterSelectedIndex" value="0"/>
			</ts:form>
		<!-- editURLFilter -->
		</div>
	<!-- end #content --></div>
</body>

<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
