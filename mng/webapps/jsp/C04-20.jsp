<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お客様企業メイン" />
<bean:define id="titleScreenName" scope="page" value="ポリシー一覧" />
<bean:define id="screenName" scope="page" value="セキュリティルール有効/無効編集" />
<bean:define id="screenID" scope="page" value="C04-20" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<script>
	var submit_flag = 0;
	function updateDISABLE_FLG() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;

		if(document._PolicyForm.DISABLE_FLG[0].checked) {
	    	document._PolicyForm.radio.value = 0;
		} else {
			document._PolicyForm.radio.value = 1;
		}

	    document._PolicyForm.submit();
	}

	function cancelClick() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document._PolicyForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/PolicyBL" />";
	    document._PolicyForm.submit();
	}
</script>
<body>
	<div id="content">
		<div id="editIpsIds">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ポリシー設定 &raquo; セキュリティルール有効/無効設定</p>

			<h3>セキュリティルール有効/無効設定</h3>
			<ts:form action="/customer/fwsetting/policysetting/UpdateDisableBL" method="post" >
				<div id="useOrUnuse">
					<ul>
						<logic:equal name="_PolicyForm" property="disableFlg" value="有効">
							<li><input name="DISABLE_FLG" type="radio"  checked="checked"/>有効</li>
							<li><input name="DISABLE_FLG" type="radio" />無効</li>
						</logic:equal>
						<logic:equal name="_PolicyForm" property="disableFlg" value="無効">
							<li><input name="DISABLE_FLG" type="radio" />有効</li>
							<li><input name="DISABLE_FLG" type="radio"  checked="checked"/>無効</li>
						</logic:equal>
					</ul>

				</div>
				<ul id="button">
					<li><input type="button" value="OK"  onclick="updateDISABLE_FLG()" /></li>
					<li><input type="button" value="Cancel" onclick="cancelClick()"/></li>
				</ul>
				<input type="hidden" name="name" value="<bean:write name="_PolicyForm" property="name"/>"/>
				<input type="hidden" name="radio" value="0"/>
			</ts:form>
		</div>
		<!-- end editIpsIds -->

	<!-- end #content --></div>

</body>

<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
