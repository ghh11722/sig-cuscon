<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="XXXXX" />
<bean:define id="titleScreenName" scope="page" value="XXXXX" />
<bean:define id="screenName" scope="page" value="XXXXX" />
<bean:define id="screenID" scope="page" value="C05-14" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<script>
	<!--
		var submit_flag = 0;

		function okClick() {
			if( submit_flag == 1 ){ return; }
			submit_flag = 1;
		    document._ApplicationFilterForm.submit();
		}

		function cancelClick() {
			if( submit_flag == 1 ){ return; }
			submit_flag = 1;
			document._ApplicationFilterForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/ApplicationFiltersBL" />";
		    document._ApplicationFilterForm.submit();
		}
	-->
</script>
<body>
	<div id="content"><!-- InstanceBeginEditable name="content" -->
		<div id="objectApplicationfilters">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; オブジェクト設定 &raquo; アプリケーションフィルター設定 &raquo; アプリケーションフィルター削除</p>
			<h3>アプリケーションフィルター削除</h3>
			<ts:form action="/customer/fwsetting/objectsetting/DelApplicationFiltersBL" method="post" >
				<table>
					<thead>
						<tr class="odd">
							<th scope="col">オブジェクト名</th>
							<th scope="col">カテゴリー</th>
							<th scope="col">サブカテゴリー</th>
							<th scope="col">テクノロジー</th>
							<th scope="col">リスク</th>
						</tr>
					</thead>
						<logic:iterate id="userBean" name="_ApplicationFilterForm" property="applicationFilterList" indexId="index" >
							<tr>
								<td><bean:write name="userBean" property="name"/></td>
								<td>
									<logic:iterate id="categoryBean" name="userBean" property="categoryList" >
										<bean:write name="categoryBean" property="name"/>
										<html:hidden name="categoryBean" property="name"/><br/>
									</logic:iterate>
								</td>
								<td>
									<logic:iterate id="subCategoryBean" name="userBean" property="subCategoryList" >
										<bean:write name="subCategoryBean" property="name"/>
										<html:hidden name="subCategoryBean" property="name"/><br/>
									</logic:iterate>
								</td>
								<td>
									<logic:iterate id="technologyBean" name="userBean" property="technologyList" >
										<bean:write name="technologyBean" property="name"/>
										<html:hidden name="technologyBean" property="name"/><br/>
									</logic:iterate>
								</td>
								<td>
									<logic:iterate id="riskBean" name="userBean" property="riskList" >
										<bean:write name="riskBean" property="name"/>
										<html:hidden name="riskBean" property="name"/><br/>
									</logic:iterate>
								</td>
					</tr>
					</logic:iterate>

				</table>
				<p>上記の設定を削除してもよろしいですか？</p>
				<ul id="button">
					<li><html:submit value="OK" /></li>
					<li><html:button property="cancel" value="Cancel" onclick="cancelClick()"/></li>
				</ul>
			</ts:form>
		<!-- end #objectApplicationfilters --></div>
	<!-- InstanceEndEditable -->
	<!-- end #content --></div>
</body>
<%@ include file="C-FOOTER-01.jspf"%>
<!-- InstanceEnd --></html:html>
