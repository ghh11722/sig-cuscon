<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="メイン画面" />
<bean:define id="screenName" scope="page" value="企業管理者メイン" />
<bean:define id="screenID" scope="page" value="K06-03" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-03.jspf"%>
<script>
	<!--
		var submit_flag = 0;

		function okClick() {
			if( submit_flag == 1 ){ return; }
			submit_flag = 1;
			document.addressForm.action = "<html:rewrite action="/admin/fwsetting/objectsetting/DelAddressesBL" />";
		    document.addressForm.submit();
		}

		function cancelClick() {
			if( submit_flag == 1 ){ return; }
			submit_flag = 1;
			document.addressForm.action = "<html:rewrite action="/admin/fwsetting/objectsetting/AddressesBL" />";
		    document.addressForm.submit();
		}
	-->
</script>
<body>
	<div id="content"><!-- InstanceBeginEditable name="content" -->
		<div id="objectAddress">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; オブジェクト設定 &raquo; アドレス設定 &raquo; アドレス削除</p>
			<h3>アドレス削除</h3>
			<form action="" method="post" name="addressForm">
				<table>
					<thead>
						<tr class="odd">
							<th scope="col">アドレス名</th>
							<th scope="col">タイプ</th>
							<th scope="col">アドレス</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<logic:notEmpty name="_AddressesForm" property="addressList">
							<tbody>
								<logic:iterate id="userBean" name="_AddressesForm" property="addressList" indexId="index">
									<tr <%=index % 2 == 0 ? "" : "class='odd'"%>>
										<td><bean:write name="userBean" property="name" /></td>
										<td><bean:write name="userBean" property="type" /></td>
										<td><bean:write name="userBean" property="address" /></td>
									</tr>
								</logic:iterate>
							</tbody>
							</logic:notEmpty>
						</tr>
					</tbody>
				</table>
				<p>上記の設定を削除してもよろしいですか？</p>
				<ul id="button">
					<li><input type="button" value="OK" onclick="okClick()" /></li>
					<li><input type="button" value="Cancel" onclick="cancelClick()"/></li>
				</ul>
			</form>
		<!-- #objectAddress --></div>
	<!-- InstanceEndEditable -->
	<!-- end #content --></div>
</body>
<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
