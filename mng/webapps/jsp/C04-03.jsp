<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お客様企業メイン" />
<bean:define id="titleScreenName" scope="page" value="ポリシー一覧" />
<bean:define id="screenName" scope="page" value="Name編集" />
<bean:define id="screenID" scope="page" value="C04-03" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<script type="text/javascript">
	<!--
		var submit_flag = 0;
		function KeyEvent(){
			if(event.keyCode == 13){event.returnValue = false;}
		}

		function okClick() {
			if( submit_flag == 1 ){ return; }
			submit_flag = 1;

			document._PolicyForm.submit();
		}

		function cancelClick() {
			if( submit_flag == 1 ){ return; }
			submit_flag = 1;
			document._PolicyForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/PolicyBL" />";
		    document._PolicyForm.submit();
		}
	-->
</script>
<body>
	<div id="content">
	<div id="editName">
		<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ポリシー設定 &raquo; ポリシー名設定</p>
			<h3>ポリシー名設定</h3>
			<ts:form action="/customer/fwsetting/policysetting/UpdatePolicyBL"  method="post" >
				<dl>
					<dt>ポリシー名</dt>
					<dd><input name="modName" value="<bean:write name="_PolicyForm" property="name"/>" onkeydown="KeyEvent()"/></dd>
				</dl>
				<ul id="button">
					<li><input type="button" value="OK" onclick="okClick()"/></li>
					<li><input type="button" value="Cancel" onclick="cancelClick()"/></li>
				</ul>
				<input type="hidden" name="name" value="<bean:write name="_PolicyForm" property="name"/>"/>
			</ts:form>
		<!-- end editName -->
	</div>
	<!-- end #editName -->

	<!-- end #content --></div>
</body>

<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
