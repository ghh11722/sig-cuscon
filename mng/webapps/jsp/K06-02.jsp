<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="メイン画面" />
<bean:define id="screenName" scope="page" value="企業管理者メイン" />
<bean:define id="screenID" scope="page" value="K06-02" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-03.jspf"%>

<head>
	<script type="text/javascript">
	<!--
	var submit_flag = 0;
	function checkAddress()
	{
		document.geneForm.type.value ="ip-netmask";
	}

	function checkRange()
	{
		document.geneForm.type.value ="ip-range";
	}

	function checkFqdn()
	{
		document.geneForm.type.value ="fqdn";
	}

	function clickOk()
	{
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		if( document.geneForm.type.value =="ip-netmask" )
		{
			document.geneForm.address.value = document.geneForm.input_Address.value;
		}else if( document.geneForm.type.value =="ip-range" )
		{
			document.geneForm.address.value = document.geneForm.input_Range.value;
		}else if( document.geneForm.type.value =="fqdn" )
		{
			document.geneForm.address.value = document.geneForm.input_Fqdn.value;
		}
		document.geneForm.action = "<html:rewrite action="/admin/fwsetting/objectsetting/RegistAddressBL" />";
        document.geneForm.submit();
	}

	function cancelClick() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document.geneForm.action = "<html:rewrite action="/admin/fwsetting/objectsetting/AddressesBL" />";
	    document.geneForm.submit();
	}
	-->
	</script>
</head>
<body>
	<div id="content"><!-- InstanceBeginEditable name="content" -->
		<div id="objectAddress">
		<p id="pankuzu">Top &raquo; FW設定機能 &raquo; オブジェクト設定 &raquo; アドレス設定 &raquo; アドレス追加</p>
		<h3>アドレス追加</h3>
		<form action="" name="geneForm" method="post">
			<input type="hidden" name="type" value="ip-netmask"/>
			<input type="hidden" name="address" value=""/>
			<dl>
				<dt>オブジェクト名</dt>
				<dd><input type="text" name="newAddressName"/></dd>
				<dt><input name="Address/Range" type="radio" onclick="checkAddress()" value="check_Address" checked="checked"/>IPアドレス</dt>
				<dd><input type="text" name="input_Address"/><br />
				(e.g. 「192.168.80.150」 or 「192.168.80.0/24」)</dd>
				<dt><input name="Address/Range" type="radio" onclick="checkRange()" value="check_Range"/>IPレンジ</dt>
				<dd><input type="text" name="input_Range" size="50"/><br />
				(e.g. 「192.168.80.10-192.168.80.20」)</dd>
				<dt><input name="Address/Range" type="radio" onclick="checkFqdn()" value="check_Fqdn"/>FQDN</dt>
				<dd><input type="text" name="input_Fqdn" size="50"/><br />
				(e.g. 「www.kddi.com」)</dd>
			</dl>

			<ul id="button">
				<li><input type="button" value="OK" onclick="clickOk()" /></li>
				<li><input type="button" value="Cancel" onclick="cancelClick()"/></li>
			</ul>
		</form>
		<!-- #objectAddress --></div>
	<!-- InstanceEndEditable -->
	<!-- end #content --></div>
</body>
<%@ include file="C-FOOTER-01.jspf"%>
</html:html>

