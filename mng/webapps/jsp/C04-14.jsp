<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お客様企業メイン" />
<bean:define id="titleScreenName" scope="page" value="ポリシー一覧" />
<bean:define id="screenName" scope="page" value="Schedule編集" />
<bean:define id="screenID" scope="page" value="C04-14" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<script>
	var submit_flag = 0;
	var lastIndex = 0;
	function radioIndex(index) {
			document._PolicyForm.radio.value = index;
	}

	function okClick() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
	    document._PolicyForm.submit();
	}
	function cancelClick() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document._PolicyForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/PolicyBL" />";
	    document._PolicyForm.submit();
	}
	function listClick( id_tr , index){
		if(document.getElementById) {
			var obj = document.getElementById("id_customers");
			var TRs = obj.getElementsByTagName("tr");
			if( lastIndex % 2 ) {
				TRs[lastIndex].className = "odd";
			} else {
				TRs[lastIndex].className = "";
			}
			if(TRs.length == 1) {
				document._PolicyForm.scheRadio.checked = true;
			} else {
				document._PolicyForm.scheRadio[index].checked = true;
			}
			document._PolicyForm.radio.value = index;
			document.getElementById(id_tr).className = "selected";
			lastIndex = index;
		}
	}
</script>
<body>
	<div id="content">
		<div id="editSchedule">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ポリシー設定 &raquo; スケジュール設定</p>
			<h3>スケジュール設定</h3>
			<ts:form action="/customer/fwsetting/policysetting/UpdateScheduleBL" method="post" >
				<table>
					<thead>
						<tr class="odd">
							<th scope="col">&nbsp;</th>
							<th scope="col">オブジェクト名</th>
							<th scope="col">周期</th>
							<th scope="col">スケジュール</th>
						</tr>
					</thead>
					<tbody id="id_customers">
						<logic:iterate id="editscheBean" name="_PolicyForm" property="scheduleList"  indexId="index">
							<bean:define id="scheName" name="_PolicyForm" property="scheduleName" />
							<logic:equal name="editscheBean" property="name" value="<%= scheName.toString() %>">
								<tr id="id_tr<bean:write name="index"/>" <%= index % 2 == 0 ? "" : "class='odd'" %>
							   onclick="listClick('id_tr<bean:write name="index"/>', <%= index %>);" >
									<td><input name="scheRadio" type="radio" onclick="radioIndex(this)" checked="checked"></input></td>
									<td><bean:write name="editscheBean" property="name"/></td>
									<td><bean:write name="editscheBean" property="recurrenceStr"/></td>
									<logic:notEmpty name="editscheBean" property="timeList">
										<td>
											<logic:iterate id="timeBean" name="editscheBean" property="timeList" >
												<bean:write name="timeBean" /><br/>
									</logic:iterate>
										</td>
									</logic:notEmpty>
									<logic:empty name="editscheBean" property="timeList">
										<td><br/></td>
									</logic:empty>
								</tr>
							</logic:equal>
							<logic:notEqual name="editscheBean" property="name" value="<%= scheName.toString() %>">
								<tr id="id_tr<bean:write name="index"/>" <%= index % 2 == 0 ? "" : "class='odd'" %>
							   onclick="listClick('id_tr<bean:write name="index"/>', <%= index %>);" >
									<td><input name="scheRadio" type="radio" onclick="radioIndex(<%= index%>)" ></input></td>
									<td><bean:write name="editscheBean" property="name"/></td>
									<td><bean:write name="editscheBean" property="recurrenceStr"/></td>
									<logic:notEmpty name="editscheBean" property="timeList">
										<td>
											<logic:iterate id="timeBean" name="editscheBean" property="timeList" >
												<bean:write name="timeBean" /><br/>
											</logic:iterate>
										</td>
									</logic:notEmpty>
									<logic:empty name="editscheBean" property="timeList">
										<td><br/></td>
									</logic:empty>
								</tr>
							</logic:notEqual>
						</logic:iterate>
					</tbody>
				</table>
				<ul id="button">
					<li><input type="button" value="OK" onclick="okClick()" /></li>
					<li><input type="button" value="Cancel" onclick="cancelClick()" /></li>
				</ul>
				<input type="hidden" name="name" value="<bean:write name="_PolicyForm" property="name"/>"/>
				<input type="hidden" name="radio" value="0"/>
			</ts:form>
		<!-- end editScheule -->
		</div>
	<!-- end #content --></div>
</body>

<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
