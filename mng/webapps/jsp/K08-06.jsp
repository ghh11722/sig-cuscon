<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>
<%@ taglib uri="/cuscon-struts" prefix="cs" %>

<bean:define id="titleUsecaseName" scope="page" value="ログモニタリング機能" />
<bean:define id="titleScreenName" scope="page" value="Webウィルスチェックログ表示画面" />
<bean:define id="screenName" scope="page" value="Webウィルスチェックログ" />
<bean:define id="screenID" scope="page" value="0001" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true" lang="true">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-03.jspf"%>

<script language="JavaScript" type="text/JavaScript">
<!--
	function submitSearch(){
		var obj = document._IdsLogForm;

        //20170331 UPDATE Yamada@Plum-systems Inc. edit start
        if(obj.startDate.value == ''){

             // 空欄時は今日の日付を補完
             var today = new Date();
             var year = today.getFullYear();
             var month = today.getMonth()+1;
             var day = today.getDate();

             obj.startDate.value = year + '/' + month + '/' + day;

           //ブラウザがIE11モードの場合
           //obj.startDate.value =  new Intl.DateTimeFormat('ja-JP').format(new Date());
        }
        //20170331 UPDATE Yamada@Plum-systems Inc. end

        if(obj.endDate.value == '')
            obj.endDate.value = obj.startDate.value;

		obj.action='<bean:write name='contextPath' />/admin/logmonitor/viruslogBL.do';
		obj.submit();
		displayProgress();
	}
	function submitDownload(){
		var obj = document._IdsLogForm;
		var str = Form.serialize('_IdsLogForm');
		new Ajax.Request( "<bean:write name='contextPath' />/admin/logmonitor/viruslogMakeCsvBL.do",
			{
				"method": "post",
				"parameters": str,
				onSuccess: function(objHttp) {
					hideProgress();
					if( checkResponse(objHttp) == true ) {
						obj.action='<bean:write name='contextPath' />/admin/logmonitor/viruslogDlBL.do';
						obj.submit();
						obj.action='<bean:write name='contextPath' />/admin/logmonitor/viruslogBL.do';
					}
				},
				onFailure: function(objHttp) {
					alert("HTTP通信失敗");
					hideProgress();
				}
			}
		);
		displayProgress();
	}
	function checkResponse(objHttp) {
		if( objHttp.responseXML != undefined ) {
			var doc = objHttp.responseXML;
			var errormsg = doc.getElementsByTagName("errormsg");
			if( errormsg.length > 0 ) {
				eval(errormsg[0].firstChild.nodeValue);
				return false;
			}
			return true;
		}
	}
// -->
</script>

<div id="content">
	<div id="blockLog">
	<p id="pankuzu">Top &raquo; ログモニタリング &raquo; Webウィルスチェックログ</p>
	<h3><bean:write name="screenName" scope="page" /></h3>
	<div id="operationBox1">
	 	<ts:form action="/admin/logmonitor/viruslogBL" method="post" styleId="_IdsLogForm">
			&raquo;&nbsp;ログ検索&nbsp;
			<html:button property="forward_submitIdsLogList" value="検索する" onclick="submitSearch(this);" /><br/><br/>
			&nbsp;&nbsp;&nbsp;&nbsp;表示件数：
			<html:select name="_IdsLogForm" property="line">
				<html:option value="20" >20</html:option>
				<html:option value="50" >50</html:option>
				<html:option value="100" >100</html:option>
				<html:option value="500" >500</html:option>
			</html:select>
			&nbsp;検索キーワード：<html:text name="_IdsLogForm" property="dataStr" maxlength="64"/>
			(検索対象：<html:checkbox name="_IdsLogForm" property="sIpFlg"/>通信元アドレス
			<html:checkbox name="_IdsLogForm" property="dIpFlg"/> 通信先アドレス
			<html:checkbox name="_IdsLogForm" property="sPortFlg"/>通信元ポート
			<html:checkbox name="_IdsLogForm" property="dPortFlg"/>通信先ポート
			<html:checkbox name="_IdsLogForm" property="appFlg"/>アプリケーション)<br/>

			<html:hidden name="_IdsLogForm" property="dataStrOld"/>
			<html:hidden name="_IdsLogForm" property="sIpFlgOld"/>
			<html:hidden name="_IdsLogForm" property="dIpFlgOld"/>
			<html:hidden name="_IdsLogForm" property="sPortFlgOld"/>
			<html:hidden name="_IdsLogForm" property="dPortFlgOld"/>
			<html:hidden name="_IdsLogForm" property="appFlgOld"/>
			<html:hidden name="_IdsLogForm" property="transitionFlg"/>
			<html:hidden name="_IdsLogForm" property="subtypeFlg" value="3"/>

			<script type="text/javascript">
			<!--
				var localja = true;
				var jscalendarMonthName = new Array("1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月");
				var jscalendarDayName = new Array("日","月","火","水","木","金","土");
				var jscalendarTodayString = "今日は";
				var jscalendarThemePrefix = "BlueStyle";
				var jscalendarImgDir = "<bean:write name='contextPath' />/img/calendar/BlueStyle/";
			//-->
			</script>
			<script src="<bean:write name='contextPath' />/js/calendar/InputCalendar.js" type="text/javascript"></script>
			<script type="text/javascript">
			<!--
				jscalendarInit();
			//-->
			</script>

			&nbsp;&nbsp;&nbsp;&nbsp;対象期間：&nbsp;<html:text name="_IdsLogForm" property="startDate" maxlength="10" onclick="jscalendarPopUpCalendar(this,this,'yyyy/MM/dd');"/>～
			<html:text name="_IdsLogForm" property="endDate" maxlength="10" onclick="jscalendarPopUpCalendar(this,this,'yyyy/MM/dd');"/> (例: 2011/05/01)<br/><br/>
			&raquo;&nbsp;ログダウンロード&nbsp;<html:button property="forward_submitIdsLogMakeCsv" value="ダウンロードする" onclick="submitDownload(this);" />※検索キーワードにかかわらず、指定した対象期間内のすべてのログをダウンロードします。
			<br/><br/>
			<font color="red">
				<p>&nbsp;&nbsp;&nbsp;&nbsp;【注意】ダウンロードを行う際はInternetExplorerにて以下の設定を行ってください。</p>
				<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;１．「ツール」⇒「インターネットオプション」⇒「セキュリティ」⇒該当するゾーンを選択⇒「レベルのカスタマイズ」をクリック</p>
				<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;２．「ダウンロード」⇒「ファイルのダウンロード」の項目で「有効にする」を選択し、「OK」をクリック</p>
			</font>
		</ts:form>
	</div>

	<p class="pages">
		<cs:pageLinks action="/admin/logmonitor/viruslogchangelineBL" name="_IdsLogForm" rowProperty="line"
		              totalProperty="totalCount" indexProperty="startIndex" clickEvent="displayProgress()">
		</cs:pageLinks>
	</p>
	<table>
		<thead>
			<tr class="odd">
				<th scope="col">日時</th>
				<th scope="col">タイプ</th>
				<th scope="col">通信元アドレス</th>
				<th scope="col">通信先アドレス</th>
				<th scope="col">ポリシー名</th>
				<th scope="col">アプリケーション</th>
				<th scope="col">通信元ゾーン</th>
				<th scope="col">通信先ゾーン</th>
				<th scope="col">通信元ポート</th>
				<th scope="col">通信先ポート</th>
				<th scope="col">アクション</th>
			</tr>
		</thead>
		<logic:notEmpty name="_IdsLogForm" property="recList">
		<tbody>
			<logic:iterate id="userBean" name="_IdsLogForm" property="recList" indexId="idx">
			<tr <%=idx % 2 == 0 ? "" : "class='odd'"%>>
				<td><bean:write name="userBean" property="reciveTime" /></td>
				<td><bean:write name="userBean" property="type" /></td>
				<td><bean:write name="userBean" property="sourceIp" /></td>
				<td><bean:write name="userBean" property="destIp" /></td>
				<td><bean:write name="userBean" property="ruleName" /></td>
				<td><bean:write name="userBean" property="application" /></td>
				<td><bean:write name="userBean" property="sourceZone" /></td>
				<td><bean:write name="userBean" property="destinationZone" /></td>
				<td><bean:write name="userBean" property="sourcePort" /></td>
				<td><bean:write name="userBean" property="destinationPort" /></td>
				<td><bean:write name="userBean" property="action" /></td>
			</tr>
			</logic:iterate>
		</tbody>
		</logic:notEmpty>
	</table>
	<p class="pages">
		<cs:pageLinks action="/admin/logmonitor/viruslogchangelineBL" name="_IdsLogForm" rowProperty="line"
		              totalProperty="totalCount" indexProperty="startIndex" clickEvent="displayProgress()">
		</cs:pageLinks>
	</p>
	</div>
</div>

<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
