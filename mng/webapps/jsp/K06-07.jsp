<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="メイン画面" />
<bean:define id="screenName" scope="page" value="企業管理者メイン" />
<bean:define id="screenID" scope="page" value="K06-07" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-03.jspf"%>
<head>
	<script language="JavaScript">
	<!--
	var chkNum = 0;
	var indexList = new Array();

	function getCheck(obj,index){
		if (obj.checked) {
			chkNum++;
			indexList.push(index);
		} else {
			chkNum--;
			for(i=0;i<indexList.length;i++) {
				if(indexList[i]==index) {
					indexList.splice(i, 1);
					i--;
				}
			}
		}
	}

	function checkDel(){
		var str = "";
		if(chkNum!=0) {
			for(i=0;i<indexList.length;i++) {
				if(i==0) {
					str = indexList[i];
				} else {
					str = str + "," + indexList[i];
				}
			}
			document._ApplicationGroupsForm.index.value = str;
			document._ApplicationGroupsForm.action = "<html:rewrite action="/admin/fwsetting/objectsetting/ApplicationGroupsConfirmBL" />";
	        document._ApplicationGroupsForm.submit();
		}
	}

	function jumpAppli(selectedIndex) {
		document.getElementById('displayType').value = "";
		document._ApplicationGroupsForm.index.value =selectedIndex;
		document._ApplicationGroupsForm.action = "<html:rewrite action="/admin/fwsetting/objectsetting/EditApplicationGroupBL" />";
		document._ApplicationGroupsForm.submit();
	}

	var lastIndex = 0;
	function listClick( id_tr , index ){
		if(document.getElementById) {
			var obj = document.getElementById("id_customers");
			var TRs = obj.getElementsByTagName("tr");
			if( lastIndex % 2 ) {
				TRs[lastIndex].className = "odd";
			} else {
				TRs[lastIndex].className = "";
			}
			document.getElementById(id_tr).className = "selected";
			lastIndex = index;
		}
	}

	function addClick(){
		document.getElementById('displayType').value = "";
		document._ApplicationGroupsForm.action = "<html:rewrite action="/admin/fwsetting/objectsetting/AddApplicationGroupBL" />";
		document._ApplicationGroupsForm.submit();
	}

	// -->
	</script>
</head>
<body onunload="unLoadWindow()">
	<div id="content">
		<div id="objectApplicationgroup">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; オブジェクト設定 &raquo; アプリケーショングループ設定</p>
			<h3>アプリケーショングループ設定</h3>
			<ts:form  action="/admin/fwsetting/objectsetting/ApplicationGroupsBL" method="post">
				<ul id="button">
				<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
					<li><input type="button" value="オブジェクトの追加" disabled="disabled"/></li>
					<li><input type="button" value="オブジェクトの削除" disabled="disabled"/></li>
				</logic:notMatch>
				<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
					<li><input type="button" value="オブジェクトの追加" onclick="addClick()" /></li>
					<li><input type="button" value="オブジェクトの削除" onclick="checkDel()" /></li>
				</logic:match>
				</ul>
				<br />
				<table>
					<thead>
						<tr class="odd">
							<th scope="col">&nbsp;</th>
							<th scope="col">オブジェクト名</th>
							<th scope="col">メンバー数</th>
							<th scope="col">アプリケーション・フィルター</th>
						</tr>
					</thead>
					<logic:notEmpty name="_ApplicationGroupsForm" property="applicationGroupList">
					<tbody id="id_customers">
						<logic:iterate id="userBean" name="_ApplicationGroupsForm" property="applicationGroupList" indexId="index">
						<tr id="id_tr<bean:write name="index"/>" <%= index % 2 == 0 ? "" : "class='odd'" %>
							    onclick="listClick('id_tr<bean:write name="index"/>',<%= index %>);" >
							<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
								<td><input name="a" type="checkbox" disabled ="disabled"/></td>
							</logic:notMatch>
							<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
								<td><input name="a" type="checkbox" onclick="getCheck(this, <%= index %>)"/></td>
							</logic:match>
							<td><a href="javascript:jumpAppli(<%= index %>)"><bean:write name="userBean" property="name" /></a></td>
							<td><bean:write name="userBean" property="members" /></td>
							<td><bean:write name="userBean" property="applications" /></td>
						</tr>
						</logic:iterate>
					</tbody>
					</logic:notEmpty>
				</table>
				<ul id="button">
				<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
					<li><input type="button" value="オブジェクトの追加" disabled="disabled"/></li>
					<li><input type="button" value="オブジェクトの削除" disabled="disabled"/></li>
				</logic:notMatch>
				<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
					<li><input type="button" value="オブジェクトの追加" onclick="addClick()" /></li>
					<li><input type="button" value="オブジェクトの削除" onclick="checkDel()" /></li>
				</logic:match>
				</ul>
				<input type="hidden" name="index"/>
				<input type="hidden" name="checkIndex" />
				<input type="hidden" id="displayType" name="displayType" value="<bean:write name="_ApplicationGroupsForm" property="displayType"/>"/>

				<logic:notEmpty name="_ApplicationGroupsForm" property="message">
					<script type="text/javascript">
					{
						alert("<bean:write name="_ApplicationGroupsForm" property="message"/>");
						document._ScheduleForm.message.value = "";
					}
					</script>
				</logic:notEmpty>
			</ts:form>
	<!-- end objectAddress --></div>
	<!-- end content --></div>
</body>
<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
