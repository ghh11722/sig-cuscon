<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お客様企業メイン" />
<bean:define id="titleScreenName" scope="page" value="ロールバック" />
<bean:define id="screenName" scope="page" value="Webウィルスチェック参照" />
<bean:define id="screenID" scope="page" value="C06-23" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<body>
	<div id="content">
		<div id="editIpsIds">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ロールバック &raquo; Webウィルスチェック参照</p>

			<h3>Webウィルスチェック参照</h3>
			<ts:form action="/customer/fwsetting/rollback/RollbackBL" method="post" >
				<div id="useOrUnuse">
					<ul>
						<logic:equal name="_RollbackForm" property="virusCheckFlg" value="default">
							<li><input name="VirusCheck" type="radio" disabled ="disabled" checked="checked"/>○：利用する</li>
							<li><input name="VirusCheck" type="radio" disabled ="disabled"/>×：利用しない</li>
						</logic:equal>
						<logic:equal name="_RollbackForm" property="virusCheckFlg" value="none">
							<li><input name="VirusCheck" type="radio" disabled ="disabled"/>○：利用する</li>
							<li><input name="VirusCheck" type="radio" disabled ="disabled" checked="checked" />×：利用しない</li>
						</logic:equal>
					</ul>

				</div>
				<ul id="button">
					<li><ts:submit value="Cancel" /></li>
				</ul>
			</ts:form>
		</div>
		<!-- end editIpsIds -->

	<!-- end #content --></div>

</body>

<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
