<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お客様企業メイン" />
<bean:define id="titleScreenName" scope="page" value="ロールバック" />
<bean:define id="screenName" scope="page" value="Source Zone参照" />
<bean:define id="screenID" scope="page" value="C06-12" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<script>
	var zoneMstList = new Array();
	var zoneNum = 0;
</script>
<body>
	<div id="content">
		<div id="editSourcezone">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ロールバック &raquo; 通信元ゾーン参照</p>

			<h3>通信元ゾーン参照</h3>
			<ts:form action="/customer/fwsetting/rollback/RollbackBL" method="post" >
				<div id="anyOrSelect">
					<ul>
						<bean:size id="zoneSize" name="_RollbackForm" property="zoneMasterList" />
						<logic:equal name="_RollbackForm" property="zoneName" value="any">
							<li><input name="ZoneRadio" type="radio" checked="checked" disabled ="disabled"//>any</li>
							<li><input name="ZoneRadio" type="radio" disabled ="disabled"//>選択する</li>
						</logic:equal>
						<logic:notEqual name="_RollbackForm" property="zoneName" value="any">
							<li><input name="ZoneRadio" type="radio" disabled ="disabled"//>any</li>
							<li><input name="ZoneRadio" type="radio" checked="checked" disabled ="disabled"//>選択する</li>
						</logic:notEqual>
					</ul>
				<!-- end anyOrSelect --></div>
				<div id="available">
					<h4>&raquo; オブジェクトの選択</h4>
					<ul>
						<logic:iterate id="zoneMstBean" name="_RollbackForm" property="zoneMasterList"  indexId="index">
							<li><input name="checkZone" type="checkbox" disabled ="disabled"//><bean:write name="zoneMstBean" /><br/></li>
								<script type="text/javascript">
										{
											zoneMstList[zoneNum] = "<bean:write name='zoneMstBean'/>";
											zoneNum++;
										}
									</script>
						</logic:iterate>
						<logic:iterate id="zoneBean" name="_RollbackForm" property="zoneList"  indexId="index">
							<script type="text/javascript">
										{
											for(i=0; i<zoneMstList.length; i++) {
												if(zoneMstList[i] == "<bean:write name='zoneBean'  property='name'/>") {
													document._RollbackForm.checkZone[i].checked=true;
												}
											}
										}
									</script>
						</logic:iterate>
					</ul>
				<!-- end #available --></div>

				<ul id="button">
					<li><ts:submit value="Cancel"/></li>
				</ul>
			</ts:form>
		<!-- editSourcezone -->
		</div>
	<!-- end #content --></div>
</body>

<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
