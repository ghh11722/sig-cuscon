<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="cssCommonFileName" scope="page" value="/css/base.css"/>
<bean:define id="cssExtendFileName" scope="page" value="/css/extend.css" />

<bean:define id="titleUsecaseName" scope="page" value="パスワード変更機能" />
<bean:define id="titleScreenName" scope="page" value="パスワード変更画面(from Login)" />
<bean:define id="screenName" scope="page" value="管理者パスワード変更" />
<bean:define id="screenID" scope="page" value="0001" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true" lang="true">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Sun,6 Jun 2006 00:00:00 GMT" />
<title>Customer Control System - Administrator Login</title>
<bean:page id="requestObj" property="request"/>
<bean:define id="contextPath" name="requestObj" property="contextPath" />
<link href="<bean:write name='contextPath' /><bean:write name="cssCommonFileName" scope="page"/>" rel="stylesheet" type="text/css" />
<link href="<bean:write name='contextPath' /><bean:write name="cssExtendFileName" scope="page"/>" rel="stylesheet" type="text/css" />
</head>
<body>
<script language="JavaScript" type="text/JavaScript">
<!--
function clearForm() {
	   document.getElementsByName('old_password')[0].value = '';
	   document.getElementsByName('new_password')[0].value = '';
	   document.getElementsByName('password_check')[0].value = '';
}
// -->
</script>

<div id="wrapper">
	<div id="kddiPassChange">
		<h1><bean:write name="screenName" scope="page"/></h1>
		<ts:form action="/admin/password/change_loginBL" >
		<dl>
			<dt>現在のパスワード</dt>
			<dd><html:password property="old_password" maxlength="14" size="25" /></dd>
			<dt>変更後のパスワード</dt>
			<dd><html:password property="new_password" maxlength="14" size="25" /></dd>
			<dt>変更後のパスワード(再入力)</dt>
			<dd><html:password property="password_check" maxlength="14" size="25" /></dd>
		</dl>
		<ul id="button">
			<li><ts:submit value="OK" /></li>
			<li><html:button property="reset" value="Clear" onclick="clearForm()" /></li>
		</ul>
		</ts:form>
	</div>
</div>
<ts:ifErrors>
<script language="JavaScript" type="text/JavaScript">
<!--
	alert("<ts:errors/>");
//-->
</script>
</ts:ifErrors>
<logic:equal name="_PasswordChangeForm" property="resultmsg" value="success">
<script language="JavaScript" type="text/JavaScript">
<!--
alert('<ts:messages id="message" message="true"><bean:write name="message"/></ts:messages>');
document.location ='<bean:write name='contextPath' />/admin/mainBL.do';
//-->
</script>
</logic:equal>
</body>
</html:html>
