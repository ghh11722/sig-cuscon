<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="メイン画面" />
<bean:define id="screenName" scope="page" value="企業管理者メイン" />
<bean:define id="screenID" scope="page" value="K05-10" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-03.jspf"%>
<head>
	<script type="text/javascript">
	<!--
	var submit_flag = 0;
	var chkNum = 0;
	var indexList = new Array();
	var chkbox = 0;

	function addData(){
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		indexList = new Array();

		chkNum = 0;

		var tbl = document.getElementById("selectTBL");

		var selectIndex = 0;

		if (chkbox == 1 && document.subForm.checkboxNum.checked) {
			selectIndex = 0
			indexList.push(selectIndex);
		} else if (chkbox > 1) {

			for (i = 1; i < chkbox + 1; i++) {
				if(document.subForm.checkboxNum[i-1].checked) {
					chkNum++;
					selectIndex = i-1;

					indexList.push(selectIndex);
				}
			}
		}

		var str = "";
		for(i=0;i<indexList.length;i++) {
			if(i==0) {
				str = indexList[i];
			} else {
				str = str + "," + indexList[i];
			}
		}
		document.subForm.groupIndex.value = str;
		document.subForm.action = "<html:rewrite action="/admin/fwsetting/policysetting/AddGroupsPolicyBL" />";
       	document.subForm.submit();
	}

	function clickCancel(){
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		var aa = document.getElementById("cancelFlg").value;

		document.getElementById("cancelFlg").value = "1";

		// submit処理
		document.subForm.action = "<html:rewrite action="/admin/fwsetting/policysetting/EditApplicationBL" />";
    	document.subForm.submit();
	}
	var lastIndex = 0;
	function listClick( id_tr , index ){
		if(document.getElementById) {
			var obj = document.getElementById("id_customers");
			var TRs = obj.getElementsByTagName("tr");
			if( lastIndex % 2 ) {
				TRs[lastIndex].className = "odd";
			} else {
				TRs[lastIndex].className = "";
			}
			document.getElementById(id_tr).className = "selected";
			lastIndex = index;
		}
	}
	-->
	</script>
</head>
<body onunload="unLoadWindow()">
	<div id="content">
		<div id="editGroups">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ポリシー設定 &raquo; アプリケーション設定 &raquo; グループ登録</p>
			<h3>グループ登録</h3>
			<form action="" name="subForm" method="post" >
				<table id="selectTBL">
				<thead>
					<tr class="odd">
						<th scope="col">&nbsp;</th>
						<th scope="col">オブジェクト名</th>
						<th scope="col">メンバー数</th>
						<th scope="col">アプリケーション・フィルター</th>
					</tr>
				</thead>
				<logic:notEmpty name="_PolicyForm" property="groupList4Add">
				<tbody id="id_customers">
					<logic:iterate id="userBean" name="_PolicyForm" property="groupList4Add" indexId="index">
						<tr id="id_tr<bean:write name="index"/>" <%= index % 2 == 0 ? "" : "class='odd'" %>
							    onclick="listClick('id_tr<bean:write name="index"/>',<%= index %>);" >
							<td>
								<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
									<logic:notMatch name="userBean"  property="chkFlg" value="true" >
										<input type="checkbox"  name="checkboxNum" disabled="disabled"/>
									</logic:notMatch>
									<logic:match name="userBean"  property="chkFlg" value="true" >
										<input type="checkbox" name="checkboxNum" disabled="disabled"checked="checked"/>
									</logic:match>
								</logic:notMatch>
								<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
									<logic:notMatch name="userBean"  property="chkFlg" value="true" >
										<input type="checkbox" name="checkboxNum"/>
									</logic:notMatch>
									<logic:match name="userBean"  property="chkFlg" value="true" >
										<input type="checkbox" name="checkboxNum" checked="checked"/>
									</logic:match>
								</logic:match>
								<script type="text/javascript">
									{
										chkbox++;
									}
								</script>
							</td>
							<td><bean:write name="userBean" property="name" /></td>
							<td><bean:write name="userBean" property="members" /></td>
							<td><bean:write name="userBean" property="applications" /></td>
						</tr>
					</logic:iterate>
				</tbody>
				</logic:notEmpty>
				</table>
				<ul id="button">
					<logic:notMatch name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<li><input type="button" value="OK" disabled="disabled"/></li>
					</logic:notMatch>
					<logic:match name="USER_VALUE_OBJECT"  property="grantFlag" value="1" >
						<li><input type="button" value="OK" onclick="addData()" /></li>
					</logic:match>
					<li><input type="button" value="Cancel" onclick="clickCancel()" /></li>
				<!-- end #button --></ul>
				<input type="hidden" name="groupIndex"/>
				<input type="hidden" name="name" value="<bean:write name="_PolicyForm" property="name"/>"/>
				<input type="hidden" id="cancelFlg" name="cancelFlg" value="<bean:write name="_PolicyForm" property="cancelFlg"/>"/>
			</form>
		<!-- end #editGroups -->
		</div>
	<!-- end #content --></div>
</body>

<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
