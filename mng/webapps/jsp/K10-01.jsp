<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="FW接続情報管理機能" />
<bean:define id="titleScreenName" scope="page" value="顧客企業情報アップロード画面" />
<bean:define id="screenName" scope="page" value="顧客企業情報アップロード" />
<bean:define id="screenID" scope="page" value="K10-01" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true" lang="true">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-01.jspf"%>

<div id="content">
	<div id="blockCAdddel">
		<ts:form action="/admin/fwconnect/uploadBL" method="post" enctype="multipart/form-data">
		<h3>お客様設定 追加／削除</h3>
		<p>お客様を設定するTSVファイルを読み込ませてください</p>
		<div id="customerUpload">
			<html:file name="_CustomerUploadForm" property="fileup" accept="text/html" size="40" maxlength="255" />
		</div>
		<ul id="button">
			<li><ts:submit value="OK" /></li>
			<li><input type="button" value="Cancel" onclick="document.location='<bean:write name='contextPath' />/admin/mainBL.do'" /></li>
		</ul>
		</ts:form>
	</div>
</div>

<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
