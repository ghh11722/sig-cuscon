<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="FWレポーティング機能" />
<bean:define id="titleScreenName" scope="page" value="レポート" />
<bean:define id="screenName" scope="page" value="top-attacks-summary" />
<bean:define id="screenID" scope="page" value="C08-07" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<script>

	function jump() {
		document._ReportForm.submit();
	}

</script>

<div id="content">
		<div id="blockReport">
			<p id="pankuzu">Top &raquo; レポート閲覧 &raquo; 攻撃トラフィック</p>

			<h3>攻撃トラフィック</h3>
			<ts:form action="/customer/report/TopAttacksSummaryBL" method="post" >
				<p id="summary">- 攻撃トラフィックを送信したスレットの集計(上位50件)</p>
				<logic:notEmpty name="_ReportForm" property="attackTargetDate">
					<bean:define id="date" name="_ReportForm" property="attackTargetDate"/>
					<select name="attackTargetDate" onchange="jump()">
							<logic:iterate id="dateBean" name="_ReportForm" property="targetDateList" indexId="index">
								<logic:equal name="dateBean" property="targetDate" value="<%= date.toString() %>" >
									<option value="<bean:write name="dateBean" property="targetDate"/>"  selected="selected"> <bean:write name="dateBean" property="targetDate"/></option>
								</logic:equal>
								<logic:notEqual name="dateBean" property="targetDate" value="<%= date.toString() %>">
									<option value="<bean:write name="dateBean" property="targetDate"/>" > <bean:write name="dateBean" property="targetDate"/></option>
								</logic:notEqual>
							</logic:iterate>
					</select>
				</logic:notEmpty>
				<logic:empty name="_ReportForm" property="attackTargetDate">
					<select name="attackTargetDate" onchange="jump()">
						<option>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
					</select>
				</logic:empty>
				<p>：レポート取得対象を選択してください。</p>
				<table>
					<thead>
						<tr class="odd">
						<th scope="col">リスク<!--severity-of-threatid--></th>
						<th scope="col">スレットID<!--threatid--></th>
						<th scope="col">tid</th>
						<th scope="col">タイプ<!--threatsubtype--></th>
						<th scope="col">回数<!--count--></th>
					</tr>
					</thead>
					<tbody>
						<logic:notEmpty name="_ReportForm" property="topAttackSummaryList">
							 <logic:iterate id="attackBean" name="_ReportForm" property="topAttackSummaryList" indexId="index">
							   <tr <%=index % 2 == 0 ? "" : "class='odd'"%>>
							   		<td><bean:write name="attackBean" property="severityOfThreatid"/></td>
							   		<td><bean:write name="attackBean" property="threatid"/></td>
							        <td><bean:write name="attackBean" property="tid"/></td>
							        <td><bean:write name="attackBean" property="threatSubType"/></td>
							        <td><bean:write name="attackBean" property="count"/></td>
						    	</tr>
						    </logic:iterate>
						</logic:notEmpty>
					</tbody>
				</table>
			</ts:form>
		<!-- end blockReport -->
		</div>
	<!-- end #content --></div>
<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
