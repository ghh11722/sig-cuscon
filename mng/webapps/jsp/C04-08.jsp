<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>
<%@ taglib uri="/struts-nested" prefix="nested" %>

<bean:define id="titleUsecaseName" scope="page" value="XXXXX" />
<bean:define id="titleScreenName" scope="page" value="XXXXX" />
<bean:define id="screenName" scope="page" value="XXXXX" />
<bean:define id="screenID" scope="page" value="C04-08" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>

<head>
	<script type="text/javascript">
	<!--
	var submit_flag = 0;
	function Apl(index){

		var selectApl = document.getElementById("aplTBL").rows[index].cells[1].innerHTML;

	    var tableId = 'selectTBL';
	    var targetTable = document.getElementById(tableId);
	    var rowCount    = targetTable.rows.length;
	    var colCount    = targetTable.rows[0].cells.length;

	    var tbl = document.getElementById("selectTBL");
	    var flg = 0;
	    for (i = 1; i < tbl.rows.length; i = i +1) {
			if (tbl.rows[i].cells[1].innerHTML == selectApl) {
				flg++;
			}
		}

	    if (flg == 0) {
		    var row = targetTable.insertRow(rowCount);

		    var cell1 = row.insertCell(-1);
		    var cell2 = row.insertCell(-1);
		    var cell3 = row.insertCell(-1);

		    var button = document.createElement("input");
		    button.type = "button";
		    button.value = "削除";

		    button.onclick = function(event) {

		        var tbl = document.getElementById("selectTBL");
		        deleteRow(this.parentNode.parentNode.rowIndex);
		    };

	    	cell1.appendChild(button);
	    	cell2.appendChild(document.createTextNode(selectApl));
	    	cell3.appendChild(document.createTextNode("アプリケーション"));
	    }

	    AutoCheck();
	}

	function deleteRow(index){

		var tbl = document.getElementById("selectTBL");

		tbl.deleteRow(index);
	}

	function jumpFilter() {

		make_selectData();
		document.aplForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/ChoiceFiltersPolicyBL" />";
        document.aplForm.submit();
	}

	function jumpGroup() {

		make_selectData();
		document.aplForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/ChoiceGroupsPolicyBL" />";
        document.aplForm.submit();
	}

	function registData(){
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		if (document.getElementById('aplRadio').checked == true) {
			document.getElementById('radio').value = "1";
			document.aplForm.radio.value = 1;
		} else {
			document.aplForm.radio.value = 0;
		}

		var tbl = document.getElementById("selectTBL");
		make_selectData();

		document.aplForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/UpdateApplicationBL" />";
        document.aplForm.submit();

	}

	function searchData(){
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		var selectApl = new Array();
		var selectType = new Array();

		var tbl = document.getElementById("selectTBL");
		if(tbl == null) {
			submit_flag = 0;
			return;
		}
		for (i = 1; i < tbl.rows.length; i = i +1) {

			var selectAplStr = document.getElementById("selectTBL").rows[i].cells[1].innerHTML;
			var selectTypeStr = document.getElementById("selectTBL").rows[i].cells[2].innerHTML;

			selectApl[i-1] = selectAplStr;
			selectType[i-1] = selectTypeStr;

			make_hidden('selectAplList', selectApl[i-1], 'aplForm');
			make_hidden('selectTypeList', selectType[i-1], 'aplForm');

		}

		document.aplForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/PolicySearchBL" />";
        document.aplForm.submit();
	}

	function make_selectData(){

		var selectApl = new Array();
		var selectType = new Array();

		var tbl = document.getElementById("selectTBL");

		for (i = 1; i < tbl.rows.length; i = i +1) {

			var selectAplStr = document.getElementById("selectTBL").rows[i].cells[1].innerHTML;
			var selectTypeStr = document.getElementById("selectTBL").rows[i].cells[2].innerHTML;

			selectApl[i-1] = selectAplStr;
			selectType[i-1] = selectTypeStr;

			make_hidden('selectAplList', selectApl[i-1], 'aplForm');
			make_hidden('selectTypeList', selectType[i-1], 'aplForm');
		}
		if(tbl.rows.length == 1) {
			make_hidden('selectAplList', "null", 'aplForm');
			make_hidden('selectTypeList', "null", 'aplForm');
		}
	}

	function make_hidden( name, value, formname ){
	    var q = document.createElement('input');
	    q.type = 'hidden';
	    q.name = name;
	    q.value = value;

		if (formname){
			document.forms[formname].appendChild(q);
		} else{
			document.forms[0].appendChild(q);
		}
	}

	function change(event){

		if (event.keyCode == 13 || event.charCode == 13){
			searchData();
		}
	}

	 function AutoCheck() {
		document.getElementById('aplRadio').checked = true;
	 }

	function radioSetting() {
		var tbl = document.getElementById("selectTBL");
		var radioFlg = tbl.rows.length;
		if (radioFlg > 1) {
			document.getElementById('aplRadio').checked = true;
		}
	}

	function clearFilter() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;

		var selectApl = new Array();
		var selectType = new Array();
		var tbl = document.getElementById("selectTBL");
		if(tbl == null) {
			submit_flag = 0;
			return;
		}
		for (i = 1; i < tbl.rows.length; i = i +1) {

			var selectAplStr = document.getElementById("selectTBL").rows[i].cells[1].innerHTML;
			var selectTypeStr = document.getElementById("selectTBL").rows[i].cells[2].innerHTML;

			selectApl[i-1] = selectAplStr;
			selectType[i-1] = selectTypeStr;

			make_hidden('selectAplList', selectApl[i-1], 'aplForm');
			make_hidden('selectTypeList', selectType[i-1], 'aplForm');
		}

		document.aplForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/PolicyClearBL" />";
        document.aplForm.submit();
	}
	function cancelClick() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document.aplForm.action = "<html:rewrite action="/customer/fwsetting/policysetting/PolicyBL" />";
	    document.aplForm.submit();
	}
	-->
	</script>
</head>

<body onload="radioSetting()">
	<div id="content">
		<div id="blockApplication">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ポリシー設定 &raquo; アプリケーション設定</p>
			<h3>アプリケーション設定</h3>
			<form action="/customer/fwsetting/policysetting/UpdateApplicationBL" name="aplForm" method="post" >
				<div id="anyOrSelect">
					<ul>
						<li><input name="ApplicationRadio" type="radio"  value="0" checked="checked"/>any</li>
						<li><input name="ApplicationRadio" id="aplRadio" value="1" type="radio" />選択する</li>
					</ul>
				<!-- end anyOrSelect --></div>
				<div id="box1">
					<p id="filters"><a href="javascript:jumpFilter()">フィルターを設定する</a></p>
					<p id="groups"><a href="javascript:jumpGroup()">グループを設定する</a></p>
				<!-- end box1 --></div>
				<div id="box2">
					<p id="search">検索：<html:text name="_PolicyForm"  property="search" onkeypress="javascript:change(event)"/></p>
					<p id="filterReset"><html:button property="clear" value="フィルターの解除"  onclick="clearFilter()"/></p>
				<!-- end box2 --></div>
				<div id="applicationeditFilter">
					<bean:define id="filterbean" name="_PolicyForm" property="filterList" />
					<div id="categoly">
						<h4>カテゴリー</h4>
						<div id="categolyList">
							<ul>
							<logic:iterate id="categoryList" name="filterbean" property="categoryList" indexId="index" >
								<li>
									<html:checkbox name= "categoryList" property="chkFlg" indexed="true" onclick="javascript:searchData()" />
									<nested:hidden property="chkFlg" value="false"/>
									<bean:write name="categoryList" property="name"/>
									<html:hidden name="categoryList" property="name" indexed="true"/>
									<html:hidden name="categoryList" property="seqNo" indexed="true"/>
								</li>
							</logic:iterate>
							</ul>
						<!-- end #categoryList --></div>
					<!-- #category --></div>

					<div id="subcategory">
						<h4>サブカテゴリー</h4>
						<div id="subcategoryList">
							<ul>
								<logic:iterate id="subCategoryList" name="filterbean" property="subCategoryList" indexId="index" >
									<li>
										<html:checkbox name= "subCategoryList" property="chkFlg" indexed="true" onclick="javascript:searchData()" />
										<nested:hidden property="chkFlg" value="false"/>
										<bean:write name="subCategoryList" property="name"/>
										<html:hidden name="subCategoryList" property="name" indexed="true"/>
										<html:hidden name="subCategoryList" property="seqNo" indexed="true"/>
									</li>
								</logic:iterate>
							</ul>
						<!-- end #subcategoryList --></div>
						<!-- #subcategoly --></div>

					<div id="technology">
						<h4>テクノロジー</h4>
						<div id="technologyList">
							<ul>
								<logic:iterate id="technologyList" name="filterbean" property="technologyList" indexId="index" >
									<li>
										<html:checkbox name= "technologyList" property="chkFlg" indexed="true" onclick="javascript:searchData()" />
										<nested:hidden property="chkFlg" value="false"/>
										<bean:write name="technologyList" property="name"/>
										<html:hidden name="technologyList" property="name" indexed="true"/>
										<html:hidden name="technologyList" property="seqNo" indexed="true"/>
									</li>
								</logic:iterate>
							</ul>
						<!-- end #technology --></div>
					<!-- #technology --></div>

					<div id="risk">
						<h4>リスク</h4>
						<div id="riskList">
							<ul>
								<logic:iterate id="riskList" name="filterbean" property="riskList" indexId="index" >
									<li>
										<html:checkbox name= "riskList" property="chkFlg" indexed="true" onclick="javascript:searchData()" />
										<nested:hidden property="chkFlg" value="false"/>
										<bean:write name="riskList" property="name"/>
										<html:hidden name="riskList" property="name" indexed="true"/>
									</li>
								</logic:iterate>
							</ul>
						<!-- end #riskList --></div>
					<!-- #risk --></div>
			<!-- end #applicationeditFilter --></div>

			<div id="filterResult">
				<table id="aplTBL">
					<thead>
						<tr class="odd">
							<th scope="col">&nbsp;</th>
							<th scope="col">アプリケーション名</th>
							<th scope="col">カテゴリー</th>
							<th scope="col">サブカテゴリー</th>
							<th scope="col">テクノロジー</th>
							<th scope="col">リスク</th>
						</tr>
					</thead>
					<tbody>
						<logic:iterate id="applicationBean" name="_PolicyForm" property="applicationList"  indexId="index">
							<tr <%=index % 2 == 0 ? "" : "class='odd'"%>>
								<td><input type="button" value="追加" onclick="Apl(this.parentNode.parentNode.rowIndex)"/></td>
								<td><bean:write name="applicationBean" property="name"/></td>
								<td><bean:write name="applicationBean" property="category"/></td>
								<td><bean:write name="applicationBean" property="subCategory"/></td>
								<td><bean:write name="applicationBean" property="technology"/></td>
								<td><bean:write name="applicationBean" property="risk"/></td>
							</tr>
						</logic:iterate>
					</tbody>
				</table>
				<!-- #filterResult --></div>

				<div id="result"><br />
				<p>選択中のアプリケーション</p>
				<table id="selectTBL">
					<thead>
						<tr class="odd">
							<th scope="col"></th>
							<th scope="col">アプリケーション名</th>
							<th scope="col">タイプ</th>
						</tr>
					</thead>
					<logic:notEmpty name="_PolicyForm" property="selectedAplList">
						<tbody>
							<logic:iterate id="applicationBean" name="_PolicyForm" property="selectedAplList"  indexId="index">
								<tr <%=index % 2 == 0 ? "" : "class='odd'"%>>
									<td><input type="button" value="削除" onclick="deleteRow(this.parentNode.parentNode.rowIndex)"/></td>
									<td><bean:write name="applicationBean" property="name"/></td>
									<td><bean:write name="applicationBean" property="type"/></td>
								</tr>
							</logic:iterate>
						</tbody>
					</logic:notEmpty>
				</table>
				<!-- end #result--></div>
				<ul id="button">
					<li><html:button property="ok" value="OK" onclick="javascript:registData()" /></li>
					<li><html:button property="cancel" value="Cancel" onclick="javascript:cancelClick()" /></li>
				</ul>
				<br />
				<input type="hidden" id="policyName" name="name" value="<bean:write name="_PolicyForm" property="name"/>"/>
				<input type="hidden" id="radio" name="radio" value="<bean:write name="_PolicyForm" property="radio"/>"/>
				<input type="hidden" id="cancelFlg" name="cancelFlg" value="<bean:write name="_PolicyForm" property="cancelFlg"/>"/>
			</form>
			<!-- end #blockApplication -->
		</div>
	<!-- end #content --></div>
</body>
<%@ include file="C-FOOTER-01.jspf"%>
<!-- InstanceEnd --></html:html>
