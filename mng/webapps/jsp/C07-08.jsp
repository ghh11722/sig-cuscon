<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>
<%@ taglib uri="/cuscon-struts" prefix="cs" %>

<bean:define id="titleUsecaseName" scope="page" value="コンフィグ管理" />
<bean:define id="titleScreenName" scope="page" value="FWコンフィグ設定表示画面" />
<bean:define id="screenName" scope="page" value="FWコンフィグ設定ダウンロード" />
<bean:define id="screenID" scope="page" value="0001" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true" lang="true">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>

<script language="JavaScript" type="text/JavaScript">
<!--
	function submitDownload(){
		var obj = document._FwConfigForm;
		var str = Form.serialize('_FwConfigForm');
		new Ajax.Request( "<bean:write name='contextPath' />/customer/fwdownload/fwcfgMakeCsvBL.do",
			{
				"method": "post",
				"parameters": str,
				onSuccess: function(objHttp) {
					hideProgress();
					if( checkResponse(objHttp) == true ) {
						obj.action='<bean:write name='contextPath' />/customer/fwdownload/fwcfgDlBL.do';
						obj.submit();
						obj.action='<bean:write name='contextPath' />/customer/fwdownload/fwcfgBL.do';
					}
				},
				onFailure: function(objHttp) {
					alert("HTTP通信失敗");
					hideProgress();
				}
			}
		);
		displayProgress();
	}
	function checkResponse(objHttp) {
		if( objHttp.responseXML != undefined ) {
			var doc = objHttp.responseXML;
			var errormsg = doc.getElementsByTagName("errormsg");
			if( errormsg.length > 0 ) {
				eval(errormsg[0].firstChild.nodeValue);
				return false;
			}
			return true;
		}
	}
// -->
</script>

<div id="content">
	<div id="blockLog">
	<p id="pankuzu">Top &raquo; コンフィグ管理 &raquo; コンフィグダウンロード</p>
	<h3><bean:write name="screenName" scope="page" /></h3>
	<div id="operationBox1">
		<ts:form action="/customer/fwdownload/fwcfgBL" method="post" styleId="_FwConfigForm">
			<br/><br/>
			<html:hidden name="_FwConfigForm" property="transitionFlg"/>
			&raquo;&nbsp;FWコンフィグ設定ダウンロード&nbsp;<html:button property="forward_submitFwConfigMakeCsv" value="ダウンロードする" onclick="submitDownload();" />※FW設定をダウンロードします。
			<br/><br/>
			<font color="red">
				<p>&nbsp;&nbsp;&nbsp;&nbsp;【注意】ダウンロードを行う際はInternetExplorerにて以下の設定を行ってください。</p>
				<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;１．「ツール」⇒「インターネットオプション」⇒「セキュリティ」⇒該当するゾーンを選択⇒「レベルのカスタマイズ」をクリック</p>
				<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;２．「ダウンロード」⇒「ファイルのダウンロード」の項目で「有効にする」を選択し、「OK」をクリック</p>
			</font>
		</ts:form>
	</div>

	</div>
</div>

<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
