<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="メイン画面" />
<bean:define id="screenName" scope="page" value="企業管理者メイン" />
<bean:define id="screenID" scope="page" value="C05-05" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<head>
	<script type="text/javascript">
	<!--
	var submit_flag = 0;
	var chkNum = 0;
	var indexList = new Array();

	document.onkeydown = KeyEvent;
	function KeyEvent(e){
	    var pressKey=event.keyCode;
	    if(pressKey == 13) {
		return false;
	    }
	}

	function getCheck(obj,index){
		if (obj.checked) {
			chkNum++;
			indexList.push(index);
		} else {
			chkNum--;
			for(i=0;i<indexList.length;i++) {
				if(indexList[i]==index) {
					indexList.splice(i, 1);
					i--;
				}
			}
		}
	}

	function add(){
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		var str = "";
		for(i=0;i<indexList.length;i++) {
			if(i==0) {
				str = indexList[i];
			} else {
				str = str + "," + indexList[i];
			}
		}
		document.geneForm.checkIndex.value = str;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/RegistAddressGroupBL" />";
        document.geneForm.submit();
	}
	function cancelClick() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/AddressGroupsSCR" />";
	    document.geneForm.submit();
	}
	-->
	</script>
</head>

<body onunload="unLoadWindow()">
	<div id="content">
		<div id="objectAddressGroup">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; オブジェクト設定 &raquo; アドレスグループ設定 &raquo; アドレスグループ追加</p>
			<h3>アドレスグループ追加</h3>
				<form name="geneForm" method="post">
					<input type = "hidden" name="checkIndex" />
					<dl>
						<dt>オブジェクト名</dt>
						<dd><input name ="name"/></dd>
						<dt>設定済みIPアドレス</dt>
						<dd>
							<ul>
								<logic:iterate id="userBean" name="_AddressGroupsForm" property="addressAndAddressGrp4Add" indexId="idx">
									<li>
										<input type="checkbox" onclick="getCheck(this, <%= idx %>)"/>
										<logic:equal name="userBean" property="objectType" value="1">
											<img alt="" src="../../../img/group_on.png" width="13" height="13"/>
										</logic:equal>
										<logic:notEqual name="userBean" property="objectType" value="1">
											<img alt="" src="../../../img/group_off.png" width="13" height="13"/>
										</logic:notEqual>
										<bean:write name="userBean" property="name" />
									</li>
								</logic:iterate>
							</ul>
						</dd>
					</dl>
					<ul id="button">
						<li><input type="button" value="OK" onclick="add()" /></li>
						<li><input type="button" value="Cancel" onclick="cancelClick()"/></li>
					</ul>
				</form>
			<!-- end objectServices --></div>
		<!-- InstanceEndEditable -->
		<!-- end #content --></div>
</body>

<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
