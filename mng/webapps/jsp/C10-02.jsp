<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="ログダウンロード機能" />
<bean:define id="titleScreenName" scope="page" value="ファイアウォールログダウンロード画面" />
<bean:define id="screenName" scope="page" value="ファイアウォールログ" />
<bean:define id="screenID" scope="page" value="0001" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true" lang="true">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>

<script language="JavaScript" type="text/JavaScript">
<!--
function jumpFileDownload(index){
	document._LogDlCommonForm.selectedFileIdx.value = index;
	var obj = document._LogDlCommonForm;
	//var str = Form.serialize('_LogDlCommonForm');
	obj.action='<bean:write name='contextPath' />/customer/logdownload/tlogDlBL.do';
	obj.submit();
}
//-->
</script>

<div id="content">
<div id="blockLog">
	<p id="pankuzu">Top &raquo; ログダウンロード &raquo; ファイアウォールログ</p>
	<h3><bean:write name="screenName" scope="page" /></h3>
	<div id="operationBox2">
	<form name="_LogDlCommonForm" method="post">
		<ul>
			<logic:iterate id="userBean" name="_LogDlCommonForm" property="fileNameList"  indexId="index">
				<li>&raquo; <a href="javascript:jumpFileDownload(<%= index %>)">
						<bean:write name="userBean" />
					</a>
				</li>
			</logic:iterate>
		</ul>
		<html:hidden name="_LogDlCommonForm" property="selectedFileIdx" value="0"/>
	</form>
	</div>
</div>
</div>

<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
