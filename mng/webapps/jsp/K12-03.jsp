<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="パスワード初期化機能" />
<bean:define id="titleScreenName" scope="page" value="パスワード初期化完了画面" />
<bean:define id="screenName" scope="page" value="パスワード初期化完了" />
<bean:define id="screenID" scope="page" value="0001" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true" lang="true">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-01.jspf"%>

<div id="content">
	<div id="blockCPassChange">
		<h3>お客様管理者パスワード初期化</h3>
		<table>
		<thead>
			<tr>
				<th scope="col">ログインID</th>
				<th scope="col">ファイアウォールID</th>
				<th scope="col">ヴァーチャルシステム(VSYS)</th>
				<th scope="col">最終変更日時</th>
			</tr>
		</thead>
<logic:notEmpty name="_InitPasswordForm" property="target_account">
		<tbody id="id_customers">
			<tr>
				<td><bean:write name="_InitPasswordForm" property="target_account.loginId" /></td>
				<td><bean:write name="_InitPasswordForm" property="target_account.firewallId" /></td>
				<td><bean:write name="_InitPasswordForm" property="target_account.vsysId" /></td>
				<td><bean:write name="_InitPasswordForm" property="target_account.last_login_date" /></td>
			</tr>
		</tbody>
</logic:notEmpty>
		</table>
<logic:notEmpty name="_InitPasswordForm" property="target_account">
<ts:messages id="msgs" message="true">
		<p><bean:write name="msgs" /></p>
</ts:messages>
</logic:notEmpty>
	</div>
</div>

<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
