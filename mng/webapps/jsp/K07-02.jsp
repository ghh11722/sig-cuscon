<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna" prefix="t" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="コミット" />
<bean:define id="screenName" scope="page" value="コミット確認" />
<bean:define id="screenID" scope="page" value="K07-02" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">

<head>
<script language="JavaScript">
<!--
	var submit_flag = 0;
	function unLoadWindow()
	{
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
	 	window.opener.location.href = "CommitProcessingSCR.do";
	}

	function cancelClick()
	{
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;

		window.opener.location.href = "CommitSCR.do";
	}
//-->
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Customer Control System</title>
<%-- この画面はCSSを適用しない
<link href="../../../css/base.css" rel="stylesheet" type="text/css" />
<link href="../../../css/sidebar_customer.css?20121101" rel="stylesheet" type="text/css" />
--%>
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>

<body>
<br/>
<%-- 20120827 kkato@PROSITE add start --%>
<%--
<logic:notEqual name="_CommitForm" property="denyAlert" value="">
	<bean:write name="_CommitForm" property="denyAlert" /><br/>
</logic:notEqual>
<hr>
--%>
<bean:size id="policyRulesSize" name="_CommitForm" property="policyRules"/>
<logic:notEqual name="policyRulesSize" value="0">
すべての通信を遮断する可能性のあるポリシーが含まれています。本当に
</logic:notEqual>
<%-- 20120827 kkato@PROSITE add end --%>
コミットしてよろしいですか？<br/>

<br/>
<ts:form action="/admin/fwsetting/commit/CommitProcessingSCR" method="post">
	<input type="button" value="OK" onclick="unLoadWindow()"/>
	<input type="button" value="Cancel"  onclick="cancelClick()"/>
</ts:form>

</body>
<!-- InstanceEnd -->
</html:html>
