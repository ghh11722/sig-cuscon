<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="コミット" />
<bean:define id="screenName" scope="page" value="コミット処理中" />
<bean:define id="screenID" scope="page" value="C06-04" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<body>
	<div id="content"><!-- InstanceBeginEditable name="content" -->
		<div id="commit">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; コミット</p>
			<h3>コミット</h3>
			<div style="height:620px;">
				<div align="left">Commit Operation</div>
				<table width="50%">
				    <tr>
				    	<td>
							<table>
							 	<!--<caption  align="left">Commit Operation</caption>-->
								<thead>
									<tr class="odd">
										<th abbr="Operation">Operation</th>
										<th abbr="Commit">コミット</th>
									</tr>
								</thead>
								<tbody>
								<tr id="tr1">
									<th abbr="Starttime">受付時間</th>
									<th abbr="StarttimeContent"><bean:write name="_CommitForm" property="startTime"/>
									</th>
								</tr>
								 <tr id="tr2">
									<th abbr="Status">処理状態</th>
									<th abbr="Processing"><bean:write name="_CommitForm" property="acceptStatus"/>
									</th>
								</tr>
								 <tr id="tr3">
									<th abbr="Details">メッセージ</th>
									<th abbr="DetailsContent"><bean:write name="_CommitForm" property="message"/>
									</th>
								</tr>
								</tbody>
							</table>
				    	</td>
					</tr>
				</table>
			</div>
	<!-- end commit --></div>
	<!-- InstanceEndEditable -->
	<!-- end #content --></div>
</body>
<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
