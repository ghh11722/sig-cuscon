<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="cssCommonFileName" scope="page" value="/css/base.css"/>
<bean:define id="cssExtendFileName" scope="page" value="/css/extend.css" />

<bean:define id="titleUsecaseName" scope="page" value="ログイン機能" />
<bean:define id="titleScreenName" scope="page" value="ログイン画面" />
<bean:define id="screenName" scope="page" value="ログイン" />
<bean:define id="screenID" scope="page" value="C01-01" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true" lang="true">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Sun,6 Jun 2006 00:00:00 GMT" />
<title>Customer Control System</title>
<bean:page id="requestObj" property="request"/>
<bean:define id="contextPath" name="requestObj" property="contextPath" />
<link href="<bean:write name='contextPath' /><bean:write name="cssCommonFileName" scope="page"/>" rel="stylesheet" type="text/css" />
<link href="<bean:write name='contextPath' /><bean:write name="cssExtendFileName" scope="page"/>" rel="stylesheet" type="text/css" />
</head>
<body>
<script type="text/javascript">
<!--
var submit_flag = 0;
function submitForm() {
	if( submit_flag == 1 ){ return; }
	submit_flag = 1;

	if(document.getElementById) {
		var elem = document.getElementById("progressDialog");
		elem.style.visibility = 'visible';
	}
	document._CustomerLoginForm.submit();
}
// -->
</script>

<div id="wrapper">
	<div id="loginCustomer">
		<h1>Customer Control System</h1>
		<ts:form action="/customer/login/loginBL" method="post">
		<dl>
			<dt>ログインID</dt>
			<dd><html:text property="loginId" maxlength="20" size="25" styleClass="broad" /></dd>
			<dt>パスワード</dt>
			<dd><html:password property="password" maxlength="14"  size="25" styleClass="broad" /></dd>
			<dt>ファイアウォールID</dt>
			<dd><html:text property="firewallId" maxlength="20"  size="25" styleClass="broad" /></dd>
			<dd><html:button property="Login" value="ログイン" onclick="submitForm()" /></dd>
		</dl>
		</ts:form>
	</div>
	<div id="progressDialog" >
		<div id="loading" >NowLoading...</div>
	</div>
</div>

<ts:ifErrors>
<script language="JavaScript" type="text/JavaScript">
<!--
	alert("<ts:errors/>");
//-->
</script>
</ts:ifErrors>
</body>

</html:html>
