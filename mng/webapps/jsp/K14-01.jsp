<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="お知らせメッセージ登録画面" />
<bean:define id="screenName" scope="page" value="お知らせメッセージ登録" />
<bean:define id="screenID" scope="page" value="K14-01" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true" lang="true">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-01.jspf"%>

<div id="content">
	<div id="blockOshirase">
		<h3>お知らせ登録</h3>

		<ts:form action="/admin/infomation/confirmBL"  method="post">
		<h4>現在の表示内容</h4>
		<div id="oshiraseNow">
		<pre><bean:write filter="false" name="_InfomationEntryForm" property="infomation"/></pre>
		</div>

		<h4>編集内容</h4>
		<div id="oshiraseEdit">
		<html:textarea property="newInfomation" cols="116" rows="15" />
		</div>
		<ul id="button">
			<li><input type="submit" name="confirm" value="プレビュー" /></li>
			<li><input type="button" value="Cancel" onclick="document.location='<bean:write name='contextPath' />/admin/mainBL.do'" /></li>
		</ul>
		</ts:form>
	</div>
</div>

<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
