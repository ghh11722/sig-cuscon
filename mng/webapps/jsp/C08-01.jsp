<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="FWレポーティング機能" />
<bean:define id="titleScreenName" scope="page" value="レポート" />
<bean:define id="screenName" scope="page" value="top-applications-summary" />
<bean:define id="screenID" scope="page" value="C08-01" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<script>

	function jump() {
		document._ReportForm.submit();
	}

</script>

<div id="content">
		<div id="blockReport">
			<p id="pankuzu">Top &raquo; レポート閲覧 &raquo; アプリケーション(期間指定)</p>

			<h3>アプリケーション(期間指定)</h3>
			<ts:form action="/customer/report/TopApplicationSummaryBL" method="post" >
				<p id="summary">- アプリケーションごとのアクセス数集計(上位50件)</p>

				<select name="targetPeriod" onchange="jump()">
					<logic:equal name="_ReportForm" property="targetPeriod" value="last-hour" >
						<option value="last-hour" selected="selected">Last Hour</option>
					</logic:equal>
					<logic:notEqual name="_ReportForm" property="targetPeriod" value="last-hour" >
						<option value="last-hour">Last Hour</option>
					</logic:notEqual>
					<logic:equal name="_ReportForm" property="targetPeriod" value="last-12-hrs" >
						<option value="last-12-hrs" selected="selected">Last 12 Hours</option>
					</logic:equal>
					<logic:notEqual name="_ReportForm" property="targetPeriod" value="last-12-hrs" >
						<option value="last-12-hrs">Last 12 Hours</option>
					</logic:notEqual>
					<logic:equal name="_ReportForm" property="targetPeriod" value="last-24-hrs" >
						<option value="last-24-hrs" selected="selected">Last 24 Hours</option>
					</logic:equal>
					<logic:notEqual name="_ReportForm" property="targetPeriod" value="last-24-hrs" >
						<option value="last-24-hrs">Last 24 Hours</option>
					</logic:notEqual>
					<logic:equal name="_ReportForm" property="targetPeriod" value="last-calendar-day" >
						<option value="last-calendar-day" selected="selected">Last Calendar Day</option>
					</logic:equal>
					<logic:notEqual name="_ReportForm" property="targetPeriod" value="last-calendar-day" >
						<option value="last-calendar-day">Last Calendar Day</option>
					</logic:notEqual>
					<logic:equal name="_ReportForm" property="targetPeriod" value="last-7-days" >
						<option value="last-7-days" selected="selected">Last 7 Days</option>
					</logic:equal>
					<logic:notEqual name="_ReportForm" property="targetPeriod" value="last-7-days" >
						<option value="last-7-days">Last 7 Days</option>
					</logic:notEqual>
					<logic:equal name="_ReportForm" property="targetPeriod" value="last-30-days" >
						<option value="last-30-days" selected="selected">Last 30 Days</option>
					</logic:equal>
					<logic:notEqual name="_ReportForm" property="targetPeriod" value="last-30-days" >
						<option value="last-30-days">Last 30 Days</option>
					</logic:notEqual>
				</select>
				<p>：レポート取得対象を選択してください。</p>
				<table>
					<thead>
						<tr class="odd">
							<th scope="col">リスク<!--risk-of-name--></th>
							<th scope="col">アプリケーション<!--name--></th>
							<th scope="col">セッション数<!--nsess--></th>
							<th scope="col">転送量(byte)<!--nbytes--></th>
							<th scope="col">カウント数<!--nthreds--></th>
						</tr>
					</thead>
					<tbody>
						<logic:notEmpty name="_ReportForm" property="topApplicationSummaryList">
							 <logic:iterate id="appsBean" name="_ReportForm" property="topApplicationSummaryList" indexId="index">
							   <tr <%=index % 2 == 0 ? "" : "class='odd'"%>>
							        <td><bean:write name="appsBean" property="rsikOfName"/></td>
							        <td><bean:write name="appsBean" property="name"/></td>
							        <td><bean:write name="appsBean" property="nsess"/></td>
							        <td><bean:write name="appsBean" property="nbytes"/></td>
							        <td><bean:write name="appsBean" property="nthreats"/></td>
						    	</tr>
						    </logic:iterate>
						</logic:notEmpty>
					</tbody>
				</table>
			</ts:form>
		<!-- end blockReport -->
		</div>
	<!-- end #content --></div>
<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
