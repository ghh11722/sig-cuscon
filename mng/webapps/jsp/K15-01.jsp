<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page import="java.util.*, java.io.*" %>
<%@ page import="org.apache.struts.util.*"%>

<%@ taglib uri="/struts-html" prefix="html"%>
<%@ taglib uri="/struts-bean" prefix="bean"%>
<%@ taglib uri="/struts-logic" prefix="logic"%>
<%@ taglib uri="/terasoluna-struts" prefix="ts"%>
<%@ taglib uri="/terasoluna" prefix="t"%>

<bean:define id="titleUsecaseName" scope="page" value="ログダウンロード機能" />
<bean:define id="titleScreenName" scope="page" value="ファイアウォールログダウンロード画面" />
<bean:define id="screenName" scope="page" value="ファイアウォールログ" />
<bean:define id="screenID" scope="page" value="0001" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="true" lang="true">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-03.jspf"%>

<script language="JavaScript" type="text/JavaScript">
<!--
	function jumpLogDl(index) {
		document._LogDlCommonForm.ymlstIdx.value = index;
		document._LogDlCommonForm.action = "<html:rewrite action="/admin/logdownload/tlogListBL" />";
		document._LogDlCommonForm.submit();
	}
// -->
</script>

<div id="content">
	<div id="blockLog">
		<p id="pankuzu">Top &raquo; ログダウンロード &raquo; ファイアウォールログ</p>
		<h3><bean:write name="screenName" scope="page" /></h3>
		<div id="operationBox2">
		<form name="_LogDlCommonForm" method="post">
			<logic:notEmpty name="_LogDlCommonForm" property="message"><bean:write name="_LogDlCommonForm" property="message"/>	</logic:notEmpty>
			<logic:notEmpty name="_LogDlCommonForm" property="ymList">
				<ul>
					<logic:iterate id="userBean" name="_LogDlCommonForm" property="ymList"  indexId="index">
						<li>&raquo; <a href="javascript:jumpLogDl(<%= index %>)">
								<script type="text/javascript">
									{
										var yyyymm = "<bean:write name='userBean'/>";
										var yyyy = yyyymm.slice(0,4);
										var mm = yyyymm.slice(4,6);
										var txt = yyyy + "年" + mm + "月";
										document.write(txt);
									}
								</script>
							</a>
						</li>
					</logic:iterate>
				</ul>
			</logic:notEmpty>
			<html:hidden name="_LogDlCommonForm" property="ymlstIdx" value="0"/>
		</form>
		</div>
	</div>
</div>

<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
