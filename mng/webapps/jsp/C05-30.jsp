<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お知らせメッセージ機能" />
<bean:define id="titleScreenName" scope="page" value="メイン画面" />
<bean:define id="screenName" scope="page" value="企業管理者メイン" />
<bean:define id="screenID" scope="page" value="C05-30" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="C-HEADER-01.jspf"%>
<%@ include file="C-MENU-01.jspf"%>
<head>
	<script type="text/javascript">
	<!--
	var submit_flag = 0;
	function clickOk()
	{
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document.geneForm.protocol.value = document.geneForm.selectProtocol.options[document.geneForm.selectProtocol.options.selectedIndex].value;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/UpdateServiceBL" />";
        document.geneForm.submit();
	}
	function cancelClick() {
		if( submit_flag == 1 ){ return; }
		submit_flag = 1;
		document.geneForm.action = "<html:rewrite action="/customer/fwsetting/objectsetting/ServicesBL" />";
	    document.geneForm.submit();
	}

	-->
	</script>
</head>
<body onunload="unLoadWindow()">
	<div id="content">
		<div id="objectServices">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; オブジェクト設定 &raquo; サービス設定 &raquo; サービス編集</p>
			<h3>サービス編集</h3>
			<form action="" name="geneForm" method="post">
				<input type="hidden" name="oldServiceName" value="<bean:write name="_ServicesForm" property="name"/>"/>
				<input type="hidden" name="protocol" />
				<dl>
					<dt>オブジェクト名</dt>
					<dd><input name="newServiceName" value="<bean:write name="_ServicesForm" property="name"/>"/></dd>

					<dt>プロトコル</dt>
					<dd>
					<logic:equal name="_ServicesForm" property="protocol" value="tcp">
						<select name="selectProtocol">
							<option value="tcp" selected="selected">TCP</option>
							<option value="udp">UDP</option>
						</select>
					</logic:equal>
					<logic:notEqual name="_ServicesForm" property="protocol" value="tcp">
						<select name="selectProtocol">
							<option value="tcp">TCP</option>
							<option value="udp" selected="selected">UDP</option>
						</select>
					</logic:notEqual>
					</dd>

 					<dt>ポート</dt>
					<dd><input name="port" value="<bean:write name="_ServicesForm" property="port"/>"/></dd>
				</dl>
				<ul id="button">
					<li><input type="button" value="OK" onclick="clickOk()" /></li>
					<li><input type="button" value="Cancel" onclick="cancelClick()"/></li>
				</ul>
			</form>
	<!-- end objectServices --></div>
	<!-- end content --></div>
</body>
<%@ include file="C-FOOTER-01.jspf"%>
</html:html>
