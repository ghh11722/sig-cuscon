<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="/struts-html" prefix="html" %>
<%@ taglib uri="/struts-bean" prefix="bean" %>
<%@ taglib uri="/struts-logic" prefix="logic" %>
<%@ taglib uri="/terasoluna-struts" prefix="ts" %>
<%@ taglib uri="/terasoluna" prefix="t" %>

<bean:define id="titleUsecaseName" scope="page" value="お客様企業メイン" />
<bean:define id="titleScreenName" scope="page" value="ロールバック" />
<bean:define id="screenName" scope="page" value="Service参照" />
<bean:define id="screenID" scope="page" value="K07-17" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html xhtml="http://www.w3.org/1999/xhtml">
<%@ include file="K-HEADER-01.jspf"%>
<%@ include file="K-MENU-03.jspf"%>
<script>
	var srvMstList = new Array();
	var mstNum = 0;
</script>
<body>
	<div id="content">
		<div id="editService">
			<p id="pankuzu">Top &raquo; FW設定機能 &raquo; ロールバック &raquo; サービス参照</p>

			<h3>サービス参照</h3>
			<ts:form action="/admin/fwsetting/rollback/RollbackBL" method="post" >
				<div id="anyOrSelect">
					<ul>
						<bean:size id="srvSize" name="_RollbackForm" property="serviceMasterList" />
						<logic:equal name="_RollbackForm" property="serviceName" value="any">
							<li><input name="ServiceRadio" type="radio" checked="checked" disabled ="disabled"/>any</li>
							<li><input name="ServiceRadio" type="radio" disabled ="disabled"/>application-default</li>
							<li><input name="ServiceRadio" type="radio" disabled ="disabled"/>選択する</li>
						</logic:equal>
						<logic:equal name="_RollbackForm" property="serviceName" value="application-default">
							<li><input name="ServiceRadio" type="radio" disabled ="disabled"/>any</li>
							<li><input name="ServiceRadio" type="radio" checked="checked" disabled ="disabled"/>application-default</li>
							<li><input name="ServiceRadio" type="radio" disabled ="disabled"/>選択する</li>
						</logic:equal>
						<logic:notEqual name="_RollbackForm" property="serviceName" value="any">
							<logic:notEqual name="_RollbackForm" property="serviceName" value="application-default">
								<li><input name="ServiceRadio" type="radio" disabled ="disabled"/>any</li>
								<li><input name="ServiceRadio" type="radio" disabled ="disabled"/>application-default</li>
								<li><input name="ServiceRadio" type="radio" checked="checked" disabled ="disabled"/>選択する</li>
							</logic:notEqual>
						</logic:notEqual>
					</ul>
				</div>

			<div id="available">
				<h4>オブジェクトの選択</h4>
					<ul>
						<logic:iterate id="serviceMstBean" name="_RollbackForm" property="serviceMasterList"  indexId="index">
							<li>
								<input name="checkSrv" type="checkbox" disabled ="disabled"/>
								<logic:equal name="serviceMstBean" property="objectType" value="0">
									<img alt="" src="../../../img/group_off.png" width="13" height="13"/>
								</logic:equal>
								<logic:equal name="serviceMstBean" property="objectType" value="1">
									<img alt="" src="../../../img/group_off.png" width="13" height="13"/>
								</logic:equal>
								<logic:equal name="serviceMstBean" property="objectType" value="2">
									<img alt="" src="../../../img/group_on.png" width="13" height="13"/>
								</logic:equal>
								<bean:write name="serviceMstBean"  property="name"/><br/>
							</li>
								<script type="text/javascript">
										{
											srvMstList[mstNum] = "<bean:write name='serviceMstBean'  property='name'/>";
											mstNum++;
										}
									</script>
						</logic:iterate>
						<logic:iterate id="serviceBean" name="_RollbackForm" property="serviceList"  indexId="index">
							<script type="text/javascript">
										{
											for(i=0; i<srvMstList.length; i++) {
												if(srvMstList[i] == "<bean:write name='serviceBean'  property='name'/>") {
													document._RollbackForm.checkSrv[i].checked=true;

												}
											}
										}
									</script>
						</logic:iterate>
					</ul>
				<!-- end #available --></div>

				<ul id="button">
					<li><ts:submit value="Cancel" /></li>
				</ul>
			</ts:form>
		<!-- end editService -->
		</div>
	<!-- end #content --></div>

</body>

<%@ include file="K-FOOTER-01.jspf"%>
</html:html>
