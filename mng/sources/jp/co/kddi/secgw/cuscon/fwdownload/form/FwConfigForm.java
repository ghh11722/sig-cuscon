/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : FwConfigForm.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2012/08/27     kkato               初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwdownload.form;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwdownload.vo.FwConfigRecord;
import jp.co.kddi.secgw.cuscon.logmonitor.common.LogMonitorCommon;
import jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx;

/**
 * クラス名 : FwConfigForm
 * 機能概要 : FWコンフィグ設定画面のアクションフォーム
 * 備考 :
 * @author kato
 * @version 1.0 kato
 *          Created 2012/08/27
 *          新規作成
 * @see
 */
public class FwConfigForm extends ValidatorActionFormEx {

	//
	private static final long serialVersionUID = 1L;

	// FwConfigリスト
	private List<FwConfigRecord> recList = null;

	// 遷移フラグ
	private String transitionFlg = LogMonitorCommon.TR_MENU;

	/**
	 * メソッド名 : recListのGetterメソッド
	 * 機能概要 : recListを取得します。
	 * @return recList FwConfigリスト
	 */
	public List<FwConfigRecord> getRecList() {
	    return recList;
	}

	/**
	 * メソッド名 : recListのSetterメソッド
	 * 機能概要 : recListを設定します。
	 * @param recList FwConfigリスト
	 */
	public void setRecList(List<FwConfigRecord> recList) {
	    this.recList = recList;
	}

	/**
	 * メソッド名 : transitionFlgのSetterメソッド
	 * 機能概要 : transitionFlgを設定します。
	 * @param flg
	 */
	public void setTransitionFlg(String flg) {
		this.transitionFlg = flg;
	}

	/**
	 * メソッド名 : transitionFlgのGetterメソッド
	 * 機能概要 : transitionFlgを取得します。
	 * @return transitionFlg
	 */
	public String getTransitionFlg() {
		return transitionFlg;
	}

}
