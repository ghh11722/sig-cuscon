/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : FwConfigMakeCsvBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2012/08/27     kkato               初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwdownload.blogic;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

import jp.co.kddi.secgw.cuscon.common.CusconFWWriteFile;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwdownload.dto.FwConfigInput;
import jp.co.kddi.secgw.cuscon.fwdownload.dto.FwConfigOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.common.CommitStatusManager;
import jp.co.kddi.secgw.cuscon.logmonitor.common.LogMonitorCommon;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.h2.util.StringUtils;

/**
 * クラス名 : FwConfigMakeCsvBLogic
 * 機能概要 :
 * 備考 :
 * @author kkato
 * @version 1.0 kkato
 *          Created 2012/08/27
 *          新規作成
 * @see
 */
public class FwConfigMakeCsvBLogic implements BLogic<FwConfigInput> {
	// DAO
	private QueryDAO queryDAO = null;
	private UpdateDAO updateDAO = null;

	// メッセージクラス
	private MessageAccessor messageAccessor = null;

	// ログクラス
	private static Log log = LogFactory.getLog(FwConfigMakeCsvBLogic.class);

	/**
	 * メソッド名 : トラフィックログCSV作成処理
	 * 機能概要 : DBを検索し、スレットログのCSVファイルを作成し、
	 *			  Zip形式にてサーバに保存
	 * @param param
	 * @return
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(FwConfigInput param) {

		log.debug("スレットログファイル作成");

		//共通セッション情報
		CusconUVO uvo = param.getUvo();

		FwConfigOutput out = new FwConfigOutput(param);		// 画面表示のパラメータ保持
		String returnStr = "failure";					// リターン文字列
		BLogicMessages errors = new BLogicMessages();	// エラーメッセージ
		//String sqlPatern = null;						// 実行するSQLのパターン
		String tmpDir = null;							// テンポラリディレクトリ
		String tmpFile = null;							// テンポラリファイル

        CommitStatusManager commtStatManager = new CommitStatusManager();

		try {
        	// コミットステータス確認
        	if (commtStatManager.isCommitting_Processing(queryDAO, param.getUvo())) {
        		errors.add("message", new BLogicMessage("DK070019"));
				log.error(messageAccessor.getMessage("DK070019" ,null, param.getUvo()));
				log.debug("コミットステータスNG");
				return makeResult(out, returnStr, errors);
        	}
        } catch (Exception e) {
        	// コミットステータス確認失敗ログ出力
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage("EK071004", objectgArray, param.getUvo()));
        	errors.add("message", new BLogicMessage("DK070025"));
			log.debug("コミットステータス確認失敗");
			return makeResult(out, returnStr, errors);
        }

		while (true) {
			//セッション情報チェック
			log.debug("セッション情報チェック");
			if (uvo == null) {
				//セッション情報NG
				errors.add("message", new BLogicMessage("DK080205"));
				log.error(messageAccessor.getMessage("EK081205" ,null, param.getUvo()));
				break;
			}

			if (uvo.getVsysId() == null){
				//VSYS-ID NG
				errors.add("message", new BLogicMessage("DK080205"));
				log.error(messageAccessor.getMessage("EK081206",null, param.getUvo()));
				break;
			}
			StringBuffer chkBuff = new StringBuffer(uvo.getVsysId());
			if (chkBuff.length() == 0){
				//VSYS-ID NG
				errors.add("message", new BLogicMessage("DK080205"));
				log.error(messageAccessor.getMessage("EK081206",null, param.getUvo()));
				break;
			}

			// 作業テンポラリの作成と取得
			tmpDir = LogMonitorCommon.makeTmp(uvo.getLoginId(), queryDAO, log, messageAccessor, uvo);
			if(tmpDir == null){
				// 失敗した場合はエラー
				errors.add("message", new BLogicMessage("DK080206"));
				log.error(messageAccessor.getMessage("EK081207",null, param.getUvo()));
				break;
			}

			// サブタイプ毎のCSV・ZIPファイル名の接頭語を設定
			String tmpFilePrefix = "fwcfg";

			// TXTファイル名生成
			Calendar cal = Calendar.getInstance();
			//String tmpFile = String.format( "%s/ids_ips_%04d%02d%02d.csv"
			String tmpName = String.format( "%s_%04d%02d%02d.TXT",
					tmpFilePrefix,
					cal.get(Calendar.YEAR),	// 年
					cal.get(Calendar.MONTH)+1,// 月
					cal.get(Calendar.DATE)	// 日
				);
			tmpFile = String.format("%s/%s", tmpDir, tmpName);

	        try{
	        	  //File file = new File(args[2]);
	        	  FileWriter filewriter = new FileWriter(tmpFile, false);
//	        	  for(int i=0; i<policyList.size(); i++) {
//	        		  filewriter.write("コマンド実行:" + policyList.get(i).getName() + "\r\n");
//	        	  }
	        	  filewriter.write("");
	        	  	filewriter.flush();
	        	  	filewriter.close();
	        } catch(IOException e) {
    			// ファイル出力処理失敗ログ出力
				e.printStackTrace();
				errors.add("message", new BLogicMessage("DK080212"));
				String[] errMsg = {tmpName};
				log.fatal(messageAccessor.getMessage("EK081210",errMsg, param.getUvo()));
				break;
	        }
			out.setTmpDir(tmpDir);
			out.setZipName(tmpName);
			out.setZipFilePath(tmpFile);
			returnStr = "success";
			break;
		}

		// ポリシー取得
		log.debug("ポリシー取得");
		CusconFWWriteFile cuscon_fw = new CusconFWWriteFile();
		try {
			cuscon_fw.setFWInfo(updateDAO, queryDAO, uvo, tmpFile);
		} catch (Exception e) {
			e.printStackTrace();
//			// トランザクションロールバック
//			TransactionUtil.setRollbackOnly();
			// エラー : ポリシー取得失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FC011016", objectgArray));
			errors.add("message", new BLogicMessage("DC010017"));
			returnStr = "failure";
			//return makeResult(out, returnStr, errors);
		}
		log.debug("ポリシー取得 終了:" + returnStr);

		// エラーが発生していたら、テンポラリディレクトリを削除する
		if(returnStr.compareTo("success") != 0){
			if(tmpDir != null){
				LogMonitorCommon.deleteDir(new File(tmpDir));
			}
		}

		log.debug(String.format("スレットログファイル作成終了(状態：%s)", returnStr));
		return makeResult(out, returnStr, errors);
	}

	/**
	 * メソッド名 : レスポンスの生成
	 * 機能概要 : レスポンスデータの生成
	 * @param out スポンス出力用のインタフェースクラス
	 * @param returnStr レスポンス文字列
	 * @param errors エラーメッセージ
	 * @return IDS/IPSログ取得処理結果を保持したBLogicResult
	 */
	public BLogicResult makeResult(FwConfigOutput out, String returnStr, BLogicMessages errors) {
		// レスポンス返却
		BLogicResult result = new BLogicResult();
		if(StringUtils.equals(returnStr, "failure") == true){
			log.debug("エラーレスポンス生成");
			result.setErrors(errors);
		}

		result.setResultString(returnStr);
		result.setResultObject(out);
		log.debug(String.format("スレットログ検索終了(状態：%s)", returnStr));

		return result;
	}

	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * queryDAOを取得します。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * queryDAOを設定します。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}

}
