/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : FwConfigOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2012/08/27     kkato               初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwdownload.dto;

import jp.co.kddi.secgw.cuscon.fwdownload.vo.FwConfigRecord;

import java.util.List;

/**
 * クラス名 : FwConfigOutput
 * 機能概要 : FWコンフィグ設定取得処理ビジネスロジックの出力データ定義
 * 備考 :
 * @author kkato
 * @version 1.0 kkato
 *          Created 2012/08/27
 *          新規作成
 * @see
 */
public class FwConfigOutput {

	// FwConfigリスト
	private List<FwConfigRecord> recList = null;

	// Zipファイルのサーバー上のパス名
	private String zipFilePath = null;

	// Zipファイルのダウンロード時のファイル名
	private String zipName = null;

	// テンポラリディレクトリパス
	private String tmpDir = null;

	// 遷移フラグ
	private String transitionFlg;

	// メッセージ
	private String message = null;

	/**
	 * メソッド名 : TrafficLogOutput
	 * 機能概要 : コンストラクタ(TrafficLogInputと共通の値について取得する。)
	 * @param param param
	 */
	public FwConfigOutput(FwConfigInput param) {
	}

	/**
	 * メソッド名 : recListのGetterメソッド
	 * 機能概要 : recListを取得します。
	 * @return recList FwConfigリスト
	 */
	public List<FwConfigRecord> getRecList() {
	    return recList;
	}

	/**
	 * メソッド名 : recListのSetterメソッド
	 * 機能概要 : recListを設定します。
	 * @param recList FwConfigリスト
	 */
	public void setRecList(List<FwConfigRecord> recList) {
	    this.recList = recList;
	}

	/**
	 * メソッド名 : zipFilePathのSetterメソッド
	 * 機能概要 : zipFilePathをセットする。
	 * @param zipFilePath Zipファイルのサーバー上のパス名
	 */
	public void setZipFilePath(String zipFilePath) {
		this.zipFilePath = zipFilePath;
	}

	/**
	 * メソッド名 : zipFilePathのGetterメソッド
	 * 機能概要 : zipFilePathを取得する。
	 * @return zipFilePath Zipファイルのサーバー上のパス名
	 */
	public String getZipFilePath() {
		return zipFilePath;
	}

	/**
	 * メソッド名 : zipNameのSetterメソッド
	 * 機能概要 : zipNameをセットする。
	 * @param zipName Zipファイルのダウンロード時のファイル名
	 */
	public void setZipName(String zipName) {
		this.zipName = zipName;
	}

	/**
	 * メソッド名 : zipNameのGetterメソッド
	 * 機能概要 : zipNameを取得する。
	 * @return zipName Zipファイルのダウンロード時のファイル名
	 */
	public String getZipName() {
		return zipName;
	}

	/**
	 * メソッド名 : tmpDirのSetterメソッド
	 * 機能概要 : tmpDirをセットする。
	 * @param tmpDir テンポラリディレクトリパス
	 */
	public void setTmpDir(String tmpDir) {
		this.tmpDir = tmpDir;
	}

	/**
	 * メソッド名 : tmpDirのGetterメソッド
	 * 機能概要 : tmpDirを取得する。
	 * @return tmpDir テンポラリディレクトリパス
	 */
	public String getTmpDir() {
		return tmpDir;
	}

	/**
	 * メソッド名 : transitionFlgのSetterメソッド
	 * 機能概要 : transitionFlgを設定します。
	 * @param flg
	 */
	public void setTransitionFlg(String flg) {
		this.transitionFlg = flg;
	}

	/**
	 * メソッド名 : transitionFlgのGetterメソッド
	 * 機能概要 : transitionFlgを取得します。
	 * @return transitionFlg
	 */
	public String getTransitionFlg() {
		return transitionFlg;
	}

	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

}
