/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : FwConfigBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2012/08/27     kkato               初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwdownload.blogic;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.h2.util.StringUtils;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwdownload.dto.FwConfigInput;
import jp.co.kddi.secgw.cuscon.fwdownload.dto.FwConfigOutput;
import jp.co.kddi.secgw.cuscon.fwdownload.vo.FwConfigRecord;
import jp.co.kddi.secgw.cuscon.fwsetting.common.CommitStatusManager;
import jp.co.kddi.secgw.cuscon.fwsetting.common.ResultJudgeData;
import jp.co.kddi.secgw.cuscon.logmonitor.common.LogMonitorCommon;
//import jp.co.kddi.secgw.cuscon.logmonitor.vo.SelectFwConfig;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
//import jp.terasoluna.fw.transaction.util.TransactionUtil;
//import jp.terasoluna.fw.util.PropertyUtil;

/**
 * クラス名 : FwConfigBLogic
 * 機能概要 : FWコンフィグ設定取得処理ビジネスロジック
 * 備考 :
 * @author kkato
 * @version 1.0 kkato
 *          Created 2012/08/27
 *          新規作成
 * @see
 */
public class FwConfigBLogic implements BLogic<FwConfigInput> {

	// QueryDAOクラス
	private QueryDAO queryDAO = null;

	// メッセージクラス
	private MessageAccessor messageAccessor = null;

	// ログクラス
	private static Log log = LogFactory.getLog(FwConfigBLogic.class);

	/**
	 * メソッド名 : IDS/IPSログ取得処理
	 * 機能概要 : IDS/IPSログの取得
	 * @param param IDS/IPSログ取得処理ビジネスロジックの入力を保持したMap
	 * @return IDS/IPSログ取得処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(FwConfigInput param) {

		log.debug("スレットログ検索");

		String returnStr = "failure";	// リータン文字列

		if(param == null){
			log.debug("インプットパラメータ：なし");
			param = new FwConfigInput();
		}

		//共通セッション情報
		CusconUVO uvo = param.getUvo();

		FwConfigOutput out = new FwConfigOutput(param);		// アウトプット
		BLogicMessages errors = new BLogicMessages();	// エラーメッセージ
		List<FwConfigRecord> recList = null; // ログ検索結果
//		int totalCount = 0; //ログ該当件数
//
//		int paramChkFlg1 = 0;	// パラメータチェックフラグ
//		int paramChkFlg2 = 0;	// パラメータチェックフラグ
//
//		SelectFwConfig selectParam = new SelectFwConfig();	// SQLパラメータ

        // 処理結果格納用
        ResultJudgeData resultJudge = new ResultJudgeData();

		//セッション情報チェック
		log.debug("セッション情報チェック");
		if (uvo == null) {
			//セッション情報NG
			errors.add("message", new BLogicMessage("DK080201"));
			log.error(messageAccessor.getMessage("EK081201" ,null, param.getUvo()));
			return makeResult(out, returnStr, errors);
		}else if (uvo.getVsysId() == null){
			//VSYS-ID NG
			errors.add("message", new BLogicMessage("DK080201"));
			log.error(messageAccessor.getMessage("EK081202",null, param.getUvo()));
			return makeResult(out, returnStr, errors);
		}
		StringBuffer chkBuff = new StringBuffer(uvo.getVsysId());
		if (chkBuff.length() == 0){
			//VSYS-ID NG
			errors.add("message", new BLogicMessage("DK080201"));
			log.error(messageAccessor.getMessage("EK081202",null, param.getUvo()));
			return makeResult(out, returnStr, errors);
		}

    	BLogicResult result = new BLogicResult();
        // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
    	result.setResultString("failure");

		CommitStatusManager commtStatManager = new CommitStatusManager();

		try {
        	// コミットステータス確認
        	if (commtStatManager.isCommitting_Processing(queryDAO, param.getUvo())) {
    			messages.add("message", new BLogicMessage("DK070019"));
    			result.setErrors(messages);
    			result.setMessages(messages);
    			result.setResultString("noneFailure");
    			log.debug("CommitBLogic処理終了");
    			return result;
        	}
        } catch (Exception e) {
        	// コミットステータス確認失敗ログ出力
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage("EK071004", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK070025"));
			result.setErrors(messages);
			result.setMessages(messages);
			result.setResultString("noneFailure");
			return result;
        }

//		//セションからVSYS-IDを取得
//		selectParam.setVirtualSystem(uvo.getVsysId());

		//遷移元判定
		if(param.getTransitionFlg().equals(LogMonitorCommon.TR_MENU)) {
			//MENUから遷移した場合は検索せずに終了
			returnStr = "success";
			out.setTransitionFlg(LogMonitorCommon.TR_FORM);
			out.setRecList(recList);
			return makeResult(out, returnStr, errors);
		}
		out.setTransitionFlg(param.getTransitionFlg());//設定されている遷移元を出力へコピー

//		// リクエスト解析
//
//		// リクエストパラメータの正常判定
//		//対象項目と絞り込み対象文字列が指定されていればOK
//		if (paramChkFlg1 == 0 && paramChkFlg2 >= 1) {
//			//リクエストパラメータNG
//			//エラー処理を記述
//			errors.add("message", new BLogicMessage("DK080202"));
//			log.error(messageAccessor.getMessage("EK081203",null, param.getUvo()));
//			return makeResult(out, returnStr, errors);
//		}else if(paramChkFlg1 >= 1 && paramChkFlg2 == 0){
//			//リクエストパラメータNG
//			//エラー処理を記述
//			errors.add("message", new BLogicMessage("DK080202"));
//			log.error(messageAccessor.getMessage("EK081204",null, param.getUvo()));
//			return makeResult(out, returnStr, errors);
//		}

//		// スレットログ検索
//		try {
//			String sqlIdSelect = null;
//			String sqlIdCount = null;
//			sqlIdSelect = "FwConfigBLogic-1";
//			sqlIdCount = "FwConfigBLogic-2";
//			// ログ検索
//			log.debug("スレットログ検索SQL発行");
//			recList = queryDAO.executeForObjectList(sqlIdSelect, selectParam);
//			// ログ件数取得
//
//		} catch (Exception e) {
//			//DBアクセスエラー
//			//エラー処理を記述
//			e.printStackTrace();
//			// トランザクションロールバック
//			TransactionUtil.setRollbackOnly();
//
//			String msgKey = "";
//			msgKey = "DK080204";	//ips/ids
//			errors.add("message", new BLogicMessage(msgKey));
//
//			String[] errMsg = {e.getMessage()};
//			log.fatal(messageAccessor.getMessage("FK081201",errMsg, param.getUvo()));
//			return makeResult(out, returnStr, errors);
//		}
		//
		returnStr = "success";

		out.setRecList(recList);
		return makeResult(out, returnStr, errors);
	}

	/**
	 * メソッド名 : レスポンスの生成
	 * 機能概要 : レスポンスデータの生成
	 * @param out スポンス出力用のインタフェースクラス
	 * @param returnStr レスポンス文字列
	 * @param errors エラーメッセージ
	 * @return IDS/IPSログ取得処理結果を保持したBLogicResult
	 */
	public BLogicResult makeResult(FwConfigOutput out, String returnStr, BLogicMessages errors) {
		// レスポンス返却
		BLogicResult result = new BLogicResult();
		if(StringUtils.equals(returnStr, "failure") == true){
			log.debug("エラーレスポンス生成");
			result.setErrors(errors);
		}

		result.setResultString(returnStr);
		result.setResultObject(out);
		log.debug(String.format("スレットログ検索終了(状態：%s)", returnStr));

		return result;
	}

	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * queryDAOを取得します。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
	    return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * queryDAOを設定します。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
	    this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}

}
