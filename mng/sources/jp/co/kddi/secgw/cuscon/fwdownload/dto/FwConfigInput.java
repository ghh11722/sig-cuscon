/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : FwConfigInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2012/08/27     kkato               初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwdownload.dto;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.logmonitor.common.LogMonitorCommon;

/**
 * クラス名 : FwConfigInput
 * 機能概要 : FWコンフィグ情報取得処理ビジネスロジックの入力データ定義
 * 備考 :
 * @author kkato
 * @version 1.0 kkato
 *          Created 2012/08/27
 *          新規作成
 * @see
 */
public class FwConfigInput {

	// 共通セッション情報
	private CusconUVO uvo = null;

	// Zipファイルのサーバー上のパス名
	private String zipFilePath = null;

	// Zipファイルのダウンロード時のファイル名
	private String zipName = null;

	// テンポラリディレクトリパス
	private String tmpDir = null;

	// 遷移フラグ
	private String transitionFlg = LogMonitorCommon.TR_MENU;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得します。
	 * @return uvo 共通セッション情報
	 */
	public CusconUVO getUvo() {
	    return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoを設定します。
	 * @param uvo 共通セッション情報
	 */
	public void setUvo(CusconUVO uvo) {
	    this.uvo = uvo;
	}

	/**
	 * メソッド名 : zipFilePathのSetterメソッド
	 * 機能概要 : zipFilePathをセットする。
	 * @param zipFilePath Zipファイルのサーバー上のパス名
	 */
	public void setZipFilePath(String zipFilePath) {
		this.zipFilePath = zipFilePath;
	}

	/**
	 * メソッド名 : zipFilePathのGetterメソッド
	 * 機能概要 : zipFilePathを取得する。
	 * @return zipFilePath Zipファイルのサーバー上のパス名
	 */
	public String getZipFilePath() {
		return zipFilePath;
	}

	/**
	 * メソッド名 : zipNameのSetterメソッド
	 * 機能概要 : zipNameをセットする。
	 * @param zipName Zipファイルのダウンロード時のファイル名
	 */
	public void setZipName(String zipName) {
		this.zipName = zipName;
	}

	/**
	 * メソッド名 : zipNameのGetterメソッド
	 * 機能概要 : zipNameを取得する。
	 * @return zipName Zipファイルのダウンロード時のファイル名
	 */
	public String getZipName() {
		return zipName;
	}

	/**
	 * メソッド名 : tmpDirのSetterメソッド
	 * 機能概要 : tmpDirをセットする。
	 * @param tmpDir テンポラリディレクトリパス
	 */
	public void setTmpDir(String tmpDir) {
		this.tmpDir = tmpDir;
	}

	/**
	 * メソッド名 : tmpDirのGetterメソッド
	 * 機能概要 : tmpDirを取得する。
	 * @return tmpDir テンポラリディレクトリパス
	 */
	public String getTmpDir() {
		return tmpDir;
	}

	/**
	 * メソッド名 : transitionFlgのSetterメソッド
	 * 機能概要 : transitionFlgを設定します。
	 * @param flg
	 */
	public void setTransitionFlg(String flg) {
		this.transitionFlg = flg;
	}

	/**
	 * メソッド名 : transitionFlgのGetterメソッド
	 * 機能概要 : transitionFlgを取得します。
	 * @return transitionFlg
	 */
	public String getTransitionFlg() {
		return transitionFlg;
	}

}
