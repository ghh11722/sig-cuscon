/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectSchedules.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/17     komakiys         初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common.vo;

import java.io.Serializable;
import java.util.List;

/**
 * クラス名 : Schedules
 * 機能概要 : スケジュールオブジェクトデータクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/17
 *          新規作成
 * @see
 */
public class Schedules implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 8395141822583356568L;
	// シーケンス番号
	private int seqNo = 0;
	// Vsys-ID
	private String vsysId = null;
	// オブジェクト名
	private String name = null;
	// 世代番号
	private int generationNo = 0;
	// Recurrence
	private int recurrence = 0;
	// RecurrenceStr
	private String recurrenceStr = null;
	// 変更フラグ
	private int modFlg = 0;
	// 開始日付
	private String startDate = null;
	// 開始時刻
	private String startTime = null;
	// 終了日付
	private String endDate = null;
	// 終了時刻
	private String endTime = null;
	// 曜日
	private int dayOfWeek = 0;
	// 日時リスト
	private List<String> timeList = null;
	// 変更後オブジェクト名
	private String modName = null;

	/**
	 * メソッド名 : seqNoのGetterメソッド
	 * 機能概要 : seqNoを取得する。
	 * @return seqNo
	 */
	public int getSeqNo() {
		return seqNo;
	}
	/**
	 * メソッド名 : seqNoのSetterメソッド
	 * 機能概要 : seqNoをセットする。
	 * @param seqNo
	 */
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得する。
	 * @return vsysId
	 */
	public String getVsysId() {
		return vsysId;
	}
	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdをセットする。
	 * @param vsysId
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : generationNoのGetterメソッド
	 * 機能概要 : generationNoを取得する。
	 * @return generationNo
	 */
	public int getGenerationNo() {
		return generationNo;
	}
	/**
	 * メソッド名 : generationNoのSetterメソッド
	 * 機能概要 : generationNoをセットする。
	 * @param generationNo
	 */
	public void setGenerationNo(int generationNo) {
		this.generationNo = generationNo;
	}
	/**
	 * メソッド名 : recurrenceのGetterメソッド
	 * 機能概要 : recurrenceを取得する。
	 * @return recurrence
	 */
	public int getRecurrence() {
		return recurrence;
	}
	/**
	 * メソッド名 : recurrenceのSetterメソッド
	 * 機能概要 : recurrenceをセットする。
	 * @param recurrence
	 */
	public void setRecurrence(int recurrence) {
		this.recurrence = recurrence;
		if(recurrence == 0) {
			setRecurrenceStr("デイリー");
		} else if(recurrence == 1) {
			setRecurrenceStr("ウィークリー");
		}else if(recurrence == 2){
			setRecurrenceStr("指定日時");
		} else {
			setRecurrenceStr("");
		}

	}
	/**
	 * メソッド名 : modFlgのGetterメソッド
	 * 機能概要 : modFlgを取得する。
	 * @return modFlg
	 */
	public int getModFlg() {
		return modFlg;
	}
	/**
	 * メソッド名 : modFlgのSetterメソッド
	 * 機能概要 : modFlgをセットする。
	 * @param modFlg
	 */
	public void setModFlg(int modFlg) {
		this.modFlg = modFlg;
	}
	/**
	 * メソッド名 : startDateのGetterメソッド
	 * 機能概要 : startDateを取得する。
	 * @return startDate
	 */
	public String getStartDate() {
		return startDate;
	}
	/**
	 * メソッド名 : startDateのSetterメソッド
	 * 機能概要 : startDateをセットする。
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	/**
	 * メソッド名 : startTimeのGetterメソッド
	 * 機能概要 : startTimeを取得する。
	 * @return startTime
	 */
	public String getStartTime() {
		return startTime;
	}
	/**
	 * メソッド名 : startTimeのSetterメソッド
	 * 機能概要 : startTimeをセットする。
	 * @param startTime
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	/**
	 * メソッド名 : endDateのGetterメソッド
	 * 機能概要 : endDateを取得する。
	 * @return endDate
	 */
	public String getEndDate() {
		return endDate;
	}
	/**
	 * メソッド名 : endDateのSetterメソッド
	 * 機能概要 : endDateをセットする。
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	/**
	 * メソッド名 : endTimeのGetterメソッド
	 * 機能概要 : endTimeを取得する。
	 * @return endTime
	 */
	public String getEndTime() {
		return endTime;
	}
	/**
	 * メソッド名 : endTimeのSetterメソッド
	 * 機能概要 : endTimeをセットする。
	 * @param endTime
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	/**
	 * メソッド名 : dayOfWeekのGetterメソッド
	 * 機能概要 : dayOfWeekを取得する。
	 * @return dayOfWeek
	 */
	public int getDayOfWeek() {
		return dayOfWeek;
	}
	/**
	 * メソッド名 : dayOfWeekのSetterメソッド
	 * 機能概要 : dayOfWeekをセットする。
	 * @param dayOfWeek
	 */
	public void setDayOfWeek(int dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}
	/**
	 * メソッド名 : timeListのGetterメソッド
	 * 機能概要 : timeListを取得する。
	 * @return timeList
	 */
	public List<String> getTimeList() {
		return timeList;
	}
	/**
	 * メソッド名 : timeListのSetterメソッド
	 * 機能概要 : timeListをセットする。
	 * @param timeList
	 */
	public void setTimeList(List<String> timeList) {
		this.timeList = timeList;
	}
	/**
	 * メソッド名 : modNameのGetterメソッド
	 * 機能概要 : modNameを取得する。
	 * @return modName
	 */
	public String getModName() {
		return modName;
	}
	/**
	 * メソッド名 : modNameのSetterメソッド
	 * 機能概要 : modNameをセットする。
	 * @param modName
	 */
	public void setModName(String modName) {
		this.modName = modName;
	}
	/**
	 * メソッド名 : recurrenceStrのGetterメソッド
	 * 機能概要 : recurrenceStrを取得する。
	 * @return recurrenceStr
	 */
	public String getRecurrenceStr() {
		return recurrenceStr;
	}
	/**
	 * メソッド名 : recurrenceStrのSetterメソッド
	 * 機能概要 : recurrenceStrをセットする。
	 * @param recurrenceStr
	 */
	public void setRecurrenceStr(String recurrenceStr) {
		this.recurrenceStr = recurrenceStr;
	}

}
