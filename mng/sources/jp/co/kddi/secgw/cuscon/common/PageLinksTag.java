/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CusconAuthenticationController.java
 *
 * [変更履歴]
 * 日付        更新者       内容
 * 2010/03/04  m.narikawa@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common;

import java.io.IOException;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.TagSupport;

import jp.terasoluna.fw.util.PropertyUtil;
import jp.terasoluna.fw.web.struts.form.ActionFormUtil;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;
import org.apache.struts.taglib.TagUtils;

/**
 * クラス名 : PageLinksTag
 * 機能概要 : カスタムタグPageLinksの実装
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/09/24
 *          新規作成
 */
public class PageLinksTag extends TagSupport {

	@SuppressWarnings("unchecked")
	public PageLinksTag() {
		id = null;
		action = null;
		name = null;
		rowProperty = null;
		indexProperty = null;
		totalProperty = null;
		scope = null;
		submit = false;
		forward = false;
		event = DEFAULT_EVENT;
		resetIndex = false;
		currentPageIndex = CURRENT_PAGE_INDEX;
		totalPageCount = TOTAL_PAGE_COUNT;
		links = new HashMap();
		maxLinkNo = 1;
		maxPageCount = 10;
		clickEvent = "";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRowProperty() {
		return rowProperty;
	}

	public void setRowProperty(String rowProperty) {
		this.rowProperty = rowProperty;
	}

	public String getIndexProperty() {
		return indexProperty;
	}

	public void setIndexProperty(String indexProperty) {
		this.indexProperty = indexProperty;
	}

	public String getTotalProperty() {
		return totalProperty;
	}

	public void setTotalProperty(String totalProperty) {
		this.totalProperty = totalProperty;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public boolean getSubmit() {
		return submit;
	}

	public void setSubmit(boolean submit) {
		this.submit = submit;
	}

	public boolean getForward() {
		return forward;
	}

	public void setForward(boolean forward) {
		this.forward = forward;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public boolean getResetIndex() {
		return resetIndex;
	}

	public void setResetIndex(boolean resetIndex) {
		this.resetIndex = resetIndex;
	}

	public String getCurrentPageIndex() {
		return currentPageIndex;
	}

	public void setCurrentPageIndex(String currentPageIndex) {
		this.currentPageIndex = currentPageIndex;
	}

	public String getTotalPageCount() {
		return totalPageCount;
	}

	public void setTotalPageCount(String totalPageCount) {
		this.totalPageCount = totalPageCount;
	}

	public String getClickEvent() {
		return clickEvent;
	}

	public void setClickEvent(String clickEvent) {
		this.clickEvent = clickEvent;
	}

	public int doStartTag() throws JspException {
		if (!submit && (action == null || "".equals(action))) {
			log.error(ERROR_MESSAGE_ACTION_REQUIRED);
			throw new JspException(ERROR_MESSAGE_ACTION_REQUIRED);
		}
		getLinkProperty();
		Object objRow = lookup(pageContext, name, rowProperty, scope);
		int row = getInt(objRow);
		if (row <= 0) {
			if(log.isWarnEnabled()) log.warn(WARN_MESSAGE_ILLEGAL_ROW);
			return 1;
		}
		Object objIndex = lookup(pageContext, name, indexProperty, scope);
		int startIndex = getInt(objIndex);
		Object objTotal = lookup(pageContext, name, totalProperty, scope);
		int totalCount = getInt(objTotal);
		StringBuilder sb = new StringBuilder();
		attributePageCount(getPageIndex(row, startIndex), getPageCount(row, totalCount));
		if (submit) {
			defineHtml(row, startIndex, totalCount);
			addPrevSubmit(sb, row, startIndex, totalCount);
			addDirectSubmit(sb, row, startIndex, totalCount);
			addNextSubmit(sb, row, startIndex, totalCount);
		} else {
			addPrevLink(sb, row, startIndex, totalCount);
			addDirectLink(sb, row, startIndex, totalCount);
			addNextLink(sb, row, startIndex, totalCount);
		}
		if (id == null || "".equals(id))
			try {
				JspWriter writer = pageContext.getOut();
				writer.println(sb.toString());
			} catch (IOException e) {
				log.error(e.getMessage());
				throw new JspTagException(e.toString());
			}
		else
			pageContext.setAttribute(id, sb.toString());
		return 1;
	}

	protected void defineHtml(int row, int startIndex, int totalCount) throws JspException {
		JspWriter writer = pageContext.getOut();
		try {
			if (!getPageContextFlg(pageContext, (new StringBuilder()).append(PAGELINKS_JAVASCRIPT_KEY).append(rowProperty).toString())) {
				writer.println((new StringBuilder()).append("<input type=\"hidden\" name=\"").append(rowProperty).append("\" value=\"").append(row).append("\"/>").toString());
				setPageContextFlg(pageContext, (new StringBuilder()).append(PAGELINKS_JAVASCRIPT_KEY).append(rowProperty).toString());
			}
			if (!getPageContextFlg(pageContext, (new StringBuilder()).append(PAGELINKS_JAVASCRIPT_KEY).append(indexProperty).toString())) {
				writer.println((new StringBuilder()).append("<input type=\"hidden\" name=\"").append(indexProperty).append("\" value=\"").append(startIndex).append("\"/>").toString());
				setPageContextFlg(pageContext, (new StringBuilder()).append(PAGELINKS_JAVASCRIPT_KEY).append(indexProperty).toString());
			}
			if (!getPageContextFlg(pageContext, (new StringBuilder()).append(PAGELINKS_JAVASCRIPT_KEY).append(event).toString()) && forward) {
				writer.println((new StringBuilder()).append("<input type=\"hidden\" name=\"").append(event).append("\" value=\"\"/>").toString());
				setPageContextFlg(pageContext, (new StringBuilder()).append(PAGELINKS_JAVASCRIPT_KEY).append(event).toString());
			}
			if (!getPageContextFlg(pageContext, (new StringBuilder()).append(PAGELINKS_JAVASCRIPT_KEY).append("resetIndex").toString()) && resetIndex) {
				if (!"startIndex".equals(indexProperty))
					writer.println((new StringBuilder()).append("<input type=\"hidden\" name=\"startIndex\" value=\"").append(startIndex).append("\"/>").toString());
				int endIndex = (startIndex + row) - 1;
				if (endIndex > totalCount)
					writer.println((new StringBuilder()).append("<input type=\"hidden\" name=\"endIndex\" value=\"").append(totalCount - 1).append("\"/>").toString());
				else
					writer.println((new StringBuilder()).append("<input type=\"hidden\" name=\"endIndex\" value=\"").append(endIndex).append("\"/>").toString());
				setPageContextFlg(pageContext, (new StringBuilder()).append(PAGELINKS_JAVASCRIPT_KEY).append("resetIndex").toString());
			}
			String formName = ActionFormUtil.getActionFormName((HttpServletRequest) pageContext.getRequest());
			if (!getPageContextFlg(pageContext, PAGELINKS_JAVASCRIPT_KEY)) {
				writer.println("<script type=\"text/javascript\">");
				writer.println("<!--");
				writer.println("  function pageLinkSubmit(rowObj, indexObj, row, startIndex){");
				writer.println("    rowObj.value = row;");
				writer.println("    indexObj.value = startIndex;");
				if (forward) {
					writer.print("    document.");
					writer.print(formName);
					writer.print(".");
					writer.print(event);
					writer.print(".value = \"");
					writer.print(FORWARD_NAME);
					writer.println("\";");
				}
				writer.print("    document.");
				writer.print(formName);
				writer.println(".submit();");
				writer.println("  }");
				writer.println("// -->");
				writer.println("</script>");
				setPageContextFlg(pageContext, PAGELINKS_JAVASCRIPT_KEY);
			}
		} catch (IOException e) {
			log.error(e.getMessage());
			throw new JspTagException(e.toString());
		}
	}

	protected void addPrevSubmit(StringBuilder sb, int row, int startIndex, int totalCount) {
		String formName = ActionFormUtil.getActionFormName((HttpServletRequest) pageContext.getRequest());
		for (int i = maxLinkNo; i > 0; i--) {
			String linkKey = (new StringBuilder()).append(PREV_LINKS).append(i).append(CHAR_LINKS).toString();
			String linkValue = (String) links.get(linkKey);
			if (linkValue == null || "".equals(linkValue))
				continue;
			int index = startIndex - i * row;
			if (index < 0) {
				sb.append((new StringBuilder()).append(linkValue).append("&nbsp;").toString());
			} else {
				sb.append("<a href=\"#\" onclick=\"");
				if (clickEvent.length() > 0) {
					sb.append(clickEvent);
					sb.append("; ");
				}
				sb.append("pageLinkSubmit(");
				sb.append("document.");
				sb.append(formName);
				sb.append(".");
				sb.append(rowProperty);
				sb.append(",");
				sb.append("document.");
				sb.append(formName);
				sb.append(".");
				sb.append(indexProperty);
				sb.append(",");
				sb.append(row);
				sb.append(",");
				sb.append(index);
				sb.append(")\">");
				sb.append(linkValue);
				sb.append("</a>&nbsp;");
			}
		}

	}

	protected void addDirectSubmit(StringBuilder sb, int row, int startIndex, int totalCount) {
		String formName = ActionFormUtil.getActionFormName((HttpServletRequest) pageContext.getRequest());
		String directLinkNo = (String) links.get(MAX_DSP_SIZE);
		if (directLinkNo != null)
			try {
				maxPageCount = Integer.parseInt(directLinkNo);
			} catch (NumberFormatException e) {
			}
		int pageCount = getPageCount(row, totalCount);
		int pageIndex = getPageIndex(row, startIndex);
		int startPage = 0;
		int endPage = 0;
		if (pageCount > maxPageCount && pageIndex > maxPageCount / 2) {
			endPage = maxPageCount;
			startPage = pageIndex - endPage / 2 - 1;
			if (startPage + endPage > pageCount)
				startPage = pageCount - endPage;
		} else {
			endPage = pageCount >= maxPageCount ? maxPageCount : pageCount;
			startPage = 0;
		}
		int size = startPage + endPage;
		for (int i = startPage; i < size; i++) {
			int idx = i + 1;
			if (pageIndex == idx) {
				sb.append("<b>");
				sb.append(idx);
				sb.append("</b>&nbsp;");
			} else {
				sb.append("<a href=\"#\" onclick=\"");
				if (clickEvent.length() > 0) {
					sb.append(clickEvent);
					sb.append("; ");
				}
				sb.append("pageLinkSubmit(");
				sb.append("document.");
				sb.append(formName);
				sb.append(".");
				sb.append(rowProperty);
				sb.append(",");
				sb.append("document.");
				sb.append(formName);
				sb.append(".");
				sb.append(indexProperty);
				sb.append(",");
				sb.append(row);
				sb.append(",");
				sb.append(i * row);
				sb.append(")\">");
				sb.append(idx);
				sb.append("</a>&nbsp;");
			}
		}

	}

	protected void addNextSubmit(StringBuilder sb, int row, int startIndex, int totalCount) {
		String formName = ActionFormUtil.getActionFormName((HttpServletRequest) pageContext.getRequest());
		for (int i = 1; i <= maxLinkNo; i++) {
			String linkKey = (new StringBuilder()).append(NEXT_LINKS).append(i).append(CHAR_LINKS).toString();
			String linkValue = (String) links.get(linkKey);
			if (linkValue == null || "".equals(linkValue))
				continue;
			int index = startIndex + i * row;
			if (index > totalCount - 1) {
				sb.append((new StringBuilder()).append(linkValue).append("&nbsp;").toString());
			} else {
				sb.append("<a href=\"#\" onclick=\"");
				if (clickEvent.length() > 0) {
					sb.append(clickEvent);
					sb.append("; ");
				}
				sb.append("pageLinkSubmit(");
				sb.append("document.");
				sb.append(formName);
				sb.append(".");
				sb.append(rowProperty);
				sb.append(",");
				sb.append("document.");
				sb.append(formName);
				sb.append(".");
				sb.append(indexProperty);
				sb.append(",");
				sb.append(row);
				sb.append(",");
				sb.append(index);
				sb.append(")\">");
				sb.append(linkValue);
				sb.append("</a>&nbsp;");
			}
		}

	}

	protected void addPrevLink(StringBuilder sb, int row, int startIndex, int totalCount) {
		TagUtils tagUtils = TagUtils.getInstance();
		HttpServletResponse response = (HttpServletResponse) pageContext.getResponse();
		String url = null;
		url = response.encodeURL(tagUtils.getActionMappingURL(action, pageContext));
		for (int i = maxLinkNo; i > 0; i--) {
			String linkKey = (new StringBuilder()).append(PREV_LINKS).append(i).append(CHAR_LINKS).toString();
			String linkValue = (String) links.get(linkKey);
			if (linkValue == null || "".equals(linkValue))
				continue;
			int index = startIndex - i * row;
			if (index < 0) {
				sb.append((new StringBuilder()).append(linkValue).append("&nbsp;").toString());
				continue;
			}
			sb.append((new StringBuilder()).append("<a href=\"").append(url).toString());
			if (url.indexOf("?") < 0)
				sb.append("?");
			else
				sb.append("&");
			sb.append(rowProperty);
			sb.append("=");
			sb.append(row);
			sb.append("&");
			sb.append(indexProperty);
			sb.append("=");
			sb.append(index);
			sb.append("\" ");
			if (clickEvent.length() > 0) {
				sb.append("onclick=\"");
				sb.append(clickEvent);
				sb.append("\"");
			}
			sb.append(">");
			sb.append(linkValue);
			sb.append("</a>&nbsp;");
		}

	}

	protected void addDirectLink(StringBuilder sb, int row, int startIndex, int totalCount) {
		TagUtils tagUtils = TagUtils.getInstance();
		HttpServletResponse response = (HttpServletResponse) pageContext.getResponse();
		String url = null;
		url = response.encodeURL(tagUtils.getActionMappingURL(action, pageContext));
		String directLinkNo = (String) links.get(MAX_DSP_SIZE);
		if (directLinkNo != null)
			try {
				maxPageCount = Integer.parseInt(directLinkNo);
			} catch (NumberFormatException e) {
			}
		int pageCount = getPageCount(row, totalCount);
		int pageIndex = getPageIndex(row, startIndex);
		int startPage = 0;
		int endPage = 0;
		if (pageCount > maxPageCount && pageIndex > maxPageCount / 2) {
			endPage = maxPageCount;
			startPage = pageIndex - endPage / 2 - 1;
			if (startPage + endPage > pageCount)
				startPage = pageCount - endPage;
		} else {
			endPage = pageCount >= maxPageCount ? maxPageCount : pageCount;
			startPage = 0;
		}
		int size = startPage + endPage;
		for (int i = startPage; i < size; i++) {
			int idx = i + 1;
			if (pageIndex == idx) {
				sb.append("<b>");
				sb.append(idx);
				sb.append("</b>&nbsp;");
				continue;
			}
			sb.append((new StringBuilder()).append("<a href=\"").append(url).toString());
			if (url.indexOf("?") < 0)
				sb.append("?");
			else
				sb.append("&");
			sb.append(rowProperty);
			sb.append("=");
			sb.append(row);
			sb.append("&");
			sb.append(indexProperty);
			sb.append("=");
			sb.append(i * row);
			sb.append("\" ");
			if (clickEvent.length() > 0) {
				sb.append("onclick=\"");
				sb.append(clickEvent);
				sb.append("\"");
			}
			sb.append(">");
			sb.append(idx);
			sb.append("</a>&nbsp;");
		}

	}

	protected void addNextLink(StringBuilder sb, int row, int startIndex, int totalCount) {
		TagUtils tagUtils = TagUtils.getInstance();
		HttpServletResponse response = (HttpServletResponse) pageContext.getResponse();
		String url = null;
		url = response.encodeURL(tagUtils.getActionMappingURL(action, pageContext));
		for (int i = 1; i <= maxLinkNo; i++) {
			String linkKey = (new StringBuilder()).append(NEXT_LINKS).append(i).append(CHAR_LINKS).toString();
			String linkValue = (String) links.get(linkKey);
			if (linkValue == null || "".equals(linkValue))
				continue;
			int index = startIndex + i * row;
			if (index > totalCount - 1) {
				sb.append((new StringBuilder()).append(linkValue).append("&nbsp;").toString());
				continue;
			}
			sb.append((new StringBuilder()).append("<a href=\"").append(url).toString());
			if (url.indexOf("?") < 0)
				sb.append("?");
			else
				sb.append("&");
			sb.append(rowProperty);
			sb.append("=");
			sb.append(row);
			sb.append("&");
			sb.append(indexProperty);
			sb.append("=");
			sb.append(index);
			sb.append("\" ");
			if (clickEvent.length() > 0) {
				sb.append("onclick=\"");
				sb.append(clickEvent);
				sb.append("\"");
			}
			sb.append(">");
			sb.append(linkValue);
			sb.append("</a>&nbsp;");
		}

	}

	@SuppressWarnings("unchecked")
	private void getLinkProperty() {
		Enumeration enume = PropertyUtil.getPropertyNames(PAGE_LINKS_PREFIX);
		do {
			if (!enume.hasMoreElements())
				break;
			String propName = (String) enume.nextElement();
			String id = propName.substring(PAGE_LINKS_PREFIX.length());
			String link = PropertyUtil.getProperty(propName);
			if (id != null && (id.startsWith(PREV_LINKS) || id.startsWith(NEXT_LINKS))) {
				String strLinkNo = id.substring(4, id.lastIndexOf(CHAR_LINKS));
				int intLinkNo = 0;
				try {
					intLinkNo = Integer.parseInt(strLinkNo);
				} catch (NumberFormatException e) {
					continue;
				}
				if (intLinkNo > maxLinkNo)
					maxLinkNo = intLinkNo;
				links.put(id, link);
			} else if (MAX_DSP_SIZE.equals(id))
				links.put(id, link);
		} while (true);
	}

	protected int getPageIndex(int row, int startIndex) {
		int pageIndex = 0;
		if (row > 0)
			pageIndex = startIndex / row + 1;
		else
			pageIndex = 0;
		if (row > 0 && startIndex % row > 0)
			pageIndex++;
		return pageIndex;
	}

	protected int getPageCount(int row, int totalCount) {
		int pageCount = 0;
		if (row > 0) {
			pageCount = totalCount / row;
			if (totalCount % row > 0)
				pageCount++;
		} else {
			pageCount = 1;
		}
		return pageCount;
	}

	protected boolean getPageContextFlg(PageContext pageContext, String key) {
		Object obj = pageContext.getAttribute(key);
		Boolean bol = new Boolean(false);
		if (obj != null && (obj instanceof Boolean))
			bol = (Boolean) obj;
		return bol.booleanValue();
	}

	protected void setPageContextFlg(PageContext pageContext, String key) {
		pageContext.setAttribute(key, Boolean.valueOf(true));
	}

	protected Object lookup(PageContext pageContext, String name, String property, String scope) throws JspException {
		if (property == null || "".equals(property))
			return null;
		Object retObj = null;
		if (name != null && !"".equals(name))
			retObj = TagUtils.getInstance().lookup(pageContext, name, property, scope);
		else
			retObj = TagUtils.getInstance().lookup(pageContext, property, scope);
		return retObj;
	}

	protected int getInt(Object obj) throws JspException {
		int retInt = 0;
		String value = ObjectUtils.toString(obj);
		if (!"".equals(value))
			try {
				retInt = Integer.parseInt(value);
			} catch (NumberFormatException e) {
				log.error(e.getMessage());
				throw new JspException(e);
			}
		return retInt;
	}

	protected void attributePageCount(int now, int total) {
		if (total <= 0)
			now = 0;
		if (currentPageIndex != null && !"".equals(currentPageIndex))
			pageContext.setAttribute(currentPageIndex, Integer.valueOf(now));
		else
			pageContext.setAttribute(CURRENT_PAGE_INDEX, Integer.valueOf(now));
		if (totalPageCount != null && !"".equals(totalPageCount))
			pageContext.setAttribute(totalPageCount, Integer.valueOf(total));
		else
			pageContext.setAttribute(TOTAL_PAGE_COUNT, Integer.valueOf(total));
	}

	public void release() {
		super.release();
		id = null;
		action = null;
		name = null;
		rowProperty = null;
		indexProperty = null;
		totalProperty = null;
		scope = null;
		submit = false;
		forward = false;
		event = DEFAULT_EVENT;
		resetIndex = false;
		currentPageIndex = CURRENT_PAGE_INDEX;
		totalPageCount = TOTAL_PAGE_COUNT;
	}

	private static final long serialVersionUID = 8412770050909647645L;
	private static Log log = LogFactory.getLog(PageLinksTag.class);
	protected String id;
	protected String action;
	protected String name;
	protected String rowProperty;
	protected String indexProperty;
	protected String totalProperty;
	protected String scope;
	protected boolean submit;
	protected boolean forward;
	protected String event;
	protected boolean resetIndex;
	protected String currentPageIndex;
	protected String totalPageCount;
	protected static String ERROR_MESSAGE_ACTION_REQUIRED = "Action attribute is required when submit attribute is \"false\".";
	protected static String WARN_MESSAGE_ILLEGAL_ROW = "Row param is illegal.";
	protected static String PAGE_LINKS_PREFIX = "pageLinks.";
	protected static String PREV_LINKS = "prev";
	protected static String NEXT_LINKS = "next";
	protected static String CHAR_LINKS = ".char";
	protected static String MAX_DSP_SIZE = "maxDirectLinkCount";
	protected static String PAGELINKS_JAVASCRIPT_KEY = "pageLinksJavaScriptKey";
	protected static String FORWARD_NAME = "forward_pageLinks";
	protected static String DEFAULT_EVENT = "event";
	protected static String TOTAL_PAGE_COUNT = "totalPageCount";
	protected static String CURRENT_PAGE_INDEX = "currentPageIndex";
	@SuppressWarnings("unchecked")
	protected Map links;
	protected int maxLinkNo;
	protected int maxPageCount;
	protected String clickEvent = "";

}

/*
 * DECOMPILATION REPORT
 * Decompiled from: C:\Program
 * Files\workspace\kddi-secgw-cuscon\webapps\WEB-INF\
 * lib\terasoluna-thin-server.jar
 * Total time: 109 ms
 * Jad reported messages/errors:
 * Exit status: 0
 * Caught exceptions:
 */