/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ApplicationGroups.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/17     komakiys         初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common.vo;

import java.io.Serializable;

/**
 * クラス名 : ApplicationGroups
 * 機能概要 : アプリケーショングループオブジェクトデータクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/17
 *          新規作成
 * @see
 */
public class ApplicationGroups implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 6519273278096558483L;
	// シーケンス番号
	private int seqNo = 0;
	// Vsys-ID
	private String vsysId = null;
	// オブジェクト名
	private String name = null;
	// 世代番号
	private int generationNo = 0;
	// 変更フラグ
	private int modFlg = 0;

	/**
	 * メソッド名 : seqNoのGetterメソッド
	 * 機能概要 : seqNoを取得する。
	 * @return seqNo
	 */
	public int getSeqNo() {
		return seqNo;
	}
	/**
	 * メソッド名 : seqNoのSetterメソッド
	 * 機能概要 : seqNoをセットする。
	 * @param seqNo
	 */
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得する。
	 * @return vsysId
	 */
	public String getVsysId() {
		return vsysId;
	}
	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdをセットする。
	 * @param vsysId
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : generationNoのGetterメソッド
	 * 機能概要 : generationNoを取得する。
	 * @return generationNo
	 */
	public int getGenerationNo() {
		return generationNo;
	}
	/**
	 * メソッド名 : generationNoのSetterメソッド
	 * 機能概要 : generationNoをセットする。
	 * @param generationNo
	 */
	public void setGenerationNo(int generationNo) {
		this.generationNo = generationNo;
	}
	/**
	 * メソッド名 : modFlgのGetterメソッド
	 * 機能概要 : modFlgを取得する。
	 * @return modFlg
	 */
	public int getModFlg() {
		return modFlg;
	}
	/**
	 * メソッド名 : modFlgのSetterメソッド
	 * 機能概要 : modFlgをセットする。
	 * @param modFlg
	 */
	public void setModFlg(int modFlg) {
		this.modFlg = modFlg;
	}
}
