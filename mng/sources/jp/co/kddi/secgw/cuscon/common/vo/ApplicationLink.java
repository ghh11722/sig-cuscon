/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ApplicationLink.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/18     komakiys         初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common.vo;

import java.io.Serializable;

/**
 * クラス名 : ApplicationLink
 * 機能概要 : アプリケーションリンクデータクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/18
 *          新規作成
 * @see
 */
public class ApplicationLink implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = -2249937472887068566L;

	// セキュリティポリシー一覧のSeqNoとのリンク
	private int secRlsSecNo = 0;
	// オブジェクトタイプ
	private int objectType = 0;
	// アプリケーションオブジェクトのSeqNoとのリンク
	private int linkSeqNo = 0;

	/**
	 * メソッド名 : secRlsSecNoのGetterメソッド
	 * 機能概要 : secRlsSecNoを取得する。
	 * @return secRlsSecNo
	 */
	public int getSecRlsSecNo() {
		return secRlsSecNo;
	}
	/**
	 * メソッド名 : secRlsSecNoのSetterメソッド
	 * 機能概要 : secRlsSecNoをセットする。
	 * @param secRlsSecNo
	 */
	public void setSecRlsSecNo(int secRlsSecNo) {
		this.secRlsSecNo = secRlsSecNo;
	}
	/**
	 * メソッド名 : objectTypeのGetterメソッド
	 * 機能概要 : objectTypeを取得する。
	 * @return objectType
	 */
	public int getObjectType() {
		return objectType;
	}
	/**
	 * メソッド名 : objectTypeのSetterメソッド
	 * 機能概要 : objectTypeをセットする。
	 * @param objectType
	 */
	public void setObjectType(int objectType) {
		this.objectType = objectType;
	}
	/**
	 * メソッド名 : linkSeqNoのGetterメソッド
	 * 機能概要 : linkSeqNoを取得する。
	 * @return linkSeqNo
	 */
	public int getLinkSeqNo() {
		return linkSeqNo;
	}
	/**
	 * メソッド名 : linkSeqNoのSetterメソッド
	 * 機能概要 : linkSeqNoをセットする。
	 * @param linkSeqNo
	 */
	public void setLinkSeqNo(int linkSeqNo) {
		this.linkSeqNo = linkSeqNo;
	}
}
