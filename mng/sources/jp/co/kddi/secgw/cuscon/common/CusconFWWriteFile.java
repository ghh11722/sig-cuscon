/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CusconFWWriteFile.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2012/08/27     kkato@PROSITE           初版作成
 * 2012/12/01     kkato@PROSITE           PANOS4.1.x対応
 * 2016/12/21     T.Yamazaki@Plum Systems PANOS7対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
//import java.util.Map.Entry;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.FilterInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.LinkInfo;
import jp.co.kddi.secgw.cuscon.common.vo.*;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;
import jp.terasoluna.fw.util.PropertyUtil;

import java.io.FileWriter;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectObjectList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectUpdatePA;

// for debug
//import java.text.*;
//import java.util.Date;

/**
 * クラス名 : CusconFWWriteFile
 * 機能概要 : FW設定情報ファイル出力を行う。
 * 備考 :
 * @author kkato@PROSITE
 * @version 1.0 kkato@PROSITE
 *          Created 2012/08/27
 *          新規作成
 *          2.0 T.Yamazaki@Plum Systems Inc.
 *          Updated 2016/12/21
 *          PANOS7対応
 * @see
 */
public class CusconFWWriteFile {

	// URL結合文字
	private static final String AND = "&";
	// URL使用文字
	private static final String PAR = "]";
	// URL使用文字
	private static final String SLASH = "/";
	// 文字列分活用
//	private static final String PHY = "-";
//	private static final String AT = "@";
	// ダブルクォーテーション(コマンド付加用)
    private static String QUOT ="\"";
	// 改行
//    private static String CRLF ="\r\n";

     // メッセージクラス
     private static MessageAccessor messageAccessor = null;

     private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.common.CusconFWDelete");

     // カンマ
     private static String COMMA =",";

 	// コマンドタイムアウト時間
// 	private int timeout = 0;
 	// 改行(コマンド付加用)
 	private String CHANGE_LINE ="\n";
// 	// ダブルクォーテーション(コマンド付加用)
// 	private String QUOT ="\"";
 	// コンフィグモードコマンド
 	private static final String CONFIG_COM =
 		PropertyUtil.getProperty("fwsetting.common.configure");
 	// コマンドエラー表示
 	private static final String COM_ERROR =
 		PropertyUtil.getProperty("fwsetting.common.commanderror");
 	// 設定/削除コマンドエラー確認(正規表現)
 	private static final String CHECK_COM =
 		PropertyUtil.getProperty("fwsetting.common.checkset");
 	// 設定/削除コマンドエラー確認箇所
// 	private static final int CHECK_NUM =Integer.parseInt(
// 				PropertyUtil.getProperty("fwsetting.common.checksetnum"));
 	// ポリシー/オブジェクト削除コマンド
// 	private static final String DEL_COM =
// 		PropertyUtil.getProperty("fwsetting.common.del");
 	// ポリシー/オブジェクト設定コマンド
 	private static final String SET_COM =
 		PropertyUtil.getProperty("fwsetting.common.set");
 	// 日時フォーマット
// 	private static final String DATE_FORMAT =
// 		PropertyUtil.getProperty("fwsetting.common.dateformat");
 	// コミット応答確認(正規表現)
// 	private static final String COMMIT_CHECK =
// 		PropertyUtil.getProperty("fwsetting.common.commitcheck");
 	// コミットコマンドエラー確認対象場所
// 	private static final int COMMIT_CHECKNUM = Integer.parseInt(
// 		PropertyUtil.getProperty("fwsetting.common.commitchecknum"));
 	// コマンド待ち時間
// 	private static int WAIT_COMMAND = 0;
 	// コミット成功メッセージ
// 	private static final String COMMIT_SUCCESS =
// 		PropertyUtil.getProperty("fwsetting.common.commitsuccess");
 	// deleteコマンド実行数
// 	private static final int DEL_COM_NUM =Integer.parseInt(
// 				PropertyUtil.getProperty("fwsetting.common.commitDelNum"));
 	// AllowLogForwardingProfile名
 	private static final String ALLOW_LOG_FORWARD_PROFILE =
 		PropertyUtil.getProperty("fwsetting.common.allowlogprofile");
 	// DenyLogForwardingProfile名
 	private static final String DENY_LOG_FORWARD_PROFILE =
 		PropertyUtil.getProperty("fwsetting.common.denylogprofile");

 	// アドレスコマンド
 	private static final String ADDRESS = " address ";
 	// アドレスグループコマンド
 	private static final String ADDRESS_GRP = " address-group ";
 	// アプリケーションフィルタコマンド
 	private static final String APPLI_FIL = " application-filter ";
 	// アプリケーショングループコマンド
 	private static final String APPLI_GRP = " application-group ";
 	// サービスコマンド
 	private static final String SERVICE = " service ";
 	// サービスグループコマンド
 	private static final String SERVICE_GRP = " service-group ";
 	// スケジュールコマンド
// 	private static final String SCHEDULE = " schedule ";
 	// コマンド用ブランク
 	private static final String SP = " ";
 	private static final String DESCRIPTION = " description ";

// 	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.commitproc.common.UpdatePA");
// 	// メッセージクラス
// 	private static MessageAccessor messageAccessor = null;

 	// 脆弱性の保護フラグ
 	private static final String DEFAULT = "default";

	// Webフィルタコマンド
 	private static final String URLFILTER = " profiles url-filtering ";
 	private static final String URLFILTER_ACTION_ALERT = "alert";
 	private static final String URLFILTER_ACTION_ALLOW = "allow";
 	private static final String URLFILTER_ACTION_BLOCK = "block";
 	private static final String URLFILTER_ACTION_CONTINUE = "continue";
 	private static final String URLFILTER_ACTION_OVERRIDE = "override";
// 	private static final String SPACE = " ";
 	private static final String URLFILTER_ACTION_ON_LiCENSE_EXPIRATION = " license-expired ";
 	private static final String URLFILTER_DYNAMIC_URL = " dynamic-url ";
// 20121201 kkato@PROSITE add start
    // XML解析用
    private static final String PORT = "port";
    private static final String PROTOCOL = "protocol";
// 20121201 kkato@PROSITE afd end

// 	// コマンドinvalid
// 	private static final String COMMAND_INVALID =
// 		PropertyUtil.getProperty("fwsetting.common.command_invalid");
// 	// コマンドunknown
// 	private static final String COMMAND_UNKNOWN =
// 		PropertyUtil.getProperty("fwsetting.common.command_unknown");
// 	// コマンドserver error
// 	private static final String COMMAND_SERVER_ERROR =
// 		PropertyUtil.getProperty("fwsetting.common.command_server_error");
// 	// running-configのロードコマンド
// 	private static final String LOAD_RUNNING_CONFIG =
// 		PropertyUtil.getProperty("fwsetting.common.running_config");
// 	// running-configのロード成功メッセージ
// 	private static final String LOAD_RUNNING_CONFIG_SUCCESS =
// 		PropertyUtil.getProperty("fwsetting.common.running_configsuccess");

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 */
	public CusconFWWriteFile() {
		log.debug("CusconFWWriteFileコンストラクタ2処理開始");
		log.debug("CusconFWWriteFileコンストラクタ2処理終了");
	}

	/**
	 * メソッド名 : setFWInfo
	 * 機能概要 : 指定VsysIDについてFW設定情報DB登録<br>
	 *            (Palo設定情報取得/0世代の削除登録)を行う。
	 * @param updateDAO
	 * @param queryDAO
	 * @param uvo
	 * @throws Exception
	 */
	public void setFWInfo(UpdateDAO updateDAO,
						QueryDAO queryDAO, CusconUVO uvo, String tmpFile) throws Exception {
		log.debug("setFWInfo処理開始:vsysId = " + uvo.getVsysId());

		// XML-API実行クラス
		CusconXmlApiExecute xmlExe = null;
	    // XML-API解析クラス
		CusconXMLAnalyze xmlAna = null;
		// CusconFWDeleteクラス
//		CusconFWDelete fwdel = null;
		// 結果を格納するリスト
		SelectUpdatePA objList = new SelectUpdatePA();

		// 要求種別
		String TYPE =
	    	PropertyUtil.getProperty("common.xmlgettype");
		// URL(ホスト名前)
		String URL1 =
	    	PropertyUtil.getProperty("common.xmlurl1");
		// URL(ホスト名後)
		String URL2 =
	    	PropertyUtil.getProperty("common.xmlurl2");
		// アクション
		String ACTION =
	    	PropertyUtil.getProperty("common.xmlaction");
		// アクセスキ－設定
		String KEY =
	    	PropertyUtil.getProperty("common.xmlkey");
		// パス
		String XPATH =
	    	PropertyUtil.getProperty("common.xmlxpath");

		if(log.isDebugEnabled()) {
			  log.debug("----------------------------------------------------");
		      log.debug("要求種別:" + TYPE);
		      log.debug("URL(ホスト名前):" + URL1);
		      log.debug("URL(ホスト名後):" + URL2);
		      log.debug("アクション:" + ACTION);
		      log.debug("アクセスキ－設定:" + KEY);
		      log.debug("パス:" + XPATH);
		      log.debug("----------------------------------------------------");
		}

		// アクセスキー
		String accessKey;
		// XML-APIのURL生成用
        String xmlApiConnect;

        // XML-API実行クラスを生成する。
		xmlExe = new CusconXmlApiExecute();
	    // XML-API解析クラスを生成する。
		xmlAna = new CusconXMLAnalyze(messageAccessor);
		// CusconFWDeleteクラスを生成する。
//		fwdel = new CusconFWDelete();

//		// DBの0世代情報を削除する。
//		fwdel.deleteFWInfo(CusconConst.ZERO_GENE, updateDAO, queryDAO, uvo);

		CusconAccessKeyCreate access = new CusconAccessKeyCreate();

		// アクセスキーの生成を行う。
		accessKey = access.getAccessKey(null, null, queryDAO, uvo);

		// アクセスキー取得失敗
		if(accessKey == null) {
			// アクセスキー取得失敗ログ出力
			log.error(messageAccessor.getMessage("EZ001001", null, uvo));
//			// トランザクションロールバック
//			TransactionUtil.setRollbackOnly();

			throw new Exception();
		}

		// XML-API FW要求生成URL作成
		xmlApiConnect = URL1 + access.getHost() + URL2  +
				TYPE + AND + ACTION + AND + KEY + accessKey + AND +
					XPATH + QUOT +  uvo.getVsysId() + QUOT + PAR;

		log.debug("* XML API FW要求生成URL : " + xmlApiConnect);

		// アドレスオブジェクトの取得＆及びDB登録
		setAddresses(xmlExe, xmlAna, updateDAO, uvo, xmlApiConnect, objList);

		// アドレスグループオブジェクトの取得＆及びDB登録
		setAddressGroups(xmlExe, xmlAna, updateDAO, uvo, xmlApiConnect, objList);

		// アプリケーションフィルタオブジェクトの取得＆及びDB登録
		setApplicationFilter(xmlExe, xmlAna, updateDAO, uvo, xmlApiConnect, objList);

		// アプリケーショングループオブジェクトの取得＆及びDB登録
		setApplicationGroups(xmlExe, xmlAna, updateDAO, uvo, xmlApiConnect, objList);

		// サービスオブジェクトの取得＆及びDB登録
		setServices(xmlExe, xmlAna, updateDAO, uvo, xmlApiConnect, objList);

		// サービスグループオブジェクトの取得＆及びDB登録
		setServiceGroups(xmlExe, xmlAna, updateDAO, uvo, xmlApiConnect, objList);

//		// スケジュールオブジェクトの取得＆及びDB登録
//		setSchedules(xmlExe, xmlAna, updateDAO, uvo, xmlApiConnect, objList);

		// Webフィルタオブジェクトの取得＆及びDB登録
		setUrlFilters(xmlExe, xmlAna,updateDAO, queryDAO, uvo, xmlApiConnect, objList);

    	// 0世代のポリシー一覧情報を取得する。
//       PolicyList policy = new PolicyList();
        List<SelectPolicyList> policyList = new ArrayList<SelectPolicyList>();

		// ポリシーの取得＆及びDB登録
		setPolicyInfo(xmlExe, xmlAna, updateDAO, queryDAO, uvo, xmlApiConnect, policyList);

		boolean rst = registPA(objList, policyList, tmpFile, uvo);

		log.debug("setFWInfo処理終了");

	}

	/**
	 * メソッド名 : setAddresses
	 * 機能概要 : XML-APIによるアドレスオブジェクト取得、及びDB登録を行う。
	 * @param xmlAna
	 * @param xmlExe
	 * @param vsysId Vsys-ID
	 * @param xmlApiConnect XML-API実行URL
	 * @throws Exception
	 */
	private void setAddresses(CusconXmlApiExecute xmlExe, CusconXMLAnalyze xmlAna,
			UpdateDAO updateDAO, CusconUVO uvo, String xmlApiConnect, SelectUpdatePA objList) throws Exception {

		log.debug("setAddresses処理開始:vsysId = " + uvo.getVsysId() +
								", xmlApiConnect = " + xmlApiConnect);
		// Map
		Map.Entry obj = null;
	    // イテレータ
	    Iterator iterator = null;
		// アドレスオブジェクト取得用
        String ADDRESS = "address";
		// アドレスオブジェクト情報リスト
		List<Addresses> adrNameList = new ArrayList<Addresses>();

        // XML解析レスポンス
        List<HashMap<String, Object>> objAnalyse;
        // XML-API取得
        String objConnect;

		// 20121201 kkato@PROSITE mod start
		//objConnect = xmlApiConnect + SLASH + ADDRESS;
		// XML解析を行う
		//objAnalyse = getXmlResult(xmlExe, xmlAna, objConnect, uvo);
		objAnalyse = getXmlResult(xmlExe, xmlAna, xmlApiConnect, uvo, ADDRESS);
		// 20121201 kkato@PROSITE mod end

		if(objAnalyse != null) {
//			FileWriter filewriter = new FileWriter(tmpFile, true);
			log.debug("PAにアドレスが存在");
			// XML解析レスポンスのデータ分繰り返す。
			for(int i=0; i<objAnalyse.size(); i++) {
		        // アドレスオブジェクトデータクラス
		        Addresses address = new Addresses();
				// アドレスの基本登録情報を設定する。
				address.setVsysId(uvo.getVsysId());
				address.setModFlg(CusconConst.NONE_NUM);
				address.setGenerationNo(CusconConst.ZERO_GENE);

		        log.debug("XMLレスポンス解析:" + i);
				iterator = objAnalyse.get(i).entrySet().iterator();

				// ハッシュマップのデータ分繰り返す。
				while (iterator.hasNext()) {
					log.debug("ハッシュマップ");
					obj = (Map.Entry)iterator.next();
					// キーがnameの場合
					if(obj.getKey().equals(CusconConst.NAME)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						address.setName(obj.getValue().toString());
					// キーがname以外の場合
					} else {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						address.setType(obj.getKey().toString());
						address.setAddress(obj.getValue().toString());
					}
				}
				adrNameList.add(i, address);
			}
//    	  	filewriter.flush();
//    	  	filewriter.close();
		}
		// アドレスオブジェクト情報リストを追加する。
		objList.setAddress(adrNameList);
		log.debug("setAddresses処理終了");
	}

	/**
	 * メソッド名 : setAddressGroups
	 * 機能概要 : XML-APIによるアドレスグループオブジェクト取得、及びDB登録を行う。
	 * @param xmlExe
	 * @param xmlAna
	 * @param vsysId Vsys-ID
	 * @param xmlApiConnect XML-API実行URL
	 * @param objList
	 * @throws Exception
	 */
	private void setAddressGroups(CusconXmlApiExecute xmlExe, CusconXMLAnalyze xmlAna,
			UpdateDAO updateDAO, CusconUVO uvo, String xmlApiConnect, SelectUpdatePA objList) throws Exception {

		log.debug("setAddressGroups処理開始:vsysId = " + uvo.getVsysId() +
				", xmlApiConnect = " + xmlApiConnect);
		// Map
		Map.Entry obj = null;
	    // イテレータ
	    Iterator iterator = null;
		// アドレスグループオブジェクト取得用
        String ADDRESS_GRP = "address-group";
	    // アドレスグループオブジェクト情報リスト
		List<LinkInfo> adrGrpNameList = new ArrayList<LinkInfo>();

        // XML解析レスポンス
        List<HashMap<String, Object>> objAnalyse;
        // XML-API取得
        String objConnect;

	    // 20121201 kkato@PROSITE mod start
        //objConnect = xmlApiConnect + SLASH + ADDRESS_GRP;
		// XML解析を行う
		//objAnalyse = getXmlResult(xmlExe, xmlAna, objConnect, uvo);
		objAnalyse = getXmlResult(xmlExe, xmlAna, xmlApiConnect, uvo, ADDRESS_GRP);
	    // 20121201 kkato@PROSITE mod end

		if(objAnalyse != null) {
			log.debug("PAにアドレスグループが存在");
			try {
//				List<LinkInfo> objLinkList = new ArrayList<LinkInfo>();
				// XML解析レスポンスのデータ分繰り返す。
				for(int i=0; i<objAnalyse.size(); i++) {
					log.debug("XMLレスポンス解析:" + i);
					iterator = objAnalyse.get(i).entrySet().iterator();
			        // リンクデータクラス
			        LinkInfo objLink = new LinkInfo();
					// リンクの基本登録情報を設定する。
					objLink.setVsysId(uvo.getVsysId());
					objLink.setGenerationNo(CusconConst.ZERO_GENE);

					// ハッシュマップのデータ分繰り返す。
					while (iterator.hasNext()) {
						log.debug("ハッシュマップ");
						obj = (Map.Entry)iterator.next();
						// キーがnameの場合
						if(obj.getKey().equals(CusconConst.NAME)) {
							log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
							objLink.setName(obj.getValue().toString());
						} else {
							log.debug("key:" + obj.getKey());
							objLink.setLinkNameList((List<String>)obj.getValue());
						}
					}
					List<String> tmpLinkNameList = objLink.getLinkNameList();
					for(String objs : tmpLinkNameList) {
						LinkInfo tmpListItem = new LinkInfo();
						tmpListItem.setName(objLink.getName()) ;
						tmpListItem.setLinkName(objs) ;
						adrGrpNameList.add(tmpListItem);
					}
					//adrGrpNameList.add(objLink);
				}
			} catch(Exception e) {
				// DBアクセスエラー
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FZ001005", objectgArray, uvo));
				throw e;
			}
		}
		// アドレスオブジェクト情報リストを追加する。
		objList.setAddressGroups(adrGrpNameList);
		log.debug("setAddressGroups処理終了");
	}

	/**
	 * メソッド名 : setApplicationFilter
	 * 機能概要 : XML-APIによるアプリケーションフィルタオブジェクト取得、及びDB登録を行う。
	 * @param xmlAna
	 * @param xmlExe
	 * @param vsysId Vsys-ID
	 * @param xmlApiConnect XML-API実行URL
	 * @throws Exception
	 */
	private void setApplicationFilter(CusconXmlApiExecute xmlExe, CusconXMLAnalyze xmlAna,
			UpdateDAO updateDAO, CusconUVO uvo, String xmlApiConnect, SelectUpdatePA objList) throws Exception {

		log.debug("setApplicationFilter処理開始:vsysId = " + uvo.getVsysId() +
				", xmlApiConnect = " + xmlApiConnect);
		// Map
		Map.Entry obj = null;
	    // イテレータ
	    Iterator iterator = null;
		// アプリケーションフィルタオブジェクト取得用
        String APPLI_FIL = "application-filter";
        // アプリケーションフィルタオブジェクト情報リスト
		List<FilterInfo> appFilNameList = new ArrayList<FilterInfo>();

        // リンクデータクラス
        LinkInfo categoryLink = new LinkInfo();
        LinkInfo subCategoryLink = new LinkInfo();
        LinkInfo technologyLink = new LinkInfo();
        LinkInfo riskLink = new LinkInfo();

        // XML解析レスポンス
        List<HashMap<String, Object>> objAnalyse;
        // XML-API取得
        String objConnect;

        // 20121201 kkato@PROSITE mod start
        //objConnect = xmlApiConnect + SLASH + APPLI_FIL;
        // XML解析を行う
		//objAnalyse = getXmlResult(xmlExe, xmlAna, objConnect, uvo);
		objAnalyse = getXmlResult(xmlExe, xmlAna, xmlApiConnect, uvo, APPLI_FIL);
		// 20121201 kkato@PROSITE mod end

		if(objAnalyse != null) {
			log.debug("PAにアプリフィルタが存在");
			// XML解析レスポンスのデータ分繰り返す。
			for(int i=0; i<objAnalyse.size(); i++) {
				log.debug("XMLレスポンス解析:" + i);
				iterator = objAnalyse.get(i).entrySet().iterator();
		        // アプリケーションフィルタオブジェクトデータクラス
				//ApplicationFilters appliFil = new ApplicationFilters();
				FilterInfo appliFil = new FilterInfo();
				// アプリケーションフィルタの基本登録情報を設定する。
				//appliFil.setVsysId(uvo.getVsysId());
				//appliFil.setModFlg(CusconConst.NONE_NUM);
				//appliFil.setGenerationNo(CusconConst.ZERO_GENE);

				categoryLink = new LinkInfo();
				subCategoryLink = new LinkInfo();
				technologyLink = new LinkInfo();
				riskLink = new LinkInfo();

				// ハッシュマップのデータ分繰り返す。
				while (iterator.hasNext()) {
					log.debug("ハッシュマップ");
					obj = (Map.Entry)iterator.next();
					// キーがnameの場合
					if(obj.getKey().equals(CusconConst.NAME)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						appliFil.setName(obj.getValue().toString());
						categoryLink.setName(obj.getValue().toString());
						subCategoryLink.setName(obj.getValue().toString());
						technologyLink.setName(obj.getValue().toString());
						riskLink.setName(obj.getValue().toString());
					// キーがcategoryの場合
					} else if(obj.getKey().equals(CusconConst.CATEGORY)){
						log.debug("key:" + obj.getKey());
						categoryLink.setLinkNameList((List<String>)obj.getValue());
						// キーがsubcategoryの場合
						log.debug("key:" + obj.getKey());
					} else if(obj.getKey().equals(CusconConst.SUBCATEGORY)){
						log.debug("key:" + obj.getKey());
						subCategoryLink.setLinkNameList(
										(List<String>)obj.getValue());
						// キーがtechnologyの場合
					} else if(obj.getKey().equals(CusconConst.TECHNOLOGY)){
						log.debug("key:" + obj.getKey());
						technologyLink.setLinkNameList(
										(List<String>)obj.getValue());
						// キーがriskの場合
					} else if(obj.getKey().equals(CusconConst.RISK)) {
						log.debug("key:" + obj.getKey());
						riskLink.setLinkNameList((List<String>)obj.getValue());
					} else {
						log.debug("その他のデータ");
					}
				}
//				appFilNameList.add(appliFil);

				// カテゴリリンクが存在する場合
				if(categoryLink.getLinkNameList() != null) {
					log.debug("登録対象カテゴリが存在");
					// カテゴリリンクをDBに登録する。
					for(int j=0; j<categoryLink.getLinkNameList().size(); j++) {
						log.debug("カテゴリリンクDB登録:" + i);
						FilterInfo tmpListItem = new FilterInfo();
						tmpListItem.setName(categoryLink.getName()) ;
						tmpListItem.setCategory(categoryLink.getLinkNameList().get(j)) ;
						appFilNameList.add(tmpListItem);

//						// アプリケーションカテゴリリンクの基本登録情報を設定する。
//						categoryLink.setVsysId(uvo.getVsysId());
//						categoryLink.setGenerationNo(CusconConst.ZERO_GENE);
//						categoryLink.setLinkName(
//								categoryLink.getLinkNameList().get(j));
//						List<String> tmpLinkNameList = (List<String>)obj.getValue();
//						for(String objs : tmpLinkNameList) {
//							FilterInfo tmpListItem = new FilterInfo();
//							tmpListItem.setName(appliFil.getName()) ;
//							tmpListItem.setCategory(objs) ;
//							appFilNameList.add(tmpListItem);
//						}
//						try {
//							updateDAO.execute(
//									"CommonT_AplFltCategoryLink-2", categoryLink);
//						} catch (Exception e) {
//							// DBアクセスエラー
//							Object[] objectgArray = {e.getMessage()};
//							log.fatal(messageAccessor.getMessage(
//												"FZ001006", objectgArray, uvo));
//							throw e;
//						}
					}
				}

				// サブカテゴリリンクが存在する場合
				if(subCategoryLink.getLinkNameList() != null) {
					log.debug("登録対象サブカテゴリが存在");
					// サブカテゴリリンクをDBに登録する。
					for(int j=0; j<subCategoryLink.getLinkNameList().size(); j++) {
						log.debug("サブカテゴリリンクDB登録:" + i);
						FilterInfo tmpListItem = new FilterInfo();
						tmpListItem.setName(subCategoryLink.getName()) ;
						tmpListItem.setSubCategory(subCategoryLink.getLinkNameList().get(j)) ;
						appFilNameList.add(tmpListItem);

//						// アプリケーションサブカテゴリリンクの基本登録情報を設定する。
//						subCategoryLink.setVsysId(uvo.getVsysId());
//						subCategoryLink.setGenerationNo(CusconConst.ZERO_GENE);
//						subCategoryLink.setLinkName(
//								subCategoryLink.getLinkNameList().get(j));
//						try {
//							updateDAO.execute(
//								"CommonT_AplFltSubCategoryLink-2", subCategoryLink);
//						} catch (Exception e) {
//							// DBアクセスエラー
//							Object[] objectgArray = {e.getMessage()};
//							log.fatal(messageAccessor.getMessage(
//												"FZ001006", objectgArray, uvo));
//							throw e;
//						}
					}
				}

				// テクノロジリンクが存在する場合
				if(technologyLink.getLinkNameList() != null) {
					log.debug("登録対象テクノロジが存在");
					// テクノロジリンクをDBに登録する。
					for(int j=0; j<technologyLink.getLinkNameList().size(); j++) {
						log.debug("テクノロジリンクDB登録:" + i);
						FilterInfo tmpListItem = new FilterInfo();
						tmpListItem.setName(technologyLink.getName()) ;
						tmpListItem.setTechnology(technologyLink.getLinkNameList().get(j)) ;
						appFilNameList.add(tmpListItem);

//						// アプリケーションテクノロジリンクの基本登録情報を設定する。
//						technologyLink.setVsysId(uvo.getVsysId());
//						technologyLink.setGenerationNo(CusconConst.ZERO_GENE);
//						technologyLink.setLinkName(
//								technologyLink.getLinkNameList().get(j));
//						try {
//							updateDAO.execute(
//									"CommonT_AplFltTechnologyLink-2", technologyLink);
//						} catch (Exception e) {
//							// DBアクセスエラー
//							Object[] objectgArray = {e.getMessage()};
//							log.fatal(messageAccessor.getMessage(
//											"FZ001006", objectgArray, uvo));
//							throw e;
//						}
					}
				}

				// リスクリンクが存在する場合
				if(riskLink.getLinkNameList() != null) {
					log.debug("登録対象リスクが存在");
					// リスクリンクをDBに登録する。
					for(int j=0; j<riskLink.getLinkNameList().size(); j++) {
						log.debug("リスクリンクDB登録:" + i);
						FilterInfo tmpListItem = new FilterInfo();
						tmpListItem.setName(riskLink.getName()) ;
						tmpListItem.setRisk(Integer.parseInt(riskLink.getLinkNameList().get(j)));
						appFilNameList.add(tmpListItem);

//						// アプリケーションリスクリンクの基本登録情報を設定する。
//						riskLink.setVsysId(uvo.getVsysId());
//						riskLink.setGenerationNo(CusconConst.ZERO_GENE);
//						riskLink.setLinkName(riskLink.getLinkNameList().get(j));
//						try {
//							updateDAO.execute("CommonT_AplFltRiskLink-2", riskLink);
//						} catch (Exception e) {
//							// DBアクセスエラー
//							Object[] objectgArray = {e.getMessage()};
//							log.fatal(messageAccessor.getMessage(
//											"FZ001006", objectgArray, uvo));
//							throw e;
//						}
					}
				}
			}
		}
		// アプリケーションフィルタオブジェクト情報リストを追加する。
		objList.setApplicationFilters(appFilNameList);
		log.debug("setApplicationFilter処理終了");
	}

	/**
	 * メソッド名 : setApplicationGroups
	 * 機能概要 : XML-APIによるアプリケーショングループオブジェクト取得、及びDB登録を行う。
	 * @param xmlAna
	 * @param xmlExe
	 * @param vsysId Vsys-ID
	 * @param xmlApiConnect XML-API実行URL
	 * @throws Exception
	 */
	private void setApplicationGroups(CusconXmlApiExecute xmlExe, CusconXMLAnalyze xmlAna,
			UpdateDAO updateDAO, CusconUVO uvo, String xmlApiConnect, SelectUpdatePA objList) throws Exception {

		log.debug("setApplicationGroups処理開始:vsysId = " + uvo.getVsysId() +
				", xmlApiConnect = " + xmlApiConnect);
		// Map
		Map.Entry obj = null;
	    // イテレータ
	    Iterator iterator = null;
		// アプリケーショングループオブジェクト取得用
        String APPLI_GRP = "application-group";
        // アプリケーションフィルタオブジェクト情報リスト
		List<LinkInfo> appGrpList = new ArrayList<LinkInfo>();

        // アプリケーショングループオブジェクトデータクラス
        ApplicationGroups appliGrp = new ApplicationGroups();

        // XML解析レスポンス
        List<HashMap<String, Object>> objAnalyse;
        // XML-API取得
        String objConnect;

		// 20121201 kkato@PROSITE mod start
		//objConnect = xmlApiConnect + SLASH + APPLI_GRP;
		// XML解析を行う
		//objAnalyse = getXmlResult(xmlExe, xmlAna, objConnect, uvo);
		objAnalyse = getXmlResult(xmlExe, xmlAna, xmlApiConnect, uvo, APPLI_GRP);
		// 20121201 kkato@PROSITE mod end

		// アプリケーショングループリンクの基本登録情報を設定する。
		appliGrp.setVsysId(uvo.getVsysId());
		appliGrp.setModFlg(CusconConst.NONE_NUM);
		appliGrp.setGenerationNo(CusconConst.ZERO_GENE);

		List<LinkInfo> objLinkList = new ArrayList<LinkInfo>();

		if(objAnalyse != null) {
			log.debug("PAにアプリグループが存在");
			try {
				// XML解析レスポンスのデータ分繰り返す。
				for(int i=0; i<objAnalyse.size(); i++) {
					log.debug("XMLレスポンス解析:" + i);
					iterator = objAnalyse.get(i).entrySet().iterator();
			        // リンクデータクラス
			        LinkInfo objLink = new LinkInfo();
					// リンクの基本登録情報を設定する。
					objLink.setVsysId(uvo.getVsysId());
					objLink.setGenerationNo(CusconConst.ZERO_GENE);

					// ハッシュマップのデータ分繰り返す。
					while (iterator.hasNext()) {
						log.debug("ハッシュマップ");
						obj = (Map.Entry)iterator.next();
						// キーがnameの場合
						if(obj.getKey().equals(CusconConst.NAME)) {
							log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
							objLink.setName(obj.getValue().toString());
						} else {
							log.debug("key:" + obj.getKey());
							objLink.setLinkNameList((List<String>)obj.getValue());
						}
					}
					objLinkList.add(objLink);
				}
				for (int i = 0; i < objLinkList.size(); i++) {
					// アプリケーショングループリンクをDBに登録する。
					log.debug("グループリンク登録");
//					updateDAO.execute("CommonT_ApplicationGroupsLink-2", objLinkList.get(i));
					List<String> tmpLinkNameList =  (List<String>)objLinkList.get(i).getLinkNameList();
					for(String objs : tmpLinkNameList) {
						LinkInfo tmpListItem = new LinkInfo();
						tmpListItem.setName(objLinkList.get(i).getName()) ;
						tmpListItem.setLinkName(objs) ;
						appGrpList.add(tmpListItem);
					}
				}
			} catch (Exception e) {
				// DBアクセスエラー
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FZ001007", objectgArray, uvo));
				throw e;
			}
		}
		// アプリケーショングループオブジェクト情報リストを追加する。
		objList.setApplicationGroups(appGrpList);
		log.debug("setApplicationGroups処理終了");
	}

	/**
	 * メソッド名 : setServices
	 * 機能概要 : XML-APIによるサービスオブジェクト取得、及びDB登録を行う。
	 * @param xmlAna
	 * @param xmlExe
	 * @param vsysId Vsys-ID
	 * @param xmlApiConnect XML-API実行URL
	 * @throws Exception
	 */
	private void setServices(CusconXmlApiExecute xmlExe, CusconXMLAnalyze xmlAna,
			UpdateDAO updateDAO, CusconUVO uvo, String xmlApiConnect, SelectUpdatePA objList) throws Exception {

		log.debug("setServices処理開始:vsysId = " + uvo.getVsysId() +
									", xmlApiConnect = " + xmlApiConnect);
		// Map
		Map.Entry obj = null;
	    // イテレータ
	    Iterator iterator = null;
        // サービスオブジェクト取得用
        String SERVICE = "service";
        // サービスオブジェクト情報リスト
		List<Services> srvNameList = new ArrayList<Services>();

        // XML解析レスポンス
        List<HashMap<String, Object>> objAnalyse;
        // XML-API取得
        String objConnect;

		// 20121201 kkato@PROSITE mod start
		//objConnect = xmlApiConnect + SLASH + SERVICE;
		// XML解析を行う
		//objAnalyse = getXmlResult(xmlExe, xmlAna, objConnect, uvo);
		objAnalyse = getXmlResult(xmlExe, xmlAna, xmlApiConnect, uvo, SERVICE);
		// 20121201 kkato@PROSITE mod end

		if(objAnalyse != null) {
			log.debug("PAにサービスが存在");
			// XML解析レスポンスのデータ分繰り返す。
			for(int i=0; i<objAnalyse.size(); i++) {
				log.debug("XMLレスポンス解析:" + i);
				iterator = objAnalyse.get(i).entrySet().iterator();

		        // サービスオブジェクトデータクラス
		        Services service = new Services();
				// サービスの基本登録情報を設定する。
				service.setVsysId(uvo.getVsysId());
				service.setModFlg(CusconConst.NONE_NUM);
				service.setGenerationNo(CusconConst.ZERO_GENE);

				// ハッシュマップのデータ分繰り返す。
				while (iterator.hasNext()) {
					log.debug("ハッシュマップ");
					obj = (Map.Entry)iterator.next();
					// キーがnameの場合
					if(obj.getKey().equals(CusconConst.NAME)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						service.setName(obj.getValue().toString());
// 20121201 kkato@PROSITE mod start
//					// キーがtcpの場合
//					} else if(obj.getKey().equals(CusconConst.TCP)){
//						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
//						service.setProtocol(obj.getKey().toString());
//						service.setPort(obj.getValue().toString());
//					// キーがudpの場合
//					} else {
//						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
//						service.setProtocol(obj.getKey().toString());
//						service.setPort(obj.getValue().toString());
					// キーがPROTCOLの場合
					} else if(obj.getKey().equals(PROTOCOL)){
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						service.setProtocol(obj.getValue().toString());
					// キーがPORTの場合
					} else if(obj.getKey().equals(PORT)){
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						service.setPort(obj.getValue().toString());
// 20121201 kkato@PROSITE mod end
					}
				}
				srvNameList.add(service);
			}
		}
		// サービスオブジェクト情報リストを追加する。
		objList.setServices(srvNameList);
		log.debug("setServices処理終了");

	}

	/**
	 * メソッド名 : setServiceGroups
	 * 機能概要 : XML-APIによるサービスグループオブジェクト取得、及びDB登録を行う。
	 * @param xmlAna
	 * @param xmlExe
	 * @param vsysId Vsys-ID
	 * @param xmlApiConnect XML-API実行URL
	 * @throws Exception
	 */
	private void setServiceGroups(CusconXmlApiExecute xmlExe, CusconXMLAnalyze xmlAna,
			UpdateDAO updateDAO, CusconUVO uvo, String xmlApiConnect, SelectUpdatePA objList) throws Exception {

		log.debug("setServiceGroups処理開始:vsysId = " + uvo.getVsysId() +
									", xmlApiConnect = " + xmlApiConnect);
		// Map
		Map.Entry obj = null;
	    // イテレータ
	    Iterator iterator = null;
		// サービスグループオブジェクト取得用
        String SERVICE_GRP = "service-group";

        // サービスグループ情報
    	List<LinkInfo> srvGrpNameList = new ArrayList<LinkInfo>();

        // XML解析レスポンス
        List<HashMap<String, Object>> objAnalyse;
        // XML-API取得
        String objConnect;

		// 20121201 kkato@PROSITE mod start
		//objConnect = xmlApiConnect + SLASH + SERVICE_GRP;
		// XML解析を行う
		//objAnalyse = getXmlResult(xmlExe, xmlAna, objConnect, uvo);
		objAnalyse = getXmlResult(xmlExe, xmlAna, xmlApiConnect, uvo, SERVICE_GRP);
		// 20121201 kkato@PROSITE mod end

		List<LinkInfo> objLinkList = new ArrayList<LinkInfo>();

		if(objAnalyse != null) {
			log.debug("PAにサービスグループが存在");
			try {
				// XML解析レスポンスのデータ分繰り返す。
				for(int i=0; i<objAnalyse.size(); i++) {
					log.debug("XMLレスポンス解析:" + i);
					iterator = objAnalyse.get(i).entrySet().iterator();

					// リンクデータクラス
			        LinkInfo objLink = new LinkInfo();
					// リンクの基本登録情報を設定する。
					objLink.setVsysId(uvo.getVsysId());
					objLink.setGenerationNo(CusconConst.ZERO_GENE);

					// ハッシュマップのデータ分繰り返す。
					while (iterator.hasNext()) {
						log.debug("ハッシュマップ");
						obj = (Map.Entry)iterator.next();
						// キーがnameの場合
						if(obj.getKey().equals(CusconConst.NAME)) {
							log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
							objLink.setName(obj.getValue().toString());
							// キーがentryの場合
						} else {
							log.debug("key:" + obj.getKey());
							objLink.setLinkNameList((List<String>)obj.getValue());
						}
					}
					objLinkList.add(objLink);
				}

				// UPDATED: 2016.12 by Plum Systems Inc.
				// サービスグループ取得対応 -->
				for (int i = 0; i < objLinkList.size(); i++) {
					// サービスグループリンクをDBに登録する。
					log.debug("グループリンク登録 : " + objLinkList.size());
					List<String> tmpLinkNameList =  (List<String>)objLinkList.get(i).getLinkNameList();
					for(String objs : tmpLinkNameList) {
						LinkInfo tmpListItem = new LinkInfo();
						tmpListItem.setName(objLinkList.get(i).getName()) ;
						tmpListItem.setLinkName(objs) ;
						log.debug(objLinkList.get(i).getName() + " - " + objs);
						srvGrpNameList.add(tmpListItem);
					}
				}
				// サービスグループ取得対応 -->
			} catch (Exception e) {
					// DBアクセスエラー
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage("FZ001009", objectgArray, uvo));
					throw e;
			}
		}
		// サービスグループオブジェクト情報リストを追加する。
		objList.setServiceGroups(srvGrpNameList);
		log.debug("setServiceGroups処理終了");
	}

	/**
	 * メソッド名 : setSchedules
	 * 機能概要 : XML-APIによるスケジュールオブジェクト取得、及びDB登録を行う。
	 * @param xmlAna
	 * @param xmlExe
	 * @param vsysId Vsys-ID
	 * @param xmlApiConnect XML-API実行URL
	 * @throws Exception
	 */
//	private void setSchedules(CusconXmlApiExecute xmlExe, CusconXMLAnalyze xmlAna,
//			UpdateDAO updateDAO, CusconUVO uvo, String xmlApiConnect) throws Exception {
//
//		log.debug("setSchedules処理開始:vsysId = " + uvo.getVsysId() +
//								", xmlApiConnect = " + xmlApiConnect);
//		// Map
//		Map.Entry obj = null;
//	    // イテレータ
//	    Iterator iterator = null;
//		// スケジュールオブジェクト取得用
//        String SCHEDULE = "schedule";
//
//        // スケジュールオブジェクトデータクラス
//        Schedules schedule = new Schedules();
//        // スケジュールリンククラス
//        DailySchedulesLink dailySchedule = new DailySchedulesLink();
//        WeeklySchedulesLink weeklySchedule = new WeeklySchedulesLink();
//        NonRecurringSchedulesLink nonRecSchedule =
//        								new NonRecurringSchedulesLink();
//        // オブジェクト旧名データクラス
//        ObjectOldName objectOldName = new ObjectOldName();
//
//        // XML解析レスポンス
//        List<HashMap<String, Object>> objAnalyse;
//        // XML-API取得
//        String objConnect;
//
//        // スケジュール一時時刻分割用
//        List<String> tmpList;
//        // スケジュール開始時刻分割用
//		List<String> startTimeList;
//		// スケジュール終了時刻分割用
//		List<String> endTimeList;
//		 // スケジュール開始日時分割用
//		List<String> startDateList;
//		// スケジュール終了日時分割用
//		List<String> endDateList;
//		// ウィークリー処理一時用
//		List<WeeklySchedulesLink> tmpWeeklyList = null;
//
//		objConnect = xmlApiConnect + SLASH + SCHEDULE;
//		// XML解析を行う
//		objAnalyse = getXmlResult(xmlExe, xmlAna, objConnect, uvo);
//
//		// スケジュールの基本登録情報を設定する。
//		schedule.setVsysId(uvo.getVsysId());
//		schedule.setModFlg(CusconConst.NONE_NUM);
//		schedule.setGenerationNo(CusconConst.ZERO_GENE);
//		objectOldName.setVsysId(uvo.getVsysId());
//		objectOldName.setObjectType(CusconConst.Schedule);
//
//		if(objAnalyse != null) {
//			log.debug("PAにスケジュールが存在");
//			// XML解析レスポンスのデータ分繰り返す。
//			for(int i=0; i<objAnalyse.size(); i++) {
//				log.debug("XMLレスポンス解析:" + i);
//				iterator = objAnalyse.get(i).entrySet().iterator();
//				tmpWeeklyList = new ArrayList<WeeklySchedulesLink>();
//				dailySchedule = new DailySchedulesLink();
//				nonRecSchedule = new NonRecurringSchedulesLink();
//				// ハッシュマップのデータ分繰り返す。
//				while (iterator.hasNext()) {
//					log.debug("ハッシュマップ");
//					obj = (Map.Entry)iterator.next();
//
//					weeklySchedule = new WeeklySchedulesLink();
//					// キーがnameの場合
//					if(obj.getKey().equals(CusconConst.NAME)) {
//						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
//						schedule.setName(obj.getValue().toString());
//						objectOldName.setName(obj.getValue().toString());
//						dailySchedule.setName(obj.getValue().toString());
//						weeklySchedule.setName(obj.getValue().toString());
//						nonRecSchedule.setName(obj.getValue().toString());
//					// キーがdailyの場合
//					} else if(obj.getKey().equals(CusconConst.DAILY_STR)) {
//						log.debug("key:" + obj.getKey());
//						schedule.setRecurrence(CusconConst.DAILY);
//
//						tmpList = (List<String>)obj.getValue();
//						startTimeList = new ArrayList<String>();
//						endTimeList = new ArrayList<String>();
//
//						// スケジュール時刻リスト分繰り返す。
//						for(int j=0; j<tmpList.size(); j++) {
//							log.debug("時刻リスト:" + j);
//							// 開始時刻、終了時刻に分割する。
//							String[] split_Str = tmpList.get(j).split(PHY);
//							startTimeList.add(split_Str[0]);
//							endTimeList.add(split_Str[1]);
//						}
//						// 開始時刻、終了時刻を格納する。
//						dailySchedule.setStartTimeList(startTimeList);
//						dailySchedule.setEndTimeList(endTimeList);
//
//					// キーがnon-recurringの場合
//					} else if(obj.getKey().equals(CusconConst.NONRECURRING_STR)) {
//						log.debug("key:" + obj.getKey());
//						schedule.setRecurrence(CusconConst.NONRECURRING);
//
//						tmpList = (List<String>)obj.getValue();
//						startDateList = new ArrayList<String>();
//						startTimeList = new ArrayList<String>();
//						endDateList = new ArrayList<String>();
//						endTimeList = new ArrayList<String>();
//
//						// スケジュール時刻リスト分繰り返す。
//						for(int j=0; j<tmpList.size(); j++) {
//							log.debug("時刻リスト:" + j);
//							// 開始日時、開始時刻、終了日時、終了時刻に分割する。
//							String[] splitAtStr = tmpList.get(j).split(AT);
//							String[] split_Str = splitAtStr[1].split(PHY);
//
//							startDateList.add(splitAtStr[0]);
//							startTimeList.add(split_Str[0]);
//							endDateList.add(split_Str[1]);
//							endTimeList.add(splitAtStr[2]);
//						}
//						// 開始日時、開始時刻、終了日時、終了時刻を格納する。
//						nonRecSchedule.setStartDateList(startDateList);
//						nonRecSchedule.setStartTimeList(startTimeList);
//						nonRecSchedule.setEndDateList(endDateList);
//						nonRecSchedule.setEndTimeList(endTimeList);
//
//					// キーが上記以外の場合
//					} else {
//						log.debug("key:" + obj.getKey());
//						schedule.setRecurrence(1);
//						// キーがsundayの場合
//						if(obj.getKey().equals(CusconConst.SUN_STR)) {
//							log.debug("key:" + obj.getKey());
//							setWeeklySchedule(weeklySchedule, obj, CusconConst.SUN);
//							tmpWeeklyList.add(weeklySchedule);
//						// キーがmondayの場合
//						} else if(obj.getKey().equals(CusconConst.MON_STR)) {
//							log.debug("key:" + obj.getKey());
//							setWeeklySchedule(weeklySchedule, obj, CusconConst.MON);
//							tmpWeeklyList.add(weeklySchedule);
//						// キーがtuesdayの場合
//						} else if(obj.getKey().equals(CusconConst.TUE_STR)) {
//							log.debug("key:" + obj.getKey());
//							setWeeklySchedule(weeklySchedule, obj, CusconConst.TUE);
//							tmpWeeklyList.add(weeklySchedule);
//						// キーがwednesdayの場合
//						} else if(obj.getKey().equals(CusconConst.WED_STR)) {
//							log.debug("key:" + obj.getKey());
//							setWeeklySchedule(weeklySchedule, obj, CusconConst.WED);
//							tmpWeeklyList.add(weeklySchedule);
//						// キーがthursdayの場合
//						} else if(obj.getKey().equals(CusconConst.THU_STR)) {
//							log.debug("key:" + obj.getKey());
//							setWeeklySchedule(weeklySchedule, obj, CusconConst.THU);
//							tmpWeeklyList.add(weeklySchedule);
//						// キーがfridayの場合
//						} else if(obj.getKey().equals(CusconConst.FRI_STR)) {
//							log.debug("key:" + obj.getKey());
//							setWeeklySchedule(weeklySchedule, obj, CusconConst.FRI);
//							tmpWeeklyList.add(weeklySchedule);
//						// キーがsaturdayの場合
//						} else {
//							log.debug("key:" + obj.getKey());
//							setWeeklySchedule(weeklySchedule, obj, CusconConst.SAT);
//							tmpWeeklyList.add(weeklySchedule);
//						}
//					}
//				}
//
//				try {
//					// スケジュールオブジェクトをDBに登録する。
//					updateDAO.execute("CommonT_Schedules-6", schedule);
//					updateDAO.execute("CommonT_ObjectOldName-1", objectOldName);
//				} catch(Exception e) {
//					// トランザクションロールバック
//					TransactionUtil.setRollbackOnly();
//					// DBアクセスエラー
//					Object[] objectgArray = {e.getMessage()};
//					log.fatal(messageAccessor.getMessage(
//											"FZ001010", objectgArray, uvo));
//					throw e;
//				}
//
//				// デイリースケジュールが存在する場合
//				if(dailySchedule.getStartTimeList() != null) {
//					log.debug("デイリースケジュールデータ存在");
//
//					// デイリースケジュールクリンクをDBに登録する。
//					for(int j=0; j<dailySchedule.getStartTimeList().size(); j++) {
//
//						log.debug("デイリースケジュールをDB登録" + j);
//						// デイリーの基本登録情報を設定する。
//						dailySchedule.setVsysId(uvo.getVsysId());
//						dailySchedule.setGenerationNo(CusconConst.ZERO_GENE);
//						dailySchedule.setStartTime(
//								dailySchedule.getStartTimeList().get(j));
//						dailySchedule.setEndTime(
//								dailySchedule.getEndTimeList().get(j));
//						try {
//							updateDAO.execute(
//								"CommonT_DailySchedulesLink-2", dailySchedule);
//						} catch (Exception e) {
//							// トランザクションロールバック
//							TransactionUtil.setRollbackOnly();
//							// DBアクセスエラー
//							Object[] objectgArray = {e.getMessage()};
//							log.fatal(messageAccessor.getMessage(
//											"FZ001010", objectgArray, uvo));
//							throw e;
//						}
//					}
//				}
//				// ウィークリースケジュールクリンクをDBに登録する。
//				for(int j=0; j<tmpWeeklyList.size(); j++) {
//					log.debug("ウィークリースケジュールをDB登録" + j);
//					for(int k=0; k<
//						tmpWeeklyList.get(j).getStartTimeList().size(); k++) {
//
//						log.debug("ウィークリースケジュールデータループ");
//
//						WeeklySchedulesLink weeklySchedule2 = new WeeklySchedulesLink();
//						// ウィークリーの基本登録情報を設定する。
//						weeklySchedule2.setDayOfWeek(
//								tmpWeeklyList.get(j).getDayOfWeek());
//						weeklySchedule2.setStartTime(
//								tmpWeeklyList.get(j).getStartTimeList().get(k));
//						weeklySchedule2.setEndTime(
//								tmpWeeklyList.get(j).getEndTimeList().get(k));
//						weeklySchedule2.setName(schedule.getName());
//						weeklySchedule2.setVsysId(uvo.getVsysId());
//						weeklySchedule2.setGenerationNo(CusconConst.ZERO_GENE);
//
//						try {
//							updateDAO.execute(
//									"CommonT_WeeklySchedulesLink-2", weeklySchedule2);
//						} catch (Exception e){
//							// トランザクションロールバック
//							TransactionUtil.setRollbackOnly();
//							// DBアクセスエラー
//							Object[] objectgArray = {e.getMessage()};
//							log.fatal(messageAccessor.getMessage("FZ001010", objectgArray, uvo));
//							throw e;
//						}
//					}
//				}
//
//				// 臨時スケジュールが存在する場合
//				if(nonRecSchedule.getStartTimeList() != null) {
//					log.debug("臨時スケジュールデータ存在");
//
//					// 臨時スケジュールクリンクをDBに登録する。
//					for(int j=0; j<nonRecSchedule.getStartTimeList().size(); j++) {
//
//						log.debug("臨時スケジュールをDB登録" + j);
//						// 臨時の基本登録情報を設定する。
//						nonRecSchedule.setVsysId(uvo.getVsysId());
//						nonRecSchedule.setGenerationNo(CusconConst.ZERO_GENE);
//						nonRecSchedule.setStartDate(
//								nonRecSchedule.getStartDateList().get(j));
//						nonRecSchedule.setStartTime(
//								nonRecSchedule.getStartTimeList().get(j));
//						nonRecSchedule.setEndDate(
//								nonRecSchedule.getEndDateList().get(j));
//						nonRecSchedule.setEndTime(
//								nonRecSchedule.getEndTimeList().get(j));
//						try {
//							updateDAO.execute(
//									"CommonT_NonRecurringSchedulesLink-2", nonRecSchedule);
//						} catch(Exception e) {
//							// トランザクションロールバック
//							TransactionUtil.setRollbackOnly();
//							// DBアクセスエラー
//							Object[] objectgArray = {e.getMessage()};
//							log.fatal(messageAccessor.getMessage("FZ001010", objectgArray, uvo));
//							throw e;
//						}
//					}
//				}
//			}
//		}
//		log.debug("setSchedules処理終了");
//
//	}
	/**
	 * メソッド名 : setWeeklySchedule
	 * 機能概要 :
	 * @param weeklySchedule ウィークリーデータクラス
	 * @param obj ウィークリーデータクラスにデータを設定する。
	 * @param week 曜日
	 * @return WeeklySchedulesLink ウィークリーデータクラス
	 */
//	private void setWeeklySchedule(
//				WeeklySchedulesLink weeklySchedule, Map.Entry obj, int week) {
//
//		log.debug("setWeeklySchedule処理開始:week = " + week);
//
//		List<String> tmpList = (List<String>)obj.getValue();
//		List<String> startTimeList = new ArrayList<String>();
//		List<String> endTimeList = new ArrayList<String>();
//
//		// スケジュール時刻リスト分繰り返す。
//		for(int i=0; i<tmpList.size(); i++) {
//			log.debug("時刻リスト" + i);
//			// 開始時刻、終了時刻に分割する。
//			String[] split_Str = tmpList.get(i).split(PHY);
//			startTimeList.add(split_Str[0]);
//			endTimeList.add(split_Str[1]);
//		}
//		// 曜日、開始時刻、終了時刻を格納する。
//		weeklySchedule.setDayOfWeek(week);
//		weeklySchedule.setStartTimeList(startTimeList);
//		weeklySchedule.setEndTimeList(endTimeList);
//
//		log.debug("setWeeklySchedule処理終了");
//	}

	/**
	 * メソッド名 : setPolicyInfo
	 * 機能概要 : XML-APIによるポリシー取得、及びDB登録を行う。
	 * @param xmlAna
	 * @param xmlExe
	 * @param vsysId Vsys-ID
	 * @param xmlApiConnect XML-API実行URL
	 * @throws Exception
	 */
	private void setPolicyInfo(CusconXmlApiExecute xmlExe, CusconXMLAnalyze xmlAna,
			UpdateDAO updateDAO, QueryDAO queryDAO,
			CusconUVO uvo, String xmlApiConnect, List<SelectPolicyList> policyList) throws Exception {

		log.debug("setPolicyInfo処理開始:vsysId = " + uvo.getVsysId() +
								", xmlApiConnect = " + xmlApiConnect);
		// Map
		Map.Entry obj = null;
	    // イテレータ
	    Iterator iterator = null;
		// ポリシー取得用
        String POLICY = "rulebase/security/rules";
        String DESCRIPTION = "description";
        String SERVICE = "service";
        String ACTION = "action";
        String ALLOW = "allow";
        String VULNERABILITY = "vulnerability";
        String SCHEDULE = "schedule";
        String FROM = "from";
        String TO = "to";
        String SOURCE = "source";
        String DESTINATION = "destination";
        String APPLICATION = "application";
        String URLFILTERING = "url-filtering";
        String VIRUS = "virus";
        String SPYWARE = "spyware";

        // XML解析レスポンス
        List<HashMap<String, Object>> objAnalyse;
        // XML-API取得
        String objConnect;
        // ポリシー情報を取得する。
		List<SecurityRules> security;

        // 一時格納用リスト
        List<String> tmpList;

        // 20121201 kkato@PROSITE mod start
		//objConnect = xmlApiConnect + SLASH + POLICY;
		// XML解析を行う
		//objAnalyse = getXmlResult(xmlExe, xmlAna, objConnect, uvo);
		objAnalyse = getXmlResult(xmlExe, xmlAna, xmlApiConnect, uvo, POLICY);
		// 20121201 kkato@PROSITE mod end

		if(objAnalyse != null) {
			log.debug("PAにポリシーが存在");
			// XML解析レスポンスのデータ分繰り返す。
			for(int i=0; i<objAnalyse.size(); i++) {
				log.debug("XMLレスポンス解析:" + i);
				iterator = objAnalyse.get(i).entrySet().iterator();

				 // ポリシーデータクラス
//		        SecurityRules policy = new SecurityRules();
				SelectPolicyList policy = new SelectPolicyList();
		        LinkInfo srcZoneLink = new LinkInfo();
		        LinkInfo dstZoneLink = new LinkInfo();
		        LinkInfo srcAddressLink = new LinkInfo();
		        LinkInfo dstAddressLink = new LinkInfo();
		        LinkInfo appliLink = new LinkInfo();
		        LinkInfo serviceLink = new LinkInfo();
		        LinkInfo urlfilterLink = new LinkInfo();

		        SecurityRules base = new SecurityRules();

				policy = new SelectPolicyList();
//				policy.setVsysId(uvo.getVsysId());
				List<SelectObjectList> sourceZoneList = new ArrayList<SelectObjectList>();
				List<SelectObjectList> destinationZoneList = new ArrayList<SelectObjectList>();
				List<SelectObjectList> sourceAddressList = new ArrayList<SelectObjectList>();
				List<SelectObjectList> destinationAddressList = new ArrayList<SelectObjectList>();
				List<SelectObjectList> applicationList = new ArrayList<SelectObjectList>();
				List<SelectObjectList> serviceList = new ArrayList<SelectObjectList>();

				// ポリシーの基本登録情報を設定する。
//				policy.setVsysId(uvo.getVsysId());
//				policy.setModFlg(CusconConst.DEL_NUM);
//				policy.setGenerationNo(CusconConst.ZERO_GENE);

				base.setVsysId(uvo.getVsysId());
				base.setModFlg(CusconConst.DEL_NUM);
				base.setGenerationNo(CusconConst.ZERO_GENE);

				// 出所ゾーンリンクの基本登録情報を設定する。
				srcZoneLink.setVsysId(uvo.getVsysId());
				srcZoneLink.setGenerationNo(CusconConst.ZERO_GENE);

				// 宛先ゾーンリンクの基本登録情報を設定する。
				dstZoneLink.setVsysId(uvo.getVsysId());
				dstZoneLink.setGenerationNo(CusconConst.ZERO_GENE);

				// 出所アドレスリンクの基本登録情報を設定する。
				srcAddressLink.setVsysId(uvo.getVsysId());
				srcAddressLink.setGenerationNo(CusconConst.ZERO_GENE);

				// 宛先アドレスリンクの基本登録情報を設定する。
				dstAddressLink.setVsysId(uvo.getVsysId());
				dstAddressLink.setGenerationNo(CusconConst.ZERO_GENE);

				// サービスリンクの基本登録情報を設定する。
				serviceLink.setVsysId(uvo.getVsysId());
				serviceLink.setGenerationNo(CusconConst.ZERO_GENE);

				// アプリケーションリンクの基本登録情報を設定する。
				appliLink.setVsysId(uvo.getVsysId());
				appliLink.setGenerationNo(CusconConst.ZERO_GENE);

				// Webフィルタリンクの基本登録情報を設定する。
				urlfilterLink.setVsysId(uvo.getVsysId());
				urlfilterLink.setGenerationNo(CusconConst.ZERO_GENE);

				// 脆弱性の保護フラグは初期化しておく
				policy.setIds_ips("0");

				// ハッシュマップのデータ分繰り返す。
				while (iterator.hasNext()) {
					log.debug("ハッシュマップ");
					obj = (Map.Entry)iterator.next();
					// キーがnameの場合
					if(obj.getKey().equals(CusconConst.NAME)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						// ルール名を設定する。
						policy.setName(obj.getValue().toString());
						srcZoneLink.setName(obj.getValue().toString());
						dstZoneLink.setName(obj.getValue().toString());
						srcAddressLink.setName(obj.getValue().toString());
						dstAddressLink.setName(obj.getValue().toString());
						appliLink.setName(obj.getValue().toString());
						serviceLink.setName(obj.getValue().toString());
						urlfilterLink.setName(obj.getValue().toString());
						// 行数を設定する。
						policy.setLineNo(i+1);

					// キーがdescriptionの場合
					} else if(obj.getKey().equals(DESCRIPTION)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						// 備考を設定する。
						policy.setDescription(obj.getValue().toString());

					// キーがserviceの場合
					} else if(obj.getKey().equals(SERVICE)) {
						log.debug("key:" + obj.getKey());
				        // 一時格納用リスト
				        List<String> tmpList2;
						tmpList2 = (List<String>)obj.getValue();
						for(String objs : tmpList2) {
							// サービスを設定する。
							serviceLink.setLinkNameList((List<String>)obj.getValue());
							SelectObjectList tmpListItem = new SelectObjectList();
							// anyの場合
							tmpListItem.setName(objs) ;
							serviceList.add(tmpListItem);
							if(objs.equals(CusconConst.ANY)) {
								log.debug("tmpList.get(0) = " + objs);
								policy.setServiceDefaultFlg(0);

							// application-defaultの場合
							} else if(objs.equals(
									CusconConst.APPLICATION_DEFAULT)) {
								log.debug("tmpList.get(0) = " + objs);
								policy.setServiceDefaultFlg(1);

							// selectの場合
							} else {
								log.debug("tmpList.get(0) = " + objs);
								policy.setServiceDefaultFlg(2);
							}
						}

					// キーがactionの場合
					} else if(obj.getKey().equals(ACTION)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						// allowの場合
						if(obj.getValue().equals(ALLOW)) {
							log.debug("obj.getValue() = " + obj.getValue());
							//policy.setAction("0");
							policy.setAction(obj.getValue().toString());
						// denyの場合
						} else {
							log.debug("obj.getValue() = " + obj.getValue());
							//policy.setAction("1");
							policy.setAction(obj.getValue().toString());
						}

					// キーがvulnerabilityの場合
					} else if(obj.getKey().equals(VULNERABILITY)) {
						log.debug("key:" + obj.getKey());
						// defaultの場合しか存在しない。
						//policy.setIds_ips("1");
						policy.setIds_ips(DEFAULT);

//					// キーがscheduleの場合
//					} else if(obj.getKey().equals(SCHEDULE)) {
//						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
//						// スケジュール名を設定する。
//						policy.setScheduleName(obj.getValue().toString());
					// キーがfromの場合
					} else if(obj.getKey().equals(FROM)) {
						log.debug("key:" + obj.getKey());
						// 出所ゾーン名を設定する。
						srcZoneLink.setLinkNameList((List<String>)obj.getValue());
						for(String objs : (List<String>)obj.getValue()) {
							SelectObjectList tmpListItem = new SelectObjectList();
							tmpListItem.setName(objs) ;
							sourceZoneList.add(tmpListItem);
						}
					// キーがtoの場合
					} else if(obj.getKey().equals(TO)) {
						log.debug("key:" + obj.getKey());
						// 宛先ゾーン名を設定する。
						dstZoneLink.setLinkNameList((List<String>)obj.getValue());
						for(String objs : (List<String>)obj.getValue()) {
							SelectObjectList tmpListItem = new SelectObjectList();
							tmpListItem.setName(objs) ;
							destinationZoneList.add(tmpListItem);
						}
					// キーがsourceの場合
					} else if(obj.getKey().equals(SOURCE)) {
						log.debug("key:" + obj.getKey());
						// 出所アドレス名を設定する。
						srcAddressLink.setLinkNameList(
								(List<String>)obj.getValue());
						for(String objs : (List<String>)obj.getValue()) {
							SelectObjectList tmpListItem = new SelectObjectList();
							tmpListItem.setName(objs) ;
							sourceAddressList.add(tmpListItem);
						}
					// キーがdestinationの場合
					} else if(obj.getKey().equals(DESTINATION)) {
						log.debug("key:" + obj.getKey());
						// 宛先アドレス名を設定する。
						dstAddressLink.setLinkNameList(
								(List<String>)obj.getValue());
						for(String objs : (List<String>)obj.getValue()) {
							SelectObjectList tmpListItem = new SelectObjectList();
							tmpListItem.setName(objs) ;
							destinationAddressList.add(tmpListItem);
						}
					// キーがapplicationの場合
					} else if(obj.getKey().equals(APPLICATION)) {
						log.debug("key:" + obj.getKey());
						// アプリケーションを設定する。
						appliLink.setLinkNameList((List<String>)obj.getValue());
						for(String objs : (List<String>)obj.getValue()) {
							SelectObjectList tmpListItem = new SelectObjectList();
							tmpListItem.setName(objs) ;
							applicationList.add(tmpListItem);
						}
					// 各々キーが存在すれば各々のフラグに1をセット
					// キーがurl-filteringの場合
					} else if(obj.getKey().equals(URLFILTERING)) {
						log.debug("key:" + obj.getKey());
						policy.setUrlfilterDefaultFlg(1);
//						urlfilterLink.setLinkNameList((List<String>)obj.getValue());
						List<String> tmpListItem = (List<String>)obj.getValue();
						policy.setUrlFilterName(tmpListItem.get(0).toString());
						// キーがurl-filteringの場合
					} else if(obj.getKey().equals(VIRUS)) {
						log.debug("key:" + obj.getKey());
						policy.setVirusCheckFlg("1");
						// キーがurl-filteringの場合
					} else if(obj.getKey().equals(SPYWARE)) {
						log.debug("key:" + obj.getKey());
						policy.setSpywareFlg("1");
					// キーがserviceの場合
					} else {
						log.debug("key:" + obj.getKey());
					}
				}
				// セキュリティポリシー情報リストを追加する。
				policy.setSourceZoneList(sourceZoneList);
				policy.setDestinationZoneList(destinationZoneList);
				policy.setSourceAddressList(sourceAddressList);
				policy.setDestinationAddressList(destinationAddressList);
				policy.setApplicationList(applicationList);
				policy.setServiceList(serviceList);
				policyList.add(policy);
//				try {
//					// ポリシー情報をDBに登録する。
//					updateDAO.execute("CommonT_SecurityRules-4", policy);
//					// ポリシー情報のスケジュールリンク番号を更新する。
//					updateDAO.execute("CommonT_SecurityRules-5", policy);
//					// ポリシー情報を取得する。
//					base.setName(policy.getName());
//					security =
//						queryDAO.executeForObjectList("MovePolicy-2", base);
//				} catch(Exception e) {
//					// トランザクションロールバック
//					TransactionUtil.setRollbackOnly();
//					// DBアクセスエラー
//					Object[] objectgArray = {e.getMessage()};
//					log.fatal(messageAccessor.getMessage(
//												"FZ001011", objectgArray, uvo));
//					throw e;
//				}

//				/**************************************************
//				 *  出所ゾーンリンクDB登録
//				 **************************************************/
//				// 出所ゾーン名リストの1つ目がany以外場合のみ登録処理を行う。
//				if(!srcZoneLink.getLinkNameList().get(0).equals(CusconConst.ANY)) {
//					log.debug("出所ゾーンリンクDB登録");
//					try {
//						updateDAO.execute("CommonT_SourceZoneLink-2", srcZoneLink);
//					} catch(Exception e) {
//						// トランザクションロールバック
//						TransactionUtil.setRollbackOnly();
//						// DBアクセスエラー
//						Object[] objectgArray = {e.getMessage()};
//						log.fatal(messageAccessor.getMessage(
//											"FZ001011", objectgArray, uvo));
//						throw e;
//					}
//				}
//
//				/**************************************************
//				 *  宛先ゾーンリンクDB登録
//				 **************************************************/
//				// 宛先ゾーン名リストの1つ目がany以外場合のみ登録処理を行う。
//				if(!dstZoneLink.getLinkNameList().get(0).equals(CusconConst.ANY)) {
//					log.debug("宛先ゾーンリンクDB登録");
//					try {
//						updateDAO.execute(
//								"CommonT_DestinationZoneLink-2", dstZoneLink);
//					} catch(Exception e) {
//						// トランザクションロールバック
//						TransactionUtil.setRollbackOnly();
//						// DBアクセスエラー
//						Object[] objectgArray = {e.getMessage()};
//						log.fatal(messageAccessor.getMessage(
//											"FZ001011", objectgArray, uvo));
//						throw e;
//					}
//				}
//
//				/**************************************************
//				 *  出所アドレスリンク、ローカル出所アドレスDB登録
//				 **************************************************/
//				// 処理結果
//				int rec = 0;
//				// 出所アドレス名リストの1つ目がany以外場合のみ登録処理を行う。
//				if(!srcAddressLink.getLinkNameList().get(0).
//												equals(CusconConst.ANY)) {
//					log.debug("出所アドレスリンクDB登録");
//					// 取得したアドレス名リスト分繰り返す。
//					for(int j = 0;
//							j<srcAddressLink.getLinkNameList().size(); j++) {
//
//						log.debug("登録中:" + j);
//						srcAddressLink.setLinkName(
//								srcAddressLink.getLinkNameList().get(j));
//						try {
//							rec = updateDAO.execute(
//								"CommonT_SourceAddressLink-2", srcAddressLink);
//						} catch(Exception e) {
//							// トランザクションロールバック
//							TransactionUtil.setRollbackOnly();
//							// DBアクセスエラー
//							Object[] objectgArray = {e.getMessage()};
//							log.fatal(messageAccessor.getMessage(
//											"FZ001011", objectgArray, uvo));
//							throw e;
//						}
//
//						// 処理結果が0件の場合
//						if(rec == 0) {
//							log.debug("ローカル出所アドレスDB登録");
//							try {
//								// ローカル出所アドレスのシーケンス番号を払い出す。
//								int linkSeqNo = queryDAO.executeForObject(
//									"LocalSourceAddressSeq-1", null,
//													java.lang.Integer.class);
//
//								srcAddressLink.setLinkSeqNo(linkSeqNo);
//
//								// ローカル出所アドレスにDB登録を行う。
//								updateDAO.execute(
//									"CommonT_LocalSourceAddress-2", srcAddressLink);
//
//								// 出所アドレスリンクに登録したローカル出所アドレス
//								// とのリンクを登録する。
//								srcAddressLink.setSeqNo(security.get(0).getSeqNo());
//								updateDAO.execute(
//									"CommonT_SourceAddressLink-3", srcAddressLink);
//							} catch(Exception e) {
//								// トランザクションロールバック
//								TransactionUtil.setRollbackOnly();
//								// DBアクセスエラー
//								Object[] objectgArray = {e.getMessage()};
//								log.fatal(messageAccessor.getMessage(
//												"FZ001011", objectgArray, uvo));
//								throw e;
//							}
//						}
//					}
//				}
//				/**************************************************
//				 *  宛先アドレスリンク、ローカル宛先アドレスDB登録
//				 **************************************************/
//				// リンクシーケンス番号取得用
//				int linkSeqNo = 0;
//
//				// 宛先アドレス名リストの1つ目がany以外場合のみ登録処理を行う。
//				if(!dstAddressLink.getLinkNameList().get(0).
//												equals(CusconConst.ANY)) {
//					log.debug("宛先アドレスリンクDB登録");
//					// 取得したアドレス名リスト分繰り返す。
//					for(int j = 0;
//							j<dstAddressLink.getLinkNameList().size(); j++) {
//
//						log.debug("登録中:" + j);
//						dstAddressLink.setLinkName(
//							dstAddressLink.getLinkNameList().get(j));
//						try {
//							rec = updateDAO.execute(
//								"CommonT_DestinationAddressLink-2", dstAddressLink);
//						} catch(Exception e) {
//							// トランザクションロールバック
//							TransactionUtil.setRollbackOnly();
//							// DBアクセスエラー
//							Object[] objectgArray = {e.getMessage()};
//							log.fatal(messageAccessor.getMessage(
//												"FZ001011", objectgArray, uvo));
//							throw e;
//						}
//
//						// 処理結果が0件の場合
//						if(rec == 0) {
//							log.debug("ローカル宛先アドレスDB登録");
//							// ローカル宛先アドレスのシーケンス番号を払い出す。
//							try {
//								linkSeqNo = queryDAO.executeForObject(
//									"LocalDestinationAddress-1", null,
//													java.lang.Integer.class);
//							} catch(Exception e) {
//								// トランザクションロールバック
//								TransactionUtil.setRollbackOnly();
//								// DBアクセスエラー
//								Object[] objectgArray = {e.getMessage()};
//								log.fatal(messageAccessor.getMessage(
//												"FZ001011", objectgArray, uvo));
//								throw e;
//							}
//
//							dstAddressLink.setLinkSeqNo(linkSeqNo);
//							try {
//								// ローカル宛先アドレスにDB登録を行う。
//								updateDAO.execute(
//										"CommonT_LocalDestinationAddress-2",
//											dstAddressLink);
//
//								// 宛先アドレスリンクに登録したローカル宛先アドレス
//								// とのリンクを登録する。
//								dstAddressLink.setSeqNo(security.get(0).getSeqNo());
//								updateDAO.execute(
//										"CommonT_DestinationAddressLink-3",
//											dstAddressLink);
//							} catch(Exception e) {
//								// トランザクションロールバック
//								TransactionUtil.setRollbackOnly();
//								// DBアクセスエラー
//								Object[] objectgArray = {e.getMessage()};
//								log.fatal(messageAccessor.getMessage(
//												"FZ001011", objectgArray, uvo));
//								throw e;
//							}
//						}
//					}
//				}
//
//				/**************************************************
//				 *  サービスリンクDB登録
//				 **************************************************/
//				// サービス適用フラグが2(select)の場合のみ登録処理を行う。
//				if(policy.getServiceDefaultFlg() == 2) {
//					log.debug("サービスリンクDB登録");
//
//					try {
//						updateDAO.execute("CommonT_ServiceLink-2", serviceLink);
//					} catch(Exception e) {
//						// トランザクションロールバック
//						TransactionUtil.setRollbackOnly();
//						// DBアクセスエラー
//						Object[] objectgArray = {e.getMessage()};
//						log.fatal(messageAccessor.getMessage(
//												"FZ001011", objectgArray, uvo));
//						throw e;
//					}
//				}
//
//				/**************************************************
//				 *  アプリケーションリンクDB登録
//				 **************************************************/
//				// アプリケーション名リストの1つ目がany以外場合のみ登録処理を行う。
//				if(!appliLink.getLinkNameList().get(0).equals(CusconConst.ANY)) {
//					log.debug("アプリケーションリンクDB登録");
//					try {
//						updateDAO.execute("CommonT_ApplicationLink-2", appliLink);
//					} catch(Exception e) {
//						// トランザクションロールバック
//						TransactionUtil.setRollbackOnly();
//						// DBアクセスエラー
//						Object[] objectgArray = {e.getMessage()};
//						log.fatal(messageAccessor.getMessage(
//												"FZ001011", objectgArray, uvo));
//						throw e;
//					}
//				}
//				/**************************************************
//				 *  WebフィルタリンクDB登録MakeT_UrlFilterLink
//				 **************************************************/
//				// Webフィルタ適用フラグが1(select)の場合のみ登録処理を行う。
//				if(policy.getUrlfilterDefaultFlg() == 1) {
//					log.debug("WebフィルタリンクDB登録");
//					try {
//						updateDAO.execute("MakeT_UrlFilterLink", urlfilterLink);
//					} catch(Exception e) {
//						// DBアクセスエラー
//						Object[] objectgArray = {e.getMessage()};
//						log.fatal(messageAccessor.getMessage(
//												"FZ001011", objectgArray, uvo));
//						throw e;
//					}
//				}
			}
		}
		log.debug("setPolicyInfo処理終了");
	}

	/**
	 * メソッド名 : getXmlResult
	 * 機能概要 : XML-APIを実行し、レスポンスを解析し結果を返却する。
	 * @param xmlAna
	 * @param xmlExe
	 * @param xmlApiConnect XML-APIのURL
	 * @param objName オブジェクト名称
	 * @return List<CODE><</CODE>HashMap<CODE><</CODE>String, Object<CODE>></CODE><CODE>></CODE> XML解析結果
	 * @throws Exception
	 */
// 20121201 kkato@PROSITE mod start
//	private List<HashMap<String, Object>> getXmlResult(
//			CusconXmlApiExecute xmlExe, CusconXMLAnalyze xmlAna, String objConnect, CusconUVO uvo) throws Exception {
	private List<HashMap<String, Object>> getXmlResult(
			CusconXmlApiExecute xmlExe, CusconXMLAnalyze xmlAna, String xmlApiConnect, CusconUVO uvo, String objName
			) throws Exception {
// 20121201 kkato@PROSITE mod end

		log.debug("getXmlResult処理開始");
		 // XML-APIレスポンス
        InputStream objRes;
        // XML解析レスポンス
        List<HashMap<String, Object>> objAnalyse;

// 20121201 kkato@PROSITE add start
	     // アドレスオブジェクト取得用
	     String ADDRESS = "address";
	     // アドレスグループオブジェクト取得用
	     String ADDRESS_GRP = "address-group";
	     // アプリケーションフィルタオブジェクト取得用
	     String APPLI_FIL = "application-filter";
	     // アプリケーショングループオブジェクト取得用
	     String APPLI_GRP = "application-group";
	     // サービスオブジェクト取得用
	     String SERVICE = "service";
	     // サービスグループオブジェクト取得用
	     String SERVICE_GRP = "service-group";
	     // スケジュールオブジェクト取得用
	     String SCHEDULE = "schedule";
	     // ポリシー取得用
	     String POLICY = "rulebase/security/rules";
	     // Webフィルタオブジェクト取得用
	     String URLFILTER = "profiles/url-filtering";
//20121201 kkato@PROSITE afd end

	     // XML-APIにてFW要求を行う。
		// 20121201 kkato@PROSITE mod start
        String objConnect = xmlApiConnect + SLASH + objName;
		objRes = xmlExe.doProc(objConnect, uvo);
		// XML解析を行う
		//objAnalyse = xmlAna.analyzeXML(objRes, uvo);
		if (objName == ADDRESS) {
			objAnalyse = xmlAna.analyzeXML_objects(objRes, uvo, objName);
		} else if (objName == ADDRESS_GRP) {
				objAnalyse = xmlAna.analyzeXML_objects(objRes, uvo, objName);
		} else if (objName == APPLI_FIL) {
			objAnalyse = xmlAna.analyzeXML_objects(objRes, uvo, objName);
		} else if (objName == APPLI_GRP) {
			objAnalyse = xmlAna.analyzeXML_objects(objRes, uvo, objName);
		} else if (objName == SERVICE) {
			objAnalyse = xmlAna.analyzeXML_objects(objRes, uvo, objName);
		} else if (objName == SERVICE_GRP) {
			objAnalyse = xmlAna.analyzeXML_objects(objRes, uvo, objName);
		} else if (objName == POLICY) {
			objAnalyse = xmlAna.analyzeXML_objects(objRes, uvo, objName);
		} else if (objName == URLFILTER) {
			objAnalyse = xmlAna.analyzeXML_objects(objRes, uvo, objName);
		} else {
			objAnalyse = xmlAna.analyzeXML(objRes, uvo);
		}
		// 20121201 kkato@PROSITE mod end

		if(objAnalyse == null) {
			log.debug("XML-APIコマンドが不正:null");
			return null;
		}
		log.debug("getXmlResult処理終了:objAnalyse.size() = " + objAnalyse.size());
		return objAnalyse;
	}
	 /**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor msgAcc){
		log.debug("setMessageAccessor処理開始");
	    setMsgAcc(msgAcc);
	    log.debug("setMessageAccessor処理終了");
	}

	/**
	 * メソッド名 : setMsgAcc
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public static void setMsgAcc(MessageAccessor messageAccessor) {
		log.debug("setMsgAcc処理開始");
		CusconFWWriteFile.messageAccessor = messageAccessor;
		log.debug("setMsgAcc処理終了");
	}

	/**
	 * メソッド名 : getMessage
	 * 機能概要 : messageAccessorをゲットする。
	 * @return MessageAccessor
	 */
	public static MessageAccessor getMessage() {
		log.debug("getMessage処理");
		return messageAccessor;
	}

	/**
	 * メソッド名 : setUrlFilters
	 * 機能概要 : XML-APIによるWebフィルタオブジェクト取得、及びDB登録を行う。
	 * @param updateDAO
	 * @param queryDAO
	 * @param uvo
	 * @param xmlApiConnect XML-API実行URL
	 * @throws Exception
	 */
	private void setUrlFilters(CusconXmlApiExecute xmlExe, CusconXMLAnalyze xmlAna, UpdateDAO updateDAO, QueryDAO queryDAO,
			CusconUVO uvo, String xmlApiConnect, SelectUpdatePA objList) throws Exception {

		log.debug("setUrlFilters処理開始:vsysId = " + uvo.getVsysId() +
								", xmlApiConnect = " + xmlApiConnect);
		// Webフィルタオブジェクト取得用
        String URLFILTER = "profiles/url-filtering";

        // Webフィルタ情報リスト
		List<UrlFilters> urlFilterNameList = new ArrayList<UrlFilters>();

		// Map
		Map.Entry obj = null;
	    // イテレータ
	    Iterator iterator = null;

        // 条件クラス
        InputBase base = new InputBase();

        // XML解析レスポンス
        List<HashMap<String, Object>> objAnalyse;
        // XML-API取得
        String objConnect;

		// 20121201 kkato@PROSITE mod start
		//objConnect = xmlApiConnect + SLASH + URLFILTER;
		// XML解析を行う
		//objAnalyse = getXmlResult(xmlExe, xmlAna, objConnect, uvo);
		objAnalyse = getXmlResult(xmlExe, xmlAna, xmlApiConnect, uvo, URLFILTER);
		// 20121201 kkato@PROSITE mod end

		// Baseの基本登録情報を設定する。
		base.setVsysId(uvo.getVsysId());
		base.setGenerationNo(CusconConst.ZERO_GENE);

		if(objAnalyse != null) {
			log.debug("PAにWebフィルタが存在");
			// XML解析レスポンスのデータ分繰り返す。
			for(int i=0; i<objAnalyse.size(); i++) {

		        // Webフィルタオブジェクトデータクラス
		        UrlFilters urlfilters = new UrlFilters();

		        List<UrlFilterCategories> urlFilterCategoryList = new ArrayList<UrlFilterCategories>();

				// Webフィルタの基本登録情報を設定する。
				urlfilters.setVsysId(uvo.getVsysId());
				urlfilters.setModFlg(CusconConst.NONE_NUM);
				urlfilters.setGenerationNo(CusconConst.ZERO_GENE);

				//WebフィルタオブジェクトとWebフィルタカテゴリのリンク付けの設定
				urlfilters.setOrg_seqno(i+1);

				//WebフィルタオブジェクトとWebフィルタカテゴリのリンク付けの設定
				urlfilters.setBlockListSite(null);
				urlfilters.setBlockListSiteCnt(0);
				urlfilters.setAllowListSite(null);
				urlfilters.setAllowListSiteCnt(0);
				urlfilters.setComment(null);

				log.debug("XMLレスポンス解析:" + i);
				iterator = objAnalyse.get(i).entrySet().iterator();
				// ハッシュマップのデータ分繰り返す。
				while (iterator.hasNext()) {
			        // Webフィルタカテゴリデータクラス
			        UrlFilterCategories urlfiltercategory = new UrlFilterCategories();
					// Webフィルタカテゴリの基本登録情報を設定する。
					urlfiltercategory.setVsysId(uvo.getVsysId());
					urlfiltercategory.setGenerationNo(CusconConst.ZERO_GENE);
					urlfiltercategory.setUrlfiltersSeqno(i+1);

			        log.debug("ハッシュマップ");
					obj = (Map.Entry)iterator.next();
					// オブジェクト名(name)
					if(obj.getKey().equals(CusconConst.NAME)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						urlfilters.setName(obj.getValue().toString());
					}
					// 説明(description)
					if(obj.getKey().equals(CusconConst.DESCRIPTION)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						urlfilters.setComment(obj.getValue().toString());
					}
					// ブロックリストアクション(block-list-site)
					if(obj.getKey().equals(CusconConst.ACTION)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						urlfilters.setActionBlockListSite(obj.getValue().toString());
					}
					// ブロックリスト(block-list-site) 戻り値形式"[yahoo.cp.jp, google.com]"
					if(obj.getKey().equals(CusconConst.BLOCKLIST)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						String str = obj.getValue().toString();
						int num = getStrNum(str, COMMA) + 1;
						urlfilters.setBlockListSiteCnt(num);
						// 先頭、最後の1文字を除く( "[", "]"を除く )
						str = str.substring(1, str.length()-1);
						str = str.replaceAll(", ","\n");
						urlfilters.setBlockListSite(str);
					}
					// 許可リスト(allow-list-site) 戻り値形式"[yahoo.cp.jp, google.com]"
					if(obj.getKey().equals(CusconConst.ALLOWLIST)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						String str = obj.getValue().toString();
						int num = getStrNum(str, COMMA) + 1;
						urlfilters.setAllowListSiteCnt(num);
						// 先頭、最後の1文字を除く( "[", "]"を除く )
						str = str.substring(1, str.length()-1);
						str = str.replaceAll(", ","\n");
						urlfilters.setAllowListSite(str);
					}
					// カテゴリ(allow)
					if(obj.getKey().equals(CusconConst.ALLOW)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						setUrlFilterCategories(updateDAO, queryDAO, uvo, urlfiltercategory, CusconConst.ALLOW, obj.getValue().toString());
//						urlFilterCategoryList.add(urlfiltercategory);
						for(String objs : urlfiltercategory.getLinkNameList()) {
							UrlFilterCategories tmpListItem = new UrlFilterCategories();
							tmpListItem.setAction(urlfiltercategory.getAction()) ;
							tmpListItem.setGenerationNo(urlfiltercategory.getGenerationNo()) ;
							tmpListItem.setName(objs) ;
							urlFilterCategoryList.add(tmpListItem);
						}
					}
					// カテゴリ(alert)
					if(obj.getKey().equals(CusconConst.ALERT)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						setUrlFilterCategories(updateDAO, queryDAO, uvo, urlfiltercategory, CusconConst.ALERT, obj.getValue().toString());
//						urlFilterCategoryList.add(urlfiltercategory);
						for(String objs : urlfiltercategory.getLinkNameList()) {
							UrlFilterCategories tmpListItem = new UrlFilterCategories();
							tmpListItem.setAction(urlfiltercategory.getAction()) ;
							tmpListItem.setGenerationNo(urlfiltercategory.getGenerationNo()) ;
							tmpListItem.setName(objs) ;
							urlFilterCategoryList.add(tmpListItem);
						}
					}
					// カテゴリ(block)
					if(obj.getKey().equals(CusconConst.BLOCK)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						setUrlFilterCategories(updateDAO, queryDAO, uvo, urlfiltercategory, CusconConst.BLOCK, obj.getValue().toString());
//						urlFilterCategoryList.add(urlfiltercategory);
						for(String objs : urlfiltercategory.getLinkNameList()) {
							UrlFilterCategories tmpListItem = new UrlFilterCategories();
							tmpListItem.setAction(urlfiltercategory.getAction()) ;
							tmpListItem.setGenerationNo(urlfiltercategory.getGenerationNo()) ;
							tmpListItem.setName(objs) ;
							urlFilterCategoryList.add(tmpListItem);
						}
					}
					// カテゴリ(continue)
					if(obj.getKey().equals(CusconConst.CONTINUE)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						setUrlFilterCategories(updateDAO, queryDAO, uvo, urlfiltercategory, CusconConst.CONTINUE, obj.getValue().toString());
//						urlFilterCategoryList.add(urlfiltercategory);
						for(String objs : urlfiltercategory.getLinkNameList()) {
							UrlFilterCategories tmpListItem = new UrlFilterCategories();
							tmpListItem.setAction(urlfiltercategory.getAction()) ;
							tmpListItem.setGenerationNo(urlfiltercategory.getGenerationNo()) ;
							tmpListItem.setName(objs) ;
							urlFilterCategoryList.add(tmpListItem);
						}
					}
					// カテゴリ(override)
					if(obj.getKey().equals(CusconConst.OVERRIDE)) {
						log.debug("key:" + obj.getKey() + ", value:" + obj.getValue());
						setUrlFilterCategories(updateDAO, queryDAO, uvo, urlfiltercategory, CusconConst.OVERRIDE, obj.getValue().toString());
//						urlFilterCategoryList.add(urlfiltercategory);
						for(String objs : urlfiltercategory.getLinkNameList()) {
							UrlFilterCategories tmpListItem = new UrlFilterCategories();
							tmpListItem.setAction(urlfiltercategory.getAction()) ;
							tmpListItem.setGenerationNo(urlfiltercategory.getGenerationNo()) ;
							tmpListItem.setName(objs) ;
							urlFilterCategoryList.add(tmpListItem);
						}
					// キーがname以外の場合
					}
				}
				urlFilterNameList.add(urlfilters);
				urlFilterNameList.get(i).setUrlfiltercategories(urlFilterCategoryList);
//				for (int j = 0; j < urlFilterCategoryList.get(i).size(); j++) {
//					List<UrlFilterCategories> tmpLinkNameList =  (List<UrlFilterCategories>)urlFilterCategoryList.get(i);
//					for(String objs : tmpLinkNameList) {
//						LinkInfo tmpListItem = new LinkInfo();
//						tmpListItem.setName(objLinkList.get(j).getName()) ;
//						tmpListItem.setLinkName(objs) ;
//						appGrpList.add(tmpListItem);
//					}
//				}



//				try {
//					// データをDBに登録する。
//					log.debug("Webフィルタオブジェクトを作成");
//					updateDAO.execute("MakeT_UrlFilters", urlfilters);
//					log.debug("WebフィルタカテゴリのURLFILTER_SEQNOの更新");
//					updateDAO.execute("UpdateT_UrlFilter_Categories_UrlFiltersSeqNo", base);
//					log.debug("WebフィルタオブジェクトのORG_SEQNOに0をセット");
//					updateDAO.execute("UpdateT_UrlFilters_OrgSeqNo", base);
//				} catch (Exception e) {
//					// DBアクセスエラー
//					Object[] objectgArray = {e.getMessage()};
//					log.fatal(messageAccessor.getMessage("FZ001012", objectgArray, uvo));
//					throw e;
//				}
			}
		}
		// Webフィルタ情報リストを追加する。
		objList.setUrlFilters(urlFilterNameList);
		log.debug("setUrlFilters処理終了");
	}

	/**
	 * メソッド名 : setUrlFilterCategories
	 * 機能概要 : XML-APIによるWebフィルタカテゴリDB登録を行う。
	 * @param updateDAO
	 * @param queryDAO
	 * @param uvo
	 * @param urlfiltercategoryクラス
	 * @param action(action)
	 * @param catetories(カテゴリ名)
	 * @throws Exception
	 */
	private void setUrlFilterCategories(UpdateDAO updateDAO, QueryDAO queryDAO, CusconUVO uvo,
			UrlFilterCategories urlfiltercategory, String action, String categories) throws Exception {

		log.debug("setUrlFilterCategories処理開始:vsysId = " + uvo.getVsysId() +
								", action = " + action +
								", categories = " + categories);
		try {
			// 先頭、最後の1文字を除く( "[", "]"を除く )
			String str = categories.substring(1, categories.length()-1);
			String[] ctgr = str.split(", ", -1);

			urlfiltercategory.setLinkNameList(ctgr);
			if(str != null) {
				log.debug("Webフィルタカテゴリを作成:");
				urlfiltercategory.setAction(action);
//				updateDAO.execute("MakeT_UrlFilter_Categories", urlfiltercategory);
			}
		} catch (Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FZ001013", objectgArray, uvo));
			throw e;
		}
		log.debug("setUrlFilterCategories処理終了");
	}

	/**
	 * メソッド名 : getStrNum
	 * 機能概要 : 対象文字列内に検索文字列が何個含まれているか
	 * @param orgstr	: 検索対象文字列
	 * @param searchstr	: 検索文字列
	 * @throws Exception
	 */
	private int getStrNum(String orgstr, String searchstr) throws Exception {
		int cnt = 0;
		int targetSize = 0;
		int replacedSize = 0;

		try{
			targetSize = orgstr.length();
			replacedSize = orgstr.replace(searchstr,"").length();
			cnt = (targetSize-replacedSize)/searchstr.length();
		}catch(Exception e){
			throw e;
		}
		return cnt;
	}

	/**
	 * メソッド名 : registPA
	 * 機能概要 : objList、policyListの情報をPAに設定する。
	 * @param objList 設定対象オブジェクト情報リスト
	 * @param policyList 設定対象ポリシー情報リスト
	 * @throws Exception
	 */
	private boolean registPA(SelectUpdatePA objList,
					List<SelectPolicyList> policyList,
					String tmpFile, CusconUVO uvo) throws Exception {

		log.debug("registPA処理開始:policyList.size() = " + policyList.size());
		// カテゴリコマンド
		String CATEGORY = " category ";
		// サブカテゴリコマンド
		String SUBCATEGORY = " subcategory ";
		// テクノロジコマンド
		String TECHNOLOGY = " technology ";
		// 脅威度コマンド
		String RISK = " risk ";
		// プロトコルコマンド
		String PROTOCOL = " protocol ";
		// ポートコマンド
		String PORT = " port ";
		// dailyコマンド
		String DAILY = " recurring daily ";
		// weeklyコマンド
		String WEEKLY = " recurring weekly ";
		// non-recurringコマンド
		String NON_RECURRING = " non-recurring ";
		// sundayコマンド
		String SUNDAY = " sunday ";
		// mondayコマンド
		String MONDAY = " monday ";
		// tuesdayコマンド
		String TUESDAY = " tuesday ";
		// wednesdayコマンド
		String WEDNESDAY = " wednesday ";
		// thursdayコマンド
		String THURSDAY = " thursday ";
		// fridayコマンド
		String FRIDAY = " friday ";
		// saturdayコマンド
		String SATURDAY = " saturday ";
		// -コマンド
		String HYPHEN = "-";
		// @コマンド
		String AT = "@";
		// セキュリティポリシーコマンド
		String SECURITY = " rulebase security rules ";
		// fromコマンド
		String FROM = " from ";
		// toコマンド
		String TO = " to ";
		// sourceコマンド
		String SOURCE = " source ";
		// destinationコマンド
		String DESTINATION = " destination ";
		// applicationコマンド
		String APPLICATION = " application ";
		// actionコマンド
		String ACTION = " action ";
		// profile-settingコマンド
		String PROFILE = " profile-setting profiles vulnerability ";
//		// 脆弱性の保護フラグ
//		String DEFAULT = "default";
		// スケジュール
		String NONE = "none";
		// ログエンド
		String LOG_END = " log-end yes ";
		// ログフォワード設定
		String LOG_SETTING = " log-setting ";
		// Action_Allow
		String ACTION_ALLOW = "allow";
		// Action_Deny
		String ACTION_DENY = "deny";
		String URLFILTER_PROFILE = " profile-setting profiles url-filtering ";
		String VIRUS_PROFILE = " profile-setting profiles virus ";
		String SPYWARE_PROFILE = " profile-setting profiles spyware ";
		String ALLOW_LIST = " allow-list ";
		String BLOCK_LIST = " block-list ";

		// 設定コマンド生成用
		String setComConnect;
		// コマンドリスト
		List<String> comList;
		// アドレスタイプ
		String type;
		// コマンドレスポンス
		String response;
		Boolean bRst;

		// コマンドリスト
		comList = new ArrayList<String>();
		 // コンフィグモードコマンド設定
		comList.add(CONFIG_COM);
		// コマンドを実行し、レスポンスを取得する。
		response = getCommandResponce(tmpFile, comList);

		/**************************************************
		 *	アドレスオブジェクト情報リスト
		 **************************************************/
		List<Addresses> addressList = objList.getAddress();

		// アドレスオブジェクト設定処理
		for(int i=0; i<addressList.size(); i++) {

			log.debug("アドレス設定:" + i);
			// コマンドリスト
			comList = new ArrayList<String>();
//			 // コンフィグモードコマンド設定
//			comList.add(CONFIG_COM);

			// アドレス種別により設定するコマンドを判定する。
			if(addressList.get(i).getType().equals(CusconConst.ADDRESS_IP)){
				log.debug("アドレス種別:IPアドレス");
				type = CusconConst.ADDRESS_IP;
			// UPDATED: 2016.12 by Plum Systems Inc.
			// FQDN対応追加 -->
			} else if(addressList.get(i).getType().equals(CusconConst.ADDRESS_FQDN)){
				log.debug("アドレス種別:FQDN");
				type = CusconConst.ADDRESS_FQDN;
			// --> FQDN対応追加
			} else {
				log.debug("アドレス種別:範囲");
				type = CusconConst.ADDRESS_RANGE;
			}
			// 設定対象オブジェクトの設定
			setComConnect = SET_COM + ADDRESS + QUOT +
				addressList.get(i).getName() + QUOT + SP  + type  + SP + QUOT +
						addressList.get(i).getAddress() + QUOT + CHANGE_LINE;
			comList.add(setComConnect);

			// 設定コマンドログ出力
			Object[] objectgArray = {setComConnect};
			log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));

			// コマンドを実行し、レスポンスを取得する。
			response = getCommandResponce(tmpFile, comList);
		}

		/**************************************************
		 *	アドレスグループオブジェクト情報リスト
		 **************************************************/
		//List<LinkInfo> addressGrpList = objList.getAddressGroups();
		List<LinkInfo> addressGrpList = objList.getAddressGroups();
		// アドレスグループオブジェクト設定処理
		bRst = setGroupOject(addressGrpList, tmpFile, uvo, 1);

		/**************************************************
		 *	アプリケーションフィルタオブジェクト情報リスト
		 **************************************************/
		List<FilterInfo> appliFilList = objList.getApplicationFilters();

		// アプリケーションフィルタオブジェクト設定処理
		for(int i=0; i<appliFilList.size(); i++) {
			log.debug("アプリケーションフィルタ設定:" + i);
			// コマンドリスト
			comList = new ArrayList<String>();
//			 // コンフィグモードコマンド設定
//			comList.add(CONFIG_COM);

			// 設定対象オブジェクトの設定
			setComConnect = SET_COM + APPLI_FIL + QUOT +
									appliFilList.get(i).getName() + QUOT;

			// カテゴリが存在する場合
			if(appliFilList.get(i).getCategory() != null) {
				log.debug("カテゴリ存在");
				setComConnect = setComConnect + CATEGORY + QUOT +
								appliFilList.get(i).getCategory() + QUOT;
			}
			// サブカテゴリが存在する場合
			if(appliFilList.get(i).getSubCategory() != null) {
				log.debug("サブカテゴリ存在");
				setComConnect = setComConnect + SUBCATEGORY + QUOT +
								appliFilList.get(i).getSubCategory() + QUOT;
			}
			// テクノロジが存在する場合
			if(appliFilList.get(i).getTechnology() != null) {
				log.debug("テクノロジ存在");
				setComConnect = setComConnect + TECHNOLOGY + QUOT +
								appliFilList.get(i).getTechnology() + QUOT;
			}
			// リスクが存在する場合
			if(appliFilList.get(i).getRisk() != 0) {
				log.debug("リスク存在");
				setComConnect = setComConnect + RISK + QUOT +
								appliFilList.get(i).getRisk() + QUOT;
			}
			// 最後に改行コードを付加する。
			setComConnect = setComConnect + CHANGE_LINE;

			comList.add(setComConnect);

			// 設定コマンドログ出力
			Object[] objectgArray = {setComConnect};
			log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));

			// コマンドを実行し、設定する。
			response = getCommandResponce(tmpFile, comList);
		}

		/**************************************************
		 *	アプリケーショングループオブジェクト情報リスト
		 **************************************************/
		List<LinkInfo> appliGrpList = objList.getApplicationGroups();
		// アドレスグループオブジェクト設定処理
		bRst=setGroupOject(appliGrpList, tmpFile, uvo, 2);
		if (!bRst){
			return false;
		}

		/**************************************************
		 *	サービスオブジェクト情報リスト
		 **************************************************/
		List<Services> serviceList = objList.getServices();

		// サービスオブジェクト設定処理
		for(int i=0; i<serviceList.size(); i++) {
			log.debug("サービス設定:" + i);
			// コマンドリスト
			comList = new ArrayList<String>();
//			 // コンフィグモードコマンド設定
//			comList.add(CONFIG_COM);

			// 設定対象オブジェクトの設定
			setComConnect = SET_COM + SERVICE + QUOT +
				serviceList.get(i).getName() + QUOT + PROTOCOL + QUOT +
					serviceList.get(i).getProtocol() + QUOT + PORT + QUOT +
						serviceList.get(i).getPort() + QUOT + CHANGE_LINE;
			comList.add(setComConnect);

			// 設定コマンドログ出力
			Object[] objectgArray = {setComConnect};
			log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));

			// コマンドを実行し、設定する。
			response = getCommandResponce(tmpFile, comList);
		}

		/**************************************************
		 *	サービスグループオブジェクト情報リスト
		 **************************************************/
		List<LinkInfo> srvGrpList = objList.getServiceGroups();
		// サービスグループオブジェクト設定処理
		bRst = setGroupOject(srvGrpList, tmpFile, uvo, 3);

//		/**************************************************
//		 *	スケジュールオブジェクト情報リスト
//		 **************************************************/
//		List<Schedules> scheduleList = objList.getSchedules();
//		List<Schedules> tmpScheduleList = new ArrayList<Schedules>();
//		Schedules schedule = new Schedules();
//		int count = 0;
//		int recurrence = 0;
//
//		if(scheduleList.size() != 0) {
//			log.debug("スケジュールが0件でない場合:");
//			recurrence = scheduleList.get(0).getRecurrence();
//		}
//		// スケジュール(デイリー、指定日時)の同一名称の件数をカウントする）
//		for(int i=0; i<scheduleList.size(); i++) {
//			log.debug("同一名称の件数カウント処理:" + i);
//			// ウィークリーでない場合
//			if(scheduleList.get(i).getRecurrence() != 1) {
//				log.debug("ウィークリー以外");
//				recurrence = scheduleList.get(i).getRecurrence();
//				// 名前が一致
//				if(i != scheduleList.size() - 1 &&
//						scheduleList.get(i).getName().equals(
//									scheduleList.get(i+1).getName())) {
//					count++;
//					log.debug("名称一致:" + count);
//				} else {
//					log.debug("名称不一致:" + scheduleList.get(i).getName() + ", " +
//												recurrence + ", " + count);
//					count++;
//					// スケジュール名を格納する。
//					schedule.setName(scheduleList.get(i).getName());
//					// 周期を格納
//					schedule.setRecurrence(recurrence);
//					// 件数を格納する。
//					schedule.setSeqNo(count);
//					tmpScheduleList.add(schedule);
//					schedule = new Schedules();
//
//					count = 0;
//				}
//			}
//		}
//		int tmpNum = 0;
//		// スケジュールオブジェクト設定処理
//		for(int i=0; i<scheduleList.size(); i++) {
//			log.debug("スケジュール設定:" + i);
//			// コマンドリスト
//			comList = new ArrayList<String>();
//			 // コンフィグモードコマンド設定
//			comList.add(CONFIG_COM);
//			String setScheName = "";
//			// 設定対象オブジェクトの設定
//			setComConnect = SET_COM + SCHEDULE + QUOT +
//										scheduleList.get(i).getName() + QUOT;
//			// Dailyの場合
//			if(scheduleList.get(i).getRecurrence() == CusconConst.DAILY) {
//				log.debug("Dailyの場合");
//
//				for(int j=0 ; j<tmpScheduleList.size(); j++) {
//					log.debug("tmp分繰り返す。:" + j);
//
//					if(tmpScheduleList.get(j).getRecurrence() == 0) {
//						log.debug("デイリー");
//						if(scheduleList.get(i).getName().equals(
//											tmpScheduleList.get(j).getName())) {
//							log.debug("名称一致:" + scheduleList.get(i).getName());
//							// 同一名称件数分繰り返す。
//							for(int k=i; k<i+tmpScheduleList.get(j).getSeqNo(); k++) {
//								log.debug("時間件数:" + k);
//								// オブジェクトの1つ目の時間
//								if(k==i) {
//									log.debug("時間結合1回目");
//									setScheName =  "[ " + scheduleList.get(k).getStartTime() + "-" +
//											scheduleList.get(k).getEndTime() + " ";
//								} else {
//									log.debug("時間結合" + (k+1) + "回目");
//									setScheName =  setScheName + scheduleList.get(k).getStartTime() + "-" +
//											scheduleList.get(k).getEndTime() + " ";
//								}
//
//								// 最後の時間の場合、閉じ括弧をつける。
//								if(k+1==i+tmpScheduleList.get(j).getSeqNo()) {
//									log.debug("閉じ括弧付加:" + k);
//									setScheName = setScheName + "]";
//									tmpNum = tmpScheduleList.get(j).getSeqNo() - 1;
//								}
//							}
//						}
//					}
//				}
//				// scheduleListのインデックスに同一名称件数をプラスする。
//				i = i + tmpNum;
//
//				setComConnect = setComConnect + DAILY + setScheName;
//
//			// Weeklyの場合
//			} else if(scheduleList.get(i).getRecurrence() ==
//													CusconConst.WEEKLY) {
//				log.debug("Weeklyの場合");
//				setComConnect = setComConnect + WEEKLY;
//
//				// 日曜の場合
//				if(scheduleList.get(i).getDayOfWeek() == CusconConst.SUN) {
//					log.debug("日曜の場合");
//					setComConnect = setComConnect + SUNDAY;
//				// 月曜の場合
//				} else if(scheduleList.get(i).getDayOfWeek() ==
//														CusconConst.MON) {
//					log.debug("月曜の場合");
//					setComConnect = setComConnect + MONDAY;
//				// 火曜の場合
//				} else if(scheduleList.get(i).getDayOfWeek() ==
//														CusconConst.TUE) {
//					log.debug("火曜の場合");
//					setComConnect = setComConnect + TUESDAY;
//				// 水曜の場合
//				} else if(scheduleList.get(i).getDayOfWeek() ==
//														CusconConst.WED) {
//					log.debug("水曜の場合");
//					setComConnect = setComConnect + WEDNESDAY;
//				// 木曜の場合
//				} else if(scheduleList.get(i).getDayOfWeek() ==
//														CusconConst.THU) {
//					log.debug("木曜の場合");
//					setComConnect = setComConnect + THURSDAY;
//				// 金曜の場合
//				} else if(scheduleList.get(i).getDayOfWeek() ==
//														CusconConst.FRI) {
//					log.debug("金曜の場合");
//					setComConnect = setComConnect + FRIDAY;
//				// 土曜の場合
//				} else {
//					log.debug("土曜の場合");
//					setComConnect = setComConnect + SATURDAY;
//				}
//
//				setComConnect = setComConnect +
//					scheduleList.get(i).getStartTime() + HYPHEN  +
//										scheduleList.get(i).getEndTime();
//
//			// Non-Recurringの場合
//			} else {
//				log.debug("Non-Recurring");
//
//				for(int j=0 ; j<tmpScheduleList.size(); j++) {
//					log.debug("tmp分繰り返す。:" + j);
//
//					if(tmpScheduleList.get(j).getRecurrence() == 2) {
//						log.debug("指定日時");
//						if(scheduleList.get(i).getName().equals(
//											tmpScheduleList.get(j).getName())) {
//							log.debug("名称一致:" + scheduleList.get(i).getName());
//							// 同一名称件数分繰り返す。
//							for(int k=i; k<i+tmpScheduleList.get(j).getSeqNo(); k++) {
//								log.debug("時間件数:" + k);
//								// オブジェクトの1つ目の時間
//								if(k==i) {
//									log.debug("時間結合1回目");
//									setScheName =  "[ " + scheduleList.get(k).getStartDate() +
//									AT + scheduleList.get(k).getStartTime() + HYPHEN +
//									scheduleList.get(k).getEndDate() + AT + scheduleList.get(k).getEndTime() + " ";
//								} else {
//									log.debug("時間結合" + (k+1) + "回目");
//									setScheName =  setScheName + scheduleList.get(k).getStartDate() +
//									AT + scheduleList.get(k).getStartTime() + HYPHEN +
//									scheduleList.get(k).getEndDate() + AT + scheduleList.get(k).getEndTime() + " ";
//								}
//
//								// 最後の時間の場合、閉じ括弧をつける。
//								if(k+1==i+tmpScheduleList.get(j).getSeqNo()) {
//									log.debug("閉じ括弧付加:" + k);
//									setScheName = setScheName + "]";
//									tmpNum = tmpScheduleList.get(j).getSeqNo() - 1;
//								}
//							}
//						}
//					}
//				}
//				// scheduleListのインデックスに同一名称件数をプラスする。
//				i = i + tmpNum;
//				setComConnect = setComConnect + NON_RECURRING + setScheName;
//			}
//
//			// 最後に改行コードを付加する。
//			setComConnect = setComConnect + CHANGE_LINE;
//
//			comList.add(setComConnect);
//
//			// 設定コマンドログ出力
//			Object[] objectgArray = {setComConnect};
//			log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));
//
//			// コマンドを実行し、設定する。
//			//command.getCommandResponce(compara, comList);
//			response = command.getCommandResponce(compara, comList);
//			// コマンドが失敗の場合( invalid or unknown or server error)
//			if(response.indexOf(COMMAND_INVALID) != -1 ||
//				response.indexOf(COMMAND_UNKNOWN) != -1 ||
//				response.indexOf(COMMAND_SERVER_ERROR) != -1) {
//				Object[] objectRes = {response};
//				log.fatal(messageAccessor.getMessage("FZ011041", objectRes, uvo));
//				return false;
//			}
//		}

		/**************************************************
		 *	Webフィルタオブジェクト情報リスト
		 **************************************************/
		if( uvo.getUrlFilteringFlg()==1 ){
			List<UrlFilters> urlfilterList = objList.getUrlFilters();
			// Webフィルタオブジェクト設定処理
			for(int i=0; i<urlfilterList.size(); i++) {
				log.debug("Webフィルタ設定:" + i);
				// コマンドリスト
				comList = new ArrayList<String>();

//				// コンフィグモードコマンド設定
//				comList.add(CONFIG_COM);

				// 設定対象オブジェクトの設定
				String commonSet = SET_COM  + URLFILTER + QUOT + urlfilterList.get(i).getName() + QUOT;
				setComConnect = commonSet;

				// description
				if( urlfilterList.get(i).getComment() != null){
					if( urlfilterList.get(i).getComment().length() != 0 ){
						setComConnect = setComConnect + DESCRIPTION  + QUOT + urlfilterList.get(i).getComment() + QUOT;
					}
				}
				// Action on License Expiration(プロパティファイルの値を設定）未設定時はAllow
			    String license = PropertyUtil.getProperty("fwsetting.common.urlfilter_action_on_license_expiration");
			    if(license==null){
			    	license=URLFILTER_ACTION_ALLOW;
			    }
				setComConnect = setComConnect + URLFILTER_ACTION_ON_LiCENSE_EXPIRATION + license;

				// Dynamic URL(プロパティファイルの値を設定）
			    String dynamic = PropertyUtil.getProperty("fwsetting.common.urlfilter_dynamic-url");
				setComConnect = setComConnect + URLFILTER_DYNAMIC_URL + dynamic;

				// action block site list
				setComConnect = setComConnect + ACTION + urlfilterList.get(i).getActionBlockListSite();
				setComConnect += CHANGE_LINE;
				comList.add(setComConnect);
				//ここまでで1つ

				//block list site
				String sendbuf = "";
				String CRLF = "\n";
			    String proLimitlength = PropertyUtil.getProperty("fwsetting.common.urlfilter_list_max_buffer");
			    Integer lim = Integer.parseInt(proLimitlength);
			    lim = lim - commonSet.length() - BLOCK_LIST.length() - 4;

				// block list site
				String blockListSite = urlfilterList.get(i).getBlockListSite();
				if(blockListSite != null){
					if( blockListSite.length() != 0 ){
						setComConnect = "";
					    String[] blockListSiteArray = blockListSite.split(CRLF, 0);
						for (int j = 0; j < blockListSiteArray.length; j++) {
							String lists = blockListSiteArray[j].replaceAll("\r", "");
							lists = QUOT + lists + QUOT;
							if( lim < (setComConnect.length() + lists.length()) ){
								// 最後の" "をカット
								setComConnect = setComConnect.substring(0, setComConnect.length()-1);
								sendbuf = commonSet + BLOCK_LIST + "[ " + setComConnect + " ]" + CHANGE_LINE;
								comList.add(sendbuf);
								setComConnect = "";
							}
							setComConnect = setComConnect + lists +  " ";
						}
						// 最後の" "をカット
						setComConnect = setComConnect.substring(0, setComConnect.length()-1);
						sendbuf = commonSet + BLOCK_LIST + "[ " + setComConnect + " ]" + CHANGE_LINE;
						comList.add(sendbuf);
					}
				}

				// allow list site
				String allowListSite = urlfilterList.get(i).getAllowListSite();
				if(allowListSite != null){
					if( allowListSite.length() != 0 ){
						setComConnect = "";
					    String[] allowListSiteArray = allowListSite.split(CRLF, 0);
					    for (int j = 0; j < allowListSiteArray.length; j++) {
					    	String lists = allowListSiteArray[j].replaceAll("\r", "");
						    lists = QUOT + lists + QUOT;
							if( lim < (setComConnect.length() + lists.length()) ){
								// 最後の" "をカット
								setComConnect = setComConnect.substring(0, setComConnect.length()-1);
								sendbuf = commonSet + ALLOW_LIST + "[ " + setComConnect + " ]" + CHANGE_LINE;
								comList.add(sendbuf);
								setComConnect = "";
							}
							setComConnect = setComConnect + lists +  " ";
						}
						// をカット
						setComConnect = setComConnect.substring(0, setComConnect.length()-1);
						sendbuf = commonSet +  ALLOW_LIST + "[ " + setComConnect + " ]" + CHANGE_LINE;
						comList.add(sendbuf);
					}
				}

				// URLフィルタカテゴリの設定
				String alertctgr = "";
				String allowctgr = "";
				String blockctgr = "";
				String contctgr = "";
				String overctgr = "";
				List<UrlFilterCategories> urlFilterCategoryList = urlfilterList.get(i).getUrlfiltercategories();
				for (int j = 0; j < urlFilterCategoryList.size(); j++) {
					if( urlFilterCategoryList.get(j).getAction().equalsIgnoreCase(URLFILTER_ACTION_ALERT)){
						alertctgr = alertctgr + QUOT + urlFilterCategoryList.get(j).getName() + QUOT + " ";
					}
					if( urlFilterCategoryList.get(j).getAction().equalsIgnoreCase(URLFILTER_ACTION_ALLOW)){
						allowctgr = allowctgr + QUOT + urlFilterCategoryList.get(j).getName() + QUOT + " ";
					}
					if( urlFilterCategoryList.get(j).getAction().equalsIgnoreCase(URLFILTER_ACTION_BLOCK)){
						blockctgr = blockctgr + QUOT + urlFilterCategoryList.get(j).getName() + QUOT + " ";
					}
					if( urlFilterCategoryList.get(j).getAction().equalsIgnoreCase(URLFILTER_ACTION_CONTINUE)){
						contctgr = contctgr + QUOT + urlFilterCategoryList.get(j).getName() + QUOT + " ";
					}
					if( urlFilterCategoryList.get(j).getAction().equalsIgnoreCase(URLFILTER_ACTION_OVERRIDE)){
						overctgr = overctgr + QUOT + urlFilterCategoryList.get(j).getName() + QUOT + " ";
					}
				}
				setComConnect = "";
				if( alertctgr.length() != 0 || allowctgr.length() != 0 ||
					blockctgr.length() != 0 || contctgr.length() != 0 || overctgr.length() != 0){
					setComConnect = commonSet;
				}
				if (alertctgr.length() != 0 ){
					setComConnect = setComConnect + SP  + URLFILTER_ACTION_ALERT  + " [ " + alertctgr.substring(0, alertctgr.length()-1) + " ]";
				}
				if (allowctgr.length() != 0 ){
					setComConnect = setComConnect + SP  + URLFILTER_ACTION_ALLOW  + " [ " + allowctgr.substring(0, allowctgr.length()-1) + " ]";
				}
				if (blockctgr.length() != 0 ){
					setComConnect = setComConnect + SP  + URLFILTER_ACTION_BLOCK  + " [ " + blockctgr.substring(0, blockctgr.length()-1) + " ]";
				}
				if (contctgr.length() != 0 ){
					setComConnect = setComConnect + SP + URLFILTER_ACTION_CONTINUE  + " [ " + contctgr.substring(0, contctgr.length()-1) + " ]";
				}
				if (overctgr.length() != 0 ){
					setComConnect = setComConnect + SP + URLFILTER_ACTION_OVERRIDE  + " [ " + overctgr.substring(0, overctgr.length()-1) + " ]";
				}
				if( setComConnect.length() != 0  ){
					setComConnect += CHANGE_LINE;
					comList.add(setComConnect);
				}
				// 設定コマンドログ出力
				for (int j = 0; j < comList.size(); j++) {
					Object[] objectgArray = {comList.get(j)};
					log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));
				}
				// コマンドを実行し、設定する。
				response = getCommandResponce(tmpFile, comList);
			}
		}

		/*********************************
		 *	ポリシー設定処理
		 *********************************/
		// 出所ゾーン名リスト
		List<SelectObjectList> srcZoneList;
		// 宛先ゾーン名リスト
		List<SelectObjectList> dstZoneList;
		// 出所アドレス名リスト
		List<SelectObjectList> srcAddressList;
		// 宛先アドレス名リスト
		List<SelectObjectList> dstAddressList;
		// アプリケーション名リスト
		List<SelectObjectList> appliList;
		// サービス名リスト
		List<SelectObjectList> srvList;

		// ポリシー設定処理
		for(int i=0; i<policyList.size(); i++) {
			log.debug("ポリシー設定:" + i);
			// コマンド一時生成用
			String tmpSetCom;
			// コマンドリスト
			comList = new ArrayList<String>();
//			 // コンフィグモードコマンド設定
//			comList.add(CONFIG_COM);

			srcZoneList = policyList.get(i).getSourceZoneList();
			dstZoneList = policyList.get(i).getDestinationZoneList();
			srcAddressList = policyList.get(i).getSourceAddressList();
			dstAddressList = policyList.get(i).getDestinationAddressList();
			appliList = policyList.get(i).getApplicationList();
			srvList = policyList.get(i).getServiceList();

			// 設定対象オブジェクトの設定
			tmpSetCom = SET_COM + SECURITY + QUOT + policyList.get(i).getName() + QUOT;
			log.debug("* tmpSetCom(設定対象オブジェクトの設定) : " + tmpSetCom);

			// 設定コマンド1回目
			setComConnect = tmpSetCom + FROM + QUOT + srcZoneList.get(0).getName() + QUOT +
				TO + QUOT + dstZoneList.get(0).getName() + QUOT  +
				SOURCE + QUOT + srcAddressList.get(0).getName() + QUOT +
				DESTINATION + QUOT + dstAddressList.get(0).getName() + QUOT +
				APPLICATION + QUOT + appliList.get(0).getName() + QUOT +
				SERVICE + QUOT + srvList.get(0).getName() + QUOT +
				ACTION + QUOT + policyList.get(i).getAction() + QUOT;

			// 備考は設定がある場合のみ、コマンド発行
			if(policyList.get(i).getDescription() != null) {
				log.debug("備考設定付加");
				setComConnect = setComConnect + DESCRIPTION + QUOT + policyList.get(i).getDescription() + QUOT;
				log.debug("* setComConnect(備考設定) : " + setComConnect);
			}

//			// スケジュールはnoneでない場合のみ、コマンド発行
//			if(!policyList.get(i).getSchedule().equals(NONE)) {
//				log.debug("スケジュール設定付加");
//				setComConnect = setComConnect + SCHEDULE + QUOT + policyList.get(i).getSchedule() + QUOT;
//			}

			// LogFowardProfileを設定する
			if(policyList.get(i).getAction().equals(ACTION_ALLOW)) {
				log.debug("AllowLogForwardingProfile設定付加");
				setComConnect = setComConnect + LOG_SETTING + ALLOW_LOG_FORWARD_PROFILE;
				log.debug("* setComConnect(LogFowardProfile) : " + setComConnect);
			}
			else if(policyList.get(i).getAction().equals(ACTION_DENY)) {
				log.debug("DenyLogForwardingProfile設定付加");
				setComConnect = setComConnect + LOG_SETTING + DENY_LOG_FORWARD_PROFILE;
				log.debug("* setComConnect(LogFowardProfile) : " + setComConnect);
			}

			// ログエンドコマンドを設定する。
			setComConnect = setComConnect + LOG_END;

			// 脆弱性の保護フラグはdefaultの場合のみ、コマンド発行
			if(policyList.get(i).getIds_ips().equals(DEFAULT)) {
				log.debug("脆弱性の保護フラグ設定付加");
				setComConnect = setComConnect + PROFILE + QUOT + policyList.get(i).getIds_ips() + QUOT + CHANGE_LINE;
				log.debug("* setComConnect : " + setComConnect);
			} else {
				// 最後に改行コードを付加する。
				setComConnect = setComConnect + CHANGE_LINE;
				log.debug("* setComConnect(脆弱性の保護フラグ) : " + setComConnect);
			}

			// コマンド実行
			comList.add(setComConnect);

			// WebフィルタがONの時に限り、profileを設定
			if( uvo.getUrlFilteringFlg()==1 ){
				log.debug("profile-setting urlfilter設定付加");
				// urlfilter-profile
				if(policyList.get(i).getUrlfilterDefaultFlg()==1){
					setComConnect = tmpSetCom + URLFILTER_PROFILE + policyList.get(i).getUrlFilterName();
					setComConnect = setComConnect + CHANGE_LINE;
					log.debug("* setComConnect(profile-setting urlfilter設定) : " + setComConnect);
					// コマンド実行
					comList.add(setComConnect);
				}
			}
			// virusプロファイルを設定する
			if(policyList.get(i).getVirusCheckFlg() == "1") {
				log.debug("profile-setting virus設定付加");
				// virus-profile
				String profileVirus = PropertyUtil.getProperty("fwsetting.common.profile_virus");
				if(profileVirus == null){
					log.debug("ApplicationResurces.propertiesにfwsetting.common.profile_virus未設定");
				}else{
					setComConnect = tmpSetCom + VIRUS_PROFILE + profileVirus;
					setComConnect = setComConnect + CHANGE_LINE;
					log.debug("* setComConnect(virusプロファイル) : " + setComConnect);
					// コマンド実行
					comList.add(setComConnect);
				}
			}
			// spywareプロファイルを設定する
			if(policyList.get(i).getSpywareFlg() == "1") {
				String profileSpyware = PropertyUtil.getProperty("fwsetting.common.profile_spyware");
				if(profileSpyware == null){
					log.debug("ApplicationResurces.propertiesにfwsetting.common.profile_spyware未設定");
				}else{
					log.debug("profile-setting spyware設定付加");
					setComConnect = tmpSetCom + SPYWARE_PROFILE + profileSpyware;
					setComConnect = setComConnect + CHANGE_LINE;
					log.debug("* setComConnect(spywareプロファイル) : " + setComConnect);
					// コマンド実行
					comList.add(setComConnect);
				}
			}
//			// 設定コマンドログ出力
//			Object[] objectgArray = {setComConnect};
//			log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));
			for(int j=0; j<comList.size(); j++) {
				// 設定コマンドログ出力
				Object[] objectgArray = {comList.get(j)};
				log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));
			}
			// コマンドを実行し、設定する。
			response = getCommandResponce(tmpFile, comList);

			// 複数設定が存在する場合
			// 出所ゾーンリストの設定
			setComConnect = tmpSetCom + FROM + QUOT;
			bRst = setSecurityRule(srcZoneList, setComConnect, tmpFile, uvo);
			if (!bRst){
				return false;
			}

			// 宛先ゾーンリストの設定
			setComConnect = tmpSetCom + TO + QUOT;
			bRst = setSecurityRule(dstZoneList, setComConnect, tmpFile, uvo);
			if (!bRst){
				return false;
			}

			// 出所アドレス名リストの設定
			setComConnect = tmpSetCom + SOURCE + QUOT;
			bRst = setSecurityRule(srcAddressList, setComConnect, tmpFile, uvo);
			if (!bRst){
				return false;
			}

			// 宛先アドレス名リストの設定
			setComConnect = tmpSetCom + DESTINATION + QUOT;
			bRst = setSecurityRule(dstAddressList, setComConnect, tmpFile, uvo);
			if (!bRst){
				return false;
			}

			// アプリケーション名リストの設定
			setComConnect = tmpSetCom + APPLICATION + QUOT;
			bRst = setSecurityRule(appliList, setComConnect, tmpFile, uvo);
			if (!bRst){
				return false;
			}

			// サービス名リストの設定
			setComConnect = tmpSetCom + SERVICE + QUOT;
			bRst = setSecurityRule(srvList, setComConnect, tmpFile, uvo);
			if (!bRst){
				return false;
			}
		}

		// コミットコマンド
		String commitCom = PropertyUtil.getProperty("fwsetting.common.commit");
		// 終了コマンド
		String exit = PropertyUtil.getProperty("fwsetting.common.exit");

		// コマンドリスト
		comList = new ArrayList<String>();
		 // コンフィグモードコマンド設定
		comList.add(commitCom);
		comList.add(exit);
		// コマンドを実行し、レスポンスを取得する。
		response = getCommandResponce(tmpFile, comList);

		log.debug("registPA処理終了:true");
		return true;
	}

	/**
	 * メソッド名 : getCommandResponce
	 * 機能概要 : commandListのコマンドをファイルに出力する。
	 * @param compara コマンドパラメータ
	 * @param commandList コマンドリスト
	 * @return String コマンドレスポンス
	 * @throws Exception
	 */
	public String getCommandResponce(String tmpFile,
				List<String> commandList) throws Exception {
		log.debug("getCommandResponce処理開始:commandList.size() = " + commandList.size());

		// コンフィグコマンド有無フラグ
//		boolean configFlg = false;
		// レスポンス
		String response = "";
//		// コンフィグモードコマンド
//		String configure = PropertyUtil.getProperty("fwsetting.common.configure");
//		// 終了コマンド
//		String exit = PropertyUtil.getProperty("fwsetting.common.exit");

//		if (log.isDebugEnabled()) {
//			log.debug("コンフィグモードコマンド:"+ configure);
//			log.debug("終了コマンド:" + exit);
//		}

		FileWriter filewriter = new FileWriter(tmpFile, true);

		try {
			// commandList分繰り返す。
			for(int i=0; i<commandList.size(); i++) {
				log.debug("* コマンド生成 : " + commandList.get(i));
				filewriter.write(commandList.get(i));
				filewriter.flush();

//		        // commandListに"configure"が含まれている場合
//				if(commandList.get(i).equals(configure)) {
//					log.debug("コマンドにコンフィグが含まれる。:commandList.get(i) = " +
//																			commandList.get(i));
//					configFlg = true;
//				}
			}
//			// commandListに"configure"が含まれている場合
//			if(configFlg) {
//				log.debug("コマンドにコンフィグが含まれるのでexit" + configFlg);
//				filewriter.write(exit);
//				filewriter.flush();
//			}

		} catch (Exception e) {
			// コマンド生成エラー
			log.error("*** " + e.getMessage());
			e.printStackTrace();
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("EZ011001", objectgArray));
			filewriter.close();
			throw e;
		}
		log.debug("getCommandResponce処理終了2");
		filewriter.close();
		response = "OK";
		return response;
	}

	/**
	 * メソッド名 :setGroupOject
	 * 機能概要 :グループオブジェクトの設定を行う。
	 * @param objList 設定対象オブジェクト情報リスト
	 * @param processFlg プロセスフラグ
	 * @throws Exception
	 */
	private boolean setGroupOject(List<LinkInfo> objList,
			String tmpFile, CusconUVO uvo, int processFlg)
	throws Exception {
		log.debug("setGroupOject処理開始:objList.size() = " +
						objList.size() + ", processFlg = " + processFlg);
		// レスポンス返却用
		String response;
		// レスポンス解析用
		String checkSetObj;
		// コマンド一時生成用
		String tmpSetCom;
		// 設定コマンド生成用
		String setComConnect;
		// ループ制御フラグ
		boolean roopFlg = true;
		// コマンドリスト
		List<String> comList;
		// コマンドタイプ
		String comType;
		// 設定処理に使用するためのリスト
		List<LinkInfo> tmp_list = objList;


		// アドレスグループオブジェクト
		if(processFlg == 1) {
			log.debug("アドレスグループ設定");
			comType = ADDRESS_GRP;
		// アプリケーショングループオブジェクト
		} else if(processFlg == 2) {
			log.debug("アプリケーショングループ設定");
			comType = APPLI_GRP;
		// サービスグループオブジェクト
		} else {
			log.debug("サービスグループ設定");
			comType = SERVICE_GRP;
		}

		// 全てのグループオブジェクトの設定が完了するまで処理を繰り返す。
		while(roopFlg) {
			log.debug("グループオブジェクト設定:" + roopFlg);
			for(int i=0; i<objList.size(); i++) {
				log.debug("設定中:" + i);
				// コマンドリスト
				comList = new ArrayList<String>();
//				 // コンフィグモードコマンド設定
//				comList.add(CONFIG_COM);

				// 対象の設定
				// UPDATED: 2016.12 by Plum Systems Inc.
				// PANOS7対応 -->
				if(comType == ADDRESS_GRP){
					tmpSetCom = SET_COM + comType + QUOT +
						objList.get(i).getName() + QUOT + SP + "static " + QUOT +
										objList.get(i).getLinkName() + QUOT;
				} else if(comType == APPLI_GRP || comType == SERVICE_GRP) {
					tmpSetCom = SET_COM + comType + QUOT +
							objList.get(i).getName() + QUOT + SP + "members " + QUOT +
											objList.get(i).getLinkName() + QUOT;
				} else {
					tmpSetCom = SET_COM + comType + QUOT +
						objList.get(i).getName() + QUOT + SP + QUOT +
										objList.get(i).getLinkName() + QUOT;
				}
				setComConnect = tmpSetCom + CHANGE_LINE;
				// --> PANOS7対応
				//tmpSetCom = SET_COM + comType + QUOT +
				//	objList.get(i).getName() + QUOT + SP + QUOT +
				//					objList.get(i).getLinkName() + QUOT;
				//setComConnect = tmpSetCom + CHANGE_LINE;

				// 設定コマンドログ出力
				Object[] objectgArray = {setComConnect};
				log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));

				comList.add(setComConnect);

				// コマンドを実行し、レスポンスを取得する。
				response = getCommandResponce(tmpFile, comList);

				checkSetObj = tmpSetCom + CHECK_COM;


				// レスポンスを分割する。
// レスポンス判定変更時有効化
				if(response.indexOf(COM_ERROR)==-1){
					log.debug("設定コマンド成功");
					// リストから要素を削除する。
					tmp_list.remove(i);
				}
			}

			// リストの要素が存在しない場合、ループを抜ける。
			if(tmp_list.size() == 0) {
				log.debug("処理対象オブジェクト設定完了：false");
				roopFlg = false;
			}
			objList = tmp_list;
		}

		log.debug("setGroupOject処理終了2:true");
		return true;
	}

	/**
	 * メソッド名 : setSecurityRule
	 * 機能概要 : objListのデータ件数分ポリシー情報を設定する。
	 * @param objList 設定対象オブジェクト情報リスト
	 * @param command 実行コマンド
	 * @param num データ件数判定
	 * @throws Exception
	 */
	private boolean setSecurityRule(List<SelectObjectList> objList,
			String commandConnect, String tmpFile, CusconUVO uvo) throws Exception{

		log.debug("setSecurityRule処理開始:objList.size() = " +
				objList.size() + ", commandConnect = " + commandConnect);
		// コマンドリスト
		List<String> comList;

		// オブジェクトリストのデータがnum件以上の場合のみ実行
		for(int i=1; i<objList.size(); i++) {
			log.debug("オブジェクト設定:" + i);
			// コマンドリスト
			comList = new ArrayList<String>();
//			 // コンフィグモードコマンド設定
//			comList.add(CONFIG_COM);

			String tmpcommandConnect = commandConnect + objList.get(i).getName() + QUOT + CHANGE_LINE;

			// 実行コマンド設定
			comList.add(tmpcommandConnect);

			// 設定コマンドログ出力
			Object[] objectgArray = {tmpcommandConnect};
			log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));
			// コマンド実行
			String response = getCommandResponce(tmpFile, comList);
		}

		log.debug("setSecurityRule処理終了2:true");

		return true;
	}

}
