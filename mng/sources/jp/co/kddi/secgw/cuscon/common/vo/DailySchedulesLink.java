/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : DailySchedulesLink.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/18     komakiys         初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common.vo;

import java.io.Serializable;
import java.util.List;

/**
 * クラス名 : DailySchedulesLink
 * 機能概要 : デイリースケジュールクリンクデータクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/18
 *          新規作成
 * @see
 */
public class DailySchedulesLink implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 3437103778229922210L;

	// スケジュールオブジェクトのSeqNoとのリンク
	private int schedulesSeqNo = 0;
	// 開始時刻
	private String startTime = null;
	// 終了時刻
	private String endTime = null;
	// 開始時刻リスト
	private List<String> startTimeList = null;
	// 開始時刻リスト
	private List<String> endTimeList = null;
	// スケジュールオブジェクト名
	private String name = null;
	// Vsys-ID
	private String vsysId = null;
	// 世代番号
	private int generationNo = 0;

	/**
	 * メソッド名 : schedulesSeqNoのGetterメソッド
	 * 機能概要 : schedulesSeqNoを取得する。
	 * @return schedulesSeqNo
	 */
	public int getSchedulesSeqNo() {
		return schedulesSeqNo;
	}
	/**
	 * メソッド名 : schedulesSeqNoのSetterメソッド
	 * 機能概要 : schedulesSeqNoをセットする。
	 * @param schedulesSeqNo
	 */
	public void setSchedulesSeqNo(int schedulesSeqNo) {
		this.schedulesSeqNo = schedulesSeqNo;
	}
	/**
	 * メソッド名 : startTimeのGetterメソッド
	 * 機能概要 : startTimeを取得する。
	 * @return startTime
	 */
	public String getStartTime() {
		return startTime;
	}
	/**
	 * メソッド名 : startTimeのSetterメソッド
	 * 機能概要 : startTimeをセットする。
	 * @param startTime
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	/**
	 * メソッド名 : endTimeのGetterメソッド
	 * 機能概要 : endTimeを取得する。
	 * @return endTime
	 */
	public String getEndTime() {
		return endTime;
	}
	/**
	 * メソッド名 : endTimeのSetterメソッド
	 * 機能概要 : endTimeをセットする。
	 * @param endTime
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	/**
	 * メソッド名 : startTimeListのGetterメソッド
	 * 機能概要 : startTimeListを取得する。
	 * @return startTimeList
	 */
	public List<String> getStartTimeList() {
		return startTimeList;
	}
	/**
	 * メソッド名 : startTimeListのSetterメソッド
	 * 機能概要 : startTimeListをセットする。
	 * @param startTimeList
	 */
	public void setStartTimeList(List<String> startTimeList) {
		this.startTimeList = startTimeList;
	}
	/**
	 * メソッド名 : endTimeListのGetterメソッド
	 * 機能概要 : endTimeListを取得する。
	 * @return endTimeList
	 */
	public List<String> getEndTimeList() {
		return endTimeList;
	}
	/**
	 * メソッド名 : endTimeListのSetterメソッド
	 * 機能概要 : endTimeListをセットする。
	 * @param endTimeList
	 */
	public void setEndTimeList(List<String> endTimeList) {
		this.endTimeList = endTimeList;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得する。
	 * @return vsysId
	 */
	public String getVsysId() {
		return vsysId;
	}
	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdをセットする。
	 * @param vsysId
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}
	/**
	 * メソッド名 : generationNoのGetterメソッド
	 * 機能概要 : generationNoを取得する。
	 * @return generationNo
	 */
	public int getGenerationNo() {
		return generationNo;
	}
	/**
	 * メソッド名 : generationNoのSetterメソッド
	 * 機能概要 : generationNoをセットする。
	 * @param generationNo
	 */
	public void setGenerationNo(int generationNo) {
		this.generationNo = generationNo;
	}
}
