/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CusconAuthenticationController.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/04     h.kubo@JCCH             初版作成
 * 2016/12/23     T.Yamazaki@Plum Systems デバッグログ出力の追加
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jp.terasoluna.fw.util.PropertyUtil;
import jp.terasoluna.fw.web.RequestUtil;
import jp.terasoluna.fw.web.thin.AuthenticationController;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * クラス名 : CusconAuthenticationController
 * 機能概要 : 認証チェックを行う
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *          Created 2010/03/04
 *          新規作成
 *          2.0 T.Yamazaki@Plum Systems Inc.
 *          Updated 2016/12/23
 *          デバッグログ出力の追加
 */
public class CusconAuthenticationController implements AuthenticationController {

	// ログクラス
	private static Log log = LogFactory.getLog(CusconAuthenticationController.class);

	// 認証チェック除外パス情報リストのキー
	private static final String AUTHENTICATED_NOCHECK_PATH_PREFIX = "access.control.authenticated.escape.";

	// 認証チェック除外パス情報リスト
	private List<String> noCheckList = null;

	// チェック対象画面パス情報リストのキー
	private static final String AUTHENTICATED_ADMIN_NOCHECK_PATH_PREFIX = "access.control.authenticated.admin.escape.";

	// チェック対象画面パス情報リスト
	private List<String> noCheckList_customer = null;

	/**
	 * メソッド名 : 認証済みチェック処理
	 * 機能概要 : リクエストのパス情報に対して、指定されたHTTPセッションが 認証済みであるかどうかを判定する。
	 * @param pathInfo パス情報
	 * @param req HTTPリクエスト
	 * @return 認証に成功すれば、<code>true</code> 失敗の場合は、<code>false</code>
	 * @see jp.terasoluna.fw.web.thin.AuthenticationController#isAuthenticated(java.lang.String,javax.servlet.ServletRequest)
	 */
	public boolean isAuthenticated(String pathInfo, ServletRequest req) {

		if (log.isDebugEnabled()) {
			log.debug("call isAuthenticated");
		}

		// リクエストパスからログアウトURLを設定
		String logoutUrl = PropertyUtil.getProperty("logout.url.customer");
		if( pathInfo.indexOf(PropertyUtil.getProperty("logout.prefix.admin")) == 0){
			logoutUrl = PropertyUtil.getProperty("logout.url.admin");
		}
		req.setAttribute("logoutUrl", logoutUrl);
		log.debug("* logoutUrl : " + logoutUrl);

		// セッションからCusconUVOを取得する。
		HttpSession session = ((HttpServletRequest) req).getSession();
		log.debug("* session : " + session.getAttributeNames());

		CusconUVO uvo = (CusconUVO) session.getAttribute("USER_VALUE_OBJECT");
		//log.debug("* uvo : " + uvo.toString());

		// CusconUVOが存在しない場合はfalseを返却
		if (uvo == null ) {
			log.debug("* CusconUVOが存在していないため、認証できませんでした。");
			return false;
		}
		log.debug("* uvo : " + uvo.toString());

		// CusconUVOに登録されているカスコンログインIDがnullの場合はfalseを返却する。
		if (uvo.getLoginId() == null) {
			log.debug("* CusconUVOに登録されているカスコンログインIDがnullのため、認証できませんでした。");
			return false;
		}

		// チェック条件：KDDI管理者が対象企業を未選択な状態以外の画面
		if (isCheckCustomer(req)) {
			// CusconUVOに登録されているPAログインIDがnullの場合はfalseを返却する。
			if (uvo.getPaLoginId() == null) {
				// 権限フラグからログアウトURLを決定する
				log.debug("* CusconUVOに登録されているPAログインIDがnullのため、認証できませんでした。");
				long grantFlag = uvo.getGrantFlag();
				log.debug("* grantFlag : " + grantFlag);
				logoutUrl = PropertyUtil.getProperty("logout.url.customer");
				if( (grantFlag == 1) || (grantFlag == 2) ){
					logoutUrl = PropertyUtil.getProperty("logout.url.admin");
				}
				req.setAttribute("logoutUrl", logoutUrl);
				log.debug("* logoutUrl : " + logoutUrl);
				return false;
			}
		}

		return true;
	}

	/**
	 * メソッド名 : チェック除外判定
	 * 機能概要 : リクエストパスがチェック対象か否か判定する。
	 * @param req 判定対象となるServletRequestインスタンス
	 * @return チェック対象の場合はtrue、チェック対象以外の場合はfalse
	 * @see jp.terasoluna.fw.web.thin.AuthenticationController#isCheckRequired(javax.servlet.ServletRequest)
	 */
	public boolean isCheckRequired(ServletRequest req) {

		if (log.isDebugEnabled()) {
			log.debug("call isCheckRequired()");
		}

		// リクエストパスを取得する
		String pathInfo = RequestUtil.getPathInfo(req);

		// 認証チェック除外パス情報リストを取得する
		if (noCheckList == null) {
			noCheckList = new ArrayList<String>();
			for (int i = 1;; i++) {
				String path = PropertyUtil.getProperty(AUTHENTICATED_NOCHECK_PATH_PREFIX + i);
				if (path == null) {
					break;
				}
				noCheckList.add(path);
			}
		}

		// リクエストパスが、認証チェック除外パス情報リストに乗っているかどうかをチェック
		for (String path : noCheckList) {
			if (pathInfo.startsWith(path) || "/".equals(pathInfo)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * メソッド名 : チェック除外判定
	 * 機能概要 : KDDI運用者が対象企業を未選択な状態以外の画面であるかどうかを判定する。
	 * @param req 判定対象となるServletRequestインスタンス
	 * @return チェック対象の場合はtrue、チェック対象以外の場合はfalse
	 */
	public boolean isCheckCustomer(ServletRequest req) {

		if (log.isDebugEnabled()) {
			log.debug("call isCheckCustomer()");
		}

		// リクエストパスを取得する
		String pathInfo = RequestUtil.getPathInfo(req);

		// 認証チェック除外パス情報リストを取得する
		if (noCheckList_customer == null) {
			noCheckList_customer = new ArrayList<String>();
			for (int i = 1;; i++) {
				String path = PropertyUtil.getProperty(AUTHENTICATED_ADMIN_NOCHECK_PATH_PREFIX + i);
				if (path == null) {
					break;
				}
				noCheckList_customer.add(path);
			}
		}

		// リクエストパスが、認証チェック除外パス情報リストに乗っているかどうかをチェック
		for (String path : noCheckList_customer) {
			if (pathInfo.startsWith(path) || "/".equals(pathInfo)) {
				return false;
			}
		}

		return true;
	}

}
