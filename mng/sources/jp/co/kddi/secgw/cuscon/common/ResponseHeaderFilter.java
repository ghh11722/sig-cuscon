/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ResponseHeaderFilter.java
 *
 * [変更履歴]
 * 日付        更新者             内容
 * 2010/03/02  m.narikawa@JCCH    初版作成
 * 2011/05/25  ktakenaka@PROSITE  ログモニタリングのURL(Webウィルスチェック、スパイウェアチェック、Webフィルタ）追加
 * 2012/08/27  kkato@PROSITE      FWコンフィグ情報ダウンロード対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import jp.terasoluna.fw.web.RequestUtil;

/**
 * クラス名 : RespponseHeaderFilter
 * 機能概要 : レスポンスヘッダにキャッシュが無効になるヘッダを追加する。
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class ResponseHeaderFilter implements Filter {
	// ログモニタリングのURL
	private static final String ANY_ACTION          = ".do";
	private static final String TLOG_ACTION         = "/tlogBL";
	private static final String TLOG_MAKECSV_ACTION = "/tlogMakeCsvBL";
	private static final String TLOG_DL_ACTION      = "/tlogDlBL";
	private static final String ILOG_ACTION         = "/ilogBL";
	private static final String ILOG_MAKECSV_ACTION = "/ilogMakeCsvBL";
	private static final String ILOG_DL_ACTION      = "/ilogDlBL";
	//20110525 ktakenaka@PROSITE add start
	private static final String VLOG_ACTION         = "/viruslogBL";
	private static final String VLOG_MAKECSV_ACTION = "/viruslogMakeCsvBL";
	private static final String VLOG_DL_ACTION      = "/viruslogDlBL";
	private static final String SLOG_ACTION         = "/spywarelogBL";
	private static final String SLOG_MAKECSV_ACTION = "/spywarelogMakeCsvBL";
	private static final String SLOG_DL_ACTION      = "/spywarelogDlBL";
	private static final String ULOG_ACTION         = "/urllogBL";
	private static final String ULOG_MAKECSV_ACTION = "/urllogMakeCsvBL";
	private static final String ULOG_DL_ACTION      = "/urllogDlBL";
	//20110525 ktakenaka@PROSITE add end
	// 20120827 kkato@PROSITE add start
	private static final String FWCFG_ACTION         = "/fwcfgBL";
	private static final String FWCFG_MAKECSV_ACTION = "/fwcfgMakeCsvBL";
	private static final String FWCFG_DL_ACTION      = "/fwcfgDlBL";
	// 20120827 kkato@PROSITE add end
	//private static final String KEEP_SESSION_ACTION = "/keepSessionSCR";

	// フィルタの動作設定
	private FilterConfig filterConfig;

	/**
	 * メソッド名 : フィルタ処理
	 * 機能概要 : レスポンスヘッダにキャッシュ無効となるヘッダを追加
	 * @param request HTTPリクエスト
	 * @param response HTTPレスポンス
	 * @param chain 次のフィルタ
	 * @throws IOException
	 * @throws ServletException
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
	 *      javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		// Enable設定がtrueの場合のみレスポンスヘッダを追加する
		String enable = filterConfig.getInitParameter("Enable");
		if(enable.compareToIgnoreCase("true") == 0){
			// リクエストパスがログモニタリング以外の場合、キャッシュ無効となるヘッダを追加する
			String pathInfo = RequestUtil.getPathInfo(request);
			//if(pathInfo.indexOf(KEEP_SESSION_ACTION) >= 0) {
			//	System.out.println("keep session!!");
			//}
			if( (pathInfo.indexOf(ANY_ACTION) >= 0) &&
				(pathInfo.indexOf(TLOG_ACTION) == -1) &&
				(pathInfo.indexOf(TLOG_MAKECSV_ACTION) == -1) &&
				(pathInfo.indexOf(TLOG_DL_ACTION) == -1) &&
				//20110525 ktakenaka@PROSITE add start
				(pathInfo.indexOf(VLOG_ACTION) == -1) &&
				(pathInfo.indexOf(VLOG_MAKECSV_ACTION) == -1) &&
				(pathInfo.indexOf(VLOG_DL_ACTION) == -1) &&
				(pathInfo.indexOf(SLOG_ACTION) == -1) &&
				(pathInfo.indexOf(SLOG_MAKECSV_ACTION) == -1) &&
				(pathInfo.indexOf(SLOG_DL_ACTION) == -1) &&
				(pathInfo.indexOf(ULOG_ACTION) == -1) &&
				(pathInfo.indexOf(ULOG_MAKECSV_ACTION) == -1) &&
				(pathInfo.indexOf(ULOG_DL_ACTION) == -1) &&
				//20110525 ktakenaka@PROSITE add end
				// 20120827 kkato@PROSITE add start
				(pathInfo.indexOf(FWCFG_ACTION) == -1) &&
				(pathInfo.indexOf(FWCFG_MAKECSV_ACTION) == -1) &&
				(pathInfo.indexOf(FWCFG_DL_ACTION) == -1) &&
				// 20120827 kkato@PROSITE add end
			    (pathInfo.indexOf(ILOG_ACTION) == -1) &&
			    (pathInfo.indexOf(ILOG_MAKECSV_ACTION) == -1) &&
			    (pathInfo.indexOf(ILOG_DL_ACTION) == -1) ){

				((HttpServletResponse) response).setHeader("Pragma", "no-cache");
				((HttpServletResponse) response).setHeader("Cache-Control", "no-cache");
				((HttpServletResponse) response).setHeader("Expires", "Sun,6 Jun 2006 00:00:00 GMT");
			}
		}

		// 次のフィルタに処理を移す（複数フィルタ適用時）
		chain.doFilter(request, response);
	}

	/**
	 * メソッド名 : フィルタ初期化処理
	 * 機能概要 :
	 * @param filterConfig
	 * @throws ServletException
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig; // フィルタの初期化
	}

	/**
	 * メソッド名 : フィルタ破棄処理
	 * 機能概要 :
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {
		this.filterConfig = null;
	}
}
