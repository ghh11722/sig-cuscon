/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UrlFilterLink.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/10     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common.vo;

import java.io.Serializable;

/**
 * クラス名 : UrlFilterLink
 * 機能概要 : Webフィルタリンクデータクラス
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/10
 *          新規作成
 * @see
 */
public class UrlFilterLink implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = -5575409879153117763L;
	// セキュリティポリシー一覧のSeqNoとのリンク
	private int secRlsSeqNo = 0;
	// WebフィルタオブジェクトのSeqNoとのリンク
	private int linkSeqNo = 0;

	/**
	 * メソッド名 : secRlsSeqNoのGetterメソッド
	 * 機能概要 : secRlsSeqNoを取得する。
	 * @return secRlsSeqNo
	 */
	public int getSecRlsSeqNo() {
		return secRlsSeqNo;
	}
	/**
	 * メソッド名 : secRlsSeqNoのSetterメソッド
	 * 機能概要 : secRlsSeqNoをセットする。
	 * @param secRlsSeqNo
	 */
	public void setSecRlsSeqNo(int secRlsSeqNo) {
		this.secRlsSeqNo = secRlsSeqNo;
	}
	/**
	 * メソッド名 : linkSeqNoのGetterメソッド
	 * 機能概要 : linkSeqNoを取得する。
	 * @return linkSeqNo
	 */
	public int getLinkSeqNo() {
		return linkSeqNo;
	}
	/**
	 * メソッド名 : linkSeqNoのSetterメソッド
	 * 機能概要 : linkSeqNoをセットする。
	 * @param linkSeqNo
	 */
	public void setLinkSeqNo(int linkSeqNo) {
		this.linkSeqNo = linkSeqNo;
	}
}
