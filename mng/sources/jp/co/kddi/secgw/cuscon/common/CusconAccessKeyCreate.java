/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CusconAccessKeyCreate.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/24     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.net.URLEncoder;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectSystemConstantList;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;
import jp.terasoluna.fw.util.PropertyUtil;

/**
 * クラス名 : CusconAccessKeyCreate
 * 機能概要 :
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/24
 *          新規作成
 * @see
 */
public class CusconAccessKeyCreate {

    // ホスト名
    private String host = null;

	// 要求種別
	private static final String TYPE =
    	PropertyUtil.getProperty("common.xmlaccesstype");
	// URL(ホスト名前)
	private static final String URL1 =
    	PropertyUtil.getProperty("common.xmlurl1");
	// URL(ホスト名後)
	private static final String URL2 =
    	PropertyUtil.getProperty("common.xmlurl2");
	// URL結合文字
	private static final String AND = "&";
	// 復号化鍵
	private static final String KEY = "kddi-secgw";
	// ユーザ
	private static final String USER = "user=";
	// パスワード
	private static final String PASSWORD = "password=";

	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.common.CusconAccessKeyCreate");
	 // メッセージクラス
    private static MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 */
	public CusconAccessKeyCreate() {
		log.debug("CusconAccessKeyCreateコンストラクタ処理開始");
		log.debug("CusconAccessKeyCreateコンストラクタ処理終了");
	}

	/**
	 * メソッド名 : getAccessKey
	 * 機能概要 : インプットの情報が存在する場合、インプットの情報にてアクセスキーを
	 *            取得する。<br>
	 *            存在しない場合、PA管理者権限のアクセスキーを取得する。
	 * @param loginId PAログインID
	 * @param password PAパスワード
	 * @return String アクセスキー(失敗の場合はnull)
	 * @throws Exception
	 */
	public String getAccessKey(String loginId, String password, QueryDAO queryDAO, CusconUVO uvo) throws Exception {

		log.debug("getAccessKey処理開始:loginId = " +
							loginId + ", password = " + password);
        // XML-APIのURL生成用
        String xmlApiConnect;
        // ユーザID
        String user;
        // パスワード
        String pass;
        // XML-APIレスポンス
        InputStream keyRes;
        // XML解析レスポンス
        List<HashMap<String, Object>> objAnalyse = null;
        // アクセスキー
        String accessKey = null;
        // PA接続情報を取得する。
		List<SelectSystemConstantList> connectionList = null;

        try {
			// PA接続情報を取得する。
			connectionList = queryDAO.executeForObjectList(
										"CommonM_SystemConstant-1", KEY);
        } catch (Exception e) {
        	// トランザクションロールバック
        	TransactionUtil.setRollbackOnly();
        	// DBアクセスエラー
        	Object[] objectgArray = {e.getMessage()};
        	log.fatal(messageAccessor.getMessage("FZ001002", objectgArray, uvo));
        	throw e;
        }

		if (log.isDebugEnabled()) {
			log.debug("要求種別:" + TYPE);
			log.debug("URL(ホスト名前):" + URL1);
			log.debug("URL(ホスト名後)" + URL2);
		}
		// URL作成用ユーザID
		if(password == null) {
			log.debug("ユーザIDの設定なし");
			user = USER + URLEncoder.encode(
					connectionList.get(0).getLoginId(), "UTF-8");
		} else {
			log.debug("ユーザIDの設定あり");
			user = USER + URLEncoder.encode(loginId, "UTF-8");
		}

		// URL作成用パスワード
		if(password == null) {
			log.debug("パスワードの設定なし");
			pass = PASSWORD + URLEncoder.encode(
					connectionList.get(0).getPassword(), "UTF-8");
		} else {
			log.debug("パスワードの設定あり");
			pass = PASSWORD + URLEncoder.encode(password, "UTF-8");
		}

		// ホストを設定する。
		host = connectionList.get(0).getUrl();
		// XML-APIアクセスキー生成URL作成
		xmlApiConnect = URL1 + host + URL2  +
				TYPE + AND + user + AND + pass;

		// XML-API実行クラスを生成する。
		CusconXmlApiExecute xmlExe = new CusconXmlApiExecute();
	    // XML-API解析クラスを生成する。
		CusconXMLAnalyze xmlAna = new CusconXMLAnalyze(messageAccessor);

		// XML-APIを実行し、レスポンスを取得する。
		keyRes = xmlExe.doProc(xmlApiConnect, uvo);

		// XML解析を行う
		objAnalyse = xmlAna.analyzeXML(keyRes, uvo);

		// XML-APIが成功の場合、アクセスキーを取得する。
		if(objAnalyse != null) {
			log.debug("XML-APIの応答あり");
			Iterator iterator = objAnalyse.get(0).entrySet().iterator();
			Map.Entry obj = (Map.Entry)iterator.next();
    		accessKey = obj.getValue().toString();
		}

		log.debug("getAccessKey処理終了:accessKey = " + accessKey);

		return accessKey;
	}

	/**
	 * メソッド名 : hostのGetterメソッド
	 * 機能概要 : hostを取得する。
	 * @return host
	 */
	public String getHost() {
		log.debug("getHost処理開始:host = " + host);
		return host;
	}

	 /**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor msgAcc){
		log.debug("setMessageAccessor処理開始");
	    setMsgAcc(msgAcc);
	    log.debug("setMessageAccessor処理終了");
	}

	/**
	 * メソッド名 : setMsgAcc
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public static void setMsgAcc(MessageAccessor messageAccessor) {
		log.debug("setMsgAcc処理開始");
		CusconAccessKeyCreate.messageAccessor = messageAccessor;
		log.debug("setMsgAcc処理終了");
	}

	/**
	 * メソッド名 : getMessage
	 * 機能概要 : messageAccessorをゲットする。
	 * @return MessageAccessor
	 */
	public static MessageAccessor getMessage() {
		log.debug("getMessage処理");
		return messageAccessor;
	}
}
