/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CusconRepDelete.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/04/08     hiroyasu                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common;

import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

import org.apache.log4j.Logger;

/**
 * クラス名 : CusconRepDelete
 * 機能概要 :
 * 備考 :
 * @author hiroyasu
 * @version 1.0 hiroyasu
 *          Created 2010/04/08
 *          新規作成
 * @see
 */
public class CusconRepDelete {

	// メッセージクラス
    private static MessageAccessor messageAccessor = null;

    private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.common.CusconRepDelete");

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		CusconRepDelete.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}


	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 */
	public CusconRepDelete() {
		log.debug("CusconRepDeleteコンストラクタ処理開始");
		log.debug("CusconRepDeleteコンストラクタ処理終了");
	}


	/**
	 * メソッド名 : setFWInfo
	 * 機能概要 : 指定VsysIDについてFW設定情報DB登録<br>
	 *            (Palo設定情報取得/0世代の削除登録)を行う。
	 * @param updateDAO
	 * @param queryDAO
	 * @param uvo
	 * @throws Exception
	 */
	public void deleteRepInfo(UpdateDAO updateDAO, CusconUVO uvo) throws Exception {

		log.debug("deleteRepInfo処理開始:vsysId = " + uvo.getVsysId());
		InputBase base = new InputBase();

		// 引数情報を削除条件に設定する。
		base.setVsysId(uvo.getVsysId());

		/***********************************************************
		 *  レポート情報削除処理
		 ***********************************************************/
		try {
			// TopAppSummaryのデータを削除する。
			updateDAO.execute("CommonT_TopAppSummary-1", base);

			// TopAttacksSummaryのデータを削除する。
			updateDAO.execute("CommonT_TopAttacksSummary-1", base);

			// TopDstSummaryのデータを削除する。
			updateDAO.execute("CommonT_TopDstSummary-1", base);

			// TopDstCountriesSummaryのデータを削除する。
			updateDAO.execute("CommonT_TopDstCountriesSummary-1", base);

			// TopSrcSummaryのデータを削除する。
			updateDAO.execute("CommonT_TopSrcSummary-1", base);

		} catch (Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FZ001012", objectgArray, uvo));
			throw e;
		}

		log.debug("deleteRepInfo処理終了");

	}
}
