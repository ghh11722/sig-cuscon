/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ApplicationGroupsLink.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/18     morisou          初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common.vo;

import java.io.Serializable;

/**
 * クラス名 : ApplicationGroupsLink
 * 機能概要 : アプリケーショングループリンクデータクラス
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/18
 *          新規作成
 * @see
 */
public class ApplicationGroupsLink implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = -2743820013669107965L;
	// アプリケーショングループオブジェクトのSeqNoとのリンク
	private int aplGrpSeqNo = 0;
	// オブジェクトタイプ
	private int objectType = 0;
	// アプリケーションオブジェクトマスタ、アプリケーショングループオブジェクト、又はアプリケーションフィルタオブジェクトのSeqNoとのリンク
	private int linkSeqNo = 0;
	/**
	 * メソッド名 : aplGrpSeqNoのGetterメソッド
	 * 機能概要 : aplGrpSeqNoを取得する。
	 * @return aplGrpSeqNo
	 */
	public int getAplGrpSeqNo() {
		return aplGrpSeqNo;
	}
	/**
	 * メソッド名 : aplGrpSeqNoのSetterメソッド
	 * 機能概要 : aplGrpSeqNoをセットする。
	 * @param aplGrpSeqNo
	 */
	public void setAplGrpSeqNo(int aplGrpSeqNo) {
		this.aplGrpSeqNo = aplGrpSeqNo;
	}
	/**
	 * メソッド名 : objectTypeのGetterメソッド
	 * 機能概要 : objectTypeを取得する。
	 * @return objectType
	 */
	public int getObjectType() {
		return objectType;
	}
	/**
	 * メソッド名 : objectTypeのSetterメソッド
	 * 機能概要 : objectTypeをセットする。
	 * @param objectType
	 */
	public void setObjectType(int objectType) {
		this.objectType = objectType;
	}
	/**
	 * メソッド名 : linkSeqNoのGetterメソッド
	 * 機能概要 : linkSeqNoを取得する。
	 * @return linkSeqNo
	 */
	public int getLinkSeqNo() {
		return linkSeqNo;
	}
	/**
	 * メソッド名 : linkSeqNoのSetterメソッド
	 * 機能概要 : linkSeqNoをセットする。
	 * @param linkSeqNo
	 */
	public void setLinkSeqNo(int linkSeqNo) {
		this.linkSeqNo = linkSeqNo;
	}
}
