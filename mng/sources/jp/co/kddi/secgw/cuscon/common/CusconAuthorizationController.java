/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CusconAuthorizationController.java
 *
 * [変更履歴]
 * 日付        更新者       内容
 * 2010/03/04  h.kubo@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jp.terasoluna.fw.util.PropertyUtil;
import jp.terasoluna.fw.web.RequestUtil;
import jp.terasoluna.fw.web.thin.AuthorizationController;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * クラス名 : CusconAuthorizationController
 * 機能概要 : 認証チェックを行う
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class CusconAuthorizationController implements AuthorizationController {

	// ログクラス
	private static Log log = LogFactory.getLog(CusconAuthorizationController.class);

	// アクセス権限チェック対象パス情報リストのキー(KDDI運用者:NOC)
	private static final String AUTHORIZED_ADMIN_NOC_NOCHECK_PATH_PREFIX = "access.control.authorized.admin_noc.escape.";

	// アクセス権限チェック対象パス情報リスト(KDDI運用者:NOC)
	private List<String> noCheckAdminNocList = null;

	// アクセス権限チェック対象パス情報リストのキー(KDDI運用者:TSC)
	private static final String AUTHORIZED_ADMIN_TSC_NOCHECK_PATH_PREFIX = "access.control.authorized.admin_tsc.escape.";

	// アクセス権限チェック対象パス情報リスト(KDDI運用者:TSC)
	private List<String> noCheckAdminTscList = null;

	// アクセス権限チェック対象パス情報リストのキー（企業管理者）
	private static final String AUTHORIZED_CUSTOMER_NOCHECK_PATH_PREFIX = "access.control.authorized.customer.escape.";

	// アクセス権限チェック対象パス情報リスト(企業管理者)
	private List<String> noCheckCustomerList = null;

	/**
	 * メソッド名 : アクセス権限チェック処理
	 * 機能概要 : リクエストのパス情報に対して、指定されたHTTPセッションに アクセス権限があるかどうかを判定する。
	 * @param pathInfo パス情報
	 * @param req HTTPリクエスト
	 * @return 権限があればtrue、権限がなければfalse
	 * @see jp.terasoluna.fw.web.thin.AuthorizationController#isAuthorized(java.lang.String,javax.servlet.ServletRequest)
	 */
	public boolean isAuthorized(String pathInfo, ServletRequest req) {

		long grantFlag = 0;

		if (log.isDebugEnabled()) {
			log.debug("call isAuthenticated");
		}

		// リクエストパスからログアウトURLを設定
		String logoutUrl = PropertyUtil.getProperty("logout.url.customer");
		if( pathInfo.indexOf(PropertyUtil.getProperty("logout.prefix.admin")) == 0){
			logoutUrl = PropertyUtil.getProperty("logout.url.admin");
		}
		req.setAttribute("logoutUrl", logoutUrl);

		// セッションからCusconUVOを取得する。
		HttpSession session = ((HttpServletRequest) req).getSession();
		CusconUVO uvo = (CusconUVO) session.getAttribute("USER_VALUE_OBJECT");

		// CusconUVOが存在しない場合はfalseを返却
		if (uvo == null ) {
			return false;
		}

		// ユーザ権限を取得する
		grantFlag = uvo.getGrantFlag();

		// 権限フラグの精査 KDDI運用者：NOC(1),TSC(2)、企業管理者(3)
		if (!(grantFlag == 1 || grantFlag == 2 || grantFlag == 3)) {
			return false;
		}

		// リクエストパスが、アクセス権限チェック対象パス(KDDI運用者画面:NOC)の場合
		if (grantFlag == 1) {
			if (isCheckAdminNoc(req)) {
				return true;
			}
			return false;
		}

		// リクエストパスが、アクセス権限チェック対象パス(KDDI運用者画面:TSC)の場合
		if (grantFlag == 2) {
			if (isCheckAdminTsc(req)) {
				return true;
			}
			return false;
		}

		// リクエストパスが、アクセス権限チェック対象パス(企業管理者画面)の場合
		if (grantFlag == 3) {
			if (isCheckCustomer(req)) {
				return true;
			}
			return false;
		}

		return true;
	}

	/**
	 * メソッド名 : チェック除外判定
	 * 機能概要 : リクエストパスがチェック対象か否か判定する。
	 * @param req 判定対象となるServletRequestインスタンス
	 * @return チェック対象の場合はtrue、チェック対象以外の場合はfalse
	 * @see jp.terasoluna.fw.web.thin.AuthorizationController#isCheckRequired(javax.servlet.ServletRequest)
	 */
	public boolean isCheckRequired(ServletRequest req) {

		if (log.isDebugEnabled()) {
			log.debug("call isCheckRequired()");
		}

		// リクエストパスが、アクセス権限チェック対象パス(KDDI運用者画面:NOC)かどうかをチェック
		if (isCheckAdminNoc(req)) {
			return true;
		}

		// リクエストパスが、アクセス権限チェック対象パス(KDDI運用者画面:TSC)かどうかをチェック
		if (isCheckAdminTsc(req)) {
			return true;
		}

		// リクエストパスが、アクセス権限チェック対象パス(企業管理者画面)かどうかをチェック
		if (isCheckCustomer(req)) {
			return true;
		}

		return false;
	}

	/**
	 * メソッド名 : アクセス権限チェック判定
	 * 機能概要 : KDDI運用者画面(NOC)であるかどうかを判定する。
	 * @param req 判定対象となるServletRequestインスタンス
	 * @return チェック対象の場合はtrue、チェック対象以外の場合はfalse
	 */
	public boolean isCheckAdminNoc(ServletRequest req) {

		if (log.isDebugEnabled()) {
			log.debug("call isCheckAdminNoc()");
		}

		// リクエストパスを取得する
		String pathInfo = RequestUtil.getPathInfo(req);

		// アクセス権限チェック対象パス情報リスト(KDDI運用者画面:TSC)を取得する
		if (noCheckAdminNocList == null) {
			noCheckAdminNocList = new ArrayList<String>();
			for (int i = 1;; i++) {
				String path = PropertyUtil.getProperty(AUTHORIZED_ADMIN_NOC_NOCHECK_PATH_PREFIX + i);
				if (path == null) {
					break;
				}
				noCheckAdminNocList.add(path);
			}
		}

		// リクエストパスが、アクセス権限チェック対象パス情報リスト(KDDI運用者画面:TSC)に乗っているかどうかをチェック
		for (String path : noCheckAdminNocList) {
			if (pathInfo.startsWith(path)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * メソッド名 : アクセス権限チェック判定
	 * 機能概要 : KDDI運用者画面(TSC)であるかどうかを判定する。
	 * @param req 判定対象となるServletRequestインスタンス
	 * @return チェック対象の場合はtrue、チェック対象以外の場合はfalse
	 */
	public boolean isCheckAdminTsc(ServletRequest req) {

		if (log.isDebugEnabled()) {
			log.debug("call isCheckAdminTsc()");
		}

		// リクエストパスを取得する
		String pathInfo = RequestUtil.getPathInfo(req);

		// アクセス権限チェック対象パス情報リスト(KDDI運用者画面:TSC)を取得する
		if (noCheckAdminTscList == null) {
			noCheckAdminTscList = new ArrayList<String>();
			for (int i = 1;; i++) {
				String path = PropertyUtil.getProperty(AUTHORIZED_ADMIN_TSC_NOCHECK_PATH_PREFIX + i);
				if (path == null) {
					break;
				}
				noCheckAdminTscList.add(path);
			}
		}

		// リクエストパスが、アクセス権限チェック対象パス情報リスト(KDDI運用者画面:TSC)に乗っているかどうかをチェック
		for (String path : noCheckAdminTscList) {
			if (pathInfo.startsWith(path)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * メソッド名 : アクセス権限チェック判定
	 * 機能概要 : 企業管理者画面であるかどうかを判定する。
	 * @param req 判定対象となるServletRequestインスタンス
	 * @return チェック対象の場合はtrue、チェック対象以外の場合はfalse
	 */
	public boolean isCheckCustomer(ServletRequest req) {

		if (log.isDebugEnabled()) {
			log.debug("call isCheckCustomer()");
		}

		// リクエストパスを取得する
		String pathInfo = RequestUtil.getPathInfo(req);

		// アクセス権限チェック対象パス情報リスト(企業管理者画面)を取得する
		if (noCheckCustomerList == null) {
			noCheckCustomerList = new ArrayList<String>();
			for (int i = 1;; i++) {
				String path = PropertyUtil.getProperty(AUTHORIZED_CUSTOMER_NOCHECK_PATH_PREFIX + i);
				if (path == null) {
					break;
				}
				noCheckCustomerList.add(path);
			}
		}

		// リクエストパスが、アクセス権限チェック対象パス情報リスト(企業管理者画面)に乗っているかどうかをチェック
		for (String path : noCheckCustomerList) {
			if (pathInfo.startsWith(path)) {
				return true;
			}
		}

		return false;
	}

}
