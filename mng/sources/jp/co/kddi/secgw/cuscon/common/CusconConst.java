/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : FWSettingConst.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/23     komakiys                初版作成
 * 2010/06/18     oohashij                コミットステータスの追加
 * 2011/06/10     inoue@PROSITE           Webフィルタ追加対応
 * 2013/09/01     kkato@PROSITE           アドレス種別にFQDN追加
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common;

/**
 * クラス名 : FWSettingConst
 * 機能概要 :
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/23
 *          新規作成
 * @version 1.1 oohashij
 *          Created 2010/06/18
 *          コミットステータスの追加
 * @see
 */
public class CusconConst {

	// 20110621 ktakenkaka@PROSITE add start
	// 改行コード
	//public static final String BR = System.getProperty("line.separator");
	// 20110621 ktakenaka@PROSITE add end

	// オブジェクト名
	public static final String NAME = "name";

    // 変更フラグの名称変換用
    public static final String ADD = "追加";
    public static final String MOD = "変更";
    public static final String DEL = "削除";

    // 選択中のアプリケーションタイプ名称変換用
    public static final String TYPE_APL = "アプリケーション";
    public static final String TYPE_FLT = "フィルター";
    public static final String TYPE_GRP = "グループ";

    // 変更フラグの値
    public static final int NONE_NUM = 0;
    public static final int ADD_NUM = 1;
    public static final int MOD_NUM = 2;
    public static final int DEL_NUM = 3;

    // 世代番号
    public static final int ZERO_GENE = 0;
    public static final int ONE_GENE = 1;
    public static final int TWO_GENE = 2;
    public static final int THREE_GENE = 3;

    // アドレス種別
    public static final String ADDRESS_IP = "ip-netmask";
    public static final String ADDRESS_RANGE = "ip-range";
 // 20130901 kkato@PROSITE add start
    public static final String ADDRESS_FQDN = "fqdn";
 // 20130901 kkato@PROSITE add end

    // プロトコル
    public static final String TCP = "tcp";
    public static final String UDP = "udp";

    // 脅威度
    public static final int RISK_ONE = 1;
    public static final int RISK_TWO = 2;
    public static final int RISK_THREE = 3;
    public static final int RISK_FOUR = 4;
    public static final int RISK_FIVE = 5;

    // 周期
    public static final int DAILY = 0;
    public static final int WEEKLY = 1;
    public static final int NONRECURRING = 2;

    // 周期(文字列)
    public static final String DAILY_STR = "daily";
    public static final String WEEKLY_STR = "weekly";
    public static final String NONRECURRING_STR = "non-recurring";

    public static final String NONE = "none";

    // 曜日
    public static final int SUN = 0;
    public static final int MON = 1;
    public static final int TUE = 2;
    public static final int WED = 3;
    public static final int THU = 4;
    public static final int FRI = 5;
    public static final int SAT = 6;

    // 曜日(日本語)
    public static final String JPA_SUN = "日";
    public static final String JPA_MON = "月";
    public static final String JPA_TUE = "火";
    public static final String JPA_WED = "水";
    public static final String JPA_THU = "木";
    public static final String JPA_FRI = "金";
    public static final String JPA_SAT = "土";

    // 曜日(文字列)
    public static final String SUN_STR = "sunday";
    public static final String MON_STR = "monday";
    public static final String TUE_STR = "tuesday";
    public static final String WED_STR = "wednesday";
    public static final String THU_STR = "thursday";
    public static final String FRI_STR = "friday";
    public static final String SAT_STR = "saturday";

    // any or application-default or select
    public static final String ANY = "any";
    public static final String APPLICATION_DEFAULT = "application-default";

    // アプリケーションフィルタ
    public static final String CATEGORY = "category";
    public static final String SUBCATEGORY = "subcategory";
    public static final String TECHNOLOGY = "technology";
    public static final String RISK = "risk";

    // 処理フラグ
    public static final int POLICY = 0;
    public static final int COMMIT = 1;
    public static final int ROLLBACK = 2;
    public static final int HiddenRules = 3;

    // ポリシー編集
    public static final int SELECT = 2;

    // オブジェクトタイプ
    public static final int Address = 0;
    public static final int AddressGrp = 1;
    public static final int ApplFlt = 2;
    public static final int ApplGrp = 3;
    public static final int Service = 4;
    public static final int ServiceGrp = 5;
    public static final int Schedule = 6;
//2011/06/10 add start inoue@prosite
    public static final int UrlFilter = 7;

    // Webフィルタ
    public static final String DESCRIPTION = "description";
    public static final String ACTION = "action";
    public static final String BLOCKLIST = "block-list";
    public static final String ALLOWLIST = "allow-list";
    public static final String ALLOW = "allow";
    public static final String ALERT = "alert";
    public static final String BLOCK = "block";
    public static final String CONTINUE = "continue";
    public static final String OVERRIDE = "override";
//2011/06/10 add end inoue@prosite

    // コミットフラグ
    public static final int COMMIT_OFF = 0;
    public static final int COMMIT_ON = 1;

    // コミットステータス
    public static final int STAT_COMMIT_OK = 0;
    public static final int STAT_COMMIT_NG = 1;
    public static final int STAT_COMMITTING = 2;

    // コミット/ロールバック処理状態
    public static final String ACCEPT_OK = "受付完了";
    public static final String ACCEPT_NG = "受付エラー";
}
