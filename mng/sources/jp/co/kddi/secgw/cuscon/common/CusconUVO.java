/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CusconUVO.java
 *
 * [変更履歴]
 * 日付        更新者             内容
 * 2010/03/04  m.narikawa@JCCH    初版作成
 * 2010/05/20  ktakenaka@PROSITE  Webフィルタ利用フラグ,DMZ利用フラグを追加
 * 2013/09/01  kkato@PROSITE      WEBフィルタDB,FQDN登録上限情報を追加
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common;

import jp.terasoluna.fw.web.UserValueObject;

/**
 * クラス名 : CusconUVO
 * 機能概要 : ユーザーバリューオブジェクトの実装
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class CusconUVO extends UserValueObject {
	//
	private static final long serialVersionUID = 1L;
	// カスコンログインID
	private String loginId = null;
	// ユーザ権限
	private long grantFlag = 0;
	// 企業管理者ログインID
	private String customerLoginId = null;
	// Vsys-ID
	private String vsysId = "";
	// PAログインID
	private String paLoginId = null;
	// PAログインパスワード
	private String paPasswd = null;
	// 便利そうなので追加by片桐
	private String grantFlagStr = null;
	// 20110520 ktakenaka@PROSITE add start
	// Webフィルタ利用フラグ
	private int urlFilteringFlg = 0;
	// DMZ利用フラグ
	private int dmzFlg = 0;
	// Webフィルタプロファイル登録上限
	private int urlProfileMax = 0;
	// BlockList登録上限
	private int blocklistMax = 0;
	// AllowList登録上限
	private int allowlistMax = 0;
	// 20110520 ktakenaka@PROSITE add end

	// 20130901 kkato@PROSITE add start
	// WEBフィルタDBバージョン
	private String urlDbVersion = null;
	// FQDN登録上限
	private int addressFqdnMax = 0;
	// 20130901 kkato@PROSITE add end

	/**
	 * メソッド名 : loginIdのGetterメソッド
	 * 機能概要 : loginIdを取得する。
	 * @return loginId カスコンログインID
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * メソッド名 : loginIdのSetterメソッド
	 * 機能概要 : loginIdをセットする。
	 * @param loginId カスコンログインID
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * メソッド名 : grantFlagのGetterメソッド
	 * 機能概要 : grantFlagを取得する。
	 * @return grantFlag ユーザ権限
	 */
	public long getGrantFlag() {
		return grantFlag;
	}

	/**
	 * メソッド名 : grantFlagのSetterメソッド
	 * 機能概要 : grantFlagをセットする。
	 * @param grantFlag ユーザ権限
	 */
	public void setGrantFlag(long grantFlag) {
		this.grantFlag = grantFlag;
		if (grantFlag == 1) {
			setGrantFlagStr("N");
		} else if (grantFlag == 2) {
			setGrantFlagStr("T");
		} else if (grantFlag == 3) {
			setGrantFlagStr("C");
		} else {
			setGrantFlagStr("");
		}
	}

	/**
	 * メソッド名 : customerLoginIdのGetterメソッド
	 * 機能概要 : customerLoginIdを取得する。
	 * @return customerLoginId 企業管理者ログインID
	 */
	public String getCustomerLoginId() {
		return customerLoginId;
	}

	/**
	 * メソッド名 : customerLoginIdのSetterメソッド
	 * 機能概要 : customerLoginIdをセットする。
	 * @param customerLoginId 企業管理者ログインID
	 */
	public void setCustomerLoginId(String customerLoginId) {
		this.customerLoginId = customerLoginId;
	}

	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得する。
	 * @return vsysId Vsys-ID
	 */
	public String getVsysId() {
		return vsysId;
	}

	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdをセットする。
	 * @param vsysId Vsys-ID
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}

	/**
	 * メソッド名 : paLoginIdのGetterメソッド
	 * 機能概要 : paLoginIdを取得する。
	 * @return paLoginId PAログインID
	 */
	public String getPaLoginId() {
		return paLoginId;
	}

	/**
	 * メソッド名 : paLoginIdのSetterメソッド
	 * 機能概要 : paLoginIdをセットする。
	 * @param paLoginId PAログインID
	 */
	public void setPaLoginId(String paLoginId) {
		this.paLoginId = paLoginId;
	}

	/**
	 * メソッド名 : paPasswdのGetterメソッド
	 * 機能概要 : paPasswdを取得します。
	 * @return paPasswd PAログインパスワード
	 */
	public String getPaPasswd() {
		return paPasswd;
	}

	/**
	 * メソッド名 : paPasswdのSetterメソッド
	 * 機能概要 : paPasswdを設定します。
	 * @param paPasswd PAログインパスワード
	 */
	public void setPaPasswd(String paPasswd) {
		this.paPasswd = paPasswd;
	}

	/**
	 * メソッド名 : grantFlagStrのSetterメソッド
	 * 機能概要 : grantFlagStrをセットする。
	 * @param grantFlagStr ユーザ権限
	 */
	public void setGrantFlagStr(String grantFlagStr) {
		this.grantFlagStr = grantFlagStr;
	}

	/**
	 * メソッド名 : grantFlagStrのGetterメソッド
	 * 機能概要 : grantFlagStrを取得する。
	 * @return grantFlagStr ユーザ権限
	 */
	public String getGrantFlagStr() {
		return grantFlagStr;
	}

	// 20110520 ktakenaka@PROSITE add start
	/**
	 * メソッド名 : urlFilteringFlgのSetterメソッド
	 * 機能概要 : urlFilteringFlgをセットする。
	 * @param urlFilteringFlg ユーザ権限
	 */
	public void setUrlFilteringFlg(int urlFilteringFlg) {
		this.urlFilteringFlg = urlFilteringFlg;
	}

	/**
	 * メソッド名 : urlFilteringFlgのGetterメソッド
	 * 機能概要 : urlFilteringFlgを取得する。
	 * @return urlFilteringFlg ユーザ権限
	 */
	public int getUrlFilteringFlg() {
		return urlFilteringFlg;
	}
	/**
	 * メソッド名 : dmzFlgのSetterメソッド
	 * 機能概要 : dmzFlgをセットする。
	 * @param dmzFlg ユーザ権限
	 */
	public void setDmzFlg(int dmzFlg) {
		this.dmzFlg = dmzFlg;
	}

	/**
	 * メソッド名 : dmzFlgのGetterメソッド
	 * 機能概要 : dmzFlgを取得する。
	 * @return dmzFlg ユーザ権限
	 */
	public int getDmzFlg() {
		return dmzFlg;
	}

	/**
	 * メソッド名 : urlProfileMaxのSetterメソッド
	 * 機能概要 : urlProfileMaxをセットする。
	 * @param urlProfileMax
	 */
	public void setUrlProfileMax(int urlProfileMax) {
		this.urlProfileMax = urlProfileMax;
	}

	/**
	 * メソッド名 : urlProfileMaxのGetterメソッド
	 * 機能概要 : urlProfileMaxを取得する。
	 * @return urlProfileMax
	 */
	public int getUrlProfileMax() {
		return urlProfileMax;
	}

	/**
	 * メソッド名 : blocklistMaxのSetterメソッド
	 * 機能概要 : blocklistMaxをセットする。
	 * @param blocklistMax
	 */
	public void setBlocklistMax(int blocklistMax) {
		this.blocklistMax = blocklistMax;
	}

	/**
	 * メソッド名 : blocklistMaxのGetterメソッド
	 * 機能概要 : blocklistMaxを取得する。
	 * @return blocklistMax
	 */
	public int getBlocklistMax() {
		return blocklistMax;
	}

	/**
	 * メソッド名 : allowlistMaxのSetterメソッド
	 * 機能概要 : allowlistMaxをセットする。
	 * @param allowlistMax
	 */
	public void setAllowlistMax(int allowlistMax) {
		this.allowlistMax = allowlistMax;
	}

	/**
	 * メソッド名 : allowlistMaxのGetterメソッド
	 * 機能概要 : allowlistMaxを取得する。
	 * @return allowlistMax ユーザ権限
	 */
	public int getAllowlistMax() {
		return allowlistMax;
	}
	// 20110520 ktakenaka@PROSITE add start

	// 20130901 kkato@PROSITE add start
	/**
	 * メソッド名 : urlDbVersionのGetterメソッド
	 * 機能概要 : urlDbVersionを取得する。
	 * @return urlDbVersion
	 */
	public String getUrlDbVersion() {
		return urlDbVersion;
	}

	/**
	 * メソッド名 : urlDbVersionのSetterメソッド
	 * 機能概要 : urlDbVersionをセットする。
	 * @param urlDbVersion
	 */
	public void setUrlDbVersion(String urlDbVersion) {
		this.urlDbVersion = urlDbVersion;
	}

	/**
	 * メソッド名 : addressFqdnMaxのGetterメソッド
	 * 機能概要 : addressFqdnMaxを取得する。
	 * @return addressFqdnMax
	 */
	public int getAddressFqdnMax() {
		return addressFqdnMax;
	}

	/**
	 * メソッド名 : addressFqdnMaxのSetterメソッド
	 * 機能概要 : addressFqdnMaxをセットする。
	 * @param addressFqdnMax
	 */
	public void setAddressFqdnMax(int addressFqdnMax) {
		this.addressFqdnMax = addressFqdnMax;
	}
	// 20130901 kkato@PROSITE add end

}
