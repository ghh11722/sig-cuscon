/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CusconExtendValidator.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/03/04  m.narikawa@JCCH  初版作成
 * 2010/04/07  m.narikawa@JCCH  ログイン時のvalidatorエラーでログ出力に対応。
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.Field;
import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.Validator;
import org.apache.commons.validator.ValidatorAction;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.apache.struts.validator.FieldChecks;
import org.apache.struts.validator.Resources;

/**
 * クラス名 : CusconExtendValidator
 * 機能概要 : フレームワークにない入力チェックを行うための機能を提供
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class CusconExtendValidator implements Serializable {
	//
	private static final long serialVersionUID = 1L;
	// ログイン処理のパス
	private static final String ADMIN_LOGIN_PATH = "/admin/login/loginBL";
	private static final String CUSTOMER_LOGIN_PATH = "/customer/login/loginBL";
	// ログクラス
	private static Log log = LogFactory.getLog(CusconExtendValidator.class);

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 */
	public CusconExtendValidator() {
		super();
	}

	/**
	 * メソッド名 : 未入力チェック処理（ラッパー）
	 * 機能概要 : org.apache.struts.validator.FieldChecks のメソッドをコールする
	 * @param bean オブジェクト
	 * @param va va
	 * @param field field
	 * @param errors errors
	 * @param validator バリデータ
	 * @param request HTTPサーブレット
	 * @return 正常:true、異常:false
	 * @throws JspException
	 */
	public static boolean wrapperRequired(Object bean, ValidatorAction va, Field field, ActionMessages errors, Validator validator, HttpServletRequest request) throws JspException {

		// validator実行
		boolean result = FieldChecks.validateRequired(bean, va, field, errors, validator, request);

		// ログ出力
		if( result == false ) {
			MessageResources msg = Resources.getMessageResources(request);
			if( validator.getFormName().equals(ADMIN_LOGIN_PATH) == true){
				log.info(msg.getMessage("IK011009", field.getArg(0).getKey()));
			}
			else if( validator.getFormName().equals(CUSTOMER_LOGIN_PATH) == true){
				log.info(msg.getMessage("IC011013", field.getArg(0).getKey()));
			}
		}
		return result;
	}

	/**
	 * メソッド名 : 最大桁数チェック処理（ラッパー）
	 * 機能概要 : org.apache.struts.validator.FieldChecks のメソッドをコールする
	 * @param bean オブジェクト
	 * @param va va
	 * @param field field
	 * @param errors errors
	 * @param validator バリデータ
	 * @param request HTTPサーブレット
	 * @return 正常:true、異常:false
	 * @throws JspException
	 */
	public static boolean wrapperMaxLength(Object bean, ValidatorAction va, Field field, ActionMessages errors, Validator validator, HttpServletRequest request) throws JspException {

		// validator実行
		boolean result = FieldChecks.validateMaxLength(bean, va, field, errors, validator, request);

		// ログ出力
		if( result == false ) {
			MessageResources msg = Resources.getMessageResources(request);
			if( validator.getFormName().equals(ADMIN_LOGIN_PATH)==true){
				log.info(msg.getMessage("IK011010", field.getArg(0).getKey()));
			}
			else if( validator.getFormName().equals(CUSTOMER_LOGIN_PATH)==true){
				log.info(msg.getMessage("IC011014", field.getArg(0).getKey()));
			}
		}
		return result;
	}

	/**
	 * メソッド名 : パスワードチェック処理（ラッパー）
	 * 機能概要 : パスワード入力チェック処理 CusconPassword()メソッドをコールする
	 * @param bean オブジェクト
	 * @param va va
	 * @param field field
	 * @param errors errors
	 * @param validator バリデータ
	 * @param request HTTPサーブレット
	 * @return 正常:true、異常:false
	 * @throws JspException
	 */
	public static boolean wrapperPassword(Object bean, ValidatorAction va, Field field, ActionMessages errors, Validator validator, HttpServletRequest request) throws JspException {

		// validator実行
		boolean result = CusconPassword(bean, va, field, errors, validator, request);

		// ログ出力
		if( result == false ) {
			MessageResources msg = Resources.getMessageResources(request);
			if( validator.getFormName().equals(ADMIN_LOGIN_PATH)==true){
				log.info(msg.getMessage("IK011011"));
			}
			else if( validator.getFormName().equals(CUSTOMER_LOGIN_PATH)==true){
				log.info(msg.getMessage("IC011015"));
			}
		}
		return result;
	}

	/**
	 * メソッド名 : パスワード入力チェック
	 * 機能概要 : 英小文字、英大文字、数値、記号のうち3種類が含まれていることをチェック
	 * @param bean オブジェクト
	 * @param va va
	 * @param field field
	 * @param errors errors
	 * @param validator バリデータ
	 * @param request HTTPサーブレット
	 * @return 正常:true、異常:false
	 * @throws JspException
	 */
	public static boolean CusconPassword(Object bean, ValidatorAction va, Field field, ActionMessages errors, Validator validator, HttpServletRequest request) throws JspException {

		// 検証対象の切り出し
		String str = null;
		if (bean instanceof String) {
			str = (String) bean;
		} else {
			try {
				str = PropertyUtils.getProperty(bean, field.getProperty()).toString();
			} catch (Exception e) {
				throw new JspException(e);
			}
		}

		// 検証対象がnullか空文字の場合には、検証成功とする
		if (str == null || "".equals(str.trim())) {
			return true;
		}

		// 桁数チェック
		if (maxLength(str, 14) == false || minLength(str, 8) == false) {
			errors.add(field.getKey(), Resources.getActionMessage(request, va, field));
			return false;
		}

		// パスワード形式チェック(1)→半角英文字、半角数字、半角記号のみ
		Pattern pattern0 = Pattern.compile("^[a-zA-Z0-9!-/:-@\\[-\\`\\{-\\~]+$");
		Matcher matcher0 = pattern0.matcher(str);
		if (matcher0.matches() == false) {
			errors.add(field.getKey(), Resources.getActionMessage(request, va, field));
			return false;
		}

		// パスワード形式チェック(2)→半角英文字、半角数字、半角記号の中から3種類以上を含む
		if (matchPassword(str) == false) {
			errors.add(field.getKey(), Resources.getActionMessage(request, va, field));
			return false;
		}

		return true;
	}

	/**
	 * メソッド名 : パスワード形式チェック
	 * 機能概要 : 英小文字、英大文字、数値、記号のうち3種類が含まれていることをチェック
	 * @param value 精査文字列
	 * @return 正常:true、異常:false
	 */
	public static boolean matchPassword(String value) {
		if (!GenericValidator.isBlankOrNull(value)) {
			try {
				int matchCounter = 0;

				// 半角英小文字を含んでいるかチェック
				Pattern pattern1 = Pattern.compile(".*[a-z]+.*");
				Matcher matcher1 = pattern1.matcher(value);
				if (matcher1.matches() == true) {
					matchCounter++;
				}

				// 半角英大文字を含んでいるかチェック
				Pattern pattern2 = Pattern.compile("^.*[A-Z]+.*$");
				Matcher matcher2 = pattern2.matcher(value);
				if (matcher2.matches() == true) {
					matchCounter++;
				}

				// 半角数値を含んでいるかチェック
				Pattern pattern3 = Pattern.compile("^.*[0-9]+.*$");
				Matcher matcher3 = pattern3.matcher(value);
				if (matcher3.matches() == true) {
					matchCounter++;
				}

				// 半角記号を含んでいるかチェック
				Pattern pattern4 = Pattern.compile("^.*[!-/:-@\\[-\\`\\{-\\~]+.*$");
				Matcher matcher4 = pattern4.matcher(value);
				if (matcher4.matches() == true) {
					matchCounter++;
				}

				// 3つ以上マッチしていたらOK
				if (matchCounter >= 3) {
					return true;
				}

				return false;
			} catch (Exception e) {
				return false;
			}
		}
		return true;
	}

	/**
	 * メソッド名 : 文字列チェック
	 * 機能概要 : 文字列かどうかを判定
	 * @param o オブジェクト
	 * @return 正常:true、異常:false
	 */
	protected static boolean isString(Object o) {
		return o != null ? (java.lang.String.class).isInstance(o) : true;
	}

	/**
	 * メソッド名 : 入力チェック
	 * 機能概要 : 入力されているかどうかを判定
	 * @param value 文字列
	 * @return 正常:true、異常:false
	 */
	public static boolean required(String value) {
		if (GenericValidator.isBlankOrNull(value)) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * メソッド名 : 最大桁数チェック
	 * 機能概要 : 指定した桁数以下であるかを判定
	 * @param value 精査文字列
	 * @param maxlength 最大桁数
	 * @return 正常:true、異常:false
	 */
	public static boolean maxLength(String value, int maxlength) {
		if (!GenericValidator.isBlankOrNull(value)) {
			try {
				if (!GenericValidator.maxLength(value, maxlength)) {
					return false;
				}
			} catch (Exception e) {
				return false;
			}
		}
		return true;
	}

	/**
	 * メソッド名 : 最小桁数チェック
	 * 機能概要 : 指定した桁数以上であるかを判定
	 * @param value 精査文字列
	 * @param minlength 最小桁数
	 * @return 正常:true、異常:false
	 */
	public static boolean minLength(String value, int minlength) {
		if (!GenericValidator.isBlankOrNull(value)) {
			try {
				if (!GenericValidator.minLength(value, minlength)) {
					return false;
				}
			} catch (Exception e) {
				return false;
			}
		}
		return true;
	}

	/**
	 * メソッド名 : 正規表現チェック
	 * 機能概要 : 正規表現にマッチするかどうかを判定
	 * @param value 精査文字列
	 * @param regexp 正規表現
	 * @return 正常:true、異常:false
	 */
	public static boolean matchRegexp(String value, String regexp) {
		if (!GenericValidator.isBlankOrNull(value)) {
			try {
				if (!GenericValidator.matchRegexp(value, regexp)) {
					return false;
				}
			} catch (Exception e) {
				return false;
			}
		}
		return true;
	}

}
