/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ServiceGroupsLink.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/18     morisou          初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common.vo;

import java.io.Serializable;

/**
 * クラス名 : ServiceGroupsLink
 * 機能概要 : サービスグループリンクデータクラス
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/18
 *          新規作成
 * @see
 */
public class ServiceGroupsLink implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 2191074840529536133L;
	// サービスグループオブジェクトのSeqNoとのリンク
	private int srvGrpSeqNo;
	// オブジェクトタイプ
	private int objectType;
	// アドレスオブジェクト又はアドレスグループオブジェクトのSeqNoとのリンク
	private int linkSeqNo;
	/**
	 * メソッド名 : srvGrpSeqNoのGetterメソッド
	 * 機能概要 : srvGrpSeqNoを取得する。
	 * @return srvGrpSeqNo
	 */
	public int getSrvGrpSeqNo() {
		return srvGrpSeqNo;
	}
	/**
	 * メソッド名 : srvGrpSeqNoのSetterメソッド
	 * 機能概要 : srvGrpSeqNoをセットする。
	 * @param srvGrpSeqNo
	 */
	public void setSrvGrpSeqNo(int srvGrpSeqNo) {
		this.srvGrpSeqNo = srvGrpSeqNo;
	}
	/**
	 * メソッド名 : objectTypeのGetterメソッド
	 * 機能概要 : objectTypeを取得する。
	 * @return objectType
	 */
	public int getObjectType() {
		return objectType;
	}
	/**
	 * メソッド名 : objectTypeのSetterメソッド
	 * 機能概要 : objectTypeをセットする。
	 * @param objectType
	 */
	public void setObjectType(int objectType) {
		this.objectType = objectType;
	}
	/**
	 * メソッド名 : linkSeqNoのGetterメソッド
	 * 機能概要 : linkSeqNoを取得する。
	 * @return linkSeqNo
	 */
	public int getLinkSeqNo() {
		return linkSeqNo;
	}
	/**
	 * メソッド名 : linkSeqNoのSetterメソッド
	 * 機能概要 : linkSeqNoをセットする。
	 * @param linkSeqNo
	 */
	public void setLinkSeqNo(int linkSeqNo) {
		this.linkSeqNo = linkSeqNo;
	}
}
