/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddressGroupsLink.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/18     morisou          初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common.vo;

import java.io.Serializable;

/**
 * クラス名 : AddressGroupsLink
 * 機能概要 : アドレスグループリンクデータクラス
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/18
 *          新規作成
 * @see
 */
public class AddressGroupsLink implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 5162582295710660055L;
	// アドレスグループオブジェクトのSeqNoとのリンク
	private int adrGrpSeqNo = 0;
	// オブジェクトタイプ
	private int objectType = 0;
	// アドレスオブジェクト又はアドレスグループオブジェクトのSeqNoとのリンク
	private int linkSeqNo = 0;

	/**
	 * メソッド名 : adrGrpSeqNoのGetterメソッド
	 * 機能概要 : adrGrpSeqNoを取得する。
	 * @return adrGrpSeqNo
	 */
	public int getAdrGrpSeqNo() {
		return adrGrpSeqNo;
	}
	/**
	 * メソッド名 : adrGrpSeqNoのSetterメソッド
	 * 機能概要 : adrGrpSeqNoをセットする。
	 * @param adrGrpSeqNo
	 */
	public void setAdrGrpSeqNo(int adrGrpSeqNo) {
		this.adrGrpSeqNo = adrGrpSeqNo;
	}
	/**
	 * メソッド名 : objectTypeのGetterメソッド
	 * 機能概要 : objectTypeを取得する。
	 * @return objectType
	 */
	public int getObjectType() {
		return objectType;
	}
	/**
	 * メソッド名 : objectTypeのSetterメソッド
	 * 機能概要 : objectTypeをセットする。
	 * @param objectType
	 */
	public void setObjectType(int objectType) {
		this.objectType = objectType;
	}
	/**
	 * メソッド名 : linkSeqNoのGetterメソッド
	 * 機能概要 : linkSeqNoを取得する。
	 * @return linkSeqNo
	 */
	public int getLinkSeqNo() {
		return linkSeqNo;
	}
	/**
	 * メソッド名 : linkSeqNoのSetterメソッド
	 * 機能概要 : linkSeqNoをセットする。
	 * @param linkSeqNo
	 */
	public void setLinkSeqNo(int linkSeqNo) {
		this.linkSeqNo = linkSeqNo;
	}
}
