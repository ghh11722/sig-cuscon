/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : MessageAccessor.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/18     hiroyasu         初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common;

/**
 * クラス名 : MessageAccessor
 * 機能概要 :メッセージアクセスインタフェース
 * 備考 :
 * @author hiroyasu
 * @version 1.0 hiroyasu
 *          Created 2010/02/18
 *          新規作成
 * @see
 */
public interface MessageAccessor {
	/**
	 * メソッド名 : getMessage
	 * 機能概要 : メッセージキーに該当するメッセージ本文を取得する
	 * @param code メッセージキー
	 * @param args メッセージ中のプレースホルダに埋め込む文字列
	 * @return String メッセージ
	 */
	public String getMessage(String code, Object[] args);
	/**
	 * メソッド名 : getMessage
	 * 機能概要 : メッセージキーに該当するメッセージ本文を取得し、
	 *            ログフォーマットに整形する
	 * @param code メッセージキー
	 * @param args メッセージ中のプレースホルダに埋め込む文字列
	 * @param uvo  ログインユーザ情報
	 * @return String ログフォーマットメッセージ
	 */
	public String getMessage(String code, Object[] args, CusconUVO uvo);
}
