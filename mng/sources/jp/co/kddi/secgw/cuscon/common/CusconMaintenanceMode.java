/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CusconMaintenanceMode.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/03/04  tinn.ra@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common;

import jp.terasoluna.fw.dao.QueryDAO;

/**
 * クラス名 : CusconMaintenanceMode
 * 機能概要 : メンテナンスモードをチェックする
 * 備考 :0:通常 1:メンテナンスモード
 * @author tinn.ra@JCCH
 * @version 1.0 tinn.ra@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class CusconMaintenanceMode {
	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : QueryDAOはコンストラクタの引数として引き渡す
	 * @param queryDAO DAO
	 */
	public CusconMaintenanceMode(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : メンテナンスモード取得
	 * 機能概要 : メンテナンスモードをチェックする
	 * @return boolean チェックした結果
	 */
	public boolean isMaintenanceMode() {
		// システム情報のモード値を取得する
		int mode = queryDAO.executeForObject("getMode", null, java.lang.Integer.class);
		if (mode == 1) {
			return true;
		} else {
			return false;
		}
	}

}
