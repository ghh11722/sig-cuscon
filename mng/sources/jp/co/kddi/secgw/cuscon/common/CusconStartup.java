/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CusconStartup.java
 *
 * [変更履歴]
 * 日付       更新者            内容
 * 2010/02/23  m.narikawa@JCCH  初期版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * クラス名 : CusconStartup
 * 機能概要 : WEBアプリケーションの初期化時の処理
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/02/23
 *          新規作成
 * @see
 */
public class CusconStartup extends HttpServlet {

	//
	private static final long serialVersionUID = 1L;
	// ログクラス
	private static Log log = LogFactory.getLog(CusconStartup.class);
	// データソース名
	private static final String DATA_SOURCE_NAME = "java:comp/env/TerasolunaDataSource";
	// SQL
	private static final String DELETE_SESSION_K_SQL = "DELETE FROM T_Session_K;";
	private static final String DELETE_SESSION_C_SQL = "DELETE FROM T_Session_C;";

	/**
	 * メソッド名 : 初期化処理
	 * 機能概要 : セッション管理テーブルの削除
	 * @throws ServletException
	 * @see javax.servlet.GenericServlet#init()
	 */
	public void init() throws ServletException {

		try {
			// DB接続
			Context context = new InitialContext();
			DataSource ds = (DataSource) context.lookup(DATA_SOURCE_NAME);
			Connection dbConnection = ds.getConnection();

			// SQL実行（KDDI運用者セッション管理テーブル削除）
			PreparedStatement stm1 = dbConnection.prepareStatement(DELETE_SESSION_K_SQL);
			stm1.executeUpdate();
			stm1.close();

			// SQL実行（企業管理者セッション管理テーブル削除）
			PreparedStatement stm2 = dbConnection.prepareStatement(DELETE_SESSION_C_SQL);
			stm2.executeUpdate();
			stm2.close();

			// DB切断
			dbConnection.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("DBアクセスエラー : " + e.getMessage());
		}
		log.debug("セッション管理テーブルをクリアしました。");

	}
}
