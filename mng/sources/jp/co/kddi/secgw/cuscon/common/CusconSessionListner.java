/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名	: CusconSessionListner.java
 *
 * [変更履歴]
 * 日付 	   更新者		内容
 * 2010/03/04  h.kubo@JCCH	初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import javax.servlet.http.HttpSession;

import java.sql.*;

import javax.naming.*;
import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * クラス名 : CusconSessionListner
 * 機能概要 : セッションタイムアウト時の処理を行う
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *			Created 2010/03/04
 *			新規作成
 */
public class CusconSessionListner implements HttpSessionListener {

	// ログクラス
	private static Log log = LogFactory.getLog(CusconSessionListner.class);
	// データソース名
	private static final String DATA_SOURCE_NAME = "java:comp/env/TerasolunaDataSource";
	// SQL
	private static final String DELETE_SESSION_K_SQL = "DELETE FROM t_session_k WHERE login_id = ?;";
	private static final String DELETE_SESSION_C_SQL = "DELETE FROM t_session_c WHERE login_id = ?;";

	/**
	 * メソッド名 : セッション開始処理
	 * 機能概要 : セッション開始時に実行される処理
	 * @param param セッション変更通知イベントクラス
	 * @see javax.servlet.http.HttpSessionListener#sessionCreated(javax.servlet.http.HttpSessionEvent)
	 */
	@Override
	public void sessionCreated(HttpSessionEvent param) {
	}

	/**
	 * メソッド名 : セッション終了処理
	 * 機能概要 : セッションタイムアウト時の処理を行う
	 * @param param セッション変更通知イベントクラス
	 * @see javax.servlet.http.HttpSessionListener#sessionDestroyed(javax.servlet.http.HttpSessionEvent)
	 */
	@Override
	public void sessionDestroyed(HttpSessionEvent param) {

		log.info("セッション終了イベント");

		// セッション情報の取得
		HttpSession session = param.getSession();
		CusconUVO uvo = (CusconUVO)session.getAttribute("USER_VALUE_OBJECT");

		// CusconUVO情報なし
		if (uvo == null) {
			return;
		}

		// ログインIDの取得
		String login_id = uvo.getLoginId();
		if (login_id == null) {
			return;
		}

		log.info("ログインID:" + login_id);

		// ユーザ権限(NOC:1,TSC:2,企業管理者:3)の取得
		long grantFlag = uvo.getGrantFlag();
		if (!(grantFlag == 1 || grantFlag == 2 || grantFlag == 3)) {
			return;
		}

		try {
			// DB接続
			Context context = new InitialContext();
			DataSource ds = (DataSource) context.lookup(DATA_SOURCE_NAME);
			Connection dbConnection = ds.getConnection();

			// 1. KDDI運用管理者セッション管理テーブル削除
			PreparedStatement stm;
			if (grantFlag == 1 || grantFlag == 2) {
				stm = dbConnection.prepareStatement(DELETE_SESSION_K_SQL);
				stm.setString(1, uvo.getLoginId());
			}
			else {
				stm = dbConnection.prepareStatement(DELETE_SESSION_C_SQL);
				stm.setString(1, uvo.getLoginId());
			}
			// SQL実行
			stm.executeUpdate();
			stm.close();

			// DB切断
			dbConnection.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}
