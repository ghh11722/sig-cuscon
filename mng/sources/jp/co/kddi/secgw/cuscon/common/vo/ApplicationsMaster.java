/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ApplicationsMaster.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/05     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common.vo;

/**
 * クラス名 : ApplicationsMaster
 * 機能概要 : アプリケーションオブジェクトマスタデータクラス
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/05
 *          新規作成
 * @see
 */
public class ApplicationsMaster {

	// シリアルバージョンID
	private static final long serialVersionUID = -2017867093485785445L;
	// シーケンス番号
	private int seqNo = 0;
	// オブジェクト名
	private String name = null;
	// カテゴリマスタのSeqNoとのリンク
	private int CategorySeqNo = 0;
	// サブカテゴリマスタのSeqNoとのリンク
	private int SubCategorySeqNo = 0;
	// 脅威度：1,2,3,4,5 低→高
	private int risk = 0;
	// テクノロジマスタのSeqNoとのリンク
	private int TechnologySeqNo = 0;

	/**
	 * メソッド名 : seqNoのGetterメソッド
	 * 機能概要 : seqNoを取得する。
	 * @return seqNo
	 */
	public int getSeqNo() {
		return seqNo;
	}
	/**
	 * メソッド名 : seqNoのSetterメソッド
	 * 機能概要 : seqNoをセットする。
	 * @param seqNo
	 */
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : categorySeqNoのGetterメソッド
	 * 機能概要 : categorySeqNoを取得する。
	 * @return categorySeqNo
	 */
	public int getCategorySeqNo() {
		return CategorySeqNo;
	}
	/**
	 * メソッド名 : categorySeqNoのSetterメソッド
	 * 機能概要 : categorySeqNoをセットする。
	 * @param categorySeqNo
	 */
	public void setCategorySeqNo(int categorySeqNo) {
		CategorySeqNo = categorySeqNo;
	}
	/**
	 * メソッド名 : subCategorySeqNoのGetterメソッド
	 * 機能概要 : subCategorySeqNoを取得する。
	 * @return subCategorySeqNo
	 */
	public int getSubCategorySeqNo() {
		return SubCategorySeqNo;
	}
	/**
	 * メソッド名 : subCategorySeqNoのSetterメソッド
	 * 機能概要 : subCategorySeqNoをセットする。
	 * @param subCategorySeqNo
	 */
	public void setSubCategorySeqNo(int subCategorySeqNo) {
		SubCategorySeqNo = subCategorySeqNo;
	}
	/**
	 * メソッド名 : riskのGetterメソッド
	 * 機能概要 : riskを取得する。
	 * @return risk
	 */
	public int getRisk() {
		return risk;
	}
	/**
	 * メソッド名 : riskのSetterメソッド
	 * 機能概要 : riskをセットする。
	 * @param risk
	 */
	public void setRisk(int risk) {
		this.risk = risk;
	}
	/**
	 * メソッド名 : technologySeqNoのGetterメソッド
	 * 機能概要 : technologySeqNoを取得する。
	 * @return technologySeqNo
	 */
	public int getTechnologySeqNo() {
		return TechnologySeqNo;
	}
	/**
	 * メソッド名 : technologySeqNoのSetterメソッド
	 * 機能概要 : technologySeqNoをセットする。
	 * @param technologySeqNo
	 */
	public void setTechnologySeqNo(int technologySeqNo) {
		TechnologySeqNo = technologySeqNo;
	}
	/**
	 * メソッド名 : serialversionuidのGetterメソッド
	 * 機能概要 : serialversionuidを取得する。
	 * @return serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
