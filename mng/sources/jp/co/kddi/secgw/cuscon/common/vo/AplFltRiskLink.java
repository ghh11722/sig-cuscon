/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AplFltRiskLink.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/18     morisou          初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common.vo;

import java.io.Serializable;

/**
 * クラス名 : AplFltRiskLink
 * 機能概要 : アプリケーションフィルタリスクリンクデータクラス
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/18
 *          新規作成
 * @see
 */
public class AplFltRiskLink implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = -5736040296287843868L;
	// アプリケーションフィルタオブジェクトのSeqNoとのリンク
	private int aplFltSeqNo = 0;
	// サブカテゴリマスタのSeqNoとのリンク
	private int categorySeqNo = 0;
	/**
	 * メソッド名 : aplFltSeqNoのGetterメソッド
	 * 機能概要 : aplFltSeqNoを取得する。
	 * @return aplFltSeqNo
	 */
	public int getAplFltSeqNo() {
		return aplFltSeqNo;
	}
	/**
	 * メソッド名 : aplFltSeqNoのSetterメソッド
	 * 機能概要 : aplFltSeqNoをセットする。
	 * @param aplFltSeqNo
	 */
	public void setAplFltSeqNo(int aplFltSeqNo) {
		this.aplFltSeqNo = aplFltSeqNo;
	}
	/**
	 * メソッド名 : categorySeqNoのGetterメソッド
	 * 機能概要 : categorySeqNoを取得する。
	 * @return categorySeqNo
	 */
	public int getCategorySeqNo() {
		return categorySeqNo;
	}
	/**
	 * メソッド名 : categorySeqNoのSetterメソッド
	 * 機能概要 : categorySeqNoをセットする。
	 * @param categorySeqNo
	 */
	public void setCategorySeqNo(int categorySeqNo) {
		this.categorySeqNo = categorySeqNo;
	}
}
