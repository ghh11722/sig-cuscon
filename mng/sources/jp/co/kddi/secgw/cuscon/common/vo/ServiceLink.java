/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ServiceLink.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/18     komakiys         初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common.vo;

import java.io.Serializable;

/**
 * クラス名 : ServiceLink
 * 機能概要 : サービスリンクデータクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/18
 *          新規作成
 * @see
 */
public class ServiceLink implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 401237711604192002L;

	// セキュリティポリシー一覧のSeqNoとのリンク
	private int secRlsSeqNo = 0;
	// オブジェクトタイプ
	private int objectType = 0;
	// サービスオブジェクトのSeqNoとのリンク
	private int linkSeqNo = 0;


	/**
	 * メソッド名 : secRlsSeqNoのGetterメソッド
	 * 機能概要 : secRlsSeqNoを取得する。
	 * @return secRlsSeqNo
	 */
	public int getSecRlsSeqNo() {
		return secRlsSeqNo;
	}
	/**
	 * メソッド名 : secRlsSeqNoのSetterメソッド
	 * 機能概要 : secRlsSeqNoをセットする。
	 * @param secRlsSeqNo
	 */
	public void setSecRlsSeqNo(int secRlsSeqNo) {
		this.secRlsSeqNo = secRlsSeqNo;
	}
	/**
	 * メソッド名 : objectTypeのGetterメソッド
	 * 機能概要 : objectTypeを取得する。
	 * @return objectType
	 */
	public int getObjectType() {
		return objectType;
	}
	/**
	 * メソッド名 : objectTypeのSetterメソッド
	 * 機能概要 : objectTypeをセットする。
	 * @param objectType
	 */
	public void setObjectType(int objectType) {
		this.objectType = objectType;
	}
	/**
	 * メソッド名 : linkSeqNoのGetterメソッド
	 * 機能概要 : linkSeqNoを取得する。
	 * @return linkSeqNo
	 */
	public int getLinkSeqNo() {
		return linkSeqNo;
	}
	/**
	 * メソッド名 : linkSeqNoのSetterメソッド
	 * 機能概要 : linkSeqNoをセットする。
	 * @param linkSeqNo
	 */
	public void setLinkSeqNo(int linkSeqNo) {
		this.linkSeqNo = linkSeqNo;
	}
}
