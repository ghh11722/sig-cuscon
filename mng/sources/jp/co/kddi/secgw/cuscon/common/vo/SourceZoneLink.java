/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SourceZoneLink.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/18     komakiys         初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.common.vo;

import java.io.Serializable;

/**
 * クラス名 : SourceZoneLink
 * 機能概要 : 出所ゾーンリンクデータクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/18
 *          新規作成
 * @see
 */
public class SourceZoneLink implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 4471801468505446988L;

	// セキュリティポリシー一覧のSeqNoとのリンク
	private int secRlsSeqNo = 0;
	// ゾーンマスタのSeqNoとのリンク
	private int zoneLinkSeqNo = 0;

	/**
	 * メソッド名 : secRlsSeqNoのGetterメソッド
	 * 機能概要 : secRlsSeqNoを取得する。
	 * @return secRlsSeqNo
	 */
	public int getSecRlsSeqNo() {
		return secRlsSeqNo;
	}
	/**
	 * メソッド名 : secRlsSeqNoのSetterメソッド
	 * 機能概要 : secRlsSeqNoをセットする。
	 * @param secRlsSeqNo
	 */
	public void setSecRlsSeqNo(int secRlsSeqNo) {
		this.secRlsSeqNo = secRlsSeqNo;
	}
	/**
	 * メソッド名 : zoneLinkSeqNoのGetterメソッド
	 * 機能概要 : zoneLinkSeqNoを取得する。
	 * @return zoneLinkSeqNo
	 */
	public int getZoneLinkSeqNo() {
		return zoneLinkSeqNo;
	}
	/**
	 * メソッド名 : zoneLinkSeqNoのSetterメソッド
	 * 機能概要 : zoneLinkSeqNoをセットする。
	 * @param zoneLinkSeqNo
	 */
	public void setZoneLinkSeqNo(int zoneLinkSeqNo) {
		this.zoneLinkSeqNo = zoneLinkSeqNo;
	}


}
