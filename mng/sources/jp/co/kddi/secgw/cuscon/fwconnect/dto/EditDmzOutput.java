/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditDmzOutput.java
 *
 * [変更履歴]
 * 日付        更新者             内容
 * 2011/05/16  ktakenaka@PROSITE  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwconnect.dto;

/**
 * クラス名 : EditDmzOutput
 * 機能概要 : DMZ選択ビジネスロジックの出力定義Bean
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/05/16
 *          新規作成
 */
public class EditDmzOutput {

	// ファイアウォールID
	private String firewallId = null;

	// DMZフラグ
	private int dmzFlg = 0;

	/**
	 * メソッド名 : firewallIdのGetterメソッド
	 * 機能概要 : firewallIdを取得します。
	 * @return firewallId Firewall-ID
	 */
	public String getFirewallId() {
		return firewallId;
	}

	/**
	 * メソッド名 : firewallIdのSetterメソッド
	 * 機能概要 : firewallIdを設定します。
	 * @param firewallId Firewall-ID
	 */
	public void setFirewallId(String firewallId) {
		this.firewallId = firewallId;
	}

	/**
	 * メソッド名 : dmzFlgのGetterメソッド
	 * 機能概要 : dmzFlgを取得する。
	 * @return dmzFlg
	 */
	public int getDmzFlg() {
		return dmzFlg;
	}
	/**
	 * メソッド名 : dmzFlgのSetterメソッド
	 * 機能概要 : dmzFlgをセットする。
	 * @param dmzFlg
	 */
	public void setDmzFlg(int dmzFlg) {
		this.dmzFlg = dmzFlg;
	}
}
