/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CustomerChoiceBLogic.java
 *
 * [変更履歴]
 * 日付        更新者             内容
 * 2010/03/04  m.narikawa@JCCH    初版作成
 * 2010/07/26  m.narikawa@JCCH    顧客企業管理者のログインチェックを行うようにする
 * 2010/05/20  ktakenaka@PROSITE  Webフィルタ利用フラグ,DMZ利用フラグをセッション情報(UVO)にセット
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwconnect.blogic;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import jp.co.kddi.secgw.cuscon.common.CusconFWSet;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwconnect.dto.CustomerChoiceInput;
import jp.co.kddi.secgw.cuscon.fwconnect.vo.CustomerChoiceRecord;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : CustomerChoiceBLogic
 * 機能概要 : 企業管理者選択ビジネスロジック
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class CustomerChoiceBLogic implements BLogic<CustomerChoiceInput> {
	// DAO
	private QueryDAO queryDAO = null;
	private UpdateDAO updateDAO = null;
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(CustomerChoiceBLogic.class);

	/**
	 * メソッド名 : 企業管理者選択処理
	 * 機能概要 : 企業管理者を選択する
	 * @param param 企業管理者選択の入力情報を保持したMap
	 * @return 企業管理者選択処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(CustomerChoiceInput param) {

		log.debug("企業選択処理");

		BLogicResult result = new BLogicResult();
		BLogicMessages errors = new BLogicMessages();

		// 不要なセッションデータを削除する。
		log.debug("不要なセッションデータを削除");
		if(param.getUvo()!= null){
			param.getUvo().setCustomerLoginId(null);
			param.getUvo().setVsysId("");
			param.getUvo().setPaLoginId(null);
			param.getUvo().setPaPasswd(null);
		}

		// 1) 企業管理者情報検索
		log.debug("1) 企業管理者情報検索");
		List<CustomerChoiceRecord> customers;
		try {
			customers = queryDAO.executeForObjectList("CustomerChoiceBLogic-1", param);
			if (customers.size() == 0) {
				// エラー : 検索Hit件数0
				log.debug("1) : 検索Hitなし");
				Object[] objectgArray = {param.getCustomerLoginId()};
				log.error(messageAccessor.getMessage("EK031002", objectgArray, param.getUvo()));
				errors.add("message", new BLogicMessage("DK030002"));
				result.setErrors(errors);
				result.setResultString("failure");
				return result;
			}

			// 1)-Ex1 顧客企業ログインチェック (mod)2010/07/26
			// 企業管理者セッション管理情報を取得する
			log.debug("1)-Ex1 多重ログインチェック");
			Integer count= queryDAO.executeForObject("CustomerLoginBLogic-3", param.getCustomerLoginId(), Integer.class);
			if (count.intValue() != 0) {
				// 準正常 : 顧客企業ログイン中
				Object[] objectgArray = {param.getCustomerLoginId()};
				log.info(messageAccessor.getMessage("IK031001", objectgArray, param.getUvo()));
				errors.add("message", new BLogicMessage("DK030004"));
				result.setErrors(errors);
				result.setResultString("failure");
				return result;
			}
			log.debug("1)-Ex1 OK");

			// 2) 企業管理者情報検索
			// セッション情報(UVO)に企業ログインID、VsysID、PAログインID、PAログインパスワードをセット
			CusconUVO uvo = param.getUvo();
			uvo.setCustomerLoginId(customers.get(0).getLoginId());
			uvo.setVsysId(customers.get(0).getVsysId());
			uvo.setPaLoginId(customers.get(0).getPaLoginId());
			uvo.setPaPasswd(customers.get(0).getPaLoginPwd());
			// 20110520 ktakenaka@PROSITE add start
			uvo.setUrlFilteringFlg(customers.get(0).getUrlFilteringFlg());
			uvo.setDmzFlg(customers.get(0).getDmzFlg());
			uvo.setUrlProfileMax(customers.get(0).getUrlProfileMax());
			uvo.setBlocklistMax(customers.get(0).getBlocklistMax());
			uvo.setAllowlistMax(customers.get(0).getAllowlistMax());
			// 20110520 ktakenaka@PROSITE add end

			// 3) ポリシー情報取得
			log.debug("3) ポリシー情報取得");
			CusconFWSet fwSet = new CusconFWSet();
			fwSet.setFWInfo(updateDAO, queryDAO, uvo);

		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK031003", objectgArray, param.getUvo()));
			errors.add("message", new BLogicMessage("DK030003"));
			result.setErrors(errors);
			result.setResultString("failure");
			return result;
		}

		// 4) レスポンス返却
		result.setResultString("success");
		log.debug("4) レスポンス返却");
		return result;
	}

	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * 機能概要 : queryDAOを取得します。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOを設定します。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : updateDAOのGetterメソッド
	 * 機能概要 : updateDAOを取得する。
	 * @return updateDAO DAO
	 */
	public UpdateDAO getUpdateDAO() {
		return updateDAO;
	}

	/**
	 * メソッド名 : updateDAOのSetterメソッド
	 * 機能概要 : updateDAOをセットする。
	 * @param updateDAO DAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		this.updateDAO = updateDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}
}
