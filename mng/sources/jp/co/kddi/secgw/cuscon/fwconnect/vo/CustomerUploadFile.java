/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CustomerUploadFile.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/03/04  m.narikawa@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwconnect.vo;

import java.io.Serializable;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;

import org.apache.struts.upload.FormFile;

/**
 * クラス名 : CustomerUploadFile
 * 機能概要 : アップロードファイル定義Bean
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class CustomerUploadFile implements Serializable {
	// UserValueObject
	private CusconUVO uvo = null;
	// シリアルバージョンID
	private static final long serialVersionUID = 4552157371398919631L;
	// アップロードファイル
	private FormFile fileup = null;
	// アップロードファイル名
	private String fileName = null;
	// アップロードファイルサイズ
	private String fileSize = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo UserValueObject
	 */
	public CusconUVO getUvo() {
		return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo UserValueObject
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}

	/**
	 * メソッド名 : fileupのGetterメソッド
	 * 機能概要 : fileupを取得します。
	 * @return fileup アップロードファイル
	 */
	public FormFile getFileup() {
		return fileup;
	}

	/**
	 * メソッド名 : fileupのSetterメソッド
	 * 機能概要 : fileupを設定します。
	 * @param fileup アップロードファイル
	 */
	public void setFileup(FormFile fileup) {
		this.fileup = fileup;
	}

	/**
	 * メソッド名 : fileNameのGetterメソッド
	 * 機能概要 : fileNameを取得します。
	 * @return fileName アップロードファイル名
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * メソッド名 : fileNameのSetterメソッド
	 * 機能概要 : fileNameを設定します。
	 * @param fileName アップロードファイル名
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * メソッド名 : fileSizeのGetterメソッド
	 * 機能概要 : fileSizeを取得します。
	 * @return fileSize アップロードファイルサイズ
	 */
	public String getFileSize() {
		return fileSize;
	}

	/**
	 * メソッド名 : fileSizeのSetterメソッド
	 * 機能概要 : fileSizeを設定します。
	 * @param fileSize アップロードファイルサイズ
	 */
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

}
