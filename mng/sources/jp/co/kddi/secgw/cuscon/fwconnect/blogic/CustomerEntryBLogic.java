/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CustomerEntryInput.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/03/04  m.narikawa@JCCH  初版作成
 * 2010/04/01  t.Sutoh@JCCH     CusconRepDeleteクラスのコールを追加
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwconnect.blogic;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import jp.co.kddi.secgw.cuscon.common.CusconFWDelete;
import jp.co.kddi.secgw.cuscon.common.CusconRepDelete;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwconnect.dto.CustomerEntryInput;
import jp.co.kddi.secgw.cuscon.fwconnect.dto.CustomerEntryOutput;
import jp.co.kddi.secgw.cuscon.fwconnect.vo.CustomerDeleteRecord;
import jp.co.kddi.secgw.cuscon.fwconnect.vo.CustomerEntryRecord;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : CustomerEntryBLogic
 * 機能概要 : 企業追加／削除処理ビジネスロジック
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class CustomerEntryBLogic implements BLogic<CustomerEntryInput> {
	// 操作フラグ
	private static final String OPE_FLG_ADD = "ADD";
	private static final String OPE_FLG_DEL = "DEL";
	// 確認画面のボタン
	private static final String CONFIRM_YES = "OK";
	// DAO
	private QueryDAO queryDAO = null;
	private UpdateDAO updateDAO = null;
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(CustomerEntryBLogic.class);

	/**
	 * メソッド名 : 企業追加削除処理
	 * 機能概要 : 企業の追加削除を行う
	 * @param param 企業追加／削除ビジネスロジックの入力情報を保持したMap
	 * @return 企業追加／削除処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(CustomerEntryInput param) {

		log.debug("企業追加削除処理");

		BLogicResult result = new BLogicResult();
		CustomerEntryOutput output = new CustomerEntryOutput();
		BLogicMessages errors = new BLogicMessages();
		BLogicMessages message = new BLogicMessages();
		String backVsysId;
		// 1）セッションデータ読み込み
		log.debug("1）セッションデータ読み込み");
		// セッション情報（UploadCSV）から全情報を取得する。
		List<CustomerEntryRecord> uploadCSV = param.getUploadCSV();

		// セッション情報がなかったら不正アクセス
		if (uploadCSV == null) {
			log.error(messageAccessor.getMessage("EK101006", null, param.getUvo()));
			errors.add("message", new BLogicMessage("DK100006"));
			message.add("message", new BLogicMessage("DK100015"));
			result.setErrors(errors);
			result.setMessages(message);
			result.setResultObject(output);
			result.setResultString("failure");
			return result;
		}

		// Cancelボタン(OK以外)が押下された場合、セッションデータを破棄して終了
		String confirm = param.getConfirm();
		if (confirm.equals(CONFIRM_YES) != true) {
			log.debug("1）: Cancelボタン押下");
			result.setMessages(message);
			output.setUploadCSV(uploadCSV);
			result.setResultObject(output);
			result.setResultString("cancel");
			return result;
		}

		// uvoのVsysIDを保存
		backVsysId = param.getUvo().getVsysId();

		// ※※※ 1で取得したデータの件数分繰り返し ※※※
		for (int i = 0; i < uploadCSV.size(); i++) {
			CustomerEntryRecord entry = uploadCSV.get(i);
			// 操作フラグが「登録」の場合
			if (entry.getOpeFlg().equals(OPE_FLG_ADD) == true) {
				// 登録処理
				try {
					log.debug("企業登録 : Firewall-ID=" + entry.getFirewallId());
					updateDAO.execute("CustomerEntryBLogic-1", entry);
				} catch (Exception e) {
					e.printStackTrace();
					// トランザクションロールバック
					TransactionUtil.setRollbackOnly();
					// データベースへのアクセスに失敗した場合は、エラーを出力する
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage("FK101007", objectgArray, param.getUvo()));
					errors.add("message", new BLogicMessage("DK100007"));
					message.add("message", new BLogicMessage("DK100015"));
					result.setErrors(errors);
					result.setMessages(message);
					output.setUploadCSV(uploadCSV);
					result.setResultObject(output);
					result.setResultString("failure");
					return result;
				}
			}
			// 操作フラグが「削除」の場合
			else if (entry.getOpeFlg().equals(OPE_FLG_DEL) == true) {
				// 削除処理
				try {
					log.debug("企業削除 : Firewall-ID=" + entry.getFirewallId());
					// ログインID、VsysIDをDBから取得する
					CustomerDeleteRecord delRecord = queryDAO.executeForObject("CustomerEntryBLogic-3", entry.getFirewallId(), jp.co.kddi.secgw.cuscon.fwconnect.vo.CustomerDeleteRecord.class);
					// 関連テーブルを削除する
					// (企業管理者セッション管理テーブル)
					updateDAO.execute("CustomerEntryBLogic-4", delRecord);
					// (企業管理者旧パスワード)
					updateDAO.execute("CustomerEntryBLogic-5", delRecord);
					// (企業管理者ロックアウト管理テーブル)
					updateDAO.execute("CustomerEntryBLogic-6", delRecord);
					// (トラフィックログ)
					updateDAO.execute("CustomerEntryBLogic-7", delRecord);
					// (IDS/IPSログ)
					updateDAO.execute("CustomerEntryBLogic-8", delRecord);
//2011/07/28 add start
					// (Webフィルタログ)
					updateDAO.execute("CustomerEntryBLogic-UrlfilterLogDelete", delRecord);
					// (Webウィルスチェックログ)
					updateDAO.execute("CustomerEntryBLogic-VirusLogDelete", delRecord);
					// (スパイウェアチェックログ)
					updateDAO.execute("CustomerEntryBLogic-SpywareLogDelete", delRecord);
//2011/07/28 add end
					// (その他のテーブル)
					param.getUvo().setVsysId(entry.getVsysId());
					CusconFWDelete fwDelete = new CusconFWDelete();
					fwDelete.deleteFWInfo(0, updateDAO, queryDAO, param.getUvo());
					fwDelete.deleteFWInfo(1, updateDAO, queryDAO, param.getUvo());
					fwDelete.deleteFWInfo(2, updateDAO, queryDAO, param.getUvo());
					fwDelete.deleteFWInfo(3, updateDAO, queryDAO, param.getUvo());
					CusconRepDelete repDelete = new CusconRepDelete();
					repDelete.deleteRepInfo(updateDAO, param.getUvo());
					param.getUvo().setVsysId(backVsysId);

					// 企業管理者テーブルを削除する
					updateDAO.execute("CustomerEntryBLogic-2", delRecord);
				} catch (Exception e) {
					e.printStackTrace();
					// トランザクションロールバック
					TransactionUtil.setRollbackOnly();
					// ・データベースへのアクセスに失敗した場合は、エラーを出力する
					param.getUvo().setVsysId(backVsysId);
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage("FK101008", objectgArray, param.getUvo()));
					errors.add("message", new BLogicMessage("DK100008"));
					message.add("message", new BLogicMessage("DK100015"));
					result.setErrors(errors);
					result.setMessages(message);
					output.setUploadCSV(uploadCSV);
					result.setResultObject(output);
					result.setResultString("failure");
					return result;
				}
			}
		}
		// ※※※ 1で取得したデータの件数分繰り返し ※※※

		// 2）レスポンス返却
		log.debug("2）レスポンス返却");
		// フレームワークによりJSPに出力するために、処理結果をビジネスロジックの出力クラスに設定する。
		// ※終了時にはセッション情報（UploadCSV）を削除すること。
		message.add("message", new BLogicMessage("DK100014"));
		result.setMessages(message);
		output.setUploadCSV(uploadCSV);
		result.setResultObject(output);
		result.setResultString("success");
		return result;
	}

	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * 機能概要 : queryDAOを取得します。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOを設定します。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : updateDAOのGetterメソッド
	 * 機能概要 : updateDAOを取得します。
	 * @return updateDAO DAO
	 */
	public UpdateDAO getUpdateDAO() {
		return updateDAO;
	}

	/**
	 * メソッド名 : updateDAOのSetterメソッド
	 * 機能概要 : updateDAOを設定します。
	 * @param updateDAO DAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		this.updateDAO = updateDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}
}
