/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AdminMainOutput.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/03/04  m.narikawa@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwconnect.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwconnect.vo.AdminMainRecord;

/**
 * クラス名 : AdminMainOutput
 * 機能概要 : KDDI運用者メイン画面作成ビジネスロジックの出力定義Bean
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class AdminMainOutput {
	// 企業管理者一覧
	private List<AdminMainRecord> customers = null;

	/**
	 * メソッド名 : customersのGetterメソッド
	 * 機能概要 : customersを取得します。
	 * @return customers 企業管理者一覧
	 */
	public List<AdminMainRecord> getCustomers() {
		return customers;
	}

	/**
	 * メソッド名 : customersのSetterメソッド
	 * 機能概要 : customersを設定します。
	 * @param customers 企業管理者一覧
	 */
	public void setCustomers(List<AdminMainRecord> customers) {
		this.customers = customers;
	}
}
