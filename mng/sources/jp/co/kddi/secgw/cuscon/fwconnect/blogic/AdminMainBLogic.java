/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AdminMainBLogic.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/03/04  m.narikawa@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwconnect.blogic;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwconnect.dto.AdminMainInput;
import jp.co.kddi.secgw.cuscon.fwconnect.dto.AdminMainOutput;
import jp.co.kddi.secgw.cuscon.fwconnect.vo.AdminMainRecord;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : AdminMainBLogic
 * 機能概要 : KDDI運用者メイン画面作成ビジネスロジック
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class AdminMainBLogic implements BLogic<AdminMainInput> {
	// DAO
	private QueryDAO queryDAO = null;
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(AdminMainBLogic.class);

	/**
	 * メソッド名 : KDDI運用者メイン作成処理
	 * 機能概要 : KDDI運用者メイン画面を作成する
	 * @param param KDDI運用者メイン画面作成の入力情報を保持したMap
	 * @return KDDI運用者メイン作成処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(AdminMainInput param) {

		log.debug("KDDI運用者メイン作成処理");

		BLogicResult result = new BLogicResult();
		BLogicMessages errors = new BLogicMessages();
		AdminMainOutput out = new AdminMainOutput();

		// 不要なセッションデータを削除する。
		log.debug("UVOから不要なデータを削除");
		if(param.getUvo()!= null){
			param.getUvo().setCustomerLoginId(null);
			param.getUvo().setVsysId("");
			param.getUvo().setPaLoginId(null);
			param.getUvo().setPaPasswd(null);
		}

		// 2) 企業管理者検索
		log.debug("2) 企業管理者検索");
		// 企業管理者テーブルより一覧情報を取得する
		List<AdminMainRecord> customers = null;
		try {
			customers = queryDAO.executeForObjectList("AdminMainBLogic-1", param.getCryptKey());
			if( customers.size() == 0 ){
				// ・検索Hitしない場合は、一覧のタイトルのみを表示する。
				log.debug("検索Hitなし");
				customers = new ArrayList<AdminMainRecord>();
				out.setCustomers(customers);
				result.setResultObject(out);
				result.setResultString("success");
				return result;
			}
		} catch (Exception e) {
			// ・不備があった場合は、エラーを出力する。
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : 企業管理者検索失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK031001", objectgArray, param.getUvo()));
			errors.add("message", new BLogicMessage("DK030001"));
			result.setErrors(errors);
			customers = new ArrayList<AdminMainRecord>();
			out.setCustomers(customers);
			result.setResultObject(out);
			result.setResultString("failure");
			return result;
		}

		// 3) レスポンス返却
		log.debug("3) レスポンス返却");
		// フレームワークによりJSPに出力するために、検索結果をビジネスロジックの出力クラスに設定する。
		out.setCustomers(customers);
		// ビジネスロジックの出力クラスに結果を設定する
		result.setResultObject(out);
		result.setResultString("success");

		return result;
	}

	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * 機能概要 : queryDAOを取得します。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOを設定します。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}
}
