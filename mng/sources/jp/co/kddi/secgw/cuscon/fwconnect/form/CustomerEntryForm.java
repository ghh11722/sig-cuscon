/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CustomerEntryForm.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/03/04  m.narikawa@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwconnect.form;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwconnect.vo.CustomerEntryRecord;
import jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx;

/**
 * クラス名 : CustomerEntryForm
 * 機能概要 : 企業追加／削除画面アクションフォーム
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 * @see jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx
 */
public class CustomerEntryForm extends ValidatorActionFormEx {
	// シリアルバージョンID
	private static final long serialVersionUID = 1L;
	// 企業管理者リスト
	private List<CustomerEntryRecord> uploadCSV = null;
	// Yes or No
	private String confirm = null;

	/**
	 * メソッド名 : uploadCSVのGetterメソッド
	 * 機能概要 : uploadCSVを取得します。
	 * @return uploadCSV 企業管理者リスト
	 */
	public List<CustomerEntryRecord> getUploadCSV() {
		return uploadCSV;
	}

	/**
	 * メソッド名 : uploadCSVのSetterメソッド
	 * 機能概要 : uploadCSVを設定します。
	 * @param uploadCSV 企業管理者リスト
	 */
	public void setUploadCSV(List<CustomerEntryRecord> uploadCSV) {
		this.uploadCSV = uploadCSV;
	}

	/**
	 * メソッド名 : confirmのGetterメソッド
	 * 機能概要 : confirmを取得します。
	 * @return confirm アクション名
	 */
	public String getConfirm() {
		return confirm;
	}

	/**
	 * メソッド名 : confirmのSetterメソッド
	 * 機能概要 : confirmを設定します。
	 * @param confirm アクション名
	 */
	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}

}
