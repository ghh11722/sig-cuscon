/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditURLFilteringOutput.java
 *
 * [変更履歴]
 * 日付        更新者             内容
 * 2011/05/16  ktakenaka@PROSITE  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwconnect.dto;

/**
 * クラス名 : EditURLFilteringOutput
 * 機能概要 : Webフィルタ選択ビジネスロジックの出力定義Bean
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/05/16
 *          新規作成
 */
public class EditURLFilteringOutput {

	// ファイアウォールID
	private String firewallId = null;

	// Webフィルタフラグ
	private int urlFilteringFlg = 0;

	/**
	 * メソッド名 : firewallIdのGetterメソッド
	 * 機能概要 : firewallIdを取得します。
	 * @return firewallId Firewall-ID
	 */
	public String getFirewallId() {
		return firewallId;
	}

	/**
	 * メソッド名 : firewallIdのSetterメソッド
	 * 機能概要 : firewallIdを設定します。
	 * @param firewallId Firewall-ID
	 */
	public void setFirewallId(String firewallId) {
		this.firewallId = firewallId;
	}

	/**
	 * メソッド名 : urlFilteringFlgのGetterメソッド
	 * 機能概要 : urlFilteringFlgを取得する。
	 * @return urlFilteringFlg
	 */
	public int getUrlFilteringFlg() {
		return urlFilteringFlg;
	}

	/**
	 * メソッド名 : urlFilteringFlgのSetterメソッド
	 * 機能概要 : urlFilteringFlgをセットする。
	 * @param urlFilteringFlg
	 */
	public void setUrlFilteringFlg(int urlFilteringFlg) {
		this.urlFilteringFlg = urlFilteringFlg;
	}
}
