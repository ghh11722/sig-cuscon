/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AdminMainRecord.java
 *
 * [変更履歴]
 * 日付        更新者             内容
 * 2010/03/04  m.narikawa@JCCH    初版作成
 * 2011/05/16  ktakenaka@PROSITE  Webフィルタ,DMZ追加
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwconnect.vo;

import java.io.Serializable;

/**
 * クラス名 : AdminMainRecord
 * 機能概要 : 企業管理者の取得に使用するレコード定義Bean（DB操作に使用）
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class AdminMainRecord implements Serializable {
	// シリアルバージョンID
	private static final long serialVersionUID = 8435977276889342399L;
	// Firewall-ID
	private String firewallId = null;
	// カスコンログインID
	private String loginId = null;
	// カスコンログインパスワード
	private String loginPwd = null;
	// PAログインID
	private String vsysId = null;
	// 最終ログイン時刻
	private String lastLoginDate = null;
	// 最終コミット時刻
	private String lastCommitDate = null;
	//20110516 ktakenaka@PROSITE add start
	// Webフィルタフラグ
	private int urlFilteringFlg = 0;
	// DMZフラグ
	private int dmzFlg = 0;
	//20110516 ktakenaka@PROSITE add end

	/**
	 * メソッド名 : firewallIdのGetterメソッド
	 * 機能概要 : firewallIdを取得します。
	 * @return firewallId Firewall-ID
	 */
	public String getFirewallId() {
		return firewallId;
	}

	/**
	 * メソッド名 : firewallIdのSetterメソッド
	 * 機能概要 : firewallIdを設定します。
	 * @param firewallId Firewall-ID
	 */
	public void setFirewallId(String firewallId) {
		this.firewallId = firewallId;
	}

	/**
	 * メソッド名 : loginIdのGetterメソッド
	 * 機能概要 : loginIdを取得します。
	 * @return loginId カスコンログインID
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * メソッド名 : loginIdのSetterメソッド
	 * 機能概要 : loginIdを設定します。
	 * @param loginId カスコンログインID
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * メソッド名 : loginIdのGetterメソッド
	 * 機能概要 : loginIdを取得します。
	 * @return loginId カスコンログインID
	 */
	public String getLoginPwd() {
		return loginPwd;
	}

	/**
	 * メソッド名 : loginIdのSetterメソッド
	 * 機能概要 : loginIdを設定します。
	 * @param loginId カスコンログインID
	 */
	public void setLoginPwd(String loginPwd) {
		this.loginPwd = loginPwd;
	}

	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得します。
	 * @return vsysId PAログインID
	 */
	public String getVsysId() {
		return vsysId;
	}

	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdを設定します。
	 * @param vsysId PAログインID
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}

	/**
	 * メソッド名 : lastLoginDateのGetterメソッド
	 * 機能概要 : lastLoginDateを取得します。
	 * @return lastLoginDate 最終ログイン時刻
	 */
	public String getLastLoginDate() {
		return lastLoginDate;
	}

	/**
	 * メソッド名 : lastLoginDateのSetterメソッド
	 * 機能概要 : lastLoginDateを設定します。
	 * @param lastLoginDate 最終ログイン時刻
	 */
	public void setLastLoginDate(String lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	/**
	 * メソッド名 : lastCommitDateのGetterメソッド
	 * 機能概要 : lastCommitDateを取得します。
	 * @return lastCommitDate 最終コミット時刻
	 */
	public String getLastCommitDate() {
		return lastCommitDate;
	}

	/**
	 * メソッド名 : lastCommitDateのSetterメソッド
	 * 機能概要 : lastCommitDateを設定します。
	 * @param lastCommitDate 最終コミット時刻
	 */
	public void setLastCommitDate(String lastCommitDate) {
		this.lastCommitDate = lastCommitDate;
	}

	//20110516 ktakenaka@PROSITE add start
	/**
	 * メソッド名 : urlFilteringFlgのGetterメソッド
	 * 機能概要 : urlFilteringFlgを取得します。
	 * @return urlFilteringFlg Webフィルタフラグ
	 */
	public int getUrlFilteringFlg() {
		return urlFilteringFlg;
	}

	/**
	 * メソッド名 : urlFilteringFlgのSetterメソッド
	 * 機能概要 : urlFilteringFlgを設定します。
	 * @param urlFilteringFlg Webフィルタフラグ
	 */
	public void setUrlFilteringFlg(int urlFilteringFlg) {
		this.urlFilteringFlg = urlFilteringFlg;
	}

	/**
	 * メソッド名 : dmzFlgのGetterメソッド
	 * 機能概要 : dmzFlgを取得します。
	 * @return dmzFlg DMZフラグ
	 */
	public int getDmzFlg() {
		return dmzFlg;
	}

	/**
	 * メソッド名 : dmzFlgのSetterメソッド
	 * 機能概要 : dmzFlgを設定します。
	 * @param dmzFlg DMZフラグ
	 */
	public void setDmzFlg(int dmzFlg) {
		this.dmzFlg = dmzFlg;
	}
	//20110516 ktakenaka@PROSITE add end

}
