/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditDmzBLogic.java
 *
 * [変更履歴]
 * 日付        更新者              内容
 * 2011/05/16  ktakenaka@PROSITE   初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwconnect.blogic;

import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwconnect.dto.AdminMainInput;
import jp.co.kddi.secgw.cuscon.fwconnect.dto.EditDmzOutput;
import jp.co.kddi.secgw.cuscon.fwconnect.vo.AdminMainRecord;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * クラス名 : EditDmzBLogic
 * 機能概要 : DMZ選択ビジネスロジック
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/05/16
 *          新規作成
 */
public class EditDmzBLogic implements BLogic<AdminMainInput> {
	// DAO
	private QueryDAO queryDAO = null;
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(EditDmzBLogic.class);

	/**
	 * メソッド名 : DMZ選択処理
	 * 機能概要 : DMZを選択する
	 * @param param DMZ選択の入力情報を保持したMap
	 * @return DMZ選択処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(AdminMainInput param) {

		log.debug("DMZ選択処理");

		BLogicResult result = new BLogicResult();
		BLogicMessages errors = new BLogicMessages();
        result.setResultString("failure");

		// 選択したリストからオブジェクトを取得する。
        AdminMainRecord adminMainRec = param.getCustomers().get(param.getIndex());

		// 企業管理者セッション管理情報を取得する
		log.debug(" 多重ログインチェック");
		Integer count= queryDAO.executeForObject("CustomerLoginBLogic-3", adminMainRec.getLoginId(), Integer.class);
		if (count.intValue() != 0) {
			// 準正常 : 顧客企業ログイン中
			Object[] objectgArray = {adminMainRec.getLoginId()};
			log.info(messageAccessor.getMessage("IK031001", objectgArray, param.getUvo()));
			errors.add("message", new BLogicMessage("DK030007"));
			result.setErrors(errors);
			result.setResultString("failure");
			return result;
		}

		// ビジネスロジックの出力クラスに結果を設定する
        EditDmzOutput out = new EditDmzOutput();
        out.setFirewallId(adminMainRec.getFirewallId());
        out.setDmzFlg(adminMainRec.getDmzFlg());

        result.setResultObject(out);
        result.setResultString("success");

		log.debug("DMZ選択処理終了");
        return result;
	}
	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * 機能概要 : queryDAOを取得します。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOを設定します。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}
}
