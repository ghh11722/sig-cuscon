/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AdminMainForm.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/03/04  m.narikawa@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwconnect.form;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwconnect.vo.AdminMainRecord;
import jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx;

/**
 * クラス名 : AdminMainForm
 * 機能概要 : KDDI運用者メイン画面アクションフォーム
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 * @see jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx
 */
public class AdminMainForm extends ValidatorActionFormEx {
	// シリアルバージョンID
	private static final long serialVersionUID = 1L;
	// 選択企業ログインID
	private String customerLoginId = null;
	// 企業管理者一覧
	private List<AdminMainRecord> customers = null;
	// 20110516 ktakenaka@PROSITE add start
	// 選択企業インデックス
	private int selectIndex = 0;
	// ファイアウォールID
	private String firewallId = null;
	// Webフィルタフラグ
	private int editUrlFilteringFlg = 0;
	// DMZフラグ
	private int editDmzFlg = 0;
	// 選択ラジオボタン
	private int radio = 0;
	// 20110516 ktakenaka@PROSITE add end

	/**
	 * メソッド名 : customerLoginIdのGetterメソッド
	 * 機能概要 : customerLoginIdを取得します。
	 * @return customerLoginId 選択企業ログインID
	 */
	public String getCustomerLoginId() {
	    return customerLoginId;
	}

	/**
	 * メソッド名 : customerLoginIdのSetterメソッド
	 * 機能概要 : customerLoginIdを設定します。
	 * @param customerLoginId 選択企業ログインID
	 */
	public void setCustomerLoginId(String customerLoginId) {
	    this.customerLoginId = customerLoginId;
	}

	/**
	 * メソッド名 : customersのGetterメソッド
	 * 機能概要 : customersを取得します。
	 * @return customers 企業管理者一覧
	 */
	public List<AdminMainRecord> getCustomers() {
		return customers;
	}

	/**
	 * メソッド名 : customersのSetterメソッド
	 * 機能概要 : customersを設定します。
	 * @param customers 企業管理者一覧
	 */
	public void setCustomers(List<AdminMainRecord> customers) {
		this.customers = customers;
	}

	// 20110516 ktakenaka@PROSITE add start
	/**
	 * メソッド名 : selectindexのGetterメソッド
	 * 機能概要 : selectindexを取得する。
	 * @return selectindex
	 */
	public int getSelectIndex() {
		return selectIndex;
	}

	/**
	 * メソッド名 : selectindexのSetterメソッド
	 * 機能概要 : selectindexをセットする。
	 * @param selectindex
	 */
	public void setSelectIndex(int selectIndex) {
		this.selectIndex = selectIndex;
	}

	/**
	 * メソッド名 : firewallIdのGetterメソッド
	 * 機能概要 : firewallIdを取得します。
	 * @return firewallId Firewall-ID
	 */
	public String getFirewallId() {
		return firewallId;
	}

	/**
	 * メソッド名 : firewallIdのSetterメソッド
	 * 機能概要 : firewallIdを設定します。
	 * @param firewallId Firewall-ID
	 */
	public void setFirewallId(String firewallId) {
		this.firewallId = firewallId;
	}

	/**
	 * メソッド名 : editUrlFilteringFlgのGetterメソッド
	 * 機能概要 : editUrlFilteringFlgを取得する。
	 * @return editUrlFilteringFlg
	 */
	public int getEditUrlFilteringFlg() {
		return editUrlFilteringFlg;
	}

	/**
	 * メソッド名 : editUrlFilteringFlgのSetterメソッド
	 * 機能概要 : editUrlFilteringFlgをセットする。
	 * @param editUrlFilteringFlg
	 */
	public void setEditUrlFilteringFlg(int editUrlFilteringFlg) {
		this.editUrlFilteringFlg = editUrlFilteringFlg;
	}

	/**
	 * メソッド名 : editDmzFlgのGetterメソッド
	 * 機能概要 : editDmzFlgを取得する。
	 * @return editDmzFlg
	 */
	public int getEditDmzFlg() {
		return editDmzFlg;
	}

	/**
	 * メソッド名 : editDmzFlgのSetterメソッド
	 * 機能概要 : editDmzFlgをセットする。
	 * @param editDmzFlg
	 */
	public void setEditDmzFlg(int editDmzFlg) {
		this.editDmzFlg = editDmzFlg;
	}

	/**
	 * メソッド名 : radioのGetterメソッド
	 * 機能概要 : radioを取得する。
	 * @return radio
	 */
	public int getRadio() {
		return radio;
	}

	/**
	 * メソッド名 : radioのSetterメソッド
	 * 機能概要 : radioをセットする。
	 * @param radio
	 */
	public void setRadio(int radio) {
		this.radio = radio;
	}
	// 20110516 ktakenaka@PROSITE add end
}
