/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CustomerChoiceInput.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/03/04  m.narikawa@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwconnect.dto;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;

/**
 * クラス名 : CustomerChoiceInput
 * 機能概要 : 企業管理者選択ビジネスロジックの入力定義Bean
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class CustomerChoiceInput {
	// UserValueObject
	private CusconUVO uvo = null;
	// 暗号鍵
	private String cryptKey = "kddi-secgw";
	// 選択企業ログインID
	private String customerLoginId = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得します。
	 * @return uvo UserValueObject
	 */
	public CusconUVO getUvo() {
	    return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoを設定します。
	 * @param uvo UserValueObject
	 */
	public void setUvo(CusconUVO uvo) {
	    this.uvo = uvo;
	}

	/**
	 * メソッド名 : cryptKeyのGetterメソッド
	 * 機能概要 : cryptKeyを取得します。
	 * @return cryptKey 暗号鍵
	 */
	public String getCryptKey() {
		return cryptKey;
	}

	/**
	 * メソッド名 : cryptKeyのSetterメソッド
	 * 機能概要 : cryptKeyを設定します。
	 * @param cryptKey 暗号鍵
	 */
	public void setCryptKey(String cryptKey) {
		this.cryptKey = cryptKey;
	}

	/**
	 * メソッド名 : customerLoginIdのGetterメソッド
	 * 機能概要 : customerLoginIdを取得します。
	 * @return customerLoginId 選択企業ログインID
	 */
	public String getCustomerLoginId() {
		return customerLoginId;
	}

	/**
	 * メソッド名 : customerLoginIdのSetterメソッド
	 * 機能概要 : customerLoginIdを設定します。
	 * @param customerLoginId 選択企業ログインID
	 */
	public void setCustomerLoginId(String customerLoginId) {
		this.customerLoginId = customerLoginId;
	}

}
