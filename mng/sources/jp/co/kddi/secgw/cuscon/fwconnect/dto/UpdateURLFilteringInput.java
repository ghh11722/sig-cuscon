/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateURLFilteringInput.java
 *
 * [変更履歴]
 * 日付        更新者             内容
 * 2011/05/16  ktakenaka@PROSITE  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwconnect.dto;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;

/**
 * クラス名 : UpdateURLFilteringInput
 * 機能概要 : Webフィルタ選択ビジネスロジックの入力定義Bean
 * 備考 :
 * @author k.takenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/05/16
 *          新規作成
 */
public class UpdateURLFilteringInput {

	// 暗号化鍵
	private String cryptKey = "kddi-secgw";

	// CusconUVO
	private CusconUVO uvo = null;

	// ファイアウォールID
	private String firewallId = null;

	// 選択ラジオボタン
	private int radio = 0;

	/**
	 * メソッド名 : cryptKeyのGetterメソッド
	 * 機能概要 : cryptKeyを取得します。
	 * @return cryptKey 暗号化鍵
	 */
	public String getCryptKey() {
		return cryptKey;
	}

	/**
	 * メソッド名 : cryptKeyのSetterメソッド
	 * 機能概要 : cryptKeyを設定します。
	 * @param cryptKey 暗号化鍵
	 */
	public void setCryptKey(String cryptKey) {
		this.cryptKey = cryptKey;
	}

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}

	/**
	 * メソッド名 : firewallIdのGetterメソッド
	 * 機能概要 : firewallIdを取得します。
	 * @return firewallId Firewall-ID
	 */
	public String getFirewallId() {
		return firewallId;
	}

	/**
	 * メソッド名 : firewallIdのSetterメソッド
	 * 機能概要 : firewallIdを設定します。
	 * @param firewallId Firewall-ID
	 */
	public void setFirewallId(String firewallId) {
		this.firewallId = firewallId;
	}

	/**
	 * メソッド名 : radioのGetterメソッド
	 * 機能概要 : radioを取得する。
	 * @return radio
	 */
	public int getRadio() {
		return radio;
	}

	/**
	 * メソッド名 : radioのSetterメソッド
	 * 機能概要 : radioをセットする。
	 * @param radio
	 */
	public void setRadio(int radio) {
		this.radio = radio;
	}
}
