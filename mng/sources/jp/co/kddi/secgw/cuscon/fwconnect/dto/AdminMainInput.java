/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AdminMainInput.java
 *
 * [変更履歴]
 * 日付        更新者             内容
 * 2010/03/04  m.narikawa@JCCH    初版作成
 * 2011/05/17  ktakenkaa@PROSITE  企業管理者一覧・選択ポリシーインデックス追加
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwconnect.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.fwconnect.vo.AdminMainRecord;

/**
 * クラス名 : AdminMainInput
 * 機能概要 : KDDI運用者メイン画面作成ビジネスロジックの入力定義Bean
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class AdminMainInput {
	// 暗号化鍵
	private String cryptKey = "kddi-secgw";
	// UserValueObject
	private CusconUVO uvo = null;

	// 20110517 ktakenaka@PROSITE add start
	// 企業管理者一覧
	private List<AdminMainRecord> customers = null;
	// 選択したポリシーのインデックス
	private int index = 0;
	// 20110517 ktakenaka@PROSITE add end

	/**
	 * メソッド名 : cryptKeyのGetterメソッド
	 * 機能概要 : cryptKeyを取得します。
	 * @return cryptKey 暗号化鍵
	 */
	public String getCryptKey() {
		return cryptKey;
	}

	/**
	 * メソッド名 : cryptKeyのSetterメソッド
	 * 機能概要 : cryptKeyを設定します。
	 * @param cryptKey 暗号化鍵
	 */
	public void setCryptKey(String cryptKey) {
		this.cryptKey = cryptKey;
	}

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo UserValueObject
	 */
	public CusconUVO getUvo() {
		return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo UserValueObject
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}

	// 20110517 ktakenaka@PROSITE add start
	/**
	 * メソッド名 : customersのGetterメソッド
	 * 機能概要 : customersを取得します。
	 * @return customers 企業管理者一覧
	 */
	public List<AdminMainRecord> getCustomers() {
		return customers;
	}

	/**
	 * メソッド名 : customersのSetterメソッド
	 * 機能概要 : customersを設定します。
	 * @param customers 企業管理者一覧
	 */
	public void setCustomers(List<AdminMainRecord> customers) {
		this.customers = customers;
	}

	/**
	 * メソッド名 : indexのGetterメソッド
	 * 機能概要 : indexを取得する。
	 * @return index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * メソッド名 : indexのSetterメソッド
	 * 機能概要 : indexをセットする。
	 * @param index
	 */
	public void setIndex(int index) {
		this.index = index;
	}
	// 20110517 ktakenaka@PROSITE add end
}
