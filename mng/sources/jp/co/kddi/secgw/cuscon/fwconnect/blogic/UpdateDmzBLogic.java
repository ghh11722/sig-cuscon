/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateDmzBLogic.java
 *
 * [変更履歴]
 * 日付        更新者              内容
 * 2011/05/18  ktakenaka@PROSITE   初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwconnect.blogic;

import java.util.ArrayList;
import java.util.List;

import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwconnect.dto.AdminMainOutput;
import jp.co.kddi.secgw.cuscon.fwconnect.dto.UpdateDmzInput;
import jp.co.kddi.secgw.cuscon.fwconnect.vo.AdminMainRecord;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * クラス名 : UpdateDmzBLogic
 * 機能概要 : DMZ設定画面にて「OK」押下時の処理を行う。
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/05/18
 *          新規作成
 */
public class UpdateDmzBLogic implements BLogic<UpdateDmzInput> {

	// DAO
	private QueryDAO queryDAO = null;
	private UpdateDAO updateDAO = null;
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(UpdateDmzBLogic.class);

	/**
	 * メソッド名 : 処理実行
	 * 機能概要 : DMZフラグの更新処理を行う。
	 * @param param DMZ設定画面入力データクラス
	 * @return DMZ更新処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(UpdateDmzInput param) {

		log.debug("DMZフラグの更新処理開始");

		BLogicResult result = new BLogicResult();
		AdminMainOutput output = new AdminMainOutput();
		BLogicMessages errors = new BLogicMessages();
		BLogicMessages message = new BLogicMessages();

        // 選択したリストからオブジェクトを取得する。
        AdminMainRecord adminMainRec = new AdminMainRecord();
        adminMainRec.setFirewallId(param.getFirewallId());
        adminMainRec.setDmzFlg(param.getRadio());

        // 登録処理
		try {
			log.debug("企業更新 : Firewall-ID=" + adminMainRec.getFirewallId());
			updateDAO.execute("UpdateDmzBLogic-1", adminMainRec);
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// データベースへのアクセスに失敗した場合は、エラーを出力する
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK031004", objectgArray, param.getUvo()));
			message.add("message", new BLogicMessage("DK030006"));
        	result.setErrors(message);
			result.setResultObject(output);
			result.setResultString("failure");
			return result;
		}

		// 企業管理者検索
		log.debug("企業管理者検索");
		// 企業管理者テーブルより一覧情報を取得する
		List<AdminMainRecord> customers = null;
		try {
			customers = queryDAO.executeForObjectList("AdminMainBLogic-1", param.getCryptKey());
			if( customers.size() == 0 ){
				// ・検索Hitしない場合は、一覧のタイトルのみを表示する。
				log.debug("検索Hitなし");
				customers = new ArrayList<AdminMainRecord>();
				output.setCustomers(customers);
				result.setResultObject(output);
				result.setResultString("success");
				return result;
			}
		} catch (Exception e) {
			// ・不備があった場合は、エラーを出力する。
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : 企業管理者検索失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK031001", objectgArray, param.getUvo()));
			errors.add("message", new BLogicMessage("DK030001"));
			result.setErrors(errors);
			customers = new ArrayList<AdminMainRecord>();
			output.setCustomers(customers);
			result.setResultObject(output);
			result.setResultString("failure");
			return result;
		}

        // ビジネスロジックの出力クラスに結果を設定する
		output.setCustomers(customers);
		result.setMessages(message);
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("DMZフラグの更新処理終了");
        return result;
	}

	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * 機能概要 : queryDAOを取得します。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOを設定します。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : updateDAOのGetterメソッド
	 * 機能概要 : updateDAOを取得する。
	 * @return updateDAO DAO
	 */
	public UpdateDAO getUpdateDAO() {
		return updateDAO;
	}

	/**
	 * メソッド名 : updateDAOのSetterメソッド
	 * 機能概要 : updateDAOをセットする。
	 * @param updateDAO DAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		this.updateDAO = updateDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}
}
