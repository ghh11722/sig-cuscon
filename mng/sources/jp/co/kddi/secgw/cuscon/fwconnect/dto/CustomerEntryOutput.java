/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CustomerEntryOutput.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/03/04  m.narikawa@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwconnect.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwconnect.vo.CustomerEntryRecord;

/**
 * クラス名 : CustomerEntryOutput
 * 機能概要 : 企業追加／削除ビジネスロジックの出力定義Bean
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class CustomerEntryOutput {
	// 登録要求データ（アップロードCSV）
	List<CustomerEntryRecord> uploadCSV = null;
	// 処理結果メッセージ
	private String resultmsg = null;

	/**
	 * メソッド名 : uploadCSVのGetterメソッド
	 * 機能概要 : uploadCSVを取得します。
	 * @return uploadCSV 登録要求データ
	 */
	public List<CustomerEntryRecord> getUploadCSV() {
		return uploadCSV;
	}

	/**
	 * メソッド名 : uploadCSVのSetterメソッド
	 * 機能概要 : uploadCSVを設定します。
	 * @param uploadCSV 登録要求データ
	 */
	public void setUploadCSV(List<CustomerEntryRecord> uploadCSV) {
		this.uploadCSV = uploadCSV;
	}

	/**
	 * メソッド名 : resultmsgのGetterメソッド
	 * 機能概要 : resultmsgを取得します。
	 * @return resultmsg 処理結果メッセージ
	 */
	public String getResultmsg() {
	    return resultmsg;
	}

	/**
	 * メソッド名 : resultmsgのSetterメソッド
	 * 機能概要 : resultmsgを設定します。
	 * @param resultmsg 処理結果メッセージ
	 */
	public void setResultmsg(String resultmsg) {
	    this.resultmsg = resultmsg;
	}

}
