/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CustomerEntryInput.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/03/04  m.narikawa@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwconnect.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.fwconnect.vo.CustomerEntryRecord;

/**
 * クラス名 : CustomerEntryInput
 * 機能概要 : 企業追加／削除ビジネスロジックの入力定義Bean
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class CustomerEntryInput {
	// UserValueObject
	private CusconUVO uvo = null;
	// 登録要求データ（アップロードCSV）
	List<CustomerEntryRecord> uploadCSV;
	// Yes or No
	private String confirm = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo UserValueObject
	 */
	public CusconUVO getUvo() {
		return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo UserValueObject
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}

	/**
	 * メソッド名 : uploadCSVのGetterメソッド
	 * 機能概要 : uploadCSVを取得します。
	 * @return uploadCSV 登録要求データ
	 */
	public List<CustomerEntryRecord> getUploadCSV() {
	    return uploadCSV;
	}

	/**
	 * メソッド名 : uploadCSVのSetterメソッド
	 * 機能概要 : uploadCSVを設定します。
	 * @param uploadCSV 登録要求データ
	 */
	public void setUploadCSV(List<CustomerEntryRecord> uploadCSV) {
	    this.uploadCSV = uploadCSV;
	}

	/**
	 * メソッド名 : confirmのGetterメソッド
	 * 機能概要 : confirmを取得します。
	 * @return confirm アクション名
	 */
	public String getConfirm() {
	    return confirm;
	}

	/**
	 * メソッド名 : confirmのSetterメソッド
	 * 機能概要 : confirmを設定します。
	 * @param confirm アクション名
	 */
	public void setConfirm(String confirm) {
	    this.confirm = confirm;
	}

}
