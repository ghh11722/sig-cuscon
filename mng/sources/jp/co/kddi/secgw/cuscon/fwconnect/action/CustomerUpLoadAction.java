/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CustomerUpLoadAction.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/03/04  m.narikawa@JCCH  初版作成
 * 2010/03/18  T.Sutoh@JCCH     ファイルの形式をTSVへ変更
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwconnect.action;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.upload.FormFile;

import jp.co.kddi.secgw.cuscon.common.CusconExtendValidator;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwconnect.dto.CustomerUploadOutput;
import jp.co.kddi.secgw.cuscon.fwconnect.vo.CustomerEntryRecord;
import jp.co.kddi.secgw.cuscon.fwconnect.vo.CustomerUploadFile;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.web.struts.actions.AbstractBLogicAction;

/**
 * クラス名 : CustomerUpLoadAction
 * 機能概要 : 企業追加／削除アップロードビジネスロジック
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 * @version 1.1 T.Sutoh@JCCH
 *          Created 2010/03/18
 *          ファイル形式をTSVへ変更
 */
public class CustomerUpLoadAction extends AbstractBLogicAction<CustomerUploadFile> {
	// アップロード最大サイズ(1MB) (2/15 チェック不要となる)
	//private static final int FILE_MAX_SIZE = 1048576;
	// CSVフィールド数
	private static final int CSV_CULUMN_NUM = 13;
	// 【CSVフィールド番号】
	private static final int CSV_VLAN = 0;
	private static final int CSV_GW_ADDRESS_VIRTUAL = 1;
	private static final int CSV_GW_ADDRESS_ACT = 2;
	private static final int CSV_GW_ADDRESS_SBY = 3;
	private static final int CSV_GW_ADDRESS_VIP = 4;
	private static final int CSV_PAT_ADDRESS = 5;
	private static final int CSV_FIREWALL_SYS = 6;
	private static final int CSV_FIREWALL_ID = 7;
	private static final int CSV_LOGIN_ID = 8;
	private static final int CSV_INIT_PASSWORD = 9;
	private static final int CSV_VSYS_ID = 10;
	private static final int CSV_PA_LOGIN_ID = 11;
	private static final int CSV_STATE_FLAG = 12;
	// 【CSVフィールド名称】
	//private static final String CSV_NAME_VLAN = "VLAN";
	//private static final String CSV_NAME_GW_ADDRESS_VIRTUAL = "GWアドレス(仮想IP)";
	//private static final String CSV_NAME_GW_ADDRESS_ACT = "GWアドレス(ACT側実IP)";
	//private static final String CSV_NAME_GW_ADDRESS_SBY = "GWアドレス(SBY側実IP)";
	//private static final String CSV_NAME_GW_ADDRESS_VIP = "GWアドレス(VIP)";
	//private static final String CSV_NAME_PAT_ADDRESS = "PATアドレス";
	private static final String CSV_NAME_FIREWALL_SYS = "Firewall-SYS";
	private static final String CSV_NAME_FIREWALL_ID = "Firewall-ID";
	private static final String CSV_NAME_LOGIN_ID = "ログインID";
	private static final String CSV_NAME_INIT_PASSWORD = "ログイン初期パスワード";
	private static final String CSV_NAME_VSYS_ID = "Vsys-ID";
	private static final String CSV_NAME_PA_LOGIN_ID = "FWログインID";
	//private static final String CSV_NAME_STATE_FLAG = "状態フラグ";
	// 【CSVフィールド最大、最小桁数】
	private static final int MAX_FIREWALL_SYS = 20;
	private static final int MAX_FIREWALL_ID = 20;
	private static final int MAX_LOGIN_ID = 20;
	private static final int MAX_INIT_PASSWORD = 14;
	private static final int MAX_VSYS_ID = 20;
	private static final int MAX_PA_LOGIN_ID = 20;
	// 操作フラグ
	private static final String OPE_FLG_ADD = "ADD";
	private static final String OPE_FLG_DEL = "DEL";
	// DAO
	private QueryDAO queryDAO = null;
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(CustomerUpLoadAction.class);

	/**
	 * メソッド名 : ビジネスロジックの実行前処理
	 * 機能概要 : ビジネスロジックの実行前処理
	 * @param request リクエスト
	 * @param response レスポンス
	 * @param tupdownloadVO パラメータ
	 * @throws Exception 予期しない例外
	 * @see jp.terasoluna.fw.web.struts.actions.AbstractBLogicAction#preDoExecuteBLogic(javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse, java.lang.Object)
	 */
	protected void preDoExecuteBLogic(HttpServletRequest request, HttpServletResponse response, CustomerUploadFile uploadData) throws Exception {
		return;
	}

	/**
	 * メソッド名 : doExecuteBLogic
	 * 機能概要 : ＤＢファイルアップロードテーブルに新規ファイルアップロード情報を登録する。
	 * @param uploadData ＤＢコードリストを登録する機能で使われるパラメータクラス
	 * @return ファイルアップロード機能処理結果
	 * @throws Exception
	 */
	@Override
	public BLogicResult doExecuteBLogic(CustomerUploadFile uploadData) throws Exception {

		log.debug("企業追加削除アップロード");

		// 処理結果
		BLogicResult result = new BLogicResult();
		// 出力データ
		CustomerUploadOutput output = new CustomerUploadOutput();
		// エラーメッセージ
		BLogicMessages errors = new BLogicMessages();
		// アップロードファイル
		FormFile fileup = uploadData.getFileup();

		// ファイルアップロードチェック
		if (fileup == null) {
			// エラー : ファイルがアップロードされていない。
			log.error(messageAccessor.getMessage("EK101001", null, uploadData.getUvo()));
			errors.add("message", new BLogicMessage("DK100001"));
			result.setErrors(errors);
			result.setResultString("failure");
			return result;
		}

		// ファイル名チェック
		String fileName = fileup.getFileName();
		if (fileName == null || "".equals(fileName)) {
			fileup.destroy();
			// エラー : アップロードファイル名が異常。
			log.error(messageAccessor.getMessage("EK101002", null, uploadData.getUvo()));
			errors.add("message", new BLogicMessage("DK100002"));
			result.setErrors(errors);
			result.setResultString("failure");
			return result;
		}

		// 1）CSVファイルアップロード
		// 2) CSVファイル解析
		List<CustomerEntryRecord> customers;
		try {
			customers = readCsv(uploadData.getUvo(), fileup.getInputStream(), errors);
			if (customers == null || customers.size() == 0) {
				fileup.destroy();
				// エラー : readCsv()でメッセージが指定される。
				result.setErrors(errors);
				result.setResultString("failure");
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			// エラー : readCsv()でメッセージが指定される。
			fileup.destroy();
			result.setErrors(errors);
			result.setResultString("failure");
			return result;
		}
		fileup.destroy();

		// 3) 企業管理者検索
		// QueryDAOクラスを使用し、企業管理者テーブルの全レコードを取得
		// テーブル 企業管理者情報テーブル
		// 取得項目 ファイアウォールID
		// 検索条件 全レコード
		log.debug("3) 企業管理者検索");
		List<String> firewallIdList;
		try {
			firewallIdList = queryDAO.executeForObjectList("CustomerUpLoadAction-1", null);
		} catch (Exception e) {
			e.printStackTrace();
			fileup.destroy();
			// エラー : FW接続情報検索失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK101003", objectgArray, uploadData.getUvo()));
			errors.add("message", new BLogicMessage("DK100003"));
			result.setErrors(errors);
			result.setResultString("failure");
			return result;
		}

		// 3で取得したデータとアップロードファイルを比較して以下の処理を実施。
		// ※※※ CSVの件数分繰り返し ※※※
		for (int i = 0; i < customers.size(); i++) {
			CustomerEntryRecord entry = customers.get(i);
			String fId_upload = entry.getFirewallId();

			// ②一致しないFirewall-IDなら操作フラグを「登録」とする。
			entry.setOpeFlg(OPE_FLG_ADD);
			if (firewallIdList != null) {
				for (int j = 0; j < firewallIdList.size(); j++) {
					String fId_db = firewallIdList.get(j);
					if (fId_upload.equals(fId_db) == true) {
						// ①同一のFirewall-IDが見つかったら操作フラグを「削除」とする。
						entry.setOpeFlg(OPE_FLG_DEL);
						// ③①②の処理で操作フラグが「削除」と判定された場合は、4で取得した一覧から同一Firewall-IDのレコードを削除する。
						firewallIdList.remove(j);
						break;
					}
				}
			}
		}
		// ※※※ CSVの件数分繰り返し ※※※

		// 4) 登録データ保存
		// 処理結果をセッション情報（UploadCSV）へ保存する。
		// ※（パスワードは、UploadCSV内で暗号化されている）
		log.debug("4) 登録データ保存");
		output.setUploadCSV(customers);

		// 5)レスポンス返却
		log.debug("5)レスポンス返却");
		result.setResultString("success");
		result.setResultObject(output);

		return result;
	}

	/**
	 * メソッド名 : CSVファイル解析
	 * 機能概要 : CSVファイルの解析を行う
	 * @param uvo UserValueObject
	 * @param stream 入力ストリーム
	 * @param errors メッセージ情報一覧
	 * @return 出力データ
	 */
	public List<CustomerEntryRecord> readCsv(CusconUVO uvo, InputStream stream, BLogicMessages errors) {
		// 1）CSVファイルアップロード
		InputStreamReader reader = null;
		BufferedReader buff = null;
		List<CustomerEntryRecord> customers = null;

		try {
			reader = new InputStreamReader(stream, "MS932");
			buff = new BufferedReader(reader);
			String record;
			String[] columns;
			int lineNum = 0;

			customers = new ArrayList<CustomerEntryRecord>();

			// ヘッダ行（先頭1行）を空読みする
			buff.readLine();

			// 1行読み込み
			while ((record = buff.readLine()) != null) {
				lineNum++;
				if (record.length() <= 0) {
					continue;
				}

				// カンマ区切り
				columns = record.split("\t");
				// 1行分解析する
				CustomerEntryRecord entryRecord = checkCsvLine(uvo, columns, lineNum, errors);
				// 解析に失敗したところで、処理終了。
				if (entryRecord == null) {
					throw new Exception();
				}
				customers.add(entryRecord);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			if (customers != null) {
				customers.clear();
			}
			// エラー : CSVファイルが異常。
			Object[] objectgArray = {ex.getMessage()};
			log.error(messageAccessor.getMessage("EK101004", objectgArray, uvo));
			errors.add("message", new BLogicMessage("DK100004"));

		} finally {
			try {
				if (buff != null) {
					buff.close();
				}
				if (reader != null) {
					reader.close();
				}
				stream.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return customers;
	}

	/**
	 * メソッド名 : CSV 1行分チェック
	 * 機能概要 : CSVの1行をチェックする
	 * @param uvo UserValueObject
	 * @param columns カラム名
	 * @param lineNum 行数
	 * @param errors メッセージ情報一覧
	 * @return 出力データ
	 */
	protected CustomerEntryRecord checkCsvLine(CusconUVO uvo, String[] columns, int lineNum, BLogicMessages errors) {

		// 1レコード分のデータ
		CustomerEntryRecord entryRecord = new CustomerEntryRecord();
		boolean checkNG = true;

		// 2) CSVファイル解析
		// アップロードされたファイルをチェックする。
		// ・CSVの項目ごとにデータ形式をチェックする。

		// フィールド数のチェック
		if (columns.length != CSV_CULUMN_NUM) {
			// CSVフォーマットエラー。
			log.error(messageAccessor.getMessage("EK101005", null, uvo));
			errors.add("message", new BLogicMessage("DK100005"));
			checkNG = false;
			return null;
		}

		// 【VLAN】
		// 入力チェックなし
		entryRecord.setVlan(columns[CSV_VLAN]);

		// 【GWアドレス(仮想IP)】
		// 入力チェックなし
		entryRecord.setGwAddressVirtual(columns[CSV_GW_ADDRESS_VIRTUAL]);

		// 【GWアドレス(ACT側実IP)】
		// 入力チェックなし
		entryRecord.setGwAddressAct(columns[CSV_GW_ADDRESS_ACT]);

		// 【GWアドレス(SBY側実IP)】
		// 入力チェックなし
		entryRecord.setGwAddressSby(columns[CSV_GW_ADDRESS_SBY]);

		// 【GWアドレス(VIP)】
		// 入力チェックなし
		entryRecord.setGwAddressVip(columns[CSV_GW_ADDRESS_VIP]);

		// 【PATアドレス】
		// 入力チェックなし
		entryRecord.setPatAddress(columns[CSV_PAT_ADDRESS]);

		// 【Firewall-SYS】
		// 必須チェック
		if (!CusconExtendValidator.required(columns[CSV_FIREWALL_SYS])) {
			Object[] objectgArray = {Integer.toString(lineNum), CSV_NAME_FIREWALL_SYS};
			log.error(messageAccessor.getMessage("EK101009", objectgArray, uvo));
			errors.add("message", new BLogicMessage("DK100009", Integer.toString(lineNum), CSV_NAME_FIREWALL_SYS));
			checkNG = false;
		}
		// 桁数チェック
		if (!CusconExtendValidator.maxLength(columns[CSV_FIREWALL_SYS], MAX_FIREWALL_SYS)) {
			Object[] objectgArray = {Integer.toString(lineNum), CSV_NAME_FIREWALL_SYS, Integer.toString(MAX_FIREWALL_ID)};
			log.error(messageAccessor.getMessage("EK101011", objectgArray, uvo));
			errors.add("message", new BLogicMessage("DK100011", Integer.toString(lineNum), CSV_NAME_FIREWALL_SYS, Integer.toString(MAX_FIREWALL_ID)));
			checkNG = false;
		}
		entryRecord.setFirewallSys(columns[CSV_FIREWALL_SYS]);

		// 【Firewall-ID】
		// 必須チェック
		if (!CusconExtendValidator.required(columns[CSV_FIREWALL_ID])) {
			Object[] objectgArray = {Integer.toString(lineNum), CSV_NAME_FIREWALL_ID};
			log.error(messageAccessor.getMessage("EK101009", objectgArray, uvo));
			errors.add("message", new BLogicMessage("DK100009", Integer.toString(lineNum), CSV_NAME_FIREWALL_ID));
			checkNG = false;
		}
		// 桁数チェック
		if (!CusconExtendValidator.maxLength(columns[CSV_FIREWALL_ID], MAX_FIREWALL_ID)) {
			Object[] objectgArray = {Integer.toString(lineNum), CSV_NAME_FIREWALL_ID, Integer.toString(MAX_FIREWALL_ID)};
			log.error(messageAccessor.getMessage("EK101011", objectgArray, uvo));
			errors.add("message", new BLogicMessage("DK100011", Integer.toString(lineNum), CSV_NAME_FIREWALL_ID, Integer.toString(MAX_FIREWALL_ID)));
			checkNG = false;
		}
		entryRecord.setFirewallId(columns[CSV_FIREWALL_ID]);

		// 【ログインID】
		// 必須チェック
		if (!CusconExtendValidator.required(columns[CSV_LOGIN_ID])) {
			Object[] objectgArray = {Integer.toString(lineNum), CSV_NAME_LOGIN_ID};
			log.error(messageAccessor.getMessage("EK101009", objectgArray, uvo));
			errors.add("message", new BLogicMessage("DK100009", Integer.toString(lineNum), CSV_NAME_LOGIN_ID));
			checkNG = false;
		}
		// 桁数チェック
		if (!CusconExtendValidator.maxLength(columns[CSV_LOGIN_ID], MAX_LOGIN_ID)) {
			Object[] objectgArray = {Integer.toString(lineNum), CSV_NAME_LOGIN_ID, Integer.toString(MAX_LOGIN_ID)};
			log.error(messageAccessor.getMessage("EK101011", objectgArray, uvo));
			errors.add("message", new BLogicMessage("DK100011", Integer.toString(lineNum), CSV_NAME_LOGIN_ID, Integer.toString(MAX_LOGIN_ID)));
			checkNG = false;
		}
		entryRecord.setLoginId(columns[CSV_LOGIN_ID]);

		// 【ログイン初期パスワード】
		// 必須チェック
		if (!CusconExtendValidator.required(columns[CSV_INIT_PASSWORD])) {
			Object[] objectgArray = {Integer.toString(lineNum), CSV_NAME_INIT_PASSWORD};
			log.error(messageAccessor.getMessage("EK101009", objectgArray, uvo));
			errors.add("message", new BLogicMessage("DK100009", Integer.toString(lineNum), CSV_NAME_INIT_PASSWORD));
			checkNG = false;
		}
		// 桁数チェック
		if (!CusconExtendValidator.maxLength(columns[CSV_INIT_PASSWORD], MAX_INIT_PASSWORD)) {
			Object[] objectgArray = {Integer.toString(lineNum), CSV_NAME_INIT_PASSWORD, Integer.toString(MAX_INIT_PASSWORD)};
			log.error(messageAccessor.getMessage("EK101011", objectgArray, uvo));
			errors.add("message", new BLogicMessage("DK100011", Integer.toString(lineNum), CSV_NAME_INIT_PASSWORD, Integer.toString(MAX_INIT_PASSWORD)));
			checkNG = false;
		}
		// 形式チェック(半角英大文字、半角英小文字、数字、記号のうちいずれか3つを含む)
		//if (!CusconExtendValidator.matchPassword(columns[CSV_INIT_PASSWORD])) {
		//	errors.add("message", new BLogicMessage("DK100012", Integer.toString(lineNum), CSV_NAME_INIT_PASSWORD));
		//	checkNG = false;
		//}
		entryRecord.setInitPassword(columns[CSV_INIT_PASSWORD]);

		// 【Vsys-ID】
		// 必須チェック
		if (!CusconExtendValidator.required(columns[CSV_VSYS_ID])) {
			Object[] objectgArray = {Integer.toString(lineNum), CSV_NAME_VSYS_ID};
			log.error(messageAccessor.getMessage("EK101009", objectgArray, uvo));
			errors.add("message", new BLogicMessage("DK100009", Integer.toString(lineNum), CSV_NAME_VSYS_ID));
			checkNG = false;
		}
		// 桁数チェック
		if (!CusconExtendValidator.maxLength(columns[CSV_VSYS_ID], MAX_VSYS_ID)) {
			Object[] objectgArray = {Integer.toString(lineNum), CSV_NAME_VSYS_ID, Integer.toString(MAX_VSYS_ID)};
			log.error(messageAccessor.getMessage("EK101011", objectgArray, uvo));
			errors.add("message", new BLogicMessage("DK100011", Integer.toString(lineNum), CSV_NAME_VSYS_ID, Integer.toString(MAX_VSYS_ID)));
			checkNG = false;
		}
		entryRecord.setVsysId(columns[CSV_VSYS_ID]);

		// 【PAログインID】
		// 必須チェック
		if (!CusconExtendValidator.required(columns[CSV_PA_LOGIN_ID])) {
			Object[] objectgArray = {Integer.toString(lineNum), CSV_NAME_PA_LOGIN_ID};
			log.error(messageAccessor.getMessage("EK101009", objectgArray, uvo));
			errors.add("message", new BLogicMessage("DK100009", Integer.toString(lineNum), CSV_NAME_PA_LOGIN_ID));
			checkNG = false;
		}
		// 桁数チェック
		if (!CusconExtendValidator.maxLength(columns[CSV_PA_LOGIN_ID], MAX_PA_LOGIN_ID)) {
			Object[] objectgArray = {Integer.toString(lineNum), CSV_NAME_PA_LOGIN_ID, Integer.toString(MAX_PA_LOGIN_ID)};
			log.error(messageAccessor.getMessage("EK101011", objectgArray, uvo));
			errors.add("message", new BLogicMessage("DK100011", Integer.toString(lineNum), CSV_NAME_PA_LOGIN_ID, Integer.toString(MAX_PA_LOGIN_ID)));
			checkNG = false;
		}
		entryRecord.setPaLoginId(columns[CSV_PA_LOGIN_ID]);

		// 【状態フラグ】
		entryRecord.setStateFlg(columns[CSV_STATE_FLAG]);

		// チェックNGとなっていたらnullを返却
		if (checkNG == false) {
			return null;
		}

		return entryRecord;
	}

	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * 機能概要 : queryDAOを取得します。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOを設定します。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}
}
