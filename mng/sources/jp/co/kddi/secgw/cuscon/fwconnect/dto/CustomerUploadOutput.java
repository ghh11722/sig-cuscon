/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CustomerUploadOutput.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/03/04  m.narikawa@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwconnect.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwconnect.vo.CustomerEntryRecord;

/**
 * クラス名 : CustomerUploadOutput
 * 機能概要 : 企業追加／削除アップロードビジネスロジックの出力定義Bean
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class CustomerUploadOutput {
	// 登録要求データ（アップロードCSV）
	private List<CustomerEntryRecord> uploadCSV;

	/**
	 * メソッド名 : uploadCSVのGetterメソッド
	 * 機能概要 : uploadCSVを取得します。
	 * @return uploadCSV 登録要求データ
	 */
	public List<CustomerEntryRecord> getUploadCSV() {
	    return uploadCSV;
	}

	/**
	 * メソッド名 : uploadCSVのSetterメソッド
	 * 機能概要 : uploadCSVを設定します。
	 * @param uploadCSV 登録要求データ
	 */
	public void setUploadCSV(List<CustomerEntryRecord> uploadCSV) {
	    this.uploadCSV = uploadCSV;
	}

}
