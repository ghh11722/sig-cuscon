/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CustomerEntryRecord.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/03/04  m.narikawa@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwconnect.vo;

import java.io.Serializable;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * クラス名 : CustomerEntryRecord
 * 機能概要 : 企業管理者の登録に使用するレコード定義Bean（DB操作に使用）
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class CustomerEntryRecord implements Serializable {
	// シリアルバージョンID
	private static final long serialVersionUID = -149525251218194498L;
	// 暗号化鍵
	private String cryptKey = "kddi-secgw";
	// 操作フラグ(DBには登録しない)
	private String opeFlg = null;
	// VLAN(DBには登録しない)
	private String vlan = null;
	// GWアドレス(仮想IP)(DBには登録しない)
	private String gwAddressVirtual = null;
	// GWアドレス(ACT側実IP)(DBには登録しない)
	private String gwAddressAct = null;
	// GWアドレス(SBY側実IP)(DBには登録しない)
	private String gwAddressSby = null;
	// GWアドレス(VIP)(DBには登録しない)
	private String gwAddressVip = null;
	// PATアドレス(DBには登録しない)
	private String patAddress = null;
	// Firewall-SYS(DBには登録しない)
	private String firewallSys = null;
	// Firewall-ID
	private String firewallId = null;
	// ログインID
	private String loginId = null;
	// ログイン初期パスワード(暗号化データ)
	private byte[] enc_initPassword = null;
	// Vsys-ID
	private String vsysId = null;
	// PAログインID
	private String paLoginId = null;
	// 状態フラグ(DBには登録しない)
	private String stateFlg = null;

	/**
	 * メソッド名 : 文字列の暗号化
	 * 機能概要 : 入力した文字列を暗号化してbyte配列を返却する
	 * @param key 暗号化鍵
	 * @param text 暗号化対象文字列
	 * @return 暗号化文字列
	 * @throws Exception
	 */
	public static byte[] encrypt(String key, String text) throws Exception {
		SecretKeySpec sksSpec = new SecretKeySpec(key.getBytes(), "Blowfish");

		Cipher cipher = Cipher.getInstance("Blowfish");
		cipher.init(javax.crypto.Cipher.ENCRYPT_MODE, sksSpec);

		return cipher.doFinal(text.getBytes());
	}

	/**
	 * メソッド名 : 文字列の復号化
	 * 機能概要 : 入力したbyte配列を復号化して文字列を返却する。
	 * @param key 複合化鍵
	 * @param encrypted 複合化文字列
	 * @return 復号後文字列
	 * @throws Exception
	 */
	public static String decrypt(String key, byte[] encrypted) throws Exception {

		SecretKeySpec sksSpec = new SecretKeySpec(key.getBytes(), "Blowfish");

		Cipher cipher = Cipher.getInstance("Blowfish");
		cipher.init(Cipher.DECRYPT_MODE, sksSpec);

		return new String(cipher.doFinal(encrypted));
	}

	/**
	 * メソッド名 : cryptKeyのGetterメソッド
	 * 機能概要 : cryptKeyを取得します。
	 * @return cryptKey 暗号化鍵
	 */
	public String getCryptKey() {
		return cryptKey;
	}

	/**
	 * メソッド名 : cryptKeyのSetterメソッド
	 * 機能概要 : cryptKeyを設定します。
	 * @param cryptKey 暗号化鍵
	 */
	public void setCryptKey(String cryptKey) {
		this.cryptKey = cryptKey;
	}

	/**
	 * メソッド名 : opeFlgのGetterメソッド
	 * 機能概要 : opeFlgを取得します。
	 * @return opeFlg 操作フラグ
	 */
	public String getOpeFlg() {
		return opeFlg;
	}

	/**
	 * メソッド名 : opeFlgのSetterメソッド
	 * 機能概要 : opeFlgを設定します。
	 * @param opeFlg 操作フラグ
	 */
	public void setOpeFlg(String opeFlg) {
		this.opeFlg = opeFlg;
	}

	/**
	 * メソッド名 : vlanのGetterメソッド
	 * 機能概要 : vlanを取得します。
	 * @return vlan VLAN
	 */
	public String getVlan() {
		return vlan;
	}

	/**
	 * メソッド名 : vlanのSetterメソッド
	 * 機能概要 : vlanを設定します。
	 * @param vlan VLAN
	 */
	public void setVlan(String vlan) {
		this.vlan = vlan;
	}

	/**
	 * メソッド名 : gwAddressVirtualのGetterメソッド
	 * 機能概要 : gwAddressVirtualを取得します。
	 * @return gwAddressVirtual GWアドレス(仮想IP)
	 */
	public String getGwAddressVirtual() {
		return gwAddressVirtual;
	}

	/**
	 * メソッド名 : gwAddressVirtualのSetterメソッド
	 * 機能概要 : gwAddressVirtualを設定します。
	 * @param gwAddressVirtual GWアドレス(仮想IP)
	 */
	public void setGwAddressVirtual(String gwAddressVirtual) {
		this.gwAddressVirtual = gwAddressVirtual;
	}

	/**
	 * メソッド名 : gwAddressActのGetterメソッド
	 * 機能概要 : gwAddressActを取得します。
	 * @return gwAddressAct GWアドレス(ACT側実IP)
	 */
	public String getGwAddressAct() {
		return gwAddressAct;
	}

	/**
	 * メソッド名 : gwAddressActのSetterメソッド
	 * 機能概要 : gwAddressActを設定します。
	 * @param gwAddressAct GWアドレス(ACT側実IP)
	 */
	public void setGwAddressAct(String gwAddressAct) {
		this.gwAddressAct = gwAddressAct;
	}

	/**
	 * メソッド名 : gwAddressSbyのGetterメソッド
	 * 機能概要 : gwAddressSbyを取得します。
	 * @return gwAddressSby GWアドレス(SBY側実IP)
	 */
	public String getGwAddressSby() {
		return gwAddressSby;
	}

	/**
	 * メソッド名 : gwAddressSbyのSetterメソッド
	 * 機能概要 : gwAddressSbyを設定します。
	 * @param gwAddressSby GWアドレス(SBY側実IP)
	 */
	public void setGwAddressSby(String gwAddressSby) {
		this.gwAddressSby = gwAddressSby;
	}

	/**
	 * メソッド名 : gwAddressVipのGetterメソッド
	 * 機能概要 : gwAddressVipを取得します。
	 * @return gwAddressVip GWアドレス(VIP)
	 */
	public String getGwAddressVip() {
		return gwAddressVip;
	}

	/**
	 * メソッド名 : gwAddressVipのSetterメソッド
	 * 機能概要 : gwAddressVipを設定します。
	 * @param gwAddressVip GWアドレス(VIP)
	 */
	public void setGwAddressVip(String gwAddressVip) {
		this.gwAddressVip = gwAddressVip;
	}

	/**
	 * メソッド名 : patAddressのGetterメソッド
	 * 機能概要 : patAddressを取得します。
	 * @return patAddress PATアドレス
	 */
	public String getPatAddress() {
		return patAddress;
	}

	/**
	 * メソッド名 : patAddressのSetterメソッド
	 * 機能概要 : patAddressを設定します。
	 * @param patAddress PATアドレス
	 */
	public void setPatAddress(String patAddress) {
		this.patAddress = patAddress;
	}

	/**
	 * メソッド名 : firewallSysのGetterメソッド
	 * 機能概要 : firewallSysを取得します。
	 * @return firewallSys Firewall-SYS
	 */
	public String getFirewallSys() {
		return firewallSys;
	}

	/**
	 * メソッド名 : firewallSysのSetterメソッド
	 * 機能概要 : firewallSysを設定します。
	 * @param firewallSys Firewall-SYS
	 */
	public void setFirewallSys(String firewallSys) {
		this.firewallSys = firewallSys;
	}

	/**
	 * メソッド名 : firewallIdのGetterメソッド
	 * 機能概要 : firewallIdを取得します。
	 * @return firewallId Firewall-ID
	 */
	public String getFirewallId() {
		return firewallId;
	}

	/**
	 * メソッド名 : firewallIdのSetterメソッド
	 * 機能概要 : firewallIdを設定します。
	 * @param firewallId Firewall-ID
	 */
	public void setFirewallId(String firewallId) {
		this.firewallId = firewallId;
	}

	/**
	 * メソッド名 : loginIdのGetterメソッド
	 * 機能概要 : loginIdを取得します。
	 * @return loginId ログインID
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * メソッド名 : loginIdのSetterメソッド
	 * 機能概要 : loginIdを設定します。
	 * @param loginId ログインID
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * メソッド名 : initPasswordのGetterメソッド
	 * 機能概要 : initPasswordを取得します。
	 * @return initPassword ログイン初期パスワード
	 */
	public String getInitPassword() {
		// 復号化
		try {
			return decrypt(this.getCryptKey(), this.enc_initPassword);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * メソッド名 : initPasswordのSetterメソッド
	 * 機能概要 : initPasswordを設定します。
	 * @param initPassword ログイン初期パスワード
	 */
	public void setInitPassword(String initPassword) {
		// 復号化
		try {
			this.enc_initPassword = encrypt(this.getCryptKey(), initPassword);
		} catch (Exception e) {
			this.enc_initPassword = null;
		}
	}

	/**
	 * メソッド名 : enc_initPasswordのGetterメソッド
	 * 機能概要 : enc_initPasswordを取得します。
	 * @return enc_initPassword ログイン初期パスワード(暗号化データ)
	 */
	public byte[] getEnc_initPassword() {
		return enc_initPassword;
	}

	/**
	 * メソッド名 : enc_initPasswordのSetterメソッド
	 * 機能概要 : enc_initPasswordを設定します。
	 * @param enc_initPassword ログイン初期パスワード(暗号化データ)
	 */
	public void setEnc_initPassword(byte[] enc_initPassword) {
		this.enc_initPassword = enc_initPassword;
	}

	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得します。
	 * @return vsysId Vsys-ID
	 */
	public String getVsysId() {
		return vsysId;
	}

	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdを設定します。
	 * @param vsysId Vsys-ID
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}

	/**
	 * メソッド名 : paLoginIdのGetterメソッド
	 * 機能概要 : paLoginIdを取得します。
	 * @return paLoginId PAログインID
	 */
	public String getPaLoginId() {
		return paLoginId;
	}

	/**
	 * メソッド名 : paLoginIdのSetterメソッド
	 * 機能概要 : paLoginIdを設定します。
	 * @param paLoginId PAログインID
	 */
	public void setPaLoginId(String paLoginId) {
		this.paLoginId = paLoginId;
	}

	/**
	 * メソッド名 : stateFlgのGetterメソッド
	 * 機能概要 : stateFlgを取得します。
	 * @return stateFlg 状態フラグ
	 */
	public String getStateFlg() {
		return stateFlg;
	}

	/**
	 * メソッド名 : stateFlgのSetterメソッド
	 * 機能概要 : stateFlgを設定します。
	 * @param stateFlg 状態フラグ
	 */
	public void setStateFlg(String stateFlg) {
		this.stateFlg = stateFlg;
	}

}
