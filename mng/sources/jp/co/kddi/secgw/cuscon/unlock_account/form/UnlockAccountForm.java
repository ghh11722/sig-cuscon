/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UnlockAccountForm.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/03/04  m.narikawa@JCCH  初版作成
 * 2010/03/18  tinn.ra@JCCH  更新
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.unlock_account.form;

import java.util.List;

import jp.co.kddi.secgw.cuscon.unlock_account.vo.LockAccountRecord;
import jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx;

/**
 * クラス名 : UnlockAccountForm
 * 機能概要 : アカウントロック解除用アクションフォーム
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 * @version 1.1 tinn.ra@JCCH
 *          Created 2010/03/18
 *          成功時のポップアップ表示処理を追加
 * @see jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx
 */
public class UnlockAccountForm extends ValidatorActionFormEx {
	//
	private static final long serialVersionUID = 1L;
	// ロックアカウント一覧
	private List<LockAccountRecord> lock_accounts = null;
	// ロック解除対象ログインID
	private String[] targetLoginId = null;
	// メッセージ
	private String resultmsg = null;

	/**
	 * メソッド名 : resultmsgのGetterメソッド
	 * 機能概要 : resultmsgを取得する。
	 * @return resultmsg
	 */
	public String getResultmsg() {
		return resultmsg;
	}

	/**
	 * メソッド名 : resultmsgのSetterメソッド
	 * 機能概要 : resultmsgをセットする。
	 * @param resultmsg
	 */
	public void setResultmsg(String resultmsg) {
		this.resultmsg = resultmsg;
	}

	/**
	 * メソッド名 : lock_accountsのGetterメソッド
	 * 機能概要 : lock_accountsを取得します。
	 * @return lock_accounts ロックアカウント一覧
	 */
	public List<LockAccountRecord> getLock_accounts() {
		return lock_accounts;
	}

	/**
	 * メソッド名 : lock_accountsのSetterメソッド
	 * 機能概要 : lock_accountsを設定します。
	 * @param lock_accounts ロックアカウント一覧
	 */
	public void setLock_accounts(List<LockAccountRecord> lock_accounts) {
		this.lock_accounts = lock_accounts;
	}

	/**
	 * メソッド名 : targetLoginIdのGetterメソッド
	 * 機能概要 : targetLoginIdを取得します。
	 * @return targetLoginId ロック解除対象ログインID
	 */
	public String[] getTargetLoginId() {
	    return targetLoginId;
	}

	/**
	 * メソッド名 : targetLoginIdのSetterメソッド
	 * 機能概要 : targetLoginIdを設定します。
	 * @param targetLoginId ロック解除対象ログインID
	 */
	public void setTargetLoginId(String[] targetLoginId) {
	    this.targetLoginId = targetLoginId;
	}

}
