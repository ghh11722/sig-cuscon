/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : LockListOutput.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/03/04  tinn.ra@JCCH  初版作成
 * 2010/03/18  tinn.ra@JCCH  更新
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.unlock_account.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.unlock_account.vo.LockAccountRecord;

/**
 * クラス名 : LockListOutput
 * 機能概要 : ロックアカウント一覧取得処理出力定義クラス
 * 備考 :
 * @author tinn.ra@JCCH
 * @version 1.0 tinn.ra@JCCH
 *          Created 2010/03/04
 *          新規作成
 * @version 1.1 tinn.ra@JCCH
 *          Created 2010/03/18
 *          成功時のポップアップ表示処理を追加
 */
public class LockListOutput {
	// ロックアカウント一覧
	private List<LockAccountRecord> lock_accounts = null;
	// メッセージ
	private String resultmsg = null;

	/**
	 * メソッド名 : resultmsgのGetterメソッド
	 * 機能概要 : resultmsgを取得する。
	 * @return resultmsg
	 */
	public String getResultmsg() {
		return resultmsg;
	}

	/**
	 * メソッド名 : resultmsgのSetterメソッド
	 * 機能概要 : resultmsgをセットする。
	 * @param resultmsg
	 */
	public void setResultmsg(String resultmsg) {
		this.resultmsg = resultmsg;
	}

	/**
	 * メソッド名 : lock_accountsのGetterメソッド
	 * 機能概要 : lock_accountsを取得します。
	 * @return lock_accounts ロックアカウント一覧
	 */
	public List<LockAccountRecord> getLock_accounts() {
		return lock_accounts;
	}

	/**
	 * メソッド名 : lock_accountsのSetterメソッド
	 * 機能概要 : lock_accountsを設定します。
	 * @param lock_accounts ロックアカウント一覧
	 */
	public void setLock_accounts(List<LockAccountRecord> lock_accounts) {
		this.lock_accounts = lock_accounts;
	}
}
