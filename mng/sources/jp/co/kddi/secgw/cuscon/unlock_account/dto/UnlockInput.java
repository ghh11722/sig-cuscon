/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UnlockInput.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/03/04  tinn.ra@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.unlock_account.dto;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;

/**
 * クラス名 : UnlockInput
 * 機能概要 : アカウントロック解除処理入力クラス
 * 備考 :
 * @author tinn.ra@JCCH
 * @version 1.0 tinn.ra@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class UnlockInput {
	// ロックアカウント一覧
	private String[] targetLoginId = null;
	// UserValueObject
	private CusconUVO uvo = null;

	/**
	 * メソッド名 : targetLoginIdのGetterメソッド
	 * 機能概要 : targetLoginIdを取得します。
	 * @return targetLoginId ロックアカウント一覧
	 */
	public String[] getTargetLoginId() {
	    return targetLoginId;
	}

	/**
	 * メソッド名 : targetLoginIdのSetterメソッド
	 * 機能概要 : targetLoginIdを設定します。
	 * @param targetLoginId ロックアカウント一覧
	 */
	public void setTargetLoginId(String[] targetLoginId) {
	    this.targetLoginId = targetLoginId;
	}

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo UserValueObject
	 */
	public CusconUVO getUvo() {
		return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo UserValueObject
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}


}
