/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : LockAccountRecord.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/03/04  m.narikawa@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.unlock_account.vo;

import java.io.Serializable;

/**
 * クラス名 : LockAccountRecord
 * 機能概要 : アカウントロックレコード定義
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class LockAccountRecord implements Serializable {
	//
	private static final long serialVersionUID = 638510963699052525L;
	// ログインID
	private String loginId = null;
	// Firewall-ID
	private String firewallId = null;
	// vSys-ID
	private String vsysId = null;
	// 最終ログイン時刻
	private String lastLoginDate;

	/**
	 * メソッド名 : loginIdのGetterメソッド
	 * 機能概要 : loginIdを取得します。
	 * @return loginId ログインID
	 */
	public String getLoginId() {
	    return loginId;
	}

	/**
	 * メソッド名 : loginIdのSetterメソッド
	 * 機能概要 : loginIdを設定します。
	 * @param loginId ログインID
	 */
	public void setLoginId(String loginId) {
	    this.loginId = loginId;
	}

	/**
	 * メソッド名 : firewallIdのGetterメソッド
	 * 機能概要 : firewallIdを取得します。
	 * @return firewallId Firewall-ID
	 */
	public String getFirewallId() {
	    return firewallId;
	}

	/**
	 * メソッド名 : firewallIdのSetterメソッド
	 * 機能概要 : firewallIdを設定します。
	 * @param firewallId Firewall-ID
	 */
	public void setFirewallId(String firewallId) {
	    this.firewallId = firewallId;
	}

	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得します。
	 * @return vsysId vSys-ID
	 */
	public String getVsysId() {
	    return vsysId;
	}

	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdを設定します。
	 * @param vsysId vSys-ID
	 */
	public void setVsysId(String vsysId) {
	    this.vsysId = vsysId;
	}

	/**
	 * メソッド名 : lastLoginDateのGetterメソッド
	 * 機能概要 : lastLoginDateを取得します。
	 * @return lastLoginDate 最終ログイン時刻
	 */
	public String getLastLoginDate() {
	    return lastLoginDate;
	}

	/**
	 * メソッド名 : lastLoginDateのSetterメソッド
	 * 機能概要 : lastLoginDateを設定します。
	 * @param lastLoginDate 最終ログイン時刻
	 */
	public void setLastLoginDate(String lastLoginDate) {
	    this.lastLoginDate = lastLoginDate;
	}

}
