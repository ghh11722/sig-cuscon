/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : LockListBLogic.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/03/04  tinn.ra@JCCH  初版作成
 * 2010/03/18  tinn.ra@JCCH  更新
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.unlock_account.blogic;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.unlock_account.dto.LockListInput;
import jp.co.kddi.secgw.cuscon.unlock_account.dto.LockListOutput;
import jp.co.kddi.secgw.cuscon.unlock_account.vo.LockAccountRecord;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : LockListBLogic
 * 機能概要 : ロックアカウント一覧取得処理
 * 備考 :
 * @author tinn.ra@JCCH
 * @version 1.0 tinn.ra@JCCH
 *          Created 2010/03/04
 *          新規作成
 * @version 1.1 tinn.ra@JCCH
 *          Created 2010/03/18
 *          成功時のポップアップ表示処理を追加
 */
public class LockListBLogic implements BLogic<LockListInput> {
	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO;
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(LockListBLogic.class);

	/**
	 * メソッド名 : ロックアカウント一覧取得処理
	 * 機能概要 : ロックアカウント一覧を作成する
	 * @param param ロックアカウント一覧取得処理入力を保持したMap
	 * @return ロックアカウント一覧取得処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(LockListInput param) {

		log.debug("ロックアカウント一覧取得処理");

		BLogicResult result = new BLogicResult();
		LockListOutput out = new LockListOutput();
		String resultmsg = param.getResultmsg();

		// 1）アカウントロック一覧作成
		// QueryDAOクラスを使用し、企業管理者テーブルとロックアウトテーブルを取得する。
		List<LockAccountRecord> lock_accounts = null;
		try {
			lock_accounts = queryDAO.executeForObjectList("LockListBLogic-1", null);
			if (lock_accounts.size() == 0) {
				// ・検索Hitしない場合は、一覧のタイトルのみを表示する。
				out.setLock_accounts(lock_accounts);
				if (resultmsg != null && resultmsg.equals("success")) {
					out.setResultmsg(resultmsg);
					result.setMessages(createMessage("DK110004"));
				}
				result.setResultObject(out);
				result.setResultString("success");
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// ・データベースへのアクセスに失敗した場合は、エラーを出力する。
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK111001", objectgArray, param.getUvo()));
			result.setErrors(createMessage("DK110001"));
			result.setResultString("failure");
			return result;
		}

		// 2) レスポンス返却
		out.setLock_accounts(lock_accounts);
		if (resultmsg != null && resultmsg.equals("success")) {
			out.setResultmsg(resultmsg);
			result.setMessages(createMessage("DK110004"));
		}
		result.setResultObject(out);
		result.setResultString("success");
		return result;
	}

	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * 機能概要 : queryDAOを取得します。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOを設定します。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}

	/**
	 * メソッド名 : createMessage
	 * 機能概要 : エラーメッセージを作成する。
	 * @param msg エラーコード
	 * @return messages メッセージ
	 */
	public BLogicMessages createMessage(String msg) {
		BLogicMessages messages = new BLogicMessages();
		messages.add("message", new BLogicMessage(msg));
		return messages;
	}

}
