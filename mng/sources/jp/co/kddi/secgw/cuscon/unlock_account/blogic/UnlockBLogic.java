/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UnlockBLogic.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/03/04  tinn.ra@JCCH  初版作成
 * 2010/03/18  tinn.ra@JCCH  更新
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.unlock_account.blogic;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.unlock_account.dto.UnlockInput;
import jp.co.kddi.secgw.cuscon.unlock_account.dto.UnlockOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : UnlockBLogic
 * 機能概要 : アカウントロック解除処理
 * 備考 :
 * @author tinn.ra@JCCH
 * @version 1.0 tinn.ra@JCCH
 *          Created 2010/03/04
 *          新規作成
 * @version 1.1 tinn.ra@JCCH
 *          Created 2010/03/18
 *          成功時のポップアップ表示処理を追加
 */
public class UnlockBLogic implements BLogic<UnlockInput> {
	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;
	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(UnlockBLogic.class);

	/**
	 * メソッド名 : アカウントロック解除処理
	 * 機能概要 : アカウントロックの解除を行う
	 * @param param アカウントロック解除処理入力を保持したMap
	 * @return アカウントロック解除処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(UnlockInput param) {

		log.debug("アカウントロック解除処理");

		// BLogicResultの生成、設定
		BLogicResult result = new BLogicResult();
		result.setResultString("failure");
		String[] targetLoginId = param.getTargetLoginId();

		// 1）リクエスト解析
		// Validatorで処理するので実装の必要なし
		if (targetLoginId == null) {
			result.setErrors(createMessage("DK110003"));
			return result;
		}

		// 2）ロック解除
		try {
			for (int i=0; i<targetLoginId.length; i++) {
				// ※削除前に排他処理を行う。
				String loginId = queryDAO.executeForObject("UnlockBLogic-1", targetLoginId[i], java.lang.String.class);
				if (loginId == null || loginId.isEmpty()) {
					Object[] objectgArray = {targetLoginId[i]};
					log.error(messageAccessor.getMessage("EK111003", objectgArray, param.getUvo()));
					result.setErrors(createMessage("DK110002"));
					return result;
				} else {
					// ・DELETEの対象がなければ、正常として次の処理を行う。
					// UpdateDAOクラスを使用し、ロックアウトテーブルからデータを削除。
					updateDAO.execute("UnlockBLogic-2", loginId);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// ・データベースへのアクセスに失敗した場合は、エラーを出力する。
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK111002", objectgArray, param.getUvo()));
			result.setErrors(createMessage("DK110002"));
			return result;
		}

		//3）レスポンス返却
		UnlockOutput output = new UnlockOutput();
		output.setResultmsg("success");
		result.setResultObject(output);
		result.setResultString("success");
		return result;
	}

	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * 機能概要 : queryDAOを取得します。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOを設定します。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : updateDAOのGetterメソッド
	 * 機能概要 : updateDAOを取得します。
	 * @return updateDAO DAO
	 */
	public UpdateDAO getUpdateDAO() {
		return updateDAO;
	}

	/**
	 * メソッド名 : updateDAOのSetterメソッド
	 * 機能概要 : updateDAOを設定します。
	 * @param updateDAO DAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		this.updateDAO = updateDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}

	/**
	 * メソッド名 : createMessage
	 * 機能概要 : 各種メッセージを作成する。
	 * @param msg メッセージコード
	 * @return messages メッセージ
	 */
	public BLogicMessages createMessage(String msg) {
		BLogicMessages messages = new BLogicMessages();
		messages.add("message", new BLogicMessage(msg));
		return messages;
	}

}
