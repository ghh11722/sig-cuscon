/*******************************************************************************
 * Copyright(c) 2010 XXXXXXXXXX All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UnlockOutput.java
 *
 * [変更履歴]
 * 日付       更新者          内容
 * 2010/03/18  ratinn@JCCH  XXXXX
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.unlock_account.dto;

/**
 * クラス名 : UnlockOutput
 * 機能概要 :
 * 備考 :
 * @author ratinn@JCCH
 * @version 1.0 ratinn@JCCH
 *          Created 2010/03/18
 *          新規作成
 * @see
 */
public class UnlockOutput {
	// ロックアカウント一覧
	private String[] targetLoginId = null;
	// メッセージ
	private String resultmsg = null;

	/**
	 * メソッド名 : resultmsgのGetterメソッド
	 * 機能概要 : resultmsgを取得する。
	 * @return resultmsg
	 */
	public String getResultmsg() {
		return resultmsg;
	}

	/**
	 * メソッド名 : resultmsgのSetterメソッド
	 * 機能概要 : resultmsgをセットする。
	 * @param resultmsg
	 */
	public void setResultmsg(String resultmsg) {
		this.resultmsg = resultmsg;
	}

	/**
	 * メソッド名 : targetLoginIdのGetterメソッド
	 * 機能概要 : targetLoginIdを取得します。
	 * @return targetLoginId ロックアカウント一覧
	 */
	public String[] getTargetLoginId() {
	    return targetLoginId;
	}

	/**
	 * メソッド名 : targetLoginIdのSetterメソッド
	 * 機能概要 : targetLoginIdを設定します。
	 * @param targetLoginId ロックアカウント一覧
	 */
	public void setTargetLoginId(String[] targetLoginId) {
	    this.targetLoginId = targetLoginId;
	}

}
