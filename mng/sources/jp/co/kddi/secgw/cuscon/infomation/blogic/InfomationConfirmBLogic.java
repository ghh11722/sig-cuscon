/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名 : InfomationConfirmBLogic.java
 *
 * [変更履歴]
 * 日付        更新者       内容
 * 2010/03/04  h.kubo@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.infomation.blogic;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.infomation.dto.InfomationConfirmInput;
import jp.co.kddi.secgw.cuscon.infomation.dto.InfomationConfirmOutput;
import jp.co.kddi.secgw.cuscon.infomation.vo.SelectInfomation;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : InfomationConfirmBLogic
 * 機能概要 : お知らせ登録確認用ビジネスロジック
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class InfomationConfirmBLogic implements BLogic<InfomationConfirmInput> {
	// DAO
	private QueryDAO queryDAO = null;
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(InfomationConfirmBLogic.class);
	// 入力画面のプレビューボタン
	private static final String CONFIRM_PREVIEW = "プレビュー";
	// 入力画面のCancelボタン
	private static final String CONFIRM_CANCEL = "Cancel";
	// お知らせメッセージ最大桁数
	private static final int INFOMATION_MESSAGE_LENGTH = 5000;

	/**
	 * メソッド名 : お知らせメッセージ確認画面作成処理
	 * 機能概要 : お知らせメッセージ確認画面を作成する
	 * @param param 入力されたお知らせメッセージを保持したMap
	 * @return お知らせメッセージ確認画面作成処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(InfomationConfirmInput param) {

		log.debug("お知らせメッセージ確認画面作成処理");

		// 処理結果
		BLogicResult result = new BLogicResult();
		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		// システム情報(お知らせメッセージ)
		SelectInfomation info_rec = null;

		// 1) リクエスト解析
		log.debug("1) リクエスト解析");
		String new_infomation = param.getNewInfomation();
		if (new_infomation == null) {
			// エラー : リクエスト値エラー
			log.error(messageAccessor.getMessage("EK141003", null, param.getUvo()));
			messages.add("message", new BLogicMessage("DK140002"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

		if (new_infomation.length() > INFOMATION_MESSAGE_LENGTH) {
			// エラー : リクエスト値エラー
			Object[] objectgArray = {Integer.toString(INFOMATION_MESSAGE_LENGTH)};
			log.error(messageAccessor.getMessage("EK141004", objectgArray, param.getUvo()));
			messages.add("message", new BLogicMessage("DK140002"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

		if (param.getConfirm() == null || !(param.getConfirm().equals(CONFIRM_PREVIEW) || param.getConfirm().equals(CONFIRM_CANCEL))) {
			// エラー : リクエスト値エラー
			log.error(messageAccessor.getMessage("EK141005", null, param.getUvo()));
			messages.add("message", new BLogicMessage("DK140002"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		log.debug("1) OK");

		// 2) 現行お知らせメッセージ取得
		// システム情報(お知らせメッセージ)を取得する
		log.debug("2) 現行お知らせメッセージ取得");
		try {
			info_rec = queryDAO.executeForObject("InfomationInputBLogic-1", null, SelectInfomation.class);
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK141006", objectgArray, param.getUvo()));
			messages.add("message", new BLogicMessage("DK140001"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

		if (info_rec == null) {
			// エラー : システム情報取得失敗
			log.fatal(messageAccessor.getMessage("FK141007", null, param.getUvo()));
			messages.add("message", new BLogicMessage("DK140001"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		log.debug("2) OK");

		// 3) 登録データ保存
		log.debug("3) 登録データ保存");
		InfomationConfirmOutput out = new InfomationConfirmOutput();
		out.setNewInfomation(new_infomation);
		log.debug("3) OK");

		// 4) レスポンス返却
		out.setInfomation(info_rec.getInfomation());
		result = new BLogicResult();
		result.setResultObject(out);
		result.setResultString("success");

		return result;
	}

	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * queryDAOを取得します。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * queryDAOを設定します。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}
}
