/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CustomerMainForm.java
 *
 * [変更履歴]
 * 日付        更新者         内容
 * 2010/03/04  h.kubo@JCCH    初版作成
 * 2013/09/01  kkato@PROSITE  URL-DBのVer表示
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.infomation.form;

import jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx;

/**
 * クラス名 : CustomerMainForm
 * 機能概要 : 顧客企業メイン画面アクションフォーム
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *          Created 2010/03/04
 *          新規作成
 * @see jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx
 */
public class CustomerMainForm extends ValidatorActionFormEx {

	//
	private static final long serialVersionUID = 1L;
	// お知らせメッセージ
	private String infomation = null;
// 20130901 kkato@PROSITE add start
	// URL-DBのVer
	private String urlDbVersion = null;
// 20130901 kkato@PROSITE add end

	/**
	 * メソッド名 : infomationのGetterメソッド
	 * 機能概要 : infomationを取得する。
	 * @return infomation お知らせメッセージ
	 */
	public String getInfomation() {
		return infomation;
	}

	/**
	 * メソッド名 : infomationのSetterメソッド
	 * 機能概要 : infomationをセットする。
	 * @param infomation お知らせメッセージ
	 */
	public void setInfomation(String infomation) {
		this.infomation = infomation;
	}
// 20130901 kkato@PROSITE add start
	/**
	 * メソッド名 : urlDbVersionのGetterメソッド
	 * 機能概要 : urlDbVersionを取得する。
	 * @return urlDbVersion
	 */
	public String getUrlDbVersion() {
		return urlDbVersion;
	}

	/**
	 * メソッド名 : urlDbVersionのSetterメソッド
	 * 機能概要 : urlDbVersionをセットする。
	 * @param urlDbVersion
	 */
	public void setUrlDbVersion(String urlDbVersion) {
		this.urlDbVersion = urlDbVersion;
	}
// 20130901 kkato@PROSITE add end
}
