/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名 : InfomationInputBLogic.java
 *
 * [変更履歴]
 * 日付        更新者       内容
 * 2010/03/04  h.kubo@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.infomation.blogic;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.infomation.dto.DefaultInput;
import jp.co.kddi.secgw.cuscon.infomation.dto.InfomationOutput;
import jp.co.kddi.secgw.cuscon.infomation.vo.SelectInfomation;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : InfomationInputBLogic
 * 機能概要 : お知らせ入力用ビジネスロジック
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class InfomationInputBLogic implements BLogic<DefaultInput> {
	// DAO
	private QueryDAO queryDAO = null;
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(InfomationInputBLogic.class);

	/**
	 * メソッド名 : お知らせメッセージ入力画面作成処理
	 * 機能概要 : お知らせメッセージ入力画面を作成する
	 * @param param デフォルト情報を保持したMap
	 * @return お知らせメッセージ入力画面作成処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(DefaultInput param) {

		log.debug("お知らせメッセージ入力画面作成処理");

		// 処理結果
		BLogicResult result = new BLogicResult();
		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		// システム情報(お知らせメッセージ)
		SelectInfomation info_rec = null;

		// 1) 現行お知らせメッセージ取得
		// システム情報(お知らせメッセージ)を取得する
		log.debug("1) 現行お知らせメッセージ取得");
		try {
			info_rec = queryDAO.executeForObject("InfomationInputBLogic-1", null, SelectInfomation.class);
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK141001", objectgArray, param.getUvo()));
			messages.add("message", new BLogicMessage("DK140001"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		if (info_rec == null) {
			// エラー : システム情報取得失敗
			log.fatal(messageAccessor.getMessage("FK141002", null, param.getUvo()));
			messages.add("message", new BLogicMessage("DK140001"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		log.debug("1) OK");

		// 2) レスポンス返却
		InfomationOutput out = new InfomationOutput();
		out.setInfomation(info_rec.getInfomation());
		out.setNewInfomation(info_rec.getInfomation());
		result.setResultObject(out);
		result.setResultString("success");

		return result;
	}

	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * 機能概要 : queryDAOを取得する。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}
}
