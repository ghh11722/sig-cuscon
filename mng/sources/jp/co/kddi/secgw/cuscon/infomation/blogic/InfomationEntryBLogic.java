/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名 : InfomationEntryBLogic.java
 *
 * [変更履歴]
 * 日付        更新者       内容
 * 2010/03/04  h.kubo@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.infomation.blogic;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.infomation.dto.InfomationEntryInput;
import jp.co.kddi.secgw.cuscon.infomation.dto.InfomationEntryOutput;
import jp.co.kddi.secgw.cuscon.infomation.vo.SelectInfomation;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : InfomationEntryBLogic
 * 機能概要 : お知らせメッセージ登録処理ビジネスロジック
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class InfomationEntryBLogic implements BLogic<InfomationEntryInput> {
	// DAO
	private QueryDAO queryDAO = null;
	private UpdateDAO updateDAO = null;
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(InfomationEntryBLogic.class);
	// 確認画面のOKボタン
	private static final String CONFIRM_OK = "OK";
	// 確認画面のCancelボタン
	private static final String CONFIRM_CANCEL = "Cancel";

	/**
	 * メソッド名 : お知らせメッセージ登録処理
	 * 機能概要 : お知らせメッセージをDBに登録する
	 * @param param 入力されたお知らせメッセージ情報を保持したMap
	 * @return お知らせメッセージ登録処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(InfomationEntryInput param) {

		log.debug("お知らせメッセージ登録処理");

		// 処理結果
		BLogicResult result = new BLogicResult();
		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		InfomationEntryOutput out = new InfomationEntryOutput();

		// 1) セッション情報取得
		log.debug("1) セッション情報取得");
		String new_infomation = param.getNewInfomation();
		if (new_infomation == null) {
			// エラー : セッション情報無し
			log.error(messageAccessor.getMessage("EK141008", null, param.getUvo()));
			messages.add("message", new BLogicMessage("DK140002"));
			result.setErrors(messages);
			result.setResultObject(out);
			result.setResultString("failure");
			return result;
		}
		log.debug("1) OK");

		// 2) リクエスト解析
		log.debug("2) リクエスト解析");
		// OK,Cancelボタン以外の場合、セッションデータを破棄して終了
		if (param.getConfirm() == null || !(param.getConfirm().equals(CONFIRM_OK) || param.getConfirm().equals(CONFIRM_CANCEL))) {
			// エラー : 不正アクセス
			log.error(messageAccessor.getMessage("EK141009", null, param.getUvo()));
			messages.add("message", new BLogicMessage("DK140002"));
			result.setErrors(messages);
			result.setResultObject(out);
			result.setResultString("failure");
			return result;
		}
		// Cancelボタンの場合、セッションデータを破棄して終了
		if (param.getConfirm().equals(CONFIRM_CANCEL)) {
			log.debug("cancelボタンクリック");
			// Cancel
			out.setNewInfomation(new_infomation);
			result.setResultObject(out);
			result.setResultString("cancel");
			return result;
		}
		log.debug("2) OK");

		// 3) お知らせメッセージ登録
		log.debug("3) お知らせメッセージ登録");
		try {
			SelectInfomation info = new SelectInfomation();
			info.setInfomation(new_infomation);
			// お知らせメッセージを更新する
			updateDAO.execute("InfomationEntryBLogic-1", info);
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : お知らせメッセージ更新失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK141010", objectgArray, param.getUvo()));
			messages.add("message", new BLogicMessage("DK140003"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		log.debug("3) OK");

		// 4) レスポンス返却
		out.setNewInfomation(new_infomation);

		result.setResultObject(out);
		messages.add("message", new BLogicMessage("DK140004"));
		result.setMessages(messages);
		result.setResultString("success");
		return result;
	}

	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * queryDAOを取得します。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * queryDAOを設定します。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : updateDAOのGetterメソッド
	 * updateDAOを取得します。
	 * @return updateDAO DAO
	 */
	public UpdateDAO getUpdateDAO() {
		return updateDAO;
	}

	/**
	 * メソッド名 : updateDAOのSetterメソッド
	 * updateDAOを設定します。
	 * @param updateDAO DAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		this.updateDAO = updateDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}
}
