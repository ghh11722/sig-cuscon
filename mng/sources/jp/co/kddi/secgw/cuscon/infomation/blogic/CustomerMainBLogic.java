/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名 : CustomerMainBLogic.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/03/04  h.kubo@JCCH      初版作成
 * 2013/09/01  kkato@PROSITE    URL-DBのVer表示
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.infomation.blogic;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.infomation.dto.CustomerMainOutput;
import jp.co.kddi.secgw.cuscon.infomation.dto.DefaultInput;
import jp.co.kddi.secgw.cuscon.infomation.vo.SelectInfomation;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : CustomerMainBLogic
 * 機能概要 : 顧客企業メインを作成するビジネスロジック
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class CustomerMainBLogic implements BLogic<DefaultInput> {
	// DAO
	private QueryDAO queryDAO = null;
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(CustomerMainBLogic.class);

	/**
	 * メソッド名 : 顧客企業メイン作成処理
	 * 機能概要 : お知らせメッセージを取得する
	 * @param param デフォルト情報を保持したMap
	 * @return 企業メイン作成処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(DefaultInput param) {

		log.debug("企業メイン作成処理");

		// 処理結果
		BLogicResult result = new BLogicResult();
		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		// システム情報(動作モード,PAアドレス)
		SelectInfomation info_rec = null;

		// 現行お知らせメッセージ取得
		// システム情報(お知らせメッセージ)を取得する
		try {
			info_rec = queryDAO.executeForObject("CustomerMainBLogic-1", null, SelectInfomation.class);
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : システム情報取得失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK041001", objectgArray, param.getUvo()));
			messages.add("message", new BLogicMessage("DK040001"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		if (info_rec == null) {
			// エラー : システム情報取得失敗
			log.fatal(messageAccessor.getMessage("FK041002", null, param.getUvo()));
			messages.add("message", new BLogicMessage("DK040001"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

		// レスポンス返却
		CustomerMainOutput output = new CustomerMainOutput();
		output.setInfomation(info_rec.getInfomation());
// 20130901 kkato@PROSITE add start
		output.setUrlDbVersion(info_rec.getUrlDbVersion());
// 20130901 kkato@PROSITE add end

		result = new BLogicResult();
		result.setResultObject(output);
		result.setResultString("success");

		return result;
	}

	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * 機能概要 : queryDAOを取得する。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}
}
