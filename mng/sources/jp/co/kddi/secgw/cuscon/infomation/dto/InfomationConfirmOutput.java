/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : InfomationConfirmOutput.java
 *
 * [変更履歴]
 * 日付        更新者       内容
 * 2010/03/04  h.kubo@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.infomation.dto;

/**
 * クラス名 : InfomationConfirmOutput
 * 機能概要 : お知らせ登録確認処理出力定義クラス
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class InfomationConfirmOutput {

	// 現行お知らせメッセージ
	private String infomation;
	// 新規お知らせメッセージ
	private String newInfomation;

	/**
	 * メソッド名 : infomationのGetterメソッド
	 * 機能概要 : infomationを取得します。
	 * @return infomation 現行お知らせメッセージ
	 */
	public String getInfomation() {
		return infomation;
	}

	/**
	 * メソッド名 : infomationのSetterメソッド
	 * 機能概要 : infomationをセットする。
	 * @param infomation 現行お知らせメッセージ
	 */
	public void setInfomation(String infomation) {
		this.infomation = infomation;
	}

	/**
	 * メソッド名 : newInfomationのGetterメソッド
	 * 機能概要 : newInfomationを取得する。
	 * @return newInfomation 新規お知らせメッセージ
	 */
	public String getNewInfomation() {
		return newInfomation;
	}

	/**
	 * メソッド名 : newInfomationのSetterメソッド
	 * 機能概要 : newInfomationをセットする。
	 * @param newInfomation 新規お知らせメッセージ
	 */
	public void setNewInfomation(String newInfomation) {
		this.newInfomation = newInfomation;
	}

}
