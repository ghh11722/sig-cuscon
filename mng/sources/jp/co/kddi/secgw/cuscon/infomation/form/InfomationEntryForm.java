/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : InfomationEntryForm.java
 *
 * [変更履歴]
 * 日付        更新者       内容
 * 2010/03/04  h.kubo@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.infomation.form;

import jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx;

/**
 * クラス名 : InfomationEntryForm
 * 機能概要 : お知らせメッセージ画面アクションフォーム
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *          Created 2010/03/04
 *          新規作成
 * @see jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx
 */
public class InfomationEntryForm extends ValidatorActionFormEx {

	//
	private static final long serialVersionUID = 1L;
	// 現行お知らせメッセージ
	private String infomation;
	// 新規お知らせメッセージ
	private String newInfomation;
	// 処理結果メッセージ
	private String resultmsg;
	// アクション名 OK or Cancel
	private String confirm;

	/**
	 * メソッド名 : infomationのGetterメソッド
	 * 機能概要 : infomationを取得する。
	 * @return infomation 現行お知らせメッセージ
	 */
	public String getInfomation() {
		return infomation;
	}

	/**
	 * メソッド名 : infomationのSetterメソッド
	 * 機能概要 : infomationをセットする。
	 * @param infomation 現行お知らせメッセージ
	 */
	public void setInfomation(String infomation) {
		this.infomation = infomation;
	}

	/**
	 * メソッド名 : newInfomationのGetterメソッド
	 * 機能概要 : newInfomationを取得する。
	 * @return newInfomation 新規お知らせメッセージ
	 */
	public String getNewInfomation() {
		return newInfomation;
	}

	/**
	 * メソッド名 : newInfomationのSetterメソッド
	 * 機能概要 : newInfomationをセットする。
	 * @param newInfomation 新規お知らせメッセージ
	 */
	public void setNewInfomation(String newInfomation) {
		this.newInfomation = newInfomation;
	}

	/**
	 * メソッド名 : resultmsgのGetterメソッド
	 * 機能概要 : resultmsgを取得する。
	 * @return resultmsg 処理結果メッセージ
	 */
	public String getResultmsg() {
		return resultmsg;
	}

	/**
	 * メソッド名 : resultmsgのSetterメソッド
	 * 機能概要 : resultmsgをセットする。
	 * @param resultmsg 処理結果メッセージ
	 */
	public void setResultmsg(String resultmsg) {
		this.resultmsg = resultmsg;
	}

	/**
	 * メソッド名 : confirmのGetterメソッド
	 * 機能概要 : confirmを取得する。
	 * @return confirm アクション名
	 */
	public String getConfirm() {
		return confirm;
	}

	/**
	 * メソッド名 : confirmのSetterメソッド
	 * 機能概要 : confirmをセットする。
	 * @param confirm アクション名
	 */
	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}

}
