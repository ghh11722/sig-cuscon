/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : InfomationEntryInput.java
 *
 * [変更履歴]
 * 日付        更新者       内容
 * 2010/03/04  h.kubo@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.infomation.dto;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;

/**
 * クラス名 : InfomationEntryInput
 * 機能概要 :
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class InfomationEntryInput {

	// UserValueObject
	private CusconUVO uvo = null;
	// 新規お知らせメッセージ
	private String newInfomation;
	// アクション名 OK or Cancel
	private String confirm = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo UserValueObject
	 */
	public CusconUVO getUvo() {
		return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo UserValueObject
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}

	/**
	 * メソッド名 : newInfomationのGetterメソッド
	 * 機能概要 : newInfomationを取得する。
	 * @return newInfomation 新規お知らせメッセージ
	 */
	public String getNewInfomation() {
		return newInfomation;
	}

	/**
	 * メソッド名 : newInfomationのSetterメソッド
	 * 機能概要 : newInfomationをセットする。
	 * @param newInfomation 新規お知らせメッセージ
	 */
	public void setNewInfomation(String newInfomation) {
		this.newInfomation = newInfomation;
	}

	/**
	 * メソッド名 : confirmのGetterメソッド
	 * 機能概要 : confirmを取得する。
	 * @return confirm アクション名
	 */
	public String getConfirm() {
		return confirm;
	}

	/**
	 * メソッド名 : confirmのSetterメソッド
	 * 機能概要 : confirmをセットする。
	 * @param confirm アクション名
	 */
	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}

}
