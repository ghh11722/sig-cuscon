/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : DefaultInput.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/03/04  m.narikawa@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.infomation.dto;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;

/**
 * クラス名 : DefaultInput
 * 機能概要 : ログアウト入力定義クラス
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class DefaultInput {

	// UserValueObject
	private CusconUVO uvo = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得します。
	 * @return uvo UserValueObject
	 */
	public CusconUVO getUvo() {
	    return uvo;
	}

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを設定します。
	 * @param uvo UserValueObject
	 */
	public void setUvo(CusconUVO uvo) {
	    this.uvo = uvo;
	}

}
