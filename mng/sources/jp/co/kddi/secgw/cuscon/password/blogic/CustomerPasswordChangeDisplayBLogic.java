/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CustomerPasswordChangeDisplayBLogic.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/03/04  tinn.ra@JCCH  初版作成
 * 2010/03/18  tinn.ra@JCCH  更新
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.password.blogic;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.password.dto.PasswordChangeInput;
import jp.co.kddi.secgw.cuscon.password.dto.PasswordChangeOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : CustomerPasswordChangeDisplayBLogic
 * 機能概要 : 企業管理者パスワード変更画面表示処理
 * 備考 :
 * @author tinn.ra@JCCH
 * @version 1.0 tinn.ra@JCCH
 *          Created 2010/03/04
 *          新規作成
 * @version 1.1 tinn.ra@JCCH
 *          Created 2010/03/18
 *          成功時のポップアップ表示処理を追加
 */
public class CustomerPasswordChangeDisplayBLogic implements BLogic<PasswordChangeInput> {
    // QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(CustomerPasswordChangeDisplayBLogic.class);

	/**
	 * メソッド名 : 企業管理者パスワード変更処理
	 * 機能概要 : 企業管理者のパスワードを変更する
	 * @param param パスワード変更処理入力を保持したMap
	 * @return 企業管理者パスワード変更処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(PasswordChangeInput param) {

		log.debug("企業管理者パスワード変更表示処理");

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		result.setResultString("failure");

		// CusconUVOからログインIDを取得する
		CusconUVO uvo = param.getUvo();
		String customerLoginId = uvo.getCustomerLoginId();
		if (customerLoginId == null) {
			log.error(messageAccessor.getMessage("EC091001", null, param.getUvo()));
			result.setErrors(createErrMessage("DC090001"));/*セッション情報読込失敗!!*/
			return result;
		}

		String loginId = customerLoginId;
		// Firewall-ID取得
		String firewallId = null;
		try {
			firewallId = queryDAO.executeForObject("CustomerPasswordChangeDisplayBLogic-1", loginId, java.lang.String.class);
			// エラー : Firewall-ID取得失敗
			if (firewallId == null || firewallId.isEmpty()) {
				log.error(messageAccessor.getMessage("EC091002", null, param.getUvo()));
				result.setErrors(createErrMessage("DC090002"));
				return result;
			}
		} catch (Exception e) {
			// ・不備があった場合は、エラーを出力する。
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FC091003", objectgArray, param.getUvo()));
			result.setErrors(createErrMessage("DC090003"));/*DBアクセスエラー!!*/
			return result;
		}

		PasswordChangeOutput out = new PasswordChangeOutput();
		out.setFirewallId(firewallId);
		out.setOld_password(null);
		out.setNew_password(null);
		out.setPassword_check(null);
		out.setResultmsg(null);
		result.setResultObject(out);
		result.setResultString("success");
		return result;
	}


	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * 機能概要 : queryDAOを取得する。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}


	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}


	/**
	 * メソッド名 : createErrMessage
	 * 機能概要 : エラーメッセージを作成する。
	 * @param error エラーコード
	 * @return messages メッセージ
	 */
	public BLogicMessages createErrMessage(String error) {
		BLogicMessages messages = new BLogicMessages();
		messages.add("message", new BLogicMessage(error));
		return messages;
	}

}
