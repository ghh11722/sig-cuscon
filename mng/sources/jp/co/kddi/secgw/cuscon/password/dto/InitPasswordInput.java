/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : InitPasswordInput.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/03/04  tinn.ra@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.password.dto;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.password.vo.SelectAccountList;

/**
 * クラス名 : InitPasswordInput
 * 機能概要 : パスワード初期化処理入力定義
 * 備考 :
 * @author tinn.ra@JCCH
 * @version 1.0 tinn.ra@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class InitPasswordInput {
	// 初期化したいログインID
	private String passInitID;
	// アカウント情報
	private SelectAccountList target_account = null;
	// アクション名 OK or Cancel
	private String confirm = null;
	// UserValueObject
	private CusconUVO uvo = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo UserValueObject
	 */
	public CusconUVO getUvo() {
		return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo UserValueObject
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}

	/**
	 * メソッド名 : passInitIDのGetterメソッド
	 * 機能概要 : passInitIDを取得します。
	 * @return passInitID 初期化したいログインID
	 */
	public String getPassInitID() {
		return passInitID;
	}

	/**
	 * メソッド名 : passInitIDのGetterメソッド
	 * 機能概要 : passInitIDを設定します。
	 * @param passInitID 初期化したいログインID
	 */
	public void setPassInitID(String passInitID) {
		this.passInitID = passInitID;
	}

	/**
	 * メソッド名 : accountsのGetterメソッド
	 * 機能概要 : accountsを取得します。
	 * @return accounts アカウント情報
	 */
	public SelectAccountList getTarget_account() {
	    return target_account;
	}

	/**
	 * メソッド名 : target_accountのGetterメソッド
	 * 機能概要 : target_accountを設定します。
	 * @param target_account アカウント情報
	 */
	public void setTarget_account(SelectAccountList target_account) {
	    this.target_account = target_account;
	}

	/**
	 * メソッド名 : confirmのGetterメソッド
	 * 機能概要 : confirmを取得します。
	 * @return confirm アクション名
	 */
	public String getConfirm() {
	    return confirm;
	}

	/**
	 * メソッド名 : confirmのGetterメソッド
	 * 機能概要 : confirmを設定します。
	 * @param confirm アクション名
	 */
	public void setConfirm(String confirm) {
	    this.confirm = confirm;
	}

}
