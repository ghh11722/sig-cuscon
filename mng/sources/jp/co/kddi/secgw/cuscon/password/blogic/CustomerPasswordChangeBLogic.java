/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CustomerPasswordChangeBLogic.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/03/04  tinn.ra@JCCH  初版作成
 * 2010/03/15  tinn.ra@JCCH  更新
 * 2010/03/18  tinn.ra@JCCH  更新
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.password.blogic;

import java.sql.Timestamp;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.password.vo.CustomerRecord;
import jp.co.kddi.secgw.cuscon.password.dto.PasswordChangeInput;
import jp.co.kddi.secgw.cuscon.password.dto.PasswordChangeOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : CustomerPasswordChangeBLogic
 * 機能概要 : 企業管理者パスワード変更処理
 * 備考 :
 * @author tinn.ra@JCCH
 * @version 1.0 tinn.ra@JCCH
 *          Created 2010/03/04
 *          新規作成
 * @version 1.1 tinn.ra@JCCH
 *          Created 2010/03/15
 *          入力チェック処理を変更
 * @version 1.2 tinn.ra@JCCH
 *          Created 2010/03/18
 *          成功時のポップアップ表示処理を追加
 */
public class CustomerPasswordChangeBLogic implements BLogic<PasswordChangeInput> {
    // QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;
    // UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(CustomerPasswordChangeBLogic.class);

	/**
	 * メソッド名 :execute
	 * 機能概要 :パスワード変更処理のビジネスロジックを実行する
	 * @param param パスワード変更処理入力を保持したMap
	 * @return パスワード変更処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(PasswordChangeInput param) {

		log.debug("企業管理者パスワード変更処理");

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		result.setResultString("failure");

		// 1) セッションデータ読込→CusconUVOからログインIDを取得する
		CusconUVO uvo = param.getUvo();
		String customerLoginId = uvo.getCustomerLoginId();
		if (customerLoginId == null) {
			log.error(messageAccessor.getMessage( "EC091001", null, param.getUvo()));
			result.setErrors(createErrMessage("DC090001"));/*セッション情報読込失敗!!*/
			return result;
		}

		String loginId = customerLoginId;
		String oldpassword = param.getOld_password();
		String password = param.getNew_password();
		String password_check = param.getPassword_check();

		// 2) リクエスト解析
		// バリデーションにて実現する

		// 3) 新パスワードチェック
		// 新パスワードとログインID重複
		if (password.equals(loginId)) {
			Object[] objectgArray = {loginId};
			log.info(messageAccessor.getMessage("IC091006", objectgArray, param.getUvo()));
			result.setErrors(createErrMessage("DC090015"));
			return result;
		}
		// 新パスワードとFirewall-ID重複
		String firewallId = null;
		try {
			firewallId = queryDAO.executeForObject("CustomerPasswordChangeBLogic-6", loginId, java.lang.String.class);
			// エラー : Firewall-ID取得失敗
			if (firewallId == null || firewallId.isEmpty()) {
				log.error(messageAccessor.getMessage("EC091009", null, param.getUvo()));
				result.setErrors(createErrMessage("DC090002"));
				return result;
			}
		} catch (Exception e) {
			// ・不備があった場合は、エラーを出力する。
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FC091010", objectgArray, param.getUvo()));
			result.setErrors(createErrMessage("DC090003"));
			return result;
		}
		if (password.equals(firewallId)) {
			Object[] objectgArray = {loginId};
			log.info(messageAccessor.getMessage("IC091007", objectgArray, param.getUvo()));
			result.setErrors(createErrMessage("DC090016"));
			return result;
		}
		// 新パスワードと旧パスワード重複
		if (oldpassword.equals(password)) {
			Object[] objectgArray = {loginId};
			log.info(messageAccessor.getMessage("IC091001", objectgArray, param.getUvo()));
			result.setErrors(createErrMessage("DC090004"));
			return result;
		}
		// 新パスワードと新パスワード(再入力)不一致
		if (!(password.equals(password_check))) {
			Object[] objectgArray = {loginId};
			log.info(messageAccessor.getMessage("IC091002", objectgArray, param.getUvo()));
			result.setErrors(createErrMessage("DC090005"));
			return result;
		}

		// 4) 現パスワード認証
		CustomerRecord customerRecord = new CustomerRecord();
		customerRecord.setLoginId(loginId);
		customerRecord.setOld_password(oldpassword);
		customerRecord.setNew_password(password);
		try {
			int newpwdcount = queryDAO.executeForObject("CustomerPasswordChangeBLogic-1", customerRecord, java.lang.Integer.class);
			// エラー : パスワード認証失敗
			if (newpwdcount != 1) {
				Object[] objectgArray = {loginId};
				log.info(messageAccessor.getMessage("IC091003", objectgArray, param.getUvo()));
				result.setErrors(createErrMessage("DC090006"));
				return result;
			}
		} catch (Exception e) {
			// ・不備があった場合は、エラーを出力する。
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FC091004", objectgArray, param.getUvo()));
			result.setErrors(createErrMessage("DC090014"));
			return result;
		}

		// 5) 有効期限取得
		String expiry = null;
		try {
			expiry = queryDAO.executeForObject("getExpiry", null, java.lang.String.class);
			if (expiry == null) {
				log.fatal(messageAccessor.getMessage("FC091005", null, param.getUvo()));
				result.setErrors(createErrMessage("DC090007"));
				return result;
			}
		} catch (Exception e) {
			// ・不備があった場合は、エラーを出力する。
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FC091006", objectgArray, param.getUvo()));
			result.setErrors(createErrMessage("DC090008"));
			return result;
		}

		// 有効期限ミリ秒計算
		int period = Integer.parseInt(expiry);
		long mili = period * 3600 * 24 * 1000L;

		// 現在時刻取得
		Timestamp current_date = new Timestamp(System.currentTimeMillis());

		// 有効期限日時計算
		long current_mili = current_date.getTime()-mili;
		Timestamp expiration_date = new Timestamp(current_mili);
		customerRecord.setExpiration_date(expiration_date);

		// 6) 新パスワード重複チェック
		try {
			int newpwdcount = queryDAO.executeForObject("CustomerPasswordChangeBLogic-2", customerRecord, java.lang.Integer.class);
			if (newpwdcount != 0) {
				Object[] objectgArray = {loginId};
				log.info(messageAccessor.getMessage("IC091004", objectgArray, param.getUvo()));
				result.setErrors(createErrMessage("DC090009"));
				return result;
			}
		} catch (Exception e) {
			// ・不備があった場合は、エラーを出力する。
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FC091007", objectgArray, param.getUvo()));
			result.setErrors(createErrMessage("DC090010"));
			return result;
		}

		// 7) 新パスワード登録
		try {
			// 新パスワードを企業管理者テーブルへ登録する
			updateDAO.execute("CustomerPasswordChangeBLogic-3", customerRecord);
			// 現パスワードを旧パスワードテーブルへインサートする
			updateDAO.execute("CustomerPasswordChangeBLogic-4", customerRecord);
			// 保存期間を過ぎたパスワードは削除する
			updateDAO.execute("CustomerPasswordChangeBLogic-5", customerRecord);
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// ・不備があった場合は、エラーを出力する。
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FC091008", objectgArray, param.getUvo()));
			result.setErrors(createErrMessage("DC090011"));
			return result;
		}

		// 8) レスポンス返却
		Object[] objectgArray = {loginId};
		log.info(messageAccessor.getMessage("IC091005", objectgArray, param.getUvo()));
		PasswordChangeOutput out = new PasswordChangeOutput();
		out.setOld_password(null);
		out.setNew_password(null);
		out.setPassword_check(null);
		out.setResultmsg("success");
		result.setResultObject(out);
		result.setResultString("success");
		result.setMessages(createErrMessage("DC090017"));
		return result;
	}

	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * 機能概要 : queryDAOを取得する。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : updateDAOのGetterメソッド
	 * 機能概要 : updateDAOを取得する。
	 * @return updateDAO DAO
	 */
	public UpdateDAO getUpdateDAO() {
		return updateDAO;
	}

	/**
	 * メソッド名 : updateDAOのSetterメソッド
	 * 機能概要 : updateDAOをセットする。
	 * @param updateDAO DAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		this.updateDAO = updateDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}

	/**
	 * メソッド名 : createErrMessage
	 * 機能概要 : エラーメッセージを作成する。
	 * @param error エラーコード
	 * @return messages メッセージ
	 */
	public BLogicMessages createErrMessage(String error) {
		BLogicMessages messages = new BLogicMessages();
		messages.add("message", new BLogicMessage(error));
		return messages;
	}

}
