/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : InitPasswordListOutput.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/03/04  tinn.ra@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.password.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.password.vo.SelectAccountList;

/**
 * クラス名 : InitPasswordListOutput
 * 機能概要 : パスワード初期化一覧処理出力定義
 * 備考 :
 * @author tinn.ra@JCCH
 * @version 1.0 tinn.ra@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class InitPasswordListOutput {
	// アカウント一覧
	private List<SelectAccountList> accounts = null;
	// 初期化したいログインID
	private String passInitID;

	/**
	 * メソッド名 : accountsのGetterメソッド
	 * 機能概要 : accountsを取得します。
	 * @return accounts アカウント一覧
	 */
	public List<SelectAccountList> getAccounts() {
	    return accounts;
	}

	/**
	 * メソッド名 : accountsのSetterメソッド
	 * 機能概要 : accountsを設定します。
	 * @param accounts アカウント一覧
	 */
	public void setAccounts(List<SelectAccountList> accounts) {
	    this.accounts = accounts;
	}

	/**
	 * メソッド名 : passInitIDのGetterメソッド
	 * 機能概要 : passInitIDを取得します。
	 * @return passInitID 初期化したいログインID
	 */
	public String getPassInitID() {
		return passInitID;
	}

	/**
	 * メソッド名 : passInitIDのSetterメソッド
	 * 機能概要 : passInitIDを設定します。
	 * @param passInitID 初期化したいログインID
	 */
	public void setPassInitID(String passInitID) {
		this.passInitID = passInitID;
	}

}
