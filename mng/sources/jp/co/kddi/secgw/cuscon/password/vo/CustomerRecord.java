/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CustomerRecord.java
 *
 * [変更履歴]
 * 日付        更新者       内容
 * 2010/03/04  ratinn@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.password.vo;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * クラス名 : CustomerRecord
 * 機能概要 : 企業パスワード変更用のレコードデータ（DBへの登録、削除に使用）
 * 備考 :
 * @author ratinn@JCCH
 * @version 1.0 ratinn@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class CustomerRecord implements Serializable {

	//
	private static final long serialVersionUID = 1L;
	// 暗号化鍵
	private String cryptKey = "kddi-secgw";
	// ログインID
	private String loginId = null;
	// 旧パスワードの保存期間
	private String period = null;
	// 現パスワード
	private String old_password = null;
	// 新パスワード
	private String new_password = null;
	// 保存期間
	private Timestamp expiration_date;

	/**
	 * メソッド名 : cryptKeyのGetterメソッド
	 * 機能概要 : cryptKeyを取得します。
	 * @return cryptKey 暗号化鍵
	 */
	public String getCryptKey() {
		return cryptKey;
	}

	/**
	 * メソッド名 : cryptKeyのSetterメソッド
	 * 機能概要 : cryptKeyを設定します。
	 * @param cryptKey 暗号化鍵
	 */
	public void setCryptKey(String cryptKey) {
		this.cryptKey = cryptKey;
	}

	/**
	 * メソッド名 : loginIdのGetterメソッド
	 * 機能概要 : loginIdを取得します。
	 * @return loginId ログインID
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * メソッド名 : loginIdのSetterメソッド
	 * 機能概要 : loginIdを設定します。
	 * @param loginId ログインID
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * メソッド名 : periodのGetterメソッド
	 * 機能概要 : periodを取得します。
	 * @return period 旧パスワードの保存期間
	 */
	public String getPeriod() {
		return period;
	}

	/**
	 * メソッド名 : periodのSetterメソッド
	 * 機能概要 : periodを設定します。
	 * @param period 旧パスワードの保存期間
	 */
	public void setPeriod(String period) {
		this.period = period;
	}


	/**
	 * メソッド名 : new_passwordのGetterメソッド
	 * 機能概要 : new_passwordを取得する。
	 * @return new_password 新パスワード
	 */
	public String getNew_password() {
		return new_password;
	}

	/**
	 * メソッド名 : new_passwordのSetterメソッド
	 * 機能概要 : new_passwordをセットする。
	 * @param new_password 新パスワード
	 */
	public void setNew_password(String new_password) {
		this.new_password = new_password;
	}

	/**
	 * メソッド名 : old_passwordのGetterメソッド
	 * 機能概要 : old_passwordを取得する。
	 * @return old_password 現パスワード
	 */
	public String getOld_password() {
		return old_password;
	}

	/**
	 * メソッド名 : old_passwordのSetterメソッド
	 * 機能概要 : old_passwordをセットする。
	 * @param oldPassword 現パスワード
	 */
	public void setOld_password(String oldPassword) {
		this.old_password = oldPassword;
	}

	/**
	 * メソッド名 : expiration_dateのGetterメソッド
	 * 機能概要 : expiration_dateを取得する。
	 * @return expiration_date 保存期間
	 */
	public Timestamp getExpiration_date() {
		return expiration_date;
	}

	/**
	 * メソッド名 : expiration_dateのSetterメソッド
	 * 機能概要 : expiration_dateをセットする。
	 * @param expiration_date 保存期間
	 */
	public void setExpiration_date(Timestamp expiration_date) {
		this.expiration_date = expiration_date;
	}

}
