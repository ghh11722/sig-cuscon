/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : InitPasswordConfirmInput.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/03/04  tinn.ra@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.password.dto;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;

/**
 * クラス名 : InitPasswordConfirmInput
 * 機能概要 : パスワード初期化確認処理入力定義
 * 備考 :
 * @author tinn.ra@JCCH
 * @version 1.0 tinn.ra@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class InitPasswordConfirmInput {
	// 初期化したいログインID
	private String passInitID;
	// UserValueObject
	private CusconUVO uvo = null;

	/**
	 * メソッド名 : passInitIDのGetterメソッド
	 * 機能概要 : passInitIDを取得します。
	 * @return passInitID 初期化したいログインID
	 */
	public String getPassInitID() {
		return passInitID;
	}

	/**
	 * メソッド名 : passInitIDのSetterメソッド
	 * 機能概要 : passInitIDを設定します。
	 * @param passInitID 初期化したいログインID
	 */
	public void setPassInitID(String passInitID) {
		this.passInitID = passInitID;
	}

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo UserValueObject
	 */
	public CusconUVO getUvo() {
		return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo UserValueObject
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}

}
