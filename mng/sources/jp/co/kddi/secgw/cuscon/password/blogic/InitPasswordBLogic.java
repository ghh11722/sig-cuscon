/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : InitPasswordBLogic.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/03/04  tinn.ra@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.password.blogic;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.password.dto.InitPasswordInput;
import jp.co.kddi.secgw.cuscon.password.dto.InitPasswordOutput;
import jp.co.kddi.secgw.cuscon.password.vo.SelectAccountList;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : InitPasswordBLogic
 * 機能概要 : パスワード初期化処理
 * 備考 :
 * @author tinn.ra@JCCH
 * @version 1.0 tinn.ra@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class InitPasswordBLogic implements BLogic<InitPasswordInput> {
   // QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;
	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;
	// 確認画面のボタン
	private static final String CONFIRM_CANCEL = "Cancel";
	private static final String CONFIRM_OK = "OK";
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(InitPasswordBLogic.class);

	/**
	 * メソッド名 : パスワード初期化処理
	 * 機能概要 : パスワードの初期化を行う
	 * @param param パスワード初期化処理入力を保持したMap
	 * @return パスワード初期化処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(InitPasswordInput param) {

		log.debug("パスワード初期化処理");

		// BLogicResultの生成、設定
		BLogicResult result = new BLogicResult();
		result.setResultString("failure");
		InitPasswordOutput output = new InitPasswordOutput();
		output.setPassInitID(null);
		result.setMessages(createMessage("DK120008"));//上記のお客様のパスワード初期化が失敗しました。

		// 1) リクエスト解析
		// 必須項目がない場合、セッションデータを破棄して終了
		String confirm = param.getConfirm();
		if (confirm == null) {
			log.error(messageAccessor.getMessage( "EK121005", null, param.getUvo()));
			result.setResultObject(output);
			result.setErrors(createMessage("DK120004"));
			return result;
		}
		// Cancelボタンが押下された場合、セッションデータを破棄して終了
		if (confirm.equals(CONFIRM_CANCEL)) {
			result.setResultObject(output);
			result.setResultString("cancel");
			return result;
		}
		// OK/Cancelボタン以外の場合
		if (!confirm.equals(CONFIRM_OK)) {
			log.error(messageAccessor.getMessage("EK121005", null, param.getUvo()));
			result.setResultObject(output);
			result.setErrors(createMessage("DK120004"));
			return result;
		}

		// 2) セッションデータ読取り→パスワード初期化対象ログインIDを取得する
		String passInitID = param.getPassInitID();
		if (passInitID == null || passInitID.isEmpty()) {
			log.error(messageAccessor.getMessage("EK121004", null, param.getUvo()));
			result.setResultObject(output);
			result.setErrors(createMessage("DK120004"));
			return result;
		}
		SelectAccountList targetAccount = param.getTarget_account();
		output.setTarget_account(targetAccount);

		// 3) パスワード初期化
		try {
			// ※初期化前に排他処理を行う。
			String loginId = queryDAO.executeForObject("InitPasswordBLogic-1", passInitID, java.lang.String.class);
			if(loginId == null || loginId.isEmpty()){
				Object[] objectgArray = {passInitID};
				log.error(messageAccessor.getMessage("EK121006", objectgArray, param.getUvo()));
				result.setResultObject(output);
				result.setErrors(createMessage("DK120006"));
				return result;
			} else {
				updateDAO.execute("InitPasswordBLogic-2", passInitID);
			}
		} catch (Exception e) {
			// ・不備があった場合は、エラーを出力する。
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK121007", objectgArray, param.getUvo()));
			result.setResultObject(output);
			result.setErrors(createMessage("DK120005"));
			return result;
		}

		// 4) レスポンス返却
		result.setResultObject(output);
		result.setMessages(createMessage("DK120007"));//上記のお客様のパスワード初期化が完了しました。
		result.setResultString("success");
		return result;
	}

    /**
	 * メソッド名 : queryDAOのGetterメソッド
	 * 機能概要 : queryDAOを取得する。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : updateDAOのGetterメソッド
	 * 機能概要 : updateDAOを取得する。
	 * @return updateDAO DAO
	 */
	public UpdateDAO getUpdateDAO() {
		return updateDAO;
	}

	/**
	 * メソッド名 : updateDAOのSetterメソッド
	 * 機能概要 : updateDAOをセットする。
	 * @param updateDAO DAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		this.updateDAO = updateDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}

	/**
	 * メソッド名 : createMessage
	 * 機能概要 : 各種メッセージを作成する。
	 * @param msg メッセージコード
	 * @return messages メッセージ
	 */
	public BLogicMessages createMessage(String msg) {
		BLogicMessages messages = new BLogicMessages();
		messages.add("message", new BLogicMessage(msg));
		return messages;
	}

}
