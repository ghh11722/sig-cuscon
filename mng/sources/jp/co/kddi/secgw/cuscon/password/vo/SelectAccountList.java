/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectAccountList.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/03/04  m.narikawa@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.password.vo;

import java.io.Serializable;

/**
 * クラス名 : SelectAccountList
 * 機能概要 : 企業管理者検索用データ
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class SelectAccountList implements Serializable {
	//
	private static final long serialVersionUID = -3119321340276070524L;
	// Firewall-ID
	private String firewallId = null;
	// ログインID
	private String loginId = null;
	// vSys-ID
	private String vsysId = null;
	// 最終ログイン時刻
	private String last_login_date;
	// status
	private String status = "";

	/**
	 * メソッド名 : statusのGetterメソッド
	 * 機能概要 : statusを取得する。
	 * @return status status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * メソッド名 : statusのSetterメソッド
	 * 機能概要 : statusをセットする。
	 * @param status status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * メソッド名 : firewallIdのSetterメソッド
	 * 機能概要 : firewallIdを取得します。
	 * @return firewallId Firewall-ID
	 */
	public String getFirewallId() {
		return firewallId;
	}

	/**
	 * メソッド名 : firewallIdのSetterメソッド
	 * 機能概要 : firewallIdを設定します。
	 * @param firewallId Firewall-ID
	 */
	public void setFirewallId(String firewallId) {
		this.firewallId = firewallId;
	}

	/**
	 * メソッド名 : loginIdのSetterメソッド
	 * 機能概要 : loginIdを取得します。
	 * @return loginId ログインID
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * メソッド名 : loginIdのSetterメソッド
	 * 機能概要 : loginIdを設定します。
	 * @param loginId ログインID
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdを取得します。
	 * @return vsysId vSys-ID
	 */
	public String getVsysId() {
		return vsysId;
	}

	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdを設定します。
	 * @param vsysId vSys-ID
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}

	/**
	 * メソッド名 : last_login_dateのSetterメソッド
	 * 機能概要 : last_login_dateを取得します。
	 * @return last_login_date 最終ログイン時刻
	 */
	public String getLast_login_date() {
		return last_login_date;
	}

	/**
	 * メソッド名 : last_login_dateのSetterメソッド
	 * 機能概要 : last_login_dateを設定します。
	 * @param last_login_date 最終ログイン時刻
	 */
	public void setLast_login_date(String last_login_date) {
		this.last_login_date = last_login_date;
	}

}
