/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名 : PasswordChangeForm.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/03/04  m.narikawa@JCCH  初版作成
 * 2010/03/18  tinn.ra@JCCH  更新
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.password.form;

import jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx;

/**
 * クラス名 : PasswordChangeForm
 * 機能概要 : パスワード変更処理アクションフォーム
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/04
 *          新規作成
 * @version 1.1 tinn.ra@JCCH
 *          Created 2010/03/18
 *          成功時のポップアップ表示処理を追加
 * @see jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx
 */
public class PasswordChangeForm extends ValidatorActionFormEx {

	//
	private static final long serialVersionUID = 1L;
	// 現パスワード
	private String old_password;
	// 新パスワード
	private String new_password;
	// 新パスワード(確認)
	private String password_check;
	// Firewall-ID
	private String firewallId = null;
	// ログインID
	private String loginId = null;
	// メッセージ
	private String resultmsg = null;



	/**
	 * メソッド名 : resultmsgのGetterメソッド
	 * 機能概要 : resultmsgを取得する。
	 * @return resultmsg
	 */
	public String getResultmsg() {
		return resultmsg;
	}

	/**
	 * メソッド名 : resultmsgのSetterメソッド
	 * 機能概要 : resultmsgをセットする。
	 * @param resultmsg
	 */
	public void setResultmsg(String resultmsg) {
		this.resultmsg = resultmsg;
	}

	/**
	 * メソッド名 : old_passwordのGetterメソッド
	 * 機能概要 : old_passwordを取得する。
	 * @return old_password 現パスワード
	 */
	public String getOld_password() {
		return old_password;
	}

	/**
	 * メソッド名 : old_passwordのSetterメソッド
	 * 機能概要 : old_passwordをセットする。
	 * @param oldPassword 現パスワード
	 */
	public void setOld_password(String oldPassword) {
		old_password = oldPassword;
	}

	/**
	 * メソッド名 : new_passwordのGetterメソッド
	 * 機能概要 : new_passwordを取得する。
	 * @return new_password 新パスワード
	 */
	public String getNew_password() {
		return new_password;
	}

	/**
	 * メソッド名 : new_passwordのSetterメソッド
	 * 機能概要 : new_passwordをセットする。
	 * @param new_password 新パスワード
	 */
	public void setNew_password(String new_password) {
		this.new_password = new_password;
	}

	/**
	 * メソッド名 : password_checkのGetterメソッド
	 * 機能概要 : password_checkを取得する。
	 * @return password_check 新パスワード(確認)
	 */
	public String getPassword_check() {
		return password_check;
	}

	/**
	 * メソッド名 : password_checkのSetterメソッド
	 * 機能概要 : password_checkをセットする。
	 * @param passwordCheck 新パスワード(確認)
	 */
	public void setPassword_check(String passwordCheck) {
		password_check = passwordCheck;
	}

	/**
	 * メソッド名 : firewallIdのSetterメソッド
	 * 機能概要 : firewallIdを取得します。
	 * @return firewallId Firewall-ID
	 */
	public String getFirewallId() {
		return firewallId;
	}

	/**
	 * メソッド名 : firewallIdのSetterメソッド
	 * 機能概要 : firewallIdを設定します。
	 * @param firewallId Firewall-ID
	 */
	public void setFirewallId(String firewallId) {
		this.firewallId = firewallId;
	}

	/**
	 * メソッド名 : loginIdのSetterメソッド
	 * 機能概要 : loginIdを取得します。
	 * @return loginId ログインID
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * メソッド名 : loginIdのSetterメソッド
	 * 機能概要 : loginIdを設定します。
	 * @param loginId ログインID
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

}
