/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : PasswordChangeInput.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/03/04  tinn.ra@JCCH  初版作成
 * 2010/03/18  tinn.ra@JCCH  更新
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.password.dto;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;

/**
 * クラス名 : PasswordChangeInput
 * 機能概要 : パスワード変更処理入力定義
 * 備考 :
 * @author tinn.ra@JCCH
 * @version 1.0 tinn.ra@JCCH
 *          Created 2010/03/04
 *          新規作成
 * @version 1.1 tinn.ra@JCCH
 *          Created 2010/03/18
 *          成功時のポップアップ表示処理を追加
 */
public class PasswordChangeInput {
	// 現パスワード
	private String old_password;
	// 新パスワード
	private String new_password;
	// 新パスワード(確認)
	private String password_check;
	// UserValueObject
	private CusconUVO uvo = null;
	// メッセージ
	private String resultmsg = null;



	/**
	 * メソッド名 : resultmsgのGetterメソッド
	 * 機能概要 : resultmsgを取得する。
	 * @return resultmsg
	 */
	public String getResultmsg() {
		return resultmsg;
	}

	/**
	 * メソッド名 : resultmsgのSetterメソッド
	 * 機能概要 : resultmsgをセットする。
	 * @param resultmsg
	 */
	public void setResultmsg(String resultmsg) {
		this.resultmsg = resultmsg;
	}

	/**
	 * メソッド名 : old_passwordのGetterメソッド
	 * 機能概要 : old_passwordを取得する。
	 * @return old_password 現パスワード
	 */
	public String getOld_password() {
		return old_password;
	}

	/**
	 * メソッド名 : old_passwordのSetterメソッド
	 * 機能概要 : old_passwordをセットする。
	 * @param oldPassword 現パスワード
	 */
	public void setOld_password(String oldPassword) {
		old_password = oldPassword;
	}

	/**
	 * メソッド名 : new_passwordのGetterメソッド
	 * 機能概要 : new_passwordを取得する。
	 * @return new_password 新パスワード
	 */
	public String getNew_password() {
		return new_password;
	}

	/**
	 * メソッド名 : new_passwordのSetterメソッド
	 * 機能概要 : new_passwordをセットする。
	 * @param new_password 新パスワード
	 */
	public void setNew_password(String new_password) {
		this.new_password = new_password;
	}

	/**
	 * メソッド名 : password_checkのGetterメソッド
	 * 機能概要 : password_checkを取得する。
	 * @return password_check 新パスワード(確認)
	 */
	public String getPassword_check() {
		return password_check;
	}

	/**
	 * メソッド名 : password_checkのSetterメソッド
	 * 機能概要 : password_checkをセットする。
	 * @param passwordCheck 新パスワード(確認)
	 */
	public void setPassword_check(String passwordCheck) {
		password_check = passwordCheck;
	}

    /**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを返却する。
     * @return uvo UserValueObject
     */
    public CusconUVO getUvo() {
        return uvo;
    }

    /**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoを設定する。
     * @param uvo UserValueObject
     */
    public void setUvo(CusconUVO uvo) {
        this.uvo = uvo;
    }

}
