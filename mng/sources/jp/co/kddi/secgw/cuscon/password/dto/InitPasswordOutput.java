/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : InitPasswordOutput.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/03/04  tinn.ra@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.password.dto;

import jp.co.kddi.secgw.cuscon.password.vo.SelectAccountList;

/**
 * クラス名 : InitPasswordOutput
 * 機能概要 : パスワード初期化処理出力定義
 * 備考 :
 * @author tinn.ra@JCCH
 * @version 1.0 tinn.ra@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class InitPasswordOutput {
	// 処理結果メッセージ
	private String resultmsg;
	// アカウント情報
	private SelectAccountList target_account = null;
	// 初期化したいログインID
	private String passInitID;

	/**
	 * メソッド名 : resultmsgのGetterメソッド
	 * 機能概要 : resultmsgを取得します。
	 * @return resultmsg 処理結果メッセージ
	 */
	public String getResultmsg() {
		return resultmsg;
	}

	/**
	 * メソッド名 : resultmsgのSetterメソッド
	 * 機能概要 : resultmsgを設定します。
	 * @param resultmsg 処理結果メッセージ
	 */
	public void setResultmsg(String resultmsg) {
		this.resultmsg = resultmsg;
	}

	/**
	 * メソッド名 : accountsのGetterメソッド
	 * 機能概要 : accountsを取得します。
	 * @return accounts アカウント情報
	 */
	public SelectAccountList getTarget_account() {
	    return target_account;
	}

	/**
	 * メソッド名 : target_accountのSetterメソッド
	 * 機能概要 : target_accountを設定します。
	 * @param target_account アカウント情報
	 */
	public void setTarget_account(SelectAccountList target_account) {
	    this.target_account = target_account;
	}

	/**
	 * メソッド名 : passInitIDのGetterメソッド
	 * 機能概要 : passInitIDを取得します。
	 * @return passInitID 初期化したいログインID
	 */
	public String getPassInitID() {
		return passInitID;
	}

	/**
	 * メソッド名 : passInitIDのSetterメソッド
	 * 機能概要 : passInitIDを設定します。
	 * @param passInitID 初期化したいログインID
	 */
	public void setPassInitID(String passInitID) {
		this.passInitID = passInitID;
	}

}
