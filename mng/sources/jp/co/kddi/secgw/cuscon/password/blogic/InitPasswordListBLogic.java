/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : InitPasswordListBLogic.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/03/04  tinn.ra@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.password.blogic;

//import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.password.dto.InitPasswordListInput;
import jp.co.kddi.secgw.cuscon.password.dto.InitPasswordListOutput;
import jp.co.kddi.secgw.cuscon.password.vo.SelectAccountList;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : InitPasswordListBLogic
 * 機能概要 : パスワード初期化一覧取得処理
 * 備考 :
 * @author tinn.ra@JCCH
 * @version 1.0 tinn.ra@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class InitPasswordListBLogic implements BLogic<InitPasswordListInput> {
    // QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(InitPasswordListBLogic.class);

	/**
	 * メソッド名 : パスワード初期化一覧取得処理
	 * 機能概要 : パスワード初期化の一覧を表示する
	 * @param param パスワード初期化一覧処理入力を保持したMap
	 * @return パスワード初期化一覧取得処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(InitPasswordListInput param) {

		log.debug("パスワード初期化一覧取得処理");

		BLogicResult result = new BLogicResult();
		result.setResultString("failure");
		InitPasswordListOutput out = new InitPasswordListOutput();

		// 1) 企業管理者検索
		// 企業管理者テーブル,旧パスワードテーブルよりアカウント一覧情報を取得する
		List<SelectAccountList> accounts = null;
		try {
			accounts = queryDAO.executeForObjectList("InitPasswordListBLogic-1", null);
			if (accounts.size() == 0) {
				// ・検索Hitしない場合は、一覧のタイトルのみを表示する。
				result.setResultString("success");
				return result;
			}
		} catch (Exception e) {
			// ・不備があった場合は、エラーを出力する。
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : 企業管理者検索失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK121001", objectgArray, param.getUvo()));
			result.setErrors(createErrMessage("DK120001"));
			return result;
		}

		// 2)レスポンス返却
		// フレームワークによりJSPに出力するために、検索結果をビジネスロジックの出力クラスに設定する。
		out.setAccounts(accounts);
		// ビジネスロジックの出力クラスに結果を設定する
		result.setResultObject(out);
		result.setResultString("success");

		return result;
	}

	/**
	 * メソッド名 : createErrMessage
	 * 機能概要 : エラーメッセージを作成する。
	 * @param error エラーコード
	 * @return messages メッセージ
	 */
	public BLogicMessages createErrMessage(String error) {
		BLogicMessages messages = new BLogicMessages();
		messages.add("message", new BLogicMessage(error));
		return messages;
	}

	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * 機能概要 : queryDAOを取得する。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}


}
