/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : TopAppSummaryBlogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/23     takahsht                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.report.blogic;

import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.report.dto.ReportInput;
import jp.co.kddi.secgw.cuscon.report.dto.TopAppSummaryOutput;
import jp.co.kddi.secgw.cuscon.report.vo.SelectTopAppSummaryList;
import jp.co.kddi.secgw.cuscon.report.vo.SelectTargetDateList;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : TopAppSummaryBLogic
 * 機能概要 :
 * 備考 :
 * @author takahsht
 * @version 1.0 takahsht
 *          Created 2010/02/23
 *          新規作成
 * @see
 */
public class TopAppSummaryBLogic implements BLogic<ReportInput> {


	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;
	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.report.blogic.TopAppSummaryBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 : Appトップサマリレポートの一覧情報を取得し、取得した情報を呼び出し元に返却する。
	 * @param param
	 * @return result
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(ReportInput param) {
		log.debug("TopAppSummaryBLogic処理開始");
		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		// エラーメッセージ
        BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = param.getUvo();
		String vsysId = uvo.getVsysId();
		// レポート取得日付を取得する。
		String targetDate = param.getAppTargetDate();

		// 検索条件設定
		SelectTopAppSummaryList searchTopAppSummaryKey =
									new SelectTopAppSummaryList();

		// Vsys-ID
		searchTopAppSummaryKey.setVsysId(vsysId);

		TopAppSummaryOutput out = new TopAppSummaryOutput();

		try {
			// レポート対象日付取得
			List<SelectTargetDateList> targetDateList =
				queryDAO.executeForObjectList(
						"TopAppSummaryList-1", searchTopAppSummaryKey);

			if(targetDate == null) {
				log.debug("別の画面から遷移");

				// データが存在しない場合
				if(!targetDateList.isEmpty()) {
					log.debug("targetDate設定" + targetDateList.get(0).getTargetDate());
					targetDate = targetDateList.get(0).getTargetDate();

					searchTopAppSummaryKey.setTargetDate(targetDate);
				} else {

					// レポート情報なしログ出力
		        	log.debug("レポート情報なし");
		        	messages.add("message", new BLogicMessage("DK090003"));

					result.setErrors(messages);
					result.setResultString("failure");
					log.debug("TopAppSummaryBLogic処理終了1");
		        	return result;
				}
			} else {
				log.debug("取得日付変更:targetDate = " + targetDate);
				searchTopAppSummaryKey.setTargetDate(targetDate);
			}

			// Appトップサマリレポート一覧情報取得
			List<SelectTopAppSummaryList> topAppSummaryList =
				queryDAO.executeForObjectList(
						"TopAppSummaryList-2", searchTopAppSummaryKey);


			StringBuffer str = new StringBuffer();
			StringBuffer str2 = new StringBuffer();
			for(int i=0; i<topAppSummaryList.size(); i++) {
				log.debug("数字の項目にカンマ付加:" + i);

				str.append(topAppSummaryList.get(i).getBytes());
				for(int j=3; j<str.toString().length(); j=j+4) {
					log.debug("3桁毎に付加する bytes:" + j);
					str.insert(str.toString().length()-j, ",");
				}
				topAppSummaryList.get(i).setBytes(str.toString());

				str2.append(topAppSummaryList.get(i).getSessions());
				for(int j=3; j<str2.toString().length(); j=j+4) {
					log.debug("3桁毎に付加する sessions:" + j);
					str2.insert(str2.toString().length()-j, ",");
				}
				topAppSummaryList.get(i).setSessions(str2.toString());
				str = new StringBuffer();
				str2 = new StringBuffer();
			}

			out.setAppTargetDate(targetDate);
			out.setTargetDateList(targetDateList);
			out.setTopAppSummaryList(topAppSummaryList);

			// レスポンス返却
			result.setResultObject(out);
			result.setResultString("success");
			log.debug("TopAppSummaryBLogic処理終了3");
			return result;

		} catch(Exception e) {
			// トランザクションロールバック
        	TransactionUtil.setRollbackOnly();
        	// DBアクセスエラー
        	Object[] objectgArray = {e.getMessage()};
        	log.fatal(messageAccessor.getMessage(
        			"FK091001", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK090004"));
        	result.setErrors(messages);
        	result.setResultObject(out);
        	result.setResultString("failure");
        	log.debug("TopAppSummaryBLogic処理終了2");
        	return result;
		}
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
