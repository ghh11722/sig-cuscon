/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : TargetDate.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/23     takahsht                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.report.vo;

/**
 * クラス名 : TargetDate
 * 機能概要 : レポート対象日付オブジェクトデータクラス
 * 備考 :
 * @author takahsht
 * @version 1.0 takahsht
 *          Created 2010/02/23
 *          新規作成
 * @see
 */
public class SelectTargetDateList {

	// レポート対象日付
	private String targetDate = null;

	/**
	 * メソッド名 : targetDateのGetterメソッド
	 * 機能概要 : targetDateを取得する。
	 * @return targetDate
	 */
	public String getTargetDate() {
		return targetDate;
	}
	/**
	 * メソッド名 : targetDateのSetterメソッド
	 * 機能概要 : targetDateをセットする。
	 * @param targetDate
	 */
	public void setTargetDate(String targetDate) {
		this.targetDate = targetDate;
	}
}
