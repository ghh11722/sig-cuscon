/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : TopApplicationSummaryOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/24     takahsht                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.report.vo;

/**
 * クラス名 : TopApplicationSummaryOutput
 * 機能概要 :
 * 備考 :
 * @author takahsht
 * @version 1.0 takahsht
 *          Created 2010/02/24
 *          新規作成
 * @see
 */
public class SelectTopApplicationSummaryList {

	// risk-of-name
	private String rsikOfName = null;
	// name
	private String name = null;
	// nsess
	private String nsess = null;
	// nbytes
	private String nbytes = null;
	// nthreats
	private String nthreats = null;
	/**
	 * メソッド名 : rsikOfNameのSetterメソッド
	 * 機能概要 : rsikOfNameをセットする。
	 * @param rsikOfName
	 */
	public void setRsikOfName(String rsikOfName) {
		this.rsikOfName = rsikOfName;
	}
	/**
	 * メソッド名 : rsikOfNameのGetterメソッド
	 * 機能概要 : rsikOfNameを取得する。
	 * @return rsikOfName
	 */
	public String getRsikOfName() {
		return rsikOfName;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nsessのSetterメソッド
	 * 機能概要 : nsessをセットする。
	 * @param nsess
	 */
	public void setNsess(String nsess) {
		this.nsess = nsess;
	}
	/**
	 * メソッド名 : nsessのGetterメソッド
	 * 機能概要 : nsessを取得する。
	 * @return nsess
	 */
	public String getNsess() {
		return nsess;
	}
	/**
	 * メソッド名 : nbytesのSetterメソッド
	 * 機能概要 : nbytesをセットする。
	 * @param nbytes
	 */
	public void setNbytes(String nbytes) {
		this.nbytes = nbytes;
	}
	/**
	 * メソッド名 : nbytesのGetterメソッド
	 * 機能概要 : nbytesを取得する。
	 * @return nbytes
	 */
	public String getNbytes() {
		return nbytes;
	}
	/**
	 * メソッド名 : nthreatsのSetterメソッド
	 * 機能概要 : nthreatsをセットする。
	 * @param nthreats
	 */
	public void setNthreats(String nthreats) {
		this.nthreats = nthreats;
	}
	/**
	 * メソッド名 : nthreatsのGetterメソッド
	 * 機能概要 : nthreatsを取得する。
	 * @return nthreats
	 */
	public String getNthreats() {
		return nthreats;
	}

}
