/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : TopSrcSummaryOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/27     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.report.dto;

import java.util.List;
import jp.co.kddi.secgw.cuscon.report.vo.SelectTargetDateList;
import jp.co.kddi.secgw.cuscon.report.vo.SelectTopSrcSummaryList;

/**
 * クラス名 : TopSrcSummaryOutput
 * 機能概要 :
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/27
 *          新規作成
 * @see
 */
public class TopSrcSummaryOutput {

	// レポート対象日付リスト
	private List<SelectTargetDateList> targetDateList = null;
	// Appトップサマリレポートリスト
	private List<SelectTopSrcSummaryList> topSrcSummaryList = null;
	// レポート対象日付
	private String srcTargetDate = null;
	/**
	 * メソッド名 : targetDateListのGetterメソッド
	 * 機能概要 : targetDateListを取得する。
	 * @return targetDateList
	 */
	public List<SelectTargetDateList> getTargetDateList() {
		return targetDateList;
	}
	/**
	 * メソッド名 : targetDateListのSetterメソッド
	 * 機能概要 : targetDateListをセットする。
	 * @param targetDateList
	 */
	public void setTargetDateList(List<SelectTargetDateList> targetDateList) {
		this.targetDateList = targetDateList;
	}
	/**
	 * メソッド名 : topSrcSummaryListのGetterメソッド
	 * 機能概要 : topSrcSummaryListを取得する。
	 * @return topSrcSummaryList
	 */
	public List<SelectTopSrcSummaryList> getTopSrcSummaryList() {
		return topSrcSummaryList;
	}
	/**
	 * メソッド名 : topSrcSummaryListのSetterメソッド
	 * 機能概要 : topSrcSummaryListをセットする。
	 * @param topSrcSummaryList
	 */
	public void setTopSrcSummaryList(List<SelectTopSrcSummaryList> topSrcSummaryList) {
		this.topSrcSummaryList = topSrcSummaryList;
	}
	/**
	 * メソッド名 : srcTargetDateのGetterメソッド
	 * 機能概要 : srcTargetDateを取得する。
	 * @return srcTargetDate
	 */
	public String getSrcTargetDate() {
		return srcTargetDate;
	}
	/**
	 * メソッド名 : srcTargetDateのSetterメソッド
	 * 機能概要 : srcTargetDateをセットする。
	 * @param srcTargetDate
	 */
	public void setSrcTargetDate(String srcTargetDate) {
		this.srcTargetDate = srcTargetDate;
	}
}
