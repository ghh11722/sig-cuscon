/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : TopApplicationSummaryOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/27     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.report.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.report.vo.SelectTopApplicationSummaryList;

/**
 * クラス名 : TopApplicationSummaryOutput
 * 機能概要 :
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/27
 *          新規作成
 * @see
 */
public class TopApplicationSummaryOutput {
	// アプリケーショントップサマリレポートリスト
	private List<SelectTopApplicationSummaryList> topApplicationSummaryList = null;
	// レポート対象日付
	private String targetPeriod = null;
	/**
	 * メソッド名 : topApplicationSummaryListのGetterメソッド
	 * 機能概要 : topApplicationSummaryListを取得する。
	 * @return topApplicationSummaryList
	 */
	public List<SelectTopApplicationSummaryList> getTopApplicationSummaryList() {
		return topApplicationSummaryList;
	}
	/**
	 * メソッド名 : topApplicationSummaryListのSetterメソッド
	 * 機能概要 : topApplicationSummaryListをセットする。
	 * @param topApplicationSummaryList
	 */
	public void setTopApplicationSummaryList(List<SelectTopApplicationSummaryList> topApplicationSummaryList) {
		this.topApplicationSummaryList = topApplicationSummaryList;
	}
	/**
	 * メソッド名 : targetPeriodのGetterメソッド
	 * 機能概要 : targetPeriodを取得する。
	 * @return targetPeriod
	 */
	public String getTargetPeriod() {
		return targetPeriod;
	}
	/**
	 * メソッド名 : targetPeriodのSetterメソッド
	 * 機能概要 : targetPeriodをセットする。
	 * @param targetPeriod
	 */
	public void setTargetPeriod(String targetPeriod) {
		this.targetPeriod = targetPeriod;
	}


}
