/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : TopAppSummaryInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/23     takahsht                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.report.dto;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;

/**
 * クラス名 : TopAppSummaryInput
 * 機能概要 :
 * 備考 :
 * @author takahsht
 * @version 1.0 takahsht
 *          Created 2010/02/23
 *          新規作成
 * @see
 */
public class ReportInput {
	private String appTargetDate = null;
	private String attackTargetDate = null;
	private String dstTargetDate = null;
	private String dstContTargetDate = null;
	private String srcTargetDate = null;
	private String targetPeriod = null;
    private CusconUVO uvo = null;


	/**
	 * メソッド名 : targetPeriodのGetterメソッド
	 * 機能概要 : targetPeriodを取得する。
	 * @return targetPeriod
	 */
	public String getTargetPeriod() {
		return targetPeriod;
	}

	/**
	 * メソッド名 : targetPeriodのSetterメソッド
	 * 機能概要 : targetPeriodをセットする。
	 * @param targetPeriod
	 */
	public void setTargetPeriod(String targetPeriod) {
		this.targetPeriod = targetPeriod;
	}


	/**
	 * メソッド名 : appTargetDateのGetterメソッド
	 * 機能概要 : appTargetDateを取得する。
	 * @return appTargetDate
	 */
	public String getAppTargetDate() {
		return appTargetDate;
	}

	/**
	 * メソッド名 : appTargetDateのSetterメソッド
	 * 機能概要 : appTargetDateをセットする。
	 * @param appTargetDate
	 */
	public void setAppTargetDate(String appTargetDate) {
		this.appTargetDate = appTargetDate;
	}

	/**
	 * メソッド名 : attackTargetDateのGetterメソッド
	 * 機能概要 : attackTargetDateを取得する。
	 * @return attackTargetDate
	 */
	public String getAttackTargetDate() {
		return attackTargetDate;
	}

	/**
	 * メソッド名 : attackTargetDateのSetterメソッド
	 * 機能概要 : attackTargetDateをセットする。
	 * @param attackTargetDate
	 */
	public void setAttackTargetDate(String attackTargetDate) {
		this.attackTargetDate = attackTargetDate;
	}

	/**
	 * メソッド名 : dstTargetDateのGetterメソッド
	 * 機能概要 : dstTargetDateを取得する。
	 * @return dstTargetDate
	 */
	public String getDstTargetDate() {
		return dstTargetDate;
	}

	/**
	 * メソッド名 : dstTargetDateのSetterメソッド
	 * 機能概要 : dstTargetDateをセットする。
	 * @param dstTargetDate
	 */
	public void setDstTargetDate(String dstTargetDate) {
		this.dstTargetDate = dstTargetDate;
	}

	/**
	 * メソッド名 : dstContTargetDateのGetterメソッド
	 * 機能概要 : dstContTargetDateを取得する。
	 * @return dstContTargetDate
	 */
	public String getDstContTargetDate() {
		return dstContTargetDate;
	}

	/**
	 * メソッド名 : dstContTargetDateのSetterメソッド
	 * 機能概要 : dstContTargetDateをセットする。
	 * @param dstContTargetDate
	 */
	public void setDstContTargetDate(String dstContTargetDate) {
		this.dstContTargetDate = dstContTargetDate;
	}

	/**
	 * メソッド名 : srcTargetDateのGetterメソッド
	 * 機能概要 : srcTargetDateを取得する。
	 * @return srcTargetDate
	 */
	public String getSrcTargetDate() {
		return srcTargetDate;
	}

	/**
	 * メソッド名 : srcTargetDateのSetterメソッド
	 * 機能概要 : srcTargetDateをセットする。
	 * @param srcTargetDate
	 */
	public void setSrcTargetDate(String srcTargetDate) {
		this.srcTargetDate = srcTargetDate;
	}

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}
}
