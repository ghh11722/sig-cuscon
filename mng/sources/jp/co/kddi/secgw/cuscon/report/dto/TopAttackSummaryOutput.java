/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : TopAttackSummaryOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/27     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.report.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.report.vo.SelectTargetDateList;
import jp.co.kddi.secgw.cuscon.report.vo.SelectTopAttacksSummaryList;

/**
 * クラス名 : TopAttackSummaryOutput
 * 機能概要 :
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/27
 *          新規作成
 * @see
 */
public class TopAttackSummaryOutput {

	// レポート対象日付リスト
	private List<SelectTargetDateList> targetDateList = null;
	// Appトップサマリレポートリスト
	private List<SelectTopAttacksSummaryList> topAttackSummaryList = null;
	// レポート対象日付
	private String attackTargetDate = null;
	/**
	 * メソッド名 : targetDateListのGetterメソッド
	 * 機能概要 : targetDateListを取得する。
	 * @return targetDateList
	 */
	public List<SelectTargetDateList> getTargetDateList() {
		return targetDateList;
	}
	/**
	 * メソッド名 : targetDateListのSetterメソッド
	 * 機能概要 : targetDateListをセットする。
	 * @param targetDateList
	 */
	public void setTargetDateList(List<SelectTargetDateList> targetDateList) {
		this.targetDateList = targetDateList;
	}
	/**
	 * メソッド名 : topAttackSummaryListのGetterメソッド
	 * 機能概要 : topAttackSummaryListを取得する。
	 * @return topAttackSummaryList
	 */
	public List<SelectTopAttacksSummaryList> getTopAttackSummaryList() {
		return topAttackSummaryList;
	}
	/**
	 * メソッド名 : topAttackSummaryListのSetterメソッド
	 * 機能概要 : topAttackSummaryListをセットする。
	 * @param topAttackSummaryList
	 */
	public void setTopAttackSummaryList(List<SelectTopAttacksSummaryList> topAttackSummaryList) {
		this.topAttackSummaryList = topAttackSummaryList;
	}
	/**
	 * メソッド名 : attackTargetDateのGetterメソッド
	 * 機能概要 : attackTargetDateを取得する。
	 * @return attackTargetDate
	 */
	public String getAttackTargetDate() {
		return attackTargetDate;
	}
	/**
	 * メソッド名 : attackTargetDateのSetterメソッド
	 * 機能概要 : attackTargetDateをセットする。
	 * @param attackTargetDate
	 */
	public void setAttackTargetDate(String attackTargetDate) {
		this.attackTargetDate = attackTargetDate;
	}

}
