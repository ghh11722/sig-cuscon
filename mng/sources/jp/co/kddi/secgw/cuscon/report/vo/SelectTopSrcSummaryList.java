/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : TopSrcSummary.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/25     takahsht                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.report.vo;

/**
 * クラス名 : TopSrcSummary
 * 機能概要 :
 * 備考 :
 * @author takahsht
 * @version 1.0 takahsht
 *          Created 2010/02/25
 *          新規作成
 * @see
 */
public class SelectTopSrcSummaryList {

	// シーケンス番号
	private int seqNo = 0;
	// Vsys-ID
	private String vsysId = null;
	// レポート対象日付
	private String targetDate = null;
	// Src
	private String src = null;
	// ResolvedSrc
	private String resolvedSrc = null;
	// SrcUser
	private String srcUser = null;
	// Bytes
	private String bytes = null;
	// Sessions
	private String sessions = null;
	/**
	 * メソッド名 : seqNoのSetterメソッド
	 * 機能概要 : seqNoをセットする。
	 * @param seqNo
	 */
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * メソッド名 : seqNoのGetterメソッド
	 * 機能概要 : seqNoを取得する。
	 * @return seqNo
	 */
	public int getSeqNo() {
		return seqNo;
	}
	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdをセットする。
	 * @param vsysId
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}
	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得する。
	 * @return vsysId
	 */
	public String getVsysId() {
		return vsysId;
	}
	/**
	 * メソッド名 : targetDateのSetterメソッド
	 * 機能概要 : targetDateをセットする。
	 * @param targetDate
	 */
	public void setTargetDate(String targetDate) {
		this.targetDate = targetDate;
	}
	/**
	 * メソッド名 : targetDateのGetterメソッド
	 * 機能概要 : targetDateを取得する。
	 * @return targetDate
	 */
	public String getTargetDate() {
		return targetDate;
	}
	/**
	 * メソッド名 : srcのSetterメソッド
	 * 機能概要 : srcをセットする。
	 * @param src
	 */
	public void setSrc(String src) {
		this.src = src;
	}
	/**
	 * メソッド名 : srcのGetterメソッド
	 * 機能概要 : srcを取得する。
	 * @return src
	 */
	public String getSrc() {
		return src;
	}
	/**
	 * メソッド名 : resolvedSrcのSetterメソッド
	 * 機能概要 : resolvedSrcをセットする。
	 * @param resolvedSrc
	 */
	public void setResolvedSrc(String resolvedSrc) {
		this.resolvedSrc = resolvedSrc;
	}
	/**
	 * メソッド名 : resolvedSrcのGetterメソッド
	 * 機能概要 : resolvedSrcを取得する。
	 * @return resolvedSrc
	 */
	public String getResolvedSrc() {
		return resolvedSrc;
	}
	/**
	 * メソッド名 : srcUserのSetterメソッド
	 * 機能概要 : srcUserをセットする。
	 * @param srcUser
	 */
	public void setSrcUser(String srcUser) {
		this.srcUser = srcUser;
	}
	/**
	 * メソッド名 : srcUserのGetterメソッド
	 * 機能概要 : srcUserを取得する。
	 * @return srcUser
	 */
	public String getSrcUser() {
		return srcUser;
	}
	/**
	 * メソッド名 : bytesのSetterメソッド
	 * 機能概要 : bytesをセットする。
	 * @param bytes
	 */
	public void setBytes(String bytes) {
		this.bytes = bytes;
	}
	/**
	 * メソッド名 : bytesのGetterメソッド
	 * 機能概要 : bytesを取得する。
	 * @return bytes
	 */
	public String getBytes() {
		return bytes;
	}
	/**
	 * メソッド名 : sessionsのSetterメソッド
	 * 機能概要 : sessionsをセットする。
	 * @param sessions
	 */
	public void setSessions(String sessions) {
		this.sessions = sessions;
	}
	/**
	 * メソッド名 : sessionsのGetterメソッド
	 * 機能概要 : sessionsを取得する。
	 * @return sessions
	 */
	public String getSessions() {
		return sessions;
	}
}
