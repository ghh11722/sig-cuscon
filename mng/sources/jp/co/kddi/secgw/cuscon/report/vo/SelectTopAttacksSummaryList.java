/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : TopAttacksSummary.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/24     takahsht                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.report.vo;

/**
 * クラス名 : TopAttacksSummary
 * 機能概要 : 攻撃トップサマリレポートオブジェクトデータクラス
 * 備考 :
 * @author takahsht
 * @version 1.0 takahsht
 *          Created 2010/02/24
 *          新規作成
 * @see
 */
public class SelectTopAttacksSummaryList {

	// シーケンス番号
	private int seqNo = 0;
	// Vsys-ID
	private String vsysId = null;
	// レポート対象日付
	private String targetDate = null;
	// Tid
	private String tid = null;
	// SeverityOfThreatid
	private String severityOfThreatid = null;
	// Threatid
	private String threatid = null;
	// ThreatSubType
	private String threatSubType = null;
	// Count
	private String count = null;
	/**
	 * メソッド名 : seqNoのSetterメソッド
	 * 機能概要 : seqNoをセットする。
	 * @param seqNo
	 */
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * メソッド名 : seqNoのGetterメソッド
	 * 機能概要 : seqNoを取得する。
	 * @return seqNo
	 */
	public int getSeqNo() {
		return seqNo;
	}
	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdをセットする。
	 * @param vsysId
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}
	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得する。
	 * @return vsysId
	 */
	public String getVsysId() {
		return vsysId;
	}
	/**
	 * メソッド名 : targetDateのSetterメソッド
	 * 機能概要 : targetDateをセットする。
	 * @param targetDate
	 */
	public void setTargetDate(String targetDate) {
		this.targetDate = targetDate;
	}
	/**
	 * メソッド名 : targetDateのGetterメソッド
	 * 機能概要 : targetDateを取得する。
	 * @return targetDate
	 */
	public String getTargetDate() {
		return targetDate;
	}
	/**
	 * メソッド名 : tidのSetterメソッド
	 * 機能概要 : tidをセットする。
	 * @param tid
	 */
	public void setTid(String tid) {
		this.tid = tid;
	}
	/**
	 * メソッド名 : tidのGetterメソッド
	 * 機能概要 : tidを取得する。
	 * @return tid
	 */
	public String getTid() {
		return tid;
	}
	/**
	 * メソッド名 : severityOfThreatidのSetterメソッド
	 * 機能概要 : severityOfThreatidをセットする。
	 * @param severityOfThreatid
	 */
	public void setSeverityOfThreatid(String severityOfThreatid) {
		this.severityOfThreatid = severityOfThreatid;
	}
	/**
	 * メソッド名 : severityOfThreatidのGetterメソッド
	 * 機能概要 : severityOfThreatidを取得する。
	 * @return severityOfThreatid
	 */
	public String getSeverityOfThreatid() {
		return severityOfThreatid;
	}
	/**
	 * メソッド名 : threatidのSetterメソッド
	 * 機能概要 : threatidをセットする。
	 * @param threatid
	 */
	public void setThreatid(String threatid) {
		this.threatid = threatid;
	}
	/**
	 * メソッド名 : threatidのGetterメソッド
	 * 機能概要 : threatidを取得する。
	 * @return threatid
	 */
	public String getThreatid() {
		return threatid;
	}
	/**
	 * メソッド名 : threatSubTypeのSetterメソッド
	 * 機能概要 : threatSubTypeをセットする。
	 * @param threatSubType
	 */
	public void setThreatSubType(String threatSubType) {
		this.threatSubType = threatSubType;
	}
	/**
	 * メソッド名 : threatSubTypeのGetterメソッド
	 * 機能概要 : threatSubTypeを取得する。
	 * @return threatSubType
	 */
	public String getThreatSubType() {
		return threatSubType;
	}
	/**
	 * メソッド名 : countのSetterメソッド
	 * 機能概要 : countをセットする。
	 * @param count
	 */
	public void setCount(String count) {
		this.count = count;
	}
	/**
	 * メソッド名 : countのGetterメソッド
	 * 機能概要 : countを取得する。
	 * @return count
	 */
	public String getCount() {
		return count;
	}

}
