/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : TopDstSummaryOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/27     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.report.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.report.vo.SelectTargetDateList;
import jp.co.kddi.secgw.cuscon.report.vo.SelectTopDstSummaryList;

/**
 * クラス名 : TopDstSummaryOutput
 * 機能概要 :
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/27
 *          新規作成
 * @see
 */
public class TopDstSummaryOutput {

	// レポート対象日付リスト
	private List<SelectTargetDateList> targetDateList = null;
	// Appトップサマリレポートリスト
	private List<SelectTopDstSummaryList> topDstSummaryList = null;
	// レポート対象日付
	private String dstTargetDate = null;
	/**
	 * メソッド名 : targetDateListのGetterメソッド
	 * 機能概要 : targetDateListを取得する。
	 * @return targetDateList
	 */
	public List<SelectTargetDateList> getTargetDateList() {
		return targetDateList;
	}
	/**
	 * メソッド名 : targetDateListのSetterメソッド
	 * 機能概要 : targetDateListをセットする。
	 * @param targetDateList
	 */
	public void setTargetDateList(List<SelectTargetDateList> targetDateList) {
		this.targetDateList = targetDateList;
	}
	/**
	 * メソッド名 : topDstSummaryListのGetterメソッド
	 * 機能概要 : topDstSummaryListを取得する。
	 * @return topDstSummaryList
	 */
	public List<SelectTopDstSummaryList> getTopDstSummaryList() {
		return topDstSummaryList;
	}
	/**
	 * メソッド名 : topDstSummaryListのSetterメソッド
	 * 機能概要 : topDstSummaryListをセットする。
	 * @param topDstSummaryList
	 */
	public void setTopDstSummaryList(List<SelectTopDstSummaryList> topDstSummaryList) {
		this.topDstSummaryList = topDstSummaryList;
	}
	/**
	 * メソッド名 : dstTargetDateのGetterメソッド
	 * 機能概要 : dstTargetDateを取得する。
	 * @return dstTargetDate
	 */
	public String getDstTargetDate() {
		return dstTargetDate;
	}
	/**
	 * メソッド名 : dstTargetDateのSetterメソッド
	 * 機能概要 : dstTargetDateをセットする。
	 * @param dstTargetDate
	 */
	public void setDstTargetDate(String dstTargetDate) {
		this.dstTargetDate = dstTargetDate;
	}

}
