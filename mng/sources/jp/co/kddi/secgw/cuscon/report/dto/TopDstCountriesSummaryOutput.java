/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : TopDstCountriesSummaryOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/27     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.report.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.report.vo.SelectTargetDateList;
import jp.co.kddi.secgw.cuscon.report.vo.SelectTopDstCountriesSummaryList;

/**
 * クラス名 : TopDstCountriesSummaryOutput
 * 機能概要 :
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/27
 *          新規作成
 * @see
 */
public class TopDstCountriesSummaryOutput {


	// レポート対象日付リスト
	private List<SelectTargetDateList> targetDateList = null;
	// Appトップサマリレポートリスト
	private List<SelectTopDstCountriesSummaryList> topDstCountriesSummaryList = null;
	// レポート対象日付
	private String dstContTargetDate = null;
	/**
	 * メソッド名 : targetDateListのGetterメソッド
	 * 機能概要 : targetDateListを取得する。
	 * @return targetDateList
	 */
	public List<SelectTargetDateList> getTargetDateList() {
		return targetDateList;
	}
	/**
	 * メソッド名 : targetDateListのSetterメソッド
	 * 機能概要 : targetDateListをセットする。
	 * @param targetDateList
	 */
	public void setTargetDateList(List<SelectTargetDateList> targetDateList) {
		this.targetDateList = targetDateList;
	}
	/**
	 * メソッド名 : topDstCountriesSummaryListのGetterメソッド
	 * 機能概要 : topDstCountriesSummaryListを取得する。
	 * @return topDstCountriesSummaryList
	 */
	public List<SelectTopDstCountriesSummaryList> getTopDstCountriesSummaryList() {
		return topDstCountriesSummaryList;
	}
	/**
	 * メソッド名 : topDstCountriesSummaryListのSetterメソッド
	 * 機能概要 : topDstCountriesSummaryListをセットする。
	 * @param topDstCountriesSummaryList
	 */
	public void setTopDstCountriesSummaryList(List<SelectTopDstCountriesSummaryList> topDstCountriesSummaryList) {
		this.topDstCountriesSummaryList = topDstCountriesSummaryList;
	}
	/**
	 * メソッド名 : dstContTargetDateのGetterメソッド
	 * 機能概要 : dstContTargetDateを取得する。
	 * @return dstContTargetDate
	 */
	public String getDstContTargetDate() {
		return dstContTargetDate;
	}
	/**
	 * メソッド名 : dstContTargetDateのSetterメソッド
	 * 機能概要 : dstContTargetDateをセットする。
	 * @param dstContTargetDate
	 */
	public void setDstContTargetDate(String dstContTargetDate) {
		this.dstContTargetDate = dstContTargetDate;
	}
}
