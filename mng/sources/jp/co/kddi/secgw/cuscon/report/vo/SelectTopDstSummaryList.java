/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : TopDstSummary.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/24     takahsht                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.report.vo;

/**
 * クラス名 : TopDstSummary
 * 機能概要 : 宛先トップサマリレポートオブジェクトデータクラス
 * 備考 :
 * @author takahsht
 * @version 1.0 takahsht
 *          Created 2010/02/24
 *          新規作成
 * @see
 */
public class SelectTopDstSummaryList {

	// シーケンス番号
	private int seqNo = 0;
	// Vsys-ID
	private String vsysId = null;
	// レポート対象日付
	private String targetDate = null;
	// Dst
	private String dst = null;
	// ResolvedDst
	private String resolvedDst = null;
	// DstUser
	private String dstUser = null;
	// Bytes
	private String bytes = null;
	// Sessions
	private String sessions = null;
	/**
	 * メソッド名 : seqNoのSetterメソッド
	 * 機能概要 : seqNoをセットする。
	 * @param seqNo
	 */
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * メソッド名 : seqNoのGetterメソッド
	 * 機能概要 : seqNoを取得する。
	 * @return seqNo
	 */
	public int getSeqNo() {
		return seqNo;
	}
	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdをセットする。
	 * @param vsysId
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}
	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得する。
	 * @return vsysId
	 */
	public String getVsysId() {
		return vsysId;
	}
	/**
	 * メソッド名 : targetDateのSetterメソッド
	 * 機能概要 : targetDateをセットする。
	 * @param targetDate
	 */
	public void setTargetDate(String targetDate) {
		this.targetDate = targetDate;
	}
	/**
	 * メソッド名 : targetDateのGetterメソッド
	 * 機能概要 : targetDateを取得する。
	 * @return targetDate
	 */
	public String getTargetDate() {
		return targetDate;
	}
	/**
	 * メソッド名 : dstのSetterメソッド
	 * 機能概要 : dstをセットする。
	 * @param dst
	 */
	public void setDst(String dst) {
		this.dst = dst;
	}
	/**
	 * メソッド名 : dstのGetterメソッド
	 * 機能概要 : dstを取得する。
	 * @return dst
	 */
	public String getDst() {
		return dst;
	}
	/**
	 * メソッド名 : resolvedDstのSetterメソッド
	 * 機能概要 : resolvedDstをセットする。
	 * @param resolvedDst
	 */
	public void setResolvedDst(String resolvedDst) {
		this.resolvedDst = resolvedDst;
	}
	/**
	 * メソッド名 : resolvedDstのGetterメソッド
	 * 機能概要 : resolvedDstを取得する。
	 * @return resolvedDst
	 */
	public String getResolvedDst() {
		return resolvedDst;
	}
	/**
	 * メソッド名 : dstUserのSetterメソッド
	 * 機能概要 : dstUserをセットする。
	 * @param dstUser
	 */
	public void setDstUser(String dstUser) {
		this.dstUser = dstUser;
	}
	/**
	 * メソッド名 : dstUserのGetterメソッド
	 * 機能概要 : dstUserを取得する。
	 * @return dstUser
	 */
	public String getDstUser() {
		return dstUser;
	}
	/**
	 * メソッド名 : bytesのSetterメソッド
	 * 機能概要 : bytesをセットする。
	 * @param bytes
	 */
	public void setBytes(String bytes) {
		this.bytes = bytes;
	}
	/**
	 * メソッド名 : bytesのGetterメソッド
	 * 機能概要 : bytesを取得する。
	 * @return bytes
	 */
	public String getBytes() {
		return bytes;
	}
	/**
	 * メソッド名 : sessionsのSetterメソッド
	 * 機能概要 : sessionsをセットする。
	 * @param sessions
	 */
	public void setSessions(String sessions) {
		this.sessions = sessions;
	}
	/**
	 * メソッド名 : sessionsのGetterメソッド
	 * 機能概要 : sessionsを取得する。
	 * @return sessions
	 */
	public String getSessions() {
		return sessions;
	}

}
