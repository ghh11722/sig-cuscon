/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : TopAplicationSummaryBlogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/25     takahsht                初版作成
 * 2012/12/01     kkato@PROSITE           PANOS4.1.x対応
 * 2016/12/21     T.Yamazaki@Plum Systems PANOS7対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.report.blogic;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import jp.co.kddi.secgw.cuscon.common.CusconAccessKeyCreate;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.CusconXMLAnalyze;
import jp.co.kddi.secgw.cuscon.common.CusconXmlApiExecute;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.report.dto.ReportInput;
import jp.co.kddi.secgw.cuscon.report.dto.TopApplicationSummaryOutput;
import jp.co.kddi.secgw.cuscon.report.vo.SelectTopApplicationSummaryList;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.util.PropertyUtil;

/**
 * クラス名 : TopAplicationSummaryBlogic
 * 機能概要 :
 * 備考 :
 * @author takahsht
 * @version 1.0 takahsht
 *          Created 2010/02/25
 *          新規作成
 *          2.0 T.Yamazaki@Plum Systems Inc.
 *          Updated 2016/12/21
 *          PANOS7対応
 * @see
 */
public class TopApplicationSummaryBLogic implements BLogic<ReportInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;
	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.report.blogic.TopApplicationSummaryBLogic");
	private MessageAccessor messageAccessor = null;
	/**
	 * メソッド名 : execute
	 * 機能概要 : アプリケーショントップサマリレポートの一覧情報を取得し、取得した情報を呼び出し元に返却する。
	 * @param param
	 * @return result
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(ReportInput param) {

		log.debug("TopApplicationSummaryBLogic処理開始");
		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		// エラーメッセージ
        BLogicMessages messages = new BLogicMessages();

		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = param.getUvo();
		String paLoginId = uvo.getPaLoginId();
		String paPasswd = uvo.getPaPasswd();

		// レポート取得期間を取得する。
		String targetPeriod = param.getTargetPeriod();

		TopApplicationSummaryOutput out = new TopApplicationSummaryOutput();

		// 一覧情報検索共通処理
		List<SelectTopApplicationSummaryList> output = null;
		try {
			output = getTopApplicationSummaryList(
					paLoginId, paPasswd, targetPeriod, uvo);
		} catch (Exception e) {
        	// 例外
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"EK091001", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK090002"));
        	result.setErrors(messages);
        	result.setResultString("failure");
        	return result;
		}

		// データが存在しない場合
		if(output.size() == 0) {
			// レポート情報なしログ出力
        	log.debug("レポート情報なし");
        	messages.add("message", new BLogicMessage("DK090001"));

			result.setErrors(messages);
			result.setResultString("failure");
			log.debug("TopApplicationSummaryBLogic処理終了1");
        	return result;
		}

		out.setTargetPeriod(targetPeriod);
		out.setTopApplicationSummaryList(output);
		// レスポンス返却
		result.setResultObject(out);
		result.setResultString("success");
		log.debug("TopApplicationSummaryBLogic処理終了2");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}

	private List<SelectTopApplicationSummaryList>
		getTopApplicationSummaryList(
				String paLoginId, String paPasswd,
					String targetPeriod, CusconUVO uvo) throws Exception {

		log.debug("getTopApplicationSummaryList処理開始");
		// アクセスキー生成
		CusconAccessKeyCreate cusconAccessKeyCreate =
					new CusconAccessKeyCreate();
		// 20121201 kkato@PROSITE mod start
		//String accessKey =
		//	cusconAccessKeyCreate.getAccessKey(paLoginId, paPasswd, queryDAO, uvo);
		String accessKey =
			cusconAccessKeyCreate.getAccessKey(null, null, queryDAO, uvo);
		// 20121201 kkato@PROSITE mod end

		// URL(ホスト名前)
		String URL1 =
	    	PropertyUtil.getProperty("common.xmlurl1");
		// URL(ホスト名後)
		String URL2 =
			PropertyUtil.getProperty("common.xmlurl2");
		// キー
		String KEY =
			PropertyUtil.getProperty("common.xmlkey");

		// ホスト名
		String host = cusconAccessKeyCreate.getHost();
		// URL結合文字
		String AND = "&";
		// 種別
		String TYPE =
			PropertyUtil.getProperty("report.type");
		// レポート種別
		String REPORTTYPE =
			PropertyUtil.getProperty("report.reporttype");
		// レポート名
		String REPORTNAME =
			PropertyUtil.getProperty("report.reportname");
		// 取得件数
		String TOPN =
			PropertyUtil.getProperty("report.topn");
		// 取得期間
		String PERIOD =
			PropertyUtil.getProperty("report.period");
		// アクセスキー
		String accessKeyCom = KEY + accessKey;
		// 取得期間初期値
		String firstPeriod = "last-hour";
		// 20121201 kkato@PROSITE add start
		// vsys
		String VSYS = "vsys=";
		// 20121201 kkato@PROSITE add end

		// UPDATED: 2016.11 by Plum Systems Inc.
		// PANOS7対応 -->
		String ASYNC = PropertyUtil.getProperty("report.async");
		// --> PANOS7対応

		// 解析タグ
		String nthreats = "nthreats";
		String nbytes = "nbytes";
		String name = "name";
		String riskOfName = "risk-of-name";
		String nsess = "nsess";

		if (log.isDebugEnabled()) {
			log.debug("URL(ホスト名前):" + URL1);
			log.debug("URL(ホスト名後)" + URL2);
			log.debug("種別:" + TYPE);
			log.debug("レポート種別:" + REPORTTYPE);
			log.debug("レポート名:" +  REPORTNAME);
			log.debug("取得件数:" + TOPN);
			log.debug("アクセスキー:" + accessKeyCom);
			log.debug("取得期間:" + PERIOD);
			log.debug("非同期フラグ:" + ASYNC);
		}

		String targetCom;
		// 取得対象期間
		if(targetPeriod == null) {
			log.debug("別の画面から遷移:targetPeriod = null");
			targetCom = PERIOD + firstPeriod;

		} else {
			log.debug("取得対象変更:targetPeriod = " + targetPeriod);
			targetCom = PERIOD + targetPeriod;

		}

		// XML-APIアクセスURL作成
		// 20121201 kkato@PROSITE mod start
		//String xmlApiCommand = URL1 + host + URL2 + TYPE +
		//	AND + REPORTTYPE + AND + REPORTNAME + AND + targetCom
		//								+ AND + TOPN + AND + accessKeyCom;
		//String xmlApiCommand = URL1 + host + URL2 + TYPE +
		//	AND + REPORTTYPE + AND + REPORTNAME + AND + targetCom
		//							+ AND + TOPN + AND + accessKeyCom + AND + VSYS + uvo.getVsysId();
		// 20121201 kkato@PROSITE mod end

		// UPDATED: 2016.11 by Plum Systems Inc.
		// PANOS7対応 -->
		String xmlApiCommand = URL1 + host + URL2 + TYPE +
			AND + ASYNC + AND + REPORTTYPE + AND + REPORTNAME + AND + targetCom +
			AND + TOPN + AND + accessKeyCom + AND + VSYS + uvo.getVsysId();
		// --> PANOS7対応

		log.debug(xmlApiCommand);


		// コマンド実行
		CusconXmlApiExecute xmlApiExecute = new CusconXmlApiExecute();
		InputStream commandRespons = xmlApiExecute.doProc(xmlApiCommand, uvo);

		// コマンド解析
		CusconXMLAnalyze xmlAnalyze = new CusconXMLAnalyze(messageAccessor);
		List<HashMap<String, Object>> analyzeResult =
								xmlAnalyze.analyzeXML(commandRespons, uvo);

		// 取得したTopApplicationSummaryの件数分、TopApplicationSummaryのリストに設定
		List <SelectTopApplicationSummaryList> topApplicationSummaryList =
							new ArrayList<SelectTopApplicationSummaryList>();
		StringBuffer str = new StringBuffer();
		StringBuffer str2 = new StringBuffer();
		StringBuffer str3 = new StringBuffer();
		for(int i = 1; i < analyzeResult.size(); i++) {
			// 20121201 kkato@PROSITE mod start
			if (analyzeResult.get(i).size() > 0) {
			// 20121201 kkato@PROSITE mod end
				log.debug("xml解析中:" + i);
				SelectTopApplicationSummaryList topApplicationSummaryOutput =
											new SelectTopApplicationSummaryList();

				str.append(analyzeResult.get(i).get(nthreats).toString());
				for(int j=3; j<str.toString().length(); j=j+4) {
					log.debug("3桁毎にカンマを付加する nthreats:" + j);
					str.insert(str.toString().length()-j, ",");
				}
				topApplicationSummaryOutput.setNthreats(str.toString());

				str2.append(analyzeResult.get(i).get(nbytes).toString());
				for(int j=3; j<str2.toString().length(); j=j+4) {
					log.debug("3桁毎にカンマを付加する nbytes:" + j);
					str2.insert(str2.toString().length()-j, ",");
				}
				topApplicationSummaryOutput.setNbytes(str2.toString());

				topApplicationSummaryOutput.setName(
							analyzeResult.get(i).get(name).toString());
				topApplicationSummaryOutput.setRsikOfName(
							analyzeResult.get(i).get(riskOfName).toString());

				str3.append(analyzeResult.get(i).get(nsess).toString());
				for(int j=3; j<str3.toString().length(); j=j+4) {
					log.debug("3桁毎にカンマを付加する nsess:" + j);
					str3.insert(str3.toString().length()-j, ",");
				}
				topApplicationSummaryOutput.setNsess(str3.toString());

				topApplicationSummaryList.add(topApplicationSummaryOutput);

				str = new StringBuffer();
				str2 = new StringBuffer();
				str3 = new StringBuffer();
			// 20121201 kkato@PROSITE mod start
			}
			// 20121201 kkato@PROSITE mod end
		}

		log.debug("getTopApplicationSummaryList処理終了");
		// アプリケーショントップサマリレポート一覧情報取得結果返却
		return topApplicationSummaryList;
	}


}
