/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : TopRuleSummary.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/25     takahsht                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.report.vo;

/**
 * クラス名 : TopRuleSummary
 * 機能概要 :
 * 備考 :
 * @author takahsht
 * @version 1.0 takahsht
 *          Created 2010/02/25
 *          新規作成
 * @see
 */
public class SelectTopRuleSummaryList {

	// シーケンス番号
	private int seqNo = 0;
	// Vsys-ID
	private String vsysId = null;
	// レポート対象日付
	private String targetDate = null;
	// Serial
	private String serial = null;
	// Rule
	private String rule = null;
	// Bytes
	private String bytes = null;
	// Sessions
	private String sessions = null;
	/**
	 * メソッド名 : seqNoのSetterメソッド
	 * 機能概要 : seqNoをセットする。
	 * @param seqNo
	 */
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * メソッド名 : seqNoのGetterメソッド
	 * 機能概要 : seqNoを取得する。
	 * @return seqNo
	 */
	public int getSeqNo() {
		return seqNo;
	}
	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdをセットする。
	 * @param vsysId
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}
	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得する。
	 * @return vsysId
	 */
	public String getVsysId() {
		return vsysId;
	}
	/**
	 * メソッド名 : targetDateのSetterメソッド
	 * 機能概要 : targetDateをセットする。
	 * @param targetDate
	 */
	public void setTargetDate(String targetDate) {
		this.targetDate = targetDate;
	}
	/**
	 * メソッド名 : targetDateのGetterメソッド
	 * 機能概要 : targetDateを取得する。
	 * @return targetDate
	 */
	public String getTargetDate() {
		return targetDate;
	}
	/**
	 * メソッド名 : serialのSetterメソッド
	 * 機能概要 : serialをセットする。
	 * @param serial
	 */
	public void setSerial(String serial) {
		this.serial = serial;
	}
	/**
	 * メソッド名 : serialのGetterメソッド
	 * 機能概要 : serialを取得する。
	 * @return serial
	 */
	public String getSerial() {
		return serial;
	}
	/**
	 * メソッド名 : ruleのSetterメソッド
	 * 機能概要 : ruleをセットする。
	 * @param rule
	 */
	public void setRule(String rule) {
		this.rule = rule;
	}
	/**
	 * メソッド名 : ruleのGetterメソッド
	 * 機能概要 : ruleを取得する。
	 * @return rule
	 */
	public String getRule() {
		return rule;
	}
	/**
	 * メソッド名 : bytesのSetterメソッド
	 * 機能概要 : bytesをセットする。
	 * @param bytes
	 */
	public void setBytes(String bytes) {
		this.bytes = bytes;
	}
	/**
	 * メソッド名 : bytesのGetterメソッド
	 * 機能概要 : bytesを取得する。
	 * @return bytes
	 */
	public String getBytes() {
		return bytes;
	}
	/**
	 * メソッド名 : sessionsのSetterメソッド
	 * 機能概要 : sessionsをセットする。
	 * @param sessions
	 */
	public void setSessions(String sessions) {
		this.sessions = sessions;
	}
	/**
	 * メソッド名 : sessionsのGetterメソッド
	 * 機能概要 : sessionsを取得する。
	 * @return sessions
	 */
	public String getSessions() {
		return sessions;
	}
}
