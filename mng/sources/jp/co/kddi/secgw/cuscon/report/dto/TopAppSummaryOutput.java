/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : TopAppSummaryOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/27     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.report.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.report.vo.SelectTopAppSummaryList;
import jp.co.kddi.secgw.cuscon.report.vo.SelectTargetDateList;

/**
 * クラス名 : TopAppSummaryOutput
 * 機能概要 :
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/27
 *          新規作成
 * @see
 */
public class TopAppSummaryOutput {

	// レポート対象日付リスト
	private List<SelectTargetDateList> targetDateList = null;
	// Appトップサマリレポートリスト
	private List<SelectTopAppSummaryList> topAppSummaryList = null;
	// レポート対象日付
	private String appTargetDate = null;
	/**
	 * メソッド名 : targetDateListのGetterメソッド
	 * 機能概要 : targetDateListを取得する。
	 * @return targetDateList
	 */
	public List<SelectTargetDateList> getTargetDateList() {
		return targetDateList;
	}
	/**
	 * メソッド名 : targetDateListのSetterメソッド
	 * 機能概要 : targetDateListをセットする。
	 * @param targetDateList
	 */
	public void setTargetDateList(List<SelectTargetDateList> targetDateList) {
		this.targetDateList = targetDateList;
	}
	/**
	 * メソッド名 : topAppSummaryListのGetterメソッド
	 * 機能概要 : topAppSummaryListを取得する。
	 * @return topAppSummaryList
	 */
	public List<SelectTopAppSummaryList> getTopAppSummaryList() {
		return topAppSummaryList;
	}
	/**
	 * メソッド名 : topAppSummaryListのSetterメソッド
	 * 機能概要 : topAppSummaryListをセットする。
	 * @param topAppSummaryList
	 */
	public void setTopAppSummaryList(List<SelectTopAppSummaryList> topAppSummaryList) {
		this.topAppSummaryList = topAppSummaryList;
	}
	/**
	 * メソッド名 : appTargetDateのGetterメソッド
	 * 機能概要 : appTargetDateを取得する。
	 * @return appTargetDate
	 */
	public String getAppTargetDate() {
		return appTargetDate;
	}
	/**
	 * メソッド名 : appTargetDateのSetterメソッド
	 * 機能概要 : appTargetDateをセットする。
	 * @param appTargetDate
	 */
	public void setAppTargetDate(String appTargetDate) {
		this.appTargetDate = appTargetDate;
	}



}
