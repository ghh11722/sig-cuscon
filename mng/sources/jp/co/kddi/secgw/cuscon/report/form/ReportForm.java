/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ReportForm.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/26     takahsht                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.report.form;

import java.util.List;

import jp.co.kddi.secgw.cuscon.report.vo.SelectTopAppSummaryList;
import jp.co.kddi.secgw.cuscon.report.vo.SelectTargetDateList;
import jp.co.kddi.secgw.cuscon.report.vo.SelectTopApplicationSummaryList;
import jp.co.kddi.secgw.cuscon.report.vo.SelectTopAttacksSummaryList;
import jp.co.kddi.secgw.cuscon.report.vo.SelectTopDstSummaryList;
import jp.co.kddi.secgw.cuscon.report.vo.SelectTopDstCountriesSummaryList;
import jp.co.kddi.secgw.cuscon.report.vo.SelectTopSrcSummaryList;
import jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx;

/**
 * クラス名 : ReportForm
 * 機能概要 :
 * 備考 :
 * @author takahsht
 * @version 1.0 takahsht
 *          Created 2010/02/26
 *          新規作成
 * @see
 */
public class ReportForm extends ValidatorActionFormEx{

	//
	private static final long serialVersionUID = 1L;

	// レポート対象日付リスト
	private List<SelectTargetDateList> targetDateList = null;
	// Appトップサマリレポートリスト
	private List<SelectTopAppSummaryList> topAppSummaryList = null;
	// Appトップサマリレポートリスト
	private List<SelectTopAttacksSummaryList> topAttackSummaryList = null;
	// Appトップサマリレポートリスト
	private List<SelectTopDstSummaryList> topDstSummaryList = null;
	// Appトップサマリレポートリスト
	private List<SelectTopDstCountriesSummaryList> topDstCountriesSummaryList = null;
	// Appトップサマリレポートリスト
	private List<SelectTopSrcSummaryList> topSrcSummaryList = null;
	// アプリケーショントップサマリレポートリスト
	private List<SelectTopApplicationSummaryList> topApplicationSummaryList = null;
	// レポート対象日付
	private String targetDate = null;
	private String targetPeriod = null;
	private String appTargetDate = null;
	private String attackTargetDate = null;
	private String dstTargetDate = null;
	private String dstContTargetDate = null;
	private String srcTargetDate = null;


	/**
	 * メソッド名 : appTargetDateのGetterメソッド
	 * 機能概要 : appTargetDateを取得する。
	 * @return appTargetDate
	 */
	public String getAppTargetDate() {
		return appTargetDate;
	}
	/**
	 * メソッド名 : appTargetDateのSetterメソッド
	 * 機能概要 : appTargetDateをセットする。
	 * @param appTargetDate
	 */
	public void setAppTargetDate(String appTargetDate) {
		this.appTargetDate = appTargetDate;
	}
	/**
	 * メソッド名 : attackTargetDateのGetterメソッド
	 * 機能概要 : attackTargetDateを取得する。
	 * @return attackTargetDate
	 */
	public String getAttackTargetDate() {
		return attackTargetDate;
	}
	/**
	 * メソッド名 : attackTargetDateのSetterメソッド
	 * 機能概要 : attackTargetDateをセットする。
	 * @param attackTargetDate
	 */
	public void setAttackTargetDate(String attackTargetDate) {
		this.attackTargetDate = attackTargetDate;
	}
	/**
	 * メソッド名 : dstTargetDateのGetterメソッド
	 * 機能概要 : dstTargetDateを取得する。
	 * @return dstTargetDate
	 */
	public String getDstTargetDate() {
		return dstTargetDate;
	}
	/**
	 * メソッド名 : dstTargetDateのSetterメソッド
	 * 機能概要 : dstTargetDateをセットする。
	 * @param dstTargetDate
	 */
	public void setDstTargetDate(String dstTargetDate) {
		this.dstTargetDate = dstTargetDate;
	}
	/**
	 * メソッド名 : dstContTargetDateのGetterメソッド
	 * 機能概要 : dstContTargetDateを取得する。
	 * @return dstContTargetDate
	 */
	public String getDstContTargetDate() {
		return dstContTargetDate;
	}
	/**
	 * メソッド名 : dstContTargetDateのSetterメソッド
	 * 機能概要 : dstContTargetDateをセットする。
	 * @param dstContTargetDate
	 */
	public void setDstContTargetDate(String dstContTargetDate) {
		this.dstContTargetDate = dstContTargetDate;
	}
	/**
	 * メソッド名 : srcTargetDateのGetterメソッド
	 * 機能概要 : srcTargetDateを取得する。
	 * @return srcTargetDate
	 */
	public String getSrcTargetDate() {
		return srcTargetDate;
	}
	/**
	 * メソッド名 : srcTargetDateのSetterメソッド
	 * 機能概要 : srcTargetDateをセットする。
	 * @param srcTargetDate
	 */
	public void setSrcTargetDate(String srcTargetDate) {
		this.srcTargetDate = srcTargetDate;
	}
	/**
	 * メソッド名 : targetPeriodのGetterメソッド
	 * 機能概要 : targetPeriodを取得する。
	 * @return targetPeriod
	 */
	public String getTargetPeriod() {
		return targetPeriod;
	}
	/**
	 * メソッド名 : targetPeriodのSetterメソッド
	 * 機能概要 : targetPeriodをセットする。
	 * @param targetPeriod
	 */
	public void setTargetPeriod(String targetPeriod) {
		this.targetPeriod = targetPeriod;
	}
	/**
	 * メソッド名 : topApplicationSummaryListのGetterメソッド
	 * 機能概要 : topApplicationSummaryListを取得する。
	 * @return topApplicationSummaryList
	 */
	public List<SelectTopApplicationSummaryList> getTopApplicationSummaryList() {
		return topApplicationSummaryList;
	}
	/**
	 * メソッド名 : topApplicationSummaryListのSetterメソッド
	 * 機能概要 : topApplicationSummaryListをセットする。
	 * @param topApplicationSummaryList
	 */
	public void setTopApplicationSummaryList(List<SelectTopApplicationSummaryList> topApplicationSummaryList) {
		this.topApplicationSummaryList = topApplicationSummaryList;
	}
	/**
	 * メソッド名 : topDstCountriesSummaryListのGetterメソッド
	 * 機能概要 : topDstCountriesSummaryListを取得する。
	 * @return topDstCountriesSummaryList
	 */
	public List<SelectTopDstCountriesSummaryList> getTopDstCountriesSummaryList() {
		return topDstCountriesSummaryList;
	}
	/**
	 * メソッド名 : topDstCountriesSummaryListのSetterメソッド
	 * 機能概要 : topDstCountriesSummaryListをセットする。
	 * @param topDstCountriesSummaryList
	 */
	public void setTopDstCountriesSummaryList(List<SelectTopDstCountriesSummaryList> topDstCountriesSummaryList) {
		this.topDstCountriesSummaryList = topDstCountriesSummaryList;
	}
	/**
	 * メソッド名 : topSrcSummaryListのGetterメソッド
	 * 機能概要 : topSrcSummaryListを取得する。
	 * @return topSrcSummaryList
	 */
	public List<SelectTopSrcSummaryList> getTopSrcSummaryList() {
		return topSrcSummaryList;
	}
	/**
	 * メソッド名 : topSrcSummaryListのSetterメソッド
	 * 機能概要 : topSrcSummaryListをセットする。
	 * @param topSrcSummaryList
	 */
	public void setTopSrcSummaryList(List<SelectTopSrcSummaryList> topSrcSummaryList) {
		this.topSrcSummaryList = topSrcSummaryList;
	}
	/**
	 * メソッド名 : topDstSummaryListのGetterメソッド
	 * 機能概要 : topDstSummaryListを取得する。
	 * @return topDstSummaryList
	 */
	public List<SelectTopDstSummaryList> getTopDstSummaryList() {
		return topDstSummaryList;
	}
	/**
	 * メソッド名 : topDstSummaryListのSetterメソッド
	 * 機能概要 : topDstSummaryListをセットする。
	 * @param topDstSummaryList
	 */
	public void setTopDstSummaryList(List<SelectTopDstSummaryList> topDstSummaryList) {
		this.topDstSummaryList = topDstSummaryList;
	}
	/**
	 * メソッド名 : topAttackSummaryListのGetterメソッド
	 * 機能概要 : topAttackSummaryListを取得する。
	 * @return topAttackSummaryList
	 */
	public List<SelectTopAttacksSummaryList> getTopAttackSummaryList() {
		return topAttackSummaryList;
	}
	/**
	 * メソッド名 : topAttackSummaryListのSetterメソッド
	 * 機能概要 : topAttackSummaryListをセットする。
	 * @param topAttackSummaryList
	 */
	public void setTopAttackSummaryList(List<SelectTopAttacksSummaryList> topAttackSummaryList) {
		this.topAttackSummaryList = topAttackSummaryList;
	}
	/**
	 * targetDateListを取得します。
	 * @return targetDateList
	 */
	public List<SelectTargetDateList> getTargetDateList() {
		return targetDateList;
	}
	/**
	 * targetDateListを設定します。
	 * @param targetDateList
	 */
	public void setTargetDateList(List<SelectTargetDateList> targetDateList) {
		this.targetDateList = targetDateList;
	}
	/**
	 * topAppSummaryListを取得します。
	 * @return topAppSummaryList
	 */
	public List<SelectTopAppSummaryList> getTopAppSummaryList() {
		return topAppSummaryList;
	}
	/**
	 * topAppSummaryListを設定します。
	 * @param topAppSummaryList
	 */
	public void setTopAppSummaryList(List<SelectTopAppSummaryList> topAppSummaryList) {
		this.topAppSummaryList = topAppSummaryList;
	}
	/**
	 * targetDateを取得します。
	 * @return targetDate
	 */
	public String getTargetDate() {
		return targetDate;
	}
	/**
	 * targetDateを設定します。
	 * @param targetDate
	 */
	public void setTargetDate(String targetDate) {
		this.targetDate = targetDate;
	}
}
