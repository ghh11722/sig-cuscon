/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : TopAppSummary.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/23     takahsht                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.report.vo;

import java.io.Serializable;

/**
 * クラス名 : TopAppSummary
 * 機能概要 : Appトップサマリレポートオブジェクトデータクラス
 * 備考 :
 * @author takahsht
 * @version 1.0 takahsht
 *          Created 2010/02/23
 *          新規作成
 * @see
 */
public class SelectTopAppSummaryList implements Serializable{

	//
	private static final long serialVersionUID = 3637625638427737215L;
	// シーケンス番号
	private int seqNo = 0;
	// Vsys-ID
	private String vsysId = null;
	// レポート対象日付
	private String targetDate = null;
	// App
	private String app = null;
	// Risk-Of-App
	private String riskOfApp = null;
	// Bytes
	private String bytes = null;
	// Sessions
	private String sessions = null;

	/**
	 * メソッド名 : seqNoのGetterメソッド
	 * 機能概要 : seqNoを取得する。
	 * @return seqNo
	 */
	public int getSeqNo() {
		return seqNo;
	}
	/**
	 * メソッド名 : seqNoのSetterメソッド
	 * 機能概要 : seqNoをセットする。
	 * @param seqNo
	 */
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得する。
	 * @return vsysId
	 */
	public String getVsysId() {
		return vsysId;
	}
	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdをセットする。
	 * @param vsysId
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}
	/**
	 * メソッド名 : targetDateのGetterメソッド
	 * 機能概要 : targetDateを取得する。
	 * @return targetDate
	 */
	public String getTargetDate() {
		return targetDate;
	}
	/**
	 * メソッド名 : targetDateのSetterメソッド
	 * 機能概要 : targetDateをセットする。
	 * @param targetDate
	 */
	public void setTargetDate(String targetDate) {
		this.targetDate = targetDate;
	}
	/**
	 * メソッド名 : appのGetterメソッド
	 * 機能概要 : appを取得する。
	 * @return app
	 */
	public String getApp() {
		return app;
	}
	/**
	 * メソッド名 : appのSetterメソッド
	 * 機能概要 : appをセットする。
	 * @param app
	 */
	public void setApp(String app) {
		this.app = app;
	}
	/**
	 * メソッド名 : riskOfAppのGetterメソッド
	 * 機能概要 : riskOfAppを取得する。
	 * @return riskOfApp
	 */
	public String getRiskOfApp() {
		return riskOfApp;
	}
	/**
	 * メソッド名 : riskOfAppのSetterメソッド
	 * 機能概要 : riskOfAppをセットする。
	 * @param riskOfApp
	 */
	public void setRiskOfApp(String riskOfApp) {
		this.riskOfApp = riskOfApp;
	}
	/**
	 * メソッド名 : bytesのGetterメソッド
	 * 機能概要 : bytesを取得する。
	 * @return bytes
	 */
	public String getBytes() {
		return bytes;
	}
	/**
	 * メソッド名 : bytesのSetterメソッド
	 * 機能概要 : bytesをセットする。
	 * @param bytes
	 */
	public void setBytes(String bytes) {
		this.bytes = bytes;
	}
	/**
	 * メソッド名 : sessionsのGetterメソッド
	 * 機能概要 : sessionsを取得する。
	 * @return sessions
	 */
	public String getSessions() {
		return sessions;
	}
	/**
	 * メソッド名 : sessionsのSetterメソッド
	 * 機能概要 : sessionsをセットする。
	 * @param sessions
	 */
	public void setSessions(String sessions) {
		this.sessions = sessions;
	}

}
