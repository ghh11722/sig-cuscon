/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : MSystemConstant.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/14     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.logdownload.vo;

import java.io.Serializable;

/**
 * クラス名 : MSystemConstant
 * 機能概要 :
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/14
 *          新規作成
 * @see
 */
public class MSystemConstant implements Serializable{

	// シリアルバージョンUID
	private static final long serialVersionUID = 4597892818109151681L;

	// ダウンロードログ保存ディレクトリ
	private String downloadLogDir;

	/**
	 * メソッド名 : downloadLogDirのSetterメソッド
	 * 機能概要 : downloadLogDirをセットする。
	 * @param downloadLogDir システム定数マスタ
	 */
	public void setDownloadLogDir(String downloadLogDir) {
		this.downloadLogDir = downloadLogDir;
	}

	/**
	 * メソッド名 : downloadLogDirのGetterメソッド
	 * 機能概要 : downloadLogDirを取得する。
	 * @return downloadLogDir システム定数マスタ
	 */
	public String getDownloadLogDir() {
		return downloadLogDir;
	}
}
