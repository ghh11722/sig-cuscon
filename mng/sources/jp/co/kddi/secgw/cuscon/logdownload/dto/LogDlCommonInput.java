/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : LogDlCommonLogInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/05/26     k_takenaka                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.logdownload.dto;

import java.io.File;
import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;

/**
 * クラス名 : LogDlCommonLogInput
 * 機能概要 :
 * 備考 :
 * @author k_takenaka
 * @version 1.0 k_takenaka
 *          Created 2011/05/26
 *          新規作成
 * @see
 */
public class LogDlCommonInput {

	// 共通セッション情報
	private CusconUVO uvo = null;
	// 年月リスト
	private List<String> ymList = null;
	// 年月リスト選択インデックス
	private int ymlstIdx = 0;
	// ファイルリスト
	private List<File> fileList = null;
	// ファイル名リスト
	private List<String> fileNameList = null;
	// ファイル名リスト選択インデックス
	private int selectedFileIdx = 0;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得します。
	 * @return uvo 共通セッション情報
	 */
	public CusconUVO getUvo() {
	    return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoを設定します。
	 * @param uvo 共通セッション情報
	 */
	public void setUvo(CusconUVO uvo) {
	    this.uvo = uvo;
	}

	/**
	 * メソッド名 : ymListのGetterメソッド
	 * 機能概要 : ymListを取得します。
	 * @return ymList 年月リスト
	 */
	public List<String> getYmList() {
	    return ymList;
	}

	/**
	 * メソッド名 : ymListのSetterメソッド
	 * 機能概要 : ymListを設定します。
	 * @param ymList 年月リスト
	 */
	public void setYmList(List<String> ymList) {
	    this.ymList = ymList;
	}

	/**
	 * メソッド名 : ymlstIdxのGetterメソッド
	 * 機能概要 : ymlstIdxを取得する。
	 * @return ymlstIdx
	 */
	public int getYmlstIdx() {
		return ymlstIdx;
	}

	/**
	 * メソッド名 : ymlstIdxのSetterメソッド
	 * 機能概要 : ymlstIdxをセットする。
	 * @param ymlstIdx
	 */
	public void setYmlstIdx(int ymlstIdx) {
		this.ymlstIdx = ymlstIdx;
	}

	/**
	 * メソッド名 : fileListのGetterメソッド
	 * 機能概要 : fileListを取得します。
	 * @return fileList ファイルリスト
	 */
	public List<File> getFileList() {
	    return fileList;
	}

	/**
	 * メソッド名 : fileListのSetterメソッド
	 * 機能概要 : fileListを設定します。
	 * @param fileList ファイルリスト
	 */
	public void setFileList(List<File> fileList) {
	    this.fileList = fileList;
	}

	/**
	 * メソッド名 : fileNameListのGetterメソッド
	 * 機能概要 : fileNameListを取得します。
	 * @return fileNameList ファイルリスト
	 */
	public List<String> getFileNameList() {
	    return fileNameList;
	}

	/**
	 * メソッド名 : fileNameListのSetterメソッド
	 * 機能概要 : fileNameListを設定します。
	 * @param fileNameList ファイルリスト
	 */
	public void setFileNameList(List<String> fileNameList) {
	    this.fileNameList = fileNameList;
	}

	/**
	 * メソッド名 : selectedFileIdxのGetterメソッド
	 * 機能概要 : selectedFileIdxを取得する。
	 * @return selectedFileIdx
	 */
	public int getSelectedFileIdx() {
		return selectedFileIdx;
	}

	/**
	 * メソッド名 : selectedFileIdxのSetterメソッド
	 * 機能概要 : selectedFileIdxをセットする。
	 * @param selectedFileIdx
	 */
	public void setSelectedFileIdx(int selectedFileIdx) {
		this.selectedFileIdx = selectedFileIdx;
	}
}
