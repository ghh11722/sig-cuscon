/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : LogDlDLAction.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/05/30     ktakenaka@PROSITE                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.logdownload.action;

import java.io.File;
import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.logdownload.common.LogDlCommon;
import jp.co.kddi.secgw.cuscon.logdownload.dto.LogDlCommonInput;
import jp.co.kddi.secgw.cuscon.logdownload.dto.LogDlCommonOutput;
import jp.co.kddi.secgw.cuscon.logmonitor.action.TrafficLogDLAction;
import jp.co.kddi.secgw.cuscon.logmonitor.common.LogMonitorCommon;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.web.struts.actions.DownloadByteArray;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.h2.util.StringUtils;

/**
 * クラス名 : LogDlDLAction
 * 機能概要 :
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/05/30
 *          新規作成
 * @see
 */
public class LogDlCommonDLAction implements BLogic<LogDlCommonInput>{

	// DAO
	private QueryDAO queryDAO = null;
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(TrafficLogDLAction.class);

	/**
	 * メソッド名 :ログダウンロード実行
	 * 機能概要 :
	 * @param arg0
	 * @return
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(LogDlCommonInput param) {
		log.debug("ログダウンロード処理開始");

		//共通セッション情報
		CusconUVO uvo = param.getUvo();

		LogDlCommonOutput out = new LogDlCommonOutput();	// 画面表示のパラメータ保持
		String returnStr = "failure";						// リターン文字列
		BLogicMessages errors = new BLogicMessages();		// エラーメッセージ
		DownloadByteArray downloadObject = null; 			// ダウンロードデータ格納バッファ

		//入力値取得
		int idx = param.getSelectedFileIdx();
		List<String>fileNameList = param.getFileNameList();
		String fileName =fileNameList.get(idx);
		String filePath = LogDlCommon.getDownloadLogDir(queryDAO, log, messageAccessor, uvo);
		String combineFileName = new File(filePath, fileName).getPath();

		boolean isNormal = true;
		//セッション情報チェック
		log.debug("セッション情報チェック");
		if (uvo == null) {
			//セッション情報NG
			errors.add("message", new BLogicMessage("DK200001"));
			log.error(messageAccessor.getMessage("EK200001" ,null, param.getUvo()));
			isNormal = false;
		}
		else {
			StringBuffer chkBuff = new StringBuffer(uvo.getVsysId());
			if (uvo.getVsysId() == null){
				//	VSYS-ID NG
				errors.add("message", new BLogicMessage("DK200001"));
				log.error(messageAccessor.getMessage("EK200002" ,null, param.getUvo()));
				isNormal = false;
			}
			else if (chkBuff.length() == 0){
				//	VSYS-ID NG
				errors.add("message", new BLogicMessage("DK200001"));
				log.error(messageAccessor.getMessage("EK200002" ,null, param.getUvo()));
				isNormal = false;
			}
			else if( (fileName == null) || (fileName.length()==0) ) {
				// 	ファイル名が指定されていない
				errors.add("message", new BLogicMessage("DK200001"));
				log.error(messageAccessor.getMessage("EK200003" ,null, param.getUvo()));
				isNormal = false;
			}
			else {
				try {
					// ファイルの存在チェック
					File file = new File(combineFileName);
					if ((file.exists() == false) || (file.isFile() == false)) {
						errors.add("message", new BLogicMessage("DK200002"));
						log.error(messageAccessor.getMessage("EK200004" ,null, param.getUvo()));
						isNormal = false;
					}
				} catch (Exception e) {
					errors.add("message", new BLogicMessage("DK080114"));
					log.error(messageAccessor.getMessage("EK081115" ,null, param.getUvo()));
					isNormal = false;
				}
			}
		}

		if(isNormal) {
			// ファイルをバイト配列に変換する
			byte[] byteArray = LogMonitorCommon.createByteArrayFromFile(combineFileName);
			// ダウンロードデータをバッファへ格納する。
			downloadObject = new DownloadByteArray(fileName, byteArray);
			returnStr = "success";
		}

		BLogicResult result = new BLogicResult();
		if(StringUtils.equals(returnStr, "failure") == true){
			result.setErrors(errors);
			result.setResultObject(out);
		}else{
			result.setResultObject(downloadObject);
		}
		result.setResultString(returnStr);
		log.debug(String.format("ログダウンロード処理終了(状態：%s)", returnStr));
		return result;
	}

	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * queryDAOを取得します。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * queryDAOを設定します。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}
}
