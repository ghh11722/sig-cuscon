/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : LogDlCommon.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/05/30     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.logdownload.common;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.logdownload.vo.MSystemConstant;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

import org.apache.commons.logging.Log;

/**
 * クラス名 : LogDlCommon
 * 機能概要 : ログダウンロード共通クラス
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/05/30
 *          新規作成
 * @see
 */
public class LogDlCommon {

	/**
	 * メソッド名 :ファイルフィルター取得（正規表現指定）
	 * 機能概要 :ファイル名が指定する正規表現のパターンにマッチするファイルを取得する
	 * @param regex 正規表現パターン
	 * @return
	 */
	public static FilenameFilter getFileRegexFilter(String regex) {
		final String _regex = regex;
		return new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				boolean ret = name.matches(_regex);
				return ret;
			}
		};
	}

	/**
	 * ログファイルリスト及び、年月リストを取得
	 * メソッド名 :
	 * 機能概要 :
	 * @param fileList
	 * @param ymList
	 * @param logFileNameFormat
	 * @param rangeDays
	 * @param queryDAO
	 * @param log
	 * @param messageAccessor
	 * @param uvo
	 */
	public static boolean getLogList(List<File> fileList, List<String> ymList, String logFileNameFormat,
			int rangeDays, QueryDAO queryDAO, Log log, MessageAccessor messageAccessor, CusconUVO uvo) {

		// ダウンロードログ保存ディレクトリを取得
		String fileDir = getDownloadLogDir(queryDAO, log, messageAccessor, uvo);

		if(fileDir == null) {
			return false;
		}

		// ログファイル一覧を取得
		File targetDir = new File(fileDir);

		if (targetDir.exists() && targetDir.isDirectory()) {
			// 指定のファイル名のみ取得
			// ファイル名「XXX_YYYYMMDD_aaa.zip -> vsysXXX_YYYYMMDD_aaa.zipが正(2011/07/19) XXXは0埋めなし
			// VSYS-IDの番号部分のみ（3桁0埋め）
			// aaa:tra/ips/web/anv/ans
//2011/07/19 rep start
//			File[] tmpfileList = targetDir.listFiles(LogDlCommon.getFileRegexFilter(
//							String.format(logFileNameFormat, Integer.valueOf(uvo.getVsysId().substring(4)))));
			File[] tmpfileList = targetDir.listFiles(LogDlCommon.getFileRegexFilter(
			String.format(logFileNameFormat, uvo.getVsysId())));
//2011/07/19 rep end

			if(tmpfileList.length == 0) {
				return true;
			}

			// 日付順でソート
			Arrays.sort(tmpfileList, new Comparator<File>() {
	            @Override
	            public int compare(File file1, File file2) {
//2011/07/19 rep start
//	            	Integer yyyymmdd1 = Integer.valueOf(file1.getName().substring(4, 12));
//	            	Integer yyyymmdd2 = Integer.valueOf(file2.getName().substring(4, 12));
	            	Integer yyyymmdd1 = 0;
	            	Integer yyyymmdd2 = 0;
	            	Integer po1 = file1.getName().indexOf("_");
	            	Integer po2 = file1.getName().indexOf("_");
	            	if(po1!=-1 && po2!=-1){
		            	yyyymmdd1 = Integer.valueOf(file1.getName().substring(po1+1, po1+1+8));
		            	yyyymmdd2 = Integer.valueOf(file2.getName().substring(po2+1, po2+1+8));
	            	}
//2011/07/19 rep end
	                return yyyymmdd1.compareTo( yyyymmdd2 );
	            }
	        });

			// 範囲日付を取得
			int limitYmd = 0;
			{
				Calendar cal = Calendar.getInstance();
				// 今日の日付から範囲日付を引く
				cal.add(Calendar.DATE, -rangeDays + 1);
				// cal.add(Calendar.MONTH, -3);
				limitYmd = Integer.valueOf(String.format("%04d%02d%02d",
						cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DATE)));
			}

			// 当日日付を取得
			int today = 0;
			{
				Calendar cal = Calendar.getInstance();
				today = Integer.valueOf(String.format("%04d%02d%02d",
						cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DATE)));
			}

			// 範囲日付のファイルオブジェクト及びファイル名を保存する
			for (int i = 0; i < tmpfileList.length; i++) {
//2011/07/19 rep start
//				int tmpYmd = Integer.valueOf(tmpfileList[i].getName().substring(4, 12));
				int tmpYmd = 0;
            	Integer po = tmpfileList[i].getName().indexOf("_");
            	if(po!=-1){
            		tmpYmd = Integer.valueOf(tmpfileList[i].getName().substring(po+1, po+1+8));
            	}
//2011/07/19 rep end

				// 範囲日付内ならば
				if (tmpYmd >= limitYmd && tmpYmd <= today) {
					// ファイルリストに追加
					fileList.add(tmpfileList[i]);

					String yyyymm = String.valueOf(tmpYmd / 100);
					// 既存チェック
					boolean isExist = false;
					if (ymList.size() > 0) {
						for (String ym : ymList) {
							if (ym.equals(yyyymm)) {
								isExist = true;
								break;
							}
						}
					}
					if (!isExist) {
						// 年月リストに追加
						ymList.add(yyyymm);
					}
				}
			}
		}
		return true;
	}

	/**
	 * ダウンロードファイルディレクトリ取得
	 * メソッド名 :
	 * 機能概要 :
	 * @param queryDAO
	 * @param log
	 * @param messageAccessor
	 * @param uvo
	 * @return
	 */
	public static String getDownloadLogDir(QueryDAO queryDAO, Log log,
			MessageAccessor messageAccessor, CusconUVO uvo) {

		// ベースディレクトを検索
		List<MSystemConstant> retList;
		String baseDir = null;

		try {
			log.debug("ダウンロードログディレクトリのルート取得");
			retList = queryDAO.executeForObjectList("LogDownloadCommon-1", null);
			if (retList.size() > 0) {
				MSystemConstant obj = retList.get(0);
				baseDir = obj.getDownloadLogDir();

				// ディレクトリの存在確認
				File file = new File(baseDir);
				if (file.exists() == false) {
					String[] errMsgAry = { baseDir };
					log.fatal(messageAccessor.getMessage("FK200002", errMsgAry, uvo));
					return null;
				}
			} else {
				log.fatal(messageAccessor.getMessage("FK200001", null, uvo));
				return null;
			}
		} catch (Exception e) {
			// DBアクセスエラー
			// エラー処理を記述
			log.fatal(messageAccessor.getMessage("FK200001", null, uvo));
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			return null;
		}
		return baseDir;
	}
}
