/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : LogDlIpsIdsMonthBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/05/26     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.logdownload.blogic;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.logdownload.common.LogDlCommon;
import jp.co.kddi.secgw.cuscon.logdownload.dto.LogDlCommonInput;
import jp.co.kddi.secgw.cuscon.logdownload.dto.LogDlCommonOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.util.PropertyUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * クラス名 : LogDlIpsIdsMonthBLogic
 * 機能概要 :
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/05/26
 *          新規作成
 * @see
 */
public class LogDlIpsIdsMonthBLogic implements BLogic<LogDlCommonInput> {

	// DAO
	private QueryDAO queryDAO = null;
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(LogDlIpsIdsMonthBLogic.class);

	/**
	 * メソッド名 : IDS/IPSログ年月一覧取得処理
	 * 機能概要 : IDS/IPSログ年月一覧を取得
	 * @param param IDS/IPSログ年月一覧取得処理ビジネスロジックの入力を保持したMap
	 * @return IDS/IPSログ年月一覧取得処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(LogDlCommonInput param) {
		log.debug("年月一覧取得処理開始");

		BLogicResult result = new BLogicResult();
		BLogicMessages errors = new BLogicMessages(); // エラーメッセージ

		List<File> fileList = new ArrayList<File>();
		List<String> ymList = new ArrayList<String>();

		// 共通セッション情報
		CusconUVO uvo = param.getUvo();
		if (uvo == null) {
			// セッション情報NG
			log.error(messageAccessor.getMessage("EK200001", null, param.getUvo()));
			errors.add("message", new BLogicMessage("DK200001"));
			result.setErrors(errors);
			result.setResultString("failure");
			return result;
		} else {
			StringBuffer chkBuff = new StringBuffer(uvo.getVsysId());
			if (uvo.getVsysId() == null) {
				// VSYS-ID NG
				log.error(messageAccessor.getMessage("EK200002", null, param.getUvo()));
				errors.add("message", new BLogicMessage("DK200001"));
				result.setErrors(errors);
				result.setResultString("failure");
				return result;
			} else if (chkBuff.length() == 0) {
				// VSYS-ID NG
				log.error(messageAccessor.getMessage("EK200002", null, param.getUvo()));
				errors.add("message", new BLogicMessage("DK200001"));
				result.setErrors(errors);
				result.setResultString("failure");
				return result;
			}
		}

		// プロパティファイルから範囲期間を読込
		int rangeDays = Integer.valueOf(PropertyUtil.getProperty("logdownload.range_days.ips"));

		// ログファイル名のフォーマットを定義
		LogDlCommonOutput out = new LogDlCommonOutput();

		// ログファイル名のフォーマットを定義
//2011/07/19 rep start
//		String logFileNameFormat = "%03d_[0-9]{8}_ips.zip";
		String logFileNameFormat = "%s_[0-9]{8}_ips.zip";
//2011/07/19 rep end

		// ログ圧縮ファイルリスト及び年月リストを取得
		if(LogDlCommon.getLogList(fileList, ymList,logFileNameFormat, rangeDays,
				queryDAO, log, messageAccessor, uvo) == false) {
			errors.add("message", new BLogicMessage("DK200002"));
			out.setYmList(ymList);
			out.setFileList(fileList);
			result.setResultObject(out);
			result.setErrors(errors);
			result.setResultString("failure");
			return result;
		}

		if(fileList.size() == 0) {
			out.setMessage(messageAccessor.getMessage("DK200003", null));
		}

		// 出力結果に、年月リストをセット
		out.setYmList(ymList);
		out.setFileList(fileList);
		result.setResultObject(out);
		result.setResultString("success");
		log.debug("年月一覧取得処理終了");
		return result;
	}

	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * queryDAOを取得します。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * queryDAOを設定します。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}
}
