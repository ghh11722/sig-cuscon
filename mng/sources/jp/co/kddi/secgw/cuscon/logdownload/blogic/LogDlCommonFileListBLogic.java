/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : LogDlCommonFileListBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/05/26     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.logdownload.blogic;

import java.util.ArrayList;
import java.util.List;

import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.logdownload.dto.LogDlCommonInput;
import jp.co.kddi.secgw.cuscon.logdownload.dto.LogDlCommonOutput;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicResult;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * クラス名 : LogDlCommonFileListBLogic
 * 機能概要 :
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/05/26
 *          新規作成
 * @see
 */
public class LogDlCommonFileListBLogic implements BLogic<LogDlCommonInput> {

	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(LogDlCommonFileListBLogic.class);

	/**
	 * メソッド名 : ログダウンロード機能共通ログ一覧取得処理
	 * 機能概要 : ログダウンロード機能共通ログ一覧を取得
	 * @param param ログダウンロード機能共通ログ一覧取得処理ビジネスロジックの入力を保持したMap
	 * @return ログダウンロード機能共通ログ一覧取得処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(LogDlCommonInput param) {
		log.debug("ログファイル一覧処理開始");

		BLogicResult result = new BLogicResult();

		// 選択した年月を取得
		String yyyymm = param.getYmList().get(param.getYmlstIdx());

		List<String> fileNameList = new ArrayList<String>();

		// 選択した年月のファイル名をリストに保存
		for(int i=0; i<param.getFileList().size(); i++) {

//2011/07/19 rep start
//			String tmpYm = param.getFileList().get(i).getName().substring(4, 10);
			String tmpYm = null;
        	Integer po = param.getFileList().get(i).getName().indexOf("_");
        	if(po!=-1){
        		tmpYm = param.getFileList().get(i).getName().substring(po+1, po+1+6);
        	}
//2011/07/19 rep end

			if(tmpYm.equals(yyyymm)) {
				fileNameList.add(param.getFileList().get(i).getName());
			}
		}

		// 出力結果に、ファイル名リストをセット
		LogDlCommonOutput out = new LogDlCommonOutput();
		out.setFileNameList(fileNameList);
		result.setResultObject(out);
		result.setResultString("success");
		log.debug("ログファイル一覧処理終了");
		return result;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}
}
