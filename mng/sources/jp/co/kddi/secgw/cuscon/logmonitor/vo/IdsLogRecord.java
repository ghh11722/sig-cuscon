/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : IdsLogRecord.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/03/04  T.Sutoh@JCCH  初版作成
 * 2010/03/26  T.Sutoh@JCCH  ID,severityの削除
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.logmonitor.vo;

/**
 * クラス名 : IdsLogRecord
 * 機能概要 :
 * 備考 :
 * @author T.Sutoh@JCCH
 * @version 1.0 T.Sutoh@JCCH
 *          Created 2010/03/04
 *          新規作成
 * @version 1.1 T.Sutoh@JCCH
 *          Created 2010/03/26
 *          ID,severityの削除
 */
public class IdsLogRecord {

	// ログ一覧出力項目
	private String action;
	private String application;
	private String destinationPort;
	private String destinationZone;
	private String destIp;
	private String logId;
	private String reciveTime;
	private String ruleName;
	private String sourceIp;
	private String sourcePort;
	private String sourceZone;
	private String type;
	private String virtualSystem;
	// 20110526 ktakenkaa@PROSITE add start
	private String category;
	private String miscellaneous;
	// 20110526 ktakenkaa@PROSITE add ends

	/**
	 * メソッド名 : actionのGetterメソッド
	 * 機能概要 : actionを取得します。
	 * @return action action
	 */
	public String getAction() {
	    return action;
	}

	/**
	 * メソッド名 : applicationのGetterメソッド
	 * 機能概要 : applicationを取得します。
	 * @return application application
	 */
	public String getApplication() {
	    return application;
	}

	/**
	 * メソッド名 : destinationPortのGetterメソッド
	 * 機能概要 : destinationPortを取得します。
	 * @return destinationPort destinationPort
	 */
	public String getDestinationPort() {
	    return destinationPort;
	}

	/**
	 * メソッド名 : destinationZoneのGetterメソッド
	 * 機能概要 : destinationZoneを取得します。
	 * @return destinationZone destinationZone
	 */
	public String getDestinationZone() {
	    return destinationZone;
	}

	/**
	 * メソッド名 : destIpのGetterメソッド
	 * 機能概要 : destIpを取得します。
	 * @return destIp destIp
	 */
	public String getDestIp() {
	    return destIp;
	}

	/**
	 * メソッド名 : logIdのGetterメソッド
	 * 機能概要 : logIdを取得します。
	 * @return logId logId
	 */
	public String getLogId() {
	    return logId;
	}

	/**
	 * メソッド名 : reciveTimeのGetterメソッド
	 * 機能概要 : reciveTimeを取得します。
	 * @return reciveTime reciveTime
	 */
	public String getReciveTime() {
	    return reciveTime;
	}

	/**
	 * メソッド名 : ruleNameのGetterメソッド
	 * 機能概要 : ruleNameを取得します。
	 * @return ruleName ruleName
	 */
	public String getRuleName() {
	    return ruleName;
	}

	/**
	 * メソッド名 : sourceIpのGetterメソッド
	 * 機能概要 : sourceIpを取得します。
	 * @return sourceIp sourceIp
	 */
	public String getSourceIp() {
	    return sourceIp;
	}

	/**
	 * メソッド名 : sourcePortのGetterメソッド
	 * 機能概要 : sourcePortを取得します。
	 * @return sourcePort sourcePort
	 */
	public String getSourcePort() {
	    return sourcePort;
	}

	/**
	 * メソッド名 : sourceZoneのGetterメソッド
	 * 機能概要 : sourceZoneを取得します。
	 * @return sourceZone sourceZone
	 */
	public String getSourceZone() {
	    return sourceZone;
	}

	/**
	 * メソッド名 : typeのGetterメソッド
	 * 機能概要 : typeを取得します。
	 * @return type type
	 */
	public String getType() {
	    return type;
	}

	/**
	 * メソッド名 : virtualSystemのGetterメソッド
	 * 機能概要 : virtualSystemを取得します。
	 * @return virtualSystem virtualSystem
	 */
	public String getVirtualSystem() {
	    return virtualSystem;
	}

	/**
	 * メソッド名 : actionのSetterメソッド
	 * 機能概要 : actionを設定します。
	 * @param action action
	 */
	public void setAction(String action) {
	    this.action = action;
	}

	/**
	 * メソッド名 : applicationのSetterメソッド
	 * 機能概要 : applicationを設定します。
	 * @param application application
	 */
	public void setApplication(String application) {
	    this.application = application;
	}

	/**
	 * メソッド名 : destinationPortのSetterメソッド
	 * 機能概要 : destinationPortを設定します。
	 * @param destinationPort destinationPort
	 */
	public void setDestinationPort(String destinationPort) {
	    this.destinationPort = destinationPort;
	}

	/**
	 * メソッド名 : destinationZoneのSetterメソッド
	 * 機能概要 : destinationZoneを設定します。
	 * @param destinationZone destinationZone
	 */
	public void setDestinationZone(String destinationZone) {
	    this.destinationZone = destinationZone;
	}

	/**
	 * メソッド名 : destIpのSetterメソッド
	 * 機能概要 : destIpを設定します。
	 * @param destIp destIp
	 */
	public void setDestIp(String destIp) {
	    this.destIp = destIp;
	}

	/**
	 * メソッド名 : logIdのSetterメソッド
	 * 機能概要 : logIdを設定します。
	 * @param logId logId
	 */
	public void setLogId(String logId) {
	    this.logId = logId;
	}

	/**
	 * メソッド名 : reciveTimeのSetterメソッド
	 * 機能概要 : reciveTimeを設定します。
	 * @param reciveTime reciveTime
	 */
	public void setReciveTime(String reciveTime) {
	    this.reciveTime = reciveTime;
	}

	/**
	 * メソッド名 : ruleNameのSetterメソッド
	 * 機能概要 : ruleNameを設定します。
	 * @param ruleName ruleName
	 */
	public void setRuleName(String ruleName) {
	    this.ruleName = ruleName;
	}

	/**
	 * メソッド名 : sourceIpのSetterメソッド
	 * 機能概要 : sourceIpを設定します。
	 * @param sourceIp sourceIp
	 */
	public void setSourceIp(String sourceIp) {
	    this.sourceIp = sourceIp;
	}

	/**
	 * メソッド名 : sourcePortのSetterメソッド
	 * 機能概要 : sourcePortを設定します。
	 * @param sourcePort sourcePort
	 */
	public void setSourcePort(String sourcePort) {
	    this.sourcePort = sourcePort;
	}

	/**
	 * メソッド名 : sourceZoneのSetterメソッド
	 * 機能概要 : sourceZoneを設定します。
	 * @param sourceZone sourceZone
	 */
	public void setSourceZone(String sourceZone) {
	    this.sourceZone = sourceZone;
	}

	/**
	 * メソッド名 : typeのSetterメソッド
	 * 機能概要 : typeを設定します。
	 * @param type type
	 */
	public void setType(String type) {
	    this.type = type;
	}

	/**
	 * メソッド名 : virtualSystemのSetterメソッド
	 * 機能概要 : virtualSystemを設定します。
	 * @param virtualSystem virtualSystem
	 */
	public void setVirtualSystem(String virtualSystem) {
	    this.virtualSystem = virtualSystem;
	}

	// 20110526 ktakenaka@PROSITE add start
	/**
	 * メソッド名 : categoryのGetterメソッド
	 * 機能概要 : categoryを取得します。
	 * @return category category
	 */
	public String getCategory() {
	    return category;
	}

	/**
	 * メソッド名 : categoryのSetterメソッド
	 * 機能概要 : categoryを設定します。
	 * @param category category
	 */
	public void setCategory(String category) {
	    this.category = category;
	}

	/**
	 * メソッド名 : miscellaneousのGetterメソッド
	 * 機能概要 : miscellaneousを取得します。
	 * @return miscellaneous miscellaneous
	 */
	public String getMiscellaneous() {
	    return miscellaneous;
	}

	/**
	 * メソッド名 : miscellaneousのSetterメソッド
	 * 機能概要 : miscellaneousを設定します。
	 * @param miscellaneous miscellaneous
	 */
	public void setMiscellaneous(String miscellaneous) {
	    this.miscellaneous = miscellaneous;
	}
// 20110526 ktakenaka@PROSITE add end
}
