/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : MSystemConstant.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/03/04  T.Sutoh@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.logmonitor.vo;

import java.io.Serializable;

/**
 * クラス名 : MSystemConstant
 * 機能概要 : システム定数マスタパラメータ
 * 備考 : ログモニタでは作業用一時ディレクトリのみ使用
 * @author T.Sutoh@JCCH
 * @version 1.0 T.Sutoh@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class MSystemConstant implements Serializable{

	//
	private static final long serialVersionUID = 809233626452357752L;

	// システム定数マスタ
	private String workTmp;

	/**
	 * メソッド名 : workTmpのSetterメソッド
	 * 機能概要 : workTmpをセットする。
	 * @param workTmp システム定数マスタ
	 */
	public void setWorkTmp(String workTmp) {
		this.workTmp = workTmp;
	}

	/**
	 * メソッド名 : workTmpのGetterメソッド
	 * 機能概要 : workTmpを取得する。
	 * @return workTmp システム定数マスタ
	 */
	public String getWorkTmp() {
		return workTmp;
	}
}
