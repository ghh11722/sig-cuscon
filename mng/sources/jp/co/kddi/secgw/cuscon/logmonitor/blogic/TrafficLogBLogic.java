/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : TrafficLogBLogic.java
 *
 * [変更履歴]
 * 日付        更新者          内容
 * 2010/02/05  T.Sutoh@JCCH    初版作成
 * 2010/02/26  T.Sutoh@JCCH    DBアクセスエラー時のロールバックコールを追加
 * 2010/03/01  T.Sutoh@JCCH    ログ出力を追加
 * 2016/11/23  T.Yamazaki@Plum 検索日付のデフォルト設定を追加
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.logmonitor.blogic;

import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.h2.util.StringUtils;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.logmonitor.common.LogMonitorCommon;
import jp.co.kddi.secgw.cuscon.logmonitor.dto.TrafficLogInput;
import jp.co.kddi.secgw.cuscon.logmonitor.dto.TrafficLogOutput;
import jp.co.kddi.secgw.cuscon.logmonitor.vo.TrafficLogRecord;
import jp.co.kddi.secgw.cuscon.logmonitor.vo.SelectTrafficLog;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;
import jp.terasoluna.fw.util.PropertyUtil;

/**
 * クラス名 : TrafficLogBLogic
 * 機能概要 : トラフィックログ一覧取得処理ビジネスロジック
 * 備考 :
 * @author T.Sutoh@JCCH
 * @version 1.0 T.Sutoh@JCCH
 *          Created 2010/02/5
 *          新規作成
 * @version 1.1 T.Sutoh@JCCH
 *          Created 2010/02/26
 *          DBアクセスエラー時のロールバックコールを追加
 * @version 1.2 T.Sutoh@JCCH
 *          Created 2010/03/01
 *          ログ出力を追加
 * @version 1.3 T.Sutoh@JCCH
 *          Created 2011/02/01
 *          メニューから遷移した場合はDB検索を行わない処理を追加
 * @version 1.4 T.Sutoh@JCCH
 *          Created 2011/02/09
 *          検索条件へ日付を追加
 * @version 2.0 T.Yamazaki@Plum Systems Inc.
 *          Updated 2016/11/23
 *          検索日付のデフォルト設定を追加
 */
public class TrafficLogBLogic implements BLogic<TrafficLogInput> {

	// QueryDAOクラス
	private QueryDAO queryDAO = null;

	// メッセージクラス
	private MessageAccessor messageAccessor = null;

	// ログクラス
	private static Log log = LogFactory.getLog(TrafficLogBLogic.class);

	/**
	 * メソッド名 : トラフィックログ一覧取得処理
	 * 機能概要 : トラフィックログ一覧を取得
	 * @param param トラフィックログ一覧取得処理ビジネスロジックの入力を保持したMap
	 * @return トラフィックログ一覧取得処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(TrafficLogInput param) {

		log.debug("トラフィックログ検索");

		String returnStr = "failure";	// リータン文字列

		if(param == null){
			log.debug("インプットパラメータ：なし");
			param = new TrafficLogInput();
		}

		//共通セッション情報
		CusconUVO uvo = param.getUvo();

		TrafficLogOutput out = new TrafficLogOutput(param);		// アウトプット用パラメータのセット

		BLogicMessages errors = new BLogicMessages();			// エラーメッセージ
		List<TrafficLogRecord> recList = null; // ログ検索結果
		int totalCount = 0; //ログ該当件数

		// ページ制御パラメータ
		// 1P表示行数
		int line;
		if(param.getLine() == null) {
			line = 20;
		} else {
			line = Integer.parseInt(param.getLine());
		}

		// 表示開始レコード数
		int startIndex;
		if(param.getStartIndex() == null) {
			startIndex = 0;
		} else {
			startIndex = Integer.parseInt(param.getStartIndex());
		}
		log.debug(String.format("ページ制御パラメータ(表示行数：%d, 表示開始レコード:%d)", line, startIndex));

		// 検索条件設定
		if(param.getDataStrOld() != null && param.getDataStrOld().length() >= 0){
			// 旧検索条件文字列が設定された場合は表示件数のみの変更のため、前回正常に検索した際の
			// 検索条件で検索を行う必要があるので条件を入れ替える。
			param.loadParam();
		} else {
			// 旧検索条件文字列が設定されていない場合は通常の検索のため入力された
			// 検索条件文字列を旧検索条件として保持する。
			param.saveParam();
		}

		String dataStr = param.getDataStr();	// 検索条件文字列

		// 条件対象項目フラグ
		boolean sIpFlg = param.issIpFlg();		// SourceIP
		boolean dIpFlg = param.isdIpFlg();		// DestinationIP
		boolean sPortFlg = param.issPortFlg();	// SourcePort
		boolean dPortFlg = param.isdPortFlg();	// DestinationPort
		boolean appFlg = param.isAppFlg();		// Application

		//検索範囲日付
		String startDate = param.getStartDate();// 開始日付
		String endDate   = param.getEndDate();  // 終了日付

		int paramChkFlg1 = 0;	// パラメータチェックフラグ
		int paramChkFlg2 = 0;	// パラメータチェックフラグ

		SelectTrafficLog selectParam = new SelectTrafficLog();	// SQLパラメータ


		//セッション情報チェック
		log.debug("セッション情報チェック");
		if (uvo == null) {
			//セッション情報NG
			errors.add("message", new BLogicMessage("DK080101"));
			log.error(messageAccessor.getMessage("EK081101" ,null, param.getUvo()));
			return makeResult(out, returnStr, errors);
		}

		if (uvo.getVsysId() == null){
			//VSYS-ID NG
			errors.add("message", new BLogicMessage("DK080101"));
			log.error(messageAccessor.getMessage("EK081102",null, param.getUvo()));
			return makeResult(out, returnStr, errors);
		}
		StringBuffer chkBuff = new StringBuffer(uvo.getVsysId());
		if (chkBuff.length() == 0){
			//VSYS-ID NG
			errors.add("message", new BLogicMessage("DK080101"));
			log.error(messageAccessor.getMessage("EK081102",null, param.getUvo()));
			return makeResult(out, returnStr, errors);
		}

		//セションからVSYS-IDを取得
		selectParam.setVirtualSystem(uvo.getVsysId());

		// UPDATED: 2016.12 by Plum Systems Inc.
		// 当日の日付を設定
		Date todayDate = new Date();
		SimpleDateFormat todayFormat = new SimpleDateFormat("yyyy/MM/dd");
		String todayString = todayFormat.format(todayDate);

		//遷移元判定
		if(param.getTransitionFlg().equals(LogMonitorCommon.TR_MENU)) {
			//MENUから遷移した場合は検索せずに終了
			returnStr = "success";
			out.setTransitionFlg(LogMonitorCommon.TR_FORM);
			out.setRecList(recList);
			out.setLine(Integer.toString(line));
			out.setStartIndex("0");
			out.setTotalCount("0");

			// UPDATED: 2016.12 by Plum Systems Inc.
			// デフォルト値として当日を指定
			out.setStartDate(todayString);
			out.setEndDate(todayString);

			return makeResult(out, returnStr, errors);
		}
		out.setTransitionFlg(param.getTransitionFlg());//設定されている遷移元を出力へコピー

		// リクエスト解析
		// 条件のチェック
		// 対象項目の選択有無をチェックとフィルタ（SQL）条件の設定
		// 選択されていた場合はパラメータチェックフラグを加算

		log.debug(String.format("検索対象パラメータ(SourceIP:%b, DestIP:%b, SourcePort:%b, DestPort:%b, Application:%b)",
				sIpFlg, dIpFlg, sPortFlg, dPortFlg, appFlg)
			);
		log.debug(String.format("検索条件文字列パラメータ(%s)", dataStr));
		log.debug(String.format("検索条件範囲日付(開始日：%s, 終了日：%s)", startDate, endDate));

		if (dataStr != null && dataStr.length() > 0){
			paramChkFlg1++;
			dataStr =String.format("%%%s%%", dataStr);
		}

		if (sIpFlg == true){
			selectParam.setsIp(dataStr);
			paramChkFlg2++;
		}
		if (dIpFlg == true){
			selectParam.setdIp(dataStr);
			paramChkFlg2++;
		}
		if (sPortFlg == true){
			selectParam.setsPort(dataStr);
			paramChkFlg2++;
		}
		if (dPortFlg == true){
			selectParam.setdPort(dataStr);
			paramChkFlg2++;
		}
		if (appFlg == true){
			selectParam.setApp(dataStr);
			paramChkFlg2++;
		}

		//検索範囲の開始日を設定
		if (startDate.length() == 0){
			// UPDATED: 2016.12 by Plum Systems Inc.
			// 入力値がなければ当日を指定
			startDate = todayString;
		}

		//開始日に時間を追加 YYYY/MM/DD 00:00:00
		String startDateWithTime = startDate + " 00:00:00";

		selectParam.setStartDate(startDateWithTime);

		//検索範囲の終了日を設定
		if (endDate.length() == 0){
			// UPDATED: 2016.12 by Plum Systems Inc.
			// 入力値がなければ当日を指定
			endDate = todayString;
		}

		//終了日に時間を追加 YYYY/MM/DD 23:59:59
		String endDateWithTime = endDate + " 23:59:59";

		selectParam.setEndDate(endDateWithTime);


		// リクエストパラメータの正常判定
		//対象項目と絞り込み対象文字列が指定されていればOK
		if (paramChkFlg1 == 0 && paramChkFlg2 >= 1) {
			//リクエストパラメータNG
			//エラー処理を記述
			errors.add("message", new BLogicMessage("DK080102"));
			log.error(messageAccessor.getMessage("EK081103",null, param.getUvo()));
			return makeResult(out, returnStr, errors);
		}else if(paramChkFlg1 >= 1 && paramChkFlg2 == 0){
			//リクエストパラメータNG
			//エラー処理を記述
			errors.add("message", new BLogicMessage("DK080102"));
			log.error(messageAccessor.getMessage("EK081104",null, param.getUvo()));
			return makeResult(out, returnStr, errors);
		}

		// 開始、終了日付の時間関係をチェック
		if(LogMonitorCommon.chkDateParam(startDateWithTime, endDateWithTime) == false){
			// falseが返った場合はエラー処理
			errors.add("message", new BLogicMessage("DK080117"));
			log.error(messageAccessor.getMessage("EK081116", null, param.getUvo()));
			return makeResult(out, returnStr, errors);
		}

		// 検索対象項目が指定されない場合
		if(paramChkFlg2 == 0){
			// 全ての条件項目へ"%"を指定
			selectParam.setsIp("%");
			selectParam.setdIp("%");
			selectParam.setsPort("%");
			selectParam.setdPort("%");
			selectParam.setApp("%");
		}
		// 取得する件数と開始オフセットをパラメータで絞り込む
		selectParam.setStartIdx(startIndex);
		selectParam.setLine(line);

		// トラフィックログ検索
		try {
			// ログ検索
			log.debug("トラフィックログ検索SQL発行");
			recList = queryDAO.executeForObjectList("TrafficLogBLogic-1", selectParam);
			// ログ件数取得
			log.debug("トラフィックログ該当件数検索SQL発行");
			totalCount = queryDAO.executeForObject("TrafficLogBLogic-2", selectParam, Integer.class);
		} catch (Exception e) {
			//DBアクセスエラー
			//エラー処理を記述
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			errors.add("message", new BLogicMessage("DK080104"));
			String[] errMsg = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK081101",errMsg, param.getUvo()));
			return makeResult(out, returnStr, errors);
		}

		returnStr = "success";

		out.setRecList(recList);
		out.setLine(Integer.toString(line));
		out.setStartIndex(Integer.toString(startIndex));
		out.setTotalCount(Integer.toString(totalCount));

		// UPDATED: 2016.12 by Plum Systems Inc.
		// 出力パラメータに日付を指定
		out.setStartDate(startDate);
		out.setEndDate(endDate);

		return makeResult(out, returnStr, errors);
	}

	/**
	 * メソッド名 : レスポンスの生成
	 * 機能概要 : レスポンスデータの生成
	 * @param out レスポンス出力用のインタフェースクラス
	 * @param returnStr レスポンス文字列
	 * @param errors エラーメッセージ
	 * @return トラフィックログ一覧取得処理結果を保持したBLogicResult
	 */
	public BLogicResult makeResult(TrafficLogOutput out, String returnStr, BLogicMessages errors) {
		// レスポンス返却
		BLogicResult result = new BLogicResult();
		if(StringUtils.equals(returnStr, "failure") == true){
			log.debug("エラーレスポンス生成");
			result.setErrors(errors);
		}

		result.setResultString(returnStr);
		result.setResultObject(out);
		log.debug(String.format("トラフィックログ検索終了(状態：%s)", returnStr));

		return result;
	}

	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * 機能概要 : queryDAOを取得します。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
	    return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOを設定します。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
	    this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}


}
