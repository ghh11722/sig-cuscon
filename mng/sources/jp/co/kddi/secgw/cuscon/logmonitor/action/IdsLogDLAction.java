/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : IdsLogDLAction.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/09/03  m.narikawa@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.logmonitor.action;

import java.io.File;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.logmonitor.common.LogMonitorCommon;
import jp.co.kddi.secgw.cuscon.logmonitor.dto.IdsLogInput;
import jp.co.kddi.secgw.cuscon.logmonitor.dto.IdsLogOutput;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.web.struts.actions.DownloadByteArray;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.h2.util.StringUtils;

/**
 * クラス名 : IdsLogDLAction
 * 機能概要 :
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/09/03
 *          新規作成
 * @see
 */
public class IdsLogDLAction implements BLogic<IdsLogInput>{
	// DAO
//	private QueryDAO queryDAO = null;

	// メッセージクラス
	private MessageAccessor messageAccessor = null;

	// ログクラス
	private static Log log = LogFactory.getLog(IdsLogDLAction.class);

	/**
	 * メソッド名 :スレットログファイルダウンロード
	 * 機能概要 : セッションデータに保存されたパスのZipファイルを出力する
	 * @param param リクエストパラメータ
	 * @return ダウンロードオブジェクト
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(IdsLogInput param) {
		log.debug("スレットログダウンロード");

		//共通セッション情報
		CusconUVO uvo = param.getUvo();

		IdsLogOutput out = new IdsLogOutput(param);			// 画面表示のパラメータ保持
		String returnStr = "failure";						// リターン文字列
		BLogicMessages errors = new BLogicMessages();		// エラーメッセージ
		DownloadByteArray downloadObject = null; 			// ダウンロードデータ格納バッファ
		String zipName = param.getZipName();				// セッション情報から取得
		String zipFile = param.getZipFilePath();			// セッション情報から取得
		String tmpDir = param.getTmpDir();					// セッション情報から取得

		// セッション情報は一度読んだら不要なので削除
		param.setZipName(null);
		param.setZipFilePath(null);
		param.setTmpDir(null);

		while (true) {
			//セッション情報チェック
			log.debug("セッション情報チェック");
			if (uvo == null) {
				//セッション情報NG
				errors.add("message", new BLogicMessage("DK080205"));
				log.error(messageAccessor.getMessage("EK081205" ,null, param.getUvo()));
				break;
			}

			if (uvo.getVsysId() == null){
				//VSYS-ID NG
				errors.add("message", new BLogicMessage("DK080205"));
				log.error(messageAccessor.getMessage("EK081206",null, param.getUvo()));
				break;
			}
			StringBuffer chkBuff = new StringBuffer(uvo.getVsysId());
			if (chkBuff.length() == 0){
				//VSYS-ID NG
				errors.add("message", new BLogicMessage("DK080205"));
				log.error(messageAccessor.getMessage("EK081206",null, param.getUvo()));
				break;
			}

			if( (zipName == null) || (zipName.length()==0) ) {
				// Zipファイル名が指定されていない
				errors.add("message", new BLogicMessage("DK080205"));
				log.error(messageAccessor.getMessage("EK081212" ,null, param.getUvo()));
				break;
			}
			if( (zipFile == null) || (zipFile.length()==0) ) {
				// Zipファイルが保存されていない
				errors.add("message", new BLogicMessage("DK080205"));
				log.error(messageAccessor.getMessage("EK081213" ,null, param.getUvo()));
				break;
			}
			if( (tmpDir == null) || (tmpDir.length()==0) ) {
				// テンポラリディレクトリが保存されていない
				errors.add("message", new BLogicMessage("DK080205"));
				log.error(messageAccessor.getMessage("EK081214" ,null, param.getUvo()));
				break;
			}
			try {
				// ファイルの存在チェック
				File file = new File(zipFile);
				if ((file.exists() == false) || (file.isFile() == false)) {
					errors.add("message", new BLogicMessage("DK080214"));
					log.error(messageAccessor.getMessage("EK081215" ,null, param.getUvo()));
					break;
				}
			} catch (Exception e) {
				errors.add("message", new BLogicMessage("DK080214"));
				log.error(messageAccessor.getMessage("EK081215" ,null, param.getUvo()));
				break;
			}

			// ファイルをバイト配列に変換する
			byte[] byteArray = LogMonitorCommon.createByteArrayFromFile(zipFile);

			// ダウンロードデータをバッファへ格納する。
			downloadObject = new DownloadByteArray(zipName, byteArray);

			returnStr = "success";
			break;
		}

		// テンポラリディレクトリを削除する
		if(tmpDir != null){
			LogMonitorCommon.deleteDir(new File(tmpDir));
		}

		BLogicResult result = new BLogicResult();
		if(StringUtils.equals(returnStr, "failure") == true){
			result.setErrors(errors);
			result.setResultObject(out);
		}else{
			result.setResultObject(downloadObject);
		}
		result.setResultString(returnStr);

		log.debug(String.format("スレットログダウンロード終了(状態：%s)", returnStr));
		return result;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOを取得します。
	 * @return queryDAO DAO
	 */
//	public QueryDAO getQueryDAO() {
//	    return queryDAO;
//	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOを設定します。
	 * @param queryDAO DAO
	 */
//	public void setQueryDAO(QueryDAO queryDAO) {
//	    this.queryDAO = queryDAO;
//	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}
}
