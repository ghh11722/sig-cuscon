/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ZipCtr.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/03/04  T.Sutoh@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.logmonitor.common;


/*ZIPのライブラリ群
 *java.utilのzip使用時は(1),(2)の有効とし、(3),(4),(5)をコメントアウト
 *apache.toolsのzipを使用時は(3),(4),(5)を友好とし、(1),(2)をコメントアウト
 *　※(5)はcreateZip関数内にあります。
 *　※zipの形式を指定する必要がある場合は(3),(4),(5)を利用し、(5)へ形式を指定する
 */
import java.util.zip.ZipEntry;					//(1)
import java.util.zip.ZipOutputStream;			//(2)
// import org.apache.tools.zip.ZipEntry;		//(3)
// import org.apache.tools.zip.ZipOutputStream;	//(4)

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.BufferedInputStream;
import java.io.IOException;



/**
 * クラス名 : ZipCtr
 * 機能概要 : ファイルのZIP圧縮
 * 備考 :
 * @author T.Sutoh@JCCH
 * @version 1.0 T.Sutoh@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class ZipCtr {

	// Zipエントリから削除するパスの長さ
	private static int deleteLength;

	/**
	 * メソッド名 : Zipファイル生成
	 * 機能概要 : targetFilesをZIP圧縮してzipFileに出力する。
	 * @param zipFile ZIP出力ファイル名
	 * @param targetFiles 入力ファイル名(ディレクトリ)配列
	 * @throws IOException 入出力例外
	 */
	public static void CreateZipFile(String zipFile, String[] targetFiles) throws IOException {
		ZipOutputStream out =
		new ZipOutputStream(new FileOutputStream(zipFile));
		//out.setEncoding("MS932");//(5)

		// ターゲットファイルを読みZIP生成処理をコール
		for (int i = 0; i < targetFiles.length; i++) {
			File file = new File(targetFiles[i]);
			deleteLength = file.getPath().length() - file.getName().length();
			CreateZip(out, file);
		}
		out.close();
	}

	/**
	 * メソッド名 : ZIPデータの生成
	 * 機能概要 : targetFileをoutのZIPエントリへ出力する。
	 * @param out ZIP出力先
	 * @param targetFile 入力ファイル名(ディレクトリ)
	 * @throws IOException 入出力例外
	 */
	private static void CreateZip(ZipOutputStream out, File targetFile) throws IOException {
		// ディレクトリ判定
		if (targetFile.isDirectory()) {
			// ディレクトリの場合は配下のファイル処理を行う
			File[] files = targetFile.listFiles();
			for (int i = 0; i < files.length; i++) {
				CreateZip(out, files[i]);
			}
		} else {
			// ファイルの場合はZIPデータを生成
			ZipEntry target = new ZipEntry(getEntryPath(targetFile));
			out.putNextEntry(target);
			byte buf[] = new byte[1024];
			int count;
			BufferedInputStream in = new BufferedInputStream(new FileInputStream(targetFile));
			while ((count = in.read(buf, 0, 1024)) != -1) {
				out.write(buf, 0, count);
			}
			in.close();
			out.closeEntry();
		}
	}

	/**
	 * メソッド名 : ZIPデータの生成
	 * 機能概要 : ZIPエントリパスを返す。
	 * @param file ZIPエントリ対象ファイル
	 * @return ZIPエントリのパス
	 */
	private static String getEntryPath(File file) {
		return file.getPath().replaceAll("\\\\", "/").substring(deleteLength);
	}

}
