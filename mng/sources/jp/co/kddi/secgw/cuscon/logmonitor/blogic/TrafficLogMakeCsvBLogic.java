/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名	: TrafficLogMakeCsvBLogic.java
 *
 * [変更履歴]
 * 日付 	   更新者			内容
 * 2010/09/01  m.narikawa@JCCH	初版作成
 * 2010/09/03  m.narikawa@JCCH	レコードの処理をQueryRowHandleDAOで実装
 * 2010/12/07  t.sutoh@JCCH 	レコードの処理を外部のShellで行う実装に修正
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.logmonitor.blogic;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.logmonitor.common.LogMonitorCommon;
import jp.co.kddi.secgw.cuscon.logmonitor.dto.TrafficLogInput;
import jp.co.kddi.secgw.cuscon.logmonitor.dto.TrafficLogOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.util.PropertyUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.h2.util.StringUtils;

/**
 * クラス名 : TrafficLogMakeCsvBLogic
 * 機能概要 : サーバ上でCSVファイルを作成し、Zipファイルとしてアーカイブする。
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *			Created 2010/09/03
 *			新規作成
 * @version 1.1 t.sutoh@JCCH
 *			Created 2010/12/10
 *			SQL実行からZIP圧縮を外部のShellで行う実装に修正
 * @see
 */
public class TrafficLogMakeCsvBLogic implements BLogic<TrafficLogInput> {

	// DAO
	private QueryDAO queryDAO = null;

	// メッセージクラス
	private MessageAccessor messageAccessor = null;

	// ログクラス
	private static Log log = LogFactory.getLog(TrafficLogMakeCsvBLogic.class);

	/**
	 * メソッド名 : トラフィックログCSV作成処理
	 * 機能概要 : DBを検索し、トラフィックログのCSVファイルを作成し、
	 *			  Zip形式にてサーバに保存
	 * @param param
	 * @return
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(TrafficLogInput param) {

		log.debug("トラフィックログファイル作成");

		//共通セッション情報
		CusconUVO uvo = param.getUvo();

		TrafficLogOutput out = new TrafficLogOutput(param);		// 画面表示のパラメータ保持
		String returnStr = "failure";							// リターン文字列
		BLogicMessages errors = new BLogicMessages();			// エラーメッセージ
		String sqlPatern = null;								// 実行するSQLのパターン
		String tmpDir = null;									// テンポラリディレクトリ

		while (true) {
			//セッション情報チェック
			log.debug("セッション情報チェック");
			if (uvo == null) {
				//セッション情報NG
				errors.add("message", new BLogicMessage("DK080105"));
				log.error(messageAccessor.getMessage("EK081105" ,null, param.getUvo()));
				break;
			}

			if (uvo.getVsysId() == null){
				//VSYS-ID NG
				errors.add("message", new BLogicMessage("DK080105"));
				log.error(messageAccessor.getMessage("EK081106",null, param.getUvo()));
				break;
			}
			StringBuffer chkBuff = new StringBuffer(uvo.getVsysId());
			if (chkBuff.length() == 0){
				//VSYS-ID NG
				errors.add("message", new BLogicMessage("DK080105"));
				log.error(messageAccessor.getMessage("EK081106",null, param.getUvo()));
				break;
			}

			String sDate = param.getStartDate();
			String eDate = param.getEndDate();

			// 作業テンポラリの作成と取得
			tmpDir = LogMonitorCommon.makeTmp(uvo.getLoginId(), queryDAO, log, messageAccessor, uvo);
			if(tmpDir == null){
				// 失敗した場合はエラー
				errors.add("message", new BLogicMessage("DK080106"));
				log.error(messageAccessor.getMessage("EK081107",null, param.getUvo()));
				break;
			}

			// パラメータチェックも兼ねて、指定された日付パラメータに基づいてSQLのパターン決め
			log.debug(String.format("ダウンロード日付範囲パラメータ(開始日：%s, 終了日:%s)", sDate, eDate));
			if((sDate.length() > 0) && (eDate.length() > 0)){
				// 開始日、終了日ともに指定がある場合
				// 日付の時間関係をチェック
				//if(chkParam(sDate, eDate) == false){ 20110614 ktakenaka@PROSITE del
				if(LogMonitorCommon.chkDateParam(sDate, eDate) == false) {	// 20110614 ktakenaka@PROSITE add
					// falseが返った場合はエラー処理
					errors.add("message", new BLogicMessage("DK080109"));
					log.error(messageAccessor.getMessage("EK081108", null, param.getUvo()));
					break;
				}
				sqlPatern = "TrafficLogBLogic-3";
			} else if ((sDate.length() > 0) && (eDate.length() == 0)){
				// 開始日のみ指定
				sqlPatern = "TrafficLogBLogic-4";

			} else if ((sDate.length() == 0) && (eDate.length() > 0)){
				// 終了日のみ指定
				sqlPatern = "TrafficLogBLogic-5";
			} else if ((sDate.length() == 0) && (eDate.length() == 0)){
				// 日付指定なし
				sqlPatern = "TrafficLogBLogic-6";
			}

			if (sDate.length() > 0){
				//開始日に時間を追加 YYYY/MM/DD 00:00:00
				sDate = sDate + " 00:00:00";
			}

			if (eDate.length() > 0){
				//終了日に時間を追加 YYYY/MM/DD 23:59:59
				eDate = eDate + " 23:59:59";
			}

			// CSVファイル名生成
			Calendar cal = Calendar.getInstance();
			String tmpFile = String.format( "%s/traffic_%04d%02d%02d.csv",
											tmpDir,// テンポラリディレクトリ
											cal.get(Calendar.YEAR),	// 年
											cal.get(Calendar.MONTH)+1,// 月
											cal.get(Calendar.DATE)	// 日
										);

			// ZIPファイル名生成
			String zipName = String.format( "traffic_%04d%02d%02d.zip",
											cal.get(Calendar.YEAR),	// 年
											cal.get(Calendar.MONTH)+1,// 月
											cal.get(Calendar.DATE)	// 日
										);
			String zipFile = String.format("%s/%s", tmpDir, zipName, param.getUvo());

			// CSV/ZIPファイルを生成する外部プロセスを起動
			ProcessBuilder pb = new ProcessBuilder(PropertyUtil.getProperty("tlogdl.csv.shell"), uvo.getVsysId(), sDate, eDate, tmpFile, zipFile, sqlPatern);
			try {
				Process p = pb.start();
				int ret = p.waitFor();
				System.out.println("process exited with value : " + ret);
				if(ret == 1){
					//shell内でSQLのエラー発生
					errors.add("message", new BLogicMessage("DK080111"));
					String[] errMsg = {"該当件数が0件またはcsv作成shellファイル内でSQLエラーが発生しました。"};
					//log.fatal(messageAccessor.getMessage("FK081102",errMsg, param.getUvo()));
					log.error(messageAccessor.getMessage("FK081102",errMsg, param.getUvo()));
					break;
				}else if(ret == 2){
					//shell内でZIP圧縮のエラー発生
					errors.add("message", new BLogicMessage("DK080113"));
					String[] errMsg = {zipFile};
					log.error(messageAccessor.getMessage("EK081111", errMsg, param.getUvo()));
					break;
				}

			} catch (IOException e) {
				// start()で例外が発生
				e.printStackTrace();
				errors.add("message", new BLogicMessage("DK080112"));
				String[] errMsg = {tmpFile};
				log.fatal(messageAccessor.getMessage("EK081110",errMsg, param.getUvo()));
				break;
			} catch (InterruptedException e) {
				// waitFor()で例外が発生
				e.printStackTrace();
				errors.add("message", new BLogicMessage("DK080112"));
				String[] errMsg = {tmpFile};
				log.fatal(messageAccessor.getMessage("EK081110",errMsg, param.getUvo()));
				break;
			}

			out.setTmpDir(tmpDir);
			out.setZipName(zipName);
			out.setZipFilePath(zipFile);
			returnStr = "success";
			break;
		}

		// エラーが発生していたら、テンポラリディレクトリを削除する
		if(returnStr.compareTo("success") != 0){
			if(tmpDir != null){
				LogMonitorCommon.deleteDir(new File(tmpDir));
			}
		}

		log.debug(String.format("トラフィックログファイル作成終了(状態：%s)", returnStr));
		return makeResult(out, returnStr, errors);
	}

	/**
	 * メソッド名 : レスポンスの生成
	 * 機能概要 : レスポンスデータの生成
	 * @param out レスポンス出力用のインタフェースクラス
	 * @param returnStr レスポンス文字列
	 * @param errors エラーメッセージ
	 * @return トラフィックログ一覧取得処理結果を保持したBLogicResult
	 */
	public BLogicResult makeResult(TrafficLogOutput out, String returnStr, BLogicMessages errors) {
		// レスポンス返却
		BLogicResult result = new BLogicResult();
		if(StringUtils.equals(returnStr, "failure") == true){
			log.debug("エラーレスポンス生成");
			result.setErrors(errors);
		}

		result.setResultString(returnStr);
		result.setResultObject(out);
		log.debug(String.format("トラフィックログ検索終了(状態：%s)", returnStr));

		return result;
	}

	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * 機能概要 : queryDAOを取得します。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOを設定します。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}

// 20110614 ktakenkaa@PROSITE del start
//	/**
//	 * メソッド名 :chkParam
//	 * 機能概要 : 開始日、終了日チェック
//	 * @param sDate : 開始日
//	 * @param eDate : 終了日
//	 * @return 正常：true、異常：false
//	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
//	 */
//	private boolean chkParam(String sDate, String eDate){
//
//		//開始日、終了日を比較
//		Date start = null;
//		Date end = null;
//
//		try {
//			start = DateFormat.getDateInstance().parse(sDate);
//			end = DateFormat.getDateInstance().parse(eDate);
//
//		} catch (ParseException e) {
//			e.printStackTrace();
//			return false;
//		}
//
//		if(start.after(end) == true){
//			//終了日が開始日以前の日付ならfalseをリターン
//			return false;
//		}
//
//		return true;
//	}
// 20110614 ktakenkaa@PROSITE del end

}
