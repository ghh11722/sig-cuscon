/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : IdsLogInput.java
 *
 * [変更履歴]
 * 日付        更新者             内容
 * 2010/03/04  T.Sutoh@JCCH       初版作成
 * 2010/09/06  m.narikawa@JCCH    ダウンロード方式の変更に伴うパラメータ追加
 * 2011/05/23  ktakenaka@PROSITE  サブタイプフラグを追加
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.logmonitor.dto;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.logmonitor.common.LogMonitorCommon;

/**
 * クラス名 : IdsLogInput
 * 機能概要 : IDS/IPSログ取得処理ビジネスロジックの入力データ定義
 * 備考 :
 * @author T.Sutoh@JCCH
 * @version 1.0 T.Sutoh@JCCH
 *          Created 2010/03/04
 *          新規作成
 * @version 1.1 m.narikawa@JCCH
 *          Created 2010/09/06
 *          セッションからZipファイルダウンロード用のデータを入出力するように
 *          パラメータを追加
 */
public class IdsLogInput {
	// Application
	private boolean appFlg = false;

	// 絞り込み条件
	private String dataStr = null;

	// DestinationID
	private boolean dIpFlg = false;

	// DestinationPort
	private boolean dPortFlg = false;

	// ページ表示行数
	private String line;

	// 表示開始レコード行
	private String startIndex;

	// SourceID
	private boolean sIpFlg = false;

	// SourcePort
	private boolean sPortFlg = false;

	// 共通セッション情報
	private CusconUVO uvo = null;

	// ダウンロード条件日付
	// 開始日
	private String startDate;
	// 終了日
	private String endDate;

	// Application（前回の検索条件）
	private boolean appFlgOld;

	// DestinationID（前回の検索条件）
	private boolean dIpFlgOld;

	// DestinationPort（前回の検索条件）
	private boolean dPortFlgOld;

	// SourceID（前回の検索条件）
	private boolean sIpFlgOld;

	// SourcePort（前回の検索条件）
	private boolean sPortFlgOld;

	// 絞り込み条件（前回の検索条件）
	private String dataStrOld;

	// Zipファイルのサーバー上のパス名
	private String zipFilePath = null;

	// Zipファイルのダウンロード時のファイル名
	private String zipName = null;

	// テンポラリディレクトリパス
	private String tmpDir = null;

	// 遷移フラグ
	private String transitionFlg = LogMonitorCommon.TR_MENU;

	// 20110523 ktakenkaa@PROSITE add start
	// サブタイプフラグ
	private int subtypeFlg = 0;
	// 20110523 ktakenkaa@PROSITE add end

	/**
	 * メソッド名 : 検索条件の保存
	 * 機能概要 : 検索条件前回の検索条件として保存します。
	 */
	public void saveParam() {

		this.setsIpFlgOld(sIpFlg);
		this.setdIpFlgOld(dIpFlg);
		this.setsPortFlgOld(sPortFlg);
		this.setdPortFlgOld(dPortFlg);
		this.setAppFlgOld(appFlg);
		this.setDataStrOld(dataStr);

		return;
	}

	/**
	 * メソッド名 : 前回の検索条件を設定
	 * 機能概要 : 前回の検索条件を検索条件としてロードします。
	 */
	public void loadParam() {

		this.setsIpFlg(sIpFlgOld);
		this.setdIpFlg(dIpFlgOld);
		this.setsPortFlg(sPortFlgOld);
		this.setdPortFlg(dPortFlgOld);
		this.setAppFlg(appFlgOld);
		this.setDataStr(dataStrOld);

		return;
	}


	/**
	 * メソッド名 : appFlgのGetterメソッド
	 * 機能概要 : appFlgを取得します。
	 * @return appFlg Application
	 */
	public boolean isAppFlg() {
	    return appFlg;
	}

	/**
	 * メソッド名 : appFlgのSetterメソッド
	 * 機能概要 : appFlgを設定します。
	 * @param appFlg Application
	 */
	public void setAppFlg(boolean appFlg) {
	    this.appFlg = appFlg;
	}

	/**
	 * メソッド名 : dataStrのGetterメソッド
	 * 機能概要 : dataStrを取得します。
	 * @return dataStr 絞り込み条件
	 */
	public String getDataStr() {
	    return dataStr;
	}

	/**
	 * メソッド名 : dataStrのSetterメソッド
	 * 機能概要 : dataStrを設定します。
	 * @param dataStr 絞り込み条件
	 */
	public void setDataStr(String dataStr) {
	    this.dataStr = dataStr;
	}

	/**
	 * メソッド名 : dIpFlgのGetterメソッド
	 * 機能概要 : dIpFlgを取得します。
	 * @return dIpFlg DestinationID
	 */
	public boolean isdIpFlg() {
	    return dIpFlg;
	}

	/**
	 * メソッド名 : dIpFlgのSetterメソッド
	 * 機能概要 : dIpFlgを設定します。
	 * @param dIpFlg DestinationID
	 */
	public void setdIpFlg(boolean dIpFlg) {
	    this.dIpFlg = dIpFlg;
	}

	/**
	 * メソッド名 : dPortFlgのGetterメソッド
	 * 機能概要 : dPortFlgを取得します。
	 * @return dPortFlg DestinationPort
	 */
	public boolean isdPortFlg() {
	    return dPortFlg;
	}

	/**
	 * メソッド名 : dPortFlgのSetterメソッド
	 * 機能概要 : dPortFlgを設定します。
	 * @param dPortFlg DestinationPort
	 */
	public void setdPortFlg(boolean dPortFlg) {
	    this.dPortFlg = dPortFlg;
	}

	/**
	 * メソッド名 : lineのGetterメソッド
	 * 機能概要 : lineを取得します。
	 * @return line ページ表示行数
	 */
	public String getLine() {
	    return line;
	}

	/**
	 * メソッド名 : lineのSetterメソッド
	 * 機能概要 : lineを設定します。
	 * @param line ページ表示行数
	 */
	public void setLine(String line) {
	    this.line = line;
	}

	/**
	 * メソッド名 : startIndexのGetterメソッド
	 * 機能概要 : startIndexを取得します。
	 * @return startIndex 表示開始レコード行
	 */
	public String getStartIndex() {
	    return startIndex;
	}

	/**
	 * メソッド名 : startIndexのSetterメソッド
	 * 機能概要 : startIndexを設定します。
	 * @param startIndex 表示開始レコード行
	 */
	public void setStartIndex(String startIndex) {
	    this.startIndex = startIndex;
	}

	/**
	 * メソッド名 : sIpFlgのGetterメソッド
	 * 機能概要 : sIpFlgを取得します。
	 * @return sIpFlg SourceID
	 */
	public boolean issIpFlg() {
	    return sIpFlg;
	}

	/**
	 * メソッド名 : sIpFlgのSetterメソッド
	 * 機能概要 : sIpFlgを設定します。
	 * @param sIpFlg SourceID
	 */
	public void setsIpFlg(boolean sIpFlg) {
	    this.sIpFlg = sIpFlg;
	}

	/**
	 * メソッド名 : sPortFlgのGetterメソッド
	 * 機能概要 : sPortFlgを取得します。
	 * @return sPortFlg SourcePort
	 */
	public boolean issPortFlg() {
	    return sPortFlg;
	}

	/**
	 * メソッド名 : sPortFlgのSetterメソッド
	 * 機能概要 : sPortFlgを設定します。
	 * @param sPortFlg SourcePort
	 */
	public void setsPortFlg(boolean sPortFlg) {
	    this.sPortFlg = sPortFlg;
	}

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得します。
	 * @return uvo 共通セッション情報
	 */
	public CusconUVO getUvo() {
	    return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoを設定します。
	 * @param uvo 共通セッション情報
	 */
	public void setUvo(CusconUVO uvo) {
	    this.uvo = uvo;
	}

	/**
	 * メソッド名 : startDateのSetterメソッド
	 * 機能概要 : startDateをセットする。
	 * @param startDate ダウンロード条件日付:開始日
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * メソッド名 : startDateのGetterメソッド
	 * 機能概要 : startDateを取得する。
	 * @return startDate ダウンロード条件日付:開始日
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * メソッド名 : endDateのSetterメソッド
	 * 機能概要 : endDateをセットする。
	 * @param endDate ダウンロード条件日付:終了日
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * メソッド名 : endDateのGetterメソッド
	 * 機能概要 : endDateを取得する。
	 * @return endDate ダウンロード条件日付:終了日
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * メソッド名 : appFlgOldのSetterメソッド
	 * 機能概要 : appFlgOldをセットする。
	 * @param appFlgOld Application（前回の検索条件）
	 */
	public void setAppFlgOld(boolean appFlgOld) {
		this.appFlgOld = appFlgOld;
	}

	/**
	 * メソッド名 : appFlgOldのGetterメソッド
	 * 機能概要 : appFlgOldを取得する。
	 * @return appFlgOld Application（前回の検索条件）
	 */
	public boolean isAppFlgOld() {
		return appFlgOld;
	}

	/**
	 * メソッド名 : dIpFlgOldのSetterメソッド
	 * 機能概要 : dIpFlgOldをセットする。
	 * @param dIpFlgOld DestinationPort（前回の検索条件）
	 */
	public void setdIpFlgOld(boolean dIpFlgOld) {
		this.dIpFlgOld = dIpFlgOld;
	}

	/**
	 * メソッド名 : dIpFlgOldのGetterメソッド
	 * 機能概要 : dIpFlgOldを取得する。
	 * @return dIpFlgOld DestinationPort（前回の検索条件）
	 */
	public boolean isdIpFlgOld() {
		return dIpFlgOld;
	}

	/**
	 * メソッド名 : dPortFlgOldのSetterメソッド
	 * 機能概要 : dPortFlgOldをセットする。
	 * @param dPortFlgOld DestinationPort（前回の検索条件）
	 */
	public void setdPortFlgOld(boolean dPortFlgOld) {
		this.dPortFlgOld = dPortFlgOld;
	}

	/**
	 * メソッド名 : dPortFlgOldのGetterメソッド
	 * 機能概要 : dPortFlgOldを取得する。
	 * @return dPortFlgOld DestinationPort（前回の検索条件）
	 */
	public boolean isdPortFlgOld() {
		return dPortFlgOld;
	}

	/**
	 * メソッド名 : sIpFlgOldのSetterメソッド
	 * 機能概要 : sIpFlgOldをセットする。
	 * @param sIpFlgOld SourceID（前回の検索条件）
	 */
	public void setsIpFlgOld(boolean sIpFlgOld) {
		this.sIpFlgOld = sIpFlgOld;
	}

	/**
	 * メソッド名 : sIpFlgOldのGetterメソッド
	 * 機能概要 : sIpFlgOldを取得する。
	 * @return sIpFlgOld SourceID（前回の検索条件）
	 */
	public boolean issIpFlgOld() {
		return sIpFlgOld;
	}

	/**
	 * メソッド名 : sPortFlgOldのSetterメソッド
	 * 機能概要 : sPortFlgOldをセットする。
	 * @param sPortFlgOld SourcePort（前回の検索条件）
	 */
	public void setsPortFlgOld(boolean sPortFlgOld) {
		this.sPortFlgOld = sPortFlgOld;
	}

	/**
	 * メソッド名 : sPortFlgOldのGetterメソッド
	 * 機能概要 : sPortFlgOldを取得する。
	 * @return sPortFlgOld SourcePort（前回の検索条件）
	 */
	public boolean issPortFlgOld() {
		return sPortFlgOld;
	}

	/**
	 * メソッド名 : dataStrOldのSetterメソッド
	 * 機能概要 : dataStrOldをセットする。
	 * @param dataStrOld 絞り込み条件（前回の検索条件）
	 */
	public void setDataStrOld(String dataStrOld) {
		this.dataStrOld = dataStrOld;
	}

	/**
	 * メソッド名 : dataStrOldのGetterメソッド
	 * 機能概要 : dataStrOldを取得する。
	 * @return dataStrOld 絞り込み条件（前回の検索条件）
	 */
	public String getDataStrOld() {
		return dataStrOld;
	}

	/**
	 * メソッド名 : zipFilePathのSetterメソッド
	 * 機能概要 : zipFilePathをセットする。
	 * @param zipFilePath Zipファイルのサーバー上のパス名
	 */
	public void setZipFilePath(String zipFilePath) {
		this.zipFilePath = zipFilePath;
	}

	/**
	 * メソッド名 : zipFilePathのGetterメソッド
	 * 機能概要 : zipFilePathを取得する。
	 * @return zipFilePath Zipファイルのサーバー上のパス名
	 */
	public String getZipFilePath() {
		return zipFilePath;
	}

	/**
	 * メソッド名 : zipNameのSetterメソッド
	 * 機能概要 : zipNameをセットする。
	 * @param zipName Zipファイルのダウンロード時のファイル名
	 */
	public void setZipName(String zipName) {
		this.zipName = zipName;
	}

	/**
	 * メソッド名 : zipNameのGetterメソッド
	 * 機能概要 : zipNameを取得する。
	 * @return zipName Zipファイルのダウンロード時のファイル名
	 */
	public String getZipName() {
		return zipName;
	}

	/**
	 * メソッド名 : tmpDirのSetterメソッド
	 * 機能概要 : tmpDirをセットする。
	 * @param tmpDir テンポラリディレクトリパス
	 */
	public void setTmpDir(String tmpDir) {
		this.tmpDir = tmpDir;
	}

	/**
	 * メソッド名 : tmpDirのGetterメソッド
	 * 機能概要 : tmpDirを取得する。
	 * @return tmpDir テンポラリディレクトリパス
	 */
	public String getTmpDir() {
		return tmpDir;
	}

	/**
	 * メソッド名 : transitionFlgのSetterメソッド
	 * 機能概要 : transitionFlgを設定します。
	 * @param flg
	 */
	public void setTransitionFlg(String flg) {
		this.transitionFlg = flg;
	}

	/**
	 * メソッド名 : transitionFlgのGetterメソッド
	 * 機能概要 : transitionFlgを取得します。
	 * @return transitionFlg
	 */
	public String getTransitionFlg() {
		return transitionFlg;
	}

	// 20110523 ktakenaka@PROSITE add start
	/**
	 * メソッド名 : subtypeFlgのSetterメソッド
	 * 機能概要 : subtypeFlgをセットする。
	 * @param subtypeFlg サブタイプフラグ
	 */
	public void setSubtypeFlg(int subtype) {
		this.subtypeFlg = subtype;
	}

	/**
	 * メソッド名 : subtypeFlgのGetterメソッド
	 * 機能概要 : subtypeFlgを取得する。
	 * @return subtypeFlg サブタイプフラグ
	 */
	public int getSubtypeFlg() {
		return subtypeFlg;
	}
	// 20110523 ktakenaka@PROSITE add end
}
