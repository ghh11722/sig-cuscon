/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : TrafficLogRecord.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/02/05  T.Sutoh@JCCH  初版作成
 * 2010/02/24  T.Sutoh@JCCH  typeの項目を追加
 * 2010/03/26  T.Sutoh@JCCH  ToUser,FromUserの削除
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.logmonitor.vo;

/**
 * クラス名 : TrafficLogRecord
 * 機能概要 :
 * 備考 :
 * @author T.Sutoh@JCCH
 * @version 1.0 T.Sutoh@JCCH
 *          Created 2010/02/05
 *          新規作成
 * @version 1.1 T.Sutoh@JCCH
 *          Created 2010/02/24
 *          新規作成
 * @version 1.2 T.Sutoh@JCCH
 *          Created 2010/03/26
 *          ToUser,FromUserの削除
 */
public class TrafficLogRecord {

	// ログ一覧出力項目
	private String action;
	private String application;
	private String bytes;
	private String type;
	private String destinationPort;
	private String destinationZone;
	private String destIp;
	private String logId;
	private String protocol;
	private String reciveTime;
	private String ruleName;
	private String sourceIp;
	private String sourcePort;
	private String sourceZone;
	private String virtualSystem;

	/**
	 * メソッド名 : actionのSetterメソッド
	 * 機能概要 : actionをセットする。
	 * @param action action
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * メソッド名 : actionのGetterメソッド
	 * 機能概要 : actionを取得する。
	 * @return action action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * メソッド名 : applicationのSetterメソッド
	 * 機能概要 : applicationをセットする。
	 * @param application application
	 */
	public void setApplication(String application) {
		this.application = application;
	}

	/**
	 * メソッド名 : applicationのGetterメソッド
	 * 機能概要 : applicationを取得する。
	 * @return application application
	 */
	public String getApplication() {
		return application;
	}

	/**
	 * メソッド名 : bytesのSetterメソッド
	 * 機能概要 : bytesをセットする。
	 * @param bytes bytes
	 */
	public void setBytes(String bytes) {
		this.bytes = bytes;
	}

	/**
	 * メソッド名 : bytesのGetterメソッド
	 * 機能概要 : bytesを取得する。
	 * @return bytes bytes
	 */
	public String getBytes() {
		return bytes;
	}

	/**
	 * メソッド名 : destinationPortのSetterメソッド
	 * 機能概要 : destinationPortをセットする。
	 * @param destinationPort destinationPort
	 */
	public void setDestinationPort(String destinationPort) {
		this.destinationPort = destinationPort;
	}

	/**
	 * メソッド名 : destinationPortのGetterメソッド
	 * 機能概要 : destinationPortを取得する。
	 * @return destinationPort destinationPort
	 */
	public String getDestinationPort() {
		return destinationPort;
	}

	/**
	 * メソッド名 : destinationZoneのSetterメソッド
	 * 機能概要 : destinationZoneをセットする。
	 * @param destinationZone destinationZone
	 */
	public void setDestinationZone(String destinationZone) {
		this.destinationZone = destinationZone;
	}

	/**
	 * メソッド名 : destinationZoneのGetterメソッド
	 * 機能概要 : destinationZoneを取得する。
	 * @return destinationZone destinationZone
	 */
	public String getDestinationZone() {
		return destinationZone;
	}

	/**
	 * メソッド名 : destIpのSetterメソッド
	 * 機能概要 : destIpをセットする。
	 * @param destIp destIp
	 */
	public void setDestIp(String destIp) {
		this.destIp = destIp;
	}

	/**
	 * メソッド名 : destIpのGetterメソッド
	 * 機能概要 : destIpを取得する。
	 * @return destIp destIp
	 */
	public String getDestIp() {
		return destIp;
	}

	/**
	 * メソッド名 : logIdのSetterメソッド
	 * 機能概要 : logIdをセットする。
	 * @param logId logId
	 */
	public void setLogId(String logId) {
		this.logId = logId;
	}

	/**
	 * メソッド名 : logIdのGetterメソッド
	 * 機能概要 : logIdを取得する。
	 * @return logId logId
	 */
	public String getLogId() {
		return logId;
	}

	/**
	 * メソッド名 : protocolのSetterメソッド
	 * 機能概要 : protocolをセットする。
	 * @param protocol protocol
	 */
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	/**
	 * メソッド名 : protocolのGetterメソッド
	 * 機能概要 : protocolを取得する。
	 * @return protocol protocol
	 */
	public String getProtocol() {
		return protocol;
	}

	/**
	 * メソッド名 : reciveTimeのSetterメソッド
	 * 機能概要 : reciveTimeをセットする。
	 * @param reciveTime reciveTime
	 */
	public void setReciveTime(String reciveTime) {
		this.reciveTime = reciveTime;
	}

	/**
	 * メソッド名 : reciveTimeのGetterメソッド
	 * 機能概要 : reciveTimeを取得する。
	 * @return reciveTime reciveTime
	 */
	public String getReciveTime() {
		return reciveTime;
	}

	/**
	 * メソッド名 : ruleNameのSetterメソッド
	 * 機能概要 : ruleNameをセットする。
	 * @param ruleName ruleName
	 */
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	/**
	 * メソッド名 : ruleNameのGetterメソッド
	 * 機能概要 : ruleNameを取得する。
	 * @return ruleName ruleName
	 */
	public String getRuleName() {
		return ruleName;
	}

	/**
	 * メソッド名 : sourceIpのSetterメソッド
	 * 機能概要 : sourceIpをセットする。
	 * @param sourceIp sourceIp
	 */
	public void setSourceIp(String sourceIp) {
		this.sourceIp = sourceIp;
	}

	/**
	 * メソッド名 : sourceIpのGetterメソッド
	 * 機能概要 : sourceIpを取得する。
	 * @return sourceIp sourceIp
	 */
	public String getSourceIp() {
		return sourceIp;
	}

	/**
	 * メソッド名 : sourcePortのSetterメソッド
	 * 機能概要 : sourcePortをセットする。
	 * @param sourcePort sourcePort
	 */
	public void setSourcePort(String sourcePort) {
		this.sourcePort = sourcePort;
	}

	/**
	 * メソッド名 : sourcePortのGetterメソッド
	 * 機能概要 : sourcePortを取得する。
	 * @return sourcePort sourcePort
	 */
	public String getSourcePort() {
		return sourcePort;
	}

	/**
	 * メソッド名 : sourceZoneのSetterメソッド
	 * 機能概要 : sourceZoneをセットする。
	 * @param sourceZone sourceZone
	 */
	public void setSourceZone(String sourceZone) {
		this.sourceZone = sourceZone;
	}

	/**
	 * メソッド名 : sourceZoneのGetterメソッド
	 * 機能概要 : sourceZoneを取得する。
	 * @return sourceZone sourceZone
	 */
	public String getSourceZone() {
		return sourceZone;
	}

	/**
	 * メソッド名 : virtualSystemのSetterメソッド
	 * 機能概要 : virtualSystemをセットする。
	 * @param virtualSystem virtualSystem
	 */
	public void setVirtualSystem(String virtualSystem) {
		this.virtualSystem = virtualSystem;
	}

	/**
	 * メソッド名 : virtualSystemのGetterメソッド
	 * 機能概要 : virtualSystemを取得する。
	 * @return virtualSystem virtualSystem
	 */
	public String getVirtualSystem() {
		return virtualSystem;
	}

	/**
	 * メソッド名 : typeのSetterメソッド
	 * 機能概要 : typeをセットする。
	 * @param type type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * メソッド名 : typeのGetterメソッド
	 * 機能概要 : typeを取得する。
	 * @return type type
	 */
	public String getType() {
		return type;
	}

}
