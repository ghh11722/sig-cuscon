/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : LogMonitorCommon.java
 *
 * [変更履歴]
 * 日付        更新者             内容
 * 2010/03/04  T.Sutoh@JCCH       初版作成
 * 2011/05/24  ktakenaka@PROSITE  プロパティの取得キー名(サブタイプ)を追加、日付変換でパース失敗する現象修正
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.logmonitor.common;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.logmonitor.vo.MSystemConstant;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

import org.apache.commons.logging.Log;


/**
 * クラス名 : LogMonitorCommon
 * 機能概要 : ログモニタ共通処理
 * 備考 :
 * @author T.Sutoh@JCCH
 * @version 1.0 T.Sutoh@JCCH
 *          Created 2010/03/04
 *          新規作成
 * @version 1.1 T.Sutoh@JCCH
 *          Created 2011/02/09
 *          chkDateParam関数を追加
 */
public class LogMonitorCommon {

	//ログ一覧画面への遷移元の定義
	public static String TR_MENU = "MENU";//メニュー
	public static String TR_FORM = "FORM";//一覧フォームから遷移

	//20110524 ktakenaka@PROSITE add start
	/** プロパティの取得キー名(サブタイプ) */
	public static String PROPERTIS_KEY_SUBTYPE = "logmonitor.subtype.";
	//20110524 ktakenaka@PROSITE add end

	/**
	 * メソッド名 :作業用ディレクトリ作成
	 * 機能概要 : ログインID毎のテンポラリディレクトリの作成
	 * @param loginId テンポラリ利用ログインID
	 * @param queryDAO QueryDAOクラス
	 * @param log logクラス
	 * @param messageAccessor messageAccessorクラス
	 * @param uvo uvo(カスコンUVOクラス)
	 * @return テンポラリパス
	 */
	public static String makeTmp(String loginId, QueryDAO queryDAO,
		Log log, MessageAccessor messageAccessor, CusconUVO uvo){

		log.debug("作業用ディレクトリ作成");

		// ベースディレクトを検索
		List<MSystemConstant> retList;
		String baseDir = null;

		try {
			log.debug("作業用ディレクトリのルート取得");
			retList = queryDAO.executeForObjectList("LogMonitorCommon-1", null);
			if(retList.size() > 0){
				MSystemConstant obj = retList.get(0);
				baseDir = obj.getWorkTmp();

				//ディレクトリの存在確認
				File file = new File(baseDir);
				if( file.exists() == false ){
					String[] errMsgAry = {baseDir};
					log.fatal(messageAccessor.getMessage("FK081002" ,errMsgAry, uvo));
					return null;
				}
			}else{
				log.fatal(messageAccessor.getMessage("FK081001" ,null, uvo));
				return null;
			}
		} catch (Exception e) {
			//DBアクセスエラー
			//エラー処理を記述
			log.fatal(messageAccessor.getMessage("FK081001" ,null, uvo));
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			return null;
		}

		String rootTmp = String.format("%s/logmonitor", baseDir);
		// サブディレクトリ作成

		File mkTmp = new File(rootTmp);
		if(mkTmp.isDirectory() == false){
			// ディレクトリが存在しなければ作成
			log.debug(String.format("ログモニタリングサブディレクトリの作成(ディレクトリ：%s)", rootTmp));
			if (mkTmp.mkdir() == false){
				// 作成失敗時はエラー
				String[] errMsgAry = {rootTmp};
				log.error(messageAccessor.getMessage("EK081003" ,errMsgAry, uvo));
				return null;
			}
		}

		// 作業テンポラリの作成（ログインID毎の一時テンポラリ）
		Calendar cal = Calendar.getInstance();
		String dateTime = String.format("%04d%02d%02d%02d%02d%02d",
			cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DATE),
			cal.get(Calendar.HOUR),cal.get(Calendar.MINUTE),cal.get(Calendar.SECOND)
		);

		String tmpStr = String.format("%s/%s_%s", rootTmp, loginId, dateTime);
		mkTmp = new File(tmpStr);
		if(mkTmp.isDirectory() == false){
			// ディレクトリが存在しなければ作成
			log.debug(String.format("ダウンロードファイル作成用のサブディレクトリ作成(ディレクトリ：%s)", tmpStr));
			if (mkTmp.mkdir() == false){
				String[] errMsgAry = {tmpStr};
				log.error(messageAccessor.getMessage("EK081004" ,errMsgAry, uvo));
				return null;
			}
		}

		return tmpStr;
	}


	/**
	 * メソッド名 :ダウンロード用Byte配列作成。
	 * 機能概要 : FileをByte配列に変換する。
	 * @param fileName ファイル名
	 * @return Byte配列
	 */
	public static byte[] createByteArrayFromFile(String fileName){
		// ファイルをバイト配列に変換する
		InputStream fis = null;
		byte[] byteArray = null;
		try {
			File file = new File(fileName);

			fis = new FileInputStream(file);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte[] b = new byte[1024];
			int j;
			while ((j = fis.read(b)) != -1) {
				baos.write(b, 0, j);
			}
			byteArray = baos.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return byteArray;
	}


	/**
	 * メソッド名 :ディレクトリ削除
	 * 機能概要 : ディレクトリ配下のファイル、ディレクトリをすべて削除後に自身のディレクトリも削除
	 *            fileへファイルを指定した場合はそのファイルを削除
	 * @param file : 削除ディレクトリのFileオブジェクト
	 */
	static public void deleteDir(File file){
		if( file.exists() == false ){
			return ;
		}

		if(file.isFile()){
			file.delete();
		}

		if(file.isDirectory()){
			File[] files = file.listFiles();
			for(int i=0; i<files.length; i++){
				deleteDir( files[i] );
			}
			file.delete();
		}
	}

	/**
	 * メソッド名 :chkDateParam
	 * 機能概要 : 開始日、終了日チェック
	 * @param sDate : 開始日
	 * @param eDate : 終了日
	 * @return 正常：true、異常：false
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	static public boolean chkDateParam(String sDate, String eDate){

		//開始日、終了日を比較
		Date start = null;
		Date end = null;

		try {
			// 20110524 ktakenaka@PROSITE del start
			//start = DateFormat.getDateInstance().parse(sDate);
			//end = DateFormat.getDateInstance().parse(eDate);
			//20110524 ktakenaka@PROSITE del end
			// 20110524 ktakenaka@PROSITE add start
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			start = sdf.parse(sDate);
			end = sdf.parse(eDate);
			// 20110524 ktakenaka@PROSITE add end

		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}

		if(start.after(end) == true){
			//終了日が開始日以前の日付ならfalseをリターン
			return false;
		}

		return true;
	}

}
