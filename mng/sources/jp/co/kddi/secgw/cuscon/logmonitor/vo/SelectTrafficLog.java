/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectTrafficLog.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/02/06  T.Sutoh@JCCH  初版作成
 * 2010/02/19  T.Sutoh@JCCH  ログダウンロード用の開始日、終了日を追加
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.logmonitor.vo;

import java.io.Serializable;

/**
 * クラス名 : SelectTrafficLog
 * 機能概要 : トラフィックログ一覧取得条件
 * 備考 :
 * @author T.Sutoh@JCCH
 * @version 1.0 T.Sutoh@JCCH
 *          Created 2010/02/19
 *          ログダウンロード用の開始日、終了日を追加
 */
public class SelectTrafficLog implements Serializable {
	//
	private static final long serialVersionUID = 2758157593848613682L;

	// 絞り込み条件
	private String dataStr = "";

	// SourceIp
	private String sIp = "";

	// DestinationIp
	private String dIp = "";

	// SourcePort
	private String sPort = "";

	// DestinationPort
	private String dPort = "";

	// Application
	private String app = "";

	// VsysID(検索条件としてセッションから取得して使用)
	private String virtualSystem = "";

	// ダウンロード条件日付:開始日
	private String startDate;

	// ダウンロード条件日付:終了日
	private String endDate;

	// 検索結果取得件数
	private long line = 0;

	// 検索結果取得オフセット値
	private long startIdx = 0;

	/**
	 * メソッド名 : dataStrのGetterメソッド
	 * 機能概要 : dataStrを取得します。
	 * @return dataStr 絞り込み条件
	 */
	public String getDataStr() {
	    return dataStr;
	}

	/**
	 * メソッド名 : dataStrのSetterメソッド
	 * 機能概要 : dataStrを設定します。
	 * @param dataStr 絞り込み条件
	 */
	public void setDataStr(String dataStr) {
	    this.dataStr = dataStr;
	}

	/**
	 * メソッド名 : sIpのGetterメソッド
	 * 機能概要 : sIpを取得します。
	 * @return sIp SourceIp
	 */
	public String getsIp() {
	    return sIp;
	}

	/**
	 * メソッド名 : sIpのSetterメソッド
	 * 機能概要 : sIpを設定します。
	 * @param sIp SourceIp
	 */
	public void setsIp(String sIp) {
	    this.sIp = sIp;
	}

	/**
	 * メソッド名 : dIpのGetterメソッド
	 * 機能概要 : dIpを取得します。
	 * @return dIp DestinationIp
	 */
	public String getdIp() {
	    return dIp;
	}

	/**
	 * メソッド名 : dIpのSetterメソッド
	 * 機能概要 : dIpを設定します。
	 * @param dIp DestinationIp
	 */
	public void setdIp(String dIp) {
	    this.dIp = dIp;
	}

	/**
	 * メソッド名 : sPortのGetterメソッド
	 * 機能概要 : sPortを取得します。
	 * @return sPort SourcePort
	 */
	public String getsPort() {
	    return sPort;
	}

	/**
	 * メソッド名 : sPortのSetterメソッド
	 * 機能概要 : sPortを設定します。
	 * @param sPort SourcePort
	 */
	public void setsPort(String sPort) {
	    this.sPort = sPort;
	}

	/**
	 * メソッド名 : dPortのGetterメソッド
	 * 機能概要 : dPortを取得します。
	 * @return dPort DestinationPort
	 */
	public String getdPort() {
	    return dPort;
	}

	/**
	 * メソッド名 : dPortのSetterメソッド
	 * 機能概要 : dPortを設定します。
	 * @param dPort DestinationPort
	 */
	public void setdPort(String dPort) {
	    this.dPort = dPort;
	}

	/**
	 * メソッド名 : appのGetterメソッド
	 * 機能概要 : appを取得します。
	 * @return app Application
	 */
	public String getApp() {
	    return app;
	}

	/**
	 * メソッド名 : appのSetterメソッド
	 * 機能概要 : appを設定します。
	 * @param app Application
	 */
	public void setApp(String app) {
	    this.app = app;
	}

	/**
	 * メソッド名 : virtualSystemのGetterメソッド
	 * 機能概要 : virtualSystemを取得します。
	 * @return virtualSystem VsysID
	 */
	public String getVirtualSystem() {
	    return virtualSystem;
	}

	/**
	 * メソッド名 : virtualSystemのSetterメソッド
	 * 機能概要 : virtualSystemを設定します。
	 * @param virtualSystem VsysID
	 */
	public void setVirtualSystem(String virtualSystem) {
	    this.virtualSystem = virtualSystem;
	}

	/**
	 * メソッド名 : startDateのGetterメソッド
	 * 機能概要 : startDateを取得します。
	 * @return startDate ダウンロード条件日付:開始日
	 */
	public String getStartDate() {
	    return startDate;
	}

	/**
	 * メソッド名 : startDateのSetterメソッド
	 * 機能概要 : startDateを設定します。
	 * @param startDate ダウンロード条件日付:開始日
	 */
	public void setStartDate(String startDate) {
	    this.startDate = startDate;
	}

	/**
	 * メソッド名 : endDateのGetterメソッド
	 * 機能概要 : endDateを取得します。
	 * @return endDate ダウンロード条件日付:終了日
	 */
	public String getEndDate() {
	    return endDate;
	}

	/**
	 * メソッド名 : endDateのSetterメソッド
	 * 機能概要 : endDateを設定します。
	 * @param endDate ダウンロード条件日付:終了日
	 */
	public void setEndDate(String endDate) {
	    this.endDate = endDate;
	}

	/**
	 * メソッド名 : lineのGetterメソッド
	 * 機能概要 : lineを取得します。
	 * @return line 検索結果取得件数
	 */
	public long getLine() {
		return line;
	}

	/**
	 * メソッド名 : lineのSetterメソッド
	 * 機能概要 : lineを設定します。
	 * @param line 検索結果取得件数
	 */
	public void setLine(long line) {
		this.line = line;
	}

	/**
	 * メソッド名 : startIdxのGetterメソッド
	 * 機能概要 : startIdxを取得します。
	 * @return startIdx 検索結果取得オフセット値
	 */
	public long getStartIdx() {
		return startIdx;
	}

	/**
	 * メソッド名 : startIdxのSetterメソッド
	 * 機能概要 : startIdxを設定します。
	 * @param startIdx 検索結果取得オフセット値
	 */
	public void setStartIdx(long startIdx) {
		this.startIdx = startIdx;
	}

}
