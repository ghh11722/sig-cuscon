/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : FilterSearchBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.blogic;

import java.util.ArrayList;
import java.util.List;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.common.ApplicationList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.dto.FilterSearchInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.dto.FilterSearchOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.RiskInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationList4Search;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectFilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectSearchList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SeqAndNameInfo;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;
import org.apache.log4j.Logger;

/**
 * クラス名 : FilterSearchBLogic
 * 機能概要 : アプリケーション検索処理を行う。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class FilterSearchBLogic implements BLogic<FilterSearchInput> {

	// QueryDAO
    private QueryDAO queryDAO = null;

    private Logger log = Logger.getLogger(
    		"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.blogic.FilterSearchBLogic");
    private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 : アプリケーション検索処理を行う。
	 * @param params FilterSearchInput入力データクラス
	 * @return result BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(FilterSearchInput params) {
		log.debug("FilterSearchBLogic処理開始");

	    // 検索条件用カテゴリリスト
		List<Integer> category = null;
		// 検索条件用サブカテゴリリスト
		List<Integer> subCategory = null;
		// 検索条件用テクノロジリスト
		List<Integer> technology = null;
		// 検索条件用リスクリスト
		List<Integer> risk = null;
		// 検索条件有無フラグ（true:検索条件あり false:検索条件なし）
		boolean searchFlg = false;

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		 // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// 返却用カテゴリリスト
		List<SeqAndNameInfo> categoryList = new ArrayList<SeqAndNameInfo>();
		// 返却用サブカテゴリリスト
		List<SeqAndNameInfo> subCategoryList = new ArrayList<SeqAndNameInfo>();
		// 返却用テクノロジリスト
		List<SeqAndNameInfo> technologyList = new ArrayList<SeqAndNameInfo>();
		// 返却用リスクリスト
		List<RiskInfo> riskList = new ArrayList<RiskInfo>();

	    // 検索条件用カテゴリリスト
		category = new ArrayList<Integer>();
		// 検索条件用サブカテゴリリスト
		subCategory = new ArrayList<Integer>();
		// 検索条件用テクノロジリスト
		technology = new ArrayList<Integer>();
		// 検索条件用リスクリスト
		risk = new ArrayList<Integer>();
		// 検索条件有無フラグ
		searchFlg = false;
		List<SelectApplicationList4Search> applicationList = null;

		// 検索条件用カテゴリリスト作成
		Object objCategoryList[] = params.getCategoryList();
		for (int i = 0; i < objCategoryList.length; i++) {
			SeqAndNameInfo secAndNameInfo = (SeqAndNameInfo) objCategoryList[i];

			// 返却用リストに追加
			categoryList.add(secAndNameInfo);

			// 画面にてカテゴリチェックONの場合
			if (secAndNameInfo.getChkFlg() == true) {
				log.debug("カテゴリチェックONの場合");

				category.add(secAndNameInfo.getSeqNo());

				// 検索条件有りと判断
				searchFlg = true;
			}
		}

		// 検索条件用サブカテゴリリスト作成
		Object[] objSubCategoryList = params.getSubCategoryList();
		for (int i = 0; i < objSubCategoryList.length; i++) {
			SeqAndNameInfo secAndNameInfo = (SeqAndNameInfo) objSubCategoryList[i];

			// 返却用リストに追加
			subCategoryList.add(secAndNameInfo);

			// 画面にてサブカテゴリチェックONの場合
			if (secAndNameInfo.getChkFlg() == true) {
				log.debug("サブカテゴリチェックONの場合");

				subCategory.add(secAndNameInfo.getSeqNo());

				// 検索条件有りと判断
				searchFlg = true;
			}
		}

		// 検索条件用テクノロジリスト作成
		Object[] objTechnologyList = params.getTechnologyList();
		for (int i = 0; i < objTechnologyList.length; i++) {
			SeqAndNameInfo secAndNameInfo = (SeqAndNameInfo) objTechnologyList[i];

			// 返却用リストに追加
			technologyList.add(secAndNameInfo);

			// 画面にてテクノロジチェックONの場合
			if (secAndNameInfo.getChkFlg() == true) {
				log.debug("テクノロジチェックONの場合");

				technology.add(secAndNameInfo.getSeqNo());

				// 検索条件有りと判断
				searchFlg = true;
			}
		}

		// 検索条件用リスクリスト作成
		Object[] objRiskList = params.getRiskList();
		for (int i = 0; i < objRiskList.length; i++) {
			RiskInfo riskInfo = (RiskInfo) objRiskList[i];

			// 返却用リストに追加
			riskList.add(riskInfo);

			// 画面にてリスクチェックONの場合
			if (riskInfo.getChkFlg() == true) {
				log.debug("リスクチェックONの場合");

				risk.add(riskInfo.getName());

				// 検索条件有りと判断
				searchFlg = true;
			}
		}

		/***********************************************************************
		 *  検索処理
		 **********************************************************************/
		SelectSearchList selectSearchList = new SelectSearchList();

		// 検索条件ありの場合
		if (searchFlg) {

			log.debug("検索条件ありの場合");
			// 検索条件用各リストをデータクラスにセット
			selectSearchList.setCategory(category);
			selectSearchList.setSubCategory(subCategory);
			selectSearchList.setTechnology(technology);
			selectSearchList.setRisk(risk);

			try {
				// 検索結果取得

			applicationList = queryDAO.executeForObjectList("ApplicationFiltersBLogic-17",
					selectSearchList);
			} catch(Exception e) {
	    		// トランザクションロールバック
	    		TransactionUtil.setRollbackOnly();
				// DBアクセスエラー
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FK061012", objectgArray, params.getUvo()));
				messages.add("message", new BLogicMessage("DK060073"));
				result.setErrors(messages);
				result.setResultString("failure");
	        	return result;
			}
		} else {

			log.debug("検索条件なしの場合");
			// 検索条件が何もなければ、アプリケーション全件取得
			try {
				// アプリケーションリスト取得（マスタ情報）
				ApplicationList apl = new ApplicationList();
				applicationList = apl.getApplicationList(queryDAO, params.getUvo());

			}  catch (Exception e) {
	    		// トランザクションロールバック
	    		TransactionUtil.setRollbackOnly();
				// DBアクセスエラー
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FK061029", objectgArray, params.getUvo()));
				messages.add("message", new BLogicMessage("DK060073"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}
		}

		SelectFilterMasterList resultFltList = new SelectFilterMasterList();
		resultFltList.setCategoryList(categoryList);
		resultFltList.setSubCategoryList(subCategoryList);
		resultFltList.setTechnologyList(technologyList);
		resultFltList.setRiskList(riskList);

		FilterSearchOutput output = new FilterSearchOutput();
		output.setSearchWord(params.getSearchWord());
		output.setName(params.getName());
		output.setFilterList(resultFltList);
		output.setApplicationList(applicationList);

		// レスポンス返却
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("FilterSearchBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
