/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ScheduleOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.vo.Schedules;

/**
 * クラス名 : ScheduleOutput
 * 機能概要 :スケジュール一覧表示出力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class ScheduleOutput {

	// スケジュールリスト
	private List<Schedules> scheduleList = null;
	private String message = null;

	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * メソッド名 : scheduleListのGetterメソッド
	 * 機能概要 : scheduleListを取得する。
	 * @return scheduleList
	 */
	public List<Schedules> getScheduleList() {
		return scheduleList;
	}

	/**
	 * メソッド名 : scheduleListのSetterメソッド
	 * 機能概要 : scheduleListをセットする。
	 * @param scheduleList
	 */
	public void setScheduleList(List<Schedules> scheduleList) {
		this.scheduleList = scheduleList;
	}
}
