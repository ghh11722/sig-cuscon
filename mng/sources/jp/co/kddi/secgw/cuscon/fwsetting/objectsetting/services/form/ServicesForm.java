/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ServicesForm.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.form;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectServiceList;
import jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx;

/**
 * クラス名 : ServicesForm
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/01
 *          新規作成
 * @see
 */
public class ServicesForm extends ValidatorActionFormEx{

	// シリアルバージョンID
	private static final long serialVersionUID = 1L;
	// サービス情報リスト
	private List<SelectServiceList> serviceList = null;
	// サービス名
	private String name = null;
	// サービス名(新)
	private String newServiceName = null;
	// サービス名(旧)
	private String oldServiceName = null;
	// プロトコル
	private String protocol = null;
	// ポート
	private String port = null;
	// 選択されたインデックス
	private String checkIndex = null;
	// インデックス
	private int index = 0;
	private String message = null;

	public void reset(ActionMapping mapping, HttpServletRequest request){
		 message=null;
	}

	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * メソッド名 : serviceListのGetterメソッド
	 * 機能概要 : serviceListを取得する。
	 * @return serviceList
	 */
	public List<SelectServiceList> getServiceList() {
		return serviceList;
	}
	/**
	 * メソッド名 : serviceListのSetterメソッド
	 * 機能概要 : serviceListをセットする。
	 * @param serviceList
	 */
	public void setServiceList(List<SelectServiceList> serviceList) {
		this.serviceList = serviceList;
	}

	/**
	 * メソッド名 : oldServiceNameのSetterメソッド
	 * 機能概要 : oldServiceNameをセットする。
	 * @param oldServiceName
	 */
	public void setOldServiceName(String oldServiceName) {
		this.oldServiceName = oldServiceName;
	}

	/**
	 * メソッド名 : oldServiceNameのGetterメソッド
	 * 機能概要 : oldServiceNameを取得する。
	 * @return oldServiceName
	 */
	public String getOldServiceName() {
		return oldServiceName;
	}

	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * メソッド名 : newServiceNameのSetterメソッド
	 * 機能概要 : newServiceNameをセットする。
	 * @param newServiceName
	 */
	public void setNewServiceName(String newServiceName) {
		this.newServiceName = newServiceName;
	}

	/**
	 * メソッド名 : newServiceNameのGetterメソッド
	 * 機能概要 : newServiceNameを取得する。
	 * @return newServiceName
	 */
	public String getNewServiceName() {
		return newServiceName;
	}

	/**
	 * メソッド名 : protocolのSetterメソッド
	 * 機能概要 : protocolをセットする。
	 * @param protocol
	 */
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	/**
	 * メソッド名 : protocolのGetterメソッド
	 * 機能概要 : protocolを取得する。
	 * @return protocol
	 */
	public String getProtocol() {
		return protocol;
	}

	/**
	 * メソッド名 : portのSetterメソッド
	 * 機能概要 : portをセットする。
	 * @param port
	 */
	public void setPort(String port) {
		this.port = port;
	}

	/**
	 * メソッド名 : portのGetterメソッド
	 * 機能概要 : portを取得する。
	 * @return port
	 */
	public String getPort() {
		return port;
	}

	/**
	 * メソッド名 : 選択されたindexのGetterメソッド
	 * 機能概要 : 選択されたindexを取得する。
	 * @return checkIndex
	 */
	public String getCheckIndex() {
		return checkIndex;
	}
	/**
	 * メソッド名 : 選択されたindexのSetterメソッド
	 * 機能概要 : 選択されたindexをセットする。
	 * @param checkIndex
	 */
	public void setCheckIndex(String checkIndex) {
		this.checkIndex = checkIndex;
	}

	/**
	 * メソッド名 : indexのGetterメソッド
	 * 機能概要 : indexを取得する。
	 * @return index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * メソッド名 : indexのSetterメソッド
	 * 機能概要 : indexをセットする。
	 * @param index
	 */
	public void setIndex(int index) {
		this.index = index;
	}
}
