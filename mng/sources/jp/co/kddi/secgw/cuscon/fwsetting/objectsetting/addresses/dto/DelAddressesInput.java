/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : DelAddressesInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/18     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectAddressList;

/**
 * クラス名 : DelAddressesInput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/18
 *          新規作成
 * @see
 */
public class DelAddressesInput {
    private CusconUVO uvo = null;

	// アドレスリスト
	private List<SelectAddressList> addressList = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}

	/**
	 * メソッド名 : addressListのGetterメソッド
	 * 機能概要 : addressListを取得する。
	 * @return addressList
	 */
	public List<SelectAddressList> getAddressList() {
		return addressList;
	}

	/**
	 * メソッド名 : addressListのSetterメソッド
	 * 機能概要 : addressListをセットする。
	 * @param addressList
	 */
	public void setAddressList(List<SelectAddressList> addressList) {
		this.addressList = addressList;
	}
}
