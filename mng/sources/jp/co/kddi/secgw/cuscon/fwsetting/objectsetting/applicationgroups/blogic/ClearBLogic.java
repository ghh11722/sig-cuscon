/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ClearBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.blogic;

import java.util.ArrayList;
import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.common.ApplicationList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.FilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto.ClearInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto.ClearOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationList4Search;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectFilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import org.apache.log4j.Logger;

/**
 * クラス名 : ClearBLogic
 * 機能概要 : フィルターの解除を行う。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class ClearBLogic implements BLogic<ClearInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger(
	"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.blogic.ClearBLogic");

	private MessageAccessor messageAccessor = null;

	// 画面遷移判別定数
	private static final String REGIST = "登録";         // 登録
	//	private static final String UPDATE = "更新";     // 更新

	/**
	 * メソッド名 : execute
	 * 機能概要 : フィルターの解除を行う。
	 * @param params ClearInput入力データクラス
	 * @return result BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(ClearInput params) {
		log.debug("ClearBLogic処理開始");

		SelectFilterMasterList resultFltList;                // フィルタマスタ情報
		List<SelectApplicationList4Search> resultAplList;    // アプリケーションマスタ情報
		ClearOutput output;                                  // 返却用リスト

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		if (params.getDisplayType().equals(REGIST)) {
			log.debug("遷移先が登録系画面");
			result.setResultString("failure4Regist");
		} else {
			log.debug("遷移先が更新系画面");
			result.setResultString("failure4Update");
		}

		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = params.getUvo();
		try {
			// フィルタリスト取得（マスタ情報）
			FilterMasterList filterMasterList = new FilterMasterList();
			resultFltList = filterMasterList.getFilterMasterList(queryDAO, uvo);

			// アプリケーションリスト取得（マスタ情報）
			ApplicationList applicationList = new ApplicationList();
			resultAplList = applicationList.getApplicationList(queryDAO, uvo);

		} catch(Exception e) {
			// DBアクセスエラーログ出力
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("FK061016",
					objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060025"));
			result.setErrors(messages);
			return result;
		}

		List<SelectedAplInfo> selectedAplList = new ArrayList<SelectedAplInfo>();
		if (params.getSelectAplList() != null) {
			log.debug("選択中アプリケーション情報作成");
			for (int i = 0; i < params.getSelectAplList().length; i++) {
				log.debug("選択中アプリケーション情報作成中:" + i);
				SelectedAplInfo aplInfo = new SelectedAplInfo();

				aplInfo.setName(params.getSelectAplList()[i]);
				aplInfo.setType(params.getSelectTypeList()[i]);

				selectedAplList.add(aplInfo);
			}
		}

		// 返却用リスト
		output = new ClearOutput();
		output.setFilterList(resultFltList);
		output.setApplicationList(resultAplList);
		output.setSelectedAplList(selectedAplList);

		// レスポンス返却
		result.setResultObject(output);
		// 遷移先判別（新規登録画面から遷移してきた場合）
		if (params.getDisplayType().equals(REGIST)) {
			log.debug("遷移先が登録系画面");
			result.setResultString("success4Regist");
		} else {
			result.setResultString("success4Update");
			log.debug("遷移先が更新系画面");
		}

		log.debug("ClearBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}

}
