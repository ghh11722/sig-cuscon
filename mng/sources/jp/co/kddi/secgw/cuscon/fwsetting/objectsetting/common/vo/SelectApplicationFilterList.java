/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectApplicationFilterListOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/20     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectObjectList;

/**
 * クラス名 : SelectApplicationFilterListOutput
 * 機能概要 : アプリケーションフィルタ一覧データクラス
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/20
 *          新規作成
 * @see
 */
public class SelectApplicationFilterList implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = -4293675828998719618L;
	// アプリケーションフィルタ名
	private String name = new String();
	// カテゴリリスト
	private List<SelectObjectList> categoryList = new ArrayList<SelectObjectList>();
	// サブカテゴリリスト
	private List<SelectObjectList> subCategoryList =new ArrayList<SelectObjectList>();
	// テクノロジリスト
	private List<SelectObjectList> technologyList = new ArrayList<SelectObjectList>();
	// リスクリスト
	private List<SelectObjectList> riskList = new ArrayList<SelectObjectList>();
	// チェック情報（画面にチェックボックスON/OFFがある場合に使用 ON:true/OFF:false）
	private boolean chkFlg = false;

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : categoryListのGetterメソッド
	 * 機能概要 : categoryListを取得する。
	 * @return categoryList
	 */
	public List<SelectObjectList> getCategoryList() {
		return categoryList;
	}
	/**
	 * メソッド名 : categoryListのSetterメソッド
	 * 機能概要 : categoryListをセットする。
	 * @param categoryList
	 */
	public void setCategoryList(List<SelectObjectList> categoryList) {
		this.categoryList = categoryList;
	}
	/**
	 * メソッド名 : subCategoryListのGetterメソッド
	 * 機能概要 : subCategoryListを取得する。
	 * @return subCategoryList
	 */
	public List<SelectObjectList> getSubCategoryList() {
		return subCategoryList;
	}
	/**
	 * メソッド名 : subCategoryListのSetterメソッド
	 * 機能概要 : subCategoryListをセットする。
	 * @param subCategoryList
	 */
	public void setSubCategoryList(List<SelectObjectList> subCategoryList) {
		this.subCategoryList = subCategoryList;
	}
	/**
	 * メソッド名 : technologyListのGetterメソッド
	 * 機能概要 : technologyListを取得する。
	 * @return technologyList
	 */
	public List<SelectObjectList> getTechnologyList() {
		return technologyList;
	}
	/**
	 * メソッド名 : technologyListのSetterメソッド
	 * 機能概要 : technologyListをセットする。
	 * @param technologyList
	 */
	public void setTechnologyList(List<SelectObjectList> technologyList) {
		this.technologyList = technologyList;
	}
	/**
	 * メソッド名 : riskListのGetterメソッド
	 * 機能概要 : riskListを取得する。
	 * @return riskList
	 */
	public List<SelectObjectList> getRiskList() {
		return riskList;
	}
	/**
	 * メソッド名 : riskListのSetterメソッド
	 * 機能概要 : riskListをセットする。
	 * @param riskList
	 */
	public void setRiskList(List<SelectObjectList> riskList) {
		this.riskList = riskList;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public boolean getChkFlg() {
		return chkFlg;
	}
	/**
	 * メソッド名 : chkFlgのSetterメソッド
	 * 機能概要 : chkFlgをセットする。
	 * @param chkFlg
	 */
	public void setChkFlg(boolean chkFlg) {
		this.chkFlg = chkFlg;
	}
	/**
	 * メソッド名 : serialversionuidのGetterメソッド
	 * 機能概要 : serialversionuidを取得する。
	 * @return serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
