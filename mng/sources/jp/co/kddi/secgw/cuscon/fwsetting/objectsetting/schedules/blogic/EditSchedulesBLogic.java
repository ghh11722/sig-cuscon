/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditSchedulesBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.blogic;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.dto.EditSchedulesInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.dto.EditSchedulesOutput;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicResult;

/**
 * クラス名 : EditSchedulesBLogic
 * 機能概要 :スケジュール編集画面を表示する。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class EditSchedulesBLogic implements BLogic<EditSchedulesInput> {

	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.blogic.EditSchedulesBLogic");
	/**
	 * メソッド名 :execute
	 * 機能概要 :スケジュール編集画面を表示する。
	 * @param param EditSchedulesInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(EditSchedulesInput param) {

		log.debug("EditSchedulesBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");

        // アウトプットデータクラスに設定する。
        EditSchedulesOutput out = new EditSchedulesOutput();

	    out.setSelectSchedule(param.getScheduleList().get(param.getNameIndex()));
	    out.setShowName(out.getSelectSchedule().getName());
	    out.setTimeList(out.getSelectSchedule().getTimeList());

	    // ディリーの場合
        if(out.getSelectSchedule().getRecurrence() == 0) {
        	log.debug("デイリー選択");
        	result.setResultString("DailySuccess");
        } else if(out.getSelectSchedule().getRecurrence() == 1) {
        	log.debug("ウィークリー選択");
        	result.setResultString("WeeklySuccess");
        } else {
        	log.debug("指定日時選択");
        	result.setResultString("NonSuccess");
        }


        result.setResultObject(out);

	    log.debug("EditSchedulesBLogic処理終了");

		return result;
	}
}
