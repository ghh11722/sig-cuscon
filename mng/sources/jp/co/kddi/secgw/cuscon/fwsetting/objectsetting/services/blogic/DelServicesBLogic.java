/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : DelServices.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/23     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.blogic;

import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.Services;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.ServiceList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectServiceList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.dto.DelServicesInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.dto.ServicesOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.ServiceLinksInfo;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : DelServices
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/23
 *          新規作成
 * @see
 */
public class DelServicesBLogic implements BLogic<DelServicesInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.blogic.DelServicesBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : BLogicResult
	 * 機能概要 :
	 * @param params
	 * @return
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(DelServicesInput params) {
		log.debug("DelServicesBLogic処理開始");
		int seqNo;             // シーケンス番号
		int count;             // リンク数

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		 // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得
		CusconUVO uvo = params.getUvo();
		String vsysId = uvo.getVsysId();

		// 画面から渡されたサービスリストを取得
		List<SelectServiceList> serviceList4Del = params.getServiceList();

		int delSize = serviceList4Del.size();
		int delNum = 0;
		String delStr = "";
		// 画面から渡されたサービスリストの件数分以下の処理を実行
		for (int i = 0; i < serviceList4Del.size(); i++) {
			log.debug("サービスリスト:" + i);
			// 検索条件設定
			Services searchServiceKey = new Services();
			searchServiceKey.setVsysId(vsysId);                            // Vsys-ID
			searchServiceKey.setGenerationNo(CusconConst.ZERO_GENE);       // 世代番号
			searchServiceKey.setName(serviceList4Del.get(i).getName());              // サービス名

			try {
				// サービスオブジェクトテーブルを検索し、削除対象サービスのSEQ_NOを取得
				List<Services> serviceObjectList = queryDAO.executeForObjectList(
						"ServicesBLogic-7", searchServiceKey);
				seqNo = serviceObjectList.get(0).getSeqNo();

				// 検索条件設定
				ServiceLinksInfo selectServiceLinks = new ServiceLinksInfo();
				selectServiceLinks.setGrpLinkSeqNo(seqNo);
				selectServiceLinks.setSrvLinkSeqNo(seqNo);

				// 当該サービスがリンクされている件数を取得
				count= queryDAO.executeForObject("ServicesBLogic-3", selectServiceLinks, Integer.class);

			} catch(Exception e) {
				// トランザクションロールバック
	        	TransactionUtil.setRollbackOnly();
				// DBアクセスエラーログ出力
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FK061020", objectgArray, params.getUvo()));
				messages.add("message", new BLogicMessage("DK060036"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}

			if (count > 0) {
				log.debug("オブジェクト削除不可:" + serviceList4Del.get(i).getName());
				delNum++;

				// 削除名称を連結する。
				if(delNum == 1) {
					delStr = serviceList4Del.get(i).getName();
				} else {
					delStr = delStr + ", " + serviceList4Del.get(i).getName();
				}
				if(delSize == delNum) {
					// 検索結果が1件以上の場合、削除不可エラー
					// レスポンスデータを作成し返却
					// 削除不可エラーメッセージを出力する。
					Object[] objectgArray = {delStr};
					log.debug(messageAccessor.getMessage(
		        			"DK060002", objectgArray, params.getUvo()));
					messages.add("message",new BLogicMessage("DK060070", objectgArray));
					result.setErrors(messages);
					result.setResultString("failure");
		        	return result;
				}
				// 削除可能
			} else {

				try {
					// サービスオブジェクトテーブルのレコードの変更フラグを'3'（削除）に更新
					updateDAO.execute("ServicesBLogic-4", seqNo);
				} catch(Exception e) {
					// トランザクションロールバック
		        	TransactionUtil.setRollbackOnly();
					// DBアクセスエラーログ出力
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage("FK061019", objectgArray, params.getUvo()));
					messages.add("message", new BLogicMessage("DK060036"));
					result.setErrors(messages);
					result.setResultString("failure");
					return result;
				}
			}
		}
		ServicesOutput output = new ServicesOutput();
		try {
			// 一覧情報検索共通処理
			ServiceList serviceList = new ServiceList();
			output.setServiceList(serviceList.getServiceList(queryDAO, uvo));
		} catch(Exception e) {
			// トランザクションロールバック
        	TransactionUtil.setRollbackOnly();
			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"EK061005", objectgArray, params.getUvo()));
        	messages.add("message", new BLogicMessage("DK060036"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

		if(delNum != 0) {
	    	// 削除不可オブジェクトの名称出力
	    	log.debug("削除不可オブジェクト名称:" + delStr);
			Object[] objectgArray = {delStr};
			output.setMessage(messageAccessor.getMessage("DK060070", objectgArray));
		}
		// レスポンス返却
		result.setResultObject(output);
		result.setResultString("success");
		log.debug("DelServicesBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("queryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("queryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
