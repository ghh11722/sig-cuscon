/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ShowUrlFilterBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/14     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.rollback.blogic;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.common.vo.UrlFilters;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto.ShowInput;
import jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto.ShowUrlFilterOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;

import org.apache.log4j.Logger;

/**
 * クラス名 : ShowUrlFilterBLogic
 * 機能概要 :UrlFilter項目選択時の処理を行う。
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/14
 *          新規作成
 * @see
 */
public class ShowUrlFilterBLogic implements BLogic<ShowInput> {

	/**
     * QueryDAO。
     * Springによりインスタンス生成され設定される。
     */
    private QueryDAO queryDAO = null;
    private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.rollback.blogic.ShowUrlFilterBLogic");
    private MessageAccessor messageAccessor = null;

    /**
	 * メソッド名 :execute
	 * 機能概要 : 選択Webフィルタをアウトプットクラスに設定する。
	 * @param param ShowInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(ShowInput param) {

		log.debug("ShowUrlFilterBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");
        // エラーメッセージ
        BLogicMessages messages = new BLogicMessages();
        CusconUVO uvo = param.getUvo();

        // 選択したリストからオブジェクトを取得する。
        SelectPolicyList selectPolicy = param.getPolicyList().get(param.getIndex());

        // Webフィルタリスト
		List<UrlFilters> urlFilterList = null;
        try {
			// InputBase設定
			InputBase inputBase = new InputBase();
			inputBase.setVsysId(uvo.getVsysId());                // Vsys-ID
			inputBase.setGenerationNo(param.getSelectGeneNo());  // 世代番号
			inputBase.setModFlg(CusconConst.DEL_NUM);            // 変更フラグ

			// Webフィルタ情報取得
			urlFilterList = queryDAO.executeForObjectList("CommonT_UrlFilters-1", inputBase);
        } catch(Exception e) {
			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"FK071001", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK070026"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

        // ビジネスロジックの出力クラスに結果を設定する
        ShowUrlFilterOutput out = new ShowUrlFilterOutput();
        out.setName(selectPolicy.getName());
        out.setUrlFilterList(urlFilterList);
        out.setUrlFilterName(selectPolicy.getUrlFilterName());
        out.setUrlfilterDefaultFlg(selectPolicy.getUrlfilterDefaultFlg());

        result.setResultObject(out);
        result.setResultString("success");
        log.debug("ShowUrlFilterBLogic処理終了");

		return result;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
