/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : RollbackEndBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     komakiys                初版作成
 * 2010/06/22     oohashij         ロールバック処理を別プロセス呼出へ変更
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.rollback.blogic;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.commit.exception.TimeoutException;
import jp.co.kddi.secgw.cuscon.fwsetting.common.CommitStatusManager;
import jp.co.kddi.secgw.cuscon.fwsetting.common.ProcessCaller;
import jp.co.kddi.secgw.cuscon.fwsetting.common.ResultJudgeData;
import jp.co.kddi.secgw.cuscon.fwsetting.common.UpdateCommitFlg;
import jp.co.kddi.secgw.cuscon.fwsetting.common.UpdatePA;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.CommitFlg;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.CommitStatus;
import jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto.RollbackEndInput;
import jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto.RollbackOutput;

import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;
import jp.terasoluna.fw.util.PropertyUtil;

/**
 * クラス名 : RollbackEndBLogic
 * 機能概要 :ロールバック処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/01
 *          新規作成
 * @version 1.1 oohashij
 *          Created 2010/06/22
 *          ロールバック処理を別プロセス呼出へ変更
 * @see
 */
public class RollbackEndBLogic implements BLogic<RollbackEndInput> {

	// QueryDAO
    private QueryDAO queryDAO = null;
    // UpdateDAO
    private UpdateDAO updateDAO = null;
    // 処理区分
    private static final int ROLLBACK = 1;
	// 日時フォーマット
	private static final String DATE_FORMAT = PropertyUtil.getProperty("fwsetting.common.dateformat");
    private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.rollback.blogic.RollbackEndBLogic");
    private MessageAccessor messageAccessor = null;
	/**
	 * メソッド名 :execute
	 * 機能概要 : UpdatePA、CusconFWSetを呼び出し、選択世代についてロールバック処理を行う。
	 * @param param RollbackEndの入力クラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(RollbackEndInput param) {

		log.debug("RollbackEndBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");
        // ビジネスロジックの出力クラスに結果を設定する
		RollbackOutput out = new RollbackOutput();

		// UpdatePAクラスの生成
		UpdatePA update = new UpdatePA();

		// 処理結果格納用
        ResultJudgeData resultJudge = new ResultJudgeData();

        // コミットフラグ
        CommitFlg commit_flg = new CommitFlg();
        commit_flg.setCommtFlg(false);

        //受付時間
        String startTime = null;
        try {
        	// コミットステータスの削除
        	CommitStatusManager commtStatManager = new CommitStatusManager();
        	commtStatManager.deleteCommitStatus(updateDAO, param.getUvo());

            update.updatePAProcess(resultJudge, param.getUvo().getPaLoginId(), param.getUvo().getPaPasswd(), queryDAO, param.getUvo(), commit_flg);

			// メンテナンス中の場合
			if(resultJudge.getResultFlg() == 1) {
				// メンテナンス中メッセージ出力
				log.debug("メンテナンス中のため、ロールバックできませんでした。");
				out.setMessage(
					messageAccessor.getMessage(resultJudge.getResultData(), null));
				out.setAcceptStatus(CusconConst.ACCEPT_NG);
				result.setResultObject(out);
				result.setResultString("failure");
				return result;
			}

			// 他利用者コミット中の場合
			if(resultJudge.getResultFlg() == 2) {
				// 他利用者コミット中メッセージ出力
				log.debug("他利用者コミット中のため、ロールバックできませんでした。");
	        	// コミット中フラグOFF
	        	if (commit_flg.isCommtFlg()) {
	            	UpdateCommitFlg ucf = new UpdateCommitFlg();
	    			try {
	    				ucf.updatePAProcessFlg(CusconConst.COMMIT_OFF);
	    				commit_flg.setCommtFlg(false);
	    			} catch (Exception e1) {
	    				Object[] objectgArray = {e1.getMessage()};
	    				log.error(messageAccessor.getMessage("EK071003", objectgArray, param.getUvo()));
	    			}
	        	}
				out.setMessage(
					messageAccessor.getMessage(resultJudge.getResultData(), null));
				out.setAcceptStatus(CusconConst.ACCEPT_NG);
				result.setResultObject(out);
				result.setResultString("failure");
				return result;
			}

			// コミットステータスの登録
			startTime = insertCommitStatus(param.getUvo());
			// 別プロセスの呼出
			ProcessCaller processCaller = new ProcessCaller();
			processCaller.call(param.getUvo(), CusconConst.ROLLBACK, param.getSelectGeneNo());

        } catch(TimeoutException e) {
        	// コミット中フラグOFF
        	if (commit_flg.isCommtFlg()) {
            	UpdateCommitFlg ucf = new UpdateCommitFlg();
    			try {
    				ucf.updatePAProcessFlg(CusconConst.COMMIT_OFF);
    				commit_flg.setCommtFlg(false);
    			} catch (Exception e1) {
    				Object[] objectgArray = {e1.getMessage()};
    				log.error(messageAccessor.getMessage("EK071003", objectgArray, param.getUvo()));
    			}
        	}
        	// コマンドタイムアウトメッセージ出力
			log.debug("コマンドタイムアウト");
			Object[] objectgArray = {"コマンドタイムアウト"};
			out.setMessage(messageAccessor.getMessage("DK070007", objectgArray));
			out.setAcceptStatus(CusconConst.ACCEPT_NG);
			result.setResultObject(out);
			result.setResultString("failure");
			return result;

		} catch (Exception e) {
        	// コミット中フラグOFF
        	if (commit_flg.isCommtFlg()) {
            	UpdateCommitFlg ucf = new UpdateCommitFlg();
    			try {
    				ucf.updatePAProcessFlg(CusconConst.COMMIT_OFF);
    				commit_flg.setCommtFlg(false);
    			} catch (Exception e1) {
    				Object[] objectgArray = {e1.getMessage()};
    				log.error(messageAccessor.getMessage("EK071003", objectgArray, param.getUvo()));
    			}
        	}
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// ロールバック処理失敗ログ出力
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("EK071002", objectgArray, param.getUvo()));
			out.setMessage(messageAccessor.getMessage("DK070007", null));
			out.setAcceptStatus(CusconConst.ACCEPT_NG);
			result.setResultObject(out);
			result.setResultString("failure");
			return result;
		}

		// ロールバック成功のメッセージ格納
		out.setMessage(messageAccessor.getMessage("DK070023", null));
		out.setStartTime(startTime);
		out.setAcceptStatus(CusconConst.ACCEPT_OK);

		result.setResultObject(out);
        result.setResultString("success");

        log.debug("RollbackEndBLogic処理終了");
        return result;
	}

	/**
	 * メソッド名 : コミットステータス登録
	 * 機能概要 : コミットステータステーブルの登録
	 * @param uvo
	 * @return StartTime
	 * @throws Exception
	 */
	private String insertCommitStatus(CusconUVO uvo) throws Exception {
		log.debug("insertCommitStatus処理開始");

		// コミットステータスの設定
		CommitStatus commitStatus = new CommitStatus();
		commitStatus.setVsysId(uvo.getVsysId());
		commitStatus.setProcessingKind(ROLLBACK);
		commitStatus.setStatus(CusconConst.STAT_COMMITTING);
		commitStatus.setMessage(messageAccessor.getMessage("DK070021", null));
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		commitStatus.setStartTime(dateFormat.format(date));
		commitStatus.setEndTime(null);

		CommitStatusManager commtStatManager = new CommitStatusManager();
		commtStatManager.insertCommitStatus(updateDAO, uvo, commitStatus);

		log.debug("insertCommitStatus処理終了 StartTime = " + commitStatus.getStartTime());
		return commitStatus.getStartTime();
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : updateDAOのSetterメソッド
	 * 機能概要 : updateDAOをセットする。
	 * @param updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		this.updateDAO = updateDAO;
	}
	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
