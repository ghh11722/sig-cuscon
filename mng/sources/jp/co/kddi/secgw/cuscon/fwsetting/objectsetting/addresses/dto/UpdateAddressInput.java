/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditAddressInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/18     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.dto;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;

/**
 * クラス名 : UpdateAddressInput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/18
 *          新規作成
 * @see
 */
public class UpdateAddressInput {

	// UVO
	private CusconUVO uvo = null;
	// 変更前アドレス名
	private String oldAddressName = null;
	// 変更後アドレス名
	private String newAddressName = null;
	// タイプ
	private String type = null;
	// アドレス
	private String address = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}
	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}
	/**
	 * メソッド名 : oldAddressNameのGetterメソッド
	 * 機能概要 : oldAddressNameを取得する。
	 * @return oldAddressName
	 */
	public String getOldAddressName() {
		return oldAddressName;
	}
	/**
	 * メソッド名 : oldAddressNameのSetterメソッド
	 * 機能概要 : oldAddressNameをセットする。
	 * @param oldAddressName
	 */
	public void setOldAddressName(String oldAddressName) {
		this.oldAddressName = oldAddressName;
	}
	/**
	 * メソッド名 : newAddressNameのGetterメソッド
	 * 機能概要 : newAddressNameを取得する。
	 * @return newAddressName
	 */
	public String getNewAddressName() {
		return newAddressName;
	}
	/**
	 * メソッド名 : newAddressNameのSetterメソッド
	 * 機能概要 : newAddressNameをセットする。
	 * @param newAddressName
	 */
	public void setNewAddressName(String newAddressName) {
		this.newAddressName = newAddressName;
	}
	/**
	 * メソッド名 : typeのGetterメソッド
	 * 機能概要 : typeを取得する。
	 * @return type
	 */
	public String getType() {
		return type;
	}
	/**
	 * メソッド名 : typeのSetterメソッド
	 * 機能概要 : typeをセットする。
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * メソッド名 : addressのGetterメソッド
	 * 機能概要 : addressを取得する。
	 * @return address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * メソッド名 : addressのSetterメソッド
	 * 機能概要 : addressをセットする。
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}
}
