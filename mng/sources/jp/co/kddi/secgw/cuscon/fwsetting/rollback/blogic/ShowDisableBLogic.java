/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ShowDisableBLogic.java
 *
 * [変更履歴]
 * 日付        更新者              内容
 * 2013/09/01  kkato@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.rollback.blogic;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto.ShowInput;
import jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto.ShowDisableOutput;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicResult;

/**
 * クラス名 : ShowDisableBLogic
 * 機能概要 :Disable項目選択時の処理を行う。
 * 備考 :
 * @author kkato@PROSITE
 * @version 1.0 kkato@PROSITE
 *          Created 2013/09/01
 *          新規作成
 * @see
 */
public class ShowDisableBLogic implements BLogic<ShowInput> {

	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.rollback.blogic.ShowDisableBLogic");
	/**
	 * メソッド名 :execute
	 * 機能概要 :ルール名、スパイウェアチェックフラグをアウトプットクラスに設定する。
	 * @param param ShowInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(ShowInput param) {

		log.debug("ShowDisableBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");

        // 選択したリストからオブジェクトを取得する。
        SelectPolicyList selectPolicy =
        	param.getPolicyList().get(param.getIndex());

        // ビジネスロジックの出力クラスに結果を設定する
        ShowDisableOutput out = new ShowDisableOutput();
        out.setDisableFlg(selectPolicy.getDisableFlg());

        result.setResultObject(out);
        result.setResultString("success");

        log.debug("ShowDisableBLogic処理終了");
        return result;
	}
}
