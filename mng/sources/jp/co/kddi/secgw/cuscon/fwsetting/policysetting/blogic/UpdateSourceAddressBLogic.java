/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateSourceAddressBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.SecurityRules;
import jp.co.kddi.secgw.cuscon.fwsetting.common.PolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.LinkInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.common.PolicyRulesList;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.PolicyOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.UpdateAddressInput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : UpdateSourceAddressBLogic
 * 機能概要 :SourceAddress項目編集画面にて「OK」押下時の処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class UpdateSourceAddressBLogic implements BLogic<UpdateAddressInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;
	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic.UpdateSourceAddressBLogic");
	private MessageAccessor messageAccessor = null;
	/**
	 * メソッド名 :execute
	 * 機能概要 :選択出所アドレスを更新する。
	 * @param param SourceAddress編集画面入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(UpdateAddressInput param) {
		log.debug("UpdateSourceAddressBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");
        // エラーメッセージ
        BLogicMessages messages = new BLogicMessages();

		// ポリシー設定共通クラスを生成する。
		PolicyRulesList policy = new PolicyRulesList();

		// 削除条件を設定する。
		SecurityRules base = new SecurityRules();
		base.setVsysId(param.getUvo().getVsysId());
		base.setGenerationNo(CusconConst.ZERO_GENE);
		base.setModFlg(CusconConst.MOD_NUM);
		base.setName(param.getName());
		base.setModName(param.getName());

		int seqNo;
		try {
			// 変更フラグを更新し、シーケンス番号を取得する。
			seqNo = policy.updateModFlg(updateDAO, queryDAO, param.getUvo(), base);

			// シーケンス番号に紐付くローカル出所アドレスを削除する。
			updateDAO.execute("DelPolicyBLogic-2", seqNo);
			// シーケンス番号に紐付く出所アドレスリンクを削除する。
			updateDAO.execute("DelPolicyBLogic-6", seqNo);
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage(
					"FK051003", objectgArray, param.getUvo()));
			messages.add("message", new BLogicMessage("DK050017"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

		// 選択するを選択した場合
		if(param.getRadio() == CusconConst.SELECT) {

			log.debug("登録処理を行う。index = 1");
			List<String> registList = new ArrayList<String>();

			String[] checkList = param.getCheckList().split(",");

			// アドレス、グループを選択した場合
			if(checkList[0] != "") {
				log.debug("アドレス、グループ選択");
				// 選択されたアドレス、グループを取得する。
				for(int i=0; i<checkList.length; i++) {
					log.debug("選択されたアドレス、グループを取得:checkList[" +
										i + "] = " + checkList[i]);

					registList.add(param.getAddressMasterList().get(
							Integer.parseInt(checkList[i])).getName());
				}
			}

			// 登録条件を設定する。
			LinkInfo link = new LinkInfo();
			link.setSeqNo(seqNo);
			link.setGenerationNo(CusconConst.ZERO_GENE);
			link.setVsysId(param.getUvo().getVsysId());
			link.setLinkNameList(registList);
			try {
				// アドレス、グループを選択した場合
				if(checkList[0] != "") {
					log.debug("アドレス、グループの登録");
					// 出所アドレスリンクに登録する。
					updateDAO.execute("UpdateSourceAddressBLogic-1", link);
				}

				String[] addAddressList = param.getAddCheckList().split(",");

				boolean registFlg = true;
				// 追加アドレスを選択した場合
				if(addAddressList[0] != "") {
					log.debug("追加アドレス選択");
					// 出所アドレス登録データ分繰り返す。
					for(int i=0; i<addAddressList.length; i++) {
						log.debug("出所アドレス登録データ:" + i);
						registFlg = true;
						for(int j=i+1; j<addAddressList.length; j++) {
							log.debug("同一名称チェック:" + j);
							if(addAddressList[i].equals(addAddressList[j])) {
								log.debug("同一名称存在:" + j);
								registFlg = false;
							}
						}
						if(registFlg) {
							log.debug("ローカル出所アドレスの登録:" + addAddressList[i]);
							// ローカル出所アドレスのシーケンス番号を払い出す。
							int linkSeqNo =
								queryDAO.executeForObject(
										"LocalSourceAddressSeq-1", null, java.lang.Integer.class);

							link.setLinkSeqNo(linkSeqNo);
							link.setLinkName(addAddressList[i]);
							// ローカル出所アドレスに登録を行う。
							updateDAO.execute("CommonT_LocalSourceAddress-2", link);

							// 出所アドレスリンクに登録したローカル出所アドレス
							// とのリンクを登録する。
							updateDAO.execute("UpdateSourceAddressBLogic-2", link);
						}
					}
				}
			} catch(Exception e) {
				// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
				// DBアクセスエラー
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage(
						"FK051002", objectgArray, param.getUvo()));
				messages.add("message", new BLogicMessage("DK050017"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}
		}

		// ポリシーリストクラスを生成する。
        PolicyList rule = new PolicyList();

        // 選択された世代番号のポリシーリスト
        List<SelectPolicyList> policyList = null;
        try {
	        // 選択された世代番号のポリシーリストを取得する。
	        policyList = rule.getPolicylist(queryDAO, param.getUvo(),
	        					CusconConst.ZERO_GENE, CusconConst.POLICY);
        } catch(Exception e) {
			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"EK051001", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK050017"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

        // ビジネスロジックの出力クラスに結果を設定する
        PolicyOutput out = new PolicyOutput();
        out.setPolicyList(policyList);
        result.setResultObject(out);
        result.setResultString("success");

        log.debug("UpdateSourceAddressBLogic処理終了");
		return result;

	}
	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
