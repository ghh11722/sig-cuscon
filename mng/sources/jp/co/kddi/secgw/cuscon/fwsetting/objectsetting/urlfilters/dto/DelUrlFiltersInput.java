/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : DelUrlFiltersInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/10     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.dto;

import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectUrlFilterList;

/**
 * クラス名 : DelUrlFiltersInput
 * 機能概要 :
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/10
 *          新規作成
 * @see
 */
public class DelUrlFiltersInput {
    private CusconUVO uvo = null;

	// Webフィルタリスト
	private List<SelectUrlFilterList> urlFilterList = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}

	/**
	 * メソッド名 : urlFilterListのGetterメソッド
	 * 機能概要 : urlFilterListを取得する。
	 * @return urlFilterList
	 */
	public List<SelectUrlFilterList> getUrlFilterList() {
		return urlFilterList;
	}
	/**
	 * メソッド名 : urlFilterListのSetterメソッド
	 * 機能概要 : urlFilterListをセットする。
	 * @param urlFilterList
	 */
	public void setUrlFilterList(List<SelectUrlFilterList> urlFilterList) {
		this.urlFilterList = urlFilterList;
	}
}
