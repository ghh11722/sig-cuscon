/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ServicesChkValidator.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/13     kinoshtt                初版作成
 *******************************************************************************
 */

package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.form;

import java.io.Serializable;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.validator.Field;
import org.apache.commons.validator.Validator;
import org.apache.commons.validator.ValidatorAction;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.validator.Resources;

/**
 * クラス名 : ServicesChkValidator
 * 機能概要 : サービスのValidatorの拡張機能
 * 備考 :
 * @author kinoshtt
 * @version 1.0 kinoshtt
 *          Created 2010/03/13
 *          新規作成
 * @see
 */
public class ServicesChkValidator implements Serializable {
	//
	private static final long serialVersionUID = 1L;

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 */
	public ServicesChkValidator() {
		super();
	}

	/**
	 * メソッド名 : サービス名の入力チェック
	 * 機能概要 : 不正なサービス名（"any","application-default"）が入力されていないかチェック
	 * @param bean
	 * @param va
	 * @param field
	 * @param errors
	 * @param validator
	 * @param request
	 * @return
	 * @throws JspException
	 */
	public static boolean serviceNameChk(Object bean, ValidatorAction va, Field field, ActionMessages errors, Validator validator, HttpServletRequest request) throws JspException {

		// 検証対象の切り出し
		String str = null;
		if (bean instanceof String) {
			str = (String) bean;
		} else {
			try {
				str = PropertyUtils.getProperty(bean, field.getProperty()).toString();
			} catch (Exception e) {
				throw new JspException(e);
			}
		}

		// サービス名が"any"または"application-default"の場合はエラーとする。
		if (str.equals("any") || str.equals("application-default")) {
			errors.add(field.getKey(), Resources.getActionMessage(request, va, field));
			return false;
		}
		return true;

	}

	/**
	 * メソッド名 : ポートの入力形式判定
	 * 機能概要 : ポートの入力形式が、以下のいずれかにあてはまるかを判定する。
	 *            ・パターン1 x（xは0～65535）
	 *            ・パターン2 x-x（xは0～65535）
	 *            ・パターン3 x,x（xは0～65535）
	 *            ・パターン4 x-x,x-x（xは0～65535）
	 * @param bean
	 * @param va
	 * @param field
	 * @param errors
	 * @param validator
	 * @param request
	 * @return boolean ポートの入力形式が正しければtrue、誤っていればfalse
	 * @throws JspException
	 */
	public static boolean portFormatChk(Object bean, ValidatorAction va, Field field, ActionMessages errors, Validator validator, HttpServletRequest request) throws JspException
	{

		// 検証対象の切り出し
		String str = null;
		if (bean instanceof String) {
			str = (String) bean;
		} else {
			try {
				str = PropertyUtils.getProperty(bean, field.getProperty()).toString();
			} catch (Exception e) {
				throw new JspException(e);
			}
		}

		// パターン1 x（xは0～65535）であるか判定
		Pattern pattern1 = Pattern.compile("\\d{1,5}?");
		Matcher matcher1 = pattern1.matcher(str);
		if(matcher1.matches()){
			if(numChk(Integer.parseInt(str))){
				return true;
			}
		}

		// パターン2 x-x（xは0～65535）であるか判定
		Pattern pattern2 = Pattern.compile("\\d{1,5}?-\\d{1,5}?");
		Matcher matcher2 = pattern2.matcher(str);
		if(matcher2.matches()){
			String firstStr = str.substring(0,str.indexOf("-"));
			String secondStr = str.substring(str.indexOf("-")+1);
			if(numChk(Integer.parseInt(firstStr)) && numChk(Integer.parseInt(secondStr))){
				return true;
			}
		}

		// パターン3 x,x（xは0～65535）であるか判定
		Pattern pattern3 = Pattern.compile("\\d{1,5}?,\\d{1,5}?");
		Matcher matcher3 = pattern3.matcher(str);
		if(matcher3.matches()){
			String firstStr = str.substring(0,str.indexOf(","));
			String secondStr = str.substring(str.indexOf(",")+1);
			if(numChk(Integer.parseInt(firstStr)) && numChk(Integer.parseInt(secondStr))){
				return true;
			}
		}

		// パターン4 x-x,x-x（xは0～65535）であるか判定
		Pattern pattern4 = Pattern.compile("\\d{1,5}?-\\d{1,5}?,\\d{1,5}?-\\d{1,5}?");
		Matcher matcher4 = pattern4.matcher(str);
		if(matcher4.matches()){
			String firstStr = str.substring(0,str.indexOf("-"));
			String secondStr = str.substring(str.indexOf("-")+1, str.indexOf(","));
			String thirdStr = str.substring(str.indexOf(",")+1, str.lastIndexOf("-"));
			String fourthStr = str.substring(str.lastIndexOf("-")+1);
			if(numChk(Integer.parseInt(firstStr)) && numChk(Integer.parseInt(secondStr)) && numChk(Integer.parseInt(thirdStr)) && numChk(Integer.parseInt(fourthStr))){
				return true;
			}
		}
		errors.add(field.getKey(), Resources.getActionMessage(request, va, field));
		return false;
	}

	/**
	 * メソッド名 : 数値判定
	 * 機能概要 : 数値が0～65535であるか判定
	 * @param int 数値
	 * @return boolean 数値が0～65535ならばtrue、それ以外ならばfalse
	 */
	private static boolean numChk(int num)
	{
		if(num >= 0 && num <= 65535){
			return true;
		} else {
			return false;
		}
	}
}
