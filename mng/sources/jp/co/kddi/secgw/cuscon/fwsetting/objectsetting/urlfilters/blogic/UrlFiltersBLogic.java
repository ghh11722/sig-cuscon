/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UrlFiltersBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/06     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.blogic;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.common.CommitStatusManager;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.UrlFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.dto.CommonInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.dto.UrlFiltersOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;

/**
 * クラス名 : UrlFiltersBLogic
 * 機能概要 :
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/06
 *          新規作成
 * @see
 */
public class UrlFiltersBLogic implements BLogic<CommonInput>{

	// QueryDAO
    private QueryDAO queryDAO = null;

    private Logger log = Logger.getLogger(
    		"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.blogic.UrlFiltersBLogic");
    private MessageAccessor messageAccessor = null;
	/**
	 * メソッド名 :
	 * 機能概要 : Webフィルタの一覧情報を取得し、取得した情報を呼び出し元に返却する。
	 * @param arg0
	 * @return
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(CommonInput params) {
		log.debug("UrlFiltersBLogic処理開始");
		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		BLogicMessages errorMessage = new BLogicMessages();// エラーメッセージ
		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = params.getUvo();

		// コミットステータス管理クラスを生成する。。
		CommitStatusManager commtStatMgr = new CommitStatusManager();

		UrlFiltersOutput output = new UrlFiltersOutput();
		try {
			if (commtStatMgr.isCommitting(queryDAO, uvo)) {
				// コミット中
				log.debug("コミット中です。");
				//コミット又はロールバック処理中です。
	        	errorMessage.add("message", new BLogicMessage("DK060074"));
				result.setErrors(errorMessage);
				result.setMessages(errorMessage);
				result.setResultString("failure");
	        	return result;
			} else {
				// 一覧情報検索共通処理
				UrlFilterList urlFilterList = new UrlFilterList();
				urlFilterList.setListLimitSet(true);
				output.setUrlFilterList(urlFilterList.getUrlFilterList(queryDAO, uvo));
			}
		} catch(Exception e) {
			// 例外
			Object[] objectgArray = {e.getMessage()};
			// Webフィルタ処理で例外が発生しました。{0}
			log.error(messageAccessor.getMessage(
								"EK061009", objectgArray, params.getUvo()));
			// Webフィルタ一覧表示に失敗しました。
			errorMessage.add("message", new BLogicMessage("DK060075"));
			result.setErrors(errorMessage);
			result.setMessages(errorMessage);
			result.setResultString("failure");
			return result;
		}

		// 件数の限定


		// レスポンス返却
		result.setResultObject(output);
		result.setResultString("success");
		log.debug("UrlFiltersBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("queryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("queryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
