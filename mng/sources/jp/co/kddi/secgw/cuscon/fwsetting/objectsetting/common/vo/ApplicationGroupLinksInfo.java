/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ApplicationGroupLinksInfo.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/23     morisou                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo;

import java.io.Serializable;

/**
 * クラス名 : ApplicationGroupLinksInfo
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/23
 *          新規作成
 * @see
 */
public class ApplicationGroupLinksInfo implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 2210738887822624271L;
	// T_ApplicationGroupsLinkテーブルのリンクシーケンス番号
	private int grpLinkSeqNo = 0;
	// T_ApplicationLinkテーブルのリンクシーケンス番号
	private int aplLinkSeqNo = 0;
	/**
	 * メソッド名 : grpLinkSeqNoのGetterメソッド
	 * 機能概要 : grpLinkSeqNoを取得する。
	 * @return grpLinkSeqNo
	 */
	public int getGrpLinkSeqNo() {
		return grpLinkSeqNo;
	}
	/**
	 * メソッド名 : grpLinkSeqNoのSetterメソッド
	 * 機能概要 : grpLinkSeqNoをセットする。
	 * @param grpLinkSeqNo
	 */
	public void setGrpLinkSeqNo(int grpLinkSeqNo) {
		this.grpLinkSeqNo = grpLinkSeqNo;
	}
	/**
	 * メソッド名 : aplLinkSeqNoのGetterメソッド
	 * 機能概要 : aplLinkSeqNoを取得する。
	 * @return aplLinkSeqNo
	 */
	public int getAplLinkSeqNo() {
		return aplLinkSeqNo;
	}
	/**
	 * メソッド名 : aplLinkSeqNoのSetterメソッド
	 * 機能概要 : aplLinkSeqNoをセットする。
	 * @param aplLinkSeqNo
	 */
	public void setAplLinkSeqNo(int aplLinkSeqNo) {
		this.aplLinkSeqNo = aplLinkSeqNo;
	}
}
