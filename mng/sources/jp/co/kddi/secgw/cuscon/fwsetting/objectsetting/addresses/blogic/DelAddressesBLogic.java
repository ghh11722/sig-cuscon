/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : DelAddressesBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/22     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.blogic;

import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.Addresses;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.dto.AddressesOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.dto.DelAddressesInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.AddressList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.AddressLinksInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectAddressList;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : DelAddressesBLogic
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/22
 *          新規作成
 * @see
 */
public class DelAddressesBLogic implements BLogic<DelAddressesInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.blogic.DelAddressesBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : BLogicResult
	 * 機能概要 :
	 * @param params
	 * @return
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(DelAddressesInput params) {
		log.debug("DelAddressesBLogic処理開始");

		int seqNo;     // シーケンス番号
		int count;     // リンク数

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		 // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得
		CusconUVO uvo = params.getUvo();
		String vsysId = uvo.getVsysId();

		// 画面から渡された削除対象アドレスリストを取得
		List<SelectAddressList> addressList4Del = params.getAddressList();

		int delSize = addressList4Del.size();
		int delNum = 0;
		String delStr = "";
		// 画面から渡されたアドレスリストの件数分、以下の処理を実行
		for (int i = 0; i < addressList4Del.size(); i++) {
			// 検索条件設定
			Addresses searchAddressKey = new Addresses();
			searchAddressKey.setVsysId(vsysId);                            // Vsys-ID
			searchAddressKey.setGenerationNo(CusconConst.ZERO_GENE);       // 世代番号
			searchAddressKey.setName(addressList4Del.get(i).getName());    // アドレス名
			// アドレスオブジェクトテーブルを検索し、削除対象アドレスのSEQ_NOを取得
			List<Addresses> addressObjectList;
			try{
				// アドレスオブジェクトテーブルを検索し、削除対象アドレスのSEQ_NOを取得
				addressObjectList = queryDAO.executeForObjectList(
						"AddressesBLogic-1", searchAddressKey);
				seqNo = addressObjectList.get(0).getSeqNo();
	        } catch(Exception e) {
				// トランザクションロールバック
	    		TransactionUtil.setRollbackOnly();
	        	// コミット対象取得処理失敗ログ出力
	        	Object[] objectgArray = {e.getMessage()};
	        	log.fatal(messageAccessor.getMessage(
	        			"FK061002", objectgArray, params.getUvo()));
	        	messages.add("message", new BLogicMessage("DK060008"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
	        }

			// 検索条件設定（リンク有無判定用条件作成）
			AddressLinksInfo selectAddressLinks = new AddressLinksInfo();
			selectAddressLinks.setGrpLinkSeqNo(seqNo);
			selectAddressLinks.setSrcLinkSeqNo(seqNo);
			selectAddressLinks.setDstLinkSeqNo(seqNo);

			try{
			// 当該アドレスがリンクされている件数を取得
				count= queryDAO.executeForObject("AddressesBLogic-6", selectAddressLinks, Integer.class);
	        } catch(Exception e) {
				// トランザクションロールバック
	    		TransactionUtil.setRollbackOnly();
	        	// コミット対象取得処理失敗ログ出力
	        	Object[] objectgArray = {e.getMessage()};
	        	log.fatal(messageAccessor.getMessage(
	        			"FK061002", objectgArray, params.getUvo()));
	        	messages.add("message", new BLogicMessage("DK060008"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
	        }

			if (count > 0) {
				log.debug("オブジェクト削除不可:" + addressList4Del.get(i).getName());
				delNum++;

				// 削除名称を連結する。
				if(delNum == 1) {
					log.debug("削除名称設定(1回目:" + addressList4Del.get(i).getName());
					delStr = addressList4Del.get(i).getName();
				} else {
					log.debug("削除名称設定(2回目以降:" + addressList4Del.get(i).getName());
					delStr = delStr + ", " + addressList4Del.get(i).getName();
				}
				if(delSize == delNum) {
					// 検索結果が1件以上の場合、削除不可エラー
		        	// トランザクションロールバック
		        	TransactionUtil.setRollbackOnly();
					// レスポンスデータを作成し返却
					// 削除不可エラーメッセージを出力する。
					Object[] objectgArray = {delStr};
					log.debug(messageAccessor.getMessage(
		        			"EK061001", objectgArray, params.getUvo()));
					messages.add("message",new BLogicMessage("DK060070", objectgArray));
					result.setErrors(messages);
					result.setResultString("failure");
		        	return result;
				}
			// 削除可能
			} else {
				log.debug("削除処理実施" + addressList4Del.get(i).getName());
				try{
					// アドレスオブジェクトテーブルのレコードの変更フラグを'3'（削除）に更新
					updateDAO.execute("AddressesBLogic-7", seqNo);
				} catch(Exception e) {
		        	// トランザクションロールバック
		        	TransactionUtil.setRollbackOnly();
		        	// コミット対象取得処理失敗ログ出力
		        	Object[] objectgArray = {e.getMessage()};
		        	log.fatal(messageAccessor.getMessage(
		        			"FK061002", objectgArray, params.getUvo()));
		        	messages.add("message", new BLogicMessage("DK060008"));
					result.setErrors(messages);
					result.setResultString("failure");
					return result;
				}
			}
		}

		// 一覧情報検索共通処理
		AddressesOutput output = new AddressesOutput();
		try{
	        AddressList addressList = new AddressList();
			List<SelectAddressList> addressesOutputList =
				addressList.getAddressList(queryDAO, uvo);
			output.setAddressList(addressesOutputList);
		} catch(Exception e) {
			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"FK061002", objectgArray, params.getUvo()));
        	messages.add("message", new BLogicMessage("DK060004"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

		 if(delNum != 0) {
	    	// 削除不可オブジェクトの名称出力
	    	log.debug("削除不可オブジェクト名称:" + delStr);
			Object[] objectgArray = {delStr};
			output.setMessage(messageAccessor.getMessage("DK060070", objectgArray));
		 }
		// レスポンス返却
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("DelAddressesBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
