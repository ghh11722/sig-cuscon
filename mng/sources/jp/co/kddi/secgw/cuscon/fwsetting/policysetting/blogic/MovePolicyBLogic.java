/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : MovePolicy.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic;

import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.SecurityRules;
import jp.co.kddi.secgw.cuscon.fwsetting.common.PolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.MovePolicyInput;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.PolicyOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : MovePolicyBLogic
 * 機能概要 :ポリシーの移動を選択時の処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class MovePolicyBLogic implements BLogic<MovePolicyInput> {

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;
	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic.MovePolicyBLogic");
	private MessageAccessor messageAccessor = null;
	/**
	 * メソッド名 :execute
	 * 機能概要 :ポリシーの優先度を変更する。
	 * @param param MovePolicyInput入力クラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(MovePolicyInput param) {
		log.debug("MovePolicyBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");
        // エラーメッセージ
        BLogicMessages messages = new BLogicMessages();

        // 選択したリストからオブジェクトを取得する。
        SelectPolicyList selectPolicy =
        	param.getPolicyList().get(param.getIndex());

        int index = 0;
        try {
	        // 一番上へ、一つ上へを選択した場合
	        if(param.getMoveType() == 0 || param.getMoveType() == 2) {
	        	log.debug("一番上へor一つ上へを選択");
	        	// 行番号(上へ移動)更新処理を行う。
	        	index = updateTopMove(param, selectPolicy, param.getUvo());

	        // 一番下へ、一つ下へを選択した場合
	        } else {
	        	log.debug("一番下へ、一つ下へを選択");
	        	// 行番号(下へ移動)更新処理を行う。
	        	index = updateBottomMove(param, selectPolicy, param.getUvo());

	        }
        } catch(Exception e) {
			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"EK051001", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK050005"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

        // ポリシーリストクラスを生成する。
        PolicyList policy = new PolicyList();

        // ポリシーリスト
        List<SelectPolicyList> policyList = null;
        try {
	        // ポリシーリストを取得する。
	        policyList = policy.getPolicylist(queryDAO, param.getUvo(),
	        					CusconConst.ZERO_GENE, CusconConst.POLICY);
        } catch(Exception e) {
    		// DBアクセスエラー
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"EK051001", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK050005"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}


        // ビジネスロジックの出力クラスに結果を設定する
        PolicyOutput out = new PolicyOutput();
        out.setPolicyList(policyList);
        out.setIndex(index);
        result.setResultObject(out);
        result.setResultString("success");
        log.debug("MovePolicyBLogic処理終了");

		return result;
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		 log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		 log.debug("setUpdateDAO処理終了");
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : updateTopMove
	 * 機能概要 :行番号(上へ移動)更新処理を行う。<br>
	 *           【一番上へ選択】<br>選択したポリシー情報の行番号を1とし、
	 *           選択ポリシーより小さい行番号に対して1を加える。<br>
	 *           【一つ上へ選択】<br>
	 *           選択したポリシー情報の行番号から1を引き、
	 *           選択ポリシーより1つ小さい行番号に対して1を加える。<br>
	 * @param param インプットパラメータ
	 * @param selectPolicy 選択したポリシー情報
	 * @throws Exception
	 */
	private int updateTopMove(MovePolicyInput param,
								SelectPolicyList selectPolicy, CusconUVO uvo) throws Exception {
		log.debug("updateTopMove処理開始");

		// 行番号が1場合、処理を終了する。
		if(selectPolicy.getLineNo() == 1) {
			log.debug("選択行が1行目のため何もしない。");
			return 0;
		}

		// 検索条件を設定する。
		SecurityRules base = new SecurityRules();
		base.setVsysId(uvo.getVsysId());
		base.setGenerationNo(CusconConst.ZERO_GENE);
		base.setModFlg(CusconConst.DEL_NUM);
		int index = 0;
		// 一番上へを選択した場合
		if(param.getMoveType() == 0) {
			log.debug("一番上へを選択");
			// 行番号を設定する。
			base.setLineNo(selectPolicy.getLineNo());
			try {
				// 選択行より小さい行番号に1を加える。
				updateDAO.execute("MovePolicy-1", base);

			} catch(Exception e) {
	    		// トランザクションロールバック
	    		TransactionUtil.setRollbackOnly();
	    		// DBアクセスエラー
	    		Object[] objectgArray = {e.getMessage()};
	    		log.fatal(messageAccessor.getMessage("FK051004", objectgArray, uvo));
	    		throw e;
	    	}

			// 更新用行番号を1に設定する。
			base.setLineNo(1);

		// 一つ上へを選択した場合
		} else {
			log.debug("一つ上へを選択");
			index = selectPolicy.getLineNo() - 2;
			// 行番号を設定する。
			base.setLineNo(selectPolicy.getLineNo());
			try {
				// 選択行より1つ小さい行番号に1を加える。
				updateDAO.execute("MovePolicy-7", base);
			} catch(Exception e) {
	    		// トランザクションロールバック
	    		TransactionUtil.setRollbackOnly();
	    		// DBアクセスエラー
	    		Object[] objectgArray = {e.getMessage()};
	    		log.fatal(messageAccessor.getMessage("FK051004", objectgArray, uvo));
	    		throw e;
	    	}

			// 更新用行番号を現在の行番号から1を引いた値に設定する。
			base.setLineNo(selectPolicy.getLineNo() - 1);
		}

		base.setName(selectPolicy.getName());
		// 選択行の情報
		List<SecurityRules> selectList = null;
		try {
			// 選択行の情報を取得する。
			selectList = queryDAO.executeForObjectList("MovePolicy-2", base);
		} catch(Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		log.fatal(messageAccessor.getMessage("FK051005", objectgArray, uvo));
    		throw e;
    	}

		// 選択行の変更フラグが1の場合
		base.setModFlg(CusconConst.ADD_NUM);
		if(selectList.get(0).getModFlg() != 1) {
			log.debug("選択行の変更フラグが1");

			try {
				// 行番号、変更フラグを更新する。
				updateDAO.execute("MovePolicy-3", base);
			} catch(Exception e) {
	    		// トランザクションロールバック
	    		TransactionUtil.setRollbackOnly();
	    		// DBアクセスエラー
	    		Object[] objectgArray = {e.getMessage()};
	    		log.fatal(messageAccessor.getMessage("FK051004", objectgArray, uvo));
	    		throw e;
	    	}

		} else {
			log.debug("選択行の変更フラグが1以外");
			try {
				// 行番号を更新する。
				updateDAO.execute("MovePolicy-4", base);
			} catch(Exception e) {
	    		// トランザクションロールバック
	    		TransactionUtil.setRollbackOnly();
	    		// DBアクセスエラー
	    		Object[] objectgArray = {e.getMessage()};
	    		log.fatal(messageAccessor.getMessage("FK051004", objectgArray, uvo));
	    		throw e;
	    	}
		}


		log.debug("updateTopMove処理終了");

		return index;
	}

	/**
	 * メソッド名 : updateBottomMove
	 * 機能概要 :行番号(下へ移動)更新処理を行う。<br>
	 *           【一番下へ選択】<br>選択したポリシー情報の行番号を最大行番号とし、
	 *           選択ポリシーより大きい行番から1を引く。<br>
	 *           【一つ下へ選択】<br>
	 *           選択したポリシー情報の行番号に1を加え、
	 *           選択ポリシーより1つ大きい行番号から1を引く。<br>
	 * @param param インプットパラメータ
	 * @param selectPolicy 選択したポリシー情報
	 * @throws Exception
	 */
	private int updateBottomMove(MovePolicyInput param,
									SelectPolicyList selectPolicy, CusconUVO uvo) throws Exception {

		log.debug("updateBottomMove処理開始");
		PolicyList policy = new PolicyList();

		// 検索条件を設定する。
		SecurityRules base = new SecurityRules();
		base.setVsysId(uvo.getVsysId());
		base.setGenerationNo(CusconConst.ZERO_GENE);
		base.setModFlg(CusconConst.DEL_NUM);
		base.setNonSubject(policy.getHiddenObject());

		// 最大行番号
        int maxNum = 0;
        int index = 0;
		try {
	        // 最大行番号を取得する。
	        maxNum = queryDAO.executeForObject("MovePolicy-5", base, java.lang.Integer.class);
		} catch(Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		log.fatal(messageAccessor.getMessage("FK051005", objectgArray, uvo));
    		throw e;
    	}

		// 行番号が最大行番号の場合、処理を終了する。
		if(selectPolicy.getLineNo() == maxNum) {
			log.debug("選択行が最大行のため何もしない。");
			return selectPolicy.getLineNo() - 1;
		}

		// 一番下へを選択した場合
		if(param.getMoveType() == 1) {
			log.debug("一番下へを選択");
			// 行番号を設定する。
			base.setLineNo(selectPolicy.getLineNo());

			try {
				// 選択行より大きい行番号から1を引く。
				updateDAO.execute("MovePolicy-6", base);
			} catch(Exception e) {
	    		// トランザクションロールバック
	    		TransactionUtil.setRollbackOnly();
	    		// DBアクセスエラー
	    		Object[] objectgArray = {e.getMessage()};
	    		log.fatal(messageAccessor.getMessage("FK051004", objectgArray, uvo));
	    		throw e;
	    	}
			index = maxNum - 1;
			// 更新用行番号を1に設定する。
			base.setLineNo(maxNum);

		// 一つ下へを選択した場合
		} else {
			log.debug("一つ下へを選択");

			// 行番号を設定する。
			base.setLineNo(selectPolicy.getLineNo());

			try {
				// 選択行より1つ大きい行番号から1を引く。
				updateDAO.execute("MovePolicy-11", base);
			} catch(Exception e) {
	    		// トランザクションロールバック
	    		TransactionUtil.setRollbackOnly();
	    		// DBアクセスエラー
	    		Object[] objectgArray = {e.getMessage()};
	    		log.fatal(messageAccessor.getMessage("FK051004", objectgArray, uvo));
	    		throw e;
	    	}

			index = selectPolicy.getLineNo();
			// 更新用行番号を現在の行番号から1を加えた値に設定する。
			base.setLineNo(selectPolicy.getLineNo() + 1);
		}

		base.setName(selectPolicy.getName());
		// 選択行の情報
		List<SecurityRules> selectList = null;
		try {
			// 選択行の情報を取得する。
			selectList = queryDAO.executeForObjectList("MovePolicy-2", base);
		} catch(Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		log.fatal(messageAccessor.getMessage("FK051005", objectgArray, uvo));
    		throw e;
    	}

		// 選択行の変更フラグが1の場合
		base.setModFlg(CusconConst.ADD_NUM);
		if(selectList.get(0).getModFlg() != 1) {
			log.debug("選択行の変更フラグが1");

			try {
				// 行番号、変更フラグを更新する。
				updateDAO.execute("MovePolicy-3", base);
			} catch(Exception e) {
	    		// トランザクションロールバック
	    		TransactionUtil.setRollbackOnly();
	    		// DBアクセスエラー
	    		Object[] objectgArray = {e.getMessage()};
	    		log.fatal(messageAccessor.getMessage("FK051004", objectgArray, uvo));
	    		throw e;
	    	}

		} else {
			log.debug("選択行の変更フラグが1以外");

			try {
				// 行番号を更新する。
				updateDAO.execute("MovePolicy-4", base);
			} catch(Exception e) {
	    		// トランザクションロールバック
	    		TransactionUtil.setRollbackOnly();
	    		// DBアクセスエラー
	    		Object[] objectgArray = {e.getMessage()};
	    		log.fatal(messageAccessor.getMessage("FK051004", objectgArray, uvo));
	    		throw e;
	    	}
		}
		log.debug("updateBottomMove処理終了");

		return index;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
