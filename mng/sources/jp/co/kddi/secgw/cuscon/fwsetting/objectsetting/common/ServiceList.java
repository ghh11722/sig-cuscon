/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ServiceList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/20     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.common.vo.Services;
import jp.co.kddi.secgw.cuscon.common.vo.ServicesMaster;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectServiceList;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : ServiceList
 * 機能概要 : サービス一覧を表示するための情報を取得する。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/20
 *          新規作成
 * @see
 */
public class ServiceList {

	private static Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.ServiceList");

	// メッセージクラス
    private static MessageAccessor messageAccessor = null;

	private static final int MASTER = 0;
	private static final int NON_MASTER = 1;

    /**
     * メソッド名 : コンストラクタ
     * 機能概要 : -
     * @param queryDAO
     * @param uvo
     */
    public ServiceList() {
    	log.debug("ServiceListコンストラクタ2処理開始");
    	log.debug("ServiceListコンストラクタ2処理終了");
    }

	/**
	 * メソッド名 :getServiceList
	 * 機能概要 : サービス一覧情報取得処理
	 * @param vsysId
	 * @return resultList
	 * @throws Exception
	 */
	public List<SelectServiceList> getServiceList(QueryDAO queryDAO,
										CusconUVO uvo) throws Exception {
		log.debug("getServiceList処理開始");
		// InputBase設定
		InputBase inputBase = new InputBase();
		inputBase.setVsysId(uvo.getVsysId());                         // Vsys-ID
		inputBase.setGenerationNo(CusconConst.ZERO_GENE);    // 世代番号
		inputBase.setModFlg(CusconConst.DEL_NUM);            // 変更フラグ
		// サービスマスタ情報取得
		List<ServicesMaster> serviceMasterList;
		// サービス情報取得
		List<Services> serviceList;
		try {
			// サービスマスタ情報取得
			serviceMasterList = new ArrayList<ServicesMaster>();
			serviceMasterList = queryDAO.executeForObjectList("CommonM_Services-1", null);

			// サービス情報取得
			serviceList = new ArrayList<Services>();
			serviceList = queryDAO.executeForObjectList("CommonT_Services-5", inputBase);
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		// Webフィルタの検索に失敗しました。{0}
    		log.fatal(messageAccessor.getMessage("FK061020", objectgArray, uvo));
    		throw e;
		}

		// 返却用リスト作成
		List <SelectServiceList> resultList = new ArrayList<SelectServiceList>();

		// 取得したサービスマスタ情報の件数分、返却用リストに設定
		for(int i = 0; i < serviceMasterList.size(); i++) {
			log.debug("サービスマスタ情報の件数:" + i);
			SelectServiceList selectServiceListOutput = new SelectServiceList();
			selectServiceListOutput.setName(serviceMasterList.get(i).getName());
			selectServiceListOutput.setProtocol(serviceMasterList.get(i).getProtocol());
			selectServiceListOutput.setPort(serviceMasterList.get(i).getPort());
			selectServiceListOutput.setObjectType(MASTER);
			resultList.add(selectServiceListOutput);
		}

		// 取得したサービス情報の件数分、返却用リストに設定
		for(int i = 0; i < serviceList.size(); i++) {
			log.debug("サービス情報の件数:" + i);
			SelectServiceList selectServiceListOutput = new SelectServiceList();
			selectServiceListOutput.setName(serviceList.get(i).getName());
			selectServiceListOutput.setProtocol(serviceList.get(i).getProtocol());
			selectServiceListOutput.setPort(serviceList.get(i).getPort());
			selectServiceListOutput.setObjectType(NON_MASTER);
			resultList.add(selectServiceListOutput);
		}
		log.debug("getServiceList処理終了");
		return resultList;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor msgAcc){
		log.debug("setMessageAccessor処理開始");
	    setMsgAcc(msgAcc);
	    log.debug("setMessageAccessor処理終了");
	}

	/**
	 * メソッド名 : setMsgAcc
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public static void setMsgAcc(MessageAccessor messageAccessor) {
		log.debug("setMsgAcc処理開始");
		ServiceList.messageAccessor = messageAccessor;
		log.debug("setMsgAcc処理終了");
	}

	/**
	 * メソッド名 : getMessage
	 * 機能概要 : messageAccessorをゲットする。
	 * @return MessageAccessor
	 */
	public static MessageAccessor getMessage() {
		log.debug("getMessage処理");
		return messageAccessor;
	}
}
