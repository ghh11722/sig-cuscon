/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AplFltGrpList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.common;

import java.util.ArrayList;
import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.ApplicationLink;
import jp.co.kddi.secgw.cuscon.common.vo.SecurityRules;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SeqAndNameInfo;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

import org.apache.log4j.Logger;

/**
 * クラス名 : AplFltGrpList
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class AplFltGrpList {

	// オブジェクトタイプ定数
	private static final int APL = 0;    // アプリケーションオブジェクトマスタ
	private static final int GRP = 1;    // アプリケーショングループオブジェクト
	private static final int FLT = 2;    // アプリケーションフィルタオブジェクト

	private static Logger log = Logger.getLogger(
	"jp.co.kddi.secgw.cuscon.fwsetting.policysetting.common.AplFltGrpList");

	// メッセージクラス
	private static MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 * @param queryDAO
	 * @param uvo
	 */
	public AplFltGrpList() {
	}

	/**
	 * メソッド名 :getApplicationGroupList
	 * 機能概要 : アプリケーショングループ一覧情報取得処理
	 * @param vsysId
	 */

	public List<SelectedAplInfo> getAplFltGrpList(QueryDAO queryDAO,
						CusconUVO uvo, String policyName) throws Exception {
		log.debug("getAplFltGrpList処理開始");

		// 選択中アプリケーションリスト
		List<SelectedAplInfo> selectedAplList = null;

		// 選択対象SEQ_NO
		int seqNo = 0;

		// 選択対象Name
		String name = null;
		/***********************************************************************
		 *  ①DBからセキュリティポリシー情報を取得（1件取得）
		 **********************************************************************/
		SecurityRules securityRules = new SecurityRules();
		securityRules.setVsysId(uvo.getVsysId());
		securityRules.setGenerationNo(CusconConst.ZERO_GENE);
		securityRules.setName(policyName);

		List<SecurityRules> selectList = null;

		try {
			// 編集中ポリシー情報取得
			selectList = queryDAO.executeForObjectList(
					"CommonT_SecurityRules-6", securityRules);
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		log.fatal(messageAccessor.getMessage("FK051004", objectgArray, uvo));
    		throw e;
		}

		// 編集中ポリシーのシーケンス番号取得
		seqNo = selectList.get(0).getSeqNo();


		/***********************************************************************
		 *  ②アプリケーションリンク情報取得（N件取得）
		 **********************************************************************/
		List<ApplicationLink> aplLinkList = null;
		try {
			// アプリケーションリンク情報を取得
			aplLinkList = queryDAO.executeForObjectList(
					"EditApplicationBLogic-1", seqNo);
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		log.fatal(messageAccessor.getMessage("FK051004", objectgArray, uvo));
    		throw e;
		}


		/***********************************************************************
		 *  ③レスポンス情報作成処理
		 **********************************************************************/
		if (aplLinkList.size() != 0 || aplLinkList != null) {
			log.debug("アプリケーションリンクがある場合");

			selectedAplList = new ArrayList<SelectedAplInfo>();
			for (int i = 0; i < aplLinkList.size(); i++) {
				log.debug("アプリケーションリンク処理中:" + i);

				SelectedAplInfo selectedAplInfo = new SelectedAplInfo();

				// タイプがアプリケーションマスタの場合
				if (aplLinkList.get(i).getObjectType() == APL) {
					log.debug("タイプがアプリケーションマスタの場合");

					selectedAplInfo.setType(CusconConst.TYPE_APL);

					List<SeqAndNameInfo> apl = null;
					try {
						// アプリケーション情報取得
						apl = queryDAO.executeForObjectList(
								"EditApplicationBLogic-2", aplLinkList.get(i).getLinkSeqNo());
					} catch(Exception e) {
						// トランザクションロールバック
						TransactionUtil.setRollbackOnly();
						// DBアクセスエラー
						Object[] objectgArray = {e.getMessage()};
						log.fatal(messageAccessor.getMessage("FK051004", objectgArray, uvo));
						throw e;
					}

					selectedAplInfo.setSeqNo(apl.get(0).getSeqNo());
					selectedAplInfo.setName(apl.get(0).getName());
				}

				// タイプがアプリケーショングループの場合
				if (aplLinkList.get(i).getObjectType() == GRP) {
					log.debug("タイプがアプリケーショングループの場合");

					selectedAplInfo.setType(CusconConst.TYPE_GRP);

					List<SeqAndNameInfo> grp = null;
					try {
						// アプリケーショングループ情報取得
						grp = queryDAO.executeForObjectList(
								"EditApplicationBLogic-3", aplLinkList.get(i).getLinkSeqNo());
					} catch(Exception e) {
						// トランザクションロールバック
						TransactionUtil.setRollbackOnly();
						// DBアクセスエラー
						Object[] objectgArray = {e.getMessage()};
						log.fatal(messageAccessor.getMessage("FK051004", objectgArray, uvo));
						throw e;
					}

					selectedAplInfo.setSeqNo(grp.get(0).getSeqNo());
					selectedAplInfo.setName(grp.get(0).getName());
				}
				// タイプがアプリケーションフィルタの場合
				if (aplLinkList.get(i).getObjectType() == FLT) {
					log.debug("タイプがアプリケーションフィルタの場合");

					selectedAplInfo.setType(CusconConst.TYPE_FLT);

					List<SeqAndNameInfo> flt = null;
					try {
						// アプリケーションフィルタ情報取得
						flt = queryDAO.executeForObjectList(
								"EditApplicationBLogic-4", aplLinkList.get(i).getLinkSeqNo());
					} catch(Exception e) {
						// トランザクションロールバック
						TransactionUtil.setRollbackOnly();
						// DBアクセスエラー
						Object[] objectgArray = {e.getMessage()};
						log.fatal(messageAccessor.getMessage("FK051004", objectgArray, uvo));
						throw e;
					}

					selectedAplInfo.setSeqNo(flt.get(0).getSeqNo());
					selectedAplInfo.setName(flt.get(0).getName());
				}
				selectedAplList.add(selectedAplInfo);
			}
		}


		/***********************************************************************
		 *  ④結果返却
		 **********************************************************************/
		log.debug("getAplFltGrpList処理終了");
		return selectedAplList;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor msgAcc){
		log.debug("setMessageAccessor処理開始");
		setMsgAcc(msgAcc);
		log.debug("setMessageAccessor処理終了");
	}

	/**
	 * メソッド名 : setMsgAcc
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public static void setMsgAcc(MessageAccessor messageAccessor) {
		log.debug("setMsgAcc処理開始");
		AplFltGrpList.messageAccessor = messageAccessor;
		log.debug("setMsgAcc処理終了");
	}

	/**
	 * メソッド名 : getMessage
	 * 機能概要 : messageAccessorをゲットする。
	 * @return MessageAccessor
	 */
	public static MessageAccessor getMessage() {
		log.debug("getMessage処理");
		return messageAccessor;
	}
}
