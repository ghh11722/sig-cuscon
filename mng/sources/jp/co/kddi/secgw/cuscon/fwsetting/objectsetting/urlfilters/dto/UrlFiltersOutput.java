/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UrlFilterOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/06     k_takenaka                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectUrlFilterList;

/**
 * クラス名 : UrlFilterOutput
 * 機能概要 :
 * 備考 :
 * @author k_takenaka
 * @version 1.0 k_takenaka
 *          Created 2011/06/06
 *          新規作成
 * @see
 */
public class UrlFiltersOutput {
	// Webフィルタ情報リスト
	private List<SelectUrlFilterList> urlFilterList = null;
	private String message = null;


	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * メソッド名 : urlFilterListのGetterメソッド
	 * 機能概要 : urlFilterListを取得する。
	 * @return urlFilterList
	 */
	public List<SelectUrlFilterList> getUrlFilterList() {
		return urlFilterList;
	}

	/**
	 * メソッド名 : urlFilterListのSetterメソッド
	 * 機能概要 : urlFilterListをセットする。
	 * @param urlFilterList
	 */
	public void setUrlFilterList(List<SelectUrlFilterList> urlFilterList) {
		this.urlFilterList = urlFilterList;
	}

}
