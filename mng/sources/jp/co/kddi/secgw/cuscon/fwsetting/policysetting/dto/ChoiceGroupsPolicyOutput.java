/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ChoiceGroupsOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;

/**
 * クラス名 : ChoiceGroupsPolicyOutput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/01
 *          新規作成
 * @see
 */
public class ChoiceGroupsPolicyOutput {

	// ポリシー名
	private String name = null;
	// アプリケーショングループリスト
	private List<SelectApplicationGroupList> groupList4Add = null;
	// セッション保持しているSelectedList(セッションデータ)
	private List<SelectedAplInfo> sessionAplList = null;

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : groupList4AddのGetterメソッド
	 * 機能概要 : groupList4Addを取得する。
	 * @return groupList4Add
	 */
	public List<SelectApplicationGroupList> getGroupList4Add() {
		return groupList4Add;
	}
	/**
	 * メソッド名 : groupList4AddのSetterメソッド
	 * 機能概要 : groupList4Addをセットする。
	 * @param groupList4Add
	 */
	public void setGroupList4Add(List<SelectApplicationGroupList> groupList4Add) {
		this.groupList4Add = groupList4Add;
	}
	/**
	 * メソッド名 : sessionAplListのGetterメソッド
	 * 機能概要 : sessionAplListを取得する。
	 * @return sessionAplList
	 */
	public List<SelectedAplInfo> getSessionAplList() {
		return sessionAplList;
	}
	/**
	 * メソッド名 : sessionAplListのSetterメソッド
	 * 機能概要 : sessionAplListをセットする。
	 * @param sessionAplList
	 */
	public void setSessionAplList(List<SelectedAplInfo> sessionAplList) {
		this.sessionAplList = sessionAplList;
	}
}
