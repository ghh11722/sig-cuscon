/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditActionOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto;

/**
 * クラス名 : EditActionOutput
 * 機能概要 :Action項目選択時の出力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class EditActionOutput {

	// ルール名
	private String name = null;

	// ルール適用フラグ
	private String action = null;

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * メソッド名 : actionのGetterメソッド
	 * 機能概要 : actionを取得する。
	 * @return action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * メソッド名 : actionのSetterメソッド
	 * 機能概要 : actionをセットする。
	 * @param action
	 */
	public void setAction(String action) {
		this.action = action;
	}
}
