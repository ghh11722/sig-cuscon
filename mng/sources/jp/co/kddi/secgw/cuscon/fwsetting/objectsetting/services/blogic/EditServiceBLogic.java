/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditServiceBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/10     kinoshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.blogic;


import org.apache.log4j.Logger;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.dto.EditServiceInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.dto.EditServiceOutput;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicResult;

/**
 * クラス名 : EditServiceBLogic
 * 機能概要 :
 * 備考 :
 * @author kinoshtt
 * @version 1.0 kinoshtt
 *          Created 2010/03/10
 *          新規作成
 * @see
 */
public class EditServiceBLogic implements BLogic<EditServiceInput> {

	private Logger log = Logger.getLogger(
	"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.blogic.EditServiceBLogic");

	/**
	 * メソッド名 : EditServiceBLogic
	 * 機能概要 :
	 * @param params
	 * @return
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(EditServiceInput params) {

		log.debug("EditServiceBLogic処理開始");
		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		result.setResultString("failure");

		// アウトプットオブジェクト生成
		EditServiceOutput out = new EditServiceOutput();

		// インデックス取得アウトプットオブジェクト生成
		int index = params.getIndex();

		// アウトプット情報設定
		out.setName(params.getServiceList().get(index).getName());
		out.setPort(params.getServiceList().get(index).getPort());
		out.setProtocol(params.getServiceList().get(index).getProtocol());

		// レスポンス返却
		result.setResultObject(out);
		result.setResultString("success");
	    log.debug("EditServiceBLogic処理終了");
		return result;
	}
}
