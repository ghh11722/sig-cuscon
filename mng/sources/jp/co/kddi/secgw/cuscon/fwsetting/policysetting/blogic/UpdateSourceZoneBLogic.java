/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateSourceZoneBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.SecurityRules;
import jp.co.kddi.secgw.cuscon.fwsetting.common.PolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.LinkInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.common.PolicyRulesList;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.PolicyOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.UpdateZoneInput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : UpdateSourceZoneBLogic
 * 機能概要 :SourceZone項目編集画面にて「OK」押下時の処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class UpdateSourceZoneBLogic implements BLogic<UpdateZoneInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;
	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic.UpdateSourceZoneBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 :execute
	 * 機能概要 :選択出所ゾーンを更新する。
	 * @param param SourceZone編集画面入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(UpdateZoneInput param) {

		log.debug("UpdateSourceZoneBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");
        // エラーメッセージ
        BLogicMessages messages = new BLogicMessages();

		// ポリシー設定共通クラスを生成する。
		PolicyRulesList policy = new PolicyRulesList();

		// 削除条件を設定する。
		SecurityRules base = new SecurityRules();
		base.setVsysId(param.getUvo().getVsysId());
		base.setGenerationNo(CusconConst.ZERO_GENE);
		base.setModFlg(CusconConst.MOD_NUM);
		base.setName(param.getName());
		base.setModName(param.getName());

		try {
			// 変更フラグを更新し、シーケンス番号を取得する。
			int seqNo = policy.updateModFlg(updateDAO, queryDAO, param.getUvo(), base);

			// シーケンス番号に紐付く出所ゾーンリンクを削除する。
			updateDAO.execute("DelPolicyBLogic-4", seqNo);

			// 選択するを選択した場合
			if(param.getRadio() == CusconConst.SELECT) {
				log.debug("登録処理を行う。index = 1");
				List<String> registList = new ArrayList<String>();

				String[] checkList = param.getCheckList().split(",");
				// 選択されたゾーンを取得する。
				for(int i=0; i<checkList.length; i++) {
					log.debug("選択されたゾーンを取得:checkList[" +
										i + "] = " + checkList[i]);
					registList.add(param.getZoneMasterList().get(
								Integer.parseInt(checkList[i])));
				}

				// 登録条件を設定する。
				LinkInfo link = new LinkInfo();
				link.setSeqNo(seqNo);
				link.setLinkNameList(registList);

				// 出所ゾーンリンクを登録する。
				updateDAO.execute("UpdateSourceZoneBLogic-1", link);
			}
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage(
					"FK051004", objectgArray, param.getUvo()));
			messages.add("message", new BLogicMessage("DK050013"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

		// ポリシーリストクラスを生成する。
        PolicyList rule = new PolicyList();

     // 選択された世代番号のポリシーリストを取得する。
        List<SelectPolicyList> policyList = null;
        try {
	        // 選択された世代番号のポリシーリストを取得する。
	        policyList = rule.getPolicylist(queryDAO, param.getUvo(),
	        				CusconConst.ZERO_GENE, CusconConst.POLICY);
        } catch(Exception e) {
			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"EK051001", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK050013"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

        // ビジネスロジックの出力クラスに結果を設定する
        PolicyOutput out = new PolicyOutput();
        out.setPolicyList(policyList);
        result.setResultObject(out);
        result.setResultString("success");
        log.debug("UpdateSourceZoneBLogic処理終了");
		return result;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}
	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
