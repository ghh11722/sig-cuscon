/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddAddressGroupBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/24     morisou                 初版作成
 * 2016/12/13     T.Yamazaki@Plum Systems アドレスグループ除外対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.blogic;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.AddressGroups;
import jp.co.kddi.secgw.cuscon.common.vo.Addresses;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.RegistLimitList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.dto.AddAddressGroupOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.dto.CommonInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.AddressesAndGroupsInfo;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : AddAddressGroupBLogic
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/24
 *          新規作成
 *          2.0 T.Yamazaki@Plum Systems Inc.
 *          Updated 2016/12/13
 *          アドレスグループ除外対応
 * @see
 */
public class AddAddressGroupBLogic implements BLogic<CommonInput> {

	// オブジェクトタイプ定数
	private static final int ADDRESS = 0;
	private static final int ADDRESS_GROUP = 1;

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.blogic.AddAddressGroupBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : BLogicResult
	 * 機能概要 :
	 * @param param
	 * @return result
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(CommonInput params) {
		log.debug("AddAddressGroupBLogic処理開始");

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		 // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");
		// 登録上限値を取得する。
        List<RegistLimitList> limitList;
        int registNum;
        InputBase inputBase = new InputBase();

		try {
	        // 登録上限値を取得する。
	        limitList = queryDAO.executeForObjectList("CommonM_SystemConstant-2", null);

	        inputBase.setVsysId(params.getUvo().getVsysId());
	        inputBase.setGenerationNo(CusconConst.ZERO_GENE);
	        inputBase.setModFlg(CusconConst.DEL_NUM);

	        // オブジェクトの登録数を取得する。
	        registNum = queryDAO.executeForObject("AddressGroupsBLogic-11", inputBase, java.lang.Integer.class);
		} catch (Exception e) {
			// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
			// DBアクセスエラーログ出力
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061005", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060012"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;

		}

        // 登録上限値とオブジェクト登録数の比較を行う。
        if(limitList.get(0).getAddrGrp() <= registNum) {
        	log.debug("登録上限値のため登録できませんでした。");
        	messages.add("message", new BLogicMessage("DK060003"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
        }

		// All Address & Groups 格納用リスト
        AddAddressGroupOutput output = new AddAddressGroupOutput();

        List<AddressesAndGroupsInfo> addressAndAddressGrp4Add =
        	new ArrayList<AddressesAndGroupsInfo>();

        // アドレス情報取得
		List<Addresses> addressObjectList;
        try {
			// アドレス情報取得
			addressObjectList = queryDAO.executeForObjectList(
					"AddressesBLogic-10", inputBase);
        } catch(Exception e) {
			// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
        	// DBアクセスエラーログ出力
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061005", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060012"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
        }

		// アドレス情報を格納
		for (int i = 0; i < addressObjectList.size(); i++) {
			log.debug("アドレス情報を格納:" + i);
			AddressesAndGroupsInfo addressesAndGroups = new AddressesAndGroupsInfo();
			addressesAndGroups.setSeqNo(addressObjectList.get(i).getSeqNo());      // SEQ_NO
			addressesAndGroups.setName(addressObjectList.get(i).getName());        // オブジェクト名
			addressesAndGroups.setObjectType(ADDRESS);                             // オブジェクトタイプ
			addressesAndGroups.setChkFlg(false);                                   // チェック情報
			addressAndAddressGrp4Add.add(addressesAndGroups);
		}

		// UPDATED: 2016.12 by Plum Systems Inc.
		// アドレスグループ除外対応 -->
		/*
		// アドレスグループ情報取得
		List<AddressGroups> addressGroupObjectList;
		try {
			// アドレスグループ情報取得
			addressGroupObjectList = queryDAO.executeForObjectList(
					"AddressGroupsBLogic-13", inputBase);
		 } catch(Exception e) {
			// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
        	// DBアクセスエラーログ出力
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("EK061002", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060012"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
	    }
		// アドレスグループ情報を格納
		for (int i = 0; i < addressGroupObjectList.size(); i++) {
			log.debug("アドレスグループ情報を格納:" + i);
			AddressesAndGroupsInfo addressesAndGroups = new AddressesAndGroupsInfo();
			addressesAndGroups.setSeqNo(addressGroupObjectList.get(i).getSeqNo());  // SEQ_NO
			addressesAndGroups.setName(addressGroupObjectList.get(i).getName());    // オブジェクト名
			addressesAndGroups.setObjectType(ADDRESS_GROUP);                        // オブジェクトタイプ
			addressesAndGroups.setChkFlg(false);                                    // チェック情報
			addressAndAddressGrp4Add.add(addressesAndGroups);
		}
		*/
		// --> アドレスグループ除外対応

		output.setAddressAndAddressGrp4Add(addressAndAddressGrp4Add);

		// レスポンス返却
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("AddAddressGroupBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
