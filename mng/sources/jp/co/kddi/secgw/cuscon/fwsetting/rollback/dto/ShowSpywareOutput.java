/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ShowSpywareOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/03     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto;

/**
 * クラス名 : ShowSpywareOutput
 * 機能概要 :スパイウェアチェック項目選択時の出力データクラス
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/03
 *          新規作成
 * @see
 */
public class ShowSpywareOutput {

	// スパイウェアチェックフラグ
	private String spywareFlg = null;

	/**
	 * メソッド名 : spywareFlgのGetterメソッド
	 * 機能概要 : spywareFlgを取得する。
	 * @return spywareFlg
	 */
	public String getSpywareFlg() {
		return this.spywareFlg;
	}
	/**
	 * メソッド名 : spywareFlgのSetterメソッド
	 * 機能概要 : spywareFlgをセットする。
	 * @param spywareFlg
	 */
	public void setSpywareFlg(String spywareFlg) {
		this.spywareFlg = spywareFlg;
	}
}
