/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : RollbackInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/27     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;

/**
 * クラス名 : RollbackInput
 * 機能概要 :ロールバック画面入力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/27
 *          新規作成
 * @see
 */
public class RollbackInput {

	// CusconUVO
	private CusconUVO uvo = null;

	// 選択世代番号(文字列)
	private int selectGeneNo = 0;


	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}

	/**
	 * メソッド名 : inGenerationNoのGetterメソッド
	 * 機能概要 : inGenerationNoを取得する。
	 * @return inGenerationNo
	 */
	public int getSelectGeneNo() {
		return selectGeneNo;
	}

	/**
	 * メソッド名 : inGenerationNoのSetterメソッド
	 * 機能概要 : inGenerationNoをセットする。
	 * @param inGenerationNo
	 */
	public void setSelectGeneNo(int selectGeneNo) {
		this.selectGeneNo = selectGeneNo;
	}
}