/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddressGroupsConfirmBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/08     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.blogic;

import java.util.ArrayList;
import java.util.List;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.dto.AddressGroupsConfirmInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.dto.AddressGroupsOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectAddressGroupList;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicResult;
import org.apache.log4j.Logger;

/**
 * クラス名 : AddressGroupsConfirmBLogic
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/08
 *          新規作成
 * @see
 */
public class AddressGroupsConfirmBLogic implements BLogic<AddressGroupsConfirmInput> {

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.blogic.AddressGroupsConfirmBLogic");

	/**
	 * メソッド名 :execute
	 * 機能概要 :アプリケーションフィルタ削除確認対象表示処理を行う。
	 * @param param ApplicationFiltersConfirmInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(AddressGroupsConfirmInput param) {

		log.debug("AddressGroupsConfirmBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");

		// アウトプットデータクラスに設定する。
        AddressGroupsOutput out = new AddressGroupsOutput();
        List<SelectAddressGroupList> addressGroupList = new ArrayList<SelectAddressGroupList>();

        String[] indexList = param.getCheckIndex().split(",");

        // 選択チェック分繰り返す。
        for(int i=0; i< indexList.length; i++) {
        	addressGroupList.add(param.getAddressGroupList().get(Integer.parseInt(indexList[i])));
        }

	    out.setAddressGroupList(addressGroupList);

	    result.setResultObject(out);
	    result.setResultString("success");

	    log.debug("AddressGroupsConfirmBLogic処理終了");
		return result;
	}
}
