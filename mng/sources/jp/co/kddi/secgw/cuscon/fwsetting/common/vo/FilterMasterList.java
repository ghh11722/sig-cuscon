/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : FilterMasterList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/24     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.common.vo;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.RiskInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectFilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SeqAndNameInfo;
import jp.terasoluna.fw.dao.QueryDAO;

/**
 * クラス名 : FilterMasterList
 * 機能概要 : フィルタ一覧（マスタ情報）を表示するための情報を取得する。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/24
 *          新規作成
 * @see
 */
public class FilterMasterList {

	List<SeqAndNameInfo> categoryList;              // カテゴリリスト
	List<SeqAndNameInfo> subCategoryList;           // サブカテゴリリスト
	List<SeqAndNameInfo> technologyList;            // テクノロジリスト
	List<RiskInfo> riskList;                        // リスクリスト
	SelectFilterMasterList resultList;              // 返却用リスト

	/**
     * QueryDAO。
     * Springによりインスタンス生成され設定される。
     */
    private QueryDAO queryDAO = null;

    /**
     * メソッド名 : コンストラクタ
     * 機能概要 : -
     * @param queryDAO
     * @param uvo
     */
    public FilterMasterList(QueryDAO queryDAO, CusconUVO uvo) {
    	this.queryDAO = queryDAO;
    }

	/**
	 * メソッド名 :getFilterMasterList
	 * 機能概要 : フィルタエリア情報取得処理
	 * @param
	 * @return resultList
	 */
	public SelectFilterMasterList getFilterMasterList() {

		// カテゴリマスタ情報取得
		categoryList = queryDAO.executeForObjectList("CommonM_Category-1", null);

		// サブカテゴリマスタ情報取得
		subCategoryList = queryDAO.executeForObjectList("CommonM_SubCategory-1", null);

		// テクノロジマスタ情報取得
		technologyList = queryDAO.executeForObjectList("CommonM_Technology-1", null);

		// リスク情報取得（定数）
		RiskInfo riskInfo = new RiskInfo();
		riskInfo.setName(CusconConst.RISK_ONE);
		riskList.add(riskInfo);

		riskInfo = new RiskInfo();
		riskInfo.setName(CusconConst.RISK_TWO);
		riskList.add(riskInfo);

		riskInfo = new RiskInfo();
		riskInfo.setName(CusconConst.RISK_THREE);
		riskList.add(riskInfo);

		riskInfo = new RiskInfo();
		riskInfo.setName(CusconConst.RISK_FOUR);
		riskList.add(riskInfo);

		riskInfo = new RiskInfo();
		riskInfo.setName(CusconConst.RISK_FIVE);
		riskList.add(riskInfo);

		// 各マスタ情報リストをセット
		resultList.setCategoryList(categoryList);
		resultList.setSubCategoryList(subCategoryList);
		resultList.setTechnologyList(technologyList);
		resultList.setRiskList(riskList);

		// 結果を返却
		return resultList;
	}
}
