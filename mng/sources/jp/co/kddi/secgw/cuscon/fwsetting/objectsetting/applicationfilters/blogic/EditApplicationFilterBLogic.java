/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditApplicationFilterBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/25     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.blogic;

import java.util.ArrayList;
import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.ApplicationFilters;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.fwsetting.common.FilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.dto.EditApplicationFilterInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.dto.EditApplicationFilterOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.FilterInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.RiskInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationList4Search;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectFilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectSearchList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SeqAndNameInfo;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;
import org.apache.log4j.Logger;

/**
 * クラス名 : EditApplicationFilterBLogic
 * 機能概要 : アプリケーションフィルター編集画面を表示する。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/25
 *          新規作成
 * @see
 */
public class EditApplicationFilterBLogic implements BLogic<EditApplicationFilterInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.blogic.EditApplicationFilterBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 : アプリケーションフィルタ編集画面を表示する。
	 * @param param EditApplicationFilterInput 入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(EditApplicationFilterInput params) {

		log.debug("EditApplicationFilterBLogic処理開始");

		// マスタ情報用変数
		SelectFilterMasterList selectFilterMasterList = null;        // フィルタマスタ情報

		// 返却用変数
		EditApplicationFilterOutput output = null;                   // 返却用リスト
		SelectFilterMasterList filterList4Return = null;             // 返却用フィルタリスト
		List<SeqAndNameInfo> categoryList4Return = null;             // 返却用カテゴリリスト
		List<SeqAndNameInfo> subCategoryList4Return = null;          // 返却用サブカテゴリリスト
		List<SeqAndNameInfo> technologyList4Return = null;           // 返却用テクノロジリスト
		List<RiskInfo> riskList4Return = null;                       // 返却用リスクリスト
		List<SelectApplicationList4Search> aplList4Return = null;    // 返却用アプリケーションリスト

		// アプリケーション検索用リスト
		List<Integer> category = null;                               // 検索条件用カテゴリリスト
		List<Integer> subCategory = null;                            // 検索条件用サブカテゴリリスト
		List<Integer> technology = null;                             // 検索条件用テクノロジリスト
		List<Integer> risk = null;                                   // 検索条件用リスクリスト

		// 処理用変数
		ArrayList<String> editCategoryList = null;                   // 編集中カテゴリリスト
		ArrayList<String> editSubCategoryList = null;                // 編集中サブカテゴリリスト
		ArrayList<String> editTechnologyList = null;                 // 編集中テクノロジリスト
		ArrayList<Integer> editRiskuList = null;                     // 編集中リスクリスト

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		 // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得
		CusconUVO uvo = params.getUvo();
		String vsysId = uvo.getVsysId();

		// InputBase設定
		InputBase inputBase = new InputBase();
		inputBase.setVsysId(vsysId);                         // Vsys-ID
		inputBase.setGenerationNo(CusconConst.ZERO_GENE);    // 世代番号
		inputBase.setModFlg(CusconConst.DEL_NUM);            // 変更フラグ

		FilterMasterList filterMasterList;
		List<SeqAndNameInfo> categoryMaster;
		List<SeqAndNameInfo> subCategoryMaster;
		List<SeqAndNameInfo> technologyMaster;
		List<RiskInfo> riskMaster;

		try {
			/***********************************************************************
			 *  ①フィルタマスタ情報取得
			 **********************************************************************/
			// フィルタエリア情報取得共通処理（フィルタ系マスタ情報取得）
			filterMasterList = new FilterMasterList();
			selectFilterMasterList = filterMasterList.getFilterMasterList(queryDAO, uvo);

		} catch(Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061029", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060023"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

		// フィルタエリア情報からカテゴリマスタ情報取得
		categoryMaster = selectFilterMasterList.getCategoryList();

		// フィルタエリア情報からサブカテゴリマスタ情報取得
		subCategoryMaster = selectFilterMasterList.getSubCategoryList();

		// フィルタエリア情報からテクノロジマスタ情報取得
		technologyMaster = selectFilterMasterList.getTechnologyList();

		// フィルタエリア情報からリスクマスタ情報取得
		riskMaster = selectFilterMasterList.getRiskList();


		/***********************************************************************
		 *  ②編集中フィルタ情報取得
		 **********************************************************************/
		// 編集用リスト生成
		editCategoryList = new ArrayList<String>();
		editSubCategoryList = new ArrayList<String>();
		editTechnologyList = new ArrayList<String>();
		editRiskuList = new ArrayList<Integer>();


		// 検索条件設定
		ApplicationFilters searchFltKey = new ApplicationFilters();
		searchFltKey.setVsysId(vsysId);                         // Vsys-ID
		searchFltKey.setGenerationNo(CusconConst.ZERO_GENE);    // 世代番号
		searchFltKey.setModFlg(CusconConst.DEL_NUM);            // 更新フラグ
		searchFltKey.setName(params.getName());                 // アプリケーションフィルタ名

		List<FilterInfo> filterList;

		try {
			// 名称をkeyに、編集中のフィルタ情報を取得
			filterList = queryDAO.executeForObjectList("ApplicationFiltersBLogic-9", searchFltKey);

		} catch(Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061012", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060023"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

		// フィルタ情報の件数分、以下の処理を行う（編集中フィルタ名に紐づく各フィルタリング情報を取得）
		for (int i = 0; i < filterList.size(); i++) {

			// カテゴリがnullでないならリストにadd
			if (filterList.get(i).getCategory() != null) {
				log.debug("カテゴリリストが存在する場合");

				editCategoryList.add(filterList.get(i).getCategory());
			}
			// サブカテゴリがnullでないならリストにadd
			if (filterList.get(i).getSubCategory() != null) {
				log.debug("サブカテゴリリストが存在する場合");

				editSubCategoryList.add(filterList.get(i).getSubCategory());
			}
			// テクノロジがnullでないならリストにadd
			if (filterList.get(i).getTechnology() != null) {
				log.debug("テクノロジリストが存在する場合");

				editTechnologyList.add(filterList.get(i).getTechnology());
			}
			// リスクが0でないならリストにadd
			if (filterList.get(i).getRisk() != 0) {
				log.debug("リスクリストが存在する場合");

				editRiskuList.add(filterList.get(i).getRisk());
			}
		}


		/***********************************************************************
		 *  ③返却用フィルタエリアリスト編集処理
		 **********************************************************************/
		// 検索条件用リスト生成
		category = new ArrayList<Integer>();
		subCategory = new ArrayList<Integer>();
		technology = new ArrayList<Integer>();
		risk = new ArrayList<Integer>();

		// 返却用リスト生成
		categoryList4Return = new ArrayList<SeqAndNameInfo>();
		subCategoryList4Return = new ArrayList<SeqAndNameInfo>();
		technologyList4Return = new ArrayList<SeqAndNameInfo>();
		riskList4Return = new ArrayList<RiskInfo>();
		aplList4Return = new ArrayList<SelectApplicationList4Search>();

		// カテゴリリスト設定
		for (int i = 0; i < categoryMaster.size(); i++) {
			SeqAndNameInfo categoryInfo = new SeqAndNameInfo();

			// 名称がマスタ情報に一致する場合、チェックON設定
			for (int j = 0; j < editCategoryList.size(); j++) {
				if (categoryMaster.get(i).getName().equals(editCategoryList.get(j))) {
					log.debug("カテゴリ名が一致した場合");

					// チェックON
					categoryInfo.setChkFlg(true);

					// ④の処理で利用する検索条件に加える
					category.add(categoryMaster.get(i).getSeqNo());
				}
			}
			categoryInfo.setSeqNo(categoryMaster.get(i).getSeqNo());
			categoryInfo.setName(categoryMaster.get(i).getName());
			categoryList4Return.add(categoryInfo);
		}

		// サブカテゴリ設定
		for (int i = 0; i < subCategoryMaster.size(); i++) {
			SeqAndNameInfo subCategoryInfo = new SeqAndNameInfo();

			// 名称がマスタ情報に一致する場合、チェックON設定
			for (int j = 0; j < editSubCategoryList.size(); j++) {
				if (subCategoryMaster.get(i).getName().equals(editSubCategoryList.get(j))) {
					log.debug("サブカテゴリ名が一致した場合");

					// チェックON
					subCategoryInfo.setChkFlg(true);

					// ④の処理で利用する検索条件に加える
					subCategory.add(subCategoryMaster.get(i).getSeqNo());
				}
			}
			subCategoryInfo.setSeqNo(subCategoryMaster.get(i).getSeqNo());
			subCategoryInfo.setName(subCategoryMaster.get(i).getName());
			subCategoryList4Return.add(subCategoryInfo);
		}

		// テクノロジ設定
		for (int i = 0; i < technologyMaster.size(); i++) {
			SeqAndNameInfo technologyInfo = new SeqAndNameInfo();

			// 名称がマスタ情報に一致する場合、チェックON設定
			for (int j = 0; j < editTechnologyList.size(); j++) {
				if (technologyMaster.get(i).getName().equals(editTechnologyList.get(j))) {
					log.debug("テクノロジ名が一致した場合");

					// チェックON
					technologyInfo.setChkFlg(true);

					// ④の処理で利用する検索条件に加える
					technology.add(technologyMaster.get(i).getSeqNo());
				}
			}
			technologyInfo.setSeqNo(technologyMaster.get(i).getSeqNo());
			technologyInfo.setName(technologyMaster.get(i).getName());
			technologyList4Return.add(technologyInfo);
		}

		// リスク設定
		for (int i = 0; i < riskMaster.size(); i++) {
			RiskInfo riskInfo = new RiskInfo();

			// 名称がマスタ情報に一致する場合、チェックON設定
			for (int j = 0; j < editRiskuList.size(); j++) {
				if (riskMaster.get(i).getName().equals(editRiskuList.get(j))) {
					log.debug("リスク名が一致した場合");

					// チェックON
					riskInfo.setChkFlg(true);

					// ④の処理で利用する検索条件に加える
					risk.add(riskMaster.get(i).getName());
				}
			}
			riskInfo.setName(riskMaster.get(i).getName());
			riskList4Return.add(riskInfo);
		}

		// カテゴリ、サブカテゴリ、テクノロジ、リスクの全リストを返却用フィルタリストの中にset
		// ここで画面に出力するフィルタエリア情報 + チェックのon/off状態までを一つのリストに格納
		filterList4Return = new SelectFilterMasterList();
		filterList4Return.setCategoryList(categoryList4Return);
		filterList4Return.setSubCategoryList(subCategoryList4Return);
		filterList4Return.setTechnologyList(technologyList4Return);
		filterList4Return.setRiskList(riskList4Return);


		/***********************************************************************
		 *  ④返却用アプリケーションリスト編集処理
		 **********************************************************************/
		// フィルタで絞り込みをしたアプリケーション一覧情報を取得

		// 検索条件用各リストをデータクラスにセット
		SelectSearchList selectSearchList = new SelectSearchList();
		selectSearchList.setCategory(category);
		selectSearchList.setSubCategory(subCategory);
		selectSearchList.setTechnology(technology);
		selectSearchList.setRisk(risk);

		try {
			// 検索結果取得
			aplList4Return = queryDAO.executeForObjectList("ApplicationFiltersBLogic-17", selectSearchList);

		} catch(Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061012", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060023"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}


		/***********************************************************************
		 *  ⑤結果セット
		 **********************************************************************/
		output = new EditApplicationFilterOutput();
		output.setName(params.getName());            // アプリケーションフィルタ名
		output.setFilterList(filterList4Return);     // フィルタリスト
		output.setApplicationList(aplList4Return);   // アプリケーションリスト

		// レスポンス返却
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("EditApplicationFilterBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
