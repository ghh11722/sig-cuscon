/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : RollbackPopupBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.rollback.blogic;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto.RollbackEndInput;
import jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto.RollbackOutput;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicResult;

/**
 * クラス名 : RollbackPopupBLogic
 * 機能概要 : ロールバック確認画面の業務ロジック
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/01
 *          新規作成
 * @see
 */
public class RollbackPopupBLogic implements BLogic<RollbackEndInput> {

	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.rollback.blogic.RollbackPopupBLogic");
	/**
	 * メソッド名 :execute
	 * 機能概要 : ロールバック画面のアウトプットをロールバック処理中画面へ渡す。
	 * @param param RollbackPopupInput
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(RollbackEndInput param) {

		log.debug("RollbackPopupBLogic処理開始");
		BLogicResult result = new BLogicResult();

		// ロールバック確認画面データクラス(アウトプット)の生成
		RollbackOutput out = new RollbackOutput();

		out.setPolicyList(param.getPolicyList());
		out.setSelectGeneNo(param.getSelectGeneNo());

		result.setResultObject(out);
		result.setResultString("success");
		log.debug("RollbackPopupBLogic処理終了");
		return result;
	}

}
