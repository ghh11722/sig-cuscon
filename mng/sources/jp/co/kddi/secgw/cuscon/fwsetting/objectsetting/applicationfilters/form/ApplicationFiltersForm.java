/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ApplicationFiltersForm.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     morisou                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.form;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.RiskInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationList4Search;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectFilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SeqAndNameInfo;
import jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx;

/**
 * クラス名 : ApplicationFiltersForm
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/01
 *          新規作成
 * @see
 */
public class ApplicationFiltersForm extends ValidatorActionFormEx {

	// シリアルバージョンID
	private static final long serialVersionUID = 1L;
	// アプリケーションフィルタリスト
	private List<SelectApplicationFilterList> applicationFilterList = new  ArrayList<SelectApplicationFilterList>();
	// フィルタ（マスタ情報）リスト
	private SelectFilterMasterList filterList = null;
	// アプリケーションリスト
	private List<SelectApplicationList4Search> applicationList = null;
	// カテゴリーリスト
	private List<SeqAndNameInfo> categoryList = new ArrayList<SeqAndNameInfo>();
	// サブカテゴリリスト
	private List<SeqAndNameInfo> subCategoryList = new ArrayList<SeqAndNameInfo>();
	// テクノロジリスト
	private List<SeqAndNameInfo> technologyList = new ArrayList<SeqAndNameInfo>();
	// リスクリスト
	private List<RiskInfo> riskList = new ArrayList<RiskInfo>();
	// 選択されたインデックス
	private String checkIndex = null;
	// 新アプリケーションフィルタ名
	private String newName = new String();
	// 現アプリケーションフィルタ名
	private String name = new String();
	// 旧アプリケーションフィルタ名
	private String oldName = new String();
	// 検索ワード
	private String searchWord = null;
	private String message = null;

	public void reset(ActionMapping mapping, HttpServletRequest request){
		 // メッセージ初期化
		 message=null;
		 // categoryListのチェック状態の初期化
		 for (int i = 0; i < this.categoryList.size(); i++){
			 categoryList.get(i).setChkFlg(false);
		 }
		 // subCategoryListのチェック状態の初期化
		 for (int i = 0; i < this.subCategoryList.size(); i++){
			 subCategoryList.get(i).setChkFlg(false);
		 }
		 // technologyListのチェック状態の初期化
		 for (int i = 0; i < this.technologyList.size(); i++){
			 technologyList.get(i).setChkFlg(false);
		 }
		 // riskListのチェック状態の初期化
		 for (int i = 0; i < this.riskList.size(); i++){
			 riskList.get(i).setChkFlg(false);
		 }
	}

	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * メソッド名 : categoryListのGetterメソッド
	 * 機能概要 : categoryListを生成し、取得する。
	 * @param cnt
	 * @return categoryList
	 */
	public Object getCategoryList(int cnt) {

		while (this.categoryList.size() <= cnt){
			this.categoryList.add(new SeqAndNameInfo());
		}

		return this.categoryList.get(cnt);
	}

	/**
	 * メソッド名 : subCategoryListのGetterメソッド
	 * 機能概要 : subCategoryListを生成し、取得する。
	 * @param cnt
	 * @return subCategoryList
	 */
	public Object getSubCategoryList(int cnt) {

		while (this.subCategoryList.size() <= cnt){
			this.subCategoryList.add(new SeqAndNameInfo());
		}

		return this.subCategoryList.get(cnt);
	}

	/**
	 * メソッド名 : technologyListのGetterメソッド
	 * 機能概要 : technologyListをを生成し、取得する。
	 * @param cnt
	 * @return technologyList
	 */
	public Object getTechnologyList(int cnt) {

		while (this.technologyList.size() <= cnt){
			this.technologyList.add(new SeqAndNameInfo());
		}

		return this.technologyList.get(cnt);
	}

	/**
	 * メソッド名 : riskListのGetterメソッド
	 * 機能概要 : riskListをを生成し、取得する。
	 * @param cnt
	 * @return riskList
	 */
	public Object getRiskList(int cnt) {

		while (this.riskList.size() <= cnt){
			this.riskList.add(new RiskInfo());
		}

		return this.riskList.get(cnt);
	}

	/**
	 * メソッド名 : newNameのGetterメソッド
	 * 機能概要 : newNameを取得する。
	 * @return newName
	 */
	public String getNewName() {
		return newName;
	}
	/**
	 * メソッド名 : newNameのSetterメソッド
	 * 機能概要 : newNameをセットする。
	 * @param newName
	 */
	public void setNewName(String newName) {
		this.newName = newName;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : oldNameのGetterメソッド
	 * 機能概要 : oldNameを取得する。
	 * @return oldName
	 */
	public String getOldName() {
		return oldName;
	}
	/**
	 * メソッド名 : oldNameのSetterメソッド
	 * 機能概要 : oldNameをセットする。
	 * @param oldName
	 */
	public void setOldName(String oldName) {
		this.oldName = oldName;
	}
	/**
	 * メソッド名 : applicationFilterListのGetterメソッド
	 * 機能概要 : applicationFilterListを取得する。
	 * @return applicationFilterList
	 */
	public List<SelectApplicationFilterList> getApplicationFilterList() {
		return applicationFilterList;
	}
	/**
	 * メソッド名 : applicationFilterListのSetterメソッド
	 * 機能概要 : applicationFilterListをセットする。
	 * @param applicationFilterList
	 */
	public void setApplicationFilterList(List<SelectApplicationFilterList> applicationFilterList) {
		this.applicationFilterList = applicationFilterList;
	}
	/**
	 * メソッド名 : filterListのGetterメソッド
	 * 機能概要 : filterListを取得する。
	 * @return filterList
	 */
	public SelectFilterMasterList getFilterList() {
		return filterList;
	}
	/**
	 * メソッド名 : filterListのSetterメソッド
	 * 機能概要 : filterListをセットする。
	 * @param filterList
	 */
	public void setFilterList(SelectFilterMasterList filterList) {
		this.filterList = filterList;
	}
	/**
	 * メソッド名 : applicationListのGetterメソッド
	 * 機能概要 : applicationListを取得する。
	 * @return applicationList
	 */
	public List<SelectApplicationList4Search> getApplicationList() {
		return applicationList;
	}
	/**
	 * メソッド名 : applicationListのSetterメソッド
	 * 機能概要 : applicationListをセットする。
	 * @param applicationList
	 */
	public void setApplicationList(List<SelectApplicationList4Search> applicationList) {
		this.applicationList = applicationList;
	}
	/**
	 * メソッド名 : categoryListのGetterメソッド
	 * 機能概要 : categoryListを取得する。
	 * @return categoryList
	 */
	public Object[] getCategoryList() {
			return categoryList.toArray();
	}
	/**
	 * メソッド名 : categoryListのSetterメソッド
	 * 機能概要 : categoryListをセットする。
	 * @param categoryList
	 */
	public void setCategoryList(List<SeqAndNameInfo> categoryList) {
		this.categoryList = categoryList;
	}
	/**
	 * メソッド名 : subCategoryListのGetterメソッド
	 * 機能概要 : subCategoryListを取得する。
	 * @return subCategoryList
	 */
	public Object[] getSubCategoryList() {

		return subCategoryList.toArray();
	}
	/**
	 * メソッド名 : subCategoryListのSetterメソッド
	 * 機能概要 : subCategoryListをセットする。
	 * @param subCategoryList
	 */
	public void setSubCategoryList(List<SeqAndNameInfo> subCategoryList) {
		this.subCategoryList = subCategoryList;
	}
	/**
	 * メソッド名 : technologyListのGetterメソッド
	 * 機能概要 : technologyListを取得する。
	 * @return technologyList
	 */
	public Object[] getTechnologyList() {
		return technologyList.toArray();
	}
	/**
	 * メソッド名 : technologyListのSetterメソッド
	 * 機能概要 : technologyListをセットする。
	 * @param technologyList
	 */
	public void setTechnologyList(List<SeqAndNameInfo> technologyList) {
		this.technologyList = technologyList;
	}
	/**
	 * メソッド名 : riskListのGetterメソッド
	 * 機能概要 : riskListを取得する。
	 * @return riskList
	 */
	public Object[] getRiskList() {

		return riskList.toArray();
	}
	/**
	 * メソッド名 : riskListのSetterメソッド
	 * 機能概要 : riskListをセットする。
	 * @param riskList
	 */
	public void setRiskList(List<RiskInfo> riskList) {
		this.riskList = riskList;
	}

	/**
	 * メソッド名 : checkIndexのGetterメソッド
	 * 機能概要 : checkIndexを取得する。
	 * @return checkIndex
	 */
	public String getCheckIndex() {
		return checkIndex;
	}

	/**
	 * メソッド名 : checkIndexのSetterメソッド
	 * 機能概要 : checkIndexをセットする。
	 * @param checkIndex
	 */
	public void setCheckIndex(String checkIndex) {
		this.checkIndex = checkIndex;
	}

	/**
	 * メソッド名 : searchWordのGetterメソッド
	 * 機能概要 : searchWordを取得する。
	 * @return searchWord
	 */
	public String getSearchWord() {
		return searchWord;
	}

	/**
	 * メソッド名 : searchWordのSetterメソッド
	 * 機能概要 : searchWordをセットする。
	 * @param searchWord
	 */
	public void setSearchWord(String searchWord) {
		this.searchWord = searchWord;
	}

	/**
	 * メソッド名 : serialversionuidのGetterメソッド
	 * 機能概要 : serialversionuidを取得する。
	 * @return serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
