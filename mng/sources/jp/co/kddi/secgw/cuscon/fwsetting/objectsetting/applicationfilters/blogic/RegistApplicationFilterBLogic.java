/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : RegistApplicationFilterBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/25     morisou                 初版作成
 * 2011/10/31     inoue@prosite           設定名称がアプリケーションコンテナマスタ内にあるか存在チェック
 * 										  あれば、「予約語のため利用できません」
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.blogic;

import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.ApplicationFilters;
import jp.co.kddi.secgw.cuscon.common.vo.ApplicationGroups;
import jp.co.kddi.secgw.cuscon.common.vo.ApplicationsMaster;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.dto.ApplicationFiltersOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.dto.RegistApplicationFilterInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.ApplicationFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.RiskInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SeqAndLinkSeqInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SeqAndNameInfo;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : RegistApplicationFilterBLogic
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/25
 *          新規作成
 * @see
 */
public class RegistApplicationFilterBLogic implements BLogic<RegistApplicationFilterInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.blogic.RegistApplicationFilterBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 : アプリケーションフィルタの登録処理を行う。
	 * @param params RegistApplicationFilterInput 入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(RegistApplicationFilterInput params) {

		log.debug("RegistApplicationFilterBLogic処理開始");

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		 // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得
		CusconUVO uvo = params.getUvo();
		String vsysId = uvo.getVsysId();

		// フィルタ選択フラグ
		boolean filterSelectFlt = false;

		/***********************************************************************
		 *  ①複数テーブル間存在チェック
		 **********************************************************************/
		List<ApplicationsMaster> aplList;
		List<ApplicationGroups> grpList;

		try {
			// M_Applicationsに今回登録したいフィルタ名と同名のレコードがないか検索
			aplList = queryDAO.executeForObjectList("ApplicationFiltersBLogic-19", params.getName());

			// T_ApplicationGroupsに今回登録したいフィルタ名と同名のレコードがないか検索
			// 検索条件設定
			ApplicationGroups grpSearchKey = new ApplicationGroups();
			grpSearchKey.setVsysId(vsysId);
			grpSearchKey.setGenerationNo(CusconConst.ZERO_GENE);
			grpSearchKey.setModFlg(CusconConst.DEL_NUM);
			grpSearchKey.setName(params.getName());

			grpList = queryDAO.executeForObjectList("ApplicationGroupsBLogic-12", grpSearchKey);

		}  catch (Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061012", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060024"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
        }

		if (aplList.size() > 0 || grpList.size() > 0 ) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
			// 重複エラーとしてレスポンスデータを作成し返却
        	log.debug("存在チェックエラー");
        	messages.add("message", new BLogicMessage("DK060060"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

//2011.10.31 add start inoue@prosite
		/***********************************************************************
		 *  ApplicationContainer存在チェック
		 **********************************************************************/
		List<String> aplContainList;
		try {
			// アプリケーションコンテナテーブルに対し、変更後アプリケーションフィルタ名と同名のレコードが既に存在しているか検索
			aplContainList = queryDAO.executeForObjectList("ApplicationContainerBLogic-1", params.getName());

		}  catch (Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061037", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060020"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
        }

		// DB検索結果が1件以上の場合
		if (!aplContainList.isEmpty()) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
        	messages.add("message", new BLogicMessage("DK060088"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}
//2011.10.31 add end inoue@prosite

		/***********************************************************************
		 *  ②存在チェック
		 **********************************************************************/
		// 検索条件設定
		ApplicationFilters searchApplicationFltKey = new ApplicationFilters();
		searchApplicationFltKey.setVsysId(vsysId);                         // Vsys-ID
		searchApplicationFltKey.setGenerationNo(CusconConst.ZERO_GENE);    // 世代番号
		searchApplicationFltKey.setName(params.getName());                 // アプリケーションフィルタ

		List<ApplicationFilters> aplFltObjectList;

		try {
			// アプリケーションフィルタテーブルに対し、変更後アプリケーションフィルタ名と同名のレコードが既に存在しているか検索
			aplFltObjectList = queryDAO.executeForObjectList("ApplicationFiltersBLogic-2", searchApplicationFltKey);

		}  catch (Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061012", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060027"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
        }

		// DB検索結果が1件以上かつ、MOD_FLGのステータスが削除でない場合
		if (!aplFltObjectList.isEmpty() && aplFltObjectList.get(0)
				.getModFlg() != CusconConst.DEL_NUM) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
			// 重複エラーとしてレスポンスデータを作成し返却
        	log.debug("重複エラーが発生しました。");
        	messages.add("message", new BLogicMessage("DK060060"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

		// DB検索結果が1件以上かつ、MOD_FLGのステータスが削除の場合
		if (!aplFltObjectList.isEmpty() &&
				aplFltObjectList.get(0).getModFlg() == CusconConst.DEL_NUM) {
			log.debug("DB検索結果が1件以上かつ、MOD_FLGのステータスが削除の場合");

			try {
				// カテゴリ情報削除（T_AplFltCategoryLink）
				updateDAO.execute("ApplicationFiltersBLogic-5",
						aplFltObjectList.get(0).getSeqNo());

				// サブカテゴリ情報削除（T_AplFltSubCategoryLink）
				updateDAO.execute("ApplicationFiltersBLogic-6",
						aplFltObjectList.get(0).getSeqNo());

				// テクノロジー情報削除（T_AplFltTechnologyLink）
				updateDAO.execute("ApplicationFiltersBLogic-7",
						aplFltObjectList.get(0).getSeqNo());

				// リンク情報削除（T_AplFltRiskLink）
				updateDAO.execute("ApplicationFiltersBLogic-8",
						aplFltObjectList.get(0).getSeqNo());

				// SEQ_NOをキーに、UpdateDAOクラスを使用し、アプリケーションフィルタテーブルのレコードを削除
				updateDAO.execute("ApplicationFiltersBLogic-10",
						aplFltObjectList.get(0).getSeqNo());

			}  catch (Exception e) {
	    		// トランザクションロールバック
	    		TransactionUtil.setRollbackOnly();
				// DBアクセスエラー
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FK061012", objectgArray, params.getUvo()));
				messages.add("message", new BLogicMessage("DK060027"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
	        }
		}

		/***********************************************************************
		 *  ③登録処理
		 **********************************************************************/
		// INSERT用param作成（T_ApplicationFilters）
		ApplicationFilters insertParam = new ApplicationFilters();
		insertParam.setVsysId(vsysId);
		insertParam.setName(params.getName());                    // オブジェクト名
		insertParam.setGenerationNo(CusconConst.ZERO_GENE);       // 世代番号
		insertParam.setModFlg(CusconConst.ADD_NUM);               // 更新フラグ

		try {
			// INSERT実行（T_ApplicationFilters）
			updateDAO.execute("CommonT_ApplicationFilters-5", insertParam);

			// 上記で登録したアプリケーションフィルタのSEQ_NOを取得
			List<ApplicationFilters> insertAplFltInfo = queryDAO.executeForObjectList(
					"ApplicationFiltersBLogic-2", searchApplicationFltKey);
			int seqNo = insertAplFltInfo.get(0).getSeqNo();

			// INSERT用param作成（T_AplFltCategoryLink）
			Object objCategoryList[] = params.getCategoryList();
			for (int i = 0; i < objCategoryList.length; i++) {
				SeqAndNameInfo secAndNameInfo = (SeqAndNameInfo) objCategoryList[i];

				// 画面にてカテゴリチェックONの場合
				if (secAndNameInfo.getChkFlg() == true) {
					log.debug("カテゴリチェックONの場合");

					filterSelectFlt = true;

					// INSERT処理
					SeqAndLinkSeqInfo seqAndLinkSeqInfo = new SeqAndLinkSeqInfo();
					seqAndLinkSeqInfo.setSeqNo(seqNo);
					seqAndLinkSeqInfo.setLinkSeqNo(secAndNameInfo.getSeqNo());

					// Link情報INSERT
					updateDAO.execute("ApplicationFiltersBLogic-11", seqAndLinkSeqInfo);
				}
			}

			// INSERT用param作成（T_AplFltSubCategoryLink）
			Object objSubCategoryList[] = params.getSubCategoryList();
			for (int i = 0; i < objSubCategoryList.length; i++) {
				SeqAndNameInfo secAndNameInfo = (SeqAndNameInfo) objSubCategoryList[i];

				// 画面にてサブカテゴリチェックONの場合
				if (secAndNameInfo.getChkFlg() == true) {
					log.debug("サブカテゴリチェックONの場合");

					filterSelectFlt = true;

					// INSERT処理
					SeqAndLinkSeqInfo seqAndLinkSeqInfo = new SeqAndLinkSeqInfo();
					seqAndLinkSeqInfo.setSeqNo(seqNo);
					seqAndLinkSeqInfo.setLinkSeqNo(secAndNameInfo.getSeqNo());

					// Link情報INSERT
					updateDAO.execute("ApplicationFiltersBLogic-12", seqAndLinkSeqInfo);
				}
			}

			// INSERT用param作成（T_AplFltTechnologyLink）
			Object objTechnologyList[] = params.getTechnologyList();
			for (int i = 0; i < objTechnologyList.length; i++) {
				SeqAndNameInfo secAndNameInfo = (SeqAndNameInfo) objTechnologyList[i];

				// 画面にてテクノロジチェックONの場合
				if (secAndNameInfo.getChkFlg() == true) {
					log.debug("テクノロジチェックONの場合");

					filterSelectFlt = true;

					// INSERT処理
					SeqAndLinkSeqInfo seqAndLinkSeqInfo = new SeqAndLinkSeqInfo();
					seqAndLinkSeqInfo.setSeqNo(seqNo);
					seqAndLinkSeqInfo.setLinkSeqNo(secAndNameInfo.getSeqNo());

					// Link情報INSERT
					updateDAO.execute("ApplicationFiltersBLogic-13", seqAndLinkSeqInfo);
				}
			}

			// INSERT用param作成（T_AplFltRiskLink）
			Object objRiskList[] = params.getRiskList();
			for (int i = 0; i < objRiskList.length; i++) {
				RiskInfo riskInfo = (RiskInfo) objRiskList[i];

				// 画面にてリスクチェックONの場合
				if (riskInfo.getChkFlg() == true) {
					log.debug("リスクチェックONの場合");

					filterSelectFlt = true;

					// INSERT処理
					SeqAndLinkSeqInfo seqAndLinkSeqInfo = new SeqAndLinkSeqInfo();
					seqAndLinkSeqInfo.setSeqNo(seqNo);
					seqAndLinkSeqInfo.setLinkSeqNo(riskInfo.getName());

					// Link情報INSERT
					updateDAO.execute("ApplicationFiltersBLogic-14", seqAndLinkSeqInfo);
				}
			}

		}  catch (Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061009", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060027"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
        }

		// フィルタ選択有無の判定
		if (filterSelectFlt == false){
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// 入力エラーとしてレスポンスデータを作成し返却
        	log.debug("フィルタを1つも選択していません。");
        	messages.add("message", new BLogicMessage("DK060072"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;

		}

		/***********************************************************************
		 *  ④一覧情報検索処理
		 **********************************************************************/
		ApplicationFiltersOutput output;

		try {
			ApplicationFilterList applicationFilterList =
				new ApplicationFilterList(messageAccessor);
			output = new ApplicationFiltersOutput();
			output.setApplicationFilterList(applicationFilterList.getApplicationFilterList(queryDAO, uvo));

		}  catch (Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("FK061012", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060027"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
        }

		/***********************************************************************
		 *  ⑤結果セット
		 **********************************************************************/
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("RegistApplicationFilterBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
