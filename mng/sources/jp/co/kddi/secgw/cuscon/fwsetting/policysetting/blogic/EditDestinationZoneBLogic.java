/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditDestinationZoneBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic;

import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.common.ZoneMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectObjectList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.EditZoneOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.PolicyInput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;

/**
 * クラス名 : EditDestinationZoneBLogic
 * 機能概要 :DestinationZone項目選択時の処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class EditDestinationZoneBLogic implements BLogic<PolicyInput> {
	/**
     * QueryDAO。
     * Springによりインスタンス生成され設定される。
     */
    private QueryDAO queryDAO = null;

    private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic.EditDestinationZoneBLogic");
    private MessageAccessor messageAccessor = null;
	/**
	 * メソッド名 :execute
	 * 機能概要 :ゾーンマスタ、入力宛先ゾーンリストを取得する。
	 * @param param PolicyInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(PolicyInput param) {

		log.debug("EditDestinationZoneBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");
        // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();

        // 選択したリストからオブジェクトを取得する。
        SelectPolicyList selectPolicy = param.getPolicyList().get(param.getIndex());

        ZoneMasterList zoneList = new ZoneMasterList();
        // ゾーンマスタリスト
        List<String> zoneMasterList = null;
        try {
	        // ゾーンマスタリストを取得する。
	        zoneMasterList = zoneList.getZoneMasterList(queryDAO, param.getUvo());
        } catch(Exception e) {
			// DBアクセスエラー
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"EK051001", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK050014"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
        }

        // ビジネスロジックの出力クラスに結果を設定する
        EditZoneOutput out = new EditZoneOutput();
        out.setName(selectPolicy.getName());
        out.setZoneList(selectPolicy.getDestinationZoneList());
        out.setZoneMasterList(zoneMasterList);
        out.setZoneName(out.getZoneList().get(0).getName());
//2011.10.31 add start inoue@prosite 送信元Zoneの取得
    	List<SelectObjectList> sourZoneList = selectPolicy.getSourceZoneList();
    	out.setOtherZoneName(sourZoneList.get(0).getName());
//2011.10.31 add start inoue@prosite

        result.setResultObject(out);
        result.setResultString("success");

        log.debug("EditDestinationZoneBLogic処理終了");
        return result;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}
	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
