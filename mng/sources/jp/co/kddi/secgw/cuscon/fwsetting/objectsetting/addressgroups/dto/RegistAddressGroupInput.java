/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : RegistAddressGroupInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/24     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
//import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SeqAndNameInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.AddressesAndGroupsInfo;


/**
 * クラス名 : RegistAddressGroupInput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/24
 *          新規作成
 * @see
 */
public class RegistAddressGroupInput {

	// UVO
    private CusconUVO uvo = null;
	// アドレスグループ名
	private String name = null;
	// チェック選択アドレスインデックス
	private String checkIndex = null;
	// アドレスグループ追加用アドレス＆アドレスグループ
	private List<AddressesAndGroupsInfo> addressAndAddressGrp4Add = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}
	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : checkIndexのGetterメソッド
	 * 機能概要 : checkIndexを取得する。
	 * @return checkIndex
	 */
	public String getCheckIndex() {
		return checkIndex;
	}

	/**
	 * メソッド名 : checkIndexのSetterメソッド
	 * 機能概要 : checkIndexをセットする。
	 * @param checkIndex
	 */
	public void setCheckIndex(String checkIndex) {
		this.checkIndex = checkIndex;
	}
	/**
	 * メソッド名 : addressAndAddressGrp4AddのGetterメソッド
	 * 機能概要 : addressAndAddressGrp4Addを取得する。
	 * @return addressAndAddressGrp4Add
	 */
	public List<AddressesAndGroupsInfo> getAddressAndAddressGrp4Add() {
		return addressAndAddressGrp4Add;
	}
	/**
	 * メソッド名 : addressAndAddressGrp4AddのSetterメソッド
	 * 機能概要 : addressAndAddressGrp4Addをセットする。
	 * @param addressAndAddressGrp4Add
	 */
	public void setAddressAndAddressGrp4Add(List<AddressesAndGroupsInfo> addressAndAddressGrp4Add) {
		this.addressAndAddressGrp4Add = addressAndAddressGrp4Add;
	}
}
