/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddressesInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/17     morisou                 初版作成
 *******************************************************************************
 */

package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.dto;

import java.util.List;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectAddressList;

/**
 * クラス名 : AddressesOutput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/17
 *          新規作成
 * @see
 */
public class AddressesOutput {

	// アドレス情報リスト
	private List<SelectAddressList> addressList = null;

	private String message = null;


	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * メソッド名 : addressListのGetterメソッド
	 * 機能概要 : addressListを取得する。
	 * @return addressList
	 */
	public List<SelectAddressList> getAddressList() {
		return addressList;
	}

	/**
	 * メソッド名 : addressListのSetterメソッド
	 * 機能概要 : addressListをセットする。
	 * @param addressList
	 */
	public void setAddressList(List<SelectAddressList> addressList) {
		this.addressList = addressList;
	}
}
