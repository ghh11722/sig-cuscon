/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : PolicyForm.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     komakiys                初版作成
 * 2011/05/16     ktakenaka@PROSITE       Webフィルタ、Webアンチウィルス、スパイウェアチェック追加
 * 2013/09/01     kkato@PROSITE           セキュリティルール有効/無効切替対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.form;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;

import jp.co.kddi.secgw.cuscon.common.vo.Schedules;
import jp.co.kddi.secgw.cuscon.common.vo.UrlFilters;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectObjectList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.RiskInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectFilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SeqAndNameInfo;
import jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx;

/**
 * クラス名 : PolicyForm
 * 機能概要 : ポリシー設定フォーム
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class PolicyForm extends ValidatorActionFormEx {

	// シリアルバージョンID
	private static final long serialVersionUID = 1L;

	// ポリシー一覧リスト
	private List<SelectPolicyList> policyList = null;

	// ルール名
	private String name = null;
	// 備考
	private String description = null;

	// 出所ゾーンリスト
	private List<SelectObjectList> zoneList= null;

	// ゾーンマスタリスト
	private List<String> zoneMasterList = null;

	// アドレス名リスト
	private List<SelectObjectList> addressList = null;

	// アドレスマスタリスト
	private List<SelectObjectList> addressMasterList = null;

	// サービス名リスト
	private List<SelectObjectList> serviceList = null;

	// サービスマスタリスト
	private List<SelectObjectList> serviceMasterList = null;

	// ルール適用フラグ
	private String action = null;

	// 脆弱性の保護フラグ
	private String ids_ips = null;

	// スケジュール名
	private String scheduleName = null;

	// スケジュールリスト
	private List<Schedules> scheduleList = null;

	// メッセージ
	private String message = null;

	// 選択番号
	private int index = 0;

	// 変更後名称
	private String modName = null;

	// 選択したMove
	private int moveType = 0;

	// 選択ポリシー一覧
	private SelectPolicyList selectPolicy = null;

	// チェックボックス選択
	private String checkList = null;

	// サービス名
	private String serviceName = null;

	// ゾーン名
	private String zoneName = null;

	// アドレス名
	private String addressName = null;

	// 追加アドレスチェックボックス選択
	private String addCheckList = null;

	// フィルタ（マスタ情報）リスト
	private SelectFilterMasterList filterList = null;

	// アプリケーションリスト
	private List<SelectApplicationList> applicationList = null;

	// カテゴリーリスト
	private List<SeqAndNameInfo> categoryList = new ArrayList<SeqAndNameInfo>();

	// サブカテゴリリスト
	private List<SeqAndNameInfo> subCategoryList = new ArrayList<SeqAndNameInfo>();

	// テクノロジリスト
	private List<SeqAndNameInfo> technologyList = new ArrayList<SeqAndNameInfo>();

	// リスクリスト
	private List<RiskInfo> riskList = new ArrayList<RiskInfo>();

	// 選択中アプリケーションリスト
	private List<SelectedAplInfo> selectedAplList = new ArrayList<SelectedAplInfo>();

	// 追加用フィルタリスト
	private List<SelectApplicationFilterList> filterList4Add = new ArrayList<SelectApplicationFilterList>();

	// 追加用グループリスト
	private List<SelectApplicationGroupList> groupList4Add = new ArrayList<SelectApplicationGroupList>();

	// 選択中アプリケーション名リスト（JSPのhiddenから取得用）
	private String[] selectAplList = null;

	// 選択中アプリケーションタイプリスト（JSPのhiddenから取得用）
	private String[] selectTypeList = null;

	// 検索文字列
	private String search = null;

	// セッション保持しているSelectedList(セッションデータ)
	private List<SelectedAplInfo> sessionAplList = new ArrayList<SelectedAplInfo>();

	// チェック選択アプリケーションフィルターインデックス
	private String filterIndex = null;

	// チェック選択アプリケーショングループインデックス
	private String groupIndex = null;

	private int selectIndex = 0;

	// ラジオボタン
	private int radio = 0;

	// キャンセルフラグ（アプリケーション編集専用 1:キャンセルボタン押下）
	private int cancelFlg = 0;

	// 20110516 ktakenaka@PROSITE add start
	// Webフィルタ名
	private String urlFilterName = null;

	// Webフィルタ適用フラグ
	private int urlfilterDefaultFlg = 0;

	// Webフィルタリスト
	private List<UrlFilters> urlFilterList= null;

	// Webフィルタ選択インデックス
	private int urlFilterSelectedIndex = 0;

	// Webウィルスチェックフラグ
	private String virusCheckFlg = null;

	// スパイウェアチェックフラグ
	private String spywareFlg= null;
	// 20110516 ktakenaka@PROSITE add end

//2011/10/31 add start inoue@prosite 相手方のZone値
	// 相手ゾーン名(通信元の時は通信先、通信先の時は通信元。但し、最初の1つのみ）
	private String otherZoneName = null;
//2011/10/31 add end inoue@prosite

	// 20130901 kkato@PROSITE add start
	// セキュリティルール有効/無効フラグ
	private String disableFlg = null;
	// 20130901 kkato@PROSITE add end

	public void reset(ActionMapping mapping, HttpServletRequest request){
		selectIndex = 0;
		index = 0;
	}
	/**
	 * メソッド名 : radioのGetterメソッド
	 * 機能概要 : radioを取得する。
	 * @return radio
	 */
	public int getRadio() {
		return radio;
	}

	/**
	 * メソッド名 : radioのSetterメソッド
	 * 機能概要 : radioをセットする。
	 * @param radio
	 */
	public void setRadio(int radio) {
		this.radio = radio;
	}

	/**
	 * メソッド名 : selectindexのGetterメソッド
	 * 機能概要 : selectindexを取得する。
	 * @return selectindex
	 */
	public int getSelectIndex() {
		return selectIndex;
	}

	/**
	 * メソッド名 : selectindexのSetterメソッド
	 * 機能概要 : selectindexをセットする。
	 * @param selectindex
	 */
	public void setSelectIndex(int selectIndex) {
		this.selectIndex = selectIndex;
	}

	/**
	 * メソッド名 : categoryListのGetterメソッド
	 * 機能概要 : categoryListを生成し、取得する。
	 * @param cnt
	 * @return categoryList
	 */
	public Object getCategoryList(int cnt) {

		while (this.categoryList.size() <= cnt){
			this.categoryList.add(new SeqAndNameInfo());
		}

		return this.categoryList.get(cnt);
	}

	/**
	 * メソッド名 : subCategoryListのGetterメソッド
	 * 機能概要 : subCategoryListを生成し、取得する。
	 * @param cnt
	 * @return subCategoryList
	 */
	public Object getSubCategoryList(int cnt) {

		while (this.subCategoryList.size() <= cnt){
			this.subCategoryList.add(new SeqAndNameInfo());
		}

		return this.subCategoryList.get(cnt);
	}

	/**
	 * メソッド名 : technologyListのGetterメソッド
	 * 機能概要 : technologyListをを生成し、取得する。
	 * @param cnt
	 * @return technologyList
	 */
	public Object getTechnologyList(int cnt) {

		while (this.technologyList.size() <= cnt){
			this.technologyList.add(new SeqAndNameInfo());
		}

		return this.technologyList.get(cnt);
	}

	/**
	 * メソッド名 : riskListのGetterメソッド
	 * 機能概要 : riskListをを生成し、取得する。
	 * @param cnt
	 * @return riskList
	 */
	public Object getRiskList(int cnt) {

		while (this.riskList.size() <= cnt){
			this.riskList.add(new RiskInfo());
		}

		return this.riskList.get(cnt);
	}



	/**
	 * メソッド名 : addCheckListのGetterメソッド
	 * 機能概要 : addCheckListを取得する。
	 * @return addCheckList
	 */
	public String getAddCheckList() {
		return addCheckList;
	}

	/**
	 * メソッド名 : addCheckListのSetterメソッド
	 * 機能概要 : addCheckListをセットする。
	 * @param addCheckList
	 */
	public void setAddCheckList(String addCheckList) {
		this.addCheckList = addCheckList;
	}

	/**
	 * メソッド名 : policyListのGetterメソッド
	 * 機能概要 : policyListを取得する。
	 * @return policyList
	 */
	public List<SelectPolicyList> getPolicyList() {
		return policyList;
	}

	/**
	 * メソッド名 : policyListのSetterメソッド
	 * 機能概要 : policyListをセットする。
	 * @param policyList
	 */
	public void setPolicyList(List<SelectPolicyList> policyList) {
		this.policyList = policyList;
	}

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * メソッド名 : descriptionのGetterメソッド
	 * 機能概要 : descriptionを取得する。
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * メソッド名 : descriptionのSetterメソッド
	 * 機能概要 : descriptionをセットする。
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * メソッド名 : zoneListのGetterメソッド
	 * 機能概要 : zoneListを取得する。
	 * @return zoneList
	 */
	public List<SelectObjectList> getZoneList() {
		return zoneList;
	}

	/**
	 * メソッド名 : zoneListのSetterメソッド
	 * 機能概要 : zoneListをセットする。
	 * @param zoneList
	 */
	public void setZoneList(List<SelectObjectList> zoneList) {
		this.zoneList = zoneList;
	}

	/**
	 * メソッド名 : zoneMasterListのGetterメソッド
	 * 機能概要 : zoneMasterListを取得する。
	 * @return zoneMasterList
	 */
	public List<String> getZoneMasterList() {
		return zoneMasterList;
	}

	/**
	 * メソッド名 : zoneMasterListのSetterメソッド
	 * 機能概要 : zoneMasterListをセットする。
	 * @param zoneMasterList
	 */
	public void setZoneMasterList(List<String> zoneMasterList) {
		this.zoneMasterList = zoneMasterList;
	}

	/**
	 * メソッド名 : addressListのGetterメソッド
	 * 機能概要 : addressListを取得する。
	 * @return addressList
	 */
	public List<SelectObjectList> getAddressList() {
		return addressList;
	}

	/**
	 * メソッド名 : addressListのSetterメソッド
	 * 機能概要 : addressListをセットする。
	 * @param addressList
	 */
	public void setAddressList(List<SelectObjectList> addressList) {
		this.addressList = addressList;
	}

	/**
	 * メソッド名 : addressMasterListのGetterメソッド
	 * 機能概要 : addressMasterListを取得する。
	 * @return addressMasterList
	 */
	public List<SelectObjectList> getAddressMasterList() {
		return addressMasterList;
	}

	/**
	 * メソッド名 : addressMasterListのSetterメソッド
	 * 機能概要 : addressMasterListをセットする。
	 * @param addressMasterList
	 */
	public void setAddressMasterList(List<SelectObjectList> addressMasterList) {
		this.addressMasterList = addressMasterList;
	}

	/**
	 * メソッド名 : serviceListのGetterメソッド
	 * 機能概要 : serviceListを取得する。
	 * @return serviceList
	 */
	public List<SelectObjectList> getServiceList() {
		return serviceList;
	}

	/**
	 * メソッド名 : serviceListのSetterメソッド
	 * 機能概要 : serviceListをセットする。
	 * @param serviceList
	 */
	public void setServiceList(List<SelectObjectList> serviceList) {
		this.serviceList = serviceList;
	}

	/**
	 * メソッド名 : serviceMasterListのGetterメソッド
	 * 機能概要 : serviceMasterListを取得する。
	 * @return serviceMasterList
	 */
	public List<SelectObjectList> getServiceMasterList() {
		return serviceMasterList;
	}

	/**
	 * メソッド名 : serviceMasterListのSetterメソッド
	 * 機能概要 : serviceMasterListをセットする。
	 * @param serviceMasterList
	 */
	public void setServiceMasterList(List<SelectObjectList> serviceMasterList) {
		this.serviceMasterList = serviceMasterList;
	}

	/**
	 * メソッド名 : actionのGetterメソッド
	 * 機能概要 : actionを取得する。
	 * @return action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * メソッド名 : actionのSetterメソッド
	 * 機能概要 : actionをセットする。
	 * @param action
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * メソッド名 : ids_ipsのGetterメソッド
	 * 機能概要 : ids_ipsを取得する。
	 * @return ids_ips
	 */
	public String getIds_ips() {
		return ids_ips;
	}

	/**
	 * メソッド名 : ids_ipsのSetterメソッド
	 * 機能概要 : ids_ipsをセットする。
	 * @param idsIps
	 */
	public void setIds_ips(String idsIps) {
		ids_ips = idsIps;
	}

	/**
	 * メソッド名 : filterList4AddのGetterメソッド
	 * 機能概要 : filterList4Addを取得する。
	 * @return filterList4Add
	 */
	public List<SelectApplicationFilterList> getFilterList4Add() {
		return filterList4Add;
	}

	/**
	 * メソッド名 : filterList4AddのSetterメソッド
	 * 機能概要 : filterList4Addをセットする。
	 * @param filterList4Add
	 */
	public void setFilterList4Add(List<SelectApplicationFilterList> filterList4Add) {
		this.filterList4Add = filterList4Add;
	}

	/**
	 * メソッド名 : groupList4AddのGetterメソッド
	 * 機能概要 : groupList4Addを取得する。
	 * @return groupList4Add
	 */
	public List<SelectApplicationGroupList> getGroupList4Add() {
		return groupList4Add;
	}

	/**
	 * メソッド名 : groupList4AddのSetterメソッド
	 * 機能概要 : groupList4Addをセットする。
	 * @param groupList4Add
	 */
	public void setGroupList4Add(List<SelectApplicationGroupList> groupList4Add) {
		this.groupList4Add = groupList4Add;
	}

	/**
	 * メソッド名 : selectAplListのGetterメソッド
	 * 機能概要 : selectAplListを取得する。
	 * @return selectAplList
	 */
	public String[] getSelectAplList() {
		return selectAplList;
	}

	/**
	 * メソッド名 : selectAplListのSetterメソッド
	 * 機能概要 : selectAplListをセットする。
	 * @param selectAplList
	 */
	public void setSelectAplList(String[] selectAplList) {
		this.selectAplList = selectAplList;
	}

	/**
	 * メソッド名 : selectTypeListのGetterメソッド
	 * 機能概要 : selectTypeListを取得する。
	 * @return selectTypeList
	 */
	public String[] getSelectTypeList() {
		return selectTypeList;
	}

	/**
	 * メソッド名 : selectTypeListのSetterメソッド
	 * 機能概要 : selectTypeListをセットする。
	 * @param selectTypeList
	 */
	public void setSelectTypeList(String[] selectTypeList) {
		this.selectTypeList = selectTypeList;
	}

	/**
	 * メソッド名 : scheduleNameのGetterメソッド
	 * 機能概要 : scheduleNameを取得する。
	 * @return scheduleName
	 */
	public String getScheduleName() {
		return scheduleName;
	}

	/**
	 * メソッド名 : scheduleNameのSetterメソッド
	 * 機能概要 : scheduleNameをセットする。
	 * @param scheduleName
	 */
	public void setScheduleName(String scheduleName) {
		this.scheduleName = scheduleName;
	}

	/**
	 * メソッド名 : scheduleListのGetterメソッド
	 * 機能概要 : scheduleListを取得する。
	 * @return scheduleList
	 */
	public List<Schedules> getScheduleList() {
		return scheduleList;
	}

	/**
	 * メソッド名 : scheduleListのSetterメソッド
	 * 機能概要 : scheduleListをセットする。
	 * @param scheduleList
	 */
	public void setScheduleList(List<Schedules> scheduleList) {
		this.scheduleList = scheduleList;
	}

	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : selectedAplListのGetterメソッド
	 * 機能概要 : selectedAplListを取得する。
	 * @return selectedAplList
	 */
	public List<SelectedAplInfo>  getSelectedAplList() {
		return selectedAplList;
	}

	/**
	 * メソッド名 : selectedAplListのSetterメソッド
	 * 機能概要 : selectedAplListをセットする。
	 * @param selectedAplList
	 */
	public void setSelectedAplList(List<SelectedAplInfo> selectedAplList) {
		this.selectedAplList = selectedAplList;
	}

	/**
	 * メソッド名 : filterListのGetterメソッド
	 * 機能概要 : filterListを取得する。
	 * @return filterList
	 */
	public SelectFilterMasterList getFilterList() {
		return filterList;
	}

	/**
	 * メソッド名 : filterListのSetterメソッド
	 * 機能概要 : filterListをセットする。
	 * @param filterList
	 */
	public void setFilterList(SelectFilterMasterList filterList) {
		this.filterList = filterList;
	}

	/**
	 * メソッド名 : applicationListのGetterメソッド
	 * 機能概要 : applicationListを取得する。
	 * @return applicationList
	 */
	public List<SelectApplicationList> getApplicationList() {
		return applicationList;
	}

	/**
	 * メソッド名 : applicationListのSetterメソッド
	 * 機能概要 : applicationListをセットする。
	 * @param applicationList
	 */
	public void setApplicationList(List<SelectApplicationList> applicationList) {
		this.applicationList = applicationList;
	}

	/**
	 * メソッド名 : categoryListのGetterメソッド
	 * 機能概要 : categoryListを取得する。
	 * @return categoryList
	 */
	public Object[] getCategoryList() {
		return categoryList.toArray();
	}

	/**
	 * メソッド名 : categoryListのSetterメソッド
	 * 機能概要 : categoryListをセットする。
	 * @param categoryList
	 */
	public void setCategoryList(List<SeqAndNameInfo> categoryList) {
		this.categoryList = categoryList;
	}

	/**
	 * メソッド名 : subCategoryListのGetterメソッド
	 * 機能概要 : subCategoryListを取得する。
	 * @return subCategoryList
	 */
	public Object[] getSubCategoryList() {
		return subCategoryList.toArray();
	}

	/**
	 * メソッド名 : subCategoryListのSetterメソッド
	 * 機能概要 : subCategoryListをセットする。
	 * @param subCategoryList
	 */
	public void setSubCategoryList(List<SeqAndNameInfo> subCategoryList) {
		this.subCategoryList = subCategoryList;
	}

	/**
	 * メソッド名 : technologyListのGetterメソッド
	 * 機能概要 : technologyListを取得する。
	 * @return technologyList
	 */
	public Object[] getTechnologyList() {
		return technologyList.toArray();
	}

	/**
	 * メソッド名 : technologyListのSetterメソッド
	 * 機能概要 : technologyListをセットする。
	 * @param technologyList
	 */
	public void setTechnologyList(List<SeqAndNameInfo> technologyList) {
		this.technologyList = technologyList;
	}

	/**
	 * メソッド名 : riskListのGetterメソッド
	 * 機能概要 : riskListを取得する。
	 * @return riskList
	 */
	public Object[] getRiskList() {
		return riskList.toArray();
	}

	/**
	 * メソッド名 : riskListのSetterメソッド
	 * 機能概要 : riskListをセットする。
	 * @param riskList
	 */
	public void setRiskList(List<RiskInfo> riskList) {
		this.riskList = riskList;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * メソッド名 : indexのGetterメソッド
	 * 機能概要 : indexを取得する。
	 * @return index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * メソッド名 : indexのSetterメソッド
	 * 機能概要 : indexをセットする。
	 * @param index
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * メソッド名 : modNameのGetterメソッド
	 * 機能概要 : modNameを取得する。
	 * @return modName
	 */
	public String getModName() {
		return modName;
	}

	/**
	 * メソッド名 : modNameのSetterメソッド
	 * 機能概要 : modNameをセットする。
	 * @param modName
	 */
	public void setModName(String modName) {
		this.modName = modName;
	}
	/**
	 * メソッド名 : moveTypeのGetterメソッド
	 * 機能概要 : moveTypeを取得する。
	 * @return moveType
	 */
	public int getMoveType() {
		return moveType;
	}

	/**
	 * メソッド名 : moveTypeのSetterメソッド
	 * 機能概要 : moveTypeをセットする。
	 * @param moveType
	 */
	public void setMoveType(int moveType) {
		this.moveType = moveType;
	}

	/**
	 * メソッド名 : selectPolicyのGetterメソッド
	 * 機能概要 : selectPolicyを取得する。
	 * @return selectPolicy
	 */
	public SelectPolicyList getSelectPolicy() {
		return selectPolicy;
	}

	/**
	 * メソッド名 : selectPolicyのSetterメソッド
	 * 機能概要 : selectPolicyをセットする。
	 * @param selectPolicy
	 */
	public void setSelectPolicy(SelectPolicyList selectPolicy) {
		this.selectPolicy = selectPolicy;
	}

	/**
	 * メソッド名 : checkListのGetterメソッド
	 * 機能概要 : checkListを取得する。
	 * @return checkList
	 */
	public String getCheckList() {
		return checkList;
	}

	/**
	 * メソッド名 : checkListのSetterメソッド
	 * 機能概要 : checkListをセットする。
	 * @param checkList
	 */
	public void setCheckList(String checkList) {
		this.checkList = checkList;
	}

	/**
	 * メソッド名 : serviceNameのGetterメソッド
	 * 機能概要 : serviceNameを取得する。
	 * @return serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * メソッド名 : serviceNameのSetterメソッド
	 * 機能概要 : serviceNameをセットする。
	 * @param serviceName
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	/**
	 * メソッド名 : zoneNameのGetterメソッド
	 * 機能概要 : zoneNameを取得する。
	 * @return zoneName
	 */
	public String getZoneName() {
		return zoneName;
	}

	/**
	 * メソッド名 : zoneNameのSetterメソッド
	 * 機能概要 : zoneNameをセットする。
	 * @param zoneName
	 */
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	/**
	 * メソッド名 : addressNameのGetterメソッド
	 * 機能概要 : addressNameを取得する。
	 * @return addressName
	 */
	public String getAddressName() {
		return addressName;
	}

	/**
	 * メソッド名 : addressNameのSetterメソッド
	 * 機能概要 : addressNameをセットする。
	 * @param addressName
	 */
	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}

	/**
	 * メソッド名 : searchのGetterメソッド
	 * 機能概要 : searchを取得する。
	 * @return search
	 */
	public String getSearch() {
		return search;
	}

	/**
	 * メソッド名 : searchのSetterメソッド
	 * 機能概要 : searchをセットする。
	 * @param search
	 */
	public void setSearch(String search) {
		this.search = search;
	}

	/**
	 * メソッド名 : sessionAplListのGetterメソッド
	 * 機能概要 : sessionAplListを取得する。
	 * @return sessionAplList
	 */
	public List<SelectedAplInfo> getSessionAplList() {
		return sessionAplList;
	}

	/**
	 * メソッド名 : sessionAplListのSetterメソッド
	 * 機能概要 : sessionAplListをセットする。
	 * @param sessionAplList
	 */
	public void setSessionAplList(List<SelectedAplInfo> sessionAplList) {
		this.sessionAplList = sessionAplList;
	}

	/**
	 * メソッド名 : filterIndexのGetterメソッド
	 * 機能概要 : filterIndexを取得する。
	 * @return filterIndex
	 */
	public String getFilterIndex() {
		return filterIndex;
	}

	/**
	 * メソッド名 : filterIndexのSetterメソッド
	 * 機能概要 : filterIndexをセットする。
	 * @param filterIndex
	 */
	public void setFilterIndex(String filterIndex) {
		this.filterIndex = filterIndex;
	}

	/**
	 * メソッド名 : groupIndexのGetterメソッド
	 * 機能概要 : groupIndexを取得する。
	 * @return groupIndex
	 */
	public String getGroupIndex() {
		return groupIndex;
	}

	/**
	 * メソッド名 : groupIndexのSetterメソッド
	 * 機能概要 : groupIndexをセットする。
	 * @param groupIndex
	 */
	public void setGroupIndex(String groupIndex) {
		this.groupIndex = groupIndex;
	}

	/**
	 * メソッド名 : serialversionuidのGetterメソッド
	 * 機能概要 : serialversionuidを取得する。
	 * @return serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * メソッド名 : cancelFlgのGetterメソッド
	 * 機能概要 : cancelFlgを取得する。
	 * @return cancelFlg
	 */
	public int getCancelFlg() {
		return cancelFlg;
	}
	/**
	 * メソッド名 : cancelFlgのSetterメソッド
	 * 機能概要 : cancelFlgをセットする。
	 * @param cancelFlg
	 */
	public void setCancelFlg(int cancelFlg) {
		this.cancelFlg = cancelFlg;
	}

	// 20110516 ktakenaka@PROSITE add start
	/**
	 * メソッド名 : urlFilterNameのGetterメソッド
	 * 機能概要 : urlFilterNameを取得する。
	 * @return urlFilterName
	 */
	public String getUrlFilterName() {
		return urlFilterName;
	}

	/**
	 * メソッド名 : urlFilterNameのSetterメソッド
	 * 機能概要 : urlFilterNameをセットする。
	 * @param urlFilterName
	 */
	public void setUrlFilterName(String urlFilterName) {
		this.urlFilterName = urlFilterName;
	}

	/**
	 * メソッド名 : urlfilterDefaultFlgのGetterメソッド
	 * 機能概要 : urlfilterDefaultFlgを取得する。
	 * @return urlfilterDefaultFlg
	 */
	public int getUrlfilterDefaultFlg() {
		return urlfilterDefaultFlg;
	}
	/**
	 * メソッド名 : urlfilterDefaultFlgのSetterメソッド
	 * 機能概要 : urlfilterDefaultFlgをセットする。
	 * @param urlfilterDefaultFlg
	 */
	public void setUrlfilterDefaultFlg(int urlfilterDefaultFlg) {
		this.urlfilterDefaultFlg = urlfilterDefaultFlg;
	}

	/**
	 * メソッド名 : urlFilterListのGetterメソッド
	 * 機能概要 : urlFilterListを取得する。
	 * @return urlFilterList
	 */
	public List<UrlFilters> getUrlFilterList() {
		return urlFilterList;
	}

	/**
	 * メソッド名 : urlFilterListのSetterメソッド
	 * 機能概要 : urlFilterListをセットする。
	 * @param urlFilterList
	 */
	public void setUrlFilterList(List<UrlFilters> urlFilterList) {
		this.urlFilterList = urlFilterList;
	}

	/**
	 * メソッド名 : urlFilterSelectedIndexのGetterメソッド
	 * 機能概要 : urlFilterSelectedIndexを取得する。
	 * @return urlFilterSelectedIndex
	 */
	public int geturlFilterSelectedIndex() {
		return urlFilterSelectedIndex;
	}

	/**
	 * メソッド名 : urlFilterSelectedIndexのSetterメソッド
	 * 機能概要 : urlFilterSelectedIndexをセットする。
	 * @param urlFilterSelectedIndex
	 */
	public void seturlFilterSelectedIndex(int urlFilterSelectedIndex) {
		this.urlFilterSelectedIndex = urlFilterSelectedIndex;
	}

	/**
	 * メソッド名 : virusCheckFlgのGetterメソッド
	 * 機能概要 : virusCheckFlgを取得する。
	 * @return virusCheckFlg
	 */
	public String getVirusCheckFlg() {
		return virusCheckFlg;
	}

	/**
	 * メソッド名 : virusCheckFlgのSetterメソッド
	 * 機能概要 : virusCheckFlgをセットする。
	 * @param virusCheckFlg
	 */
	public void setVirusCheckFlg(String virusCheckFlg) {
		this.virusCheckFlg = virusCheckFlg;
	}

	/**
	 * メソッド名 : spywareFlgのGetterメソッド
	 * 機能概要 : spywareFlgを取得する。
	 * @return spywareFlg
	 */
	public String getSpywareFlg() {
		return spywareFlg;
	}

	/**
	 * メソッド名 : spywareFlgのSetterメソッド
	 * 機能概要 : spywareFlgをセットする。
	 * @param spywareFlg
	 */
	public void setSpywareFlg(String spywareFlg) {
		this.spywareFlg = spywareFlg;
	}
	// 20110516 ktakenaka@PROSITE add start

//2011/10/31 add start inoue@prosite 相手方のZone値
	/**
	 * メソッド名 : otherZoneNameのGetterメソッド
	 * 機能概要 : otherZoneNameを取得する。
	 * @return otherZoneName
	 */
	public String getOtherZoneName() {
		return otherZoneName;
	}

	/**
	 * メソッド名 : otherZoneNameのSetterメソッド
	 * 機能概要 : otherZoneNameをセットする。
	 * @param otherZoneName
	 */
	public void setOtherZoneName(String otherZoneName) {
		this.otherZoneName = otherZoneName;
	}
//2011/10/31 add end inoue@prosite

	// 20130901 kkato@PROSITE add start
	/**
	 * メソッド名 : disableFlgのGetterメソッド
	 * 機能概要 : disableFlgを取得する。
	 * @return disableFlg
	 */
	public String getDisableFlg() {
		return disableFlg;
	}
	/**
	 * メソッド名 : disableFlgのSetterメソッド
	 * 機能概要 : disableFlgをセットする。
	 * @param disableFlg
	 */
	public void setDisableFlg(String disableFlg) {
		this.disableFlg = disableFlg;
	}
	// 20130901 kkato@PROSITE add end
}
