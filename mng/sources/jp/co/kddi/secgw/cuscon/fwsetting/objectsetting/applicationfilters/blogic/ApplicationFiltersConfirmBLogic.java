/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ApplicationFiltersConfirmBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/08     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.blogic;

import java.util.ArrayList;
import java.util.List;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.dto.ApplicationFiltersConfirmInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.dto.ApplicationFiltersOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationFilterList;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicResult;
import org.apache.log4j.Logger;

/**
 * クラス名 : ApplicationFiltersConfirmBLogic
 * 機能概要 : アプリケーションフィルター削除確認対象表示処理を行う。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/08
 *          新規作成
 * @see
 */
public class ApplicationFiltersConfirmBLogic implements BLogic<ApplicationFiltersConfirmInput> {

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.blogic.ApplicationFiltersConfirmBLogic");

	/**
	 * メソッド名 :execute
	 * 機能概要 :アプリケーションフィルター削除確認対象表示処理を行う。
	 * @param param ApplicationFiltersConfirmInput入力データクラス
	 * @return result BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(ApplicationFiltersConfirmInput param) {

		log.debug("ApplicationFiltersConfirmBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");

		// アウトプットデータクラスに設定する。
        ApplicationFiltersOutput out = new ApplicationFiltersOutput();
        List<SelectApplicationFilterList> applicationFilterList = new ArrayList<SelectApplicationFilterList>();

        String[] indexList = param.getCheckIndex().split(",");

        // 選択チェック分繰り返す。
        for(int i=0; i< indexList.length; i++) {
    		log.debug("チェックされたレコードを追加　対象のアプリケーションフィルタ：" + param.getApplicationFilterList().get(Integer.parseInt(indexList[i])).getName());
        	applicationFilterList.add(param.getApplicationFilterList().get(Integer.parseInt(indexList[i])));
        }

	    out.setApplicationFilterList(applicationFilterList);

	    result.setResultObject(out);
	    result.setResultString("success");

	    log.debug("ApplicationFiltersConfirmBLogic処理終了");
		return result;
	}
}
