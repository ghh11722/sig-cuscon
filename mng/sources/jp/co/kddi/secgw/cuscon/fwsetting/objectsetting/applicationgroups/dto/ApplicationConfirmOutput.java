/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ApplicationConfirmOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/13     morisou                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationGroupList;

/**
 * クラス名 : ApplicationConfirmOutput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/13
 *          新規作成
 * @see
 */
public class ApplicationConfirmOutput {

	// アプリケーショングループリスト
	private List<SelectApplicationGroupList> applicationGroupList = null;

	/**
	 * メソッド名 : applicationGroupListのGetterメソッド
	 * 機能概要 : applicationGroupListを取得する。
	 * @return applicationGroupList
	 */
	public List<SelectApplicationGroupList> getApplicationGroupList() {
		return applicationGroupList;
	}

	/**
	 * メソッド名 : applicationGroupListのSetterメソッド
	 * 機能概要 : applicationGroupListをセットする。
	 * @param applicationGroupList
	 */
	public void setApplicationGroupList(List<SelectApplicationGroupList> applicationGroupList) {
		this.applicationGroupList = applicationGroupList;
	}
}
