/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ChangeEditReccurenceInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.dto;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.vo.Schedules;

/**
 * クラス名 : ChangeEditReccurenceInput
 * 機能概要 :編集画面recurrence変更入力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class ChangeEditReccurenceInput {

	// CusconUVO
	private CusconUVO uvo = null;

	// 選択スケジュールデータクラス
	private Schedules selectSchedule = null;

	// 設定時間リスト
	private String time = null;

	// 編集名
	private String showName = null;

	// 編集周期
	private int showRecurrence = 0;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}

	/**
	 * メソッド名 : selectScheduleのGetterメソッド
	 * 機能概要 : selectScheduleを取得する。
	 * @return selectSchedule
	 */
	public Schedules getSelectSchedule() {
		return selectSchedule;
	}

	/**
	 * メソッド名 : selectScheduleのSetterメソッド
	 * 機能概要 : selectScheduleをセットする。
	 * @param selectSchedule
	 */
	public void setSelectSchedule(Schedules selectSchedule) {
		this.selectSchedule = selectSchedule;
	}

	/**
	 * メソッド名 : timeListのGetterメソッド
	 * 機能概要 : timeListを取得する。
	 * @return timeList
	 */
	public String getTime() {
		return time;
	}

	/**
	 * メソッド名 : timeListのSetterメソッド
	 * 機能概要 : timeListをセットする。
	 * @param timeList
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * メソッド名 : showNameのGetterメソッド
	 * 機能概要 : showNameを取得する。
	 * @return showName
	 */
	public String getShowName() {
		return showName;
	}

	/**
	 * メソッド名 : showNameのSetterメソッド
	 * 機能概要 : showNameをセットする。
	 * @param showName
	 */
	public void setShowName(String showName) {
		this.showName = showName;
	}

	/**
	 * メソッド名 : showRecurrenceのGetterメソッド
	 * 機能概要 : showRecurrenceを取得する。
	 * @return showRecurrence
	 */
	public int getShowRecurrence() {
		return showRecurrence;
	}

	/**
	 * メソッド名 : showRecurrenceのSetterメソッド
	 * 機能概要 : showRecurrenceをセットする。
	 * @param showRecurrence
	 */
	public void setShowRecurrence(int showRecurrence) {
		this.showRecurrence = showRecurrence;
	}
}
