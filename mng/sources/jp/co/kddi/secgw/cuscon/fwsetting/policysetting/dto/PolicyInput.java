/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : PolicyInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;

/**
 * クラス名 : PolicyInput
 * 機能概要 : ポリシー一覧画面入力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/01
 *          新規作成
 * @see
 */
public class PolicyInput {

	// CusconUVO
	private CusconUVO uvo = null;
	// ポリシー一覧リスト
	private List<SelectPolicyList> policyList = null;
	// 選択したポリシーのインデックス
	private int index = 0;
	// 選択したラジオボタン
	private int radio = 0;
	// キャンセルフラグ（アプリケーション編集専用 1:キャンセルボタン押下）
	private int cancelFlg = 0;
	// セッション情報（アプリケーション編集専用）
	List<SelectedAplInfo> sessionAplList = null;
	// 選択したポリシー名（アプリケーション編集専用）
	private String name = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}

	/**
	 * メソッド名 : policyListのGetterメソッド
	 * 機能概要 : policyListを取得する。
	 * @return policyList
	 */
	public List<SelectPolicyList> getPolicyList() {
		return policyList;
	}

	/**
	 * メソッド名 : policyListのSetterメソッド
	 * 機能概要 : policyListをセットする。
	 * @param policyList
	 */
	public void setPolicyList(List<SelectPolicyList> policyList) {
		this.policyList = policyList;
	}

	/**
	 * メソッド名 : indexのGetterメソッド
	 * 機能概要 : indexを取得する。
	 * @return index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * メソッド名 : indexのSetterメソッド
	 * 機能概要 : indexをセットする。
	 * @param index
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * メソッド名 : radioのGetterメソッド
	 * 機能概要 : radioを取得する。
	 * @return radio
	 */
	public int getRadio() {
		return radio;
	}

	/**
	 * メソッド名 : radioのSetterメソッド
	 * 機能概要 : radioをセットする。
	 * @param radio
	 */
	public void setRadio(int radio) {
		this.radio = radio;
	}

	/**
	 * メソッド名 : cancelFlgのGetterメソッド
	 * 機能概要 : cancelFlgを取得する。
	 * @return cancelFlg
	 */
	public int getCancelFlg() {
		return cancelFlg;
	}

	/**
	 * メソッド名 : cancelFlgのSetterメソッド
	 * 機能概要 : cancelFlgをセットする。
	 * @param cancelFlg
	 */
	public void setCancelFlg(int cancelFlg) {
		this.cancelFlg = cancelFlg;
	}

	/**
	 * メソッド名 : sessionAplListのGetterメソッド
	 * 機能概要 : sessionAplListを取得する。
	 * @return sessionAplList
	 */
	public List<SelectedAplInfo> getSessionAplList() {
		return sessionAplList;
	}

	/**
	 * メソッド名 : sessionAplListのSetterメソッド
	 * 機能概要 : sessionAplListをセットする。
	 * @param sessionAplList
	 */
	public void setSessionAplList(List<SelectedAplInfo> sessionAplList) {
		this.sessionAplList = sessionAplList;
	}

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
}
