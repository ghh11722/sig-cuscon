/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectCommitList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/22     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.commit.vo;

import java.io.Serializable;

/**
 * クラス名 : SelectCommitList
 * 機能概要 : 変更情報一覧データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/22
 *          新規作成
 * @see
 */
public class SelectCommitList implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 8878104685620457194L;

	// オブジェクト名
	private String name = null;
	// 変更フラグ
	private int modFlg = 0;
	// 変更名称
	private String modName = null;
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : modFlgのGetterメソッド
	 * 機能概要 : modFlgを取得する。
	 * @return modFlg
	 */
	public int getModFlg() {
		return modFlg;
	}
	/**
	 * メソッド名 : modFlgのSetterメソッド
	 * 機能概要 : modFlgをセットする。
	 * @param modFlg
	 */
	public void setModFlg(int modFlg) {
		this.modFlg = modFlg;
	}
	/**
	 * メソッド名 : modNameのGetterメソッド
	 * 機能概要 : modNameを取得する。
	 * @return modName
	 */
	public String getModName() {
		return modName;
	}
	/**
	 * メソッド名 : modNameのSetterメソッド
	 * 機能概要 : modNameをセットする。
	 * @param modName
	 */
	public void setModName(String modName) {
		this.modName = modName;
	}

}
