/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateAddressBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/18     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.blogic;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectAddressList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.dto.AddressesConfirmInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.dto.AddressesConfirmOutput;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicResult;

/**
 * クラス名 : addressesConfirmBLogic
 * 機能概要 :アドレス削除確認対象表示処理を行う。
 * 備考 :
 * @author kinoshtt
 * @version 1.0 kinoshtt
 *          Created 2010/03/08
 *          新規作成
 * @see
 */
public class AddressesConfirmBLogic implements BLogic<AddressesConfirmInput> {

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.blogic.AddressesConfirmBLogic");

	/**
	 * メソッド名 :BLogicResult
	 * 機能概要 :
	 * @param params SchedulesConfirmInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(AddressesConfirmInput param) {
		log.debug("AddressesConfirmBLogic処理開始");

		BLogicResult result = new BLogicResult();
        result.setResultString("failure");

		// アウトプットデータクラスに設定する。
        AddressesConfirmOutput out = new AddressesConfirmOutput();
        List<SelectAddressList> addressList = new ArrayList<SelectAddressList>();

        String[] indexList = param.getCheckIndex().split(",");

        // 選択チェック分繰り返す。
        for(int i=0; i< indexList.length; i++) {
    		log.debug("チェックされたアドレスリストを格納　アドレス名：" + param.getAddressList().get(Integer.parseInt(indexList[i])).getName());
        	addressList.add(param.getAddressList().get(Integer.parseInt(indexList[i])));
        }

        out.setAddressList(addressList);

	    result.setResultObject(out);
	    result.setResultString("success");

	    log.debug("AddressesConfirmBLogic処理終了");
		return result;
	}
}
