/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditAddressGroupBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/19     morisou                 初版作成
 * 2016/11/23     T.Yamazaki@Plum Systems アドレスグループ除外対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.blogic;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.AddressGroups;
import jp.co.kddi.secgw.cuscon.common.vo.Addresses;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.dto.EditAddressGroupInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.dto.EditAddressGroupOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.AddressesAndGroupsInfo;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : EditAddressGroupBLogic
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/19
 *          新規作成
 *          2.0 T.Yamazaki@Plum Systems Inc.
 *          Updated 2016/11/23
 *          アドレスグループ除外対応
 * @see
 */
public class EditAddressGroupBLogic implements BLogic<EditAddressGroupInput> {

	// オブジェクトタイプ定数
	private static final int ADDRESS = 0;
	private static final int ADDRESS_GROUP = 1;

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.blogic.EditAddressGroupBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : BLogicResult
	 * 機能概要 : アドレスグループの編集用情報を取得し、 <br>
	 *            取得した情報を呼び出し元に返却する。
	 * @param param
	 * @return result
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(EditAddressGroupInput params) {
		log.debug("EditAddressGroupBLogic処理開始");

		String addresses;          // リクエスト情報.Address
		String[] strAry;           // 分割した文字列を格納する配列

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		 // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = params.getUvo();
		String vsysId = uvo.getVsysId();

		// InputBase設定
		InputBase inputBase = new InputBase();
		inputBase.setVsysId(vsysId);                            // Vsys-ID
		inputBase.setGenerationNo(CusconConst.ZERO_GENE);    // 世代番号
		inputBase.setModFlg(CusconConst.DEL_NUM);            // 変更フラグ

		// 返却結果リスト
        EditAddressGroupOutput output = new EditAddressGroupOutput();

		// All Address & Groups 格納用リスト
		List<AddressesAndGroupsInfo> outputAddressAndGroup =
								new ArrayList<AddressesAndGroupsInfo>();

		// インデックス取得変数定義
		int index = params.getIndex();

		// 入力パラメータで受け取ったAddressをカンマで分割
		addresses = params.getAddressGroupList().get(index).getAddress();
		strAry = addresses.split(",");

		// アドレス情報取得
		List<Addresses> addressObjectList;
		try {
			// アドレス情報取得
			addressObjectList = queryDAO.executeForObjectList(
					"AddressesBLogic-10", inputBase);
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage(
								"FK061007", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060016"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

		// アドレス情報を格納
		for (int i = 0; i < addressObjectList.size(); i++) {
			log.debug("アドレス情報格納:" + i);
			AddressesAndGroupsInfo addressesAndGroups =
											new AddressesAndGroupsInfo();
		    // SEQ_NO
			addressesAndGroups.setSeqNo(
					addressObjectList.get(i).getSeqNo());
			// オブジェクト名
			addressesAndGroups.setName(addressObjectList.get(i).getName());
			// オブジェクトタイプ
			addressesAndGroups.setObjectType(ADDRESS);
			// チェック情報
			addressesAndGroups.setChkFlg(false);

			// チェックボックスON/OFF判別
			for (int j = 0; j < strAry.length; j++) {
				log.debug("チェックボックスON/OFF判別:" + j);
		    	// 入力パラメータのアドレス情報と、
				// DBから取得したアドレス名が一致する場合
		    	if (strAry[j].equals(addressObjectList.get(i).getName())) {
		    		log.debug("アドレス名一致:" + strAry[j]);
		    		// チェック情報
		    		addressesAndGroups.setChkFlg(true);
		    	}
		    }
			outputAddressAndGroup.add(addressesAndGroups);
		}

		// UPDATED: 2016.11 by Plum Systems Inc.
		// アドレスグループ除外対応 -->

		// アドレスグループ検索条件設定
//		AddressGroups searchAddressGroupKey = new AddressGroups();
//		searchAddressGroupKey.setVsysId(vsysId);
//		searchAddressGroupKey.setGenerationNo(CusconConst.ZERO_GENE);
//		searchAddressGroupKey.setModFlg(CusconConst.DEL_NUM);
//		searchAddressGroupKey.setName(
//				params.getAddressGroupList().get(index).getName());
//
//		List<AddressGroups> addressGroupObjectList;
//		try {
//			// アドレスグループ一覧情報取得
//			//（但し現在編集中のアドレスグループは除く）
//			addressGroupObjectList =
//				queryDAO.executeForObjectList(
//						"AddressGroupsBLogic-2", searchAddressGroupKey);
//		} catch(Exception e) {
//			// トランザクションロールバック
//			TransactionUtil.setRollbackOnly();
//			// DBアクセスエラー
//			Object[] objectgArray = {e.getMessage()};
//			log.fatal(messageAccessor.getMessage(
//					"FK061007", objectgArray, params.getUvo()));
//			messages.add("message", new BLogicMessage("DK060016"));
//			result.setErrors(messages);
//			result.setResultString("failure");
//        	return result;
//		}
//
//		// アドレスグループ情報を格納
//		for (int i = 0; i < addressGroupObjectList.size(); i++) {
//			log.debug("アドレスグループ情報格納:" + i);
//			AddressesAndGroupsInfo addressAndAddressGrp4Add =
//												new AddressesAndGroupsInfo();
//			// SEQ_NO
//			addressAndAddressGrp4Add.setSeqNo(
//					addressGroupObjectList.get(i).getSeqNo());
//			// オブジェクト名
//			addressAndAddressGrp4Add.setName(
//					addressGroupObjectList.get(i).getName());
//			// オブジェクトタイプ
//			addressAndAddressGrp4Add.setObjectType(ADDRESS_GROUP);
//			// チェック情報
//			addressAndAddressGrp4Add.setChkFlg(false);
//
//			// チェックボックスON/OFF判別
//			for (int j = 0; j < strAry.length; j++) {
//				log.debug("チェックボックスON/OFF判別(グループ用):" + j);
//		    	// 入力パラメータのアドレスグループ情報と、
//				// DBから取得したアドレスグループ名が一致する場合
//		    	if (strAry[j].equals(addressGroupObjectList.get(i).getName())) {
//		    		log.debug("アドレス名一致(グループ用):" + strAry[j]);
//		    		// チェック情報
//		    		addressAndAddressGrp4Add.setChkFlg(true);
//		    	}
//		    }
//			outputAddressAndGroup.add(addressAndAddressGrp4Add);
//		}
		// --> アドレスグループ除外対応

		// 結果設定
		output.setName(params.getAddressGroupList().get(index).getName());
		output.setAddressAndAddressGrp4Add(outputAddressAndGroup);
		// レスポンス返却
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("EditAddressGroupBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
