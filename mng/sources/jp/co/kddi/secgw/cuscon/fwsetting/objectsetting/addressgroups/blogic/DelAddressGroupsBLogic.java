/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : DelAddressGroupsBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/23     morisou                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.blogic;

import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.AddressGroups;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.AddressGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectAddressGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.dto.AddressGroupsOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.dto.DelAddressGroupsInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.AddressLinksInfo;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : DelAddressGroupsBLogic
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/23
 *          新規作成
 * @see
 */
public class DelAddressGroupsBLogic implements BLogic<DelAddressGroupsInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.blogic.DelAddressGroupsBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : BLogicResult
	 * 機能概要 :
	 * @param params
	 * @return
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(DelAddressGroupsInput params) {
		log.debug("DelAddressGroupsBLogic処理開始");

		int seqNo;     // シーケンス番号
		int count;     // リンク数

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		 // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");
		AddressGroupsOutput output = new AddressGroupsOutput();

		// セッション情報.CusconUVOからVSYS_IDを取得
		CusconUVO uvo = params.getUvo();
		String vsysId = uvo.getVsysId();

		// 画面から渡されたアドレスリストを取得
		List<SelectAddressGroupList> addressGroupList4Del = params.getAddressGroupList();

		int delSize = addressGroupList4Del.size();
		int delNum = 0;
		String delStr = "";
		// 画面から渡されたアドレスグループリストの件数分、以下の処理を実行
		for (int i = 0; i < addressGroupList4Del.size(); i++) {
			log.debug("画面から渡されたアドレスグループリストの件数分繰り返す:" + i);
			// 検索条件設定
			AddressGroups searchAddressGroupKey = new AddressGroups();
			searchAddressGroupKey.setVsysId(vsysId);                          // Vsys-ID
			searchAddressGroupKey.setGenerationNo(CusconConst.ZERO_GENE);  // 世代番号
			searchAddressGroupKey.setName(addressGroupList4Del.get(i).getName());       // アドレス名

			try {
				// アドレスグループオブジェクトテーブルを検索し、削除対象アドレスグループのSEQ_NOを取得
				List<AddressGroups> addressGroupObjectList = queryDAO.executeForObjectList(
						"AddressGroupsBLogic-3", searchAddressGroupKey);
				seqNo = addressGroupObjectList.get(0).getSeqNo();

				// 検索条件設定
				AddressLinksInfo selectAddressLinks = new AddressLinksInfo();
				selectAddressLinks.setGrpLinkSeqNo(seqNo);
				selectAddressLinks.setSrcLinkSeqNo(seqNo);
				selectAddressLinks.setDstLinkSeqNo(seqNo);

				// 当該アドレスグループがリンクされている件数を取得
				count= queryDAO.executeForObject("AddressGroupsBLogic-14", selectAddressLinks, Integer.class);

				if (count > 0) {
					log.debug("オブジェクト削除不可:" + addressGroupList4Del.get(i).getName());
					delNum++;

					// 削除名称を連結する。
					if(delNum == 1) {
						delStr = addressGroupList4Del.get(i).getName();
					} else {
						delStr = delStr + ", " + addressGroupList4Del.get(i).getName();
					}
					if(delSize == delNum) {
						// 検索結果が1件以上の場合、削除不可エラー
						// レスポンスデータを作成し返却
						// 削除不可エラーメッセージを出力する。
						// トランザクションロールバック
						TransactionUtil.setRollbackOnly();
						Object[] objectgArray = {delStr};
						log.debug(messageAccessor.getMessage(
			        			"DK060002", objectgArray, params.getUvo()));
						messages.add("message",new BLogicMessage("DK060070", objectgArray));
						result.setErrors(messages);
						result.setResultString("failure");
			        	return result;
					}
					// 削除可能
				} else {

					// アドレスグループに紐づくリンク情報削除（T_AddressGroupsLink）
					updateDAO.execute("AddressGroupsBLogic-5", seqNo);

					// アドレスグループオブジェクトテーブルのレコードの変更フラグを'3'（削除）に更新
					updateDAO.execute("AddressGroupsBLogic-4", seqNo);

				}

			} catch(Exception e) {
				// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
				// DBアクセスエラー
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FK061006", objectgArray, params.getUvo()));
				messages.add("message", new BLogicMessage("DK060015"));
				result.setErrors(messages);
				result.setResultString("failure");
	        	return result;
			}
		}

		try {
			// 一覧情報検索共通処理
			AddressGroupList addressGroupList = new AddressGroupList();
			output.setAddressGroupList(addressGroupList.getAddressGroupList(queryDAO, uvo));
		} catch(Exception e) {
			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"FK061006", objectgArray, params.getUvo()));
        	messages.add("message", new BLogicMessage("DK060011"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

		 if(delNum != 0) {
		    	// 削除不可オブジェクトの名称出力
		    	log.debug("削除不可オブジェクト名称:" + delStr);
				Object[] objectgArray = {delStr};
				output.setMessage(messageAccessor.getMessage("DK060070", objectgArray));
		 }

		// レスポンス返却
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("DelAddressGroupsBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理開始");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理開始");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
