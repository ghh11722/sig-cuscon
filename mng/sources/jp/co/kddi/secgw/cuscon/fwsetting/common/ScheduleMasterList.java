/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ScheduleMasterList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/04     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.common;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.common.vo.Schedules;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : ScheduleMasterList
 * 機能概要 : スケジュール一覧情報を取得する。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/04
 *          新規作成
 * @see
 */
public class ScheduleMasterList {

    private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.common.ScheduleMasterList");
    // メッセージクラス
	private static MessageAccessor messageAccessor = null;

    public ScheduleMasterList(){
    	log.debug("コンストラクタ2処理開始");
    	log.debug("コンストラクタ2処理終了");
    }

    /**
	 * メソッド名 : getScheduleList
	 * 機能概要 : スケジュール一覧を取得する。
	 * @param vsysId Vsys-ID
	 * @param processFlg 処理フラグ  0：オブジェクト、1：ポリシー、ロールバック
	 * @return List<Schedules> スケジュールリスト
     * @throws Exception
	 */
	public List<Schedules> getScheduleMasterList(QueryDAO queryDAO, CusconUVO uvo,
								int processFlg, int generationNo) throws Exception {
		log.debug("getScheduleMasterList処理開始:vsysId = " +
									uvo.getVsysId() + ", processFlg = " + processFlg);
		InputBase base = new InputBase();
		base.setVsysId(uvo.getVsysId());
		base.setModFlg(CusconConst.DEL_NUM);
		base.setGenerationNo(generationNo);


		// スケジュールリスト
		List<Schedules> scheduleList = null;
		try {
			// スケジュールリストを取得する。
			scheduleList = queryDAO.executeForObjectList("CommonT_Schedules-4", base);
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FZ011028", objectgArray, uvo));
			throw e;
		}

		// オブジェクト名
		String name = null;
//		if(scheduleList !=null) {
//			log.debug("スケジュールリストが存在");
//			// オブジェクト名に初期値を設定する。
//			name = scheduleList.get(0).getName();
//		}
		// 周期
		int recurrence;
		// 時刻リスト
		List<String> timeList = new ArrayList<String>();
		String time = null;
		// 結果格納用リスト
		Schedules resultSchedule = new Schedules();
		List<Schedules> resultList = new ArrayList<Schedules>();

		// 処理フラグが1の場合
		if(processFlg == 1) {
			log.debug("ポリシー設定orロールバック処理");
			resultSchedule.setName(CusconConst.NONE);
			resultSchedule.setRecurrence(-1);
			resultSchedule.setTimeList(null);
			resultList.add(resultSchedule);
			resultSchedule =  new Schedules();

		}
		// スケジュールリスト分繰り返す。
		for(int i=0; i<scheduleList.size(); i++) {
			log.debug("スケジュールリスト生成処理:" + i);
			recurrence = scheduleList.get(i).getRecurrence();

			// recurrenceが0の場合
			if(recurrence == CusconConst.DAILY) {
				log.debug("DAILY");
				// 開始時刻/終了時刻を設定する。
				time = scheduleList.get(i).getStartTime() + "-" + scheduleList.get(i).getEndTime();
			// recurrenceが1の場合
			} else if(recurrence == CusconConst.WEEKLY) {
				log.debug("WEEKLY");
				// 曜日/開始時刻/終了時刻を設定する。
				time = convWeekIntToStr(scheduleList.get(i).getDayOfWeek()) + "@" +
				scheduleList.get(i).getStartTime() + "-" + scheduleList.get(i).getEndTime();
			// recurrenceが2の場合
			} else {
				log.debug("NONRECURRING");
				// 開始日時/開始時刻/終了日時/終了時刻を設定する。
				time = scheduleList.get(i).getStartDate() + "@" +
						scheduleList.get(i).getStartTime() + "-" +
					scheduleList.get(i).getEndDate() + "@" + scheduleList.get(i).getEndTime();
			}

			timeList.add(time);

			name = scheduleList.get(i).getName();
			// 現在と次のオブジェクト名が同じでない場合
			if(i == scheduleList.size() -1 ||
					!name.equals(scheduleList.get(i + 1).getName())) {
				log.debug("オブジェクト名不一致orカウントが最後:name = " +
					name + ", scheduleList.get(i).getName() = " +
						scheduleList.get(i).getName() +
							", カウント =" + (scheduleList.size()-1));
				resultSchedule.setName(scheduleList.get(i).getName());
				resultSchedule.setRecurrence(recurrence);
				resultSchedule.setTimeList(timeList);
				resultList.add(resultSchedule);
				resultSchedule =  new Schedules();
				timeList = new ArrayList<String>();
			}
		}

		log.debug("getScheduleMasterList処理終了:resultList.size() = " +
															resultList.size());

		return resultList;
	}
	/**
	 * メソッド名 : convWeekIntToStr
	 * 機能概要 : 曜日を対応する日付(文字列)に変換する。
	 * @param dayOfWeek 曜日
	 * @return 変換後曜日(String)
	 */
	private String convWeekIntToStr(int dayOfWeek) {
		log.debug("convWeekIntToStr処理開始:dayOfWeek = " + dayOfWeek);
		String result = null;

		if(dayOfWeek == CusconConst.SUN) {
			log.debug("日曜");
			result = CusconConst.JPA_SUN;

		} else if(dayOfWeek == CusconConst.MON) {
			log.debug("月曜");
			result = CusconConst.JPA_MON;

		} else if(dayOfWeek == CusconConst.TUE) {
			log.debug("火曜");
			result = CusconConst.JPA_TUE;

		} else if(dayOfWeek == CusconConst.WED) {
			log.debug("水曜");
			result = CusconConst.JPA_WED;

		} else if(dayOfWeek == CusconConst.THU) {
			log.debug("木曜");
			result = CusconConst.JPA_THU;

		} else if(dayOfWeek == CusconConst.FRI) {
			log.debug("金曜");
			result = CusconConst.JPA_FRI;

		} else {
			log.debug("土曜");
			result = CusconConst.JPA_SAT;
		}
		log.debug("convWeekIntToStr処理終了:result = " + result);
		return result;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor msgAcc){
		log.debug("setMessageAccessor処理開始");
	    setMsgAcc(msgAcc);
	    log.debug("setMessageAccessor処理終了");
	}

	/**
	 * メソッド名 : setMsgAcc
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public static void setMsgAcc(MessageAccessor messageAccessor) {
		log.debug("setMsgAcc処理開始");
		ScheduleMasterList.messageAccessor = messageAccessor;
		log.debug("setMsgAcc処理終了");
	}

	/**
	 * メソッド名 : getMessage
	 * 機能概要 : messageAccessorをゲットする。
	 * @return MessageAccessor
	 */
	public static MessageAccessor getMessage() {
		log.debug("getMessage処理");
		return messageAccessor;
	}
}
