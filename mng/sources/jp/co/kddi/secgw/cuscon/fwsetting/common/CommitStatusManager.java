/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CommitStatusManager.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/06/16     nomurays                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.common;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.CommitStatus;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

import org.apache.log4j.Logger;

/**
 * クラス名 : CommitStatusManager
 * 機能概要 : コミットステータス管理クラス
 * 備考 :
 * @author nomurays
 * @version 1.0 nomurays
 *          Created 2010/06/16
 *          新規作成
 * @see
 */
public class CommitStatusManager {

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 */
	public CommitStatusManager() {
	}

	// メッセージクラス
	private static MessageAccessor messageAccessor = null;

	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.common.CommitStatusManager");

	/**
	 * メソッド名 : isCommitting
	 * 機能概要 : ステータスがコミット中か判定する。
	 * @param queryDAO QueryDAO
	 * @param uvo CusconUVO
	 * @return コミット中 <code>true</code>
	 * @throws Exception
	 */
	public boolean isCommitting(QueryDAO queryDAO, CusconUVO uvo) throws Exception {

		log.debug("isCommitting処理開始 vsysId = " + uvo.getVsysId());

		CommitStatus commtStat = getCommitStatus(queryDAO, uvo);

		if (commtStat != null && commtStat.getStatus() == CusconConst.STAT_COMMITTING) {
			// コミット中
			log.debug("isCommitting処理終了:コミット中");
			return true;
		}

		// コミット中以外
		log.debug("isCommitting処理終了:コミット中以外");
		return false;
	}

	/**
	 * メソッド名 : コミットステータス取得
	 * 機能概要 : コミットステータスを取得する
	 * @param queryDAO QueryDAO
	 * @param uvo CusconUVO
	 * @return コミットステータス情報
	 * @throws Exception
	 */
	public CommitStatus getCommitStatus(QueryDAO queryDAO, CusconUVO uvo) throws Exception {
		log.debug("getCommitStatus処理開始 vsysId = " + uvo.getVsysId());
		CommitStatus commitStatus = null;
		try {
			commitStatus = queryDAO.executeForObject("CommonT_CommitStatus-1", uvo.getVsysId(), CommitStatus.class);
		} catch (Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = { e.getMessage() };
			log.fatal(messageAccessor.getMessage("FZ011037", objectgArray, uvo));
			throw e;
		}
		log.debug("getCommitStatus処理終了");
		return commitStatus;
	}

	/**
	 * メソッド名 : コミットステータス登録
	 * 機能概要 : コミットステータスを登録する
	 * @param updateDAO UpdateDAO
	 * @param uvo CusconUVO
	 * @param commitStatus コミットステータス情報
	 * @throws Exception
	 */
	public void insertCommitStatus(UpdateDAO updateDAO, CusconUVO uvo, CommitStatus commitStatus) throws Exception {
		log.debug("insertCommitStatus処理開始 vsysId = " + uvo.getVsysId());
		try {
			updateDAO.execute("CommonT_CommitStatus-2", commitStatus);
		} catch (Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = { e.getMessage() };
			log.fatal(messageAccessor.getMessage("FZ011039", objectgArray, uvo));
			throw e;
		}
		log.debug("insertCommitStatus処理終了");
	}

	/**
	 * メソッド名 : コミットステータス削除
	 * 機能概要 : コミットステータスを削除する
	 * @param updateDAO UpdateDAO
	 * @param uvo CusconUVO
	 * @throws Exception
	 */
	public void deleteCommitStatus(UpdateDAO updateDAO, CusconUVO uvo) throws Exception {
		log.debug("deleteCommitStatus処理開始 vsysId = " + uvo.getVsysId());
		int delCnt = 0;
		try {
			delCnt = updateDAO.execute("CommonT_CommitStatus-3", uvo.getVsysId());
		} catch (Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = { e.getMessage() };
			log.fatal(messageAccessor.getMessage("FZ011039", objectgArray, uvo));
			throw e;
		}
		log.debug("deleteCommitStatus処理終了 削除件数 = " + delCnt);
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		CommitStatusManager.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}

//2011.10.31 add start inoue@prosite
	/**
	 * メソッド名 : isCommitting_Processing
	 * 機能概要 : ステータスがコミット中(t_commitprocessing)か判定する。
	 * @param queryDAO QueryDAO
	 * @param uvo CusconUVO
	 * @return コミット中 <code>true</code>
	 * @throws Exception
	 */
	public boolean isCommitting_Processing(QueryDAO queryDAO, CusconUVO uvo) throws Exception {

		log.debug("isCommitting_Processing処理開始");

		int commitprocessing_flg = getCommitStatus_Processing(queryDAO, uvo);

		if (commitprocessing_flg == CusconConst.COMMIT_ON) {
			// コミット中
			log.debug("isCommitting_Processing処理終了:コミット中");
			return true;
		}

		// コミット中以外
		log.debug("isCommitting_Processing処理終了:コミット中以外");
		return false;
	}
	/**
	 * メソッド名 : コミットステータス取得
	 * 機能概要 : コミットステータスを取得する
	 * @param queryDAO QueryDAO
	 * @param uvo CusconUVO
	 * @return コミットステータス情報
	 * @throws Exception
	 */
	public int getCommitStatus_Processing(QueryDAO queryDAO, CusconUVO uvo) throws Exception {
		log.debug("getCommitStatus_Processing処理開始");
		int commitprocessing_flg = 0;
		try {
			// システム情報のモード値を取得する
			commitprocessing_flg = queryDAO.executeForObject("CommonT_CommitStatus-Processing", null, java.lang.Integer.class);
		} catch (Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = { e.getMessage() };
			log.fatal(messageAccessor.getMessage("FZ011042", objectgArray, uvo));
			throw e;
		}
		log.debug("getCommitStatus_Processing処理終了");
		return commitprocessing_flg;
	}
//2011.10.31 add end inoue@prosite

}