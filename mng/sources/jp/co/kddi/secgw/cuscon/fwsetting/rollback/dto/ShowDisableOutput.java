/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ShowDisableOutput.java
 *
 * [変更履歴]
 * 日付        更新者              内容
 * 2013/09/01  kkato@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto;

/**
 * クラス名 : ShowDisableOutput
 * 機能概要 :スパイウェアチェック項目選択時の出力データクラス
 * 備考 :
 * @author kkato@PROSITE
 * @version 1.0 kkato@PROSITE
 *          Created 2013/09/01
 *          新規作成
 * @see
 */
public class ShowDisableOutput {

	// セキュリティルール有効/無効フラグ
	private String disableFlg = null;

	/**
	 * メソッド名 : disableFlgのGetterメソッド
	 * 機能概要 : disableFlgを取得する。
	 * @return disableFlg
	 */
	public String getDisableFlg() {
		return this.disableFlg;
	}
	/**
	 * メソッド名 : disableFlgのSetterメソッド
	 * 機能概要 : disableFlgをセットする。
	 * @param disableFlg
	 */
	public void setDisableFlg(String disableFlg) {
		this.disableFlg = disableFlg;
	}
}
