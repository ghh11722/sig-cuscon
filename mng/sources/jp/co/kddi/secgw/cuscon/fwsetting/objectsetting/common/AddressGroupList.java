/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddressGroupList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/19     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectAddressGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.LinkInfo;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : AddressGroupList
 * 機能概要 : アドレスグループ一覧を表示するための情報を取得する。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/19
 *          新規作成
 * @see
 */
public class AddressGroupList {

	private static Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.AddressGroupList");

	// メッセージクラス
    private static MessageAccessor messageAccessor = null;

    /**
     * メソッド名 : コンストラクタ
     * 機能概要 : -
     */
    public AddressGroupList() {
    	log.debug("コンストラクタ2開始");
    	log.debug("コンストラクタ2終了");
    }

	/**
	 * メソッド名 :getAddressGroupList
	 * 機能概要 : アドレスグループ一覧情報取得処理
	 * @param vsysId
	 * @return resultList
	 */
	public List<SelectAddressGroupList> getAddressGroupList(
						QueryDAO queryDAO, CusconUVO uvo) throws Exception {
		log.debug("getAddressGroupList開始");
		// 変数初期化処理
		String lastName = null;                          // 切り替え判別用アドレスグループ名
		int memberCount = 0;                             // メンバカウンタ
		StringBuffer address = new StringBuffer();       // アドレス＆アドレスグループ連結文字列
		ArrayList<SelectAddressGroupList> resultList =
				new ArrayList<SelectAddressGroupList>(); // 返却用リスト
		List<LinkInfo> linkList;

		// InputBase設定
		InputBase inputBase = new InputBase();
		inputBase.setVsysId(uvo.getVsysId());               // Vsys-ID
		inputBase.setGenerationNo(CusconConst.ZERO_GENE);   // 世代番号
		inputBase.setModFlg(CusconConst.DEL_NUM);           // 変更フラグ

		try {
			// アドレスグループリンク情報取得（名称、オブジェクトタイプ、リンク先名称の昇順でリストに格納）
			linkList = queryDAO.executeForObjectList("AddressGroupsBLogic-1", inputBase);

		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		log.fatal(messageAccessor.getMessage("FK061008", objectgArray, uvo));
    		throw e;

		}
		// 検索結果が0件
		if(linkList.size() == 0) {
			log.debug("getAddressGroupList終了1:" + linkList.size());
			return null;
		}

		// アドレスグループリンク情報の件数分、以下の処理を行う
		for (int i = 0; i < linkList.size(); i++) {
			log.debug("アドレスグループリンク情報件数:" + i);
			// アドレスグループ名が切り替わった場合、以下の処理を行う
			if (i != 0 && !lastName.equals(linkList.get(i).getName())) {
				log.debug("アドレスグループ名が切り替わり");
				// 返却用リスト設定処理
				setResultList(lastName, memberCount, address, resultList);
				// メンバカウンタリセット
				memberCount = 0;
				// アドレスリセット
				address = new StringBuffer();
			}
			// メンバカウントアップ
			memberCount++;
			// メンバ数が1の場合
			if (memberCount == 1) {
				log.debug("メンバ数が1");
				// 文字連結（1個目限定）
				address = new StringBuffer(String.valueOf(linkList.get(i).getLinkName()));
			// メンバ数が2以上の場合
			} else {
				log.debug("メンバ数が2以上");
				// 文字連結（2個目以降）
				address.append(",").append(linkList.get(i).getLinkName());
			}

			// 切り替え判別用アドレスグループ名設定
			lastName = linkList.get(i).getName();
		}
		// 返却用リスト設定処理
		setResultList(lastName, memberCount, address, resultList);

		log.debug("getAddressGroupList終了2:" + resultList.size());
		return resultList;
	}

	/**
	 * メソッド名 :setResultList
	 * 機能概要 : 返却用リスト設定処理
	 * @param resultList
	 * @param address
	 * @param memberCount
	 * @param lastName
	 * @param
	 */
	private void setResultList (String lastName, int memberCount,
			StringBuffer address, ArrayList<SelectAddressGroupList> resultList) {
		log.debug("setResultList開始");
		SelectAddressGroupList selectAddressGroupListOutput = new SelectAddressGroupList();
		// Name
		selectAddressGroupListOutput.setName(lastName);
		// Members
		selectAddressGroupListOutput.setMembers(memberCount);
		// Address
		selectAddressGroupListOutput.setAddress(address.toString());
		// resultListにセット
		resultList.add(selectAddressGroupListOutput);
		log.debug("setResultList終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor msgAcc){
		log.debug("setMessageAccessor処理開始");
	    setMsgAcc(msgAcc);
	    log.debug("setMessageAccessor処理終了");
	}

	/**
	 * メソッド名 : setMsgAcc
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public static void setMsgAcc(MessageAccessor messageAccessor) {
		log.debug("setMsgAcc処理開始");
		AddressGroupList.messageAccessor = messageAccessor;
		log.debug("setMsgAcc処理終了");
	}

	/**
	 * メソッド名 : getMessage
	 * 機能概要 : messageAccessorをゲットする。
	 * @return MessageAccessor
	 */
	public static MessageAccessor getMessage() {
		log.debug("getMessage処理");
		return messageAccessor;
	}
}
