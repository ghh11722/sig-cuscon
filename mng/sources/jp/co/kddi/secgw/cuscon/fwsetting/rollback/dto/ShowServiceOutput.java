/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ShowServiceOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/04     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectObjectList;

/**
 * クラス名 : ShowServiceOutput
 * 機能概要 :Service項目選択時の出力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/04
 *          新規作成
 * @see
 */
public class ShowServiceOutput {

	// サービス名リスト
	private List<SelectObjectList> serviceList = null;

	// サービスマスタリスト
	private List<SelectObjectList> serviceMasterList = null;

	// サービス名
	private String serviceName = null;

	/**
	 * メソッド名 : serviceNameのGetterメソッド
	 * 機能概要 : serviceNameを取得する。
	 * @return serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}
	/**
	 * メソッド名 : serviceNameのSetterメソッド
	 * 機能概要 : serviceNameをセットする。
	 * @param serviceName
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	/**
	 * メソッド名 : serviceListのGetterメソッド
	 * 機能概要 : serviceListを取得する。
	 * @return serviceList
	 */
	public List<SelectObjectList> getServiceList() {
		return serviceList;
	}
	/**
	 * メソッド名 : serviceListのSetterメソッド
	 * 機能概要 : serviceListをセットする。
	 * @param serviceList
	 */
	public void setServiceList(List<SelectObjectList> serviceList) {
		this.serviceList = serviceList;
	}

	/**
	 * メソッド名 : serviceMasterListのGetterメソッド
	 * 機能概要 : serviceMasterListを取得する。
	 * @return serviceMasterList
	 */
	public List<SelectObjectList> getServiceMasterList() {
		return serviceMasterList;
	}

	/**
	 * メソッド名 : serviceMasterListのSetterメソッド
	 * 機能概要 : serviceMasterListをセットする。
	 * @param serviceMasterList
	 */
	public void setServiceMasterList(List<SelectObjectList> serviceMasterList) {
		this.serviceMasterList = serviceMasterList;
	}
}
