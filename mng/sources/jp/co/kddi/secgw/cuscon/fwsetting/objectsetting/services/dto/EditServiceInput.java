/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditServiceInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/10     kinoshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectServiceList;

/**
 * クラス名 : EditServiceInput
 * 機能概要 :
 * 備考 :
 * @author kinoshtt
 * @version 1.0 kinoshtt
 *          Created 2010/03/10
 *          新規作成
 * @see
 */
public class EditServiceInput {

	// アドレス情報リスト
	private List<SelectServiceList> serviceList = null;

	// インデックス
	private int index = 0;

	/**
	 * メソッド名 : serviceListのGetterメソッド
	 * 機能概要 : serviceListを取得する。
	 * @return serviceList
	 */
	public List<SelectServiceList> getServiceList() {
		return serviceList;
	}

	/**
	 * メソッド名 : serviceListのSetterメソッド
	 * 機能概要 : serviceListをセットする。
	 * @param serviceList
	 */
	public void setServiceList(List<SelectServiceList> serviceList) {
		this.serviceList = serviceList;
	}

	/**
	 * メソッド名 : indexのGetterメソッド
	 * 機能概要 : indexを取得する。
	 * @return index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * メソッド名 : indexのSetterメソッド
	 * 機能概要 : indexをセットする。
	 * @param index
	 */
	public void setIndex(int index) {
		this.index = index;
	}
}
