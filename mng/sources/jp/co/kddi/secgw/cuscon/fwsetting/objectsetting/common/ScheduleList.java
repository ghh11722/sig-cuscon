/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ScheduleList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common;

import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.Schedules;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : ScheduleList
 * 機能概要 :
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class ScheduleList {

	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.ScheduleList");
	// メッセージクラス
    private static MessageAccessor messageAccessor = null;

	public ScheduleList(){
		log.debug("コンストラクタ2処理開始");
		log.debug("コンストラクタ2処理終了");
	}


	/**
	 * メソッド名 : checkScheduleName
	 * 機能概要 : オブジェクト名が使用されているかのチェックを行う。
	 * @param base スケジュールデータクラス
	 * @return boolean チェック結果
	 * @throws Exception
	 */
	public boolean checkScheduleName(UpdateDAO updateDAO, QueryDAO queryDAO,
								CusconUVO uvo, Schedules base) throws Exception {
		log.debug("checkScheduleName処理開始");
		List<Schedules> scheduleList = null;

		try {
			scheduleList = queryDAO.executeForObjectList("UpdateSchedulesBLogic-1", base);
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		log.fatal(messageAccessor.getMessage("FK061028", objectgArray, uvo));
    		throw e;
    	}


    	if(scheduleList.size() != 0) {
    		log.debug("検索結果が存在する。");
    		// 検索結果が存在する、かつ削除フラグが3でない場合
    		if(scheduleList.get(0).getModFlg() != CusconConst.DEL_NUM){
    			log.debug("既に該当オブジェクト名が存在する。");

    			return false;
    		}

	    	// 検索結果が存在する、かつ削除フラグが3の場合
	    	if(scheduleList.get(0).getModFlg() == CusconConst.DEL_NUM) {
	    		log.debug("削除フラグが3のデータを削除する。");
	    		base.setModFlg(CusconConst.DEL_NUM);

	    		try {
		    		// データを削除する。
		    		updateDAO.execute("UpdateSchedulesBLogic-2", base);
	    		} catch(Exception e) {
	        		// トランザクションロールバック
	        		TransactionUtil.setRollbackOnly();
	        		// DBアクセスエラー
	        		Object[] objectgArray = {e.getMessage()};
	        		log.fatal(messageAccessor.getMessage("FK061026", objectgArray, uvo));
	        		throw e;
	        	}
	    	}
    	}

    	log.debug("checkScheduleName処理終了:true");
    	return true;
	}

	/**
	 * メソッド名 : registDaiySchedule
	 * 機能概要 : DAILY情報を登録する。
	 * @param base DB検索条件
	 * @param timeList 時刻リスト
	 * @throws Exception
	 */
	public void registDaiySchedule(UpdateDAO updateDAO, CusconUVO uvo,
					Schedules base, String[] timeList) throws Exception {
		log.debug("registDaiySchedule処理開始:timeList.size() = " + timeList.length);
		for(int i=0; i<timeList.length; i++) {
			log.debug("registDaiySchedule処理開始:timeList.size() = " + timeList[i]);
		}
		// 時刻リスト分繰り返す。
		for(int i=0; i<timeList.length; i++) {
			log.debug("DAILY登録:" + i);
			// 開始時刻/終了時刻に分割する。
			String[] splitTime  = timeList[i].split("-");
			base.setStartTime(splitTime[0]);
			base.setEndTime(splitTime[1]);
			try {
				// DAILYの情報を登録する。
				updateDAO.execute("UpdateSchedulesBLogic-5", base);
			} catch(Exception e) {
        		// トランザクションロールバック
        		TransactionUtil.setRollbackOnly();
        		// DBアクセスエラー
        		Object[] objectgArray = {e.getMessage()};
        		log.fatal(messageAccessor.getMessage("FK061025", objectgArray, uvo));
        		throw e;
        	}
		}
		log.debug("registDaiySchedule処理終了");
	}

	/**
	 * メソッド名 : registWeeklySchedule
	 * 機能概要 : WEEKLY情報を登録する。
	 * @param base DB検索条件
	 * @param timeList 時刻リスト
	 * @throws Exception
	 */
	public void registWeeklySchedule(UpdateDAO updateDAO, CusconUVO uvo,
						Schedules base, String[] timeList) throws Exception {
		log.debug("registWeeklySchedule処理開始:timeList.size() = " + timeList.length);
		// 曜日/時刻リスト分繰り返す。
		for(int i=0; i<timeList.length; i++) {
			log.debug("WEEKLY登録:" + i);
			// 曜日/開始時刻/終了時刻に分割する。
			String[] splitWeek  = timeList[i].split("@");
			String[] splitTime = splitWeek[1].split("-");

			base.setDayOfWeek(convWeekStrToInt(splitWeek[0]));
			base.setStartTime(splitTime[0]);
			base.setEndTime(splitTime[1]);
			try {
				// WEEKLYの情報を登録する。
				updateDAO.execute("UpdateSchedulesBLogic-6", base);
			} catch(Exception e) {
        		// トランザクションロールバック
        		TransactionUtil.setRollbackOnly();
        		// DBアクセスエラー
        		Object[] objectgArray = {e.getMessage()};
        		log.fatal(messageAccessor.getMessage("FK061025", objectgArray, uvo));
        		throw e;
        	}
		}
		log.debug("registWeeklySchedule処理終了:timeList.size() = " + timeList.length);
	}

	/**
	 * メソッド名 : registNonSchedule
	 * 機能概要 : NONRECURRING情報を登録する。
	 * @param base DB検索条件
	 * @param timeList 時刻リスト
	 * @throws Exception
	 */
	public void registNonSchedule(UpdateDAO updateDAO, CusconUVO uvo,
						Schedules base, String[] timeList) throws Exception {
		log.debug("registNonSchedule処理開始:timeList.size() = " + timeList.length);
		// 開始日時/開始時刻/開始日時/終了時刻リスト分繰り返す。
		for(int i=0; i<timeList.length; i++) {
			log.debug("NONRECURRING登録:" + i);
			// 開始日時/開始時刻/開始日時/終了時刻に分割する。
			String[] splitDate  = timeList[i].split("-");
			String[] splitStart = splitDate[0].split("@");
			String[] splitEnd = splitDate[1].split("@");

			base.setStartDate(splitStart[0]);
			base.setStartTime(splitStart[1]);
			base.setEndDate(splitEnd[0]);
			base.setEndTime(splitEnd[1]);
			try {
				// NONRECURRINGの情報を登録する。
				updateDAO.execute("UpdateSchedulesBLogic-7", base);
			} catch(Exception e) {
        		// トランザクションロールバック
        		TransactionUtil.setRollbackOnly();
        		// DBアクセスエラー
        		Object[] objectgArray = {e.getMessage()};
        		log.fatal(messageAccessor.getMessage("FK061025", objectgArray, uvo));
        		throw e;
        	}
		}
		log.debug("registNonSchedule処理終了:timeList.size() = " + timeList.length);
	}

	/**
	 * メソッド名 : convWeekStrToInt
	 * 機能概要 : 曜日を対応する日付に変換する。
	 * @param dayOfWeek 曜日
	 * @return 変換後曜日(int)
	 */
	private int convWeekStrToInt(String dayOfWeek) {
		log.debug("convWeekStrToInt処理開始:dayOfWeek = " + dayOfWeek);
		int result = 0;

		if(dayOfWeek.equals(CusconConst.JPA_SUN)) {
			log.debug("日曜");
			result = CusconConst.SUN;

		} else if(dayOfWeek.equals(CusconConst.JPA_MON)) {
			log.debug("月曜");
			result = CusconConst.MON;

		} else if(dayOfWeek.equals(CusconConst.JPA_TUE)) {
			log.debug("火曜");
			result = CusconConst.TUE;

		} else if(dayOfWeek.equals(CusconConst.JPA_WED)) {
			log.debug("水曜");
			result = CusconConst.WED;

		} else if(dayOfWeek.equals(CusconConst.JPA_THU)) {
			log.debug("木曜");
			result = CusconConst.THU;

		} else if(dayOfWeek.equals(CusconConst.JPA_FRI)) {
			log.debug("金曜");
			result = CusconConst.FRI;

		} else {
			log.debug("土曜");
			result = CusconConst.SAT;
		}
		log.debug("convWeekStrToInt処理終了:result = " + result);
		return result;
	}

	/**
	 * メソッド名 : delDaiySchedule
	 * 機能概要 : DAILY情報を削除する。
	 * @param base DB検索条件
	 * @throws Exception
	 */
	public void delDaiySchedule(UpdateDAO updateDAO, CusconUVO uvo,
											Schedules base) throws Exception {

		log.debug("delDaiySchedule処理開始");
		try {
			// DAILYの情報を削除する。
			updateDAO.execute("UpdateSchedulesBLogic-8", base);
		} catch(Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		log.fatal(messageAccessor.getMessage("FK061026", objectgArray, uvo));
    		throw e;
    	}

		log.debug("delDaiySchedule処理終了");
	}

	/**
	 * メソッド名 : delWeeklySchedule
	 * 機能概要 : WEEKLY情報を削除する。
	 * @param base DB検索条件
	 * @throws Exception
	 */
	public void delWeeklySchedule(UpdateDAO updateDAO, CusconUVO uvo,
											Schedules base) throws Exception {
		log.debug("delWeeklySchedule処理開始");
		try {
			// WEEKLYの情報を削除する。
			updateDAO.execute("UpdateSchedulesBLogic-9", base);
		} catch(Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		log.fatal(messageAccessor.getMessage("FK061026", objectgArray, uvo));
    		throw e;
    	}
		log.debug("delWeeklySchedule処理終了");
	}

	/**
	 * メソッド名 : delNonSchedule
	 * 機能概要 : NONRECURRING情報を削除する。
	 * @param base DB検索条件
	 * @throws Exception
	 */
	public void delNonSchedule(UpdateDAO updateDAO, CusconUVO uvo,
											Schedules base) throws Exception {
		log.debug("delNonSchedule処理開始");
		try {
			// NONRECURRINGの情報を削除する。
			updateDAO.execute("UpdateSchedulesBLogic-10", base);
		} catch(Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		log.fatal(messageAccessor.getMessage("FK061026", objectgArray, uvo));
    		throw e;
    	}
		log.debug("delNonSchedule処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor msgAcc){
		log.debug("setMessageAccessor処理開始");
	    setMsgAcc(msgAcc);
	    log.debug("setMessageAccessor処理終了");
	}

	/**
	 * メソッド名 : setMsgAcc
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public static void setMsgAcc(MessageAccessor messageAccessor) {
		log.debug("setMsgAcc処理開始");
		ScheduleList.messageAccessor = messageAccessor;
		log.debug("setMsgAcc処理終了");
	}

	/**
	 * メソッド名 : getMessage
	 * 機能概要 : messageAccessorをゲットする。
	 * @return MessageAccessor
	 */
	public static MessageAccessor getMessage() {
		log.debug("getMessage処理");
		return messageAccessor;
	}
}
