/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ChangeAddReccurenceBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.blogic;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.dto.ChangeAddReccurenceInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.dto.ChangeAddReccurenceOutput;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicResult;

/**
 * クラス名 : ChangeAddReccurenceBLogic
 * 機能概要 :新規作成画面にてrecurrence変更した際の処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class ChangeAddReccurenceBLogic implements
										BLogic<ChangeAddReccurenceInput> {

	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.blogic.ChangeAddReccurenceBLogic");
	/**
	 * メソッド名 :execute
	 * 機能概要 : 新規作成画面にてrecurrence変更した際の処理を行う。
	 * @param param ChangeAddReccurenceInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(ChangeAddReccurenceInput param) {
		log.debug("ChangeAddReccurenceBLogic処理開始:" + param.getSelectRecurrence());
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");

		// アウトプットデータクラスに設定する。
        ChangeAddReccurenceOutput out = new ChangeAddReccurenceOutput();

        if(param.getName() != null) {
        	log.debug("nameが設定済");
        	// インプットをアウトプットに設定する。
        	out.setName(param.getName());
        }

        // ディリーの場合
        if(param.getSelectRecurrence() == 0) {
        	log.debug("デイリー選択");
        	result.setResultString("DailySuccess");
        } else if(param.getSelectRecurrence() == 1) {
        	log.debug("ウィークリー選択");
        	result.setResultString("WeeklySuccess");
        } else {
        	log.debug("指定日時選択");
        	result.setResultString("NonSuccess");
        }

	    result.setResultObject(out);

	    log.debug("ChangeAddReccurenceBLogic処理終了");
		return result;

	}

}
