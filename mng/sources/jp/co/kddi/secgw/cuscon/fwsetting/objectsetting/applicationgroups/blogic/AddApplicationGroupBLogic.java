/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddApplicationGroupBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/24     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.blogic;

import java.util.ArrayList;
import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.fwsetting.common.ApplicationList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.FilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.RegistLimitList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto.AddApplicationGroupInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto.AddApplicationGroupOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationList4Search;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectFilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;
import org.apache.log4j.Logger;

/**
 * クラス名 : AddApplicationGroupBLogic
 * 機能概要 : アプリケーショングループ追加時の処理を行う。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/24
 *          新規作成
 * @see
 */
public class AddApplicationGroupBLogic implements BLogic<AddApplicationGroupInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting." +
	"applicationgroups.blogic.AddApplicationGroupBLogic");

	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 : アプリケーショングループ追加時の処理を行う。
	 * @param params AddApplicationGroupInput入力データクラス
	 * @return result BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(AddApplicationGroupInput params) {
		log.debug("AddApplicationGroupBLogic処理開始");

		String aplGrpName;                                   // アプリケーショングループ名
		SelectFilterMasterList resultFltList;                // フィルタマスタ情報
		List<SelectApplicationList4Search> resultAplList;    // アプリケーションマスタ情報
		AddApplicationGroupOutput output;                    // 返却用リスト
		List<SelectedAplInfo> selectedAplList;               // 返却用選択中アプリケーションリスト

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();

		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();

		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = params.getUvo();

		/***********************************************************************
		 *  ①登録上限数チェック
		 **********************************************************************/
		List<RegistLimitList> limitList = null;
		try {
			// 登録上限値を取得する。
			limitList = queryDAO.executeForObjectList("CommonM_SystemConstant-2",
					null);
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラーログ出力
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061029",
					objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060026"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

		// 検索条件作成
		InputBase inputBase = new InputBase();
		inputBase.setVsysId(params.getUvo().getVsysId());
		inputBase.setGenerationNo(CusconConst.ZERO_GENE);
		inputBase.setModFlg(CusconConst.DEL_NUM);

		int registNum = 0;

		try {
			// オブジェクトの登録数を取得する。
			registNum = queryDAO.executeForObject("ApplicationGroupsBLogic-11",
					inputBase, java.lang.Integer.class);

		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage(
					"FK061016", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060026"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

		// 登録上限値とオブジェクト登録数の比較を行う。
		if(limitList.get(0).getAppGrp() <= registNum) {
			log.debug("登録上限値のため登録できませんでした。");
			messages.add("message", new BLogicMessage("DK060003"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

		try {
			/***********************************************************************
			 *  ②フィルタリスト取得（マスタ情報）
			 **********************************************************************/
			FilterMasterList filterMasterList = new FilterMasterList();
			resultFltList = filterMasterList.getFilterMasterList(queryDAO, uvo);


			/***********************************************************************
			 *  ③アプリケーションリスト取得（マスタ情報）
			 **********************************************************************/
			ApplicationList applicationList = new ApplicationList();
			resultAplList = applicationList.getApplicationList(queryDAO, uvo);

		} catch(Exception e) {
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("EK061004", objectgArray,
					params.getUvo()));
			messages.add("message", new BLogicMessage("DK060026"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}


		/***********************************************************************
		 *  ④選択中アプリケーションリスト設定（新規追加時はセッション内は空）
		 **********************************************************************/
		// 選択グループリストにセッション情報を設定
		selectedAplList = new ArrayList<SelectedAplInfo>();

		// 一覧から新規追加ボタン押下時でない場合
		if (params.getDisplayType() != null) {
			log.debug("DisplayTypeに値が入っている場合");
			selectedAplList = params.getSessionAplList();
		// 一覧から新規追加ボタン押下で遷移の場合
		} else {
			log.debug("DisplayTypeに値が入っていない場合");
			selectedAplList = null;
		}


		/***********************************************************************
		 *  ⑤レスポンス返却
		 **********************************************************************/
		// 画面種別がnullの場合（一覧画面から最初に遷移してきた場合）
		if (params.getDisplayType() == null || params.getDisplayType() == "") {
			log.debug("一覧画面から最初に遷移してきた場合は全情報クリア");
			// アプリケーショングループ名はクリア
			aplGrpName = null;
			selectedAplList = null;
		} else {
			log.debug("上記以外の場合");
			aplGrpName = params.getName();

		}

		// 返却用リスト
		output = new AddApplicationGroupOutput();
		output.setName(aplGrpName);
		output.setFilterList(resultFltList);
		output.setApplicationList(resultAplList);
		output.setSelectedAplList(selectedAplList);
		output.setDisplayType("登録");
		output.setSearchWord("");

		// レスポンス返却
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("AddApplicationGroupBLogic処理終了");
		return result;
	}


	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
