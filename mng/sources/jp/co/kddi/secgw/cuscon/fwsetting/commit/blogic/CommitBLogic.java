/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CommitBLogic.java
 *
 * [変更履歴]
 * 日付           更新者               内容
 * 2010/02/17     komakiys             更新情報一覧を取得する。
 * 2010/06/18     oohashij             コミットステータス確認の追加
 * 2011/06/14     ktakenaka@PROSITE    Webフィルタの追加
 * 2011/10/31     inoue@prosite        コミットステータス確認の変更
 * 2012/08/27     kkato@PROSITE        通信全断アラート対応
 * 2016/11/17     T.Yamazaki@Plum      通信全断アラート変更対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.commit.blogic;

import java.util.ArrayList;
import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.fwsetting.commit.dto.CommitInput;
import jp.co.kddi.secgw.cuscon.fwsetting.commit.dto.CommitOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.commit.vo.SelectCommitList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.CommitStatusManager;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;
import jp.terasoluna.fw.util.PropertyUtil;

import org.apache.log4j.Logger;

/**
 * クラス名 : CommitBLogic
 * 機能概要 : 更新情報一覧を取得する。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/17
 *          新規作成
 * @version 1.1 oohashij
 *          Created 2010/06/18
 *          コミットステータス確認の追加
 * @version 1.2 ktakenaka@PROSITE
 *          Created 2011/06/14
 *          Webフィルタの追加
 * @version 1.3 inoue@prosite
 *          Created 2011/10/31
 *          コミットステータス確認の変更
 * @version 1.4 kkato@prosite
 *          Created 2012/08/27
 *          通信全断アラート対応
 * @version 2.0 T.Yamazaki@Plum
 *          Updated 2016/11/17
 *          通信全断アラート変更対応
 * @see
 */
public class CommitBLogic implements BLogic<CommitInput> {

    /**
     * QueryDAO。
     * Springによりインスタンス生成され設定される。
     */
    private QueryDAO queryDAO = null;
    // メッセージクラス
	private MessageAccessor messageAccessor = null;

	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.commit.blogic.CommitBLogic");
	// 20120827 kkato@PROSITE add start
    // 区切り文字
	private static final String SPLIT = ",";
	// 20120827 kkato@PROSITE add end

    /**
     * メソッド名 :execute
     * 機能概要 : 更新オブジェクト名一覧取得処理を行う
     * @param param CommitBLogicの入力クラス
     * @return BLogicResult BLogicResultクラス
     * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
     */
    public BLogicResult execute(CommitInput param) {

    	log.debug("CommitBLogic処理開始");
    	BLogicResult result = new BLogicResult();
        // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
    	result.setResultString("failure");

        // 基本検索条件クラスにパラメータを設定する。
        InputBase base = new InputBase();
        base.setGenerationNo(0);
        base.setVsysId(param.getUvo().getVsysId());
        base.setModFlg(0);

        // セキュリティポリシー更新情報一覧
        List<SelectCommitList> rules = null;
        // アドレス更新情報一覧
        List<SelectCommitList> addresses = null;
        // アドレスグループ更新情報一覧
        List<SelectCommitList> addressGroups = null;
        // アプリケーショングループ更新情報一覧
        List<SelectCommitList> applicationGroups = null;
        // アプリケーションフィルタ更新情報一覧
        List<SelectCommitList> applicationFilters = null;
        // サービス更新情報一覧
        List<SelectCommitList> services = null;
        // サービスグループ更新情報一覧
        List<SelectCommitList> serviceGroups = null;
        // スケジュール更新情報一覧
        List<SelectCommitList> schedules = null;
        // 20110614 ktakenaka@PROSITE add start
        // Webフィルタ更新情報一覧
        List<SelectCommitList> urlFilters = null;
        // 20110614 ktakenaka@PROSITE add end
        // 20120827 kkato@PROSITE add start
        // セキュリティポリシー更新情報一覧
        List<SelectPolicyList> policyRules = null;
        // 20120827 kkato@PROSITE add end

        CommitStatusManager commtStatManager = new CommitStatusManager();

        try {
        	// コミットステータス確認
//2011/10/31 rep start inoue@prosite
//        	if (commtStatManager.isCommitting(queryDAO, param.getUvo())) {
        	if (commtStatManager.isCommitting_Processing(queryDAO, param.getUvo())) {
//2011/10/31 rep end inoue@prosite
    			messages.add("message", new BLogicMessage("DK070019"));
    			result.setErrors(messages);
    			result.setMessages(messages);
    			result.setResultString("noneFailure");
    			log.debug("CommitBLogic処理終了");
    			return result;
        	}
        } catch (Exception e) {
        	// コミットステータス確認失敗ログ出力
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage("EK071004", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK070025"));
			result.setErrors(messages);
			result.setMessages(messages);
			result.setResultString("noneFailure");
			return result;
        }

        try {
	        // セキュリティポリシー更新情報一覧を取得する。
	       rules = queryDAO.executeForObjectList(
	    		   					"CommonT_SecurityRules-2", base);

	        // アドレス更新情報一覧を取得する。
	        addresses =
	        	queryDAO.executeForObjectList("CommonT_Addresses-3", base);

	        // アドレスグループ更新情報一覧を取得する。
	        addressGroups =
	        	queryDAO.executeForObjectList("CommonT_AddressGroups-3", base);

	        // アプリケーショングループ更新情報一覧を取得する。
	        applicationGroups = queryDAO.executeForObjectList(
	        							"CommonT_ApplicationGroups-3", base);

	        // アプリケーションフィルタ更新情報一覧を取得する。
	        applicationFilters = queryDAO.executeForObjectList(
	        							"CommonT_ApplicationFilters-3", base);

	        // サービス更新情報一覧を取得する。
	        services =
	        	queryDAO.executeForObjectList("CommonT_Services-3", base);

	        // サービスグループ更新情報一覧を取得する。
	        serviceGroups = queryDAO.executeForObjectList(
	        								"CommonT_ServicesGroups-3", base);

	        // スケジュール更新情報一覧を取得する。
	        schedules =
	        	queryDAO.executeForObjectList("CommonT_Schedules-3", base);

	        // 20110614 ktakenaka@PROSITE add start
	        // Webフィルタ更新情報一覧を取得する。
	        urlFilters =
	        	queryDAO.executeForObjectList("CommonT_UrlFilters-2", base);
	        // 20110614 ktakenaka@PROSITE add end

	        // 20120827 kkato@PROSITE add start
        	base.setNonSubject(getHiddenObject());
	        // セキュリティポリシー更新情報一覧を取得する。
	        policyRules = queryDAO.executeForObjectList("CommonPolicyList-PolicyCheck", base);
	        // 20120827 kkato@PROSITE add end

        } catch(Exception e) {
        	// トランザクションロールバック
        	TransactionUtil.setRollbackOnly();
        	// コミット対象取得処理失敗ログ出力
        	Object[] objectgArray = {e.getMessage()};
        	log.fatal(messageAccessor.getMessage(
        			"FK071001", objectgArray, param.getUvo()));

        	messages.add("message", new BLogicMessage("DK070002"));
			result.setErrors(messages);
			result.setMessages(messages);
			result.setResultString("noneFailure");
			return result;
        }

        // セキュリティポリシー更新種別を設定する。
        setModName(rules);
        // アドレス更新種別を設定する。
        setModName(addresses);
        // アドレスグループ更新種別を設定する。
        setModName(addressGroups);
        // アプリケーショングループ更新種別を設定する。
        setModName(applicationGroups);
        // アプリケーションフィルタ更新種別を設定する。
        setModName(applicationFilters);
        // サービス更新種別を設定する。
        setModName(services);
        // サービスグループ更新種別を設定する。
        setModName(serviceGroups);
        // スケジュール更新種別を設定する。
        setModName(schedules);
        // 20110614 ktakenaka@PROSITE add start
        // Webフィルタ更新種別を設定する。
        setModName(urlFilters);
        // 20110614 ktakenaka@PROSITE add end

        // 0件でなければコミット対象フラグをtrueとする。
        if(!rules.isEmpty()) {
        	log.debug("セキュリティポリシー更新データ存在");
        } else if(!addresses.isEmpty()) {
        	log.debug("アドレス更新データ存在");
        } else if(!addressGroups.isEmpty()) {
        	log.debug("アドレスグループ更新データ存在");
        } else if(!applicationGroups.isEmpty()) {
        	log.debug("アプリケーショングループ更新データ存在");
        } else if(!applicationFilters.isEmpty()) {
        	log.debug("アプリケーションフィルタ更新データ存在");
        } else if(!services.isEmpty()) {
        	log.debug("サービス更新データ存在");
        } else if(!serviceGroups.isEmpty()) {
        	log.debug("サービスグループ更新データ存在");
        } else if(!schedules.isEmpty()) {
        	log.debug("スケジュール更新データ存在");
        // 20110614 ktakenaka@PROSITE add start
        } else if(!urlFilters.isEmpty()) {
        	log.debug("Webフィルタ更新データ存在");
        // 20110614 ktakenaka@PROSITE add end
        } else {
        	// コミット対象なし
        	log.debug("コミット対象が存在しません。");

        	// 20110616 ktakenaka@PROSITE add start
        	// 企業管理者の場合
        	if(param.getUvo().getGrantFlag() == 3) {
            // 20110616 ktakenaka@PROSITE add end

    			messages.add("message", new BLogicMessage("DK070001"));
    			result.setErrors(messages);
    			result.setMessages(messages);
    			result.setResultString("noneFailure");
    			log.debug("CommitBLogic処理終了");
    			return result;

           	// 20110616 ktakenaka@PROSITE add start
        	}
        	// 20110616 ktakenaka@PROSITE add end
        }

        // ビジネスロジックの出力クラスに結果を設定する
        CommitOutput out = new CommitOutput();
        out.setRules(rules);
        out.setAddresses(addresses);
        out.setAddressGroups(addressGroups);
        out.setApplicationGroups(applicationGroups);
        out.setApplicationFilters(applicationFilters);
        out.setServices(services);
        out.setServiceGroups(serviceGroups);
        out.setSchedules(schedules);
        out.setUrlFilters(urlFilters);	// 20110614 ktakenaka@PROSITE add
        // 20120827 kkato@PROSITE add start
        out.setPolicyRules(policyRules);	// 20120827 kkato@PROSITE add
        if(policyRules.size() > 0){
            out.setDenyAlert("すべての通信を遮断する可能性のあるポリシーが含まれています。本当に");
        } else {
            out.setDenyAlert("");
        }
        // 20120827 kkato@PROSITE add end

        result.setResultObject(out);
        result.setResultString("success");
        log.debug("CommitBLogic処理終了");
    	return result;
    }

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
	/**
	 * メソッド名 : setModName
	 * 機能概要 : 変更フラグより変更名称を設定する。
	 * @param objList 更新オブジェクトリスト
	 */
	private void setModName(List<SelectCommitList> objList) {
		log.debug("setModName処理開始：" + objList.size());

        // リスト分繰り返す
        for(int i=0; i<objList.size(); i++) {
        	log.debug("objList：" + i);
        	if(objList.get(i).getModFlg() == CusconConst.ADD_NUM) {
        		log.debug("objList.get(i).getModFlg()" + objList.get(i).getModFlg() + ",1");
        		objList.get(i).setModName(CusconConst.ADD);
        	} else if(objList.get(i).getModFlg() == CusconConst.MOD_NUM) {
        		log.debug("objList.get(i).getModFlg()" + objList.get(i).getModFlg() + ",1");
        		objList.get(i).setModName(CusconConst.MOD);
        	} else {
        		log.debug("objList.get(i).getModFlg()" + objList.get(i).getModFlg() + ",1");
        		objList.get(i).setModName(CusconConst.DEL);
        	}
        }
        log.debug("setModName処理終了");
	}
	// 20120827 kkato@PROSITE add start
	public List<String> getHiddenObject() {
		log.debug("getHiddenObject処理開始");
		// 隠しポリシー名を取得
        String nonSubject =
        	PropertyUtil.getProperty("fwsetting.common.nosubectpolicy");
        log.debug("隠しポリシー名:" + nonSubject);

        String[] splitNonSubject  = nonSubject.split(SPLIT);
        List<String> objectList = new ArrayList<String>();

        // 隠しポリシー名をリストに格納する。
        for(int i=0; i<splitNonSubject.length; i++) {
        	log.debug("隠しポリシー存在:splitNonSubject.length = " + splitNonSubject.length);
        	objectList.add(splitNonSubject[i]);
        }
        log.debug("getHiddenObject処理終了:objectList.size = " + objectList.size());
        return objectList;
	}
	// 20120827 kkato@PROSITE add end
}
