/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : RegistPolicyBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic;

import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.SecurityRules;
import jp.co.kddi.secgw.cuscon.fwsetting.common.PolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.common.PolicyRulesList;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.PolicyOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.RegistPolicyInput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : RegistPolicyBLogic
 * 機能概要 : Name項目登録画面にて「OK」押下時の処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class RegistPolicyBLogic implements BLogic<RegistPolicyInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;
	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic.RegistPolicyBLogic");
	private MessageAccessor messageAccessor = null;
	/**
	 * メソッド名 :execute
	 * 機能概要 :ポリシー一覧にデータの追加処理を行う。
	 * @param param 新規登録画面入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(RegistPolicyInput param) {

		log.debug("RegistPolicyBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");
        // エラーメッセージ
        BLogicMessages messages = new BLogicMessages();
        SecurityRules base = new SecurityRules();

        PolicyRulesList policyrule = new PolicyRulesList();

        // 検索条件を設定する。
    	base.setVsysId(param.getUvo().getVsysId());
    	base.setGenerationNo(CusconConst.ZERO_GENE);
    	base.setModName(param.getName());
    	// 選択された世代番号のポリシーリスト
        List<SelectPolicyList> policyList = null;
    	try {
	    	// ルール名の存在チェックを行う。
	    	if(!policyrule.checkPolicyName(updateDAO, queryDAO,
	    											param.getUvo(), base)) {
	    		// ルール名重複
	        	log.debug("ルール名が既に存在している。");
	        	messages.add("message", new BLogicMessage("DK050001"));
				result.setErrors(messages);
				result.setResultString("failure");
	        	return result;
	    	}
	    	 // ポリシーリストクラスを生成する。
	        PolicyList policy = new PolicyList();

	    	// DB更新処理を行う。
	    	updatePolicyInfo(param);

	        // 選択された世代番号のポリシーリストを取得する。
	        policyList = policy.getPolicylist(queryDAO, param.getUvo(),
	        					CusconConst.ZERO_GENE, CusconConst.POLICY);
        } catch(Exception e) {
			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"EK051001", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK050007"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

        // ビジネスロジックの出力クラスに結果を設定する
        PolicyOutput out = new PolicyOutput();
        out.setPolicyList(policyList);
        result.setResultObject(out);
        result.setResultString("success");

        log.debug("RegistPolicyBLogic処理終了");
		return result;
    }

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}

	/**
	 * メソッド名 : updatePolicyInfo
	 * 機能概要 : ポリシー情報の更新処理を行う。
	 * @param param 新規作成画面入力データクラス
	 * @throws Exception
	 */
	private void updatePolicyInfo(RegistPolicyInput param) throws Exception {

		log.debug("updatePolicyInfo処理開始");
		try {
			// 検索条件を設定する。
			SecurityRules base = new SecurityRules();
			base.setVsysId(param.getUvo().getVsysId());
			base.setGenerationNo(CusconConst.ZERO_GENE);
			base.setModFlg(CusconConst.DEL_NUM);

			// 全てのレコードの行番号に1を加える。
			updateDAO.execute("RegistPolicyBLogic-1", base);

			// 登録条件を設定する。
			base.setName(param.getName());
			base.setDescription(param.getDescription());
			base.setModFlg(CusconConst.ADD_NUM);

			// ポリシー情報を登録する。
			updateDAO.execute("RegistPolicyBLogic-2", base);
		} catch(Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		log.fatal(messageAccessor.getMessage("FK051002", objectgArray, param.getUvo()));
    		throw e;
    	}
		log.debug("updatePolicyInfo処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
