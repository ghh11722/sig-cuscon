/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : RollbackEndInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;

/**
 * クラス名 : RollbackEndInput
 * 機能概要 :ロールバック完了画面入力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/01
 *          新規作成
 * @see
 */
public class RollbackEndInput {

	// CusconUVO
	private CusconUVO uvo = null;

	// ポリシー一覧リスト
	private List<SelectPolicyList> policyList = null;

	// 選択世代番号
	private int selectGeneNo = 0;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}
	/**
	 * メソッド名 : policyListのGetterメソッド
	 * 機能概要 : policyListを取得する。
	 * @return policyList
	 */
	public List<SelectPolicyList> getPolicyList() {
		return policyList;
	}

	/**
	 * メソッド名 : policyListのSetterメソッド
	 * 機能概要 : policyListをセットする。
	 * @param policyList
	 */
	public void setPolicyList(List<SelectPolicyList> policyList) {
		this.policyList = policyList;
	}

	/**
	 * メソッド名 : selectGeneNoのGetterメソッド
	 * 機能概要 : selectGeneNoを取得する。
	 * @return selectGeneNo
	 */
	public int getSelectGeneNo() {
		return selectGeneNo;
	}

	/**
	 * メソッド名 : selectGeneNoのSetterメソッド
	 * 機能概要 : selectGeneNoをセットする。
	 * @param selectGeneNo
	 */
	public void setSelectGeneNo(int selectGeneNo) {
		this.selectGeneNo = selectGeneNo;
	}
}
