/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateServiceInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/27     morisou                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.dto;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;

/**
 * クラス名 : UpdateServiceInput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/27
 *          新規作成
 * @see
 */
public class UpdateServiceInput {

	// UVO
	private CusconUVO uvo = null;
	// 変更前サービス名
	private String oldServiceName = null;
	// 変更後サービス名
	private String newServiceName = null;
	// プロトコル
	private String protocol = null;
	// ポート
	private String port = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}
	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}
	/**
	 * メソッド名 : oldServiceNameのGetterメソッド
	 * 機能概要 : oldServiceNameを取得する。
	 * @return oldServiceName
	 */
	public String getOldServiceName() {
		return oldServiceName;
	}
	/**
	 * メソッド名 : oldServiceNameのSetterメソッド
	 * 機能概要 : oldServiceNameをセットする。
	 * @param oldServiceName
	 */
	public void setOldServiceName(String oldServiceName) {
		this.oldServiceName = oldServiceName;
	}
	/**
	 * メソッド名 : newServiceNameのGetterメソッド
	 * 機能概要 : newServiceNameを取得する。
	 * @return newServiceName
	 */
	public String getNewServiceName() {
		return newServiceName;
	}
	/**
	 * メソッド名 : newServiceNameのSetterメソッド
	 * 機能概要 : newServiceNameをセットする。
	 * @param newServiceName
	 */
	public void setNewServiceName(String newServiceName) {
		this.newServiceName = newServiceName;
	}
	/**
	 * メソッド名 : protocolのGetterメソッド
	 * 機能概要 : protocolを取得する。
	 * @return protocol
	 */
	public String getProtocol() {
		return protocol;
	}
	/**
	 * メソッド名 : protocolのSetterメソッド
	 * 機能概要 : protocolをセットする。
	 * @param protocol
	 */
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	/**
	 * メソッド名 : portのGetterメソッド
	 * 機能概要 : portを取得する。
	 * @return port
	 */
	public String getPort() {
		return port;
	}
	/**
	 * メソッド名 : portのSetterメソッド
	 * 機能概要 : portをセットする。
	 * @param port
	 */
	public void setPort(String port) {
		this.port = port;
	}
}
