/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateApplicationFilterInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/27     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.dto;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;

/**
 * クラス名 : UpdateApplicationFilterInput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/27
 *          新規作成
 * @see
 */
public class UpdateApplicationFilterInput {

	// UVO
	private CusconUVO uvo = null;
	// 変更前アプリケーションフィルタ名
	private String oldName = null;
	// 変更後アプリケーションフィルタ名
	private String newName = null;
	// カテゴリリスト
	private Object[] categoryList = null;
	// サブカテゴリリスト
	private Object[] subCategoryList = null;
	// テクノロジリスト
	private Object[] technologyList = null;
	// リスクリスト
	private Object[] riskList = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}
	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}
	/**
	 * メソッド名 : oldNameのGetterメソッド
	 * 機能概要 : oldNameを取得する。
	 * @return oldName
	 */
	public String getOldName() {
		return oldName;
	}
	/**
	 * メソッド名 : oldNameのSetterメソッド
	 * 機能概要 : oldNameをセットする。
	 * @param oldName
	 */
	public void setOldName(String oldName) {
		this.oldName = oldName;
	}
	/**
	 * メソッド名 : newNameのGetterメソッド
	 * 機能概要 : newNameを取得する。
	 * @return newName
	 */
	public String getNewName() {
		return newName;
	}
	/**
	 * メソッド名 : newNameのSetterメソッド
	 * 機能概要 : newNameをセットする。
	 * @param newName
	 */
	public void setNewName(String newName) {
		this.newName = newName;
	}
	/**
	 * メソッド名 : categoryListのGetterメソッド
	 * 機能概要 : categoryListを取得する。
	 * @return categoryList
	 */
	public Object[] getCategoryList() {
		return categoryList;
	}
	/**
	 * メソッド名 : categoryListのSetterメソッド
	 * 機能概要 : categoryListをセットする。
	 * @param categoryList
	 */
	public void setCategoryList(Object[] categoryList) {
		this.categoryList = categoryList;
	}
	/**
	 * メソッド名 : subCategoryListのGetterメソッド
	 * 機能概要 : subCategoryListを取得する。
	 * @return subCategoryList
	 */
	public Object[] getSubCategoryList() {
		return subCategoryList;
	}
	/**
	 * メソッド名 : subCategoryListのSetterメソッド
	 * 機能概要 : subCategoryListをセットする。
	 * @param subCategoryList
	 */
	public void setSubCategoryList(Object[] subCategoryList) {
		this.subCategoryList = subCategoryList;
	}
	/**
	 * メソッド名 : technologyListのGetterメソッド
	 * 機能概要 : technologyListを取得する。
	 * @return technologyList
	 */
	public Object[] getTechnologyList() {
		return technologyList;
	}
	/**
	 * メソッド名 : technologyListのSetterメソッド
	 * 機能概要 : technologyListをセットする。
	 * @param technologyList
	 */
	public void setTechnologyList(Object[] technologyList) {
		this.technologyList = technologyList;
	}
	/**
	 * メソッド名 : riskListのGetterメソッド
	 * 機能概要 : riskListを取得する。
	 * @return riskList
	 */
	public Object[] getRiskList() {
		return riskList;
	}
	/**
	 * メソッド名 : riskListのSetterメソッド
	 * 機能概要 : riskListをセットする。
	 * @param riskList
	 */
	public void setRiskList(Object[] riskList) {
		this.riskList = riskList;
	}
}
