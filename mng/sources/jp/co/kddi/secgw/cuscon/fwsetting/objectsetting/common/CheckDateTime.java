/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CheckDateTime.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/09     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;

import org.apache.log4j.Logger;

/**
 * クラス名 : CheckDateTime
 * 機能概要 : 入力設定時刻の大小チェックを行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/09
 *          新規作成
 * @see
 */
public class CheckDateTime {
	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.CheckDateTime");
	// メッセージクラス
    private static MessageAccessor messageAccessor = null;

    public CheckDateTime() {
    	log.debug("コンストラクタ2(CheckDateTime)処理終了");
    	log.debug("コンストラクタ2(CheckDateTime)処理終了");
    }


    /**
     * メソッド名 : checkTime
     * 機能概要 : 開始時刻と終了時刻の大小関係を判定する。
     * @param startTimeList 開始時刻リスト
     * @param endTimeList 終了時刻リスト
     * @return boolean 処理結果
     */
    public boolean checkTime(CusconUVO uvo, String[] startTimeList, String[] endTimeList) {

    	log.debug("checkTime処理開始");
    	String[] start = null;
    	String[] end = null;
    	String startStr = null;
    	String endStr = null;

    	// 時刻リスト分繰り返す。
		for(int i=0; i< startTimeList.length; i++) {
			log.debug("startTimeList = " + startTimeList[i] + ", endTimeList = " + endTimeList[i]);

			// 開始時刻を取得
			start = startTimeList[i].split(":");
			startStr = start[0] + start[1];
			// 終了時刻を取得
			end = endTimeList[i].split(":");
			endStr = end[0] + end[1];

			// 大小関係をチェックする。
			if(Integer.parseInt(endStr) < Integer.parseInt(startStr)) {
				log.debug("大小関係エラー:startStr=" + startStr + "," + "endStr = " + endStr);
				// 大小関係エラー
	    		Object[] objectgArray = {startStr + "," + endStr};
	    		log.error(messageAccessor.getMessage("EK061008", objectgArray, uvo));

	    		log.debug("checkTime処理終了:false");
				return false;

			}
		}
		log.debug("checkTime処理終了:true");
    	return true;
    }

    /**
     * メソッド名 : checkDate
     * 機能概要 : 開始日付と終了日付の大小関係を判定する。
     * @param startDateList 開始日付
     * @param endDateList 終了日付
     * @return boolean 処理結果
     */
    public boolean checkDate(CusconUVO uvo, String[] startDateList, String[] startTimeList,
    							String[] endDateList, String[] endTimeList) {

    	log.debug("checkDate処理開始");
    	String[] start = null;
    	String[] end = null;
    	String[] startTime = null;
    	String[] endTime = null;
    	String startStr = null;
    	String endStr = null;
    	String startTimeStr = null;
    	String endTimeStr = null;

    	// 日付リスト分繰り返す。
		for(int i=0; i< startDateList.length; i++) {
			log.debug("startDateList = " + startDateList[i] + ", endDateList = " + endDateList[i]);

			// 開始時刻を取得
			start = startDateList[i].split("/");
			startStr = start[0] + start[1] + start[2];
			// 終了時刻を取得
			end = endDateList[i].split("/");
			endStr = end[0] + end[1] + end[2];

			// 大小関係をチェックする。
			if(Integer.parseInt(endStr) < Integer.parseInt(startStr)) {
				log.debug("大小関係エラー:startStr=" + startStr + "," + "endStr = " + endStr);
				// 大小関係エラー
	    		Object[] objectgArray = {startStr + "," + endStr};
	    		log.error(messageAccessor.getMessage("EK061008", objectgArray, uvo));

	    		log.debug("checkTime処理終了:false");
				return false;

			}

			if(endStr.equals(startStr)) {
				log.debug("日時一致:" + startStr);

				// 開始時刻を取得
				startTime = startTimeList[i].split(":");
				startTimeStr = startTime[0] + startTime[1];
				// 終了時刻を取得
				endTime = endTimeList[i].split(":");
				endTimeStr = endTime[0] + endTime[1];
				// 大小関係をチェックする。
				if(Integer.parseInt(endTimeStr) < Integer.parseInt(startTimeStr)) {
					log.debug("大小関係エラー:startStr=" + startTimeStr +
											"," + "endStr = " + endTimeStr);
					// 大小関係エラー
		    		Object[] objectgArray = {startTimeStr + "," + endTimeStr};
		    		log.error(messageAccessor.getMessage("EK061008", objectgArray, uvo));

		    		log.debug("checkTime処理終了:false");
					return false;

				}
			}
		}
		log.debug("checkDate処理終了:true");
    	return true;
    }


    /**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor msgAcc){
		log.debug("setMessageAccessor処理開始");
	    setMsgAcc(msgAcc);
	    log.debug("setMessageAccessor処理終了");
	}

	/**
	 * メソッド名 : setMsgAcc
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public static void setMsgAcc(MessageAccessor messageAccessor) {
		log.debug("setMsgAcc処理開始");
		CheckDateTime.messageAccessor = messageAccessor;
		log.debug("setMsgAcc処理終了");
	}

	/**
	 * メソッド名 : getMessage
	 * 機能概要 : messageAccessorをゲットする。
	 * @return MessageAccessor
	 */
	public static MessageAccessor getMessage() {
		log.debug("getMessage処理");
		return messageAccessor;
	}
}
