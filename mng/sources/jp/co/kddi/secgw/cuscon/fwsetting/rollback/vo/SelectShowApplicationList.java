/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectShowApplicationList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.rollback.vo;

import java.io.Serializable;
import java.util.List;


/**
 * クラス名 : SelectShowApplicationList
 * 機能概要 : アプリケーション参照画面データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/01
 *          新規作成
 * @see
 */
public class SelectShowApplicationList implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = -380301320341285522L;

	// Vsys-ID
	private String vsysId = null;
	// 世代番号
	private int generationNo = 0;
	// オブジェクトリスト
	private List<String> objList = null;

	/**
	 * メソッド名 : objListのGetterメソッド
	 * 機能概要 : objListを取得する。
	 * @return objList
	 */
	public List<String> getObjList() {
		return objList;
	}

	/**
	 * メソッド名 : objListのSetterメソッド
	 * 機能概要 : objListをセットする。
	 * @param objList
	 */
	public void setObjList(List<String> objList) {
		this.objList = objList;
	}

	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得する。
	 * @return vsysId
	 */
	public String getVsysId() {
		return vsysId;
	}

	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdをセットする。
	 * @param vsysId
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}

	/**
	 * メソッド名 : generationNoのGetterメソッド
	 * 機能概要 : generationNoを取得する。
	 * @return generationNo
	 */
	public int getGenerationNo() {
		return generationNo;
	}

	/**
	 * メソッド名 : generationNoのSetterメソッド
	 * 機能概要 : generationNoをセットする。
	 * @param generationNo
	 */
	public void setGenerationNo(int generationNo) {
		this.generationNo = generationNo;
	}
}
