/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : RegistSchedulesInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.dto;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;

/**
 * クラス名 : RegistSchedulesInput
 * 機能概要 :スケジュール登録処理入力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class RegistSchedulesInput {

	// CusconUVO
	private CusconUVO uvo = null;

	// オブジェクト名
	private String name = null;

	// recurrence
	private int recurrence = 0;

	// Timesリスト
	private String[] startDateList = null;

	// Timesリスト
	private String[] endDateList = null;

	// Timesリスト
	private String[] startTimeList = null;

	// Timesリスト
	private String[] endTimeList = null;

	private String[] weekList = null;


	/**
	 * メソッド名 : startDateListのGetterメソッド
	 * 機能概要 : startDateListを取得する。
	 * @return startDateList
	 */
	public String[] getStartDateList() {
		return startDateList;
	}

	/**
	 * メソッド名 : startDateListのSetterメソッド
	 * 機能概要 : startDateListをセットする。
	 * @param startDateList
	 */
	public void setStartDateList(String[] startDateList) {
		this.startDateList = startDateList;
	}

	/**
	 * メソッド名 : endDateListのGetterメソッド
	 * 機能概要 : endDateListを取得する。
	 * @return endDateList
	 */
	public String[] getEndDateList() {
		return endDateList;
	}

	/**
	 * メソッド名 : endDateListのSetterメソッド
	 * 機能概要 : endDateListをセットする。
	 * @param endDateList
	 */
	public void setEndDateList(String[] endDateList) {
		this.endDateList = endDateList;
	}

	/**
	 * メソッド名 : weekListのGetterメソッド
	 * 機能概要 : weekListを取得する。
	 * @return weekList
	 */
	public String[] getWeekList() {
		return weekList;
	}

	/**
	 * メソッド名 : weekListのSetterメソッド
	 * 機能概要 : weekListをセットする。
	 * @param weekList
	 */
	public void setWeekList(String[] weekList) {
		this.weekList = weekList;
	}

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * メソッド名 : recurrenceのGetterメソッド
	 * 機能概要 : recurrenceを取得する。
	 * @return recurrence
	 */
	public int getRecurrence() {
		return recurrence;
	}

	/**
	 * メソッド名 : recurrenceのSetterメソッド
	 * 機能概要 : recurrenceをセットする。
	 * @param recurrence
	 */
	public void setRecurrence(int recurrence) {
		this.recurrence = recurrence;
	}

	/**
	 * メソッド名 : startTimeListのGetterメソッド
	 * 機能概要 : startTimeListを取得する。
	 * @return startTimeList
	 */
	public String[] getStartTimeList() {
		return startTimeList;
	}

	/**
	 * メソッド名 : startTimeListのSetterメソッド
	 * 機能概要 : startTimeListをセットする。
	 * @param startTimeList
	 */
	public void setStartTimeList(String[] startTimeList) {
		this.startTimeList = startTimeList;
	}

	/**
	 * メソッド名 : endTimeListのGetterメソッド
	 * 機能概要 : endTimeListを取得する。
	 * @return endTimeList
	 */
	public String[] getEndTimeList() {
		return endTimeList;
	}

	/**
	 * メソッド名 : endTimeListのSetterメソッド
	 * 機能概要 : endTimeListをセットする。
	 * @param endTimeList
	 */
	public void setEndTimeList(String[] endTimeList) {
		this.endTimeList = endTimeList;
	}
}
