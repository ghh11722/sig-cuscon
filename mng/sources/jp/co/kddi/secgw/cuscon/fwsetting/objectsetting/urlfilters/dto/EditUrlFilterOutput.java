/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditUrlFilterOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/08     ktakenaka@PROSITE                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.CommonSelectItem;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.UrlFilterCategoryInfo;

/**
 * クラス名 : EditUrlFilterOutput
 * 機能概要 :
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/08
 *          新規作成
 * @see
 */
public class EditUrlFilterOutput {

	// オブジェクト名
	private String name = null;
	// 説明
	private String comment = null;
	// ブロックリストサイト
	private String blockListSite = null;
	// ブロックサイト動作
	private String actionBlockListSite = null;
	// 許可リストサイト
	private String allowListSite = null;
	// Webフィルタカテゴリマスタリスト
	List<UrlFilterCategoryInfo> categoryList = null;
	// アクションセレクトボックスリスト
	List<CommonSelectItem> blockActionList = null;
	// カテゴリアクションセレクトボックスリスト
	List<CommonSelectItem> categoryActionList = null;

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * メソッド名 : commentのGetterメソッド
	 * 機能概要 : commentを取得する。
	 * @return comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * メソッド名 : commentのSetterメソッド
	 * 機能概要 : commentをセットする。
	 * @param comment
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * メソッド名 : blockListSiteのGetterメソッド
	 * 機能概要 : blockListSiteを取得する。
	 * @return blockListSite
	 */
	public String getBlockListSite() {
		return blockListSite;
	}
	/**
	 * メソッド名 : blockListSiteのSetterメソッド
	 * 機能概要 : blockListSiteをセットする。
	 * @param blockListSite
	 */
	public void setBlockListSite(String blockListSite) {
		this.blockListSite = blockListSite;
	}

	/**
	 * メソッド名 : actionBlockListSiteのGetterメソッド
	 * 機能概要 : actionBlockListSiteを取得する。
	 * @return actionBlockListSite
	 */
	public String getActionBlockListSite() {
		return actionBlockListSite;
	}
	/**
	 * メソッド名 : actionBlockListSiteのSetterメソッド
	 * 機能概要 : actionBlockListSiteをセットする。
	 * @param actionBlockListSite
	 */
	public void setActionBlockListSite(String actionBlockListSite) {
		this.actionBlockListSite = actionBlockListSite;
	}

	/**
	 * メソッド名 : allowListSiteのGetterメソッド
	 * 機能概要 : allowListSiteを取得する。
	 * @return allowListSite
	 */
	public String getAllowListSite() {
		return allowListSite;
	}
	/**
	 * メソッド名 : allowListSiteのSetterメソッド
	 * 機能概要 : allowListSiteをセットする。
	 * @param allowListSite
	 */
	public void setAllowListSite(String allowListSite) {
		this.allowListSite = allowListSite;
	}

	/**
	 * メソッド名 : categoryListのGetterメソッド
	 * 機能概要 : categoryListを取得する。
	 * @return categoryList
	 */
	public List<UrlFilterCategoryInfo> getCategoryList() {
		return categoryList;
	}
	/**
	 * メソッド名 : categoryListのSetterメソッド
	 * 機能概要 : categoryListをセットする。
	 * @param categoryList
	 */
	public void setCategoryList(List<UrlFilterCategoryInfo> categoryList) {
		this.categoryList = categoryList;
	}

	/**
	 * メソッド名 : blockActionListのGetterメソッド
	 * 機能概要 : blockActionListを取得する。
	 * @return blockActionList
	 */
	public List<CommonSelectItem> getBlockActionList() {
		return blockActionList;
	}
	/**
	 * メソッド名 : blockActionListのSetterメソッド
	 * 機能概要 : blockActionListをセットする。
	 * @param blockActionList
	 */
	public void setBlockActionList(List<CommonSelectItem> blockActionList) {
		this.blockActionList = blockActionList;
	}

	/**
	 * メソッド名 : categoryActionListのGetterメソッド
	 * 機能概要 : categoryActionListを取得する。
	 * @return categoryActionList
	 */
	public List<CommonSelectItem> getCategoryActionList() {
		return categoryActionList;
	}
	/**
	 * メソッド名 : categoryActionListのSetterメソッド
	 * 機能概要 : categoryActionListをセットする。
	 * @param categoryActionList
	 */
	public void setCategoryActionList(List<CommonSelectItem> categoryActionList) {
		this.categoryActionList = categoryActionList;
	}
}
