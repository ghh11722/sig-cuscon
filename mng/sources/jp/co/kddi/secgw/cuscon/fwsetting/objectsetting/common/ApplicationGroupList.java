/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ApplicationGroupList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/20     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.LinkInfo;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : ApplicationGroupList
 * 機能概要 : アプリケーショングループ一覧を表示するための情報を取得する。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/20
 *          新規作成
 * @see
 */
public class ApplicationGroupList {

	private String name;                                         // 切り替え判別用アプリケーショングループ名
	private int memberCount;                                     // メンバカウンタ
	private StringBuffer applications;                           // アプリケーション＆アプリケーショングループ連結文字列
	private ArrayList<SelectApplicationGroupList> resultList;    // 返却用リスト

	private static Logger log = Logger.getLogger(
	"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.ApplicationGroupList");

	// メッセージクラス
    private static MessageAccessor messageAccessor = null;

    /**
     * メソッド名 : コンストラクタ
     * 機能概要 : -
     * @param queryDAO
     * @param uvo
     */
    public ApplicationGroupList(MessageAccessor msgAcc) {
    	log.debug("コンストラクタ2処理開始");
    	messageAccessor = msgAcc;
    	log.debug("コンストラクタ2処理終了");
    }

	/**
	 * メソッド名 :getApplicationGroupList
	 * 機能概要 : アプリケーショングループ一覧情報取得処理
	 * @param vsysId
	 * @return resultList
	 */

	public List<SelectApplicationGroupList> getApplicationGroupList(
						QueryDAO queryDAO, CusconUVO uvo) throws Exception {
		log.debug("getApplicationGroupList処理開始");

		// InputBase設定
		InputBase inputBase = new InputBase();
		inputBase.setVsysId(uvo.getVsysId());                         // Vsys-ID
		inputBase.setGenerationNo(CusconConst.ZERO_GENE);    // 世代番号
		inputBase.setModFlg(CusconConst.DEL_NUM);            // 変更フラグ


		List<LinkInfo> linkList = null;
		try {
			// アプリケーショングループリンク情報取得（名称、オブジェクトタイプ、リンク先名称の昇順でリストに格納）
			linkList = queryDAO.executeForObjectList("ApplicationGroupsBLogic-1", inputBase);
		} catch(Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		log.fatal(messageAccessor.getMessage("FK061016", objectgArray, uvo));
    		throw e;

		}

		// 検索結果が0件
		if(linkList.size() == 0) {
			log.debug("検索結果0件");
			log.debug("getApplicationGroupList終了1:" + linkList.size());
			return null;
		}

		resultList = new ArrayList<SelectApplicationGroupList>();

		// アプリケーショングループリンク情報の件数分、以下の処理を行う
		name = null;                           // 前回LOOP名
		memberCount = 0;                       // メンバカウンタ
		applications = new StringBuffer();     // アプリケーション＆アプリケーショングループ連結用

		for (int i = 0; i < linkList.size(); i++) {
			log.debug("アプリケーショングループリンク情報処理中:" + i);
			// アプリケーショングループ名が切り替わった場合、以下の処理を行う
			if (i != 0 && !name.equals(linkList.get(i).getName())) {
				log.debug("アプリケーショングループ名切り変わり");
				// 返却用リスト設定処理
				setResultList ();
				// メンバカウンタリセット
				memberCount = 0;
				// アプリケーションリセット
				applications = new StringBuffer();
			}

			// メンバカウントアップ
			memberCount++;
			// メンバ数が1の場合
			if (memberCount == 1) {
				log.debug("文字連結1個目");
				// 文字連結（1個目限定）
				applications = new StringBuffer(String.valueOf(linkList.get(i).getLinkName()));
			// メンバ数が2以上の場合
			} else {
				// 文字連結（2個目以降）
				log.debug("文字連結2個目以降");
				applications.append(",").append(linkList.get(i).getLinkName());
			}
			// 今回LOOP名を前回LOOP名に格納
			name = linkList.get(i).getName();
		}

		// 返却用リスト設定処理
		setResultList();

		log.debug("getApplicationGroupList処理終了");
		return resultList;
	}

	/**
	 * メソッド名 :setResultList
	 * 機能概要 : 返却用リスト設定処理
	 * @param
	 */
	private void setResultList() {
		log.debug("setResultList処理開始");

		SelectApplicationGroupList selectApplicationGroupListOutput = new SelectApplicationGroupList();
		// Name
		selectApplicationGroupListOutput.setName(name);
		// Members
		selectApplicationGroupListOutput.setMembers(memberCount);
		// Applications
		selectApplicationGroupListOutput.setApplications(applications.toString());
		// resultListにセット
		resultList.add(selectApplicationGroupListOutput);

		log.debug("setResultList処理終了");
	}
}

