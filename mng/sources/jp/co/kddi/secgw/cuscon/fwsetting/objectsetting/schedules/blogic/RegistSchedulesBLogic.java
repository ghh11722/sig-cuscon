/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : RegistSchedulesBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.blogic;

import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.Schedules;
import jp.co.kddi.secgw.cuscon.fwsetting.common.ScheduleMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.CheckDateTime;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.ScheduleList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.dto.RegistSchedulesInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.dto.ScheduleOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : RegistSchedulesBLogic
 * 機能概要 :スケジュールオブジェクトの登録処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class RegistSchedulesBLogic implements BLogic<RegistSchedulesInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;

	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.blogic.RegistSchedulesBLogic");
	private MessageAccessor messageAccessor = null;
	/**
	 * メソッド名 :execute
	 * 機能概要 : スケジュールオブジェクトの登録処理を行う。
	 * @param param RegistSchedulesInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(RegistSchedulesInput param) {

		log.debug("BLogicResult処理開始:" + param.getName());
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");
        // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		CheckDateTime checktime = new CheckDateTime();

		List<Schedules> sheduleList = null;

		int timeSize = param.getStartTimeList().length;
		String[] timeList = new String[timeSize];

		// デイリーの場合
		if(param.getRecurrence() == 0) {
			log.debug("デイリー");

			// 入力設定時間大小関係チェックを行う。
			if(!checktime.checkTime(param.getUvo(), param.getStartTimeList(),
												param.getEndTimeList())) {
				// 入力設定時間大小関係エラー
	        	log.debug("入力設定時間大小関係エラー");
	        	messages.add("message", new BLogicMessage("DK060057"));
				result.setErrors(messages);
				result.setResultString("failure");
	        	return result;
			}

			// 時刻リスト分繰り返す。
			for(int i=0; i< param.getStartTimeList().length; i++) {
				log.debug("startTimeList = " + param.getStartTimeList()[i] +
								", endTimeList = " + param.getEndTimeList()[i]);

				// 入力設定時間大小関係チェックを行う。
				if(!checktime.checkTime(param.getUvo(), param.getStartTimeList(),
													param.getEndTimeList())) {
					// 入力設定時間大小関係エラー
		        	log.debug("入力設定時間大小関係エラー");
		        	messages.add("message", new BLogicMessage("DK060057"));
					result.setErrors(messages);
					result.setResultString("failure");
		        	return result;
				}

				timeList[i] = param.getStartTimeList()[i] + "-" + param.getEndTimeList()[i];
			}
		// ウィークリーの場合
		} else if (param.getRecurrence() == 1) {
			log.debug("ウィークリー");
			// 入力設定時間大小関係チェックを行う。
			if(!checktime.checkTime(param.getUvo(), param.getStartTimeList(),
												param.getEndTimeList())) {
				// 入力設定時間大小関係エラー
	        	log.debug("入力設定時間大小関係エラー");
	        	messages.add("message", new BLogicMessage("DK060057"));
				result.setErrors(messages);
				result.setResultString("failure");
	        	return result;
			}

			// 時刻リスト分繰り返す。
			for(int i=0; i< param.getStartTimeList().length; i++) {
				log.debug("weekList = " + param.getWeekList()[i] +
						", startTimeList = " + param.getStartTimeList()[i] +
								", endTimeList = " + param.getEndTimeList()[i]);

				timeList[i] = param.getWeekList()[i] + "@" + param.getStartTimeList()[i] +
													"-" + param.getEndTimeList()[i];
			}

		// 指定日時の場合
		} else {
			log.debug("指定日時の場合:");

			if(!checktime.checkDate(param.getUvo(), param.getStartDateList(), param.getStartTimeList(),
					param.getEndDateList(),param.getEndTimeList())) {
				// 入力設定時間大小関係エラー
	        	log.debug("入力設定時間大小関係エラー");
	        	messages.add("message", new BLogicMessage("DK060057"));
				result.setErrors(messages);
				result.setResultString("failure");
	        	return result;
			}


			// 時刻リスト分繰り返す。
			for(int i=0; i< param.getStartTimeList().length; i++) {
				log.debug("startDateList = " + param.getStartDateList()[i] +
						", startTimeList = " + param.getStartTimeList()[i] +
						", endDateList = " + param.getEndDateList()[i] +
								", endTimeList = " + param.getEndTimeList()[i]);

				timeList[i] = param.getStartDateList()[i] + "@" + param.getStartTimeList()[i] +
								"-" + param.getEndDateList()[i] + "@" + param.getEndTimeList()[i];
			}

		}
        // スケジュール共通クラスを生成する。
        ScheduleList schedule = new ScheduleList();

        // スケジュールマスタクラスを生成する。
        ScheduleMasterList scheduleMaster = new ScheduleMasterList();

        // オブジェクト名が既に使用されていないかのチェックを行う。
        Schedules base = new Schedules();
    	base.setVsysId(param.getUvo().getVsysId());
    	base.setGenerationNo(CusconConst.ZERO_GENE);
    	base.setModName(param.getName());

    	try {
	    	// オブジェクト名の存在チェックを行う。
	    	if(!schedule.checkScheduleName(updateDAO, queryDAO, param.getUvo(), base)) {
	        	log.debug("既に該当オブジェクト名が存在する。");
	        	messages.add("message", new BLogicMessage("DK060001"));
				result.setErrors(messages);
				result.setResultString("failure");
	    		return result;
	    	}
    	} catch(Exception e) {
			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"EK061007", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK060048"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

    	// DB登録を行う。
    	base.setName(param.getName());
    	base.setRecurrence(param.getRecurrence());
    	base.setModFlg(CusconConst.ADD_NUM);

    	// 登録したオブジェクトの情報を取得する。
		List<Schedules> scheduleList;
    	try {
	    	// スケジュールオブジェクトの登録を行う。
	    	updateDAO.execute("CommonT_Schedules-6", base);

	    	// 登録したオブジェクトの情報を取得する。
			scheduleList =
				queryDAO.executeForObjectList("UpdateSchedulesBLogic-1", base);
    	} catch(Exception e) {
			// トランザクションロールバック
        	TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
        	Object[] objectgArray = {e.getMessage()};
        	log.fatal(messageAccessor.getMessage(
        			"FK061025", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK060048"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

		base.setSeqNo(scheduleList.get(0).getSeqNo());

		try {
			// recurrenceがDAILYの場合
			if(param.getRecurrence() == CusconConst.DAILY) {
				log.debug("DAILY登録");
				// DAILYの情報を登録する。
				schedule.registDaiySchedule(updateDAO, param.getUvo(), base, timeList);

			// recurrenceがWEEKLYの場合
			} else if(param.getRecurrence() == CusconConst.WEEKLY) {
				log.debug("WEEKLY登録");
				// WEEKLYの情報を登録する。
				schedule.registWeeklySchedule(updateDAO, param.getUvo(), base, timeList);

			// recurrenceがNONRECURRINGの場合
			} else {
				log.debug("NONRECURRING登録");
				// NONRECURRINGの情報を登録する。
				schedule.registNonSchedule(updateDAO, param.getUvo(), base, timeList);
			}

	    	sheduleList =
	    		scheduleMaster.getScheduleMasterList(
	    						queryDAO, param.getUvo(), 0,
	    											CusconConst.ZERO_GENE);

		} catch(Exception e) {
			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"EK061007", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK060048"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

		// アウトプットデータクラスに設定する。
		ScheduleOutput out = new ScheduleOutput();
	    out.setScheduleList(sheduleList);

	    result.setResultObject(out);
	    result.setResultString("success");

	    log.debug("BLogicResult処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}

}
