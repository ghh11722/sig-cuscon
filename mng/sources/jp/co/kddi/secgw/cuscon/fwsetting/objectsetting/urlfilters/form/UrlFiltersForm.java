/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UrlFiltersForm.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/06     ktakenaka@PROSITE                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.form;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.CommonSelectItem;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectUrlFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.UrlFilterCategoryInfo;
import jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx;

import org.apache.struts.action.ActionMapping;

/**
 * クラス名 : UrlFiltersForm
 * 機能概要 :
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/06
 *          新規作成
 * @see
 */
public class UrlFiltersForm extends ValidatorActionFormEx{

	//
	private static final long serialVersionUID = 1L;

	// Webフィルタ情報リスト
	private List<SelectUrlFilterList> urlFilterList = null;
	// Webフィルタ名
	private String name = null;
	// Webフィルタ名(新)
	private String newUrlFilterName = null;
	// Webフィルタ名(旧)
	private String oldUrlFilterName = null;
	// 選択されたインデックス
	private String checkIndex = null;
	// インデックス
	private int index = 0;
	private String message = null;

	// アクションセレクトボックスリスト
	List<CommonSelectItem> blockActionList = null;
	// カテゴリアクションセレクトボックスリスト
	List<CommonSelectItem> categoryActionList = null;

	//----------------------------------------
	// 登録画面入力値
	//----------------------------------------
	// Webフィルタカテゴリリスト(画面表示用）
	List<UrlFilterCategoryInfo> categoryList = null;
	// 説明
	private String comment = null;
	// ブロックリストサイト
	private String blockListSite = null;
	// ブロックサイト動作
	private String actionBlockListSite = null;
	// 許可リストサイト
	private String allowListSite = null;
	// 選択Webフィルタカテゴリアクションリスト
	private String selectActionList = null;

	//----------------------------------------
	// 更新画面入力値
	//----------------------------------------
	// 変更後ブロックリストサイト
	private String newBlockListSite = null;
	// 変更後許可リストサイト
	private String newAllowListSite = null;

	/**
	 *
	 * メソッド名 :
	 * 機能概要 :
	 * @param mapping
	 * @param request
	 * @see jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx#reset(org.apache.struts.action.ActionMapping, javax.servlet.http.HttpServletRequest)
	 */
	public void reset(ActionMapping mapping, HttpServletRequest request){
		 message=null;
	}

	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * メソッド名 : urlFilterListのGetterメソッド
	 * 機能概要 : urlFilterListを取得する。
	 * @return urlFilterList
	 */
	public List<SelectUrlFilterList> getUrlFilterList() {
		return urlFilterList;
	}
	/**
	 * メソッド名 : urlFilterListのSetterメソッド
	 * 機能概要 : urlFilterListをセットする。
	 * @param urlFilterList
	 */
	public void setUrlFilterList(List<SelectUrlFilterList> urlFilterList) {
		this.urlFilterList = urlFilterList;
	}

	/**
	 * メソッド名 : oldUrlFilterNameのSetterメソッド
	 * 機能概要 : oldUrlFilterNameをセットする。
	 * @param oldUrlFilterName
	 */
	public void setOldUrlFilterName(String oldUrlFilterName) {
		this.oldUrlFilterName = oldUrlFilterName;
	}

	/**
	 * メソッド名 : oldUrlFilterNameのGetterメソッド
	 * 機能概要 : oldUrlFilterNameを取得する。
	 * @return oldUrlFilterName
	 */
	public String getOldUrlFilterName() {
		return oldUrlFilterName;
	}

	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * メソッド名 : newUrlFilterNameのSetterメソッド
	 * 機能概要 : newUrlFilterNameをセットする。
	 * @param newUrlFilterName
	 */
	public void setNewUrlFilterName(String newUrlFilterName) {
		this.newUrlFilterName = newUrlFilterName;
	}

	/**
	 * メソッド名 : newUrlFilterNameのGetterメソッド
	 * 機能概要 : newUrlFilterNameを取得する。
	 * @return newUrlFilterName
	 */
	public String getNewUrlFilterName() {
		return newUrlFilterName;
	}

	/**
	 * メソッド名 : 選択されたindexのGetterメソッド
	 * 機能概要 : 選択されたindexを取得する。
	 * @return checkIndex
	 */
	public String getCheckIndex() {
		return checkIndex;
	}
	/**
	 * メソッド名 : 選択されたindexのSetterメソッド
	 * 機能概要 : 選択されたindexをセットする。
	 * @param checkIndex
	 */
	public void setCheckIndex(String checkIndex) {
		this.checkIndex = checkIndex;
	}

	/**
	 * メソッド名 : indexのGetterメソッド
	 * 機能概要 : indexを取得する。
	 * @return index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * メソッド名 : indexのSetterメソッド
	 * 機能概要 : indexをセットする。
	 * @param index
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * メソッド名 : categoryListのGetterメソッド
	 * 機能概要 : categoryListを取得する。
	 * @return categoryList
	 */
	public List<UrlFilterCategoryInfo> getCategoryList() {
		return categoryList;
	}

	/**
	 * メソッド名 : categoryListのSetterメソッド
	 * 機能概要 : categoryListをセットする。
	 * @param categoryList
	 */
	public void setCategoryList(List<UrlFilterCategoryInfo> categoryList) {
		this.categoryList = categoryList;
	}

	/**
	 * メソッド名 : commentのGetterメソッド
	 * 機能概要 : commentを取得する。
	 * @return comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * メソッド名 : commentのSetterメソッド
	 * 機能概要 : commentをセットする。
	 * @param comment
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * メソッド名 : blockListSiteのGetterメソッド
	 * 機能概要 : blockListSiteを取得する。
	 * @return blockListSite
	 */
	public String getBlockListSite() {
		return blockListSite;
	}
	/**
	 * メソッド名 : blockListSiteのSetterメソッド
	 * 機能概要 : blockListSiteをセットする。
	 * @param blockListSite
	 */
	public void setBlockListSite(String blockListSite) {
		this.blockListSite = blockListSite;
	}
	/**
	 * メソッド名 : actionBlockListSiteのGetterメソッド
	 * 機能概要 : actionBlockListSiteを取得する。
	 * @return actionBlockListSite
	 */
	public String getActionBlockListSite() {
		return actionBlockListSite;
	}
	/**
	 * メソッド名 : actionBlockListSiteのSetterメソッド
	 * 機能概要 : actionBlockListSiteをセットする。
	 * @param actionBlockListSite
	 */
	public void setActionBlockListSite(String actionBlockListSite) {
		this.actionBlockListSite = actionBlockListSite;
	}
	/**
	 * メソッド名 : allowListSiteのGetterメソッド
	 * 機能概要 : allowListSiteを取得する。
	 * @return allowListSite
	 */
	public String getAllowListSite() {
		return allowListSite;
	}
	/**
	 * メソッド名 : allowListSiteのSetterメソッド
	 * 機能概要 : allowListSiteをセットする。
	 * @param allowListSite
	 */
	public void setAllowListSite(String allowListSite) {
		this.allowListSite = allowListSite;
	}

	/**
	 * メソッド名 : blockActionListのGetterメソッド
	 * 機能概要 : blockActionListを取得する。
	 * @return blockActionList
	 */
	public List<CommonSelectItem> getBlockActionList() {
		return blockActionList;
	}
	/**
	 * メソッド名 : blockActionListのSetterメソッド
	 * 機能概要 : blockActionListをセットする。
	 * @param blockActionList
	 */
	public void setBlockActionList(List<CommonSelectItem> blockActionList) {
		this.blockActionList = blockActionList;
	}

	/**
	 * メソッド名 : categoryActionListのGetterメソッド
	 * 機能概要 : categoryActionListを取得する。
	 * @return categoryActionList
	 */
	public List<CommonSelectItem> getCategoryActionList() {
		return categoryActionList;
	}
	/**
	 * メソッド名 : categoryActionListのSetterメソッド
	 * 機能概要 : categoryActionListをセットする。
	 * @param categoryActionList
	 */
	public void setCategoryActionList(List<CommonSelectItem> categoryActionList) {
		this.categoryActionList = categoryActionList;
	}

	/**
	 * メソッド名 : newBlockListSiteのGetterメソッド
	 * 機能概要 : newBlockListSiteを取得する。
	 * @return newBlockListSite
	 */
	public String getNewBlockListSite() {
		return newBlockListSite;
	}
	/**
	 * メソッド名 : newBlockListSiteのSetterメソッド
	 * 機能概要 : newBlockListSiteをセットする。
	 * @param newBlockListSite
	 */
	public void setNewBlockListSite(String newBlockListSite) {
		this.newBlockListSite = newBlockListSite;
	}
	/**
	 * メソッド名 : newAllowListSiteのGetterメソッド
	 * 機能概要 : newAllowListSiteを取得する。
	 * @return newAllowListSite
	 */
	public String getNewAllowListSite() {
		return newAllowListSite;
	}
	/**
	 * メソッド名 : newAllowListSiteのSetterメソッド
	 * 機能概要 : newAllowListSiteをセットする。
	 * @param newAllowListSite
	 */
	public void setNewAllowListSite(String newAllowListSite) {
		this.newAllowListSite = newAllowListSite;
	}
	/**
	 * メソッド名 : selectActionListのGetterメソッド
	 * 機能概要 : selectActionListを取得する。
	 * @return selectActionList
	 */
	public String getSelectActionList() {
		return selectActionList;
	}
	/**
	 * メソッド名 : selectActionListのSetterメソッド
	 * 機能概要 : selectActionListをセットする。
	 * @param selectActionList
	 */
	public void setSelectActionList(String selectActionList) {
		this.selectActionList = selectActionList;
	}
}
