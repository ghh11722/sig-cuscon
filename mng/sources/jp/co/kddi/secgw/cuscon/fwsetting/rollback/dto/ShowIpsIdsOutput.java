/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ShowIpsIdsOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/04     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto;

/**
 * クラス名 : ShowIpsIdsOutput
 * 機能概要 :IDS/IPS項目選択時の出力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/04
 *          新規作成
 * @see
 */
public class ShowIpsIdsOutput {

	// 脆弱性の保護フラグ
	private String ids_ips = null;

	/**
	 * メソッド名 : ids_ipsのGetterメソッド
	 * 機能概要 : ids_ipsを取得する。
	 * @return ids_ips
	 */
	public String getIds_ips() {
		return ids_ips;
	}
	/**
	 * メソッド名 : ids_ipsのSetterメソッド
	 * 機能概要 : ids_ipsをセットする。
	 * @param idsIps
	 */
	public void setIds_ips(String idsIps) {
		ids_ips = idsIps;
	}
}
