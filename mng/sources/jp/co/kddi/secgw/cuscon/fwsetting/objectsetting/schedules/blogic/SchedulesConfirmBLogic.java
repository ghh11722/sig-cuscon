/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SchedulesConfirmBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.blogic;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.vo.Schedules;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.dto.ScheduleOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.dto.SchedulesConfirmInput;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicResult;

/**
 * クラス名 : SchedulesConfirmBLogic
 * 機能概要 :スケジュール削除確認対象表示処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class SchedulesConfirmBLogic implements BLogic<SchedulesConfirmInput> {

	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.blogic.SchedulesConfirmBLogic");
	/**
	 * メソッド名 :execute
	 * 機能概要 :スケジュール削除確認対象表示処理を行う。
	 * @param param SchedulesConfirmInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(SchedulesConfirmInput param) {

		log.debug("SchedulesConfirmBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");

		// アウトプットデータクラスに設定する。
        ScheduleOutput out = new ScheduleOutput();
        List<Schedules> scheduleList = new ArrayList<Schedules>();

        String[] indexList = param.getIndex().split(",");

        // 選択チェック分繰り返す。
        for(int i=0; i< indexList.length; i++) {
        	log.debug("選択チェック分繰り返す。" + i);
        	scheduleList.add(param.getScheduleList().get(Integer.parseInt(indexList[i])));
        }

	    out.setScheduleList(scheduleList);

	    result.setResultObject(out);
	    result.setResultString("success");

	    log.debug("SchedulesConfirmBLogic処理終了");
		return result;
	}

}
