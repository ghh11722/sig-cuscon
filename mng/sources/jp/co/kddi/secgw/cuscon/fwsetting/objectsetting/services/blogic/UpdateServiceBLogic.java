/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateServiceBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/27     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.blogic;

import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.ServiceGroups;
import jp.co.kddi.secgw.cuscon.common.vo.Services;
import jp.co.kddi.secgw.cuscon.common.vo.ServicesMaster;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.ServiceList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.dto.ServicesOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.dto.UpdateServiceInput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

import org.apache.log4j.Logger;

/**
 * クラス名 : UpdateServiceBLogic
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/27
 *          新規作成
 * @see
 */
public class UpdateServiceBLogic implements BLogic<UpdateServiceInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.blogic.UpdateServiceBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 :BLogicResult
	 * 機能概要 :
	 * @param params
	 * @return
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(UpdateServiceInput params) {
		log.debug("UpdateServiceBLogic処理開始");
		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		 // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得
		CusconUVO uvo = params.getUvo();
		String vsysId = uvo.getVsysId();

		/***********************************************************************
		 *  ①複数テーブル間存在チェック
		 **********************************************************************/
		// T_ServiceGroupsに今回登録したいサービス名と同名のレコードがないか検索
		// 検索条件設定
		ServiceGroups serviceGroups = new ServiceGroups();
		serviceGroups.setName(params.getNewServiceName());
		serviceGroups.setVsysId(vsysId);
		serviceGroups.setGenerationNo(CusconConst.ZERO_GENE);
		serviceGroups.setModFlg(CusconConst.DEL_NUM);
		List<ServiceGroups> list;
		List<ServicesMaster> masterList;
		try{
			list = queryDAO.executeForObjectList(
					"ServiceGroupsBLogic-11", serviceGroups);

			// M_Servicesに今回登録したいサービス名と同名のレコードがないか検索
			masterList = queryDAO.executeForObjectList(
					"ServicesBLogic-12", serviceGroups);
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラーログ出力
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061020", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060038"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

		// 検索結果が0件でない場合
		if (list.size() > 0 || masterList.size() > 0) {
			// 重複エラーとしてレスポンスデータを作成し返却
			log.debug("複数テーブル間存在チェックエラー");
        	messages.add("message", new BLogicMessage("DK060069"));
			result.setErrors(messages);
			result.setResultString("failure");
    		return result;
		}


		/***********************************************************************
		 *  ②存在チェック
		 **********************************************************************/
		// 検索条件設定
		Services searchServiceKey = new Services();
		searchServiceKey.setVsysId(vsysId);                            // Vsys-ID
		searchServiceKey.setGenerationNo(CusconConst.ZERO_GENE);       // 世代番号
		searchServiceKey.setName(params.getNewServiceName());          // サービス名

		// 差分比較処理
		// 変更前と変更後のアドレス名が異なる場合
		if (!params.getNewServiceName().equals(params.getOldServiceName())){
			List<Services> serviceObjectList;
			try {
				// サービスオブジェクトテーブルに対し、変更後サービス名と同名のレコードが既に存在しているか検索
				serviceObjectList =
					queryDAO.executeForObjectList("ServicesBLogic-2", searchServiceKey);
			} catch(Exception e) {
				// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
				// DBアクセスエラーログ出力
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FK061020", objectgArray, params.getUvo()));
				messages.add("message", new BLogicMessage("DK060038"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}

			/*******************************************************************
			 *  ③存在チェックエラー処理
			 ******************************************************************/
			// DB検索結果が1件以上かつ、MOD_FLGのステータスが削除でない場合
			if (! serviceObjectList.isEmpty() && serviceObjectList.get(0).getModFlg() != CusconConst.DEL_NUM) {
				// 重複エラーとしてレスポンスデータを作成し返却
				log.debug("既に該当オブジェクト名が存在する。");
	        	messages.add("message", new BLogicMessage("DK060001"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}

			/*******************************************************************
			 *  ④不要レコード削除処理
			 ******************************************************************/
			// DB検索結果が1件以上かつ、MOD_FLGのステータスが削除の場合
			if (!serviceObjectList.isEmpty() &&
					serviceObjectList.get(0).getModFlg() ==
						CusconConst.DEL_NUM) {
				// SEQ_NOをキーに、UpdateDAOクラスを使用し、サービステーブルのレコードを削除
				try {
					updateDAO.execute("ServicesBLogic-5",
							serviceObjectList.get(0).getSeqNo());
				} catch(Exception e) {
					// トランザクションロールバック
		        	TransactionUtil.setRollbackOnly();
					// DBアクセスエラーログ出力
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage("FK061018", objectgArray, params.getUvo()));
					messages.add("message", new BLogicMessage("DK060038"));
					result.setErrors(messages);
					result.setResultString("failure");
					return result;
				}
			}
		}


		/***********************************************************************
		 *  ⑤更新処理
		 **********************************************************************/
		// 検索条件設定
		Services searchOldServiceKey = new Services();
		searchOldServiceKey.setVsysId(vsysId);                           // Vsys-ID
		searchOldServiceKey.setGenerationNo(CusconConst.ZERO_GENE);      // 世代番号
		searchOldServiceKey.setName(params.getOldServiceName());         // 変更前サービス名

		// 変更前サービス情報を取得
		List<Services> oldServiceList = queryDAO.executeForObjectList("ServicesBLogic-2", searchOldServiceKey);

		// 取得したレコードの変更フラグが追加の場合（更新フラグは書き換えずupdate）
		if (oldServiceList.get(0).getModFlg() == CusconConst.ADD_NUM) {
			// update用param作成
			Services updateParamSp = new Services();
			updateParamSp.setSeqNo(oldServiceList.get(0).getSeqNo());    // シーケンス番号
			updateParamSp.setName(params.getNewServiceName());           // サービス名
			updateParamSp.setPort(params.getPort());                     // ポート
			updateParamSp.setProtocol(params.getProtocol());             // プロトコル

			try {
				updateDAO.execute("ServicesBLogic-8", updateParamSp);
			} catch(Exception e) {
				// トランザクションロールバック
	        	TransactionUtil.setRollbackOnly();
				// DBアクセスエラーログ出力
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FK061019", objectgArray, params.getUvo()));
				messages.add("message", new BLogicMessage("DK060038"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}

		// 取得したレコードの変更フラグが追加でない場合（更新フラグも書き換えてupdate）
		} else {
			// update用param作成
			Services updateParam = new Services();
			updateParam.setSeqNo(oldServiceList.get(0).getSeqNo());      // シーケンス番号
			updateParam.setName(params.getNewServiceName());             // サービス名
			updateParam.setPort(params.getPort());                       // ポート
			updateParam.setProtocol(params.getProtocol());               // プロトコル
			updateParam.setModFlg(CusconConst.MOD_NUM);                  // 更新フラグ
			try {
				updateDAO.execute("ServicesBLogic-9", updateParam);
			} catch(Exception e) {
				// トランザクションロールバック
	        	TransactionUtil.setRollbackOnly();
				// DBアクセスエラーログ出力
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FK061019", objectgArray, params.getUvo()));
				messages.add("message", new BLogicMessage("DK060038"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}
		}


		/***********************************************************************
		 *  ⑥一覧情報検索共通処理
		 **********************************************************************/
		ServicesOutput output = new ServicesOutput();
		try {
			ServiceList serviceList = new ServiceList();
			output.setServiceList(serviceList.getServiceList(queryDAO, uvo));
		} catch(Exception e) {
			// トランザクションロールバック
        	TransactionUtil.setRollbackOnly();
			// 例外
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage(
						"EK061005", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060038"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		/***********************************************************************
		 *  ⑦レスポンス返却
		 **********************************************************************/
		result.setResultObject(output);
		result.setResultString("success");
		log.debug("UpdateServiceBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("queryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("queryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
