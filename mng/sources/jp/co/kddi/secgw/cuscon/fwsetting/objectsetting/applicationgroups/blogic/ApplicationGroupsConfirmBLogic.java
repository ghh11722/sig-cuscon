/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ApplicationGroupsConfirmBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/08     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.blogic;

import java.util.ArrayList;
import java.util.List;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto.ApplicationConfirmOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto.ApplicationGroupsConfirmInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationGroupList;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicResult;
import org.apache.log4j.Logger;

/**
 * クラス名 : ApplicationGroupsConfirmBLogic
 * 機能概要 : アプリケーショングループ削除確認対象表示処理を行う。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/08
 *          新規作成
 * @see
 */
public class ApplicationGroupsConfirmBLogic implements BLogic<ApplicationGroupsConfirmInput> {

	private Logger log = Logger.getLogger(
	"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.blogic." +
	"ApplicationFiltersConfirmBLogic");

	/**
	 * メソッド名 : execute
	 * 機能概要 : アプリケーショングループ削除確認対象表示処理を行う。
	 * @param param ApplicationGroupsConfirmInput入力データクラス
	 * @return result BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(ApplicationGroupsConfirmInput param) {
		log.debug("ApplicationGroupsConfirmBLogic処理開始");
		BLogicResult result = new BLogicResult();
		result.setResultString("failure");

		// アウトプットデータクラスに設定する。
		ApplicationConfirmOutput out = new ApplicationConfirmOutput();
		List<SelectApplicationGroupList> applicationGroupList = new ArrayList<
		SelectApplicationGroupList>();

		String[] indexList = param.getIndex().split(",");

		// 選択チェック分繰り返す。
		for(int i=0; i< indexList.length; i++) {
			log.debug("選択チェック数分処理中:" + i);
			applicationGroupList.add(param.getApplicationGroupList().get(Integer.
					parseInt(indexList[i])));
		}

		out.setApplicationGroupList(applicationGroupList);

		result.setResultObject(out);
		result.setResultString("success");

		log.debug("ApplicationGroupsConfirmBLogic処理終了");
		return result;
	}
}
