/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditZoneOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectObjectList;

/**
 * クラス名 : EditZoneOutput
 * 機能概要 :Zone項目選択時の出力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class EditZoneOutput {

	// ルール名
	private String name = null;

	// 出所ゾーンリスト
	private List<SelectObjectList> zoneList= null;

	// ゾーンマスタリスト
	private List<String> zoneMasterList = null;

	// ゾーン名
	private String zoneName = null;

//2011/10/31 add start inoue@prosite 相手方のZone値
	// ゾーン名
	private String otherZoneName = null;
	/**
	 * メソッド名 : otherZoneNameのGetterメソッド
	 * 機能概要 : otherZoneNameを取得する。
	 * @return otherZoneName
	 */
	public String getOtherZoneName() {
		return otherZoneName;
	}

	/**
	 * メソッド名 : otherZoneNameのSetterメソッド
	 * 機能概要 : otherZoneNameをセットする。
	 * @param otherZoneName
	 */
	public void setOtherZoneName(String otherZoneName) {
		this.otherZoneName = otherZoneName;
	}
//2011/10/31 add end inoue@prosite

	/**
	 * メソッド名 : zoneNameのGetterメソッド
	 * 機能概要 : zoneNameを取得する。
	 * @return zoneName
	 */
	public String getZoneName() {
		return zoneName;
	}

	/**
	 * メソッド名 : zoneNameのSetterメソッド
	 * 機能概要 : zoneNameをセットする。
	 * @param zoneName
	 */
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * メソッド名 : zoneListのGetterメソッド
	 * 機能概要 : zoneListを取得する。
	 * @return zoneList
	 */
	public List<SelectObjectList> getZoneList() {
		return zoneList;
	}

	/**
	 * メソッド名 : zoneListのSetterメソッド
	 * 機能概要 : zoneListをセットする。
	 * @param zoneList
	 */
	public void setZoneList(List<SelectObjectList> zoneList) {
		this.zoneList = zoneList;
	}

	/**
	 * メソッド名 : zoneMasterListのGetterメソッド
	 * 機能概要 : zoneMasterListを取得する。
	 * @return zoneMasterList
	 */
	public List<String> getZoneMasterList() {
		return zoneMasterList;
	}

	/**
	 * メソッド名 : zoneMasterListのSetterメソッド
	 * 機能概要 : zoneMasterListをセットする。
	 * @param zoneMasterList
	 */
	public void setZoneMasterList(List<String> zoneMasterList) {
		this.zoneMasterList = zoneMasterList;
	}


}
