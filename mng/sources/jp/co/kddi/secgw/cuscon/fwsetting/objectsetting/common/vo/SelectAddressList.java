/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddressesInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/17     morisou                 初版作成
 *******************************************************************************
 */

package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo;

import java.io.Serializable;

/**
 * クラス名 : SelectAddressListOutput
 * 機能概要 : アドレス一覧データクラス
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/17
 *          新規作成
 * @see
 */
public class SelectAddressList implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = -4044981911304703780L;
	// Name
	private String name = null;
	// Type
	private String type = null;
	// Address
	private String address = null;

	/**
	 * nameを取得します。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * nameを設定します。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * typeを取得します。
	 * @return type
	 */
	public String getType() {
		return type;
	}
	/**
	 * typeを設定します。
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * addressを取得します。
	 * @return address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * addressを設定します。
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}
}
