/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CommitEnd.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/19     komakiys         初版作成
 * 2010/06/18     oohashij         コミット処理を別プロセス呼出へ変更
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.commit.blogic;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.commit.dto.CommitEndInput;
import jp.co.kddi.secgw.cuscon.fwsetting.commit.dto.CommitEndOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.commit.exception.TimeoutException;
import jp.co.kddi.secgw.cuscon.fwsetting.common.CommitStatusManager;
import jp.co.kddi.secgw.cuscon.fwsetting.common.ProcessCaller;
import jp.co.kddi.secgw.cuscon.fwsetting.common.ResultJudgeData;
import jp.co.kddi.secgw.cuscon.fwsetting.common.UpdateCommitFlg;
import jp.co.kddi.secgw.cuscon.fwsetting.common.UpdatePA;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.CommitFlg;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.CommitStatus;

import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;
import jp.terasoluna.fw.util.PropertyUtil;

/**
 * クラス名 : CommitEndBLogic
 * 機能概要 : コミット処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/19
 *          新規作成
 * @version 1.1 oohashij
 *          Created 2010/06/18
 *          コミット処理を別プロセス呼出へ変更
 * @see
 */
public class CommitEndBLogic implements BLogic<CommitEndInput>{

	// QueryDAO
    private QueryDAO queryDAO = null;
    // UpdateDAO
    private UpdateDAO updateDAO = null;
    // 処理区分
    private static final int COMMIT = 0;
	// 日時フォーマット
	private static final String DATE_FORMAT = PropertyUtil.getProperty("fwsetting.common.dateformat");

    private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.commit.blogic.CommitEndBLogic");
    // メッセージクラス
    private MessageAccessor messageAccessor = null;

    /**
     * メソッド名 :execute
     * 機能概要 : コミット処理を行う
     * @param param CommitEndの入力クラス
     * @return BLogicResult BLogicResultクラス
     * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
     */
    public BLogicResult execute(CommitEndInput param) {
    	log.debug("CommitEndBLogic処理開始");

    	CusconUVO uvo = null;

    	uvo = param.getUvo();
    	BLogicResult result = new BLogicResult();
        result.setResultString("failure");
        // ビジネスロジックの出力クラスに結果を設定する
        CommitEndOutput out = new CommitEndOutput();
        // コミットフラグ
        CommitFlg commit_flg = new CommitFlg();
        commit_flg.setCommtFlg(false);

        try{
	        // コミットステータスの削除
	        CommitStatusManager commtStatManager = new CommitStatusManager();
        	commtStatManager.deleteCommitStatus(updateDAO, uvo);

	        UpdatePA update = new UpdatePA();

	        // 処理結果格納用
	        ResultJudgeData resultJudge = new ResultJudgeData();

			update.updatePAProcess(resultJudge, uvo.getPaLoginId(), uvo.getPaPasswd(), queryDAO, uvo, commit_flg);

			// メンテナンス中の場合
			if(resultJudge.getResultFlg() == 1) {
				// メンテナンス中メッセージ出力
				log.debug("メンテナンス中のため、コミットできませんでした。");

				out.setMessage(
					messageAccessor.getMessage(resultJudge.getResultData(), null));
				out.setAcceptStatus(CusconConst.ACCEPT_NG);
				result.setResultObject(out);
				result.setResultString("failure");
				return result;
			}

			// 他利用者コミット中の場合
			if(resultJudge.getResultFlg() == 2) {
				// 他利用者コミット中メッセージ出力
				log.debug("他利用者コミット中のため、コミットできませんでした。");
	        	// コミット中フラグOFF
	        	if (commit_flg.isCommtFlg()) {
	            	UpdateCommitFlg ucf = new UpdateCommitFlg();
	    			try {
	    				ucf.updatePAProcessFlg(CusconConst.COMMIT_OFF);
	    				commit_flg.setCommtFlg(false);
	   			} catch (Exception e1) {
	    				Object[] objectgArray = {e1.getMessage()};
	    				log.error(messageAccessor.getMessage("EK071003", objectgArray, uvo));
	    			}
	        	}
				out.setMessage(
					messageAccessor.getMessage(resultJudge.getResultData(), null));
				out.setAcceptStatus(CusconConst.ACCEPT_NG);
				result.setResultObject(out);
				result.setResultString("failure");
				return result;
			}

			// コミットステータスの登録
			String startTime = insertCommitStatus(uvo);
			// 別プロセスの呼出
			ProcessCaller processCaller = new ProcessCaller();
			processCaller.call(uvo, CusconConst.COMMIT, CusconConst.ZERO_GENE);

			// コミット成功のメッセージ格納
			out.setMessage(messageAccessor.getMessage("DK070022", null));
			out.setStartTime(startTime);
			out.setAcceptStatus(CusconConst.ACCEPT_OK);

        } catch(TimeoutException e) {
        	// コミット中フラグOFF
        	if (commit_flg.isCommtFlg()) {
            	UpdateCommitFlg ucf = new UpdateCommitFlg();
    			try {
    				ucf.updatePAProcessFlg(CusconConst.COMMIT_OFF);
    				commit_flg.setCommtFlg(false);
   			} catch (Exception e1) {
    				Object[] objectgArray = {e1.getMessage()};
    				log.error(messageAccessor.getMessage("EK071003", objectgArray, uvo));
    			}
        	}
        	// コマンドタイムアウトメッセージ出力
			log.debug("コマンドタイムアウト");
			Object[] objectgArray = {"コマンドタイムアウト"};
			out.setMessage(messageAccessor.getMessage("DK070003", objectgArray));
			out.setAcceptStatus(CusconConst.ACCEPT_NG);
			result.setResultObject(out);
			result.setResultString("failure");
			return result;

        } catch(Exception e) {
        	// コミット中フラグOFF
        	if (commit_flg.isCommtFlg()) {
            	UpdateCommitFlg ucf = new UpdateCommitFlg();
    			try {
    				ucf.updatePAProcessFlg(CusconConst.COMMIT_OFF);
    				commit_flg.setCommtFlg(false);
    			} catch (Exception e1) {
    				Object[] objectgArray = {e1.getMessage()};
    				log.error(messageAccessor.getMessage("EK071003", objectgArray, uvo));
    			}
        	}
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// コミット処理失敗ログ出力
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("EK071001", objectgArray, uvo));
			out.setMessage(messageAccessor.getMessage("DK070003", null));
			out.setAcceptStatus(CusconConst.ACCEPT_NG);
			result.setResultObject(out);
			result.setResultString("failure");
			return result;
		}

        result.setResultObject(out);
        result.setResultString("success");

        log.debug("CommitEndBLogic処理終了");
    	return result;
    }

	/**
	 * メソッド名 : コミットステータス登録
	 * 機能概要 : コミットステータステーブルの登録
	 * @param uvo
	 * @return StartTime
	 * @throws Exception
	 */
	private String insertCommitStatus(CusconUVO uvo) throws Exception {
		log.debug("insertCommitStatus処理開始");

		// コミットステータスの設定
		CommitStatus commitStatus = new CommitStatus();
		commitStatus.setVsysId(uvo.getVsysId());
		commitStatus.setProcessingKind(COMMIT);
		commitStatus.setStatus(CusconConst.STAT_COMMITTING);
		commitStatus.setMessage(messageAccessor.getMessage("DK070020", null));
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		commitStatus.setStartTime(dateFormat.format(date));
		commitStatus.setEndTime(null);

		CommitStatusManager commtStatManager = new CommitStatusManager();
		commtStatManager.insertCommitStatus(updateDAO, uvo, commitStatus);

		log.debug("insertCommitStatus処理終了 StartTime = " + commitStatus.getStartTime());
		return commitStatus.getStartTime();
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : updateDAOのSetterメソッド
	 * 機能概要 : updateDAOをセットする。
	 * @param updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
