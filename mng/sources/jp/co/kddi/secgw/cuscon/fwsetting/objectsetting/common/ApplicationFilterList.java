/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ApplicationFilterList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/20     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectObjectList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.FilterInfo;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : ApplicationFilterList
 * 機能概要 : アプリケーションフィルタ一覧を表示するための情報を取得する。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/20
 *          新規作成
 * @see
 */
public class ApplicationFilterList {

	// 返却用リスト
	private ArrayList<SelectApplicationFilterList> resultList = new ArrayList<SelectApplicationFilterList>();
	// カテゴリ格納用リスト
	private ArrayList<SelectObjectList> categoryList = null;
	// サブカテゴリ格納用リスト
	private ArrayList<SelectObjectList> subCategoryList = null;
	// テクノロジ格納用リスト
	private ArrayList<SelectObjectList> technologyList = null;
	// リスク格納用リスト
	private ArrayList<SelectObjectList> riskList = null;
	// 切り替え判別用フィルタ名
	private String name = null;
	// カテゴリ文字列連結用
	private StringBuffer categoryBuf = new StringBuffer();
	// サブカテゴリ文字列連結用
	private StringBuffer subCategoryBuf = new StringBuffer();
	// テクノロジー文字列連結用
	private StringBuffer technologyBuf = new StringBuffer();
	// リスク文字列連結用
	private StringBuffer riskBuf = new StringBuffer();

    private static Logger log = Logger.getLogger(
	"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.ApplicationFilterList");
	// メッセージクラス
	private static MessageAccessor messageAccessor = null;
	private int categoryCnt = 0;
	private int subCategoryCnt = 0;
	private int technologyCnt = 0;
	private int riskCnt = 0;

    /**
     * メソッド名 : コンストラクタ
     * 機能概要 : -
     * @param queryDAO
     * @param uvo
     */
    public ApplicationFilterList(MessageAccessor msgAcc) {
    	messageAccessor = msgAcc;
    }

	/**
	 * メソッド名 :getApplicationFilterList
	 * 機能概要 : アプリケーションフィルタ一覧情報取得処理
	 * @param vsysId
	 * @return resultList
	 */

	public List<SelectApplicationFilterList> getApplicationFilterList(
							QueryDAO queryDAO, CusconUVO uvo) throws Exception{

		// InputBase設定
		InputBase inputBase = new InputBase();
		inputBase.setVsysId(uvo.getVsysId());                         // Vsys-ID
		inputBase.setGenerationNo(CusconConst.ZERO_GENE);    // 世代番号
		inputBase.setModFlg(CusconConst.DEL_NUM);            // 変更フラグ

		List<FilterInfo> filterList;

		try {
			// アプリケーションフィルタ情報取得
			filterList = queryDAO.executeForObjectList("ApplicationFiltersBLogic-1", inputBase);
		} catch(Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		log.fatal(messageAccessor.getMessage("FK061012", objectgArray, uvo));
    		throw e;

		}

		// リスト初期化処理
		listReset();

		// 切り替え判別用フィルタ名
		name = null;
		// フィルタ情報の件数分、以下の処理を行う
		for (int i = 0; i < filterList.size(); i++) {

			// フィルタ名が切り替わった場合、以下の処理を行う
			if (i != 0 && !name.equals(filterList.get(i).getName())) {
				// 返却用リスト設定処理
				setResultList();
				// リスト初期化処理
				listReset();
			}
			// カテゴリがnullでないならリストにadd
			if (filterList.get(i).getCategory() != null) {
				categoryCnt++;

				// メンバ数が1の場合
				if (categoryCnt == 1) {
					log.debug("メンバ数が1");
					// 文字連結（1個目限定）
					categoryBuf.append(filterList.get(i).getCategory());
				// メンバ数が2以上の場合
				} else {
					log.debug("メンバ数が2以上");
					// 文字連結（2個目以降）
					categoryBuf.append(",").append(filterList.get(i).getCategory());
				}
			}
			// サブカテゴリがnullでないならリストにadd
			if (filterList.get(i).getSubCategory() != null) {
				subCategoryCnt++;

				// メンバ数が1の場合
				if (subCategoryCnt == 1) {
					log.debug("メンバ数が1");
					// 文字連結（1個目限定）
					subCategoryBuf.append(filterList.get(i).getSubCategory());
				// メンバ数が2以上の場合
				} else {
					log.debug("メンバ数が2以上");
					// 文字連結（2個目以降）
					subCategoryBuf.append(",").append(filterList.get(i).getSubCategory());
				}
			}
			// テクノロジがnullでないならリストにadd
			if (filterList.get(i).getTechnology() != null) {
				technologyCnt++;

				// メンバ数が1の場合
				if (technologyCnt == 1) {
					log.debug("メンバ数が1");
					// 文字連結（1個目限定）
					technologyBuf.append(filterList.get(i).getTechnology());
				// メンバ数が2以上の場合
				} else {
					log.debug("メンバ数が2以上");
					// 文字連結（2個目以降）
					technologyBuf.append(",").append(filterList.get(i).getTechnology());
				}
			}
			// リスクが0でないならリストにadd
			if (filterList.get(i).getRisk() != 0) {
				riskCnt++;

				// メンバ数が1の場合
				if (riskCnt == 1) {
					log.debug("メンバ数が1");
					// 文字連結（1個目限定）
					riskBuf.append(filterList.get(i).getRisk());
				// メンバ数が2以上の場合
				} else {
					log.debug("メンバ数が2以上");
					// 文字連結（2個目以降）
					riskBuf.append(",").append(filterList.get(i).getRisk());
				}
			}
			name = filterList.get(i).getName();
		}

		if(filterList.size() != 0) {
			setResultList();
		}

		// 返却用リスト設定処理
		return resultList;
	}

	/**
	 * メソッド名 :listReset
	 * 機能概要 : リスト初期化処理
	 * @param
	 * @return
	 */
	private void listReset() {
		categoryList = new ArrayList<SelectObjectList>();
		subCategoryList = new ArrayList<SelectObjectList>();
		technologyList = new ArrayList<SelectObjectList>();
		riskList = new ArrayList<SelectObjectList>();

		categoryBuf = new StringBuffer();
		subCategoryBuf = new StringBuffer();
		technologyBuf = new StringBuffer();
		riskBuf = new StringBuffer();

		categoryCnt = 0;
		subCategoryCnt = 0;
		technologyCnt = 0;
		riskCnt = 0;
	}

	/**
	 * メソッド名 :setResultList
	 * 機能概要 : 返却用リスト設定処理
	 * @param
	 * @return
	 * @return
	 */
	private void setResultList() {

		// オブジェクトリスト
		SelectObjectList categoryObj = new SelectObjectList();
		SelectObjectList subCategoryObj = new SelectObjectList();
		SelectObjectList technologyObj = new SelectObjectList();
		SelectObjectList riskObj = new SelectObjectList();

		categoryObj.setName(categoryBuf.toString());
		categoryList.add(categoryObj);
		subCategoryObj.setName(subCategoryBuf.toString());
		subCategoryList.add(subCategoryObj);
		technologyObj.setName(technologyBuf.toString());
		technologyList.add(technologyObj);
		riskObj.setName(riskBuf.toString());
		riskList.add(riskObj);

		SelectApplicationFilterList selectApplicationFilterListOutput = new SelectApplicationFilterList();
		selectApplicationFilterListOutput.setName(name);
		selectApplicationFilterListOutput.setCategoryList(categoryList);
		selectApplicationFilterListOutput.setSubCategoryList(subCategoryList);
		selectApplicationFilterListOutput.setTechnologyList(technologyList);
		selectApplicationFilterListOutput.setRiskList(riskList);
		resultList.add(selectApplicationFilterListOutput);
	}
}
