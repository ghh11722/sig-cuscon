/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ScheduleForm.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.form;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;

import jp.co.kddi.secgw.cuscon.common.vo.Schedules;
import jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx;

/**
 * クラス名 : ScheduleForm
 * 機能概要 :スケジュールオブジェクトフォーム
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class ScheduleForm extends ValidatorActionFormEx {

	// シリアルバージョンID
	private static final long serialVersionUID = 1L;

	// オブジェクト名
	private String name = null;

	// 選択スケジュールデータクラス
	private Schedules selectSchedule = null;

	// 選択周期
	private int selectRecurrence = 0;

	// 画面表示用データクラス
	private Schedules showSchedule= null;

	// 画面表示用周期
	private int showRecurrence = 0;

	// スケジュールリスト
	private List<Schedules> scheduleList = null;

	// 指定時間
	private int recurrence = 0;

	private String time = null;

	private List<String> timeList = null;

	// チェック選択スケジュールインデックス
	private String index = null;

	// 編集画面選択インデックス
	private int nameIndex = 0;

	// 変更後オブジェクト名
	private String showName =  null;

	// Timesリスト
	private String[] startTimeList = null;

	// Timesリスト
	private String[] endTimeList = null;

	private String[] weekList = null;

	// Timesリスト
	private String[] startDateList = null;

	// Timesリスト
	private String[] endDateList = null;

	private String message = null;

	public void reset(ActionMapping mapping, HttpServletRequest request){
		 message=null;
		 startTimeList=null;
		 endTimeList=null;
		 weekList=null;
		 startDateList=null;
		 endDateList=null;
	}

	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * メソッド名 : startDateListのGetterメソッド
	 * 機能概要 : startDateListを取得する。
	 * @return startDateList
	 */
	public String[] getStartDateList() {
		return startDateList;
	}

	/**
	 * メソッド名 : startDateListのSetterメソッド
	 * 機能概要 : startDateListをセットする。
	 * @param startDateList
	 */
	public void setStartDateList(String[] startDateList) {
		this.startDateList = startDateList;
	}

	/**
	 * メソッド名 : endDateListのGetterメソッド
	 * 機能概要 : endDateListを取得する。
	 * @return endDateList
	 */
	public String[] getEndDateList() {
		return endDateList;
	}

	/**
	 * メソッド名 : endDateListのSetterメソッド
	 * 機能概要 : endDateListをセットする。
	 * @param endDateList
	 */
	public void setEndDateList(String[] endDateList) {
		this.endDateList = endDateList;
	}

	/**
	 * メソッド名 : weekListのGetterメソッド
	 * 機能概要 : weekListを取得する。
	 * @return weekList
	 */
	public String[] getWeekList() {
		return weekList;
	}

	/**
	 * メソッド名 : weekListのSetterメソッド
	 * 機能概要 : weekListをセットする。
	 * @param weekList
	 */
	public void setWeekList(String[] weekList) {
		this.weekList = weekList;
	}

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * メソッド名 : selectScheduleのGetterメソッド
	 * 機能概要 : selectScheduleを取得する。
	 * @return selectSchedule
	 */
	public Schedules getSelectSchedule() {
		return selectSchedule;
	}

	/**
	 * メソッド名 : selectScheduleのSetterメソッド
	 * 機能概要 : selectScheduleをセットする。
	 * @param selectSchedule
	 */
	public void setSelectSchedule(Schedules selectSchedule) {
		this.selectSchedule = selectSchedule;
	}

	/**
	 * メソッド名 : selectRecurrenceのGetterメソッド
	 * 機能概要 : selectRecurrenceを取得する。
	 * @return selectRecurrence
	 */
	public int getSelectRecurrence() {
		return selectRecurrence;
	}

	/**
	 * メソッド名 : selectRecurrenceのSetterメソッド
	 * 機能概要 : selectRecurrenceをセットする。
	 * @param selectRecurrence
	 */
	public void setSelectRecurrence(int selectRecurrence) {
		this.selectRecurrence = selectRecurrence;
	}

	/**
	 * メソッド名 : showScheduleのGetterメソッド
	 * 機能概要 : showScheduleを取得する。
	 * @return showSchedule
	 */
	public Schedules getShowSchedule() {
		return showSchedule;
	}

	/**
	 * メソッド名 : showScheduleのSetterメソッド
	 * 機能概要 : showScheduleをセットする。
	 * @param showSchedule
	 */
	public void setShowSchedule(Schedules showSchedule) {
		this.showSchedule = showSchedule;
	}

	/**
	 * メソッド名 : showRecurrenceのGetterメソッド
	 * 機能概要 : showRecurrenceを取得する。
	 * @return showRecurrence
	 */
	public int getShowRecurrence() {
		return showRecurrence;
	}

	/**
	 * メソッド名 : showRecurrenceのSetterメソッド
	 * 機能概要 : showRecurrenceをセットする。
	 * @param showRecurrence
	 */
	public void setShowRecurrence(int showRecurrence) {
		this.showRecurrence = showRecurrence;
	}

	/**
	 * メソッド名 : scheduleListのGetterメソッド
	 * 機能概要 : scheduleListを取得する。
	 * @return scheduleList
	 */
	public List<Schedules> getScheduleList() {
		return scheduleList;
	}

	/**
	 * メソッド名 : scheduleListのSetterメソッド
	 * 機能概要 : scheduleListをセットする。
	 * @param scheduleList
	 */
	public void setScheduleList(List<Schedules> scheduleList) {
		this.scheduleList = scheduleList;
	}

	/**
	 * メソッド名 : recurrenceのGetterメソッド
	 * 機能概要 : recurrenceを取得する。
	 * @return recurrence
	 */
	public int getRecurrence() {
		return recurrence;
	}

	/**
	 * メソッド名 : recurrenceのSetterメソッド
	 * 機能概要 : recurrenceをセットする。
	 * @param recurrence
	 */
	public void setRecurrence(int recurrence) {
		this.recurrence = recurrence;
	}

	/**
	 * メソッド名 : timeのGetterメソッド
	 * 機能概要 : timeを取得する。
	 * @return time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * メソッド名 : timeのSetterメソッド
	 * 機能概要 : timeをセットする。
	 * @param time
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * メソッド名 : timeListのGetterメソッド
	 * 機能概要 : timeListを取得する。
	 * @return timeList
	 */
	public List<String> getTimeList() {
		return timeList;
	}

	/**
	 * メソッド名 : timeListのSetterメソッド
	 * 機能概要 : timeListをセットする。
	 * @param timeList
	 */
	public void setTimeList(List<String> timeList) {
		this.timeList = timeList;
	}

	/**
	 * メソッド名 : indexのGetterメソッド
	 * 機能概要 : indexを取得する。
	 * @return index
	 */
	public String getIndex() {
		return index;
	}

	/**
	 * メソッド名 : indexのSetterメソッド
	 * 機能概要 : indexをセットする。
	 * @param index
	 */
	public void setIndex(String index) {
		this.index = index;
	}

	/**
	 * メソッド名 : nameIndexのGetterメソッド
	 * 機能概要 : nameIndexを取得する。
	 * @return nameIndex
	 */
	public int getNameIndex() {
		return nameIndex;
	}

	/**
	 * メソッド名 : nameIndexのSetterメソッド
	 * 機能概要 : nameIndexをセットする。
	 * @param nameIndex
	 */
	public void setNameIndex(int nameIndex) {
		this.nameIndex = nameIndex;
	}

	/**
	 * メソッド名 : showNameのGetterメソッド
	 * 機能概要 : showNameを取得する。
	 * @return showName
	 */
	public String getShowName() {
		return showName;
	}

	/**
	 * メソッド名 : showNameのSetterメソッド
	 * 機能概要 : showNameをセットする。
	 * @param showName
	 */
	public void setShowName(String showName) {
		this.showName = showName;
	}

	/**
	 * メソッド名 : startTimeListのGetterメソッド
	 * 機能概要 : startTimeListを取得する。
	 * @return startTimeList
	 */
	public String[] getStartTimeList() {
		return startTimeList;
	}

	/**
	 * メソッド名 : startTimeListのSetterメソッド
	 * 機能概要 : startTimeListをセットする。
	 * @param startTimeList
	 */
	public void setStartTimeList(String[] startTimeList) {
		this.startTimeList = startTimeList;
	}

	/**
	 * メソッド名 : endTimeListのGetterメソッド
	 * 機能概要 : endTimeListを取得する。
	 * @return endTimeList
	 */
	public String[] getEndTimeList() {
		return endTimeList;
	}

	/**
	 * メソッド名 : endTimeListのSetterメソッド
	 * 機能概要 : endTimeListをセットする。
	 * @param endTimeList
	 */
	public void setEndTimeList(String[] endTimeList) {
		this.endTimeList = endTimeList;
	}

}
