/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ChoiceFiltersPolicyBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic;

import java.util.ArrayList;
import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.ApplicationFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.ChoiceFiltersPolicyInput;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.ChoiceFiltersPolicyOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import org.apache.log4j.Logger;

/**
 * クラス名 : ChoiceFiltersPolicyBLogic
 * 機能概要 : フィルター設定画面表示処理を行う。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/01
 *          新規作成
 * @see
 */
public class ChoiceFiltersPolicyBLogic implements BLogic<ChoiceFiltersPolicyInput> {

	// QueryDAO
    private QueryDAO queryDAO = null;

    private Logger log = Logger.getLogger(
    		"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups." +
    		"blogic.ChoiceFiltersPolicyBLogic");

    private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 : フィルター設定画面表示処理を行う。
	 * @param params ChoiceFiltersPolicyInput入力データクラス
	 * @return result BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(ChoiceFiltersPolicyInput params) {
		log.debug("ChoiceFiltersPolicyBLogic処理開始");
		List<SelectApplicationFilterList> list4Out = null;   // アプリケーションフィルタ情報のリスト

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();

		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = params.getUvo();

		/***********************************************************************
		 *  ①画面から取得した「選択中のアプリケーション」リストからフィルタ情報取得
		 **********************************************************************/
		// 「選択中のアプリケーション」リスト
		//List<SelectedAplInfo> selectedAplInfoList = params.getSelectedAplList();

		// 選択中アプリケーション名リスト
		String[] selectAplList = params.getSelectAplList();
		// 選択中アプリケーションタイプリスト
		String[] selectTypeList = params.getSelectTypeList();

		// 編集用tmpフィルタリスト
		ArrayList<String> tmpFltList = new ArrayList<String>();

		// 「選択中のアプリケーション」リストの件数分、以下の処理を実行
		if (!selectAplList[0].equals("null") || selectAplList.length != 1) {
			log.debug("「選択中のアプリケーション」がnullでない場合");

			for (int i = 0; i < selectAplList.length; i++) {
				log.debug("「選択中のアプリケーション」分、処理中:" + i);

				// タイプがフィルタの場合、それをリストに格納
				if (selectTypeList[i].equals(CusconConst.TYPE_FLT)) {

					log.debug("「選択中のアプリケーション」にフィルタが存在する場合リストに格納");
					tmpFltList.add(selectAplList[i]);
				}
			}
		}


		/***********************************************************************
		 *  ②DBからアプリケーションフィルタ情報を取得
		 **********************************************************************/
		ApplicationFilterList applicationFilterList =
										new ApplicationFilterList(messageAccessor);

		List<SelectApplicationFilterList> dbFltList = null;

		try {
		dbFltList = applicationFilterList.getApplicationFilterList(queryDAO, uvo);

		} catch (Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("EK051001", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK050033"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}


		/***********************************************************************
		 *  ③マッチング処理
		 **********************************************************************/
		list4Out = new ArrayList<SelectApplicationFilterList>();
		if (applicationFilterList != null) {
			log.debug("DBから取得したフィルタ情報がnullでない場合");

			// ①で得たリストと②で得たリストのフィルタ名が一致する場合、チェックON対象とみなす。
			for (int i = 0; i < dbFltList.size(); i++) {
				log.debug("DBから取得したフィルタ情報の件数分処理中:" + i);
				SelectApplicationFilterList selectApplicationFilterList =
					new SelectApplicationFilterList();

				if(tmpFltList.size() != 0) {
					log.debug("選択アプリあり");
					for (int j = 0; j < tmpFltList.size(); j++) {
						log.debug("「選択中のアプリケーション」に含まれていたフィルタ件数分処理中:" + j);
						// チェックON/OFF判別
						if (tmpFltList.get(j).equals(dbFltList.get(i).getName())) {
							log.debug("チェックONフィルタ名:" + tmpFltList.get(j));

							selectApplicationFilterList.setChkFlg(true);
						}
					}
				} else {
					log.debug("選択アプリ0件のためチェックボックス初期化");
					selectApplicationFilterList.setChkFlg(false);
				}
				selectApplicationFilterList.setName(dbFltList.get(i).getName());
				selectApplicationFilterList.setCategoryList(dbFltList.get(i).getCategoryList());
				selectApplicationFilterList.setSubCategoryList(dbFltList.get(i).getSubCategoryList());
				selectApplicationFilterList.setTechnologyList(dbFltList.get(i).getTechnologyList());
				selectApplicationFilterList.setRiskList(dbFltList.get(i).getRiskList());

				list4Out.add(selectApplicationFilterList);
			}
		}


		/***********************************************************************
		 *  ④セッション設定処理
		 **********************************************************************/
		// 選択中アプリケーション情報をセッション情報に格納
		List<SelectedAplInfo> sessionAplList = new ArrayList<SelectedAplInfo>();

		if (!selectAplList[0].equals("null") || selectAplList.length != 1) {
			log.debug("「選択中のアプリケーション」がnullでない場合");

			for (int i = 0; i < selectAplList.length; i++) {
				log.debug("「選択中のアプリケーション」分、セッション情報に格納中:" + i);

				SelectedAplInfo aplInfo = new SelectedAplInfo();

				aplInfo.setName(selectAplList[i]);
				aplInfo.setType(selectTypeList[i]);

				sessionAplList.add(aplInfo);
			}
		} else {
			log.debug("「選択中のアプリケーション」がnullの場合");
			sessionAplList = null;
		}


		/***********************************************************************
		 *  ⑤レスポンス返却
		 **********************************************************************/
		ChoiceFiltersPolicyOutput output = new ChoiceFiltersPolicyOutput();
		output.setFilterList4Add(list4Out);
		output.setSessionAplList(sessionAplList);
		output.setName(params.getName());
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("ChoiceFiltersPolicyBLogic処理終了");
		return result;
	}


	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
