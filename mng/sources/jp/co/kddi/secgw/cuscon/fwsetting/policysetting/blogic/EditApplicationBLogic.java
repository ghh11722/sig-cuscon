/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditApplicationBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.common.ApplicationList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.FilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationList4Search;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectFilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.common.AplFltGrpList;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.EditApplicationOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.PolicyInput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : EditApplicationBLogic
 * 機能概要 : Application項目選択時の処理を行う。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class EditApplicationBLogic implements BLogic<PolicyInput> {

	/**
	 * QueryDAO。
	 * Springによりインスタンス生成され設定される。
	 */
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger(
	"jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic.EditApplicationBLogic");

	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 : アプリケーション編集情報を取得する。
	 * @param params PolicyInput入力データクラス
	 * @return result BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(PolicyInput params) {

		log.debug("EditApplicationBLogic処理開始");

		SelectFilterMasterList selectFilterMasterList = null;               // フィルタエリア情報
		List<SelectApplicationList4Search> selectApplicationList = null;    // アプリケーション情報

		// キャンセルフラグ（1:キャンセルボタン押下）
		int cancelFlg;
		// 選択中アプリケーション名リスト（JSPのhiddenから取得用）
		String[] selectAplList = null;
		// 選択中アプリケーションタイプリスト（JSPのhiddenから取得用）
		String[] selectTypeList = null;

		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = params.getUvo();

		BLogicResult result = new BLogicResult();

		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		try {
			/*******************************************************************
			 *  ①フィルタエリア情報取得共通処理
			 *******************************************************************/
			FilterMasterList filterMasterList = new FilterMasterList();
			selectFilterMasterList = filterMasterList.getFilterMasterList(queryDAO, uvo);


			/*******************************************************************
			 *  ②アプリケーション情報取得共通処理
			 *******************************************************************/
			ApplicationList applicationList = new ApplicationList();

			// アプリケーション情報を全件取得
			selectApplicationList = applicationList.getApplicationList(queryDAO, uvo);

		}  catch (Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage(
					"EK051001", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK050020"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}


		/***********************************************************************
		 *  ③Application/Filter/Group取得共通処理
		 **********************************************************************/
		// キャンセルフラグ
		cancelFlg = params.getCancelFlg();

		// 選択したリストからオブジェクトを取得する
		SelectPolicyList selectPolicy =
			params.getPolicyList().get(params.getIndex());

		// ポリシー名
		String policyName = null;

		AplFltGrpList aplFltGrpList = new AplFltGrpList();

		List<SelectedAplInfo> selectedAplInfoList = null;

		List<SelectedAplInfo> clearSession = new ArrayList<SelectedAplInfo>();

		// キャンセルフラグが1以外の場合（一覧画面から直接遷移してきた場合）
		if (cancelFlg != 1) {
			// セッションクリア
			clearSession = null;
			selectAplList = null;
			selectTypeList = null;

			// ポリシー名取得
			policyName = selectPolicy.getName();

			log.debug("一覧画面から直接遷移してきた場合");
			try {
				// 編集中ポリシーに紐づくアプリケーション情報を取得
				selectedAplInfoList =
					aplFltGrpList.getAplFltGrpList(queryDAO, uvo, policyName);

			} catch	(Exception e) {
				// DBアクセスエラー
				Object[] objectgArray = {e.getMessage()};
				log.error(messageAccessor.getMessage(
						"EK051001", objectgArray, params.getUvo()));
				messages.add("message", new BLogicMessage("DK050020"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}
			// キャンセルフラグリセット
			cancelFlg = 0;

		// セッション情報を取得
		} else {
			// ポリシー名取得
			policyName = params.getName();

			log.debug("セッション情報を取得");
			selectedAplInfoList = params.getSessionAplList();
			// キャンセルフラグリセット
			cancelFlg = 0;
		}


		/***********************************************************************
		 *  ④結果セット
		 **********************************************************************/
		// ビジネスロジックの出力クラスに結果を設定する
		EditApplicationOutput out = new EditApplicationOutput();
		out.setSearch(null);
		out.setName(policyName);
		out.setFilterList(selectFilterMasterList);
		out.setApplicationList(selectApplicationList);
		out.setSelectedAplList(selectedAplInfoList);
		out.setCancelFlg(cancelFlg);
		out.setSessionAplList(clearSession);
		out.setSelectAplList(selectAplList);
		out.setSelectTypeList(selectTypeList);

		result.setResultObject(out);
		result.setResultString("success");

		log.debug("EditApplicationBLogic処理終了");
		return result;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
