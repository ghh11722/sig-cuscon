/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddServiceGroupBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/24     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.servicegroups.blogic;

import java.util.ArrayList;
import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.common.vo.ServiceGroups;
import jp.co.kddi.secgw.cuscon.common.vo.Services;
import jp.co.kddi.secgw.cuscon.common.vo.ServicesMaster;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.RegistLimitList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.dto.CommonInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.ServicesAndGroupsInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.servicegroups.dto.AddServiceGroupOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

import org.apache.log4j.Logger;

/**
 * クラス名 : AddServiceGroupBLogic
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/24
 *          新規作成
 * @see
 */
public class AddServiceGroupBLogic implements BLogic<CommonInput> {

	// オブジェクトタイプ定数
	private static final int SERVICE_MASTER = 0;    // サービスマスタ
	private static final int SERVICE_OBJECT = 1;    // サービスオブジェクト
	private static final int SERVICE_GROUP = 2;     // サービスグループオブジェクト

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.servicegroups.blogic.AddServiceGroupBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : BLogicResult
	 * 機能概要 :
	 * @param param
	 * @return result
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(CommonInput params) {
		log.debug("AddServiceGroupBLogic処理開始");
		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		 // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = params.getUvo();
		String vsysId = uvo.getVsysId();

		// InputBase設定
		InputBase inputBase = new InputBase();
		inputBase.setVsysId(vsysId);                            // Vsys-ID
		inputBase.setGenerationNo(CusconConst.ZERO_GENE);    // 世代番号
		inputBase.setModFlg(CusconConst.DEL_NUM);            // 変更フラグ

		// All Service & Groups 格納用リスト
		AddServiceGroupOutput output = new AddServiceGroupOutput();
		ArrayList<ServicesAndGroupsInfo> serviceAndServiceGrp4Add =
			new ArrayList<ServicesAndGroupsInfo>();
		// 登録上限値を取得する。
        List<RegistLimitList> limitList;
        int registNum;

		try {
	        // 登録上限値を取得する。
	        limitList = queryDAO.executeForObjectList("CommonM_SystemConstant-2", null);

	        inputBase.setVsysId(params.getUvo().getVsysId());
	        inputBase.setGenerationNo(CusconConst.ZERO_GENE);
	        inputBase.setModFlg(CusconConst.DEL_NUM);

	        // オブジェクトの登録数を取得する。
	        registNum = queryDAO.executeForObject("ServiceGroupsBLogic-10", inputBase, java.lang.Integer.class);
		} catch (Exception e) {
        	// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラーログ出力
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061024", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060040"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;

		}

        // 登録上限値とオブジェクト登録数の比較を行う。
        if(limitList.get(0).getServiceGrp() <= registNum) {
        	log.debug("登録上限値のため登録できませんでした。");
        	messages.add("message", new BLogicMessage("DK060003"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
        }

		// サービスマスタ情報取得
		List<ServicesMaster> serviceMasterList;
		try {
			// サービスマスタ情報取得
			serviceMasterList = queryDAO.executeForObjectList(
					"CommonM_Services-1", null);
		} catch(Exception e) {
        	// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラーログ出力
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061024", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060040"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;

		}
		// サービスマスタ情報を格納
		for (int i = 0; i < serviceMasterList.size(); i++) {
			log.debug("サービスマスタ情報を格納:" + i);
			ServicesAndGroupsInfo servicesAndGroups = new ServicesAndGroupsInfo();
			servicesAndGroups.setSeqNo(serviceMasterList.get(i).getSeqNo());      // SEQ_NO
			servicesAndGroups.setName(serviceMasterList.get(i).getName());        // オブジェクト名
			servicesAndGroups.setObjectType(SERVICE_MASTER);                      // オブジェクトタイプ
			servicesAndGroups.setChkFlg(false);                                   // チェック情報
			serviceAndServiceGrp4Add.add(servicesAndGroups);
		}

		// サービスオブジェクト情報取得
		List<Services> serviceObjectList;
		try {
			// サービスオブジェクト情報取得
			serviceObjectList = queryDAO.executeForObjectList(
					"ServiceGroupsBLogic-13", inputBase);
		} catch (Exception e) {
        	// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラーログ出力
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061024", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060040"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		// サービスオブジェクト情報を格納
		for (int i = 0; i < serviceObjectList.size(); i++) {
			log.debug("サービスオブジェクト情報を格納:" + i);
			ServicesAndGroupsInfo servicesAndGroups = new ServicesAndGroupsInfo();
			servicesAndGroups.setSeqNo(serviceObjectList.get(i).getSeqNo());      // SEQ_NO
			servicesAndGroups.setName(serviceObjectList.get(i).getName());        // オブジェクト名
			servicesAndGroups.setObjectType(SERVICE_OBJECT);                      // オブジェクトタイプ
			servicesAndGroups.setChkFlg(false);                                   // チェック情報
			serviceAndServiceGrp4Add.add(servicesAndGroups);
		}

		// サービスグループ情報取得
		List<ServiceGroups> serviceGroupList;
		try {
			// サービスグループ情報取得
			serviceGroupList = queryDAO.executeForObjectList(
					"ServiceGroupsBLogic-14", inputBase);
		} catch(Exception e) {
        	// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラーログ出力
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061024", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060040"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		// サービスグループ情報を格納
		for (int i = 0; i < serviceGroupList.size(); i++) {
			log.debug("サービスグループ情報を格納:" + i);
			ServicesAndGroupsInfo servicesAndGroups = new ServicesAndGroupsInfo();
			servicesAndGroups.setSeqNo(serviceGroupList.get(i).getSeqNo());       // SEQ_NO
			servicesAndGroups.setName(serviceGroupList.get(i).getName());         // オブジェクト名
			servicesAndGroups.setObjectType(SERVICE_GROUP);                       // オブジェクトタイプ
			servicesAndGroups.setChkFlg(false);                                   // チェック情報
			serviceAndServiceGrp4Add.add(servicesAndGroups);
		}
		output.setServiceAndServiceGrp4Add(serviceAndServiceGrp4Add);

		// レスポンス返却
		result.setResultObject(output);
		result.setResultString("success");
		log.debug("AddServiceGroupBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("queryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("queryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
