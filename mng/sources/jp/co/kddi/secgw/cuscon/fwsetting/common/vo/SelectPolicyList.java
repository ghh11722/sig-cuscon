/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectPolicyListOutput.java
 *
 * [変更履歴]
 * 日付           更新者               内容
 * 2010/02/19     komakiys             初版作成
 * 2011/05/18     ktakenaka@PROSITE    Webウィルスチェック利用フラグ,スパイウェアチェック利用フラグ追加
 * 2013/09/01     kkato@PROSITE        セキュリティルール有効/無効切替対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.common.vo;

import java.io.Serializable;
import java.util.List;

/**
 * クラス名 : SelectPolicyListOutput
 * 機能概要 : セキュリティポリシー更新情報一覧データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/19
 *          新規作成
 * @see
 */
public class SelectPolicyList implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 2644006662108252816L;


	// 行番号
	private int lineNo = 0;
	// ルール名
	private String name = null;
	// 備考
	private String description = null;
	// スケジュールリンク
	private String schedulesSeqNo = null;
	// 出所ゾーン名
	private String sourceZone = null;
	// 宛先ゾーン名
	private String destinationZone = null;
	// 出所アドレス名
	private String sourceAddress = null;
	// 出所アドレスタイプ
	private int sourceAddressType = 0;
	// 宛先アドレス名
	private String destinationAddress = null;
	// 宛先アドレスタイプ
	private int destinationAddressType = 0;
	// アプリケーション名
	private String application = null;
	// アプリケーションタイプ
	private int applicationType = 0;
	// サービス名
	private String service = null;
	// サービスタイプ
	private int serviceType = 0;
	// サービス適用フラグ
	private int serviceDefaultFlg = 0;
	// ルール適用フラグ
	private String action = null;
	// 脆弱性の保護フラグ
	private String ids_ips = null;
	// スケジュール名
	private String schedule = null;
	// 出所ゾーン名リスト
	private List<SelectObjectList> sourceZoneList = null;
	// 宛先ゾーン名リスト
	private List<SelectObjectList> destinationZoneList = null;
	// 出所アドレス名リスト
	private List<SelectObjectList> sourceAddressList = null;
	// 宛先アドレス名リスト
	private List<SelectObjectList> destinationAddressList = null;
	// アプリケーション名リスト
	private List<SelectObjectList> applicationList = null;
	// サービス名リスト
	private List<SelectObjectList> serviceList = null;

	// 20110518 ktakenaka@PROSITE add start
	// Webフィルタリスト
	private String urlFilterName = null;
	// Webフィルタ適用フラグ
	private int urlfilterDefaultFlg = 0;
	// Webウィルスチェック利用フラグ
	private String virusCheckFlg = null;
	// スパイウェアチェック利用フラグ
	private String spywareFlg = null;
	// 20110518 ktakenaka@PROSITE add end

	// 20130901 kkato@PROSITE add start
	// 有効/無効フラグ
	private String disableFlg = null;
	// 20130901 kkato@PROSITE add end

	/**
	 * メソッド名 : lineNoのGetterメソッド
	 * 機能概要 : lineNoを取得する。
	 * @return lineNo
	 */
	public int getLineNo() {
		return lineNo;
	}
	/**
	 * メソッド名 : lineNoのSetterメソッド
	 * 機能概要 : lineNoをセットする。
	 * @param lineNo
	 */
	public void setLineNo(int lineNo) {
		this.lineNo = lineNo;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : descriptionのGetterメソッド
	 * 機能概要 : descriptionを取得する。
	 * @return description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * メソッド名 : descriptionのSetterメソッド
	 * 機能概要 : descriptionをセットする。
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * メソッド名 : schedulesSeqNoのGetterメソッド
	 * 機能概要 : schedulesSeqNoを取得する。
	 * @return schedulesSeqNo
	 */
	public String getSchedulesSeqNo() {
		return schedulesSeqNo;
	}
	/**
	 * メソッド名 : schedulesSeqNoのSetterメソッド
	 * 機能概要 : schedulesSeqNoをセットする。
	 * @param schedulesSeqNo
	 */
	public void setSchedulesSeqNo(String schedulesSeqNo) {
		this.schedulesSeqNo = schedulesSeqNo;
	}
	/**
	 * メソッド名 : sourceZoneのGetterメソッド
	 * 機能概要 : sourceZoneを取得する。
	 * @return sourceZone
	 */
	public String getSourceZone() {
		return sourceZone;
	}
	/**
	 * メソッド名 : sourceZoneのSetterメソッド
	 * 機能概要 : sourceZoneをセットする。
	 * @param sourceZone
	 */
	public void setSourceZone(String sourceZone) {
		this.sourceZone = sourceZone;
	}
	/**
	 * メソッド名 : destinationZoneのGetterメソッド
	 * 機能概要 : destinationZoneを取得する。
	 * @return destinationZone
	 */
	public String getDestinationZone() {
		return destinationZone;
	}
	/**
	 * メソッド名 : destinationZoneのSetterメソッド
	 * 機能概要 : destinationZoneをセットする。
	 * @param destinationZone
	 */
	public void setDestinationZone(String destinationZone) {
		this.destinationZone = destinationZone;
	}
	/**
	 * メソッド名 : sourceAddressのGetterメソッド
	 * 機能概要 : sourceAddressを取得する。
	 * @return sourceAddress
	 */
	public String getSourceAddress() {
		return sourceAddress;
	}
	/**
	 * メソッド名 : sourceAddressのSetterメソッド
	 * 機能概要 : sourceAddressをセットする。
	 * @param sourceAddress
	 */
	public void setSourceAddress(String sourceAddress) {
		this.sourceAddress = sourceAddress;
	}
	/**
	 * メソッド名 : sourceAddressTypeのGetterメソッド
	 * 機能概要 : sourceAddressTypeを取得する。
	 * @return sourceAddressType
	 */
	public int getSourceAddressType() {
		return sourceAddressType;
	}
	/**
	 * メソッド名 : sourceAddressTypeのSetterメソッド
	 * 機能概要 : sourceAddressTypeをセットする。
	 * @param sourceAddressType
	 */
	public void setSourceAddressType(int sourceAddressType) {
		this.sourceAddressType = sourceAddressType;
	}
	/**
	 * メソッド名 : destinationAddressのGetterメソッド
	 * 機能概要 : destinationAddressを取得する。
	 * @return destinationAddress
	 */
	public String getDestinationAddress() {
		return destinationAddress;
	}
	/**
	 * メソッド名 : destinationAddressのSetterメソッド
	 * 機能概要 : destinationAddressをセットする。
	 * @param destinationAddress
	 */
	public void setDestinationAddress(String destinationAddress) {
		this.destinationAddress = destinationAddress;
	}
	/**
	 * メソッド名 : destinationAddressTypeのGetterメソッド
	 * 機能概要 : destinationAddressTypeを取得する。
	 * @return destinationAddressType
	 */
	public int getDestinationAddressType() {
		return destinationAddressType;
	}
	/**
	 * メソッド名 : destinationAddressTypeのSetterメソッド
	 * 機能概要 : destinationAddressTypeをセットする。
	 * @param destinationAddressType
	 */
	public void setDestinationAddressType(int destinationAddressType) {
		this.destinationAddressType = destinationAddressType;
	}
	/**
	 * メソッド名 : applicationのGetterメソッド
	 * 機能概要 : applicationを取得する。
	 * @return application
	 */
	public String getApplication() {
		return application;
	}
	/**
	 * メソッド名 : applicationのSetterメソッド
	 * 機能概要 : applicationをセットする。
	 * @param application
	 */
	public void setApplication(String application) {
		this.application = application;
	}
	/**
	 * メソッド名 : applicationTypeのGetterメソッド
	 * 機能概要 : applicationTypeを取得する。
	 * @return applicationType
	 */
	public int getApplicationType() {
		return applicationType;
	}
	/**
	 * メソッド名 : applicationTypeのSetterメソッド
	 * 機能概要 : applicationTypeをセットする。
	 * @param applicationType
	 */
	public void setApplicationType(int applicationType) {
		this.applicationType = applicationType;
	}
	/**
	 * メソッド名 : serviceのGetterメソッド
	 * 機能概要 : serviceを取得する。
	 * @return service
	 */
	public String getService() {
		return service;
	}
	/**
	 * メソッド名 : serviceのSetterメソッド
	 * 機能概要 : serviceをセットする。
	 * @param service
	 */
	public void setService(String service) {
		this.service = service;
	}
	/**
	 * メソッド名 : serviceTypeのGetterメソッド
	 * 機能概要 : serviceTypeを取得する。
	 * @return serviceType
	 */
	public int getServiceType() {
		return serviceType;
	}
	/**
	 * メソッド名 : serviceTypeのSetterメソッド
	 * 機能概要 : serviceTypeをセットする。
	 * @param serviceType
	 */
	public void setServiceType(int serviceType) {
		this.serviceType = serviceType;
	}
	/**
	 * メソッド名 : serviceDefaultFlgのGetterメソッド
	 * 機能概要 : serviceDefaultFlgを取得する。
	 * @return serviceDefaultFlg
	 */
	public int getServiceDefaultFlg() {
		return serviceDefaultFlg;
	}
	/**
	 * メソッド名 : serviceDefaultFlgのSetterメソッド
	 * 機能概要 : serviceDefaultFlgをセットする。
	 * @param serviceDefaultFlg
	 */
	public void setServiceDefaultFlg(int serviceDefaultFlg) {
		this.serviceDefaultFlg = serviceDefaultFlg;
	}
	/**
	 * メソッド名 : actionのGetterメソッド
	 * 機能概要 : actionを取得する。
	 * @return action
	 */
	public String getAction() {
		return action;
	}
	/**
	 * メソッド名 : actionのSetterメソッド
	 * 機能概要 : actionをセットする。
	 * @param action
	 */
	public void setAction(String action) {
		this.action = action;
	}
	/**
	 * メソッド名 : ids_ipsのGetterメソッド
	 * 機能概要 : ids_ipsを取得する。
	 * @return ids_ips
	 */
	public String getIds_ips() {
		return ids_ips;
	}
	/**
	 * メソッド名 : ids_ipsのSetterメソッド
	 * 機能概要 : ids_ipsをセットする。
	 * @param idsIps
	 */
	public void setIds_ips(String idsIps) {
		ids_ips = idsIps;
	}
	/**
	 * メソッド名 : scheduleのGetterメソッド
	 * 機能概要 : scheduleを取得する。
	 * @return schedule
	 */
	public String getSchedule() {
		return schedule;
	}
	/**
	 * メソッド名 : scheduleのSetterメソッド
	 * 機能概要 : scheduleをセットする。
	 * @param schedule
	 */
	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}
	/**
	 * メソッド名 : sourceZoneListのGetterメソッド
	 * 機能概要 : sourceZoneListを取得する。
	 * @return sourceZoneList
	 */
	public List<SelectObjectList> getSourceZoneList() {
		return sourceZoneList;
	}
	/**
	 * メソッド名 : sourceZoneListのSetterメソッド
	 * 機能概要 : sourceZoneListをセットする。
	 * @param sourceZoneList
	 */
	public void setSourceZoneList(List<SelectObjectList> sourceZoneList) {
		this.sourceZoneList = sourceZoneList;
	}
	/**
	 * メソッド名 : destinationZoneListのGetterメソッド
	 * 機能概要 : destinationZoneListを取得する。
	 * @return destinationZoneList
	 */
	public List<SelectObjectList> getDestinationZoneList() {
		return destinationZoneList;
	}
	/**
	 * メソッド名 : destinationZoneListのSetterメソッド
	 * 機能概要 : destinationZoneListをセットする。
	 * @param destinationZoneList
	 */
	public void setDestinationZoneList(List<SelectObjectList> destinationZoneList) {
		this.destinationZoneList = destinationZoneList;
	}
	/**
	 * メソッド名 : sourceAddressListのGetterメソッド
	 * 機能概要 : sourceAddressListを取得する。
	 * @return sourceAddressList
	 */
	public List<SelectObjectList> getSourceAddressList() {
		return sourceAddressList;
	}
	/**
	 * メソッド名 : sourceAddressListのSetterメソッド
	 * 機能概要 : sourceAddressListをセットする。
	 * @param sourceAddressList
	 */
	public void setSourceAddressList(List<SelectObjectList> sourceAddressList) {
		this.sourceAddressList = sourceAddressList;
	}
	/**
	 * メソッド名 : destinationAddressListのGetterメソッド
	 * 機能概要 : destinationAddressListを取得する。
	 * @return destinationAddressList
	 */
	public List<SelectObjectList> getDestinationAddressList() {
		return destinationAddressList;
	}
	/**
	 * メソッド名 : destinationAddressListのSetterメソッド
	 * 機能概要 : destinationAddressListをセットする。
	 * @param destinationAddressList
	 */
	public void setDestinationAddressList(List<SelectObjectList> destinationAddressList) {
		this.destinationAddressList = destinationAddressList;
	}
	/**
	 * メソッド名 : applicationListのGetterメソッド
	 * 機能概要 : applicationListを取得する。
	 * @return applicationList
	 */
	public List<SelectObjectList> getApplicationList() {
		return applicationList;
	}
	/**
	 * メソッド名 : applicationListのSetterメソッド
	 * 機能概要 : applicationListをセットする。
	 * @param applicationList
	 */
	public void setApplicationList(List<SelectObjectList> applicationList) {
		this.applicationList = applicationList;
	}
	/**
	 * メソッド名 : serviceListのGetterメソッド
	 * 機能概要 : serviceListを取得する。
	 * @return serviceList
	 */
	public List<SelectObjectList> getServiceList() {
		return serviceList;
	}
	/**
	 * メソッド名 : serviceListのSetterメソッド
	 * 機能概要 : serviceListをセットする。
	 * @param serviceList
	 */
	public void setServiceList(List<SelectObjectList> serviceList) {
		this.serviceList = serviceList;
	}

	// 20110518 ktakenaka@PROSITE add start
	/**
	 * メソッド名 : urlFilterNameのGetterメソッド
	 * 機能概要 : urlFilterNameを取得する。
	 * @return urlFilterName
	 */
	public String getUrlFilterName() {
		return urlFilterName;
	}

	/**
	 * メソッド名 : urlFilterNameのSetterメソッド
	 * 機能概要 : urlFilterNameをセットする。
	 * @param urlFilterName
	 */
	public void setUrlFilterName(String urlFilterName) {
		this.urlFilterName = urlFilterName;
	}
	/**
	 * メソッド名 : urlfilterDefaultFlgのGetterメソッド
	 * 機能概要 : urlfilterDefaultFlgを取得する。
	 * @return urlfilterDefaultFlg
	 */
	public int getUrlfilterDefaultFlg() {
		return urlfilterDefaultFlg;
	}
	/**
	 * メソッド名 : urlfilterDefaultFlgのSetterメソッド
	 * 機能概要 : urlfilterDefaultFlgをセットする。
	 * @param urlfilterDefaultFlg
	 */
	public void setUrlfilterDefaultFlg(int urlfilterDefaultFlg) {
		this.urlfilterDefaultFlg = urlfilterDefaultFlg;
	}
	/**
	 * メソッド名 : virusCheckFlgのGetterメソッド
	 * 機能概要 : virusCheckFlgを取得する。
	 * @return virusCheckFlg
	 */
	public String getVirusCheckFlg() {
		return virusCheckFlg;
	}
	/**
	 * メソッド名 : virusCheckFlgのSetterメソッド
	 * 機能概要 : virusCheckFlgをセットする。
	 * @param virusCheckFlg
	 */
	public void setVirusCheckFlg(String virusCheckFlg) {
		this.virusCheckFlg = virusCheckFlg;
	}

	/**
	 * メソッド名 : spywareFlgのGetterメソッド
	 * 機能概要 : spywareFlgを取得する。
	 * @return spywareFlg
	 */
	public String getSpywareFlg() {
		return spywareFlg;
	}
	/**
	 * メソッド名 : spywareFlgのSetterメソッド
	 * 機能概要 : spywareFlgをセットする。
	 * @param spywareFlg
	 */
	public void setSpywareFlg(String spywareFlg) {
		this.spywareFlg = spywareFlg;
	}
	// 20110518 ktakenaka@PROSITE add end

	// 20130901 kkato@PROSITE add start
	/**
	 * メソッド名 : disableFlgのGetterメソッド
	 * 機能概要 : disableFlgを取得する。
	 * @return disableFlg
	 */
	public String getDisableFlg() {
		return disableFlg;
	}
	/**
	 * メソッド名 : disableFlgのSetterメソッド
	 * 機能概要 : disableFlgをセットする。
	 * @param disableFlg
	 */
	public void setDisableFlg(String disableFlg) {
		this.disableFlg = disableFlg;
	}
	// 20130901 kkato@PROSITE add end
}
