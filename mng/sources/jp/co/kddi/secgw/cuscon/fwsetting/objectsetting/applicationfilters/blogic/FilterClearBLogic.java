/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : FilterClearBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.blogic;

import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.common.ApplicationList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.FilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.dto.FilterClearInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.dto.FilterClearOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationList4Search;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectFilterMasterList;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import org.apache.log4j.Logger;

/**
 * クラス名 : FilterClearBLogic
 * 機能概要 : アプリケーションフィルタ解除処理を行う。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class FilterClearBLogic implements BLogic<FilterClearInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.blogic.FilterClearBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 : アプリケーションフィルタ解除処理を行う。
	 * @param params ClearInput 入力データクラス
	 * @return result
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(FilterClearInput params) {

		log.debug("FilterClearBLogic処理開始");

		SelectFilterMasterList resultFltList;                // フィルタマスタ情報
		List<SelectApplicationList4Search> resultAplList;    // アプリケーションマスタ情報
		FilterClearOutput output;                            // 返却用リスト

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		 // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = params.getUvo();

		try {
			// フィルタリスト取得（マスタ情報）
			FilterMasterList filterMasterList = new FilterMasterList();
			resultFltList = filterMasterList.getFilterMasterList(queryDAO, uvo);

			// アプリケーションリスト取得（マスタ情報）
			ApplicationList applicationList = new ApplicationList();
			resultAplList = applicationList.getApplicationList(queryDAO, uvo);

		} catch(Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("FK061012", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060059"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}
		// 返却用リスト
		output = new FilterClearOutput();
		output.setFilterList(resultFltList);
		output.setApplicationList(resultAplList);

		// レスポンス返却
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("FilterClearBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}

}
