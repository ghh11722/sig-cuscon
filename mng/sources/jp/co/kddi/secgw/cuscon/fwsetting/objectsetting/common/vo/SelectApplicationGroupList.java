/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectApplicationGroupList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/20     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo;

import java.io.Serializable;

/**
 * クラス名 : SelectApplicationGroupList
 * 機能概要 : アプリケーショングループ一覧データクラス
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/20
 *          新規作成
 * @see
 */
public class SelectApplicationGroupList implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 3774530489650808052L;
	// Name
	private String name = null;
	// Members
	private int members = 0;
	// Applications/Filters/Groups
	private String applications = null;
	// チェック情報（画面にチェックボックスON/OFFがある場合に使用 ON:true/OFF:false）
	private boolean chkFlg = false;

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : membersのGetterメソッド
	 * 機能概要 : membersを取得する。
	 * @return members
	 */
	public int getMembers() {
		return members;
	}
	/**
	 * メソッド名 : membersのSetterメソッド
	 * 機能概要 : membersをセットする。
	 * @param members
	 */
	public void setMembers(int members) {
		this.members = members;
	}
	/**
	 * メソッド名 : applicationsのGetterメソッド
	 * 機能概要 : applicationsを取得する。
	 * @return applications
	 */
	public String getApplications() {
		return applications;
	}
	/**
	 * メソッド名 : applicationsのSetterメソッド
	 * 機能概要 : applicationsをセットする。
	 * @param applications
	 */
	public void setApplications(String applications) {
		this.applications = applications;
	}
	/**
	 * メソッド名 : chkFlgのGetterメソッド
	 * 機能概要 : chkFlgを取得する。
	 * @return chkFlg
	 */
	public boolean isChkFlg() {
		return chkFlg;
	}
	/**
	 * メソッド名 : chkFlgのSetterメソッド
	 * 機能概要 : chkFlgをセットする。
	 * @param chkFlg
	 */
	public void setChkFlg(boolean chkFlg) {
		this.chkFlg = chkFlg;
	}
	/**
	 * メソッド名 : serialversionuidのGetterメソッド
	 * 機能概要 : serialversionuidを取得する。
	 * @return serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
