/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateCommitFlg.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/04/02     hiroyasu                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.common;

import org.apache.log4j.Logger;
import java.sql.*;

import javax.naming.*;
import javax.sql.DataSource;

/**
 * クラス名 : UpdateCommitFlg
 * 機能概要 : commitフラグを判定、更新する。
 * 備考 :
 * @author hiroyasu
 * @version 1.0 hiroyasu
 *          Created 2010/04/02
 *          新規作成
 * @see
 */
public class UpdateCommitFlg {
	// データソース名
	private static final String DATA_SOURCE_NAME = "java:comp/env/TerasolunaDataSource";
	// SQL
	private static final String UPDATE_COMMITFLG_SQL = "SELECT T_CommitProcessing_update(?);";


	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 */
	public UpdateCommitFlg() {
		// 自動生成されたコンストラクター・スタブ
	}

	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.common.UpdateCommitFlg");

	/**
	 * メソッド名 : updatePAProcessFlg
	 * 機能概要 : commitフラグの設定可否を判定し、<br>
	 *            ON/OFFを設定する。
	 * @param commit_flg commit処理中フラグ
     * @return int 処理判定フラグ
	 * @throws Exception
	 */
	public int updatePAProcessFlg(int commit_flg) throws Exception {
		log.debug("updatePAProcessFlg処理開始");

		int myrec = 0;
		// DB接続
		Context context = new InitialContext();
		DataSource ds = (DataSource) context.lookup(DATA_SOURCE_NAME);
		Connection dbConnection = ds.getConnection();
		PreparedStatement stm;

		stm = dbConnection.prepareStatement(UPDATE_COMMITFLG_SQL);
		stm.setInt(1, commit_flg);

		// SQL実行
		stm.executeQuery();
		ResultSet rec = stm.getResultSet();
		rec.next();
		myrec = rec.getInt(1);
   		if (myrec == 1) {
   			log.debug("他でコミット中");
   		}
		stm.close();

		// DB切断
		dbConnection.close();

		log.debug("updatePAProcessFlg処理終了：myrec=" + myrec);
		return myrec;
	}

}
