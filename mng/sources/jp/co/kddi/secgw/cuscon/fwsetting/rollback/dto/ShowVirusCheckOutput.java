/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ShowVirusCheckOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/03     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto;

/**
 * クラス名 : ShowVirusCheckOutput
 * 機能概要 :Webウィルスチェック項目選択時の出力データクラス
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/03
 *          新規作成
 * @see
 */
public class ShowVirusCheckOutput {

	// Webウィルスチェックフラグ
	private String virusCheckFlg = null;

	/**
	 * メソッド名 : virusCheckFlgのGetterメソッド
	 * 機能概要 : virusCheckFlgを取得する。
	 * @return virusCheckFlg
	 */
	public String getVirusCheckFlg() {
		return this.virusCheckFlg;
	}
	/**
	 * メソッド名 : virusCheckFlgのSetterメソッド
	 * 機能概要 : virusCheckFlgをセットする。
	 * @param virusCheckFlg
	 */
	public void setVirusCheckFlg(String virusCheckFlg) {
		this.virusCheckFlg = virusCheckFlg;
	}
}
