/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddFiltersBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.blogic;

import java.util.ArrayList;
import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.common.ApplicationList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.FilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto.AddFiltersInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto.AddFiltersOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationList4Search;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectFilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import org.apache.log4j.Logger;

/**
 * クラス名 : AddFiltersBLogic
 * 機能概要 : フィルター情報を選択中アプリケーション情報に追加する。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class AddFiltersBLogic implements BLogic<AddFiltersInput> {

	// 画面遷移判別定数
	private static final String REGIST = "登録";    // 登録

	// QueryDAO
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting." +
	"applicationgroups.blogic.AddFiltersBLogic");

	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 : フィルター情報を選択中アプリケーション情報に追加する。
	 * @param params AddFiltersInput入力データクラス
	 * @return result BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(AddFiltersInput params) {

		log.debug("AddFiltersBLogic処理開始");

		String failString;
		SelectFilterMasterList resultFltList = null;                // フィルタマスタリスト
		List<SelectApplicationList4Search> resultAplList = null;    // アプリケーションマスタリスト

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();

		// 遷移先が登録系画面の場合
		if (params.getDisplayType().equals(REGIST)) {
			log.debug("遷移先が登録系画面");
			failString = "failure4Regist";
			result.setResultString(failString);

			// 遷移先が更新系画面の場合
		} else {
			log.debug("遷移先が更新系画面");
			failString = "failure4Update";
			result.setResultString(failString);
		}

		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = params.getUvo();

		// 画面でチェックボックスonにされたフィルタ情報を取得
		String[] indexList = params.getIndex().split(",");

		// 選択チェック分繰り返しフィルタ追加用リストに詰める
		List<SelectApplicationFilterList> filterList4Add =
			new ArrayList<SelectApplicationFilterList>();

		if(!indexList[0].equals("") || indexList.length != 1) {
			log.debug("チェックあり");
			for(int i=0; i< indexList.length; i++) {
				log.debug("選択チェック分フィルタ追加用リストに設定中:" + i);
				filterList4Add.add(params.getFilterList4Add().
						get(Integer.parseInt(indexList[i])));
			}
		}

		try {
			/***********************************************************************
			 *  ①フィルタリスト取得（マスタ情報）
			 **********************************************************************/
			FilterMasterList filterMasterList = new FilterMasterList();
			resultFltList = filterMasterList.getFilterMasterList(queryDAO, uvo);


			/***********************************************************************
			 *  ②アプリケーションリスト取得（マスタ情報）
			 **********************************************************************/
			ApplicationList applicationList = new ApplicationList();
			resultAplList = applicationList.getApplicationList(queryDAO, uvo);

		} catch(Exception e) {
			// 例外
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("EK061004", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060064"));
			result.setErrors(messages);
			result.setResultString(failString);
			return result;
		}


		/***********************************************************************
		 *  ③「選択中のアプリケーション」リストをマージ
		 **********************************************************************/
		List<SelectedAplInfo> resultSelectedAplList = new ArrayList<SelectedAplInfo>();
		// セッション情報取得
		List<SelectedAplInfo> sessionAplList = params.getSessionAplList();

		// セッション情報の件数分、以下の処理を行う
		if (sessionAplList != null) {
			log.debug("セッション情報がnullでない場合");
			for (int i = 0; i < sessionAplList.size(); i++) {
				log.debug("選択中アプリケーションリストマージ中:" + i);
				// Typeがフィルタでない場合
				if (!sessionAplList.get(i).getType().equals(CusconConst.TYPE_FLT)) {
					log.debug("フィルタ以外のアプリケーションを追加");
					SelectedAplInfo selectedAplInfo = new SelectedAplInfo();
					selectedAplInfo.setSeqNo(sessionAplList.get(i).getSeqNo());
					selectedAplInfo.setName(sessionAplList.get(i).getName());
					selectedAplInfo.setType(sessionAplList.get(i).getType());

					// 選択中アプリケーション情報に追加
					resultSelectedAplList.add(selectedAplInfo);
				}
			}
		}

		// フィルタ情報を選択中アプリケーション情報に追加
		if (filterList4Add != null) {
			log.debug("画面で選択されたフィルタを追加");
			for (int i = 0; i < filterList4Add.size(); i++) {
				log.debug("選択中アプリケーションリストマージ中:" + i);
				SelectedAplInfo selectedFltInfo = new SelectedAplInfo();
				selectedFltInfo.setName(filterList4Add.get(i).getName());
				selectedFltInfo.setType(CusconConst.TYPE_FLT);
				resultSelectedAplList.add(selectedFltInfo);
			}
		}


		/***********************************************************************
		 *  ④結果セット
		 **********************************************************************/
		AddFiltersOutput out = new AddFiltersOutput();
		out.setName(params.getName());
		out.setFilterList(resultFltList);
		out.setApplicationList(resultAplList);
		out.setSelectedAplList(resultSelectedAplList);
		out.setSearchWord("");

		result.setResultObject(out);

		// 遷移先が登録系画面の場合
		if (params.getDisplayType().equals(REGIST)) {
			log.debug("遷移先が登録系画面");
			result.setResultString("success4Regist");
			// 遷移先が更新系画面の場合
		} else {
			log.debug("遷移先が更新系画面");
			result.setResultString("success4Update");
		}

		log.debug("AddFiltersBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
