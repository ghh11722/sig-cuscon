/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditUrlFilterBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/14     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.common.vo.UrlFilters;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.EditUrlFilterOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.PolicyInput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

import org.apache.log4j.Logger;

/**
 * クラス名 : EditUrlFilterBLogic
 * 機能概要 :UrlFilter項目選択時の処理を行う。
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/14
 *          新規作成
 * @see
 */
public class EditUrlFilterBLogic implements BLogic<PolicyInput> {

	/**
     * QueryDAO。
     * Springによりインスタンス生成され設定される。
     */
    private QueryDAO queryDAO = null;
    private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic.EditUrlFilterBLogic");
    private MessageAccessor messageAccessor = null;
	/**
	 * メソッド名 :execute
	 * 機能概要 : Webフィルタマスタ、入力出所Webフィルタリストをアウトプットクラスに設定する。
	 * @param param PolicyInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(PolicyInput param) {

		log.debug("EditUrlFilterBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");
        // エラーメッセージ
        BLogicMessages messages = new BLogicMessages();
        CusconUVO uvo = param.getUvo();

        // 選択したリストからオブジェクトを取得する。
        SelectPolicyList selectPolicy = param.getPolicyList().get(param.getIndex());

		List<UrlFilters> urlFilterList = null;
		{
			// InputBase設定
			InputBase inputBase = new InputBase();
			inputBase.setVsysId(uvo.getVsysId());                // Vsys-ID
			inputBase.setGenerationNo(CusconConst.ZERO_GENE);    // 世代番号
			inputBase.setModFlg(CusconConst.DEL_NUM);            // 変更フラグ

			// Webフィルタ情報取得
			try {
				// Webフィルタ情報取得
				urlFilterList = queryDAO.executeForObjectList("CommonT_UrlFilters-1", inputBase);
			} catch(Exception e) {
				// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
	    		// DBアクセスエラー
	        	Object[] objectgArray = {e.getMessage()};
	    		// ポリシー設定で例外が発生しました。{0}
	        	log.error(messageAccessor.getMessage(
	        			"EK051001", objectgArray, param.getUvo()));
	        	// Webフィルタ設定画面表示に失敗しました。
	        	messages.add("message", new BLogicMessage("DK050041"));
				result.setErrors(messages);
				result.setResultString("failure");
	        	return result;
			}
		}

		// ビジネスロジックの出力クラスに結果を設定する
        EditUrlFilterOutput out = new EditUrlFilterOutput();

        out.setName(selectPolicy.getName());
        out.setUrlFilterList(urlFilterList);
        out.setUrlFilterName(selectPolicy.getUrlFilterName());
        out.setUrlfilterDefaultFlg(selectPolicy.getUrlfilterDefaultFlg());

        result.setResultObject(out);
        result.setResultString("success");

        log.debug("EditUrlFilterBLogic処理終了");
        return result;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}