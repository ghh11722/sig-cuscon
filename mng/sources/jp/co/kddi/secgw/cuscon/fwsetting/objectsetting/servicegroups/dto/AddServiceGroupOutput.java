/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddServiceGroupOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/08     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.servicegroups.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.ServicesAndGroupsInfo;

/**
 * クラス名 : AddServiceGroupOutput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/08
 *          新規作成
 * @see
 */
public class AddServiceGroupOutput {

	// サービスグループ追加用サービス＆サービスグループ
	private List<ServicesAndGroupsInfo> serviceAndServiceGrp4Add = null;

	/**
	 * メソッド名 : serviceAndServiceGrp4AddのGetterメソッド
	 * 機能概要 : serviceAndServiceGrp4Addを取得する。
	 * @return serviceAndServiceGrp4Add
	 */
	public List<ServicesAndGroupsInfo> getServiceAndServiceGrp4Add() {
		return serviceAndServiceGrp4Add;
	}

	/**
	 * メソッド名 : serviceAndServiceGrp4AddのSetterメソッド
	 * 機能概要 : serviceAndServiceGrp4Addをセットする。
	 * @param serviceAndServiceGrp4Add
	 */
	public void setServiceAndServiceGrp4Add(List<ServicesAndGroupsInfo> serviceAndServiceGrp4Add) {
		this.serviceAndServiceGrp4Add = serviceAndServiceGrp4Add;
	}
}
