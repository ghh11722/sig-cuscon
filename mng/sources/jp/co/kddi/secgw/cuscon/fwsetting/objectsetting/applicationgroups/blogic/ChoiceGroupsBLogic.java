/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ChoiceGroupsBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.blogic;

import java.util.ArrayList;
import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto.ChoiceGroupsInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto.ChoiceGroupsOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.ApplicationGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import org.apache.log4j.Logger;

/**
 * クラス名 : ChoiceGroupsBLogic
 * 機能概要 : グループ設定画面表示処理を行う。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/01
 *          新規作成
 * @see
 */
public class ChoiceGroupsBLogic implements BLogic<ChoiceGroupsInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger(
	"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.blogic." +
	"ChoiceGroupsBLogic");

	private MessageAccessor messageAccessor = null;

	// 画面遷移判別定数
	private static final String REGIST = "登録";     // 登録
	private static final String UPDATE = "更新";     // 更新

	/**
	 * メソッド名 : execute
	 * 機能概要 : グループ設定画面表示処理を行う。
	 * @param params ChoiceGroupsInput入力データクラス
	 * @return result BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(ChoiceGroupsInput params) {
		log.debug("ChoiceGroupsBLogic処理開始");
		List<SelectApplicationGroupList> list4Out = null;   // アプリケーショングループ情報のリスト
		String failString;

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();

		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();

		if (params.getDisplayType().equals(REGIST)) {
			log.debug("遷移先が登録系画面");
			failString = "failure4Regist";
			result.setResultString(failString);
		} else {
			log.debug("遷移先が更新系画面");
			failString = "failure4Update";
			result.setResultString(failString);
		}

		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = params.getUvo();

		/***********************************************************************
		 *  ①画面から受け取った「選択中のアプリケーション」リストからグループ情報だけ取得
		 **********************************************************************/
		// 「選択中のアプリケーション」リスト
		String[] selectAplList = params.getSelectAplList();

		// 「選択中のアプリケーション」のタイプリスト（並び順は「選択中のアプリケーション」リストと同様）
		String[] selectTypeList = params.getSelectTypeList();

		// 編集用tmpグループリスト
		ArrayList<String> tmpGrpList = new ArrayList<String>();

		// 「選択中のアプリケーション」リストの件数分、以下の処理を実行
		if (!selectAplList[0].equals("null") || selectAplList.length != 1) {
			log.debug("タイプがグループのアプリケーションをリストに格納");
			for (int i = 0; i < selectAplList.length; i++) {
				// タイプがグループの場合、それをリストに格納
				if (selectTypeList[i].equals(CusconConst.TYPE_GRP)) {
					tmpGrpList.add(selectAplList[i]);
				}
			}
		}


		/***********************************************************************
		 *  ②DBからアプリケーショングループ情報を取得
		 **********************************************************************/
		List<SelectApplicationGroupList> dbGrpList = null;

		try {
			ApplicationGroupList applicationGroupList =
										new ApplicationGroupList(messageAccessor);
			dbGrpList = applicationGroupList.getApplicationGroupList(queryDAO, uvo);

		} catch(Exception e) {
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("EK061004", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060065"));
			result.setErrors(messages);
        	return result;
		}


		/***********************************************************************
		 *  ③マッチング処理
		 **********************************************************************/
		list4Out = new ArrayList<SelectApplicationGroupList>();

		// ①で得たリストと②で得たリストのアプリケーショングループ名が一致する場合、チェックON対象とみなす。
		if (dbGrpList != null) {
			for (int i = 0; i < dbGrpList.size(); i++) {
				log.debug("チェックボックスON/OFF判定中:" + i);

				SelectApplicationGroupList selectApplicationGroupList = new SelectApplicationGroupList();

				String name = params.getName();
				String oldName = params.getOldName();

				if (name == null) {
					log.debug("nameがnullのため、空文字に変換");
					name = "";
				}

				// 編集画面から遷移してきた場合
				if (params.getDisplayType().equals(UPDATE)) {
					// 編集中のグループ名でないものを画面表示対象として扱う（編集画面から遷移してきた場合）

					if (!dbGrpList.get(i).getName().equals(oldName)) {
						log.debug("編集中のグループ名でなければチェックボックスON/OFF判別");

						for (int j = 0; j < tmpGrpList.size(); j++) {
							// チェックON/OFF判別
							if (tmpGrpList.get(j).equals(dbGrpList.get(i).getName())) {
								log.debug("チェックボックスON対象:" + dbGrpList.get(i).getName());
								selectApplicationGroupList.setChkFlg(true);
							}
						}
						selectApplicationGroupList.setName(dbGrpList.get(i).getName());
						selectApplicationGroupList.setMembers(dbGrpList.get(i).getMembers());
						selectApplicationGroupList.setApplications(dbGrpList.get(i).getApplications());
						list4Out.add(selectApplicationGroupList);
					}

				// 追加画面から遷移してきた場合
				} else {
					for (int j = 0; j < tmpGrpList.size(); j++) {
						// チェックON/OFF判別
						if (tmpGrpList.get(j).equals(dbGrpList.get(i).getName())) {
							log.debug("チェックボックスON対象:" + dbGrpList.get(i).getName());
							selectApplicationGroupList.setChkFlg(true);
						}
					}
					selectApplicationGroupList.setName(dbGrpList.get(i).getName());
					selectApplicationGroupList.setMembers(dbGrpList.get(i).getMembers());
					selectApplicationGroupList.setApplications(dbGrpList.get(i).getApplications());
					list4Out.add(selectApplicationGroupList);
				}
			}
		}


		/***********************************************************************
		 *  ④セッション設定処理
		 **********************************************************************/
		List<SelectedAplInfo> selectedAplList = new ArrayList<SelectedAplInfo>();
		// 選択中アプリケーション情報をセッション情報に格納
		List<SelectedAplInfo> sessionAplList = new ArrayList<SelectedAplInfo>();
		if (!selectAplList[0].equals("null") || selectAplList.length != 1) {
			log.debug("セッション情報作成");
			for (int i = 0; i < params.getSelectAplList().length; i++) {
				log.debug("セッション情報作成中:" + i);

				SelectedAplInfo aplInfo = new SelectedAplInfo();

				aplInfo.setName(params.getSelectAplList()[i]);
				aplInfo.setType(params.getSelectTypeList()[i]);

				selectedAplList.add(aplInfo);
			}
			sessionAplList = selectedAplList;
		} else {
			log.debug("セッションクリア");
			// 選択中アプリケーション情報をセッション情報に格納
			sessionAplList = null;
		}


		/***********************************************************************
		 *  ⑤レスポンス返却
		 **********************************************************************/
		ChoiceGroupsOutput output = new ChoiceGroupsOutput();
		output.setGroupList4Add(list4Out);
		output.setSessionAplList(sessionAplList);
		output.setDisplayType(params.getDisplayType());
		output.setName(params.getName());
		output.setOldName(params.getOldName());
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("ChoiceGroupsBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}
	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
