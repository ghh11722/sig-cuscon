/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : RiskInfo.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/25     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo;

import java.io.Serializable;

/**
 * クラス名 : RiskInfo
 * 機能概要 : リスクの汎用データクラス
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/25
 *          新規作成
 * @see
 */
public class RiskInfo implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 2663313119907275707L;
	// リスク
	private Integer name = null;
	// チェック情報（画面にチェックボックスON/OFFがある場合に使用 ON:true/OFF:false）
	private boolean chkFlg = false;

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public Integer getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(Integer name) {
		this.name = name;
	}
	/**
	 * メソッド名 : chkFlgのGetterメソッド
	 * 機能概要 : chkFlgを取得する。
	 * @return chkFlg
	 */
	public boolean getChkFlg() {
		return chkFlg;
	}
	/**
	 * メソッド名 : chkFlgのSetterメソッド
	 * 機能概要 : chkFlgをセットする。
	 * @param chkFlg
	 */
	public void setChkFlg(boolean chkFlg) {
		this.chkFlg = chkFlg;
	}
}
