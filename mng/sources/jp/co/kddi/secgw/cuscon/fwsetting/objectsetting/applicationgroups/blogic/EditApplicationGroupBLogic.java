/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditApplicationGroupBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/24     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.blogic;

import java.util.ArrayList;
import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.ApplicationGroups;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.fwsetting.common.ApplicationList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.FilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto.EditApplicationGroupInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto.EditApplicationGroupOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.LinkInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationList4Search;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectFilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;
import org.apache.log4j.Logger;

/**
 * クラス名 : EditApplicationGroupBLogic
 * 機能概要 : アプリケーショングループ編集画面を表示する。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/24
 *          新規作成
 * @see
 */
public class EditApplicationGroupBLogic implements BLogic<EditApplicationGroupInput> {

	// オブジェクトタイプ定数
	private static final int APL = 0;    // アプリケーションオブジェクトマスタ
	private static final int GRP = 1;    // アプリケーショングループオブジェクト
	private static final int FLT = 2;    // アプリケーションフィルタオブジェクト

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger(
	"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.blogic.EditApplicationGroupBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 : グループ情報をアウトプットクラスに設定する。
	 * @param params EditApplicationGroupInput入力データクラス
	 * @return result BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(EditApplicationGroupInput params) {

		log.debug("EditApplicationGroupBLogic処理開始");

		// 変数初期化処理
		List<SelectedAplInfo> selectedAplList = null;                       // 選択中アプリケーション情報
		EditApplicationGroupOutput output = null;                           // 返却用リスト
		String typeName = null;                                             // 選択中のアプリケーションタイプ名称
		String name = null;                                                 // 編集中アプリケーショングループ名
		SelectFilterMasterList selectFilterMasterList = null;               // フィルタエリア情報
		List<SelectApplicationList4Search> selectApplicationList = null;    // アプリケーション情報
		int seqNo = 0;                                                      // シーケンス番号

		// アプリケーショングループリンク情報
		List<LinkInfo> linkList = new ArrayList<LinkInfo>();
		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();

		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = params.getUvo();
		String vsysId = uvo.getVsysId();

		// InputBase設定
		InputBase inputBase = new InputBase();
		inputBase.setVsysId(vsysId);                         // Vsys-ID
		inputBase.setGenerationNo(CusconConst.ZERO_GENE);    // 世代番号
		inputBase.setModFlg(CusconConst.DEL_NUM);            // 変更フラグ

		// アプリケーション設定画面から遷移してきた場合
		if (params.getDisplayType() == null || params.getDisplayType() == "") {
			// 一覧画面で選択された行のインデックス取得
			String index = params.getIndex();
			// 一覧画面で選択された行のアプリケーショングループ名取得
			name = params.getApplicationGroupList().get(Integer.valueOf(index)).getName();

		// アプリケーションフィルタ設定 or アプリケーショングループ設定画面から遷移してきた場合
		} else {
			// oldNameを設定
			name = params.getOldName();
		}

		try {
			/***********************************************************************
			 *  ①フィルタエリア情報取得共通処理
			 **********************************************************************/
			FilterMasterList filterMasterList = new FilterMasterList();
			// フィルタエリア情報取得
			selectFilterMasterList = filterMasterList.getFilterMasterList(queryDAO, uvo);


			/***********************************************************************
			 *  ②アプリケーション情報取得共通処理
			 **********************************************************************/

			ApplicationList applicationList = new ApplicationList();
			// アプリケーション情報取得
			selectApplicationList = applicationList.getApplicationList(queryDAO, uvo);

		} catch(Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage(
					"FK061016", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060030"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}


		/***********************************************************************
		 *  ③後続処理用SEQ_NO取得
		 **********************************************************************/
		// 検索条件設定
		ApplicationGroups searchApplicationGroupKey = new ApplicationGroups();
		searchApplicationGroupKey.setVsysId(vsysId);                         // Vsys-ID
		searchApplicationGroupKey.setGenerationNo(CusconConst.ZERO_GENE);    // 世代番号
		searchApplicationGroupKey.setName(name);                             // アプリケーショングループ名

		List<ApplicationGroups> applicationGroupList = null;
		try {
			// アプリケーショングループオブジェクトテーブルを検索し、対象アプリケーショングループのSEQ_NOを取得
			applicationGroupList = queryDAO.executeForObjectList(
					"ApplicationGroupsBLogic-2", searchApplicationGroupKey);
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage(
					"FK061016", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060030"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

		seqNo = applicationGroupList.get(0).getSeqNo();


		/***********************************************************************
		 *  ④リンク情報取得
		 **********************************************************************/
		// 検索条件設定
		ApplicationGroups applicationGroups = new ApplicationGroups();
		applicationGroups.setSeqNo(seqNo);
		LinkInfo link = new LinkInfo();
		try {
			if (params.getDisplayType() == null || params.getDisplayType().equals("")) {
				log.debug("DB検索");
				// アプリケーショングループリンク情報取得（名称、オブジェクトタイプ、リンク先名称の昇順でリストに格納）
				// ここで画面で選択したアプリケーショングループに紐づくリンク情報を全て取得
				linkList = queryDAO.executeForObjectList("ApplicationGroupsBLogic-6", applicationGroups);
			} else {
				log.debug("DisplayTypeに値が入っていない場合");
				if(params.getSessionAplList() != null) {
					log.debug("セッションがnullでない");
					for(int i=0;i<params.getSessionAplList().size(); i++) {
						log.debug("セッション詰め替え");
						link.setSeqNo(params.getSessionAplList().get(i).getSeqNo());
						link.setLinkName(params.getSessionAplList().get(i).getName());
						if(params.getSessionAplList().get(i).getType().equals("アプリケーション")) {
							log.debug("アプリ");
							link.setObjectType(APL);
						} else if(params.getSessionAplList().get(i).getType().equals("グループ")) {
							log.debug("グループ");
							link.setObjectType(GRP);
						} else {
							log.debug("フィルター");
							link.setObjectType(FLT);
						}
						linkList.add(link);
						link = new LinkInfo();
					}
				}
			}
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage(
					"FK061016", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060030"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}


		/***********************************************************************
		 *  ⑤「選択中アプリケーション」リスト作成
		 **********************************************************************/
		selectedAplList = new ArrayList<SelectedAplInfo>();
		// アプリケーショングループに紐づくApplication/Filter/Group情報を取得し選択中アプリケーションリストへ格納
		for (int i = 0; i < linkList.size(); i++) {
			log.debug("選択中アプリケーションリスト作成中:" + i);
			typeName = null;
			SelectedAplInfo selectedAplInfo = new SelectedAplInfo();
			selectedAplInfo.setSeqNo(linkList.get(i).getLinkSeqNo());    // リンク先シーケンス番号
			selectedAplInfo.setName(linkList.get(i).getLinkName());      // リンク先名称

			// オブジェクトタイプがアプリケーションの場合
			if (linkList.get(i).getObjectType() == APL) {
				log.debug("オブジェクトタイプがアプリケーション");
				// 選択中のアプリケーションタイプ名称変換（アプリケーション）
				typeName = CusconConst.TYPE_APL;

				// オブジェクトタイプがグループの場合
			} else if (linkList.get(i).getObjectType() == GRP) {
				log.debug("オブジェクトタイプがグループ");
				// 選択中のアプリケーションタイプ名称変換（アプリケーショングループ）
				typeName = CusconConst.TYPE_GRP;

				// オブジェクトタイプがフィルタの場合
			} else {
				log.debug("オブジェクトタイプがフィルタ");
				// 選択中のアプリケーションタイプ名称変換（アプリケーションフィルタ）
				typeName = CusconConst.TYPE_FLT;
			}
			selectedAplInfo.setType(typeName);    // リンク先タイプ
			selectedAplList.add(selectedAplInfo);
		}


		/***********************************************************************
		 *  ⑥結果セット
		 **********************************************************************/
		// 結果セット
		output = new EditApplicationGroupOutput();

		// アプリケーショングループ名
		if (params.getDisplayType() == null || params.getDisplayType() == "") {
			log.debug("DisplayTypeに値が入っている場合");
			output.setName(name);
		} else {
			log.debug("DisplayTypeに値が入っていない場合");
			output.setName(params.getName());
		}

		output.setFilterList(selectFilterMasterList);        // フィルタリスト
		output.setApplicationList(selectApplicationList);    // アプリケーションリスト
		output.setSelectedAplList(selectedAplList);          // 選択中アプリケーションリスト
		output.setOldName(name);                             // 選択中アプリケーショングループ名
		output.setDisplayType("更新");
		output.setSearchWord("");

		// レスポンス返却
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("EditApplicationGroupBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
