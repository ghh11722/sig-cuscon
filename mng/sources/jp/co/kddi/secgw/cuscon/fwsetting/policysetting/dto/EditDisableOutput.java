/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditDisableOutput.java
 *
 * [変更履歴]
 * 日付        更新者              内容
 * 2013/09/01  kkato@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto;

/**
 * クラス名 : EditDisableOutput
 * 機能概要 :Disable項目選択時の出力データクラス
 * 備考 :
 * @author kkato@PROSITE
 * @version 1.0 kkato@PROSITE
 *          Created 2011/05/18
 *          新規作成
 * @see
 */
public class EditDisableOutput {

	// ルール名
	private String name = null;

	// セキュリティルール有効/無効フラグ
	private String disableFlg = null;
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : disableFlgのGetterメソッド
	 * 機能概要 : disableFlgを取得する。
	 * @return disableFlg
	 */
	public String getDisableFlg() {
		return disableFlg;
	}
	/**
	 * メソッド名 : disableFlgのSetterメソッド
	 * 機能概要 : disableFlgをセットする。
	 * @param disableFlg
	 */
	public void setDisableFlg(String disableFlg) {
		this.disableFlg = disableFlg;
	}
}
