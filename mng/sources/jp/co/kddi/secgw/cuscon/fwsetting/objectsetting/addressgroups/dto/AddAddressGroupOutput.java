/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddAddressGroupOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/08     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.dto;

import java.util.List;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.AddressesAndGroupsInfo;

/**
 * クラス名 : AddAddressGroupOutput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/08
 *          新規作成
 * @see
 */
public class AddAddressGroupOutput {

	// アドレスグループ追加用サービス＆サービスグループ
	private List<AddressesAndGroupsInfo> addressAndAddressGrp4Add = null;

	/**
	 * メソッド名 : addressAndAddressGrp4AddのGetterメソッド
	 * 機能概要 : addressAndAddressGrp4Addを取得する。
	 * @return addressAndAddressGrp4Add
	 */
	public List<AddressesAndGroupsInfo> getAddressAndAddressGrp4Add() {
		return addressAndAddressGrp4Add;
	}

	/**
	 * メソッド名 : addressAndAddressGrp4AddのSetterメソッド
	 * 機能概要 : addressAndAddressGrp4Addをセットする。
	 * @param addressAndAddressGrp4Add
	 */
	public void setAddressAndAddressGrp4Add(List<AddressesAndGroupsInfo> addressAndAddressGrp4Add) {
		this.addressAndAddressGrp4Add = addressAndAddressGrp4Add;
	}
}
