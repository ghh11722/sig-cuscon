/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : DelApplicationFiltersInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/23     morisou                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.dto;

import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationFilterList;

/**
 * クラス名 : DelApplicationFiltersInput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/23
 *          新規作成
 * @see
 */
public class DelApplicationFiltersInput {
    private CusconUVO uvo = null;

	// アプリケーションフィルタリスト
	private List<SelectApplicationFilterList> applicationFilterList = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}

	/**
	 * メソッド名 : applicationFilterListのGetterメソッド
	 * 機能概要 : applicationFilterListを取得する。
	 * @return applicationFilterList
	 */
	public List<SelectApplicationFilterList> getApplicationFilterList() {
		return applicationFilterList;
	}

	/**
	 * メソッド名 : applicationFilterListのSetterメソッド
	 * 機能概要 : applicationFilterListをセットする。
	 * @param applicationFilterList
	 */
	public void setApplicationFilterList(List<SelectApplicationFilterList> applicationFilterList) {
		this.applicationFilterList = applicationFilterList;
	}
}
