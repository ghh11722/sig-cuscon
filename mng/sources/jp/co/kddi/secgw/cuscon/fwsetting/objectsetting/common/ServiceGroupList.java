/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ServiceGroupList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/20     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectServiceGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.LinkInfo;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : ServiceGroupList
 * 機能概要 : サービスグループ一覧を表示するための情報を取得する。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/20
 *          新規作成
 * @see
 */
public class ServiceGroupList {

	private String lastName;                                 // 切り替え判別用サービスグループ名
	private int memberCount;                                 // メンバカウンタ
	private StringBuffer services;                           // サービス＆サービスグループ連結文字列
	private ArrayList<SelectServiceGroupList> resultList;    // 返却用リスト

	private static Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.ServiceGroupList");

	// メッセージクラス
    private static MessageAccessor messageAccessor = null;

    /**
     * メソッド名 : コンストラクタ
     * 機能概要 : -
     * @param queryDAO
     * @param uvo
     */
    public ServiceGroupList(MessageAccessor msgAcc) {
    	log.debug("ServiceGroupListコンストラクタ2処理開始");
    	messageAccessor = msgAcc;
    	log.debug("ServiceGroupListコンストラクタ2処理終了");
    }

	/**
	 * メソッド名 :getServiceGroupList
	 * 機能概要 : サービスグループ一覧情報取得処理
	 * @param vsysId
	 * @return resultList
	 */

	public List<SelectServiceGroupList> getServiceGroupList(QueryDAO queryDAO,
												CusconUVO uvo) throws Exception {
		log.debug("getServiceGroupList処理開始");
		// InputBase設定
		InputBase inputBase = new InputBase();
		inputBase.setVsysId(uvo.getVsysId());                         // Vsys-ID
		inputBase.setGenerationNo(CusconConst.ZERO_GENE);    // 世代番号
		inputBase.setModFlg(CusconConst.DEL_NUM);            // 変更フラグ

		List<LinkInfo> linkList;

		try {
			// サービスグループリンク情報取得（名称、オブジェクトタイプ、リンク先名称の昇順でリストに格納）
			linkList = new ArrayList<LinkInfo>();
			linkList = queryDAO.executeForObjectList("ServiceGroupsBLogic-12", inputBase);

		} catch(Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		log.fatal(messageAccessor.getMessage("FK061024", objectgArray, uvo));
    		throw e;

		}

		resultList = new ArrayList<SelectServiceGroupList>();
		lastName = null;                   // 前回LOOP名
		memberCount = 0;                   // メンバカウンタ
		services = new StringBuffer();     // サービス＆サービスマスタ＆サービスグループ名称連結用

		if (!linkList.isEmpty()) {
			log.debug("サービスグループリンクが存在");
			// サービスグループリンク情報の件数分、以下の処理を行う
			for (int i = 0; i < linkList.size(); i++) {
				log.debug("サービスグループリンク情報の件数:" + i);
				// サービスグループ名が切り替わった場合、以下の処理を行う
				if (i != 0 && !lastName.equals(linkList.get(i).getName())) {
					log.debug("サービスグループ名が切替:" + i);
					// 返却用リスト設定処理
					setResultList();
					// メンバカウンタリセット
					memberCount = 0;
					// サービスリセット
					services = new StringBuffer();
				}

				// メンバカウントアップ
				memberCount++;
				// メンバ数が1の場合
				if (memberCount == 1) {
					log.debug("メンバ数が1:" + i);
					// 文字連結（1個目限定）
					services = new StringBuffer(String.valueOf(linkList.get(i).getLinkName()));

					// メンバ数が2以上の場合
				} else {
					log.debug("メンバ数が2以上" + i);
					// 文字連結（2個目以降）
					services.append(",").append(linkList.get(i).getLinkName());
				}

				lastName = linkList.get(i).getName();
			}
			// 返却用リスト設定処理
			setResultList();
		}
		log.debug("getServiceGroupList処理終了");
		return resultList;
	}

	/**
	 * メソッド名 :setResultList
	 * 機能概要 : 返却用リスト設定処理
	 * @param
	 */
	private void setResultList() {
		log.debug("setResultList処理開始");
		SelectServiceGroupList selectServiceGroupListOutput = new SelectServiceGroupList();

		// Name
		selectServiceGroupListOutput.setName(lastName);
		// Members
		selectServiceGroupListOutput.setMembers(memberCount);
		// Services
		selectServiceGroupListOutput.setServices(services.toString());

		// resultListにセット
		resultList.add(selectServiceGroupListOutput);
		log.debug("setResultList処理終了");
	}
}
