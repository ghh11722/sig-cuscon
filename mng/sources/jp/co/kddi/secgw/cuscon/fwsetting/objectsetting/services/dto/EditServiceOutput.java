/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditServiceOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/10     kinoshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.dto;

/**
 * クラス名 : EditServiceOutput
 * 機能概要 :
 * 備考 :
 * @author kinoshtt
 * @version 1.0 kinoshtt
 *          Created 2010/03/10
 *          新規作成
 * @see
 */
public class EditServiceOutput {

	// サービス名
	private String name = null;

	// プロトコル
	private String protocol = null;

	// ポート
	private String port = null;

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * メソッド名 : protocolのGetterメソッド
	 * 機能概要 : protocolを取得する。
	 * @return protocol
	 */
	public String getProtocol() {
		return protocol;
	}

	/**
	 * メソッド名 : protocolのSetterメソッド
	 * 機能概要 : protocolをセットする。
	 * @param protocol
	 */
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	/**
	 * メソッド名 : portのGetterメソッド
	 * 機能概要 : portを取得する。
	 * @return port
	 */
	public String getPort() {
		return port;
	}

	/**
	 * メソッド名 : portのSetterメソッド
	 * 機能概要 : portをセットする。
	 * @param port
	 */
	public void setPort(String port) {
		this.port = port;
	}
}
