/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddressGroupsOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/19     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.dto;

import java.util.List;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectAddressGroupList;

/**
 * クラス名 : AddressGroupsOutput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/19
 *          新規作成
 * @see
 */
public class AddressGroupsOutput {

	// アドレスグループリスト
	private List<SelectAddressGroupList> addressGroupList = null;
	private String message = null;


	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * メソッド名 : addressGroupListのGetterメソッド
	 * 機能概要 : addressGroupListを取得する。
	 * @return addressGroupList
	 */
	public List<SelectAddressGroupList> getAddressGroupList() {
		return addressGroupList;
	}

	/**
	 * メソッド名 : addressGroupListのSetterメソッド
	 * 機能概要 : addressGroupListをセットする。
	 * @param addressGroupList
	 */
	public void setAddressGroupList(List<SelectAddressGroupList> addressGroupList) {
		this.addressGroupList = addressGroupList;
	}
}
