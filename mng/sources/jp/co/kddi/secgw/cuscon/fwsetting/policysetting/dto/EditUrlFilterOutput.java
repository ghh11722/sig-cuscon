/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditUrlFilterOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/10     ktakenaka@PROSITE                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.vo.UrlFilters;

/**
 * クラス名 : EditUrlFilterOutput
 * 機能概要 :Zone項目選択時の出力データクラス
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/10
 *          新規作成
 * @see
 */
public class EditUrlFilterOutput {

	// ルール名
	private String name = null;

	// Webフィルタリスト
	private List<UrlFilters> urlFilterList= null;

	// Webフィルタ名
	private String urlFilterName = null;

	// Webフィルタ適用フラグ
	private int urlfilterDefaultFlg = 0;

	/**
	 * メソッド名 : urlFilterNameのGetterメソッド
	 * 機能概要 : urlFilterNameを取得する。
	 * @return urlFilterName
	 */
	public String getUrlFilterName() {
		return urlFilterName;
	}

	/**
	 * メソッド名 : urlFilterNameのSetterメソッド
	 * 機能概要 : urlFilterNameをセットする。
	 * @param urlFilterName
	 */
	public void setUrlFilterName(String urlFilterName) {
		this.urlFilterName = urlFilterName;
	}

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * メソッド名 : urlFilterListのGetterメソッド
	 * 機能概要 : urlFilterListを取得する。
	 * @return urlFilterList
	 */
	public List<UrlFilters> getUrlFilterList() {
		return urlFilterList;
	}

	/**
	 * メソッド名 : urlFilterListのSetterメソッド
	 * 機能概要 : urlFilterListをセットする。
	 * @param urlFilterList
	 */
	public void setUrlFilterList(List<UrlFilters> urlFilterList) {
		this.urlFilterList = urlFilterList;
	}

	/**
	 * メソッド名 : urlfilterDefaultFlgのGetterメソッド
	 * 機能概要 : urlfilterDefaultFlgを取得する。
	 * @return urlfilterDefaultFlg
	 */
	public int getUrlfilterDefaultFlg() {
		return urlfilterDefaultFlg;
	}
	/**
	 * メソッド名 : urlfilterDefaultFlgのSetterメソッド
	 * 機能概要 : urlfilterDefaultFlgをセットする。
	 * @param urlfilterDefaultFlg
	 */
	public void setUrlfilterDefaultFlg(int urlfilterDefaultFlg) {
		this.urlfilterDefaultFlg = urlfilterDefaultFlg;
	}
}
