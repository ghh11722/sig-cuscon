/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ApplicationGroupBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.blogic;

import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.common.CommitStatusManager;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto.ApplicationGroupsOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.ApplicationGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.dto.CommonInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import org.apache.log4j.Logger;

/**
 * クラス名 : ApplicationGroupBLogic
 * 機能概要 : アプリケーショングループ設定画面を表示する。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/01
 *          新規作成
 * @see
 */
public class ApplicationGroupBLogic implements BLogic<CommonInput> {

	// QueryDAO
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups." +
	"blogic.ApplicationGroupBLogic");

	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 : アプリケーショングループの一覧情報を取得し、取得した情報を呼び出し元に返却する。
	 * @param params CommonInput入力データクラス
	 * @return result BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(CommonInput params) {
		log.debug("ApplicationGroupBLogic処理開始");

		// SelectedList(セッションデータ)
		List<SelectedAplInfo> sessionAplList = null;

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = params.getUvo();

		/***********************************************************************
		 *  ①一覧情報検索共通処理
		 **********************************************************************/
		// コミットステータス管理クラスを生成する。
		CommitStatusManager commtStatMgr = new CommitStatusManager();

		ApplicationGroupsOutput output = new ApplicationGroupsOutput();
		try {
			if (commtStatMgr.isCommitting(queryDAO, uvo)) {
				// コミット中
				log.debug("コミット中です。");
	        	messages.add("message", new BLogicMessage("DK060074"));
				result.setErrors(messages);
				result.setMessages(messages);
				result.setResultString("failure");
	        	return result;
			} else {
				// 一覧情報検索共通処理
				ApplicationGroupList applicationGroupList =
					new ApplicationGroupList(messageAccessor);
				output.setApplicationGroupList(
						applicationGroupList.getApplicationGroupList(queryDAO, uvo));
			}

		} catch(Exception e) {
			// DBアクセスエラーログ出力
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("EK061004", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060025"));
			result.setErrors(messages);
			result.setMessages(messages);
			result.setResultString("failure");
			return result;
		}


		/***********************************************************************
		 *  ②セッション情報初期化
		 **********************************************************************/
		// 選択グループリスト設定
		sessionAplList = null;
		output.setSessionAplList(sessionAplList);


		/***********************************************************************
		 *  ③結果セット
		 **********************************************************************/
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("ApplicationGroupBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
