/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : IpAddressChk.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/19     hiroyasu                初版作成
 * 2013/09/01     kkato@PROSITE           アドレスOBJのFQDN対応
 * 2013/12/08     S.Yamada@Plum-systems   サブネットマスク/0除外対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.common;

import sun.net.util.IPAddressUtil;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;

/**
 * クラス名 : IpAddressChk
 * 機能概要 : IPアドレスチェッククラス
 * 備考 :
 * @author hiroyasu
 * @version 1.0 hiroyasu
 *          Created 2010/02/19
 *          新規作成
 * @see
 */
public class IpAddressChk {

	// ログ
	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.common.IpAddressChk");

	/**
	 * メソッド名 : isIpAddress
	 * 機能概要 : IPv4/IPv6フォーマットチェック
     * @param value
     *            検査対象の文字列
     * @return 入力値が正しければ <code>true</code>
	 */
    public static boolean isIpAddress(String value) {

    	log.debug("isIpAddress start:Input=" + value);

    	// 検証値がnullまたは空文字の時、false返却
    	if (GenericValidator.isBlankOrNull(value)) {
        	log.debug("isIpAddress end:value=none");
    		return false;
    	}
    	// フォーマットチェック
        if (!isIpv4(value)) {
        	log.debug("value!=IpV4");
        	// IPv4形式でない場合
           	if (!isIpv6(value)) {
            	log.debug("isIpAddress end:value!=IpV4/6");
           		// Ipv6形式で無い場合、false返却
           		return false;
           	}
        }
    	log.debug("isIpAddress return=true");
        return true;
    }

	/**
	 * メソッド名 : isIpRange
	 * 機能概要 : IPv4/IPv6レンジフォーマットチェック
     * @param value
     *            検査対象の文字列
     * @return 入力値が正しければ <code>true</code>
	 */
    public static boolean isIpRange(String value) {

    	log.debug("isIpRange start:Input=" + value);

    	// 検証値がnullまたは空文字の時、false返却
    	if (GenericValidator.isBlankOrNull(value)) {
        	log.debug("isIpRange end:value=none");
    		return false;
    	}

         // 範囲はマスク指定不可
        if (value.contains("/")) {
        	log.debug("マスク値指定不可");
        	return false;
        }
        // "-"でフィールド分割
        String[] address = value.split("-");
        if (address.length != 2) {
        	log.debug("isIpRange end:範囲指定不正");
        	return false;
        }
        boolean[] ipv4flg = new boolean[2];
        for (int i = 0; i < 2; i++) {
        	log.debug("フォーマットチェックループ");
        	ipv4flg[i] = false;
        	ipv4flg[i] = isIpv4(address[i]);
        	if (!(ipv4flg[i])) {
            	log.debug("IPv4フォーマットチェックNG");
               	if (!isIpv6(address[i])) {
                	log.debug("isIpRange end:IPv6フォーマットチェックNG");
               		return false;
               	}
           	}
		}
   		if (ipv4flg[0] != ipv4flg[1]) {
   			// "-"前後でフォーマットが異なる場合
        	log.debug("isIpRange end:IPv4/6混在");
   			return false;
   		}
   		if (ipv4flg[0]) {
   			// IPv4大小チェック
        	log.debug("IPv4大小チェック");
   			if (!isIpv4Range(address[0], address[1])) {
   	        	log.debug("isIpRange end:IPv4大小チェックNG");
	   			return false;
			}
		} else {
			// IPv6大小チェック
        	log.debug("IPv4大小チェック");
			if (!isIpv6Range(address[0], address[1])) {
   	        	log.debug("isIpRange end:IPv6大小チェックNG");
				return false;
   			}
   		}
       	log.debug("isIpRange return=true");
		return true;
	}

// 20130901 kkato@PROSITE add start
	/**
	 * メソッド名 : isFqdn
	 * 機能概要 : FQDNフォーマットチェック
     * @param value
     *            検査対象の文字列
     * @return 入力値が正しければ <code>true</code>
	 */
    public static boolean isFqdn(String value) {

    	log.debug("isFqdn start:Input=" + value);

    	// 検証値がnullまたは空文字の時、false返却
    	if (GenericValidator.isBlankOrNull(value)) {
        	log.debug("isFqdn end:value=none");
    		return false;
    	}

		// 入力長さチェック
		if (value.length() > 255) {
	    	log.debug("isFqdn end:/入力長不正1");
			return false;
		}

		// 使用可能文字チェック
		if (!(value.matches("^[a-zA-Z0-9\\.-]*$"))) {
	    	log.debug("isFqdn end:正規表現チェックNG");
			return false;
		}

		// [.]で分割
        String[] s_fqdn = value.split("\\.");
		if (s_fqdn.length < 2) {
	    	log.debug("ピリオド区切りなし");
			return false;
		}
		// 各ラベルチェック
		for (int i = 0; i < s_fqdn.length; i++) {
			log.debug("各フィールドをチェック:" + s_fqdn[i]);
			if (s_fqdn[i].length() > 63) {
		    	log.debug("フィールド長63以上");
				return false;
			}
			if (s_fqdn[i].length() < 2) {
		    	log.debug("フィールド長2以下");
				return false;
			}
			if (s_fqdn[i].startsWith("-")) {
		    	log.debug("フィールド先頭[-]");
				return false;
			}
			if (s_fqdn[i].endsWith("-")) {
		    	log.debug("フィールド終了[-]");
				return false;
			}
		}

        log.debug("isFqdn return=true");
		return true;
	}
// 20130901 kkato@PROSITE add end

	/**
	 * メソッド名 : isIpv4
	 * 機能概要 : IPv4フォーマットチェック
	 * @param val 検査対象の文字列
	 * @return 入力値が正しければ <code>true</code>
	 */
	private static boolean isIpv4(String val) {

    	log.debug("isIpv4 start:Input=" + val);

    	String[] s_val = val.split("/");
		if (s_val.length < 1 || s_val.length > 2) {
	    	log.debug("isIpv4 end:/数不正1");
			return false;
		}
		if (val.contains("/") && s_val.length != 2) {
	    	log.debug("isIpv4 end:/数不正2");
			return false;
		}

		if (!(s_val[0].matches("^([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})$"))) {
	    	log.debug("isIpv4 end:正規表現チェックNG");
			return false;
		}
		if (null == IPAddressUtil.textToNumericFormatV4(s_val[0])) {
	    	log.debug("isIpv4 end:textToNumericFormatV4=NG");
			return false;
		}
		// PALO用特殊チェック
		String[] palo = s_val[0].split("\\.");
		for (int i = 0; i < palo.length; i++) {
			log.debug("各フィールドの先頭が0でないことをチェック");
			if (palo[i].startsWith("0")) {
		    	log.debug("フィールド先頭0");
		    	if (palo[i].length() != 1) {
			    	log.debug("isIpv4 end:0で始まる2桁以上の数字はNG");
					return false;
		    	}
			}
		}

		// マスク値が存在する場合
		if (s_val.length == 2) {
	    	log.debug("サブマスク値が存在する");
	    	// マスク値がnullまたは空文字の時、false返却
	    	if (GenericValidator.isBlankOrNull(s_val[1])) {
		    	log.debug("isIpv4 end:サブマスク値がnullまたは空文字");
	    		return false;
	    	}
			if (!(s_val[1].matches("^([0-9]{1,2})$"))) {
		    	log.debug("isIpv4 end:サブマスク値が数字でない");
				return false;
			}
			// 2017/12/08 追加分(S.Yamada@Plum Systems Inc.)
			// サブネットマスク/0除外対応
			if (Integer.parseInt(s_val[1]) < 1
					|| Integer.parseInt(s_val[1]) > 32) {
		    	log.debug("isIpv4 end:サブマスク値が1-32範囲にない");
				return false;
			}
		}
    	log.debug("isIpv4 return=true");
		return true;
	}

	/**
	 * メソッド名 : isIpv6
	 * 機能概要 : IPv6フォーマットチェック
	 * @param val 検査対象の文字列
	 * @return 入力値が正しければ <code>true</code>
	 */
	private static boolean isIpv6(String val) {

    	log.debug("isIpv6 start:Input=" + val);

    	String [] s_val = val.split("/");
		if (s_val.length < 1 || s_val.length > 2) {
	    	log.debug("isIpv6 end:/数不正1");
			return false;
		}
		if (val.contains("/") && s_val.length != 2) {
	    	log.debug("isIpv6 end:/数不正2");
			return false;
		}

		if (!(s_val[0].matches("^[:\\.0-9a-fA-F]*$"))) {
	    	log.debug("isIpv6 end:正規表現チェックNG");
			return false;
		}
		if (null == IPAddressUtil.textToNumericFormatV6(s_val[0])) {
	    	log.debug("isIpv6 end:textToNumericFormatV6=NG");
			return false;
		}

		// マスク値が存在する場合
		if (s_val.length == 2) {
	    	log.debug("サブマスク値が存在する");
	    	// マスク値がnullまたは空文字の時、false返却
	    	if (GenericValidator.isBlankOrNull(s_val[1])) {
		    	log.debug("isIpv6 end:サブマスク値がnullまたは空文字");
	    		return false;
	    	}
			if (!(s_val[1].matches("^([0-9]{1,3})$"))) {
		    	log.debug("isIpv6 end:サブマスク値が数字でない");
				return false;
			}
			if (Integer.parseInt(s_val[1]) < 3
					|| Integer.parseInt(s_val[1]) > 128) {
		    	log.debug("isIpv6 end:サブマスク値が3-128範囲にない");
				return false;
			}
		}
    	log.debug("isIpv6 return=true");
		return true;
	}

	/**
	 * メソッド名 : isIpv4Range
	 * 機能概要 : IPv4アドレス前<code><<code/>IPv4アドレス後に一致するか判定
	 * @param val1 IpV4アドレス前
	 * @param val2 IPv4アドレス後
	 * @return 条件に一致すれば <code>true</code>
	 */
	private static boolean isIpv4Range(String val1, String val2) {

		if (log.isDebugEnabled()) {
	    	log.debug("isIpv4Range start");
	    	log.debug("val1=" + val1);
	    	log.debug("val2=" + val2);
		}

		if (Double.compare(toIpv4Double(val1), toIpv4Double(val2)) < 0) {
	    	log.debug("isIpv4Range return=true");
			return true;
		}
    	log.debug("isIpv4Range return=false");
		return false;
	}

	/**
	 * メソッド名 : isIpv6Range
	 * 機能概要 : IPv6アドレス前<code><<code/>IPv6アドレス後に一致するか判定
	 * @param val1 IpV6アドレス前
	 * @param val2 IPv6アドレス後
	 * @return 条件に一致すれば <code>true</code>
	 */
	private static boolean isIpv6Range(String val1, String val2) {

		if (log.isDebugEnabled()) {
	    	log.debug("isIpv6Range start");
	    	log.debug("val1=" + val1);
	    	log.debug("val2=" + val2);
		}

    	int[] ad1 = toIpv6IntArray(val1);
		int[] ad2 = toIpv6IntArray(val2);
		boolean flg = false;
		for (int i = 0; i < ad1.length; i++) {
	    	log.debug("同値の間繰り返す");
			if (ad1[i] > ad2[i]) {
		    	log.debug("val1 > val2 確定");
				break;
			} else if (ad1[i] < ad2[i]) {
		    	log.debug("val1 < val2 確定");
				flg = true;
				break;
			}
		}
    	log.debug("isIpv6Range return=" + flg);
		return flg;
	}

	/**
	 * メソッド名 : toIpv4Double
	 * 機能概要 : IPv4アドレスを数値(Double)に変換する
	 * @param val IpV4アドレス
	 * @return 数値化したIPv4アドレス
	 */
	private static Double toIpv4Double(String val) {

    	log.debug("toIpv4Double start:val=" + val);

    	String[] tp = val.split("\\.");
		Double addressnum =
			(((Double.valueOf(tp[0]) * 256) + Double.valueOf(tp[1])) * 256
					+ Double.valueOf(tp[2])) * 256 + Double.valueOf(tp[3]);

    	log.debug("toIpv4Double return=" + addressnum);
		return addressnum;
	}

	/**
	 * メソッド名 : toIpv6IntArray
	 * 機能概要 : IPv6アドレスを数値(int)配列に変換する
	 * @param val IpV6アドレス
	 * @return 数値化したIPv6アドレス配列
	 */
	private static int[] toIpv6IntArray(String val) {

    	log.debug("toIpv6IntArray start:val=" + val);

    	int max_len = 8;
		int chgnum[] = new int[8];

		String[] tp = val.split(":");
		int len = tp.length;
		if (isIpv4(tp[len - 1])) {
		   	log.debug("最終32byteがIpV4表現");
			max_len = 7;
		}
		int j = 0;
		for (int i = 0; i + j < 8; i++) {
		   	log.debug("IPv6ループ開始");
			if (isIpv4(tp[i])) {
			   	log.debug("IpV4フォーマット");
				String[] ipv4 = tp[i].split("\\.");
				chgnum[i + j] = (Integer.parseInt(ipv4[0]) * 256)
								+ Integer.parseInt(ipv4[1]);
				i++;
				chgnum[i + j] = (Integer.parseInt(ipv4[2]) * 256)
								+ Integer.parseInt(ipv4[3]);
			} else if (tp[i].isEmpty()) {
			   	log.debug("isEmpty=true");
	    		for (j = 0; j < (max_len - len + 1); j++) {
				   	log.debug("省略0の復元ループ");
	    			chgnum[i + j] = 0;
	    		}
	    		j--;
	    	} else {
			   	log.debug("16進数");
	    		chgnum[i + j] = Integer.parseInt(tp[i], 16);
	    	}
		}
		log.debug("toIpv6IntArray end:int配列返却");
		return chgnum;
	}

}
