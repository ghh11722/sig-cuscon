/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectServiceGroupListOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/20     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo;

import java.io.Serializable;

/**
 * クラス名 : SelectServiceGroupListOutput
 * 機能概要 : サービスグループ一覧データクラス
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/20
 *          新規作成
 * @see
 */
public class SelectServiceGroupList implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = -9095826283096481401L;
	// Name
	private String name = null;
	// Members
	private int members = 0;
	// Services
	private String services = null;

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : membersのGetterメソッド
	 * 機能概要 : membersを取得する。
	 * @return members
	 */
	public int getMembers() {
		return members;
	}
	/**
	 * メソッド名 : membersのSetterメソッド
	 * 機能概要 : membersをセットする。
	 * @param members
	 */
	public void setMembers(int members) {
		this.members = members;
	}
	/**
	 * メソッド名 : servicesのGetterメソッド
	 * 機能概要 : servicesを取得する。
	 * @return services
	 */
	public String getServices() {
		return services;
	}
	/**
	 * メソッド名 : servicesのSetterメソッド
	 * 機能概要 : servicesをセットする。
	 * @param services
	 */
	public void setServices(String services) {
		this.services = services;
	}
}
