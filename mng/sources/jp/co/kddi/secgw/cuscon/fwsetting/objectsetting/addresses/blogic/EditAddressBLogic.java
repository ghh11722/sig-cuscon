/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditAddressBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/06     kinoshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.blogic;


import org.apache.log4j.Logger;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.dto.EditAddressInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.dto.EditAddressOutput;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicResult;

/**
 * クラス名 : EditAddressBLogic
 * 機能概要 :
 * 備考 :
 * @author kinoshtt
 * @version 1.0 kinoshtt
 *          Created 2010/03/06
 *          新規作成
 * @see
 */
public class EditAddressBLogic implements BLogic<EditAddressInput> {

	private Logger log = Logger.getLogger(
	"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.blogic.EditAddressBLogic");

	/**
	 * メソッド名 : EditAddressBLogic
	 * 機能概要 :
	 * @param params
	 * @return
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(EditAddressInput params) {

		log.debug("EditAddressBLogic処理開始");
		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		result.setResultString("failure");

		// アウトプットオブジェクト生成
		EditAddressOutput out = new EditAddressOutput();

		// インデックス取得変数定義
		int index = params.getIndex();

		// アウトプット情報設定
		out.setName(params.getAddressList().get(index).getName());
		out.setType(params.getAddressList().get(index).getType());
		out.setAddress(params.getAddressList().get(index).getAddress());

		// レスポンス返却
		result.setResultObject(out);
		result.setResultString("success");
	    log.debug("EditAddressBLogic処理終了");
		return result;
	}
}
