/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CommitFlg.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/04/06     hiroyasu                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.common.vo;

/**
 * クラス名 : CommitFlg
 * 機能概要 :
 * 備考 :
 * @author hiroyasu
 * @version 1.0 hiroyasu
 *          Created 2010/04/06
 *          新規作成
 * @see
 */
public class CommitFlg {
	private boolean commtFlg;

	/**
	 * メソッド名 : commtFlgのSetterメソッド
	 * 機能概要 : commtFlgをセットする。
	 * @param commtFlg
	 */
	public void setCommtFlg(boolean commtFlg) {
		this.commtFlg = commtFlg;
	}

	/**
	 * メソッド名 : commtFlgのGetterメソッド
	 * 機能概要 : commtFlgを取得する。
	 * @return commtFlg
	 */
	public boolean isCommtFlg() {
		return commtFlg;
	}
}
