/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ShowSourceAddressBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/04     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.rollback.blogic;

import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.common.AddressMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectObjectList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto.ShowAddressOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto.ShowInput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : ShowSourceAddressBLogic
 * 機能概要 :SourceAddress項目選択時の処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/04
 *          新規作成
 * @see
 */
public class ShowSourceAddressBLogic implements BLogic<ShowInput> {

	/**
     * QueryDAO。
     * Springによりインスタンス生成され設定される。
     */
    private QueryDAO queryDAO = null;
    private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.rollback.blogic.ShowSourceAddressBLogic");
    private MessageAccessor messageAccessor = null;
    /**
	 * メソッド名 :execute
	 * 機能概要 : 選択出所アドレス、アドレスマスタをアウトプットクラスに設定する。
	 * @param param ShowInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(ShowInput param) {

		log.debug("ShowSourceAddressBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");
        // エラーメッセージ
        BLogicMessages messages = new BLogicMessages();

        // 選択したリストからオブジェクトを取得する。
        SelectPolicyList selectPolicy = param.getPolicyList().get(param.getIndex());

        AddressMasterList addressList = new AddressMasterList();

        List<SelectObjectList> addressMasterList = null;

        try {
	        // アドレスマスタリストを取得する。
	        addressMasterList = addressList.getAddressMasterList(
	        			queryDAO, param.getUvo(), param.getSelectGeneNo());
        } catch(Exception e) {
        	// トランザクションロールバック
        	TransactionUtil.setRollbackOnly();
        	// DBアクセスエラー
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"EK071002", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK070012"));
        	result.setErrors(messages);
        	result.setResultString("failure");
        	return result;
        }

        // ビジネスロジックの出力クラスに結果を設定する
        ShowAddressOutput out = new ShowAddressOutput();
        out.setAddressList(selectPolicy.getSourceAddressList());
        out.setAddressMasterList(addressMasterList);
        out.setAddressName(out.getAddressList().get(0).getName());

        result.setResultObject(out);
        result.setResultString("success");

        log.debug("ShowSourceAddressBLogic処理終了");
        return result;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}
	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}

