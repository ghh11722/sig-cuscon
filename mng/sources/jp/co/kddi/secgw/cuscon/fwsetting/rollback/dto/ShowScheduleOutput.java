/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ShowScheduleOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/04     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.vo.Schedules;

/**
 * クラス名 : ShowScheduleOutput
 * 機能概要 :Schedule項目選択時の出力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/04
 *          新規作成
 * @see
 */
public class ShowScheduleOutput {

	// スケジュール名
	private String scheduleName = null;

	// スケジュールリスト
	private List<Schedules> scheduleList = null;

	/**
	 * メソッド名 : scheduleNameのGetterメソッド
	 * 機能概要 : scheduleNameを取得する。
	 * @return scheduleName
	 */
	public String getScheduleName() {
		return scheduleName;
	}

	/**
	 * メソッド名 : scheduleNameのSetterメソッド
	 * 機能概要 : scheduleNameをセットする。
	 * @param scheduleName
	 */
	public void setScheduleName(String scheduleName) {
		this.scheduleName = scheduleName;
	}

	/**
	 * メソッド名 : scheduleListのGetterメソッド
	 * 機能概要 : scheduleListを取得する。
	 * @return scheduleList
	 */
	public List<Schedules> getScheduleList() {
		return scheduleList;
	}

	/**
	 * メソッド名 : scheduleListのSetterメソッド
	 * 機能概要 : scheduleListをセットする。
	 * @param scheduleList
	 */
	public void setScheduleList(List<Schedules> scheduleList) {
		this.scheduleList = scheduleList;
	}
}
