/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddFiltersInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto;

import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;

/**
 * クラス名 : AddFiltersInput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class AddFiltersInput {

	// UVO
	private CusconUVO uvo = null;
	// アプリケーショングループ名
	private String name = null;
	// セッション保持しているSelectedList(セッションデータ)
	private List<SelectedAplInfo> sessionAplList = null;
    // 選択フィルタリスト
	private List<SelectedAplInfo> selectedAplList = null;
	// 画面遷移種別(登録/更新)
	private String displayType = null;
	// チェック選択アプリケーショングループインデックス
	private String index = null;
	// アプリケーションフィルタリスト
	private List<SelectApplicationFilterList> filterList4Add = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}
	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : sessionAplListのGetterメソッド
	 * 機能概要 : sessionAplListを取得する。
	 * @return sessionAplList
	 */
	public List<SelectedAplInfo> getSessionAplList() {
		return sessionAplList;
	}
	/**
	 * メソッド名 : sessionAplListのSetterメソッド
	 * 機能概要 : sessionAplListをセットする。
	 * @param sessionAplList
	 */
	public void setSessionAplList(List<SelectedAplInfo> sessionAplList) {
		this.sessionAplList = sessionAplList;
	}
	/**
	 * メソッド名 : selectedAplListのGetterメソッド
	 * 機能概要 : selectedAplListを取得する。
	 * @return selectedAplList
	 */
	public List<SelectedAplInfo> getSelectedAplList() {
		return selectedAplList;
	}
	/**
	 * メソッド名 : selectedAplListのSetterメソッド
	 * 機能概要 : selectedAplListをセットする。
	 * @param selectedAplList
	 */
	public void setSelectedAplList(List<SelectedAplInfo> selectedAplList) {
		this.selectedAplList = selectedAplList;
	}
	/**
	 * メソッド名 : displayTypeのGetterメソッド
	 * 機能概要 : displayTypeを取得する。
	 * @return displayType
	 */
	public String getDisplayType() {
		return displayType;
	}
	/**
	 * メソッド名 : displayTypeのSetterメソッド
	 * 機能概要 : displayTypeをセットする。
	 * @param displayType
	 */
	public void setDisplayType(String displayType) {
		this.displayType = displayType;
	}
	/**
	 * メソッド名 : indexのGetterメソッド
	 * 機能概要 : indexを取得する。
	 * @return index
	 */
	public String getIndex() {
		return index;
	}
	/**
	 * メソッド名 : indexのSetterメソッド
	 * 機能概要 : indexをセットする。
	 * @param index
	 */
	public void setIndex(String index) {
		this.index = index;
	}
	/**
	 * メソッド名 : filterList4AddのGetterメソッド
	 * 機能概要 : filterList4Addを取得する。
	 * @return filterList4Add
	 */
	public List<SelectApplicationFilterList> getFilterList4Add() {
		return filterList4Add;
	}
	/**
	 * メソッド名 : filterList4AddのSetterメソッド
	 * 機能概要 : filterList4Addをセットする。
	 * @param filterList4Add
	 */
	public void setFilterList4Add(List<SelectApplicationFilterList> filterList4Add) {
		this.filterList4Add = filterList4Add;
	}
}
