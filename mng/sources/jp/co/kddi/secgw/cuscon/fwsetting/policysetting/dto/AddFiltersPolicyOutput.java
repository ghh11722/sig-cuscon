/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddFiltersPolicyOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto;

import java.util.List;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationList4Search;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectFilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;

/**
 * クラス名 : AddFiltersPolicyOutput
 * 機能概要 : フィルター設定画面出力データクラス
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class AddFiltersPolicyOutput {

	// ポリシー名
	private String name = null;
	// フィルタリスト
	private SelectFilterMasterList filterList = null;
	// アプリケーションリスト
	private List<SelectApplicationList4Search> applicationList = null;
    // 選択アプリケーションリスト
	private List<SelectedAplInfo> selectedAplList = null;
	// アプリケーションフィルタリスト
	private List<SelectApplicationFilterList> filterList4Add = null;
	// Search入力情報
	private String search = null;

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : filterListのGetterメソッド
	 * 機能概要 : filterListを取得する。
	 * @return filterList
	 */
	public SelectFilterMasterList getFilterList() {
		return filterList;
	}
	/**
	 * メソッド名 : filterListのSetterメソッド
	 * 機能概要 : filterListをセットする。
	 * @param filterList
	 */
	public void setFilterList(SelectFilterMasterList filterList) {
		this.filterList = filterList;
	}
	/**
	 * メソッド名 : applicationListのGetterメソッド
	 * 機能概要 : applicationListを取得する。
	 * @return applicationList
	 */
	public List<SelectApplicationList4Search> getApplicationList() {
		return applicationList;
	}
	/**
	 * メソッド名 : applicationListのSetterメソッド
	 * 機能概要 : applicationListをセットする。
	 * @param applicationList
	 */
	public void setApplicationList(List<SelectApplicationList4Search> applicationList) {
		this.applicationList = applicationList;
	}
	/**
	 * メソッド名 : selectedAplListのGetterメソッド
	 * 機能概要 : selectedAplListを取得する。
	 * @return selectedAplList
	 */
	public List<SelectedAplInfo> getSelectedAplList() {
		return selectedAplList;
	}
	/**
	 * メソッド名 : selectedAplListのSetterメソッド
	 * 機能概要 : selectedAplListをセットする。
	 * @param selectedAplList
	 */
	public void setSelectedAplList(List<SelectedAplInfo> selectedAplList) {
		this.selectedAplList = selectedAplList;
	}
	/**
	 * メソッド名 : filterList4AddのGetterメソッド
	 * 機能概要 : filterList4Addを取得する。
	 * @return filterList4Add
	 */
	public List<SelectApplicationFilterList> getFilterList4Add() {
		return filterList4Add;
	}
	/**
	 * メソッド名 : filterList4AddのSetterメソッド
	 * 機能概要 : filterList4Addをセットする。
	 * @param filterList4Add
	 */
	public void setFilterList4Add(List<SelectApplicationFilterList> filterList4Add) {
		this.filterList4Add = filterList4Add;
	}
	/**
	 * メソッド名 : searchのGetterメソッド
	 * 機能概要 : searchを取得する。
	 * @return search
	 */
	public String getSearch() {
		return search;
	}
	/**
	 * メソッド名 : searchのSetterメソッド
	 * 機能概要 : searchをセットする。
	 * @param search
	 */
	public void setSearch(String search) {
		this.search = search;
	}
}
