/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : PolicyRulesList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.common;

import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.SecurityRules;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : PolicyRulesList
 * 機能概要 : ポリシー設定画面の共通処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class PolicyRulesList {

	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.policysetting.common.PolicyRulesList");
	private static MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 */
	public PolicyRulesList() {
		log.debug("PolicyRulesListコンストラクタ2処理開始");
		log.debug("PolicyRulesListコンストラクタ2処理開始");
	}

	/**
	 * メソッド名 : checkPolicyName
	 * 機能概要 : ルール名が使用されているかのチェックを行う。
	 * @param base セキュリティポリシーデータクラス
	 * @return boolean チェック結果
	 * @throws Exception
	 */
	public boolean checkPolicyName(UpdateDAO updateDAO,
			QueryDAO queryDAO, CusconUVO uvo, SecurityRules base) throws Exception {

		log.debug("checkPolicyName処理開始");
		List<SecurityRules> policyList = null;
		try {
			policyList = queryDAO.executeForObjectList("PolicyRulesList-1", base);
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		log.fatal(messageAccessor.getMessage("FK051005", objectgArray, uvo));
    		throw e;
		}

		// 検索結果が存在する、かつ削除フラグが3でない場合
    	if(!policyList.isEmpty() && policyList.get(0).getModFlg() != CusconConst.DEL_NUM) {
    		log.debug("既に存在するルール名");
    		return false;
    	}
    	// 検索結果が存在する、かつ削除フラグが3の場合
    	if(!policyList.isEmpty() && policyList.get(0).getModFlg() == CusconConst.DEL_NUM) {
    		log.debug("削除フラグが3のデータ削除する。");
    		base.setModFlg(CusconConst.DEL_NUM);
    		try {
	    		// データを削除する。
	    		updateDAO.execute("PolicyRulesList-2", base);
    		} catch(Exception e) {
        		// トランザクションロールバック
        		TransactionUtil.setRollbackOnly();
        		// DBアクセスエラー
        		Object[] objectgArray = {e.getMessage()};
        		log.fatal(messageAccessor.getMessage("FK051003", objectgArray, uvo));
        		throw e;
        	}
    	}
    	log.debug("checkPolicyName処理終了:true");

    	return true;
	}

	/**
	 * メソッド名 : updateModFlg
	 * 機能概要 : 変更フラグが1でない場合、変更フラグを2に更新する。<br>
	 *            また、更新情報のシーケンス番号を返却する。
	 * @param base 検索条件
	 * @return int 更新レコードのシーケンス番号
	 * @throws Exception
	 */
	public int updateModFlg(UpdateDAO updateDAO, QueryDAO queryDAO,
						CusconUVO uvo, SecurityRules base) throws Exception {
		log.debug("updateModFlg処理開始");

		// 更新したレコードの情報
		List<SecurityRules> policyList = null;
		try {
			// 変更フラグが1でない場合、変更フラグを2に更新する。
			updateDAO.execute("PolicyRulesList-3", base);

			// 更新したレコードの情報を取得する。
			policyList =
				queryDAO.executeForObjectList("PolicyRulesList-1", base);
		} catch(Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		log.fatal(messageAccessor.getMessage(
    									"FK051003", objectgArray, uvo));
    		throw e;
    	}

		log.debug("updateModFlg処理終了:policyList.get(0).getSeqNo() = " +
												policyList.get(0).getSeqNo());
		return policyList.get(0).getSeqNo();
	}
	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor msgAcc){
		log.debug("setMessageAccessor処理開始");
	    setMsgAcc(msgAcc);
	    log.debug("setMessageAccessor処理終了");
	}

	/**
	 * メソッド名 : setMsgAcc
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public static void setMsgAcc(MessageAccessor messageAccessor) {
		log.debug("setMsgAcc処理開始");
		PolicyRulesList.messageAccessor = messageAccessor;
		log.debug("setMsgAcc処理終了");
	}

	/**
	 * メソッド名 : getMessage
	 * 機能概要 : messageAccessorをゲットする。
	 * @return MessageAccessor
	 */
	public static MessageAccessor getMessage() {
		log.debug("getMessage処理");
		return messageAccessor;
	}
}
