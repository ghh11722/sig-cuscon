/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectAddressGroupList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/19     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo;

import java.io.Serializable;

/**
 * クラス名 : SelectAddressGroupList
 * 機能概要 : アドレスグループ一覧データクラス
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/19
 *          新規作成
 * @see
 */
public class SelectAddressGroupList implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 6621495476494369219L;
	// Name
	private String name = null;
	// Members
	private int members = 0;
	// Address
	private String address = null;
	// チェック情報（画面にチェックボックスON/OFFがある場合に使用 ON:true/OFF:false）
	private boolean chkFlg = false;

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : membersのGetterメソッド
	 * 機能概要 : membersを取得する。
	 * @return members
	 */
	public int getMembers() {
		return members;
	}
	/**
	 * メソッド名 : membersのSetterメソッド
	 * 機能概要 : membersをセットする。
	 * @param members
	 */
	public void setMembers(int members) {
		this.members = members;
	}
	/**
	 * メソッド名 : addressのGetterメソッド
	 * 機能概要 : addressを取得する。
	 * @return address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * メソッド名 : addressのSetterメソッド
	 * 機能概要 : addressをセットする。
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * メソッド名 : chkFlgのGetterメソッド
	 * 機能概要 : chkFlgを取得する。
	 * @return chkFlg
	 */
	public boolean isChkFlg() {
		return chkFlg;
	}
	/**
	 * メソッド名 : chkFlgのSetterメソッド
	 * 機能概要 : chkFlgをセットする。
	 * @param chkFlg
	 */
	public void setChkFlg(boolean chkFlg) {
		this.chkFlg = chkFlg;
	}
}
