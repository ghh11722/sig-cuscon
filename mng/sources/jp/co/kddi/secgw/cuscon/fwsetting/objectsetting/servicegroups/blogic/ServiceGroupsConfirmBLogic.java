/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ServiceGroupsConfirmBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/08     morisou                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.servicegroups.blogic;

import java.util.ArrayList;
import java.util.List;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectServiceGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.servicegroups.dto.ServiceGroupsConfirmInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.servicegroups.dto.ServiceGroupsOutput;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicResult;
import org.apache.log4j.Logger;

/**
 * クラス名 : ServiceGroupsConfirmBLogic
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/08
 *          新規作成
 * @see
 */
public class ServiceGroupsConfirmBLogic implements BLogic<ServiceGroupsConfirmInput> {

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.servicegroups.blogic.ServiceGroupsConfirmBLogic");

	/**
	 * メソッド名 :execute
	 * 機能概要 :サービスグループ削除確認対象表示処理を行う。
	 * @param param ServiceGroupsConfirmInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(ServiceGroupsConfirmInput param) {

		log.debug("ServiceGroupsConfirmBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");

		// アウトプットデータクラスに設定する。
        ServiceGroupsOutput out = new ServiceGroupsOutput();
        List<SelectServiceGroupList> serviceGroupList = new ArrayList<SelectServiceGroupList>();

        String[] indexList = param.getCheckIndex().split(",");

        // 選択チェック分繰り返す。
        for(int i=0; i< indexList.length; i++) {
        	log.debug("選択チェック分繰り返す。:" + i);
        	serviceGroupList.add(param.getServiceList().get(Integer.parseInt(indexList[i])));
        }

	    out.setServiceGroupList(serviceGroupList);

	    result.setResultObject(out);
	    result.setResultString("success");

	    log.debug("ServiceGroupsConfirmBLogic処理終了");
		return result;
	}
}
