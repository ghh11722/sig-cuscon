/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddressGroupsConfirmInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/08     morisou                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.dto;

import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectAddressGroupList;

/**
 * クラス名 : AddressGroupsConfirmInput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/08
 *          新規作成
 * @see
 */
public class AddressGroupsConfirmInput {

	// CusconUVO
	private CusconUVO uvo = null;
	// チェック選択アドレスグループインデックス
	private String checkIndex = null;
	// アドレスグループリスト
	private List<SelectAddressGroupList> addressGroupList = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}
	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}
	/**
	 * メソッド名 : checkIndexのGetterメソッド
	 * 機能概要 : checkIndexを取得する。
	 * @return checkIndex
	 */
	public String getCheckIndex() {
		return checkIndex;
	}
	/**
	 * メソッド名 : checkIndexのSetterメソッド
	 * 機能概要 : checkIndexをセットする。
	 * @param checkIndex
	 */
	public void setCheckIndex(String checkIndex) {
		this.checkIndex = checkIndex;
	}
	/**
	 * メソッド名 : addressGroupListのGetterメソッド
	 * 機能概要 : addressGroupListを取得する。
	 * @return addressGroupList
	 */
	public List<SelectAddressGroupList> getAddressGroupList() {
		return addressGroupList;
	}
	/**
	 * メソッド名 : addressGroupListのSetterメソッド
	 * 機能概要 : addressGroupListをセットする。
	 * @param addressGroupList
	 */
	public void setAddressGroupList(List<SelectAddressGroupList> addressGroupList) {
		this.addressGroupList = addressGroupList;
	}
}
