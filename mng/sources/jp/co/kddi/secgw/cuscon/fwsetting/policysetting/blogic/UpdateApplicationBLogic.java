/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateApplicationBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/04     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic;

import java.util.ArrayList;
import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.ApplicationFilters;
import jp.co.kddi.secgw.cuscon.common.vo.ApplicationGroups;
import jp.co.kddi.secgw.cuscon.common.vo.ApplicationLink;
import jp.co.kddi.secgw.cuscon.common.vo.ApplicationsMaster;
import jp.co.kddi.secgw.cuscon.common.vo.SecurityRules;
import jp.co.kddi.secgw.cuscon.fwsetting.common.PolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.common.AplFltGrpList;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.PolicyOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.UpdateApplicationInput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;
import org.apache.log4j.Logger;

/**
 * クラス名 : UpdateApplicationBLogic
 * 機能概要 : Application項目編集画面にて「OK」押下時の処理を行う。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/04
 *          新規作成
 * @see
 */
public class UpdateApplicationBLogic implements BLogic<UpdateApplicationInput> {

	// オブジェクトタイプ定数
	private static final int APL = 0;    // アプリケーションオブジェクトマスタ
	private static final int GRP = 1;    // アプリケーショングループオブジェクト
	private static final int FLT = 2;    // アプリケーションフィルタオブジェクト

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;

	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon." +
	"fwsetting.policysetting.blogic.UpdateApplicationBLogic");

	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 : 選択アプリケーションを更新する。
	 * @param param Application編集画面入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(UpdateApplicationInput param) {
		log.debug("UpdateApplicationBLogic処理開始");

		int seqNo = 0;                                     // ポリシーのシーケンス番号
		int modFlg = 0;                                    // ポリシーの修正フラグ
		String[] selectAplList;                            // 選択中アプリケーションリスト
		String[] selectTypeList;                           // 選択中タイプリスト
		List<SecurityRules> selectList = null;             // ポリシー情報
		List<SelectedAplInfo> selectedAplInfoList = null;  // 編集中policyに紐づくアプリケーション情報

		String vsysId = null;
		BLogicResult result = new BLogicResult();
		result.setResultString("failure");

		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();

//2011/10/31 add start inoue@prosite any/any対応
        // anyが選択された場合
        if(param.getRadio() == 0) {
    		// ポリシーリストクラスを生成する。
            PolicyList selrule = new PolicyList();
            List<SelectPolicyList> selpolicyList = null;
            try {
    	        // 編集中のポリシー情報を取得する。
            	selpolicyList = selrule.getPolicylist_SelPolicy(queryDAO, param.getUvo(),
    	        				CusconConst.ZERO_GENE, param.getName());
            } catch(Exception e) {
    			// 例外
            	Object[] objectgArray = {e.getMessage()};
            	log.error(messageAccessor.getMessage(
            			"EK051001", objectgArray, param.getUvo()));
            	messages.add("message", new BLogicMessage("DK050043"));
    			result.setErrors(messages);
    			result.setResultString("failure");
            	return result;
    		}
            //	サービスにapplication-defaultが設定されているか
            if ( selpolicyList.get(0).getService().equals(CusconConst.APPLICATION_DEFAULT) ){
            	messages.add("message", new BLogicMessage("DK050046"));
    			result.setErrors(messages);
    			result.setResultString("failure");
    			return result;
            }
        }
//2011/10/31 add end inoue@prosite any/any対応

		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = param.getUvo();
		vsysId = uvo.getVsysId();


		/***********************************************************************
		 *  ①DBからセキュリティポリシー情報を取得（1件取得）
		 **********************************************************************/
		// ポリシー情報取得条件セット
		SecurityRules securityRules = new SecurityRules();
		securityRules.setVsysId(vsysId);
		securityRules.setGenerationNo(CusconConst.ZERO_GENE);
		securityRules.setModFlg(CusconConst.DEL_NUM);
		securityRules.setName(param.getName());

		selectList = null;
		try {
			// ポリシー情報取得
			selectList = queryDAO.executeForObjectList(
					"MovePolicy-2", securityRules);
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK051004", objectgArray, param.getUvo()));
			messages.add("message", new BLogicMessage("DK050021"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

		// 編集中ポリシーのシーケンス番号取得
		seqNo = selectList.get(0).getSeqNo();
		modFlg = selectList.get(0).getModFlg();


		/***********************************************************************
		 *  ②Application/Filter/Group取得共通処理
		 **********************************************************************/
		// 編集中ポリシーに紐づくアプリケーション情報を取得
		AplFltGrpList aplFltGrpList = new AplFltGrpList();
		selectedAplInfoList = new ArrayList<SelectedAplInfo>();

		try {
			selectedAplInfoList =
				aplFltGrpList.getAplFltGrpList(queryDAO, uvo, param.getName());
		} catch (Exception e){
			// 例外
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("EK051001", objectgArray, param.getUvo()));
			messages.add("message", new BLogicMessage("DK050021"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}


		/***********************************************************************
		 *  ③差分比較処理（差分があれば登録）
		 **********************************************************************/
		// 画面情報取り出し
		selectAplList = param.getSelectAplList();      // 選択中アプリ
		selectTypeList = param.getSelectTypeList();    // 選択中アプリのタイプ

		// ラジオボタン選択状態がanyの場合
		if (param.getRadio() == 0) {
			log.debug("ラジオボタン選択状態がanyの場合");
			try {
				// 編集中のポリシーのアプリケーションリンク情報を削除（DELETE）
				updateDAO.execute("DelPolicyBLogic-8", seqNo);

				// DB更新用情報作成（T_SecurityRules更新用）
				SecurityRules base = new SecurityRules();
				base.setVsysId(param.getUvo().getVsysId());
				base.setGenerationNo(CusconConst.ZERO_GENE);
				base.setName(param.getName());
				base.setModFlg(CusconConst.MOD_NUM);

				// 変更フラグが1でない場合
				if(modFlg != CusconConst.ADD_NUM) {
					log.debug("ルールの変更フラグが「追加」でない場合、変更フラグを更新");

					// 変更フラグ更新する。
					updateDAO.execute("PolicyRulesList-3", base);
				}

			} catch (Exception e) {
				// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
				// DBアクセスエラー
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FK051004", objectgArray, param.getUvo()));
				messages.add("message", new BLogicMessage("DK050021"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}

			// ラジオボタン選択状態がanyではない場合
		} else {
			log.debug("ラジオボタン選択状態がanyでない場合");

			// 選択中アプリがnullの場合、エラーを返却
			if (selectAplList == null) {
				log.debug("選択中アプリがnullの場合、エラーを返却");
				messages.add("message", new BLogicMessage("DK050037"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}

			// 選択中アプリが0件の場合、エラーを返却
			if (selectAplList[0].equals("null") && selectAplList.length == 1) {
				log.debug("選択中アプリが0件の場合、エラーを返却");
				messages.add("message", new BLogicMessage("DK050037"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}

			// 差分比較処理を行い、差分ありの場合、登録処理を行う
			if(diff(selectedAplInfoList, selectAplList)) {
				log.debug("差分ありの場合、登録処理を行う");

				try {
					updateApl(param, seqNo, selectAplList, selectTypeList, modFlg);

				} catch (Exception e) {
					// 例外
					Object[] objectgArray = {e.getMessage()};
					log.error(messageAccessor.getMessage("EK051001", objectgArray, param.getUvo()));
					messages.add("message", new BLogicMessage("DK050021"));
					result.setErrors(messages);
					result.setResultString("failure");
					return result;
				}
			} else {
				log.debug("差分なしの場合、変更フラグのみ書き換える");
				try {
					// DB更新用情報作成（T_SecurityRules更新用）
					SecurityRules base = new SecurityRules();
					base.setVsysId(param.getUvo().getVsysId());
					base.setGenerationNo(CusconConst.ZERO_GENE);
					base.setName(param.getName());
					base.setModFlg(CusconConst.MOD_NUM);

					// 変更フラグが1でない場合
					if(modFlg != CusconConst.ADD_NUM) {
						log.debug("ルールの変更フラグが「追加」でない場合、変更フラグを更新");

						// 変更フラグ更新する。
						updateDAO.execute("PolicyRulesList-3", base);
					}

				} catch (Exception e) {
					// 例外
					Object[] objectgArray = {e.getMessage()};
					log.error(messageAccessor.getMessage("FK051004", objectgArray, param.getUvo()));
					messages.add("message", new BLogicMessage("DK050021"));
					result.setErrors(messages);
					result.setResultString("failure");
					return result;
				}
			}
		}


		/***********************************************************************
		 *  ④ポリシー一覧情報検索
		 **********************************************************************/
		// ポリシーリストクラスを生成する。
		PolicyList rule = new PolicyList();

		// 選択された世代番号のポリシーリストを取得する。
		List<SelectPolicyList> policyList = null;
		try {
			// 選択された世代番号のポリシーリストを取得する。
			policyList = rule.getPolicylist(queryDAO, param.getUvo(),
					CusconConst.ZERO_GENE, CusconConst.POLICY);
		} catch(Exception e) {
			// 例外
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage(
					"EK051001", objectgArray, param.getUvo()));
			messages.add("message", new BLogicMessage("DK050021"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}


		/***********************************************************************
		 *  ⑤結果返却
		 **********************************************************************/
		// セッションクリア
		List<SelectedAplInfo> clearSession = new ArrayList<SelectedAplInfo>();
		clearSession = null;

		selectAplList = null;
		selectTypeList = null;

		// ビジネスロジックの出力クラスに結果を設定する
		PolicyOutput out = new PolicyOutput();
		out.setPolicyList(policyList);
		out.setSessionAplList(clearSession);
		out.setSelectAplList(selectAplList);
		out.setSelectTypeList(selectTypeList);
		result.setResultObject(out);
		result.setResultString("success");

		log.debug("UpdateApplicationBLogic処理終了");
		return result;
	}


	/**
	 * メソッド名 : updateApl
	 * 機能概要 : アプリケーション更新処理
	 * @param param UpdateApplicationInput入力データクラス
	 * @param seqNo シーケンス番号
	 * @param selectAplList 選択中アプリケーションリスト
	 * @param selectTypeList 選択中タイプリスト
	 * @param modFlg ポリシーの修正フラグ
	 */
	private void updateApl(UpdateApplicationInput param, int seqNo, String[] selectAplList,
			String[] selectTypeList, int modFlg) throws Exception {

		log.debug("updateApl処理開始");

		try {
			// 編集中のポリシーのアプリケーションリンク情報を削除（DELETE）
			updateDAO.execute("DelPolicyBLogic-8", seqNo);
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK051003", objectgArray, param.getUvo()));
			throw e;

		}

		// 画面から渡された選択中のアプリケーション情報を取得
		for (int k = 0; k < selectAplList.length; k++) {
			log.debug("画面から渡された選択中のアプリケーション情報取得中:" + k);

			ApplicationLink applicationLink = new ApplicationLink();

			// 画面指定のTypeがアプリケーションの場合
			if (selectTypeList[k].equals(CusconConst.TYPE_APL)) {
				applicationLink.setObjectType(APL);

				log.debug("選択中のアプリケーションのタイプがアプリケーションの場合");

				List<ApplicationsMaster> aplMasterList = null;
				try {
					// シーケンス番号取得
					aplMasterList = queryDAO.executeForObjectList(
							"ApplicationFiltersBLogic-19", selectAplList[k]);
				} catch (Exception e) {
					// トランザクションロールバック
					TransactionUtil.setRollbackOnly();
					// DBアクセスエラー
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage("FK051004", objectgArray, param.getUvo()));
					throw e;
				}

				// 紐づくアプリケーションのシーケンス番号をセット
				applicationLink.setLinkSeqNo(aplMasterList.get(0).getSeqNo());    // リンクのSEQ_NO
			}

			// 画面指定のTypeがアプリケーショングループの場合
			if (selectTypeList[k].equals(CusconConst.TYPE_GRP)) {
				applicationLink.setObjectType(GRP);

				log.debug("選択中のアプリケーションのタイプがグループの場合");

				// シーケンス番号取得
				ApplicationGroups search4Grp = new ApplicationGroups();
				search4Grp.setVsysId(param.getUvo().getVsysId());
				search4Grp.setGenerationNo(CusconConst.ZERO_GENE);
				search4Grp.setModFlg(CusconConst.DEL_NUM);
				search4Grp.setName(selectAplList[k]);

				List<ApplicationGroups> aplGrpList = null;
				try {
					aplGrpList = queryDAO.executeForObjectList(
							"ApplicationGroupsBLogic-12", search4Grp);
				} catch (Exception e) {
					// トランザクションロールバック
					TransactionUtil.setRollbackOnly();
					// DBアクセスエラー
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage("FK051004", objectgArray, param.getUvo()));
					throw e;
				}

				// シーケンス番号セット
				applicationLink.setLinkSeqNo(aplGrpList.get(0).getSeqNo());

			}
			// 画面指定のTypeがアプリケーションフィルタの場合
			if (selectTypeList[k].equals(CusconConst.TYPE_FLT)) {
				applicationLink.setObjectType(FLT);

				log.debug("選択中のアプリケーションのタイプがフィルタの場合");

				// シーケンス番号取得
				ApplicationFilters search4Flt = new ApplicationFilters();
				search4Flt.setVsysId(param.getUvo().getVsysId());
				search4Flt.setGenerationNo(CusconConst.ZERO_GENE);
				search4Flt.setModFlg(CusconConst.DEL_NUM);
				search4Flt.setName(selectAplList[k]);

				List<ApplicationFilters> aplFltList = null;
				try {
					aplFltList = queryDAO.executeForObjectList(
							"ApplicationFiltersBLogic-20", search4Flt);

				} catch(Exception e) {
					// トランザクションロールバック
					TransactionUtil.setRollbackOnly();
					// DBアクセスエラー
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage("FK051004", objectgArray, param.getUvo()));
					throw e;
				}

				// シーケンス番号セット
				applicationLink.setLinkSeqNo(aplFltList.get(0).getSeqNo());

			}
			applicationLink.setSecRlsSecNo(seqNo);

			try {
				// DB更新（リンク情報INSERT）
				updateDAO.execute("UpdateApplicationBLogic-1", applicationLink);
			} catch (Exception e) {
				// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
				// DBアクセスエラー
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage(
						"FK051002", objectgArray, param.getUvo()));
				throw e;
			}

			// DB更新（T_SecurityRules更新）
			SecurityRules base = new SecurityRules();
			base.setVsysId(param.getUvo().getVsysId());
			base.setGenerationNo(CusconConst.ZERO_GENE);
			base.setName(param.getName());
			base.setModFlg(CusconConst.MOD_NUM);

			// 変更フラグが1でない場合
			if(modFlg != CusconConst.ADD_NUM) {

				log.debug("ルールの変更フラグが「追加」でない場合、変更フラグを更新");

				try {
				// 変更フラグ更新する。
				updateDAO.execute("PolicyRulesList-3", base);

				} catch (Exception e) {
					// トランザクションロールバック
					TransactionUtil.setRollbackOnly();
					// DBアクセスエラー
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage(
							"FK051004", objectgArray, param.getUvo()));
					throw e;
				}
			}
		}
		log.debug("updateApl処理終了");
	}


	/**
	 * メソッド名 : diff
	 * 機能概要 : 差分比較処理
	 * @param selectedAplInfoList 編集中policyに紐づくアプリケーション情報
	 * @param selectAplList 選択中アプリケーションリスト
	 * @return chkresult 差分比較結果
	 */
	private boolean diff(List<SelectedAplInfo> selectedAplInfoList, String[] selectAplList) {
		log.debug("diff処理開始");

		/***********************************************************************
		 *  画面の選択中アプリケーション情報とDBから取得したポリシーに紐づく
		 *  アプリケーション情報を比較
		 **********************************************************************/
		boolean chkresult = false;    // 差分比較結果（true:差分あり false:差分なし）

		// 現状のpolicyに紐づくアプリケーション情報が1件もない場合、差分ありと判定
		if (selectedAplInfoList.size() == 0 || selectedAplInfoList == null) {
			log.debug("DBにて選択したルールに紐づくアプリケーション情報がないため、差分ありと判定");

			chkresult = true;

			// policyに紐づくアプリケーションと選択中アプリケーション情報を比較
		} else {
			log.debug("DBにて選択したルールに紐づくアプリケーション情報があるため、画面情報と比較");

			// 両リストのサイズが異なる場合、差分ありと判定
			if (selectedAplInfoList.size() != selectAplList.length) {
				log.debug("両リストのサイズが異なるため、差分ありと判定");

				chkresult = true;

				// 両リストのサイズが同じ場合、それぞれの名称を比較
			} else {
				log.debug("両リストのサイズが同じ場合、それぞれの名称を比較");

				for (int i = 0; i < selectedAplInfoList.size(); i++) {
					log.debug("名称比較中:" + i);

					// policyに紐づくアプリケーション名を取得
					String policyAplName = selectedAplInfoList.get(i).getName();
					log.debug("policyに紐づくアプリケーション名:" + policyAplName);

					// 選択中アプリケーションに同名のアプリケーションがあるか比較
					int chkCount = 0;    // 同名存在チェックカウント
					for (int j = 0; j < selectAplList.length; j++) {
						log.debug("policyに紐づくアプリケーション名が存在するか検索中" + j);

						String selectName = selectAplList[j];

						// 同名称のアプリケーションが存在する場合、チェックカウントを上げる
						if (policyAplName.equals(selectName)) {
							log.debug("同名称のアプリケーションが存在する");

							chkCount ++;
						}
					}
					// 同名称のアプリが存在しない場合、差分ありと判定
					if (chkCount == 0) {
						log.debug("同名称のアプリケーションが1件も存在しないため、差分ありと判定");
						chkresult = true;
					}
				}
			}
		}
		log.debug("差分比較結果:" + chkresult);

		log.debug("diff処理終了");
		return chkresult;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
