/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateZoneInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;

/**
 * クラス名 : UpdateZoneInput
 * 機能概要 :Zone項目編集画面にて「OK」選択時の入力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class UpdateZoneInput {

	// CusconUVO
	private CusconUVO uvo = null;
	// ルール名
	private String name = null;
	// ラジオボタン選択
	private int radio = 0;
	// ゾーンマスタリスト
	private List<String> zoneMasterList = null;
	// チェックボックス選択
	private String checkList = null;


	/**
	 * メソッド名 : checkListのGetterメソッド
	 * 機能概要 : checkListを取得する。
	 * @return checkList
	 */
	public String getCheckList() {
		return checkList;
	}
	/**
	 * メソッド名 : checkListのSetterメソッド
	 * 機能概要 : checkListをセットする。
	 * @param checkList
	 */
	public void setCheckList(String checkList) {
		this.checkList = checkList;
	}
	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}
	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * メソッド名 : radioのGetterメソッド
	 * 機能概要 : radioを取得する。
	 * @return radio
	 */
	public int getRadio() {
		return radio;
	}
	/**
	 * メソッド名 : radioのSetterメソッド
	 * 機能概要 : radioをセットする。
	 * @param radio
	 */
	public void setRadio(int radio) {
		this.radio = radio;
	}
	/**
	 * メソッド名 : zoneMasterListのGetterメソッド
	 * 機能概要 : zoneMasterListを取得する。
	 * @return zoneMasterList
	 */
	public List<String> getZoneMasterList() {
		return zoneMasterList;
	}
	/**
	 * メソッド名 : zoneMasterListのSetterメソッド
	 * 機能概要 : zoneMasterListをセットする。
	 * @param zoneMasterList
	 */
	public void setZoneMasterList(List<String> zoneMasterList) {
		this.zoneMasterList = zoneMasterList;
	}
}
