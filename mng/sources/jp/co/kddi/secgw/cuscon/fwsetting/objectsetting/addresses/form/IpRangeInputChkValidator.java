/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : IpRangeInputChkValidator.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/17     kinoshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.form;

import org.apache.log4j.Logger;

import jp.terasoluna.fw.web.struts.form.MultiFieldValidator;

/**
 * クラス名 : IpRangeInputChkValidator
 * 機能概要 :
 * 備考 :
 * @author kinoshtt
 * @version 1.0 kinoshtt
 *          Created 2010/03/17
 *          新規作成
 * @see
 */
public class IpRangeInputChkValidator implements MultiFieldValidator {

	private Logger log = Logger.getLogger(
	"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.form.IpAddressChkValidator");

	// オブジェクトタイプ定数
	private static final String IP_RANGE = "ip-range";
	private static final String BLANK = "";

	// チェック結果
	boolean result = true;

	/**
	 * メソッド名 :
	 * 機能概要 :アドレス登録および、更新時に画面で入力されたIPレンジの必須入力チェックを行う。
	 *           (ただしIPレンジ選択時のみ)
	 * @param address
	 * @param types
	 * @return result
	 * @see jp.terasoluna.fw.web.struts.form.MultiFieldValidator#validate(java.lang.String, java.lang.String[])
	 */
	@Override
	public boolean validate(String address, String[] fields) {
		log.debug("IpRangeInputChkValidator処理開始");
        String type = fields[0];
        // 画面で選択されたのが「IPレンジ」であるか判定
        if (type.equals(IP_RANGE)) {
    		log.debug("タイプがIPレンジ");
        	// IPレンジが画面入力されているか判定
	        if (address == null || BLANK.equals(address)) {
                // 未入力の場合、falseを設定
	    		log.debug("IPレンジに入力なし");
            	result = false;
            }
        }
		log.debug("IpRangeInputChkValidator処理終了　結果：" + result);
        // チェック結果返却
		return result;
	}
}
