/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditApplicationGroupInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/24     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto;

import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;

/**
 * クラス名 : EditApplicationGroupInput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/24
 *          新規作成
 * @see
 */
public class EditApplicationGroupInput {

	// UVO
	private CusconUVO uvo = null;
	// アプリケーショングループ名
	private String name = null;
	// アプリケーショングループリスト
	private List<SelectApplicationGroupList> applicationGroupList = null;
	// セッション保持しているSelectedList(セッションデータ)
	private List<SelectedAplInfo> sessionAplList = null;
	// インデックス
	private String index = null;
	// 画面遷移種別(登録/更新)
	private String displayType = null;
	// 変更前アプリケーショングループ名
	private String oldName = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}
	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : applicationGroupListのGetterメソッド
	 * 機能概要 : applicationGroupListを取得する。
	 * @return applicationGroupList
	 */
	public List<SelectApplicationGroupList> getApplicationGroupList() {
		return applicationGroupList;
	}
	/**
	 * メソッド名 : applicationGroupListのSetterメソッド
	 * 機能概要 : applicationGroupListをセットする。
	 * @param applicationGroupList
	 */
	public void setApplicationGroupList(List<SelectApplicationGroupList> applicationGroupList) {
		this.applicationGroupList = applicationGroupList;
	}
	/**
	 * メソッド名 : sessionAplListのGetterメソッド
	 * 機能概要 : sessionAplListを取得する。
	 * @return sessionAplList
	 */
	public List<SelectedAplInfo> getSessionAplList() {
		return sessionAplList;
	}
	/**
	 * メソッド名 : sessionAplListのSetterメソッド
	 * 機能概要 : sessionAplListをセットする。
	 * @param sessionAplList
	 */
	public void setSessionAplList(List<SelectedAplInfo> sessionAplList) {
		this.sessionAplList = sessionAplList;
	}
	/**
	 * メソッド名 : indexのGetterメソッド
	 * 機能概要 : indexを取得する。
	 * @return index
	 */
	public String getIndex() {
		return index;
	}
	/**
	 * メソッド名 : indexのSetterメソッド
	 * 機能概要 : indexをセットする。
	 * @param index
	 */
	public void setIndex(String index) {
		this.index = index;
	}
	/**
	 * メソッド名 : displayTypeのGetterメソッド
	 * 機能概要 : displayTypeを取得する。
	 * @return displayType
	 */
	public String getDisplayType() {
		return displayType;
	}
	/**
	 * メソッド名 : displayTypeのSetterメソッド
	 * 機能概要 : displayTypeをセットする。
	 * @param displayType
	 */
	public void setDisplayType(String displayType) {
		this.displayType = displayType;
	}
	/**
	 * メソッド名 : oldNameのGetterメソッド
	 * 機能概要 : oldNameを取得する。
	 * @return oldName
	 */
	public String getOldName() {
		return oldName;
	}
	/**
	 * メソッド名 : oldNameのSetterメソッド
	 * 機能概要 : oldNameをセットする。
	 * @param oldName
	 */
	public void setOldName(String oldName) {
		this.oldName = oldName;
	}
}
