/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectSearch.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     morisou                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * クラス名 : SelectSearch
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class SelectSearchList implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 2134752690241435249L;
	// 検索条件ワード
	private String search = null;
	// 検索条件用カテゴリリスト
	List<Integer> category = new ArrayList<Integer>();
	// 検索条件用サブカテゴリリスト
	List<Integer> subCategory = new ArrayList<Integer>();
	// 検索条件用テクノロジリスト
	List<Integer> technology = new ArrayList<Integer>();
	// 検索条件用リスクリスト
	List<Integer> risk = new ArrayList<Integer>();

	/**
	 * メソッド名 : searchのGetterメソッド
	 * 機能概要 : searchを取得する。
	 * @return search
	 */
	public String getSearch() {
		return search;
	}
	/**
	 * メソッド名 : searchのSetterメソッド
	 * 機能概要 : searchをセットする。
	 * @param search
	 */
	public void setSearch(String search) {
		this.search = search;
	}
	/**
	 * メソッド名 : categoryのGetterメソッド
	 * 機能概要 : categoryを取得する。
	 * @return category
	 */
	public List<Integer> getCategory() {
		return category;
	}
	/**
	 * メソッド名 : categoryのSetterメソッド
	 * 機能概要 : categoryをセットする。
	 * @param category
	 */
	public void setCategory(List<Integer> category) {
		this.category = category;
	}
	/**
	 * メソッド名 : subCategoryのGetterメソッド
	 * 機能概要 : subCategoryを取得する。
	 * @return subCategory
	 */
	public List<Integer> getSubCategory() {
		return subCategory;
	}
	/**
	 * メソッド名 : subCategoryのSetterメソッド
	 * 機能概要 : subCategoryをセットする。
	 * @param subCategory
	 */
	public void setSubCategory(List<Integer> subCategory) {
		this.subCategory = subCategory;
	}
	/**
	 * メソッド名 : technologyのGetterメソッド
	 * 機能概要 : technologyを取得する。
	 * @return technology
	 */
	public List<Integer> getTechnology() {
		return technology;
	}
	/**
	 * メソッド名 : technologyのSetterメソッド
	 * 機能概要 : technologyをセットする。
	 * @param technology
	 */
	public void setTechnology(List<Integer> technology) {
		this.technology = technology;
	}
	/**
	 * メソッド名 : riskのGetterメソッド
	 * 機能概要 : riskを取得する。
	 * @return risk
	 */
	public List<Integer> getRisk() {
		return risk;
	}
	/**
	 * メソッド名 : riskのSetterメソッド
	 * 機能概要 : riskをセットする。
	 * @param risk
	 */
	public void setRisk(List<Integer> risk) {
		this.risk = risk;
	}
	/**
	 * メソッド名 : serialversionuidのGetterメソッド
	 * 機能概要 : serialversionuidを取得する。
	 * @return serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
