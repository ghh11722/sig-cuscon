/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : RollbackForm.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/27     komakiys                初版作成
 * 2010/06/23     oohashij                StartTime、受付状態の追加
 * 2011/06/14     ktakenaka@PROSITE       URLフィルタ,VirusCheck,AntiSpyware項目追加
 * 2013/09/01     kkato@PROSITE           セキュリティルール有効/無効切替対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.rollback.form;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.vo.Schedules;
import jp.co.kddi.secgw.cuscon.common.vo.SecurityRules;
import jp.co.kddi.secgw.cuscon.common.vo.UrlFilters;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectObjectList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationList;
import jp.co.kddi.secgw.cuscon.fwsetting.rollback.vo.SelectShowFilterList;
import jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx;

/**
 * クラス名 : RollbackForm
 * 機能概要 :ロールバックフォーム
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/27
 *          新規作成
 * @version 1.1 oohashij
 *          Created 2010/06/23
 *          StartTime、受付状態の追加
 * @version 1.2 ktakenaka@PROSITE
 *          Created 2011/06/14
 *          URLフィルタ,VirusCheck,AntiSpyware項目追加
 * @see
 */
public class RollbackForm extends ValidatorActionFormEx {

	// シリアルバージョンID
	private static final long serialVersionUID = 1L;

	// ポリシー一覧リスト
	private List<SelectPolicyList> policyList = null;

	// 世代番号、コミット日時リスト
	private List<SecurityRules> generationList = null;

	private int index = 0;
	// 選択世代番号
	private int selectGeneNo = 0;

	// メッセージ(エラー時のみ)
	private String message = null;

	// アプリケーションリスト
	private List<SelectApplicationList> appliList = null;

	// アプリケーショングループリスト
	private List<SelectApplicationGroupList> appliGrpList = null;

	// アプリケーションフィルタリスト
	private List<SelectShowFilterList> appliFilList = null;

	// ルール名
	private String name = null;
	// 備考
	private String description = null;

	// 出所ゾーンリスト
	private List<SelectObjectList> zoneList= null;

	// ゾーンマスタリスト
	private List<String> zoneMasterList = null;

	// アドレス名リスト
	private List<SelectObjectList> addressList = null;

	// アドレスマスタリスト
	private List<SelectObjectList> addressMasterList = null;

	// サービス名リスト
	private List<SelectObjectList> serviceList = null;

	// サービスマスタリスト
	private List<SelectObjectList> serviceMasterList = null;

	// ルール適用フラグ
	private String action = null;

	// 脆弱性の保護フラグ
	private String ids_ips = null;

	// スケジュール名
	private String scheduleName = null;

	// スケジュールリスト
	private List<Schedules> scheduleList = null;

	// サービス名
	private String serviceName = null;

	// アドレス名
	private String addressName = null;

	// ゾーン名
	private String zoneName = null;

	// StartTime
	private String startTime = null;

	// 受付状態
	private String acceptStatus = null;

	// 20110614 ktakenaka@PROSITE add start
	// Webフィルタリスト
	private List<UrlFilters> urlFilterList= null;

	// Webフィルタ名
	private String urlFilterName = null;

	// Webフィルタ適用フラグ
	private int urlfilterDefaultFlg = 0;

	// Webウィルスチェックフラグ
	private String virusCheckFlg = null;

	// スパイウェアチェックフラグ
	private String spywareFlg = null;
	// 20110614 ktakenaka@PROSITE add end

	// 20130901 kkato@PROSITE add start
	// セキュリティルール有効/無効フラグ
	private String disableFlg = null;
	// 20130901 kkato@PROSITE add end

	/**
	 * メソッド名 : zoneNameのGetterメソッド
	 * 機能概要 : zoneNameを取得する。
	 * @return zoneName
	 */
	public String getZoneName() {
		return zoneName;
	}

	/**
	 * メソッド名 : zoneNameのSetterメソッド
	 * 機能概要 : zoneNameをセットする。
	 * @param zoneName
	 */
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	/**
	 * メソッド名 : addressNameのGetterメソッド
	 * 機能概要 : addressNameを取得する。
	 * @return addressName
	 */
	public String getAddressName() {
		return addressName;
	}

	/**
	 * メソッド名 : addressNameのSetterメソッド
	 * 機能概要 : addressNameをセットする。
	 * @param addressName
	 */
	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}

	/**
	 * メソッド名 : serviceNameのGetterメソッド
	 * 機能概要 : serviceNameを取得する。
	 * @return serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}
	/**
	 * メソッド名 : serviceNameのSetterメソッド
	 * 機能概要 : serviceNameをセットする。
	 * @param serviceName
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	/**
	 * メソッド名 : indexのGetterメソッド
	 * 機能概要 : indexを取得する。
	 * @return index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * メソッド名 : indexのSetterメソッド
	 * 機能概要 : indexをセットする。
	 * @param index
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * メソッド名 : policyListのGetterメソッド
	 * 機能概要 : policyListを取得する。
	 * @return policyList
	 */
	public List<SelectPolicyList> getPolicyList() {
		return policyList;
	}

	/**
	 * メソッド名 : policyListのSetterメソッド
	 * 機能概要 : policyListをセットする。
	 * @param policyList
	 */
	public void setPolicyList(List<SelectPolicyList> policyList) {
		this.policyList = policyList;
	}

	/**
	 * メソッド名 : generationListのGetterメソッド
	 * 機能概要 : generationListを取得する。
	 * @return generationList
	 */
	public List<SecurityRules> getGenerationList() {
		return generationList;
	}

	/**
	 * メソッド名 : generationListのSetterメソッド
	 * 機能概要 : generationListをセットする。
	 * @param generationList
	 */
	public void setGenerationList(List<SecurityRules> generationList) {
		this.generationList = generationList;
	}

	/**
	 * メソッド名 : selectGeneNoのGetterメソッド
	 * 機能概要 : selectGeneNoを取得する。
	 * @return selectGeneNo
	 */
	public int getSelectGeneNo() {
		return selectGeneNo;
	}

	/**
	 * メソッド名 : selectGeneNoのSetterメソッド
	 * 機能概要 : selectGeneNoをセットする。
	 * @param selectGeneNo
	 */
	public void setSelectGeneNo(int selectGeneNo) {
		this.selectGeneNo = selectGeneNo;
	}

	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * メソッド名 : appliListのGetterメソッド
	 * 機能概要 : appliListを取得する。
	 * @return appliList
	 */
	public List<SelectApplicationList> getAppliList() {
		return appliList;
	}

	/**
	 * メソッド名 : appliListのSetterメソッド
	 * 機能概要 : appliListをセットする。
	 * @param appliList
	 */
	public void setAppliList(List<SelectApplicationList> appliList) {
		this.appliList = appliList;
	}

	/**
	 * メソッド名 : appliGrpListのGetterメソッド
	 * 機能概要 : appliGrpListを取得する。
	 * @return appliGrpnList
	 */
	public List<SelectApplicationGroupList> getAppliGrpList() {
		return appliGrpList;
	}

	/**
	 * メソッド名 : appliGrpnListのSetterメソッド
	 * 機能概要 : appliGrpnListをセットする。
	 * @param appliGrpnList
	 */
	public void setAppliGrpList(
			List<SelectApplicationGroupList> appliGrpList) {
		this.appliGrpList = appliGrpList;
	}

	/**
	 * メソッド名 : appliFilListのGetterメソッド
	 * 機能概要 : appliFilListを取得する。
	 * @return appliFilList
	 */
	public List<SelectShowFilterList> getAppliFilList() {
		return appliFilList;
	}

	/**
	 * メソッド名 : appliFilListのSetterメソッド
	 * 機能概要 : appliFilListをセットする。
	 * @param appliFilList
	 */
	public void setAppliFilList(List<SelectShowFilterList> appliFilList) {
		this.appliFilList = appliFilList;
	}

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * メソッド名 : descriptionのGetterメソッド
	 * 機能概要 : descriptionを取得する。
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * メソッド名 : descriptionのSetterメソッド
	 * 機能概要 : descriptionをセットする。
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * メソッド名 : zoneListのGetterメソッド
	 * 機能概要 : zoneListを取得する。
	 * @return zoneList
	 */
	public List<SelectObjectList> getZoneList() {
		return zoneList;
	}

	/**
	 * メソッド名 : zoneListのSetterメソッド
	 * 機能概要 : zoneListをセットする。
	 * @param zoneList
	 */
	public void setZoneList(List<SelectObjectList> zoneList) {
		this.zoneList = zoneList;
	}

	/**
	 * メソッド名 : zoneMasterListのGetterメソッド
	 * 機能概要 : zoneMasterListを取得する。
	 * @return zoneMasterList
	 */
	public List<String> getZoneMasterList() {
		return zoneMasterList;
	}

	/**
	 * メソッド名 : zoneMasterListのSetterメソッド
	 * 機能概要 : zoneMasterListをセットする。
	 * @param zoneMasterList
	 */
	public void setZoneMasterList(List<String> zoneMasterList) {
		this.zoneMasterList = zoneMasterList;
	}

	/**
	 * メソッド名 : addressListのGetterメソッド
	 * 機能概要 : addressListを取得する。
	 * @return addressList
	 */
	public List<SelectObjectList> getAddressList() {
		return addressList;
	}

	/**
	 * メソッド名 : addressListのSetterメソッド
	 * 機能概要 : addressListをセットする。
	 * @param addressList
	 */
	public void setAddressList(List<SelectObjectList> addressList) {
		this.addressList = addressList;
	}

	/**
	 * メソッド名 : addressMasterListのGetterメソッド
	 * 機能概要 : addressMasterListを取得する。
	 * @return addressMasterList
	 */
	public List<SelectObjectList> getAddressMasterList() {
		return addressMasterList;
	}

	/**
	 * メソッド名 : addressMasterListのSetterメソッド
	 * 機能概要 : addressMasterListをセットする。
	 * @param addressMasterList
	 */
	public void setAddressMasterList(
			List<SelectObjectList> addressMasterList) {
		this.addressMasterList = addressMasterList;
	}

	/**
	 * メソッド名 : serviceListのGetterメソッド
	 * 機能概要 : serviceListを取得する。
	 * @return serviceList
	 */
	public List<SelectObjectList> getServiceList() {
		return serviceList;
	}

	/**
	 * メソッド名 : serviceListのSetterメソッド
	 * 機能概要 : serviceListをセットする。
	 * @param serviceList
	 */
	public void setServiceList(List<SelectObjectList> serviceList) {
		this.serviceList = serviceList;
	}

	/**
	 * メソッド名 : serviceMasterListのGetterメソッド
	 * 機能概要 : serviceMasterListを取得する。
	 * @return serviceMasterList
	 */
	public List<SelectObjectList> getServiceMasterList() {
		return serviceMasterList;
	}

	/**
	 * メソッド名 : serviceMasterListのSetterメソッド
	 * 機能概要 : serviceMasterListをセットする。
	 * @param serviceMasterList
	 */
	public void setServiceMasterList(
			List<SelectObjectList> serviceMasterList) {
		this.serviceMasterList = serviceMasterList;
	}

	/**
	 * メソッド名 : actionのGetterメソッド
	 * 機能概要 : actionを取得する。
	 * @return action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * メソッド名 : actionのSetterメソッド
	 * 機能概要 : actionをセットする。
	 * @param action
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * メソッド名 : ids_ipsのGetterメソッド
	 * 機能概要 : ids_ipsを取得する。
	 * @return ids_ips
	 */
	public String getIds_ips() {
		return ids_ips;
	}

	/**
	 * メソッド名 : ids_ipsのSetterメソッド
	 * 機能概要 : ids_ipsをセットする。
	 * @param idsIps
	 */
	public void setIds_ips(String idsIps) {
		ids_ips = idsIps;
	}

	/**
	 * メソッド名 : scheduleNameのGetterメソッド
	 * 機能概要 : scheduleNameを取得する。
	 * @return scheduleName
	 */
	public String getScheduleName() {
		return scheduleName;
	}

	/**
	 * メソッド名 : scheduleNameのSetterメソッド
	 * 機能概要 : scheduleNameをセットする。
	 * @param scheduleName
	 */
	public void setScheduleName(String scheduleName) {
		this.scheduleName = scheduleName;
	}

	/**
	 * メソッド名 : scheduleListのGetterメソッド
	 * 機能概要 : scheduleListを取得する。
	 * @return scheduleList
	 */
	public List<Schedules> getScheduleList() {
		return scheduleList;
	}

	/**
	 * メソッド名 : scheduleListのSetterメソッド
	 * 機能概要 : scheduleListをセットする。
	 * @param scheduleList
	 */
	public void setScheduleList(List<Schedules> scheduleList) {
		this.scheduleList = scheduleList;
	}

	/**
	 * メソッド名 : startTimeのGetterメソッド
	 * 機能概要 : startTimeを取得する。
	 * @return startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * メソッド名 : startTimeのSetterメソッド
	 * 機能概要 : startTimeをセットする。
	 * @param startTime
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * メソッド名 : acceptStatusのGetterメソッド
	 * 機能概要 : acceptStatusを取得する。
	 * @return acceptStatus
	 */
	public String getAcceptStatus() {
		return acceptStatus;
	}

	/**
	 * メソッド名 : acceptStatusのSetterメソッド
	 * 機能概要 : acceptStatusをセットする。
	 * @param acceptStatus
	 */
	public void setAcceptStatus(String acceptStatus) {
		this.acceptStatus = acceptStatus;
	}

	// 20110603 ktakenaka@PROSITE add start
	/**
	 * メソッド名 : urlFilterNameのGetterメソッド
	 * 機能概要 : urlFilterNameを取得する。
	 * @return urlFilterName
	 */
	public String getUrlFilterName() {
		return urlFilterName;
	}

	/**
	 * メソッド名 : urlFilterNameのSetterメソッド
	 * 機能概要 : urlFilterNameをセットする。
	 * @param urlFilterName
	 */
	public void setUrlFilterName(String urlFilterName) {
		this.urlFilterName = urlFilterName;
	}

	/**
	 * メソッド名 : urlFilterListのGetterメソッド
	 * 機能概要 : urlFilterListを取得する。
	 * @return urlFilterList
	 */
	public List<UrlFilters> getUrlFilterList() {
		return urlFilterList;
	}

	/**
	 * メソッド名 : urlFilterListのSetterメソッド
	 * 機能概要 : urlFilterListをセットする。
	 * @param urlFilterList
	 */
	public void setUrlFilterList(List<UrlFilters> urlFilterList) {
		this.urlFilterList = urlFilterList;
	}

	/**
	 * メソッド名 : urlfilterDefaultFlgのGetterメソッド
	 * 機能概要 : urlfilterDefaultFlgを取得する。
	 * @return urlfilterDefaultFlg
	 */
	public int getUrlfilterDefaultFlg() {
		return urlfilterDefaultFlg;
	}
	/**
	 * メソッド名 : urlfilterDefaultFlgのSetterメソッド
	 * 機能概要 : urlfilterDefaultFlgをセットする。
	 * @param urlfilterDefaultFlg
	 */
	public void setUrlfilterDefaultFlg(int urlfilterDefaultFlg) {
		this.urlfilterDefaultFlg = urlfilterDefaultFlg;
	}

	/**
	 * メソッド名 : virusCheckFlgのGetterメソッド
	 * 機能概要 : virusCheckFlgを取得する。
	 * @return virusCheckFlg
	 */
	public String getVirusCheckFlg() {
		return virusCheckFlg;
	}

	/**
	 * メソッド名 : virusCheckFlgのSetterメソッド
	 * 機能概要 : virusCheckFlgをセットする。
	 * @param virusCheckFlg
	 */
	public void setVirusCheckFlg(String virusCheckFlg) {
		this.virusCheckFlg = virusCheckFlg;
	}

	/**
	 * メソッド名 : spywareFlgのGetterメソッド
	 * 機能概要 : spywareFlgを取得する。
	 * @return spywareFlg
	 */
	public String getSpywareFlg() {
		return spywareFlg;
	}

	/**
	 * メソッド名 : spywareFlgのSetterメソッド
	 * 機能概要 : spywareFlgをセットする。
	 * @param spywareFlg
	 */
	public void setSpywareFlg(String spywareFlg) {
		this.spywareFlg = spywareFlg;
	}

	// 20110603 ktakenaka@PROSITE add end

	// 20130901 kkato@PROSITE add start
	/**
	 * メソッド名 : disableFlgのGetterメソッド
	 * 機能概要 : disableFlgを取得する。
	 * @return disableFlg
	 */
	public String getDisableFlg() {
		return disableFlg;
	}

	/**
	 * メソッド名 : disableFlgのSetterメソッド
	 * 機能概要 : disableFlgをセットする。
	 * @param disableFlg
	 */
	public void setDisableFlg(String disableFlg) {
		this.disableFlg = disableFlg;
	}
	// 20130901 kkato@PROSITE add end
}
