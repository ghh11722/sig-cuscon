/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : RollbackOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/27     komakiys                初版作成
 * 2010/06/23     oohashij         StartTime、受付状態の追加
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.vo.SecurityRules;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;

/**
 * クラス名 : RollbackOutput
 * 機能概要 :ロールバック画面出力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/27
 *          新規作成
 * @version 1.1 oohashij
 *          Created 2010/06/23
 *          StartTime、受付状態の追加
 * @see
 */
public class RollbackOutput {

	// ポリシー一覧リスト
	private List<SelectPolicyList> policyList = null;

	// 世代番号、コミット日時リスト
	private List<SecurityRules> generationList = null;

	// 選択世代番号
	private int selectGeneNo = 0;

	// メッセージ(エラー時のみ)
	private String message = null;

	// StartTime
	private String startTime = null;

	// 受付状態
	private String acceptStatus = null;

	/**
	 * メソッド名 : policyListのGetterメソッド
	 * 機能概要 : policyListを取得する。
	 * @return policyList
	 */
	public List<SelectPolicyList> getPolicyList() {
		return policyList;
	}

	/**
	 * メソッド名 : policyListのSetterメソッド
	 * 機能概要 : policyListをセットする。
	 * @param policyList
	 */
	public void setPolicyList(List<SelectPolicyList> policyList) {
		this.policyList = policyList;
	}

	/**
	 * メソッド名 : generationListのGetterメソッド
	 * 機能概要 : generationListを取得する。
	 * @return generationList
	 */
	public List<SecurityRules> getGenerationList() {
		return generationList;
	}

	/**
	 * メソッド名 : generationListのSetterメソッド
	 * 機能概要 : generationListをセットする。
	 * @param generationList
	 */
	public void setGenerationList(List<SecurityRules> generationList) {
		this.generationList = generationList;
	}

	/**
	 * メソッド名 : selectGeneNoのGetterメソッド
	 * 機能概要 : selectGeneNoを取得する。
	 * @return selectGeneNo
	 */
	public int getSelectGeneNo() {
		return selectGeneNo;
	}

	/**
	 * メソッド名 : selectGeneNoのSetterメソッド
	 * 機能概要 : selectGeneNoをセットする。
	 * @param selectGeneNo
	 */
	public void setSelectGeneNo(int selectGeneNo) {
		this.selectGeneNo = selectGeneNo;
	}

	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * メソッド名 : startTimeのGetterメソッド
	 * 機能概要 : startTimeを取得する。
	 * @return startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * メソッド名 : startTimeのSetterメソッド
	 * 機能概要 : startTimeをセットする。
	 * @param startTime
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * メソッド名 : acceptStatusのGetterメソッド
	 * 機能概要 : acceptStatusを取得する。
	 * @return acceptStatus
	 */
	public String getAcceptStatus() {
		return acceptStatus;
	}

	/**
	 * メソッド名 : acceptStatusのSetterメソッド
	 * 機能概要 : acceptStatusをセットする。
	 * @param acceptStatus
	 */
	public void setAcceptStatus(String acceptStatus) {
		this.acceptStatus = acceptStatus;
	}

}
