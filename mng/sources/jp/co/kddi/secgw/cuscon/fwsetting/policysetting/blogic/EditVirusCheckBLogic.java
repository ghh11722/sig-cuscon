/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditVirusCheckBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/05/18     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.EditVirusCheckOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.PolicyInput;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicResult;

/**
 * クラス名 : EditVirusCheckBLogic
 * 機能概要 :IpsIds項目選択時の処理を行う。
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/05/18
 *          新規作成
 * @see
 */
public class EditVirusCheckBLogic implements BLogic<PolicyInput> {

	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic.EditVirusCheckBLogic");
	/**
	 * メソッド名 :execute
	 * 機能概要 :ルール名、Webウィルスチェックフラグをアウトプットクラスに設定する。
	 * @param param PolicyInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(PolicyInput param) {

		log.debug("EditVirusCheckBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");

        // 選択したリストからオブジェクトを取得する。
        SelectPolicyList selectPolicy = param.getPolicyList().get(param.getIndex());

        // ビジネスロジックの出力クラスに結果を設定する
        EditVirusCheckOutput out = new EditVirusCheckOutput();
        out.setName(selectPolicy.getName());
        out.setVirusCheckFlg(selectPolicy.getVirusCheckFlg());

        result.setResultObject(out);
        result.setResultString("success");

        log.debug("EditVirusCheckBLogic処理終了");
        return result;
	}
}
