/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CommitForm.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/22     komakiys                初版作成
 * 2010/06/23     oohashij                StartTime、受付状態の追加
 * 2011/06/14     ktakenaka@PROSITE       Webフィルタの追加
 * 2012/08/27     kkato@PROSITE           通信全断アラート対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.commit.form;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.vo.AddressGroups;
import jp.co.kddi.secgw.cuscon.common.vo.Addresses;
import jp.co.kddi.secgw.cuscon.common.vo.ApplicationFilters;
import jp.co.kddi.secgw.cuscon.common.vo.ApplicationGroups;
import jp.co.kddi.secgw.cuscon.common.vo.Schedules;
import jp.co.kddi.secgw.cuscon.common.vo.SecurityRules;
import jp.co.kddi.secgw.cuscon.common.vo.ServiceGroups;
import jp.co.kddi.secgw.cuscon.common.vo.Services;
import jp.co.kddi.secgw.cuscon.common.vo.UrlFilters;
import jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx;

/**
 * クラス名 : CommitForm
 * 機能概要 :
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/22
 *          新規作成
 * @version 1.1 oohashij
 *          Created 2010/06/23
 *          StartTime、受付状態の追加
 * @version 1.4 kkato@prosite
 *          Created 2012/08/27
 *          通信全断アラート対応
 * @see
 */
public class CommitForm extends ValidatorActionFormEx {

	// シリアルバージョンID
	private static final long serialVersionUID = 1L;

	// CusconUVOのログインID
	private String loginId = null;

	// セキュリティポリシー情報
	private List<SecurityRules> rules = null;

	// アドレス情報
	private List<Addresses> addresses = null;

	// アドレスグループ情報
	private List<AddressGroups> addressGroups = null;

	// アプリケーショングループ情報
	private List<ApplicationGroups> applicationGroups = null;

	// アプリケーションフィルタ情報
	private List<ApplicationFilters> applicationFilters = null;

	// サービス情報
	private List<Services> services = null;

	// サービスグループ情報
	private List<ServiceGroups> serviceGroups = null;

	// スケジュール情報
	private List<Schedules> schedules = null;

	// 20110614 ktakenaka@PROSITE add start
	// Webフィルタ情報
	private List<UrlFilters> urlFilters = null;
	// 20110614 ktakenaka@PROSITE add end

	// 20120827 kkato@PROSITE add start
	// セキュリティポリシー情報
	private List<SecurityRules> policyRules = null;

	// 通信全断アラートフラグ
	private String denyAlert = null;
	// 20120827 kkato@PROSITE add end

	// メッセージ
	private String message = null;

	// StartTime
	private String startTime = null;

	// 受付状態
	private String acceptStatus = null;

	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * メソッド名 : loginIdのGetterメソッド
	 * 機能概要 : loginIdを取得する。
	 * @return loginId
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * メソッド名 : loginIdのSetterメソッド
	 * 機能概要 : loginIdをセットする。
	 * @param loginId
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * メソッド名 : rulesのGetterメソッド
	 * 機能概要 : rulesを取得する。
	 * @return rules
	 */
	public List<SecurityRules> getRules() {
		return rules;
	}

	/**
	 * メソッド名 : rulesのSetterメソッド
	 * 機能概要 : rulesをセットする。
	 * @param rules
	 */
	public void setRules(List<SecurityRules> rules) {
		this.rules = rules;
	}

	/**
	 * メソッド名 : addressesのGetterメソッド
	 * 機能概要 : addressesを取得する。
	 * @return addresses
	 */
	public List<Addresses> getAddresses() {
		return addresses;
	}

	/**
	 * メソッド名 : addressesのSetterメソッド
	 * 機能概要 : addressesをセットする。
	 * @param addresses
	 */
	public void setAddresses(List<Addresses> addresses) {
		this.addresses = addresses;
	}

	/**
	 * メソッド名 : addressGroupsのGetterメソッド
	 * 機能概要 : addressGroupsを取得する。
	 * @return addressGroups
	 */
	public List<AddressGroups> getAddressGroups() {
		return addressGroups;
	}

	/**
	 * メソッド名 : addressGroupsのSetterメソッド
	 * 機能概要 : addressGroupsをセットする。
	 * @param addressGroups
	 */
	public void setAddressGroups(List<AddressGroups> addressGroups) {
		this.addressGroups = addressGroups;
	}

	/**
	 * メソッド名 : applicationGroupsのGetterメソッド
	 * 機能概要 : applicationGroupsを取得する。
	 * @return applicationGroups
	 */
	public List<ApplicationGroups> getApplicationGroups() {
		return applicationGroups;
	}

	/**
	 * メソッド名 : applicationGroupsのSetterメソッド
	 * 機能概要 : applicationGroupsをセットする。
	 * @param applicationGroups
	 */
	public void setApplicationGroups(List<ApplicationGroups> applicationGroups) {
		this.applicationGroups = applicationGroups;
	}

	/**
	 * メソッド名 : applicationFiltersのGetterメソッド
	 * 機能概要 : applicationFiltersを取得する。
	 * @return applicationFilters
	 */
	public List<ApplicationFilters> getApplicationFilters() {
		return applicationFilters;
	}

	/**
	 * メソッド名 : applicationFiltersのSetterメソッド
	 * 機能概要 : applicationFiltersをセットする。
	 * @param applicationFilters
	 */
	public void setApplicationFilters(List<ApplicationFilters> applicationFilters) {
		this.applicationFilters = applicationFilters;
	}

	/**
	 * メソッド名 : servicesのGetterメソッド
	 * 機能概要 : servicesを取得する。
	 * @return services
	 */
	public List<Services> getServices() {
		return services;
	}

	/**
	 * メソッド名 : servicesのSetterメソッド
	 * 機能概要 : servicesをセットする。
	 * @param services
	 */
	public void setServices(List<Services> services) {
		this.services = services;
	}

	/**
	 * メソッド名 : serviceGroupsのGetterメソッド
	 * 機能概要 : serviceGroupsを取得する。
	 * @return serviceGroups
	 */
	public List<ServiceGroups> getServiceGroups() {
		return serviceGroups;
	}

	/**
	 * メソッド名 : serviceGroupsのSetterメソッド
	 * 機能概要 : serviceGroupsをセットする。
	 * @param serviceGroups
	 */
	public void setServiceGroups(List<ServiceGroups> serviceGroups) {
		this.serviceGroups = serviceGroups;
	}

	/**
	 * メソッド名 : schedulesのGetterメソッド
	 * 機能概要 : schedulesを取得する。
	 * @return schedules
	 */
	public List<Schedules> getSchedules() {
		return schedules;
	}

	/**
	 * メソッド名 : schedulesのSetterメソッド
	 * 機能概要 : schedulesをセットする。
	 * @param schedules
	 */
	public void setSchedules(List<Schedules> schedules) {
		this.schedules = schedules;
	}

	// 20110614 ktakenaka@PROSITE add start
	/**
	 * メソッド名 : urlFiltersのSetterメソッド
	 * 機能概要 : urlFiltersをセットする。
	 * @param urlFilters
	 */
	public void setUrlFilters(List<UrlFilters> urlFilters) {
		this.urlFilters = urlFilters;
	}

	/**
	 * メソッド名 : urlFiltersのGetterメソッド
	 * 機能概要 : urlFiltersを取得する。
	 * @return urlFilters
	 */
	public List<UrlFilters> getUrlFilters() {
		return urlFilters;
	}
	// 20110614 ktakenaka@PROSITE add end

	// 20120827 kkato@PROSITE add start
	/**
	 * メソッド名 : policyRulesのSetterメソッド
	 * 機能概要 : policyRulesをセットする。
	 * @param policyRules
	 */
	public void setPolicyRules(List<SecurityRules> policyRules) {
		this.policyRules = policyRules;
	}

	/**
	 * メソッド名 : policyRulesのGetterメソッド
	 * 機能概要 : policyRulesを取得する。
	 * @return policyRules
	 */
	public List<SecurityRules> getPolicyRules() {
		return policyRules;
	}

	/**
	 * メソッド名 : policyRulesのSetterメソッド
	 * 機能概要 : policyRulesをセットする。
	 * @param policyRules
	 */
	public void setDenyAlert(String denyAlert) {
		this.denyAlert = denyAlert;
	}

	/**
	 * メソッド名 : policyRulesのGetterメソッド
	 * 機能概要 : policyRulesを取得する。
	 * @return policyRules
	 */
	public String getDenyAlert() {
		return denyAlert;
	}
// 20120827 kkato@PROSITE add end

	/**
	 * メソッド名 : startTimeのGetterメソッド
	 * 機能概要 : startTimeを取得する。
	 * @return startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * メソッド名 : startTimeのSetterメソッド
	 * 機能概要 : startTimeをセットする。
	 * @param startTime
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * メソッド名 : acceptStatusのGetterメソッド
	 * 機能概要 : acceptStatusを取得する。
	 * @return acceptStatus
	 */
	public String getAcceptStatus() {
		return acceptStatus;
	}

	/**
	 * メソッド名 : acceptStatusのSetterメソッド
	 * 機能概要 : acceptStatusをセットする。
	 * @param acceptStatus
	 */
	public void setAcceptStatus(String acceptStatus) {
		this.acceptStatus = acceptStatus;
	}

}
