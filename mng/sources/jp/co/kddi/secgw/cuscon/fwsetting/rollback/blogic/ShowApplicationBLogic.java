/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ShowApplicationBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/28     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.rollback.blogic;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectObjectList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.FilterInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto.ShowInput;
import jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto.ShowApplicationOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.rollback.vo.SelectShowApplicationList;
import jp.co.kddi.secgw.cuscon.fwsetting.rollback.vo.SelectShowFilterList;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : ShowApplicationBLogic
 * 機能概要 :Application項目選択時の処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/28
 *          新規作成
 * @see
 */
public class ShowApplicationBLogic implements BLogic<ShowInput> {

	/**
     * QueryDAO。
     * Springによりインスタンス生成され設定される。
     */
    private QueryDAO queryDAO = null;
    private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.rollback.blogic.ShowApplicationBLogic");
    private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 :execute
	 * 機能概要 :選択アプリケーションリストをアウトプットクラスに設定する。
	 * @param param ShowInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(ShowInput param) {
		log.debug("ShowApplicationBLogic処理開始");

		List<SelectObjectList> categoryList = null;
	    List<SelectObjectList> subCategoryList = null;
	    List<SelectObjectList> technologyList = null;
	    List<SelectObjectList> riskList = null;
	    // 切り替え判別用フィルタ名
		String lastFilterName = null;

		BLogicResult result = new BLogicResult();
        result.setResultString("failure");
        // エラーメッセージ
        BLogicMessages messages = new BLogicMessages();

		// アプリケーションマスタリスト
		SelectObjectList obj = new SelectObjectList();
		// アプリケーションマスタリスト
		List<String> appliList = new ArrayList<String>();
		// アプリケーショングループリスト
		List<String> appliGrpList = new ArrayList<String>();
		// アプリケーションフィルタリスト
		List<String> appliFilList = new ArrayList<String>();

		// 返却用リスト
		List<SelectApplicationList> resultAppliList =
						new ArrayList<SelectApplicationList>();
		List<SelectApplicationGroupList> resultGrpliList =
						new ArrayList<SelectApplicationGroupList>();
		List<SelectShowFilterList> resultFilList =
						new ArrayList<SelectShowFilterList>();

		// 選択ポリシーリスト
		SelectPolicyList selectPolicy = param.getPolicyList().get(param.getIndex());

		List<SelectObjectList> selectAppliList = selectPolicy.getApplicationList();

		// アプリケーション名リスト分繰りかえす。
		for(int i=0; i<selectAppliList.size(); i++) {
			log.debug("アプリケーション名:" + selectAppliList.get(i).getName());

			obj.setName(selectAppliList.get(i).getName());
			// オブジェクトタイプが0(アプリケーションマスタ)の場合
			if(selectAppliList.get(i).getObjectType() == 0) {
				log.debug("オブジェクトタイプ０");
				appliList.add(obj.getName());
			// オブジェクトタイプが1(アプリケーショングループ)の場合
			} else if(selectAppliList.get(i).getObjectType() == 1) {
				log.debug("オブジェクトタイプ１");
				appliGrpList.add(obj.getName());
			// オブジェクトタイプが2(アプリケーションフィルタ)の場合
			} else {
				log.debug("オブジェクトタイプ２");
				appliFilList.add(obj.getName());
			}
			obj = new SelectObjectList();
		}

		// アプリケーションマスタ取得用
		SelectShowApplicationList selAppli = new SelectShowApplicationList();

		// アプリケーションマスタが存在する場合
		if(appliList.size() != 0) {
			log.debug("アプリケーションマスタが存在");
			selAppli.setObjList(appliList);
	        try {
				// アプリケーション一覧情報取得
				resultAppliList =
					queryDAO.executeForObjectList(
							"ShowApplicationBLogic-1", selAppli);
	        } catch(Exception e) {
	        	// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
	        	// DBアクセスエラー
	        	Object[] objectgArray = {e.getMessage()};
	        	log.fatal(messageAccessor.getMessage(
	        			"FK071001", objectgArray, param.getUvo()));
	        	messages.add("message", new BLogicMessage("DK070014"));
	        	result.setErrors(messages);
	        	result.setResultString("failure");
	        	return result;
	        }
		}

		// アプリケーショングループ取得用
		SelectShowApplicationList selAppliGrp = new SelectShowApplicationList();
		// アプリケーショングループが存在する場合
		if(appliGrpList.size() != 0) {
			log.debug("アプリケーショングループが存在");
			selAppliGrp.setObjList(appliGrpList);
			selAppliGrp.setVsysId(param.getUvo().getVsysId());
			selAppliGrp.setGenerationNo(param.getSelectGeneNo());

			List<SelectApplicationGroupList> appliGrpOut = null;
	        try {
				// アプリケーショングループ一覧情報取得
				appliGrpOut = queryDAO.executeForObjectList(
							"ShowApplicationBLogic-2", selAppliGrp);
	        } catch(Exception e) {
	        	// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
	        	// DBアクセスエラー
	        	Object[] objectgArray = {e.getMessage()};
	        	log.fatal(messageAccessor.getMessage(
	        			"FK071001", objectgArray, param.getUvo()));
	        	messages.add("message", new BLogicMessage("DK070014"));
	        	result.setErrors(messages);
	        	result.setResultString("failure");
	        	return result;
	        }
			String lastGrpName = null;
			String connectName = "";
			// アプリカウント用
			int count = 0;
			SelectApplicationGroupList selGrp = null;
			// アプリケーショングループ一覧分繰り返す。
			for(int i=0; i<appliGrpOut.size(); i++) {
				log.debug("アプリケーショングループ名:" +
						appliGrpOut.get(i).getName() +
								", lastGrpName:" + lastGrpName);

				// グループ名が切り替わった場合、以下の処理を行う
				if (i != 0 &&
						!lastGrpName.equals(appliGrpOut.get(i).getName())) {
					log.debug("グループ名変更i = " + i);

					selGrp = new SelectApplicationGroupList();
					selGrp.setName(lastGrpName);
					selGrp.setMembers(count);
					selGrp.setApplications(connectName);
					resultGrpliList.add(selGrp);
					count = 0;
				}
				// カウントする。
				count++;
				// カウントが1の場合
				if(count == 1) {
					log.debug("カウント:" + i);
					connectName = appliGrpOut.get(i).getApplications();
				} else {
					log.debug("カウント:" + i);
					connectName = connectName + "/" + appliGrpOut.get(i).getApplications();
				}
				lastGrpName = appliGrpOut.get(i).getName();
			}
			// 最後の要素を追加する。
			selGrp = new SelectApplicationGroupList();
			selGrp.setName(lastGrpName);
			selGrp.setMembers(count);
			selGrp.setApplications(connectName);
			resultGrpliList.add(selGrp);
		}


		// アプリケーションフィルタ取得用
		SelectShowApplicationList selAppliFil = new SelectShowApplicationList();

		if(appliFilList.size() != 0) {
			log.debug(
				"アプリケーションフィルタが存在がappliFilList.size() = " +
														appliFilList.size() );
			selAppliFil.setObjList(appliFilList);
			selAppliFil.setVsysId(param.getUvo().getVsysId());
			selAppliFil.setGenerationNo(param.getSelectGeneNo());


			List<FilterInfo> appliFilOut = null;
	        try {
				// アプリケーションフィルタ一覧情報取得
				appliFilOut = queryDAO.executeForObjectList("ShowApplicationBLogic-3",
															selAppliFil);
	        } catch(Exception e) {
	        	// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
	        	// DBアクセスエラー
	        	Object[] objectgArray = {e.getMessage()};
	        	log.fatal(messageAccessor.getMessage(
	        			"FK071001", objectgArray, param.getUvo()));
	        	messages.add("message", new BLogicMessage("DK070014"));
	        	result.setErrors(messages);
	        	result.setResultString("failure");
	        	return result;
	        }

			SelectObjectList category = null;
		    SelectObjectList subCategory = null;
		    SelectObjectList technology = null;
		    SelectObjectList risk = null;

			// リスト初期化処理
			categoryList = new ArrayList<SelectObjectList>();
			subCategoryList = new ArrayList<SelectObjectList>();
			technologyList = new ArrayList<SelectObjectList>();
			riskList = new ArrayList<SelectObjectList>();
			// 切り替え判別用フィルタ名
			lastFilterName = null;
			// フィルタ情報の件数分、以下の処理を行う
			for (int i = 0; i < appliFilOut.size(); i++) {
				log.debug("lastFilterName = " + lastFilterName +
						", appliFilOut.get(i).getName() = " +
									appliFilOut.get(i).getName());

				// フィルタ名が切り替わった場合、以下の処理を行う
				if (i != 0 &&
						!lastFilterName.equals(appliFilOut.get(i).getName())) {

					log.debug("フィルタ名切り替わり:i = " + i);
					// 返却用リスト設定処理
					setResultList(lastFilterName, categoryList, subCategoryList,
							technologyList, riskList, resultFilList);
					// リスト初期化処理
					categoryList = new ArrayList<SelectObjectList>();
					subCategoryList = new ArrayList<SelectObjectList>();
					technologyList = new ArrayList<SelectObjectList>();
					riskList = new ArrayList<SelectObjectList>();
				}
				// カテゴリがnullでないならリストにadd
				if (appliFilOut.get(i).getCategory() != null) {
					log.debug("カテゴリが存在");
					category = new SelectObjectList();
					category.setName(appliFilOut.get(i).getCategory());
					categoryList.add(category);
				}
				// サブカテゴリがnullでないならリストにadd
				if (appliFilOut.get(i).getSubCategory() != null) {
					log.debug("サブカテゴリが存在");
					subCategory = new SelectObjectList();
					subCategory.setName(appliFilOut.get(i).getSubCategory());
					subCategoryList.add(subCategory);
				}
				// テクノロジがnullでないならリストにadd
				if (appliFilOut.get(i).getTechnology() != null) {
					log.debug("テクノロジが存在");
					technology = new SelectObjectList();
					technology.setName(appliFilOut.get(i).getTechnology());
					technologyList.add(technology);
				}
				// リスクが0でないならリストにadd
				if (appliFilOut.get(i).getRisk() != 0) {
					log.debug("リスクが存在");
					risk = new SelectObjectList();
					risk.setObjectType(appliFilOut.get(i).getRisk());
					riskList.add(risk);
				}
				lastFilterName = appliFilOut.get(i).getName();
			}

			// 最後の要素を追加する。
			setResultList(lastFilterName, categoryList, subCategoryList,
							technologyList, riskList, resultFilList);
		}

		ShowApplicationOutput out = new ShowApplicationOutput();
		out.setAppliList(resultAppliList);
		out.setAppliGrpList(resultGrpliList);
		out.setAppliFilList(resultFilList);

		result.setResultObject(out);
	    result.setResultString("success");

	    log.debug("ShowApplicationBLogic処理終了");
		return result;
	}


	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 :setResultList
	 * 機能概要 : 返却用リスト設定処理
	 * @param lastFilterName
	 * @param categoryList
	 * @param subCategoryList
	 * @param technologyList
	 * @param riskList
	 * @param resultFilList
	 */
	private void setResultList(String lastFilterName, List<SelectObjectList> categoryList,
			List<SelectObjectList> subCategoryList, List<SelectObjectList> technologyList,
			List<SelectObjectList> riskList, List<SelectShowFilterList> resultFilList) {
		log.debug("setResultList処理開始");
		SelectShowFilterList SelectShowFilterList = new SelectShowFilterList();
		SelectShowFilterList.setName(lastFilterName);
		SelectShowFilterList.setCategoryList(categoryList);
		SelectShowFilterList.setSubCategoryList(subCategoryList);
		SelectShowFilterList.setTechnologyList(technologyList);
		SelectShowFilterList.setRiskList(riskList);
		resultFilList.add(SelectShowFilterList);
		log.debug("setResultList処理終了");
	}
	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
