/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditServiceGroupOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/11     kinoshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.servicegroups.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.ServicesAndGroupsInfo;

/**
 * クラス名 : EditServiceGroupOutput
 * 機能概要 :
 * 備考 :
 * @author kinoshtt
 * @version 1.0 kinoshtt
 *          Created 2010/03/11
 *          新規作成
 * @see
 */
public class EditServiceGroupOutput {

	// アドレスグループ名
	private String name = null;

	// アドレスグループ追加用サービス＆サービスグループ
	private List<ServicesAndGroupsInfo> serviceAndServiceGrp4Add = null;

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * メソッド名 : serviceAndServiceGrp4AddのGetterメソッド
	 * 機能概要 : serviceAndServiceGrp4Addを取得する。
	 * @return serviceAndServiceGrp4Add
	 */
	public List<ServicesAndGroupsInfo> getServiceAndServiceGrp4Add() {
		return serviceAndServiceGrp4Add;
	}

	/**
	 * メソッド名 : serviceAndServiceGrp4AddのSetterメソッド
	 * 機能概要 : serviceAndServiceGrp4Addをセットする。
	 * @param serviceAndServiceGrp4Add
	 */
	public void setServiceAndServiceGrp4Add(List<ServicesAndGroupsInfo> serviceAndServiceGrp4Add) {
		this.serviceAndServiceGrp4Add = serviceAndServiceGrp4Add;
	}
}
