/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectRadioValidator.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/20     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.form;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.terasoluna.fw.web.struts.form.MultiFieldValidator;

/**
 * クラス名 : SelectRadioValidator
 * 機能概要 : ポリシー編集画面のチェックを行う。
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/20
 *          新規作成
 * @see
 */
public class SelectRadioValidator implements MultiFieldValidator {

	/**
	 * メソッド名 :validate
	 * 機能概要 :ポリシー編集画面にて「選択する」を選択時に「オブジェクトの選択」<br>
	 *           を選択しているかのチェックを行う。
	 * @param checkList 「オブジェクトの選択」の選択インデックス
	 * @param index ラジオボタンのインデックス
	 * @return チェック結果
	 * @see jp.terasoluna.fw.web.struts.form.MultiFieldValidator#validate(java.lang.String, java.lang.String[])
	 */
	@Override
	public boolean validate(String objIdx, String[] radioIdx) {

		// 選択ラジオボタンを取得する。
		int index = Integer.parseInt(radioIdx[0]);

		// 選択ラジオボタンが「選択する」の場合
		// オブジェクトのラジオボタンのインデックスが無効ならばエラーとする。
		if(index == CusconConst.SELECT && Integer.valueOf(objIdx) < 0) {
			return false;
		}

		return true;
	}

}
