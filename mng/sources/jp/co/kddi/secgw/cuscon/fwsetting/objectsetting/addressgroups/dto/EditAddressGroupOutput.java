/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditAddressOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/06     kinoshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.AddressesAndGroupsInfo;

/**
 * クラス名 : EditAddressOutput
 * 機能概要 :
 * 備考 :
 * @author kinoshtt
 * @version 1.0 kinoshtt
 *          Created 2010/03/06
 *          新規作成
 * @see
 */
public class EditAddressGroupOutput {

	// アドレスグループ名
	private String name = null;

	// アドレスグループ追加用サービス＆サービスグループ
	private List<AddressesAndGroupsInfo> addressAndAddressGrp4Add = null;

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * メソッド名 : addressAndAddressGrp4AddのGetterメソッド
	 * 機能概要 : addressAndAddressGrp4Addを取得する。
	 * @return addressAndAddressGrp4Add
	 */
	public List<AddressesAndGroupsInfo> getAddressAndAddressGrp4Add() {
		return addressAndAddressGrp4Add;
	}

	/**
	 * メソッド名 : addressAndAddressGrp4AddのSetterメソッド
	 * 機能概要 : addressAndAddressGrp4Addをセットする。
	 * @param addressAndAddressGrp4Add
	 */
	public void setAddressAndAddressGrp4Add(List<AddressesAndGroupsInfo> addressAndAddressGrp4Add) {
		this.addressAndAddressGrp4Add = addressAndAddressGrp4Add;
	}
}
