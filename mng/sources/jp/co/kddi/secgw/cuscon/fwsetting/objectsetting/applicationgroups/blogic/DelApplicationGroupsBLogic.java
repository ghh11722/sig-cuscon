/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : DelApplicationGroupsBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/23     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.blogic;

import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.ApplicationGroups;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto.ApplicationGroupsOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto.DelApplicationGroupsInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.ApplicationGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.ApplicationGroupLinksInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationGroupList;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;
import org.apache.log4j.Logger;

/**
 * クラス名 : DelApplicationGroupsBLogic
 * 機能概要 : 選択したオブジェクトの削除を行う。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/23
 *          新規作成
 * @see
 */
public class DelApplicationGroupsBLogic implements BLogic<DelApplicationGroupsInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;

	private Logger log = Logger.getLogger(
	"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.blogic.DelApplicationGroupsBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 : 選択したオブジェクトの削除を行う。
	 * @param params DelApplicationGroupsInput入力データクラス
	 * @return result BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(DelApplicationGroupsInput params) {
		log.debug("DelApplicationGroupsBLogic処理開始");

		int seqNo;     // シーケンス番号
		int count;     // リンク数

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得
		CusconUVO uvo = params.getUvo();
		String vsysId = uvo.getVsysId();

		/***********************************************************************
		 *  ①画面から渡されたアプリケーショングループリスト取得
		 **********************************************************************/
		List<SelectApplicationGroupList> applicationGroupList4Del = params.getApplicationGroupList();

		int delSize = applicationGroupList4Del.size();
		int delNum = 0;
		String delStr = "";


		/***********************************************************************
		 *  ②アプリケーショングループの更新フラグ書き換え＆リンク情報削除
		 **********************************************************************/
		log.debug("削除対象あり");
		// 画面から渡されたアプリケーショングループリストの件数分、以下の処理を実行
		for (int i = 0; i < applicationGroupList4Del.size(); i++) {
			log.debug("オブジェクト削除処理中:" + i);
			// 検索条件設定
			ApplicationGroups searchApplicationGroupKey = new ApplicationGroups();
			// Vsys-ID
			searchApplicationGroupKey.setVsysId(vsysId);
			// 世代番号
			searchApplicationGroupKey.setGenerationNo(CusconConst.ZERO_GENE);
			// アプリケーショングループ名
			searchApplicationGroupKey.setName(applicationGroupList4Del.get(i).getName());

			List<ApplicationGroups> applicationGroupList = null;
			try {
				// アプリケーショングループオブジェクトテーブルを検索し、
				// 削除対象アプリケーショングループのSEQ_NOを取得
				applicationGroupList = queryDAO.
				executeForObjectList("ApplicationGroupsBLogic-2", searchApplicationGroupKey);
			} catch(Exception e) {
				// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
				// DBアクセスエラー
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage(
						"FK061016", objectgArray, params.getUvo()));
				messages.add("message", new BLogicMessage("DK060029"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;

			}

			seqNo = applicationGroupList.get(0).getSeqNo();

			// 検索条件設定
			ApplicationGroupLinksInfo selectApplicationGroupLinks =
				new ApplicationGroupLinksInfo();
			selectApplicationGroupLinks.setGrpLinkSeqNo(seqNo);
			selectApplicationGroupLinks.setAplLinkSeqNo(seqNo);

			try {
				// 当該アプリケーショングループがリンクされている件数を取得
				count= queryDAO.executeForObject("ApplicationGroupsBLogic-3",
						selectApplicationGroupLinks, Integer.class);
			} catch(Exception e) {
				// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
				// DBアクセスエラー
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage(
						"FK061016", objectgArray, params.getUvo()));
				messages.add("message", new BLogicMessage("DK060029"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}

			if (count > 0) {
				log.debug("オブジェクト削除不可:" + applicationGroupList4Del.get(i).getName());
				delNum++;

				// 削除名称を連結する。
				if(delNum == 1) {
					delStr = applicationGroupList4Del.get(i).getName();
				} else {
					delStr = delStr + ", " + applicationGroupList4Del.get(i).getName();
				}
				if(delSize == delNum) {
					// 検索結果が1件以上の場合、削除不可エラー
					// レスポンスデータを作成し返却
					// 削除不可エラーメッセージを出力する。
					Object[] objectgArray = {delStr};
					log.debug(messageAccessor.getMessage(
		        			"DK060002", objectgArray, params.getUvo()));
					messages.add("message",new BLogicMessage("DK060070", objectgArray));
					result.setErrors(messages);
					result.setResultString("failure");
		        	return result;
				}
			// 削除可能
			} else {

				try {
					// アプリケーショングループに紐づくリンク情報削除（T_ApplicationGroupsLink）
					updateDAO.execute("ApplicationGroupsBLogic-5", seqNo);

					// アプリケーショングループオブジェクトテーブルのレコードの変更フラグを'3'（削除）に更新
					updateDAO.execute("ApplicationGroupsBLogic-4", seqNo);
				} catch(Exception e) {
					// トランザクションロールバック
		        	TransactionUtil.setRollbackOnly();
					// DBアクセスエラー
		        	Object[] objectgArray = {e.getMessage()};
		        	log.fatal(messageAccessor.getMessage(
		        			"FK061014", objectgArray, params.getUvo()));
		        	messages.add("message", new BLogicMessage("DK060029"));
					result.setErrors(messages);
					result.setResultString("failure");
		        	return result;
				}
			}
		}


		/***********************************************************************
		 *  ③一覧情報検索共通処理
		 **********************************************************************/
		ApplicationGroupsOutput output = new ApplicationGroupsOutput();

		try {
			ApplicationGroupList applicationGroupList =
										new ApplicationGroupList(messageAccessor);
			output.setApplicationGroupList(applicationGroupList.getApplicationGroupList(queryDAO, uvo));

		} catch(Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("EK061004", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060029"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

		if(delNum != 0) {
	    	// 削除不可オブジェクトの名称出力
	    	log.debug("削除不可オブジェクト名称:" + delStr);
			Object[] objectgArray = {delStr};
			output.setMessage(messageAccessor.getMessage("DK060070", objectgArray));
		}


		/***********************************************************************
		 *  ④結果セット
		 **********************************************************************/
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("DelApplicationGroupsBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
