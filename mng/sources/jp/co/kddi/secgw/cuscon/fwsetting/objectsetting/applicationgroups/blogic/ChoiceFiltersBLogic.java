/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ChoiceFiltersBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.blogic;

import java.util.ArrayList;
import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto.ChoiceFiltersInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto.ChoiceFiltersOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.ApplicationFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import org.apache.log4j.Logger;

/**
 * クラス名 : ChoiceFiltersBLogic
 * 機能概要 : フィルター設定画面表示処理を行う。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/01
 *          新規作成
 * @see
 */
public class ChoiceFiltersBLogic implements BLogic<ChoiceFiltersInput> {

	// QueryDAO
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting." +
	"applicationgroups.blogic.ChoiceFiltersBLogic");

	private MessageAccessor messageAccessor = null;

	// 画面遷移判別定数
	private static final String REGIST = "登録";         // 登録

	/**
	 * メソッド名 : BLogicResult
	 * 機能概要 : フィルター設定画面表示処理を行う。
	 * @param params ChoiceFiltersInput入力データクラス
	 * @return result クラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(ChoiceFiltersInput params) {
		log.debug("ChoiceFiltersBLogic処理開始");
		List<SelectApplicationFilterList> list4Out = null;   // アプリケーションフィルタ情報のリスト
		String failString;

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();

		if (params.getDisplayType().equals(REGIST)) {
			log.debug("遷移先が登録系画面");
			failString = "failure4Regist";
			result.setResultString(failString);
		} else {
			log.debug("遷移先が更新系画面");
			failString = "failure4Update";
			result.setResultString(failString);
		}

		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = params.getUvo();

		/***********************************************************************
		 *  ①画面から取得した「選択中のアプリケーション」リストからフィルタ情報取得
		 **********************************************************************/
		// 「選択中のアプリケーション」リスト
		String[] selectAplList = params.getSelectAplList();

		// 「選択中のアプリケーション」のタイプリスト（並び順は「選択中のアプリケーション」リストと同様）
		String[] selectTypeList = params.getSelectTypeList();

		// 編集用tmpフィルタリスト
		ArrayList<String> tmpFltList = new ArrayList<String>();

		// 「選択中のアプリケーション」リストの件数分、以下の処理を実行
		if (!selectAplList[0].equals("null") || selectAplList.length != 1) {
			log.debug("選択中アプリケーションリストがnullでない場合");
			for (int i = 0; i < selectAplList.length; i++) {
				log.debug("選択中アプリケーション処理中:" + i);
				// タイプがフィルタの場合、それをリストに格納
				if (selectTypeList[i].equals(CusconConst.TYPE_FLT)) {
					log.debug("タイプがフィルタになっているアプリケーションをリストに格納");
					tmpFltList.add(selectAplList[i]);
				}
			}
		}


		/***********************************************************************
		 *  ②DBからアプリケーションフィルタ情報を取得
		 **********************************************************************/
		List<SelectApplicationFilterList> dbFltList = null;

		try {
			ApplicationFilterList applicationFilterList =
										new ApplicationFilterList(messageAccessor);
			dbFltList = applicationFilterList.getApplicationFilterList(queryDAO, uvo);

		} catch(Exception e) {
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("EK061004", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060063"));
			result.setErrors(messages);
        	return result;
		}


		/***********************************************************************
		 *  ③マッチング処理
		 **********************************************************************/
		list4Out = new ArrayList<SelectApplicationFilterList>();
		// ①で得たリストと②で得たリストのアプリケーショングループ名が一致する場合、チェックON対象とみなす。
		for (int i = 0; i < dbFltList.size(); i++) {
			log.debug("DBから取得したアプリケーションフィルタ情報処理中:" + i);

			SelectApplicationFilterList selectApplicationFilterList =
				new SelectApplicationFilterList();

			for (int j = 0; j < tmpFltList.size(); j++) {
				log.debug("チェックON/OFF判別中:" + i);
				// チェックON/OFF判別
				if (tmpFltList.get(j).equals(dbFltList.get(i).getName())) {
					log.debug("チェックボックスON対象:" + dbFltList.get(i).getName());
					selectApplicationFilterList.setChkFlg(true);
				}
			}
			selectApplicationFilterList.setName(dbFltList.get(i).getName());
			selectApplicationFilterList.setCategoryList(dbFltList.get(i).getCategoryList());
			selectApplicationFilterList.setSubCategoryList(dbFltList.get(i).getSubCategoryList());
			selectApplicationFilterList.setTechnologyList(dbFltList.get(i).getTechnologyList());
			selectApplicationFilterList.setRiskList(dbFltList.get(i).getRiskList());

			list4Out.add(selectApplicationFilterList);
		}


		/***********************************************************************
		 *  ④セッション設定処理
		 **********************************************************************/
		List<SelectedAplInfo> sessionAplList = new ArrayList<SelectedAplInfo>();
		List<SelectedAplInfo> selectedAplList = new ArrayList<SelectedAplInfo>();
		// 「選択中のアプリケーション」リストの件数分、以下の処理を実行
		if (!selectAplList[0].equals("null") || selectAplList.length != 1) {
			log.debug("セッション情報作成");

			for (int i = 0; i < params.getSelectAplList().length; i++) {
				log.debug("セッション情報作成中:" + i);
				SelectedAplInfo aplInfo = new SelectedAplInfo();

				aplInfo.setName(params.getSelectAplList()[i]);
				aplInfo.setType(params.getSelectTypeList()[i]);

				selectedAplList.add(aplInfo);
			}
			// 選択中アプリケーション情報をセッション情報に格納

			sessionAplList = selectedAplList;
		}
		else {
			log.debug("セッションクリア");
			// 選択中アプリケーション情報をセッション情報に格納
			sessionAplList = null;
		}


		/***********************************************************************
		 *  ⑤レスポンス返却
		 **********************************************************************/
		ChoiceFiltersOutput output = new ChoiceFiltersOutput();
		output.setFilterList4Add(list4Out);
		output.setSessionAplList(sessionAplList);
		output.setDisplayType(params.getDisplayType());
		output.setName(params.getName());
		output.setOldName(params.getOldName());
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("ChoiceFiltersBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
