/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddressList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/19     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.Addresses;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectAddressList;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : AddressList
 * 機能概要 : アドレス一覧を表示するための情報を取得する。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/19
 *          新規作成
 * @see
 */
public class AddressList {

	private static Logger log = Logger.getLogger(
	"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.AddressList");

	// メッセージクラス
    private static MessageAccessor messageAccessor = null;

    /**
     * メソッド名 : コンストラクタ
     * 機能概要 : -
     * @param queryDAO
     * @param uvo
     */
    public AddressList() {
    	log.debug("コンストラクタ2開始");
    	log.debug("コンストラクタ3終了");
    }

	/**
	 * メソッド名 :getAddressList
	 * 機能概要 : アドレス一覧情報取得処理
	 * @param vsysId
	 * @return resultList
	 */
	public List<SelectAddressList> getAddressList(QueryDAO queryDAO, CusconUVO uvo) throws Exception{

		log.debug("getAddressList開始");
		// InputBase設定
		InputBase inputBase = new InputBase();
		inputBase.setVsysId(uvo.getVsysId());                         // Vsys-ID
		inputBase.setGenerationNo(CusconConst.ZERO_GENE);    // 世代番号
		inputBase.setModFlg(CusconConst.DEL_NUM);            // 変更フラグ

		List<Addresses> addressList;
		List<SelectAddressList> resultList;

		try {
			// アドレス一覧情報取得
			addressList = queryDAO.executeForObjectList("CommonT_Addresses-1", inputBase);

			// 取得したaddressListの件数分、返却用リストに設定
			resultList = new ArrayList<SelectAddressList>();
			for(int i = 0; i < addressList.size(); i++) {
				SelectAddressList selectAddressListOutput = new SelectAddressList();
				selectAddressListOutput.setName(addressList.get(i).getName());
				selectAddressListOutput.setType(addressList.get(i).getType());
				selectAddressListOutput.setAddress(addressList.get(i).getAddress());
				resultList.add(selectAddressListOutput);
			}
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		log.fatal(messageAccessor.getMessage("FK061004", objectgArray, uvo));
    		throw e;

		}
		// アドレス一覧情報取得結果返却
		log.debug("getAddressList終了:" + addressList.size());
		return resultList;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor msgAcc){
		log.debug("setMessageAccessor処理開始");
	    setMsgAcc(msgAcc);
	    log.debug("setMessageAccessor処理終了");
	}

	/**
	 * メソッド名 : setMsgAcc
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public static void setMsgAcc(MessageAccessor messageAccessor) {
		log.debug("setMsgAcc処理開始");
		AddressList.messageAccessor = messageAccessor;
		log.debug("setMsgAcc処理終了");
	}

	/**
	 * メソッド名 : getMessage
	 * 機能概要 : messageAccessorをゲットする。
	 * @return MessageAccessor
	 */
	public static MessageAccessor getMessage() {
		log.debug("getMessage処理");
		return messageAccessor;
	}
}
