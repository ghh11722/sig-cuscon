/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CommonSelectItem.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/09     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo;

import java.io.Serializable;

/**
 * クラス名 : CommonSelectItem
 * 機能概要 : 汎用セレクトボックス動的生成用
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/09
 *          新規作成
 * @see
 */
public class CommonSelectItem implements Serializable{

	// シリアルバージョンID
	private static final long serialVersionUID = 3218981316380622941L;

	// Value
	private String value= null;
	// Text
	private String text = null;

	/**
	 * メソッド名 : valueのGetterメソッド
	 * 機能概要 : valueを取得する。
	 * @return value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * メソッド名 : valueのSetterメソッド
	 * 機能概要 : valueをセットする。
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * メソッド名 : textのGetterメソッド
	 * 機能概要 : textを取得する。
	 * @return text
	 */
	public String getText() {
		return text;
	}
	/**
	 * メソッド名 : textのSetterメソッド
	 * 機能概要 : textをセットする。
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}
	/**
	 * メソッド名 : serialversionuidのGetterメソッド
	 * 機能概要 : serialversionuidを取得する。
	 * @return serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
