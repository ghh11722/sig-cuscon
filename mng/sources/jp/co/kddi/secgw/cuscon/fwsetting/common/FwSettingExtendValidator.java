/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名	: FwSettingExtendValidator.java
 *
 * [変更履歴]
 * 日付 		  更新者				  内容
 * 2010/03/15	  kinoshtt				  初版作成
 * 2013/01/07	  kkato@PROSITE			  urlFilterCheck追加
 * 2013/09/01     kkato@PROSITE           半角チェック追加
 * 2017/12/20     S.Yamada@Plum-systems   URLフィルタ制限文字列追加
 *******************************************************************************
 */

package jp.co.kddi.secgw.cuscon.fwsetting.common;

import java.io.Serializable;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.validator.Field;
import org.apache.commons.validator.Validator;
import org.apache.commons.validator.ValidatorAction;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.validator.Resources;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import org.apache.commons.validator.util.ValidatorUtils;
import jp.terasoluna.fw.util.PropertyUtil;

/**
 * クラス名 : FwSettingExtendValidator
 * 機能概要 : FwSettingのValidatorの拡張機能
 * 備考 :
 * @author kinoshtt
 * @version 1.0 kinoshtt
 *			Created 2010/03/15
 *			新規作成
 * @see
 */
public class FwSettingExtendValidator implements Serializable {
	//
	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.common.FwSettingExtendValidator");
	// メッセージクラス
	private static MessageAccessor messageAccessor = null;

	//アスタリスクバリデータの上限値
	private static final String ASTERISK_NUMBER = PropertyUtil.getProperty("fwsetting.common.urlfilter_check.asterisk_number", "2");
	//キャレットバリデータの上限値
	private static final String CARET_NUMBER = PropertyUtil.getProperty("fwsetting.common.urlfilter_check.caret_number", "9");

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 */
	public FwSettingExtendValidator() {
		super();
	}

	/**
	 * メソッド名 : 文字列フォーマットチェック1
	 * 機能概要 : 以下のチェックを行う。
	 *			  ①文字列の先頭が半角英数字であること
	 *			  ②文字列が半角英数字または一部半角記号(許容文字列・・・ _-.の3文字)のみで構成されていること
	 * @param bean
	 * @param va
	 * @param field
	 * @param errors
	 * @param validator
	 * @param request
	 * @return boolean 文字列の先頭が半角英数字でかつ、全体が半角英数字と許容する半角記号のみで構成されていればtrue、それ以外の場合はfalse
	 * @throws JspException
	 */
	public static boolean strFormatChk1(Object bean, ValidatorAction va, Field field, ActionMessages errors, Validator validator, HttpServletRequest request)
	throws JspException
	{

		log.debug("strFormatChk1開始");

		// 検証対象の切り出し
		String str = null;
		if (bean instanceof String) {
			str = (String) bean;
		} else {
			try {
				str = PropertyUtils.getProperty(bean, field.getProperty()).toString();
			} catch (Exception e) {
				// 例外
				Object[] objectgArray = {e.getMessage()};
				log.error(messageAccessor.getMessage("EZ011004", objectgArray));
				throw new JspException(e);
			}
		}

		// 文字列の先頭が半角英数字であることをチェック
		Pattern pattern1 = Pattern.compile("^[a-zA-Z0-9]");
		Matcher matcher1 = pattern1.matcher(str);
		if (matcher1.find() == false) {
			errors.add(field.getKey(), Resources.getActionMessage(request, va, field));
			log.debug("strFormatChk1(文字列の先頭が半角英数字以外でfalse)");
			return false;
		}

		// 文字列が半角英数字または半角記号(禁止文字を除く)のみで構成されていることをチェック
		Pattern pattern2 = Pattern.compile("^[a-zA-Z0-9\\-\\.\\_]+$");
		Matcher matcher2 = pattern2.matcher(str);
		if (matcher2.matches() == false) {
			errors.add(field.getKey(), Resources.getActionMessage(request, va, field));
			log.debug("strFormatChk1(文字列に半角英数字、半角記号(禁止文字列を除く)以外が含まれていてfalse)");
			return false;
		}
		log.debug("strFormatChk1(trueで終了)");
		return true;
	}

	/**
	 * メソッド名 : オブジェクト名称の入力チェック1
	 * 機能概要 : 以下の不正な名称が入力されていないかチェック
	 *			  ・"any"
	 * @param bean
	 * @param va
	 * @param field
	 * @param errors
	 * @param validator
	 * @param request
	 * @return
	 * @throws JspException
	 */
	public static boolean objectNameChk1(Object bean, ValidatorAction va, Field field, ActionMessages errors, Validator validator, HttpServletRequest request)
		throws JspException {

		log.debug("nameChk1開始");

		// 検証対象の切り出し
		String str = null;
		if (bean instanceof String) {
			str = (String) bean;
		} else {
			try {
				str = PropertyUtils.getProperty(bean, field.getProperty()).toString();
			} catch (Exception e) {
				// 例外
				Object[] objectgArray = {e.getMessage()};
				log.error(messageAccessor.getMessage("EZ011004", objectgArray));
				throw new JspException(e);
			}
		}

		// 名称が"any"の場合はエラーとする。
		if (str.equals("any")) {
			errors.add(field.getKey(), Resources.getActionMessage(request, va, field));
			log.debug("nameChk1(anyでfalse)");
			return false;
		}
		log.debug("nameChk1(trueで終了)");
		return true;
	}

	/**
	 * メソッド名 : オブジェクト名称の入力チェック2
	 * 機能概要 : 以下の不正な名称が入力されていないかチェック
	 *			  ・"any"
	 *			  ・"application-default"
	 * @param bean
	 * @param va
	 * @param field
	 * @param errors
	 * @param validator
	 * @param request
	 * @return
	 * @throws JspException
	 */
	public static boolean objectNameChk2(Object bean, ValidatorAction va, Field field, ActionMessages errors, Validator validator, HttpServletRequest request)
		throws JspException {

		log.debug("nameChk2開始");

		// 検証対象の切り出し
		String str = null;
		if (bean instanceof String) {
			str = (String) bean;
		} else {
			try {
				str = PropertyUtils.getProperty(bean, field.getProperty()).toString();
			} catch (Exception e) {
				// 例外
				Object[] objectgArray = {e.getMessage()};
				log.error(messageAccessor.getMessage("EZ011004", objectgArray));
				throw new JspException(e);
			}
		}

		// 名称が"any"または"application-default"の場合はエラーとする。
		if (str.equals("any") || str.equals("application-default")) {
			errors.add(field.getKey(), Resources.getActionMessage(request, va, field));
			log.debug("nameChk1(anyまたはapplication-defaultでfalse)");
			return false;
		}
		log.debug("nameChk2(trueで終了)");
		return true;

	}

	/**
	 * メソッド名 : ポートの入力形式チェック
	 * 機能概要 : ポートの入力形式が、以下のいずれかにあてはまるかを判定する。
	 *			  ・パターン1 x（xは0～65535）
	 *			  ・パターン2 x-x（xは0～65535）
	 *			  ・パターン3 x,x（xは0～65535）
	 *			  ・パターン4 x-x,x-x（xは0～65535）
	 * @param bean
	 * @param va
	 * @param field
	 * @param errors
	 * @param validator
	 * @param request
	 * @return boolean ポートの入力形式が正しければtrue、誤っていればfalse
	 * @throws JspException
	 */
	public static boolean portFormatChk(Object bean, ValidatorAction va, Field field, ActionMessages errors, Validator validator, HttpServletRequest request)
	throws JspException
	{

		log.debug("portFormatChk開始");

		// 検証対象の切り出し
		String str = null;
		if (bean instanceof String) {
			str = (String) bean;
		} else {
			try {
				str = PropertyUtils.getProperty(bean, field.getProperty()).toString();
			} catch (Exception e) {
				// 例外
				Object[] objectgArray = {e.getMessage()};
				log.error(messageAccessor.getMessage("EZ011004", objectgArray));
				throw new JspException(e);
			}
		}

		// パターン1 x（xは0～65535）であるか判定
		Pattern pattern1 = Pattern.compile("\\d{1,5}?");
		Matcher matcher1 = pattern1.matcher(str);
		// パターン1の正規表現とマッチしているか判定
		if(matcher1.matches()){
			// 数値部分が0～65535の範囲内であるか判定
			if(numChk(Integer.parseInt(str))){
				log.debug("portFormatChk(パターン1でtrue)");
				return true;
			}
		}

		// パターン2 x-x（xは0～65535）であるか判定
		Pattern pattern2 = Pattern.compile("\\d{1,5}?-\\d{1,5}?");
		Matcher matcher2 = pattern2.matcher(str);
		// パターン2の正規表現とマッチしているか判定
		if(matcher2.matches()){
			String firstStr = str.substring(0,str.indexOf("-"));
			String secondStr = str.substring(str.indexOf("-")+1);
			// それぞれの数値部分が0～65535の範囲内であるか、また大小比較が問題ないか判定
			if(numChk(Integer.parseInt(firstStr))
				&& numChk(Integer.parseInt(secondStr))
//				&& (Integer.parseInt(firstStr) <= Integer.parseInt(secondStr))){
				&& (Integer.parseInt(firstStr) < Integer.parseInt(secondStr))){
				log.debug("portFormatChk(パターン2でtrue)");
				return true;
			}
		}

		// パターン3 x,x（xは0～65535）であるか判定
		Pattern pattern3 = Pattern.compile("\\d{1,5}?,\\d{1,5}?");
		Matcher matcher3 = pattern3.matcher(str);
		// パターン3の正規表現とマッチしているか判定
		if(matcher3.matches()){
			String firstStr = str.substring(0,str.indexOf(","));
			String secondStr = str.substring(str.indexOf(",")+1);
			// それぞれの数値部分が0～65535の範囲内であるか、また大小比較が問題ないか判定
			if(numChk(Integer.parseInt(firstStr))
				&& numChk(Integer.parseInt(secondStr))
//				&& (Integer.parseInt(firstStr) <= Integer.parseInt(secondStr))){
				&& (Integer.parseInt(firstStr) < Integer.parseInt(secondStr))){
				log.debug("portFormatChk(パターン3でtrue)");
				return true;
			}
		}

		// パターン4 x-x,x-x（xは0～65535）であるか判定
		Pattern pattern4 = Pattern.compile("\\d{1,5}?-\\d{1,5}?,\\d{1,5}?-\\d{1,5}?");
		Matcher matcher4 = pattern4.matcher(str);
		// パターン4の正規表現とマッチしているか判定
		if(matcher4.matches()){
			String firstStr = str.substring(0,str.indexOf("-"));
			String secondStr = str.substring(str.indexOf("-")+1, str.indexOf(","));
			String thirdStr = str.substring(str.indexOf(",")+1, str.lastIndexOf("-"));
			String fourthStr = str.substring(str.lastIndexOf("-")+1);
			// それぞれの数値部分が0～65535の範囲内であるか判定
			if(numChk(Integer.parseInt(firstStr))
				&& numChk(Integer.parseInt(secondStr))
				&& numChk(Integer.parseInt(thirdStr))
				&& numChk(Integer.parseInt(fourthStr))
//				&& (Integer.parseInt(firstStr) <= Integer.parseInt(secondStr))
//				&& (Integer.parseInt(thirdStr) <= Integer.parseInt(fourthStr))){
				&& (Integer.parseInt(firstStr) < Integer.parseInt(secondStr))
				&& (Integer.parseInt(thirdStr) < Integer.parseInt(fourthStr))){
				log.debug("portFormatChk(パターン4でtrue)");
				return true;
			}
		}
		errors.add(field.getKey(), Resources.getActionMessage(request, va, field));
		log.debug("portFormatChk終了(false)");
		return false;
	}

	/**
	 * メソッド名 : チェックボックスのチェック判定
	 * 機能概要 :  チェックボックスで1つ以上チェックされているか判定する。
	 * @param bean
	 * @param va
	 * @param field
	 * @param errors
	 * @param validator
	 * @param request
	 * @return boolean チェックボックスが1つ以上チェックされていればtrue、1つもチェックされていなければfalse
	 * @throws JspException
	 */
	public static boolean checkBoxChk(Object bean, ValidatorAction va, Field field, ActionMessages errors, Validator validator, HttpServletRequest request)
	throws JspException
	{

		log.debug("checkBoxChk開始");

		// 検証対象の切り出し
		String str = null;
		if (bean instanceof String) {
			str = (String) bean;
		} else {
			try {
				str = PropertyUtils.getProperty(bean, field.getProperty()).toString();
			} catch (Exception e) {
				// 例外
				Object[] objectgArray = {e.getMessage()};
				log.error(messageAccessor.getMessage("EZ011004", objectgArray));
				throw new JspException(e);
			}
		}

		// チェックボックスが選択されていなければエラーとする。
		if(str.length() == 0) {
			errors.add(field.getKey(), Resources.getActionMessage(request, va, field));
			log.debug("checkBoxChk(チェックボックスに１つも選択がなく、false)");
			return false;
		}

		log.debug("checkBoxChk(trueで終了)");
		return true;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor msgAcc){
		log.debug("setMessageAccessor処理開始");
		setMsgAcc(msgAcc);
		log.debug("setMessageAccessor処理終了");
	}

	/**
	 * メソッド名 : setMsgAcc
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public static void setMsgAcc(MessageAccessor messageAccessor) {
		log.debug("setMsgAcc処理開始");
		FwSettingExtendValidator.messageAccessor = messageAccessor;
		log.debug("setMsgAcc処理終了");
	}

	/**
	 * メソッド名 : getMessage
	 * 機能概要 : messageAccessorをゲットする。
	 * @return MessageAccessor
	 */
	public static MessageAccessor getMessage() {
		log.debug("getMessage処理");
		return messageAccessor;
	}

	/**
	 * メソッド名 : 数値判定
	 * 機能概要 : 数値が0～65535であるか判定
	 * @param int 数値
	 * @return boolean 数値が0～65535ならばtrue、それ以外ならばfalse
	 */
	private static boolean numChk(int num)
	{
		log.debug("private numChk開始");

		if(num >= 0 && num <= 65535){
			log.debug("private numChk(true)");
			return true;
		} else {
			log.debug("private numChk(false)");
			return false;
		}
	}

	// 20130107 kkato@PROSITE add start
    /**
     * URフィルタ設定制限文字が含まれているかをチェックするバリデータメソッド
     * @param bean
     * @param va
     * @param field
     * @param errors
     * @param validator
     * @param request
     * @return
     * @throws JspException
     * @version 1.0 URLフィルタ禁止文字列・パターンの追加
     *               Modified 2017/12/20 S.Yamada@Plum Systems Inc.
     */
    public static boolean urlFormatChk1(
            Object bean
            , ValidatorAction va
            , Field field
            , ActionMessages errors
            , Validator validator
            , HttpServletRequest request) throws JspException {
		log.debug("private urlFormatChk1開始");

		/** ASCII文字がマッチするパターン(0x21-0x7eで0x22以外) */
		Pattern asciiPattern = Pattern.compile("^[\\x21\\x23-\\x7e\n]*$");
		/** アスタリスク(0x2a)「*」の前後チェック */
		Pattern komePattern1 = Pattern.compile("[^\\x26\\x2e\\x2b\\x2f\\x3b\\x3d\\x3f]\\x2a");	//「*」の前が不許可文字
		Pattern komePattern2 = Pattern.compile("\\x2a[^\\x26\\x2e\\x2b\\x2f\\x3b\\x3d\\x3f]");	//「*」の後が不許可文字

		//2017/12/20 追加分(S.Yamada@Plum Systems Inc.)

		//禁止文字列の設定：全体チェック
		List<Pattern> wholeListChk = new ArrayList<Pattern>();
		wholeListChk.add(Pattern.compile("://"));
		wholeListChk.add(Pattern.compile(Pattern.quote("*&*")));
		wholeListChk.add(Pattern.compile(Pattern.quote("*+*")));
		wholeListChk.add(Pattern.compile(Pattern.quote("*.*")));
		wholeListChk.add(Pattern.compile(Pattern.quote("*/*")));
		wholeListChk.add(Pattern.compile(Pattern.quote("*;*")));
		wholeListChk.add(Pattern.compile(Pattern.quote("*=*")));
		wholeListChk.add(Pattern.compile(Pattern.quote("*?*")));

		//禁止文字列の設定：行毎チェック
		List<Pattern> lineListChk = new ArrayList<Pattern>();
		lineListChk.add(Pattern.compile(Pattern.quote("**")));
		lineListChk.add(Pattern.compile(Pattern.quote("^^")));
		lineListChk.add(Pattern.compile("(?=.*\\^)(?=.*\\*).*"));//「*」「^」混在

		//「*」「^」の上限設定値設定（初期値）
    	Pattern pattern_asterisk = Pattern.compile("^(.*\\*){3,}");
    	Pattern pattern_caret = Pattern.compile("^(.*\\^){10,}");

    	try{
	   	    // 「*」個数チェック
    		if(Pattern.compile("^([0-9]+$)").matcher(ASTERISK_NUMBER).find()){
	    	  int aster_limit_num = Integer.parseInt(ASTERISK_NUMBER) +1;
	    	  pattern_asterisk = Pattern.compile("^(.*\\*){"+ aster_limit_num +",}");
	    	  log.debug("*の利用可能上限値を「"+ ASTERISK_NUMBER + "」に設定しました。");
	    	}
    		// 「^」個数チェック
    		if(Pattern.compile("^([0-9]+$)").matcher(CARET_NUMBER).find()){
	    	  int caret_limit_num = Integer.parseInt(CARET_NUMBER) +1;
	    	  pattern_caret = Pattern.compile("^(.*\\^){"+ caret_limit_num +",}");
	    	  log.debug("^の利用可能上限値を「"+ CARET_NUMBER + "」に設定しました。");
	      }
    	} catch (Exception e) {
    		log.warn("プロパティファイルに設定されている上限設定値が不正です。");
    	}

		lineListChk.add(pattern_asterisk);
		lineListChk.add(pattern_caret);

		// bean(フォーム)から入力チェック対象の値を取得する
        String fieldValue = evaluateBean(bean, field);
        fieldValue = fieldValue.replace("\r", "");
        String[] arrValue = fieldValue.split("\n");

        // チェック対象の値が空の場合は入力チェックを行わない
        if (fieldValue == null || fieldValue.length() == 0) {
            return true;
        }

        // 使用可能文字かどうかチェックする
        if (asciiPattern.matcher(fieldValue).matches() == false) {
            // チェックNGの場合：エラーメッセージを追加してfalseを返す
            errors.add(field.getKey(), Resources.getActionMessage(validator, request, va, field));
            return false;
        }

		//禁止文字列の検索：全体チェック
        for (int i = 0; i < wholeListChk.size(); i++) {
        	if (wholeListChk.get(i).matcher(fieldValue).find()) {
        		log.debug(fieldValue + " ：検索条件 " +wholeListChk.get(i) +"と一致");
                errors.add(field.getKey(), Resources.getActionMessage(validator, request, va, field));
              	return false;
        	}
        }

		//禁止文字列の検索：行毎チェック
		for (String value : arrValue) {
			log.debug("対象URL:" + value);

	        for (int i = 0; i < lineListChk.size(); i++) {
	        	if (lineListChk.get(i).matcher(value).find()) {
	        		log.debug(value + " ：検索条件 " +lineListChk.get(i) +"と一致");
	                errors.add(field.getKey(), Resources.getActionMessage(validator, request, va, field));
	                return false;
	        	}
	        }
		}
		return true;
		//2017/12/20 追加分 end
    }

    /**
     * bean(フォーム)から入力チェック対象の値を取得する
     * @param bean
     * @param field
     * @return
     */
    private static String evaluateBean(Object bean, Field field)
	throws JspException {
        String value;

        if (bean == null || String.class.isInstance(bean)) {
            value = (String) bean;
        } else {
			try {
	            value = ValidatorUtils.getValueAsString(bean, field.getProperty());
			} catch (Exception e) {
				// 例外
				Object[] objectgArray = {e.getMessage()};
				log.error(messageAccessor.getMessage("EZ011004", objectgArray));
				throw new JspException(e);
			}
        }

        return value;
    }
	// 20130107 kkato@PROSITE add end

    // 20130901 kkato@PROSITE add start
	/**
	 * メソッド名 : 文字列フォーマットチェック2
	 * 機能概要 : 以下のチェックを行う。
	 *			  ①文字列が半角であること
	 * @param bean
	 * @param va
	 * @param field
	 * @param errors
	 * @param validator
	 * @param request
	 * @return boolean 文字列が半角のみで構成されていればtrue、それ以外の場合はfalse
	 * @throws JspException
	 */
	public static boolean strFormatChk2(Object bean, ValidatorAction va, Field field, ActionMessages errors, Validator validator, HttpServletRequest request)
	throws JspException
	{
		log.debug("strFormatChk2開始");

        // bean(フォーム)から入力チェック対象の値を取得する
        String fieldValue = evaluateBean(bean, field);
        if (fieldValue.length() == 0){
    		log.debug("strFormatChk2(未入力の為trueで終了)");
    		return true;
        }

		// 文字列がASCIIコードであることをチェック(x22ダブルクオート以外)
		Pattern asciiPattern = Pattern.compile("^[\\x20-\\x21\\x23-\\x7e]*$");
		Matcher matcher1 = asciiPattern.matcher(fieldValue);
		if (matcher1.find() == false) {
			errors.add(field.getKey(), Resources.getActionMessage(request, va, field));
			log.debug("strFormatChk2(文字列が半角以外でfalse)");
			return false;
		}

		log.debug("strFormatChk2(trueで終了)");
		return true;
	}
	// 20130901 kkato@PROSITE add end
}
