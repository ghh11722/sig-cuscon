/***s****************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateUrlFilterInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/13     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.vo.UrlFilters;

/**
 * クラス名 : UpdateUrlFilterInput
 * 機能概要 : Webフィルタ項目編集画面にて「OK」選択時の入力データクラス
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/13
 *          新規作成
 * @see
 */
public class UpdateUrlFilterInput {

	// CusconUVO
	private CusconUVO uvo = null;

	// ルール名
	private String name = null;

	// ラジオボタン選択
	private int radio = 0;

	// Webフィルタリスト
	private List<UrlFilters> urlFilterList= null;

	// Webフィルタ選択インデックス
	private int urlFilterSelectedIndex = 0;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}
	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * メソッド名 : radioのGetterメソッド
	 * 機能概要 : radioを取得する。
	 * @return radio
	 */
	public int getRadio() {
		return radio;
	}
	/**
	 * メソッド名 : radioのSetterメソッド
	 * 機能概要 : radioをセットする。
	 * @param radio
	 */
	public void setRadio(int radio) {
		this.radio = radio;
	}

	/**
	 * メソッド名 : urlFilterListのGetterメソッド
	 * 機能概要 : urlFilterListを取得する。
	 * @return urlFilterList
	 */
	public List<UrlFilters> getUrlFilterList() {
		return urlFilterList;
	}

	/**
	 * メソッド名 : urlFilterListのSetterメソッド
	 * 機能概要 : urlFilterListをセットする。
	 * @param urlFilterList
	 */
	public void setUrlFilterList(List<UrlFilters> urlFilterList) {
		this.urlFilterList = urlFilterList;
	}

	/**
	 * メソッド名 : urlFilterSelectedIndexのGetterメソッド
	 * 機能概要 : urlFilterSelectedIndexを取得する。
	 * @return urlFilterSelectedIndex
	 */
	public int geturlFilterSelectedIndex() {
		return urlFilterSelectedIndex;
	}

	/**
	 * メソッド名 : urlFilterSelectedIndexのSetterメソッド
	 * 機能概要 : urlFilterSelectedIndexをセットする。
	 * @param urlFilterSelectedIndex
	 */
	public void seturlFilterSelectedIndex(int urlFilterSelectedIndex) {
		this.urlFilterSelectedIndex = urlFilterSelectedIndex;
	}
}
