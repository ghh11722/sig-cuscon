/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ShowZoneOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/04     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectObjectList;

/**
 * クラス名 : ShowZoneOutput
 * 機能概要 :Zone項目選択時の出力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/04
 *          新規作成
 * @see
 */
public class ShowZoneOutput {

	// 出所ゾーンリスト
	private List<SelectObjectList> zoneList= null;

	// ゾーンマスタリスト
	private List<String> zoneMasterList = null;

	// ゾーン名
	private String zoneName = null;

	/**
	 * メソッド名 : zoneNameのGetterメソッド
	 * 機能概要 : zoneNameを取得する。
	 * @return zoneName
	 */
	public String getZoneName() {
		return zoneName;
	}

	/**
	 * メソッド名 : zoneNameのSetterメソッド
	 * 機能概要 : zoneNameをセットする。
	 * @param zoneName
	 */
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	/**
	 * メソッド名 : zoneListのGetterメソッド
	 * 機能概要 : zoneListを取得する。
	 * @return zoneList
	 */
	public List<SelectObjectList> getZoneList() {
		return zoneList;
	}

	/**
	 * メソッド名 : zoneListのSetterメソッド
	 * 機能概要 : zoneListをセットする。
	 * @param zoneList
	 */
	public void setZoneList(List<SelectObjectList> zoneList) {
		this.zoneList = zoneList;
	}

	/**
	 * メソッド名 : zoneMasterListのGetterメソッド
	 * 機能概要 : zoneMasterListを取得する。
	 * @return zoneMasterList
	 */
	public List<String> getZoneMasterList() {
		return zoneMasterList;
	}

	/**
	 * メソッド名 : zoneMasterListのSetterメソッド
	 * 機能概要 : zoneMasterListをセットする。
	 * @param zoneMasterList
	 */
	public void setZoneMasterList(List<String> zoneMasterList) {
		this.zoneMasterList = zoneMasterList;
	}
}