/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditSpywareFlgOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/05/18     ktakenaka@PROSITE                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto;

/**
 * クラス名 : EditSpywareOutput
 * 機能概要 :SpywareFlg項目選択時の出力データクラス
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/05/18
 *          新規作成
 * @see
 */
public class EditSpywareOutput {

	// ルール名
	private String name = null;

	// スパイウェアチェック利用フラグ
	private String spywareFlg = null;
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : spywareFlgのGetterメソッド
	 * 機能概要 : spywareFlgを取得する。
	 * @return spywareFlg
	 */
	public String getSpywareFlg() {
		return spywareFlg;
	}
	/**
	 * メソッド名 : spywareFlgのSetterメソッド
	 * 機能概要 : spywareFlgをセットする。
	 * @param spywareFlg
	 */
	public void setSpywareFlg(String spywareFlg) {
		this.spywareFlg = spywareFlg;
	}


}
