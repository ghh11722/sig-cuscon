/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : RegistLimitList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/04     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.common.vo;

import java.io.Serializable;

/**
 * クラス名 : RegistLimitList
 * 機能概要 : ポリシー/オブジェクト登録上限数データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/04
 *          新規作成
 * @see
 */
public class RegistLimitList implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = -4113993361235980500L;

	// FWポリシー登録上限値
	private int rule = 0;
	// アドレスOBJ登録上限値
	private int addr = 0;
	// アドレスグループOBJ登録上限値
	private int addrGrp = 0;
	// アドレスグループメンバOBJ登録上限値
	private int addrGrpMember = 0;
	// サービスOBJ登録上限値
	private int service = 0;
	// サービスグループOBJ登録上限値
	private int serviceGrp = 0;
	// サービスグループメンバOBJ登録上限値
	private int serviceGrpMember = 0;
	// アプリケーショングループOBJ登録上限値
	private int appGrp = 0;
	// アプリケーショングループメンバOBJ登録上限値
	private int appGrpMember = 0;
	// アプリケーションフィルタOBJ登録上限値
	private int appFilter = 0;
	// スケジュールOBJ登録上限値
	private int schedule = 0;

	/**
	 * メソッド名 : ruleのGetterメソッド
	 * 機能概要 : ruleを取得する。
	 * @return rule
	 */
	public int getRule() {
		return rule;
	}
	/**
	 * メソッド名 : ruleのSetterメソッド
	 * 機能概要 : ruleをセットする。
	 * @param rule
	 */
	public void setRule(int rule) {
		this.rule = rule;
	}
	/**
	 * メソッド名 : addrのGetterメソッド
	 * 機能概要 : addrを取得する。
	 * @return addr
	 */
	public int getAddr() {
		return addr;
	}
	/**
	 * メソッド名 : addrのSetterメソッド
	 * 機能概要 : addrをセットする。
	 * @param addr
	 */
	public void setAddr(int addr) {
		this.addr = addr;
	}
	/**
	 * メソッド名 : addrGrpのGetterメソッド
	 * 機能概要 : addrGrpを取得する。
	 * @return addrGrp
	 */
	public int getAddrGrp() {
		return addrGrp;
	}
	/**
	 * メソッド名 : addrGrpのSetterメソッド
	 * 機能概要 : addrGrpをセットする。
	 * @param addrGrp
	 */
	public void setAddrGrp(int addrGrp) {
		this.addrGrp = addrGrp;
	}
	/**
	 * メソッド名 : addrGrpMemberのGetterメソッド
	 * 機能概要 : addrGrpMemberを取得する。
	 * @return addrGrpMember
	 */
	public int getAddrGrpMember() {
		return addrGrpMember;
	}
	/**
	 * メソッド名 : addrGrpMemberのSetterメソッド
	 * 機能概要 : addrGrpMemberをセットする。
	 * @param addrGrpMember
	 */
	public void setAddrGrpMember(int addrGrpMember) {
		this.addrGrpMember = addrGrpMember;
	}
	/**
	 * メソッド名 : serviceのGetterメソッド
	 * 機能概要 : serviceを取得する。
	 * @return service
	 */
	public int getService() {
		return service;
	}
	/**
	 * メソッド名 : serviceのSetterメソッド
	 * 機能概要 : serviceをセットする。
	 * @param service
	 */
	public void setService(int service) {
		this.service = service;
	}
	/**
	 * メソッド名 : serviceGrpのGetterメソッド
	 * 機能概要 : serviceGrpを取得する。
	 * @return serviceGrp
	 */
	public int getServiceGrp() {
		return serviceGrp;
	}
	/**
	 * メソッド名 : serviceGrpのSetterメソッド
	 * 機能概要 : serviceGrpをセットする。
	 * @param serviceGrp
	 */
	public void setServiceGrp(int serviceGrp) {
		this.serviceGrp = serviceGrp;
	}
	/**
	 * メソッド名 : serviceGrpMemberのGetterメソッド
	 * 機能概要 : serviceGrpMemberを取得する。
	 * @return serviceGrpMember
	 */
	public int getServiceGrpMember() {
		return serviceGrpMember;
	}
	/**
	 * メソッド名 : serviceGrpMemberのSetterメソッド
	 * 機能概要 : serviceGrpMemberをセットする。
	 * @param serviceGrpMember
	 */
	public void setServiceGrpMember(int serviceGrpMember) {
		this.serviceGrpMember = serviceGrpMember;
	}
	/**
	 * メソッド名 : appGrpのGetterメソッド
	 * 機能概要 : appGrpを取得する。
	 * @return appGrp
	 */
	public int getAppGrp() {
		return appGrp;
	}
	/**
	 * メソッド名 : appGrpのSetterメソッド
	 * 機能概要 : appGrpをセットする。
	 * @param appGrp
	 */
	public void setAppGrp(int appGrp) {
		this.appGrp = appGrp;
	}
	/**
	 * メソッド名 : appGrpMemberのGetterメソッド
	 * 機能概要 : appGrpMemberを取得する。
	 * @return appGrpMember
	 */
	public int getAppGrpMember() {
		return appGrpMember;
	}
	/**
	 * メソッド名 : appGrpMemberのSetterメソッド
	 * 機能概要 : appGrpMemberをセットする。
	 * @param appGrpMember
	 */
	public void setAppGrpMember(int appGrpMember) {
		this.appGrpMember = appGrpMember;
	}
	/**
	 * メソッド名 : appFilterのGetterメソッド
	 * 機能概要 : appFilterを取得する。
	 * @return appFilter
	 */
	public int getAppFilter() {
		return appFilter;
	}
	/**
	 * メソッド名 : appFilterのSetterメソッド
	 * 機能概要 : appFilterをセットする。
	 * @param appFilter
	 */
	public void setAppFilter(int appFilter) {
		this.appFilter = appFilter;
	}
	/**
	 * メソッド名 : scheduleのGetterメソッド
	 * 機能概要 : scheduleを取得する。
	 * @return schedule
	 */
	public int getSchedule() {
		return schedule;
	}
	/**
	 * メソッド名 : scheduleのSetterメソッド
	 * 機能概要 : scheduleをセットする。
	 * @param schedule
	 */
	public void setSchedule(int schedule) {
		this.schedule = schedule;
	}
}
