/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名	: UpdatePA.java
 *
 * [変更履歴]
 * 日付 		  更新者				  内容
 * 2010/02/19	  komakiys		   初版作成
 * 2010/06/22	  oohashij		   別プロセス起動処理方式へ変更に伴う修正
 * 2012/05/04	  inoue@prosite    PAの接続、コマンド発行のexceptionエラー時、リトライ数繰り返し処理を追加
  *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.common;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import jp.co.kddi.secgw.cuscon.common.CusconMaintenanceMode;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectSystemConstantList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.CommandPara;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.CommitFlg;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;
import jp.terasoluna.fw.util.PropertyUtil;

/**
 * クラス名 : UpdatePA
 * 機能概要 : PAの設定全削除、及びDBの指定世代にて<br>
 *			  PAの設定を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *			Created 2010/02/19
 *			新規作成
 * @version 1.1 oohashij
 *			Created 2010/06/22
 *			別プロセス起動処理方式へ変更に伴う修正
 * @see
 */
public class UpdatePA {

	// 日時フォーマット
	private static final String DATE_FORMAT =
		PropertyUtil.getProperty("fwsetting.common.dateformat");

	// コマンド待ち時間
	private static int WAIT_COMMAND = 0;

	// メッセージクラス
	private static MessageAccessor messageAccessor = null;

	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.common.UpdatePA");

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 * @param cusconUVO
	 * @param queryDAO
	 */
	public UpdatePA() {
		log.debug("UpdatePAコンストラクタ1処理開始");
		log.debug("UpdatePAコンストラクタ1処理終了");
	}

	/**
	 * メソッド名 : updatePAProcess
	 * 機能概要 : 現在のPAの設定を全て削除し、<br>
	 *			  generationNoの世代の情報にてPAを更新する。
	 * @param resultJudge 結果
	 * @param loginId PAログインID
	 * @param password PAパスワード
	 * @param queryDAO QueryDAO
	 * @param uvo CusconUVO
	 * @param commit_flg commit判定フラグ
	 * @throws Exception
	 */
	public void updatePAProcess(ResultJudgeData resultJudge, String loginId, String password, QueryDAO queryDAO, CusconUVO uvo, CommitFlg commit_flg) throws Exception{

		log.debug("updatePAProcess処理開始");
		// コマンドタイムアウト時間
		int timeout = 0;

		// メンテナンスモードチェッククラス
		CusconMaintenanceMode mode = new CusconMaintenanceMode(queryDAO);
		try {
			// メンテナンスモードの場合
			if(mode.isMaintenanceMode()) {
				// メンテナンスモードのため、コミット不可
				log.debug(messageAccessor.getMessage("DZ010001", null, uvo));

				resultJudge.setResultFlg(1);
				resultJudge.setResultData("DZ010001");
				log.debug("updatePAProcess処理終了:flg=1, DZ010001");
				return;
			}
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FZ011036", objectgArray, uvo));
			throw e;
		}

		// PA接続情報を取得する。
		SelectSystemConstantList connectionInfo =
									getConnectionInfo(
											queryDAO, uvo, loginId, password);

		// 他利用者コミット中の場合(DB)
		if(!checkDbCommitProcess(connectionInfo)) {

				// 他利用者コミット中のため、コミット不可
				log.debug(messageAccessor.getMessage("DZ010002", null, uvo));

				resultJudge.setResultFlg(2);
				resultJudge.setResultData("DZ010002");
				log.debug("updatePAProcess処理終了:flg=2, DZ010002");
				return;
		}
		// 処理終了時にコミットフラグOFFが必要
		commit_flg.setCommtFlg(true);

		// 参照・削除コマンド用セッション生成
		CommandPara delcompara = new CommandPara();
		delcompara.setConnectionInfo(connectionInfo);
		delcompara.setUvo(uvo);
		// コマンドタイムアウト時間
		timeout = connectionInfo.getTimeout() * 60 * 1000;
		delcompara.setTimeout(timeout);
		delcompara.setWaitCommand(WAIT_COMMAND);

//2012.05.04 rep start inoue@prosite ※PA接続でException発生時、リトライする
//	  	CommandExecute delcommand = new CommandExecute(delcompara);
		// 他利用者コミット中の場合(PALO)
//	  	if(!checkCommitProcess(connectionInfo, uvo, delcommand, delcompara, timeout)) {
	   	if(!checkCommitProcess(connectionInfo, uvo, delcompara, timeout)) {
//2012.05.04 rep end inoue@prosite

			// 他利用者コミット中のため、コミット不可
			log.debug(messageAccessor.getMessage("DZ010002", null, uvo));

			resultJudge.setResultFlg(2);
			resultJudge.setResultData("DZ010002");
			log.debug("updatePAProcess処理終了:flg=2, DZ010002");
//2012.05.04 comment start inoue@prosite
			// 参照・削除コマンド用セッション切断
//		  	delcommand.disconnect(delcompara);
//2012.05.04 comment end inoue@prosite
			return;
		}
//2012.05.04 comment start inoue@prosite
		// 参照・削除コマンド用セッション切断
//	  	delcommand.disconnect(delcompara);
//2012.05.04 comment end inoue@prosite

		Date date = new Date();

		// 時刻フォーマットを定義する。
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);

		// 現在時刻を返却する。
		dateFormat.format(date);

		// 結果を格納する。
		resultJudge.setResultFlg(0);
		// 現在時刻を返却する。
		resultJudge.setResultData(dateFormat.format(date));

		log.debug("updatePAProcess処理終了:flg=2,updateTime = " +
													dateFormat.format(date));
		return;
	}

	/**
	 * メソッド名 : getConnectionInfo
	 * 機能概要 : DBよりPA接続情報を取得し、返却する。
	 * @param loginId PAログインID
	 * @param password PAパスワード
	 * @return SelectSystemConstantList PA接続情報
	 * @throws Exception
	 */
	private SelectSystemConstantList getConnectionInfo(QueryDAO queryDAO, CusconUVO uvo,
									String loginId, String password) throws Exception {
		log.debug("getConnectionInfo処理開始: loginId = " +
									loginId + ",password = " + password);

		// PAのホスト、ユーザID、パスワードを取得する。
		// 復号化鍵
		String key = "kddi-secgw";
		// PA接続情報
		List<SelectSystemConstantList> connectionList = null;

		try {
			// PA接続情報を取得する。
			connectionList =
				queryDAO.executeForObjectList("CommonM_SystemConstant-1", key);
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FZ011033", objectgArray, uvo));
			throw e;
		}

		// 接続情報を戻り値に格納する。
		connectionList.get(0).setLoginId(loginId);
		connectionList.get(0).setPassword(password);
		WAIT_COMMAND = connectionList.get(0).getCmdWait();
		log.debug("getConnectionInfo処理終了");
		return connectionList.get(0);
	}

	/**
	 * メソッド名 : checkCommitProcess
	 * 機能概要 : 他利用者がコミット中でないかチェックを行う。<br>
	 *			  他利用者がコミット中の場合、指定回数リトライを行う。
	 * @param connectionInfo PA接続情報
	 * @param timeout
	 * @return boolean チェック結果
	 * @throws Exception
	 */
//2012.05.04 rep start inoue@prosite 'パラメータ変更
//	private boolean checkCommitProcess(
//			SelectSystemConstantList connectionInfo, CusconUVO uvo,
//			CommandExecute command, CommandPara compara, int timeout) throws Exception{
	private boolean checkCommitProcess(
			SelectSystemConstantList connectionInfo, CusconUVO uvo,
			CommandPara compara, int timeout) throws Exception{
//2012.05.04 rep end inoue@prosite
		log.debug("checkCommitProcess処理開始");

		// 他利用者コミット確認コマンド
		String commitCom =
			PropertyUtil.getProperty("fwsetting.common.commitConfirm");

		// 他利用者コミット確認(正規表現)
		String checkCommit =
					PropertyUtil.getProperty("fwsetting.common.checkcommit");

		// 他利用者コミット確認出現箇所
		int checkNum = Integer.parseInt(
				PropertyUtil.getProperty("fwsetting.common.checkcommitnum"));

		// 他利用者コミット中表示
		String act =
			PropertyUtil.getProperty("fwsetting.common.commitProcess");

		if (log.isDebugEnabled()) {
			log.debug("他利用者コミット確認コマンド:" + commitCom);
			log.debug("他利用者コミット確認出現箇所:" + checkNum);
			log.debug("他利用者コミット確認(正規表現):" + checkCommit);
			log.debug("他利用者コミット中表示:" + act);
		}

		 // リトライ回数
		int retryNum = connectionInfo.getRetryNum();
		// ウェイト時間(ミリ秒⇒秒へ変換する)
		int waitTime = connectionInfo.getWaitTime() * 1000;
//2012.05.04 add start inoue@prosite
		int exclErrretryNum = connectionInfo.getexclErrretryNum();
		int exclErrwaitTime = connectionInfo.getexclErrwaitTime() * 1000;
//2012.05.04 add end inoue@prosite

		if (log.isDebugEnabled()) {
			log.debug("リトライ回数:" + retryNum);
			log.debug("ウェイト時間:" + waitTime);
			log.debug("コマンドタイムアウト時間:" + timeout);
//2012.05.04 add start inoue@prosite
			log.debug("排他コマンドエラーリトライ回数:" + exclErrretryNum);
			log.debug("排他コマンドエラーウェイト時間:" + exclErrwaitTime);
//2012.05.04 add end inoue@prosite
		}

		// 他利用者チェック処理(指定回数チェックを行う)
		for(int i=0; i<retryNum+1; i++) {
			log.debug("他利用者チェック カウント:" + i);

//2012.05.04 add start inoue@prosite
			// 他利用者チェック処理(指定回数チェックを行う)
			for(int k=0; k<exclErrretryNum+1; k++) {
				log.info(uvo.getGrantFlagStr() + " " + uvo.getLoginId() + " " + uvo.getVsysId() + " " +
						"排他チェックコマンド発行 カウント:" + k);
				boolean exception_flg = false;
				try {
					CommandExecute delcommand = new CommandExecute(compara);
					String[] commitComList = commitCom.split(",");
					String[] checkCommitList = checkCommit.split(",");
					boolean processing_flg = false;
					for (int j =0; j < commitComList.length; j++) {
						Object[] objectgArray = {commitComList[j]};
						log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));

						// コマンドリスト
						List<String> comList = new ArrayList<String>();
						// 他利用者コミット確認コマンド
						comList.add(commitComList[j]);

						// コマンドを実行し、レスポンスを取得する。
						String response = null;
						response = delcommand.getCommandResponce(compara, comList);

						// レスポンスを分割する。
						String[] split_res = response.split(checkCommitList[j]);

						// コミットJOBが存在する場合
						if (split_res.length >= checkNum + 1) {
							log.debug("check対象JOBが存在");
							// コミット中の場合
							if (split_res[checkNum].indexOf(act) != -1) {
								log.debug("check対象JOBがACT");
								processing_flg = true;
								break;
							}
						}
					}
					// 参照・削除コマンド用セッション切断
					delcommand.disconnect(compara);
					if (!processing_flg) {
						if (k!=0){
							log.info(uvo.getGrantFlagStr() + " " + uvo.getLoginId() + " " + uvo.getVsysId() + " " +
									"checkCommitProcess JOBチェック エラーリトライ成功");
						}
						log.debug("checkCommitProcess処理終了:true");
						return true;
					}
				} catch (Exception e) {
					log.info(uvo.getGrantFlagStr() + " " + uvo.getLoginId() + " " + uvo.getVsysId() + " " +
							"checkCommitProcess 例外発生 リトライカウント:" + k);
					exception_flg = true;
					if (k==exclErrretryNum) {
						log.info(uvo.getGrantFlagStr() + " " + uvo.getLoginId() + " " + uvo.getVsysId() + " " +
								"checkCommitProcess JOBチェック リトライオーバー:" + k);
						throw e;
					}
				}
				// exceptionエラー発生時、ウェイト
				if(exception_flg){
					// 排他コマンドエラー待ちウェイト時間待つ
					log.info(uvo.getGrantFlagStr() + " " + uvo.getLoginId() + " " + uvo.getVsysId() + " " +
							"checkCommitProcess 排他チェックコマンドエラーウェイト");
					Thread.sleep(exclErrwaitTime);
				}
			}
			if (i==retryNum) {
				break;
			}
			// 指定時間待つ
			log.debug("排他ウェイト");
			Thread.sleep(waitTime);
		}
		log.debug("checkCommitProcess処理終了:false");
		return false;
//2012.05.04 add end inoue@prosite


//2012.05.04 comment start inoue@prosite
/*
   			String[] commitComList = commitCom.split(",");
			String[] checkCommitList = checkCommit.split(",");
			boolean processing_flg = false;
			for (int j =0; j < commitComList.length; j++) {
				Object[] objectgArray = {commitComList[j]};
				log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));

				// コマンドリスト
				List<String> comList = new ArrayList<String>();
				// 他利用者コミット確認コマンド
				comList.add(commitComList[j]);

				// コマンドを実行し、レスポンスを取得する。
				String response =
						command.getCommandResponce(compara, comList);

				// レスポンスを分割する。
				String[] split_res = response.split(checkCommitList[j]);

				// コミットJOBが存在する場合
				if (split_res.length >= checkNum + 1) {
					log.debug("check対象JOBが存在");
					// コミット中の場合
					if (split_res[checkNum].indexOf(act) != -1) {
						log.debug("check対象JOBがACT");
						processing_flg = true;
						break;
					}
				}
			}
			if (!processing_flg) {
				log.debug("checkCommitProcess処理終了:true");
				return true;
			}
			if (i==retryNum) {
				break;
			}
   			// 指定時間待つ
   			Thread.sleep(waitTime);
		}
		log.debug("checkCommitProcess処理終了:false");
		return false;
*/
	}

	/**
	 * メソッド名 : checkDbCommitProcess
	 * 機能概要 : 他利用者がコミット中でないかチェックを行う。<br>
	 *			  他利用者がコミット中の場合、指定回数リトライを行う。
	 * @param connectionInfo PA接続情報
	 * @return boolean チェック結果
	 * @throws Exception
	 */
	private boolean checkDbCommitProcess(
			SelectSystemConstantList connectionInfo) throws Exception {
		log.debug("checkDbCommitProcess処理開始");

		 // リトライ回数
		int retryNum = connectionInfo.getRetryNum();
		// ウェイト時間(秒⇒ミリ秒へ変換する)
		int waitTime = connectionInfo.getWaitTime() * 1000;

		if (log.isDebugEnabled()) {
			log.debug("リトライ回数:" + retryNum);
			log.debug("ウェイト時間:" + waitTime);
		}

		UpdateCommitFlg ucf = new UpdateCommitFlg();

		// 他利用者チェック処理(指定回数チェックを行う)
		for(int i=0; i<retryNum+1; i++) {
			log.debug("他利用者チェック カウント:" + i);

			// コミット中でない場合
			if(ucf.updatePAProcessFlg(CusconConst.COMMIT_ON) == 0) {
				log.debug("checkDbCommitProcess処理終了2 :true");
				return true;
			}
			if (i==retryNum) {
				break;
			}
			// 指定時間待つ
			Thread.sleep(waitTime);
		}
		log.debug("checkDbCommitProcess処理終了:false");

		return false;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor msgAcc){
		log.debug("setMessageAccessor処理開始");
		setMsgAcc(msgAcc);
		log.debug("setMessageAccessor処理終了");
	}

	/**
	 * メソッド名 : setMsgAcc
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public static void setMsgAcc(MessageAccessor messageAccessor) {
		log.debug("setMsgAcc処理開始");
		UpdatePA.messageAccessor = messageAccessor;
		log.debug("setMsgAcc処理終了");
	}

	/**
	 * メソッド名 : getMessage
	 * 機能概要 : messageAccessorをゲットする。
	 * @return MessageAccessor
	 */
	public static MessageAccessor getMessage() {
		log.debug("getMessage処理");
		return messageAccessor;
	}
}


