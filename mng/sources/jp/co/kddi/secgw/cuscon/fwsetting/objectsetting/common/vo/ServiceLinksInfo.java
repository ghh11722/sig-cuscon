/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectServiceLinks.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/23     morisou                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo;

import java.io.Serializable;

/**
 * クラス名 : SelectServiceLinks
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/23
 *          新規作成
 * @see
 */
public class ServiceLinksInfo implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = -3045015674256066824L;
	// T_ServicesGroupsLinkテーブルのリンクシーケンス番号
	private int grpLinkSeqNo = 0;
	// T_ServiceLinkテーブルのリンクシーケンス番号
	private int srvLinkSeqNo = 0;
	/**
	 * メソッド名 : grpLinkSeqNoのGetterメソッド
	 * 機能概要 : grpLinkSeqNoを取得する。
	 * @return grpLinkSeqNo
	 */
	public int getGrpLinkSeqNo() {
		return grpLinkSeqNo;
	}
	/**
	 * メソッド名 : grpLinkSeqNoのSetterメソッド
	 * 機能概要 : grpLinkSeqNoをセットする。
	 * @param grpLinkSeqNo
	 */
	public void setGrpLinkSeqNo(int grpLinkSeqNo) {
		this.grpLinkSeqNo = grpLinkSeqNo;
	}
	/**
	 * メソッド名 : srvLinkSeqNoのGetterメソッド
	 * 機能概要 : srvLinkSeqNoを取得する。
	 * @return srvLinkSeqNo
	 */
	public int getSrvLinkSeqNo() {
		return srvLinkSeqNo;
	}
	/**
	 * メソッド名 : srvLinkSeqNoのSetterメソッド
	 * 機能概要 : srvLinkSeqNoをセットする。
	 * @param srvLinkSeqNo
	 */
	public void setSrvLinkSeqNo(int srvLinkSeqNo) {
		this.srvLinkSeqNo = srvLinkSeqNo;
	}
}
