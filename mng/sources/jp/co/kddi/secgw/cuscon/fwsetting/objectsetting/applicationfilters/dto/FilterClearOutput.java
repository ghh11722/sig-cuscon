/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ClearOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationList4Search;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectFilterMasterList;

/**
 * クラス名 : FilterClearOutput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class FilterClearOutput {

	// アプリケーションフィルタ名
	private String name = null;
	// フィルタリスト
	private SelectFilterMasterList filterList = null;
	// アプリケーションリスト
	private List<SelectApplicationList4Search> applicationList = null;

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : filterListのGetterメソッド
	 * 機能概要 : filterListを取得する。
	 * @return filterList
	 */
	public SelectFilterMasterList getFilterList() {
		return filterList;
	}
	/**
	 * メソッド名 : filterListのSetterメソッド
	 * 機能概要 : filterListをセットする。
	 * @param filterList
	 */
	public void setFilterList(SelectFilterMasterList filterList) {
		this.filterList = filterList;
	}
	/**
	 * メソッド名 : applicationListのGetterメソッド
	 * 機能概要 : applicationListを取得する。
	 * @return applicationList
	 */
	public List<SelectApplicationList4Search> getApplicationList() {
		return applicationList;
	}
	/**
	 * メソッド名 : applicationListのSetterメソッド
	 * 機能概要 : applicationListをセットする。
	 * @param applicationList
	 */
	public void setApplicationList(List<SelectApplicationList4Search> applicationList) {
		this.applicationList = applicationList;
	}
}
