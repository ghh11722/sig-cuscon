/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : DescriptionChkValidator.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/13     kinoshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.form;

import jp.terasoluna.fw.web.struts.form.MultiFieldValidator;

/**
 * クラス名 : DescriptionChkValidator
 * 機能概要 : ポリシー設定(アドレス編集画面)のチェックを行う。
 * 備考 :
 * @author kinoshtt
 * @version 1.0 kinoshtt
 *          Created 2010/03/13
 *          新規作成
 * @see
 */
public class DescriptionChkValidator implements MultiFieldValidator {

	/**
	 * メソッド名 :validate
	 * 機能概要 :追加アドレスの形式チェックを行う。
	 * @param description 備考
	 * @param name ポリシー名
	 * @return チェック結果
	 * @see jp.terasoluna.fw.web.struts.form.MultiFieldValidator#validate(java.lang.String, java.lang.String[])
	 */
	@Override
	public boolean validate(String description, String[] fields) {

		// ポリシー名備考を取得する。
		String name = fields[0];

		// ポリシー名が入力されていて、かつ備考が256桁以上の場合エラー
		if(!name.trim().isEmpty() && description.length() > 255) {
       		return false;
		}
		return true;
	}
}
