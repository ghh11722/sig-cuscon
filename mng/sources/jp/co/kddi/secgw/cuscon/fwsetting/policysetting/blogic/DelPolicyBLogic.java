/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : DelPolicyBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic;

import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.SecurityRules;
import jp.co.kddi.secgw.cuscon.fwsetting.common.PolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.DelPolicyInput;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.PolicyOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : DelPolicyBLogic
 * 機能概要 :ポリシー削除処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class DelPolicyBLogic implements BLogic<DelPolicyInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;

	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic.DelPolicyBLogic");
	private MessageAccessor messageAccessor = null;
	/**
	 * メソッド名 :execute
	 * 機能概要 :ポリシー一覧情報を削除する。
	 * @param param ポリシー削除画面入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(DelPolicyInput param) {

		log.debug("DelPolicyBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");
        // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();

        SecurityRules base = new SecurityRules();
        // 検索条件を設定する。
    	base.setVsysId(param.getUvo().getVsysId());
    	base.setGenerationNo(CusconConst.ZERO_GENE);
    	base.setModFlg(CusconConst.DEL_NUM);
    	base.setName(param.getName());

    	try {
	    	// ポリシー一覧の変更フラグを3に更新する。
	    	updateDAO.execute("DelPolicyBLogic-1", base);

	    	// 変更フラグを更新したポリシーのシーケンス番号を取得する。
	    	base.setModName(param.getName());
	    	List<SecurityRules> updatePolicyList = null;

	    	updatePolicyList = queryDAO.executeForObjectList("PolicyRulesList-1", base);

	    	// 削除対象シーケンス番号
	    	int delSeqNo = updatePolicyList.get(0).getSeqNo();

	    	// 取得したシーケンス番号に紐付くローカル出所アドレスを削除する。
	    	updateDAO.execute("DelPolicyBLogic-2", delSeqNo);
	    	// 取得したシーケンス番号に紐付くローカル宛先アドレスを削除する。
	    	updateDAO.execute("DelPolicyBLogic-3", delSeqNo);
	    	// 取得したシーケンス番号に紐付く出所ゾーンリンクを削除する。
	    	updateDAO.execute("DelPolicyBLogic-4", delSeqNo);
	    	// 取得したシーケンス番号に紐付く宛先ゾーンリンクを削除する。
	    	updateDAO.execute("DelPolicyBLogic-5", delSeqNo);
	    	// 取得したシーケンス番号に紐付く出所アドレスリンクを削除する。
	    	updateDAO.execute("DelPolicyBLogic-6", delSeqNo);
	    	// 取得したシーケンス番号に紐付く宛先アドレスリンクを削除する。
	    	updateDAO.execute("DelPolicyBLogic-7", delSeqNo);
	    	// 取得したシーケンス番号に紐付くアプリケーションリンクを削除する。
	    	updateDAO.execute("DelPolicyBLogic-8", delSeqNo);
	    	// 取得したシーケンス番号に紐付くサービスリンクを削除する。
	    	updateDAO.execute("DelPolicyBLogic-9", delSeqNo);
//2011/07/25 add start (ポリシーを削除してもWebフィルタリンクが残るバグ対応)
	    	updateDAO.execute("DelPolicyBLogic-WebFilter", delSeqNo);
//2011/07/25 add end   (ポリシーを削除してもWebフィルタリンクが残るバグ対応)

	    	// 削除したポリシーより大きい行番号のポリシーの行番号を-1する。
	    	base.setLineNo(updatePolicyList.get(0).getLineNo());
	    	updateDAO.execute("DelPolicyBLogic-10", base);
    	} catch (Exception e) {
    		// トランザクションロールバック
        	TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
        	Object[] objectgArray = {e.getMessage()};
        	log.fatal(messageAccessor.getMessage(
        			"FK051004", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK050009"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
    	}

        // ポリシーリストクラスを生成する。
        PolicyList policy = new PolicyList();

        List<SelectPolicyList> policyList = null;

        try {
        	// 選択された世代番号のポリシーリストを取得する。
	        policyList = policy.getPolicylist(queryDAO, param.getUvo(),
	        			CusconConst.ZERO_GENE, CusconConst.POLICY);
    	} catch (Exception e) {
			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"EK051001", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK050009"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
    	}

        // ビジネスロジックの出力クラスに結果を設定する
        PolicyOutput out = new PolicyOutput();
        out.setPolicyList(policyList);
        result.setResultObject(out);
        result.setResultString("success");

        log.debug("DelPolicyBLogic処理終了");
		return result;
    }

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}