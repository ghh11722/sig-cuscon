/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateAddressGroupBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/27     morisou                 初版作成
 * 2016/12/13     T.Yamazaki@Plum Systems アドレスグループ除外対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.blogic;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.AddressGroups;
import jp.co.kddi.secgw.cuscon.common.vo.AddressGroupsLink;
import jp.co.kddi.secgw.cuscon.common.vo.Addresses;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.RegistLimitList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.AddressGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.AddressesAndGroupsInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.dto.AddressGroupsOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.dto.UpdateAddressGroupInput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : UpdateAddressGroupBLogic
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/27
 *          新規作成
 *          2.0 T.Yamazaki@Plum Systems Inc.
 *          Updated 2016/12/13
 *          アドレスグループ除外対応
 * @see
 */
public class UpdateAddressGroupBLogic implements BLogic<UpdateAddressGroupInput> {

	// オブジェクトタイプ定数
	private static final int ADDRESS = 0;
	private static final int ADDRESS_GROUP = 1;

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.blogic.UpdateAddressGroupBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 :BLogicResult
	 * 機能概要 :
	 * @param params
	 * @return
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(UpdateAddressGroupInput params) {
		log.debug("UpdateAddressGroupBLogic処理開始");

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		 // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");
		AddressGroupsOutput output = new AddressGroupsOutput();

		// セッション情報.CusconUVOからVSYS_IDを取得
		CusconUVO uvo = params.getUvo();
		String vsysId = uvo.getVsysId();


		// チェック対象アドレス格納用リスト宣言
		List<AddressesAndGroupsInfo> selectAddressList =
							new ArrayList<AddressesAndGroupsInfo>();

		// チェック対象のアドレスのインデックスを取得
        String[] indexList = params.getCheckIndex().split(",");

        // チェック対象のデータを格納
        for(int i=0; i< indexList.length; i++) {
        	log.debug("チェック対象のデータ格納:" + i);
        	selectAddressList.add(
        			params.getAddressAndAddressGrp4Add().get(
        							Integer.parseInt(indexList[i])));
        }

		/***********************************************************************
		 *  ①メンバ登録上限チェック
		 **********************************************************************/
        // 登録上限値を取得する。
        List<RegistLimitList> limitList;
        try {
	        // 登録上限値を取得する。
	        limitList =
	        	queryDAO.executeForObjectList("CommonM_SystemConstant-2", null);
        } catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage(
								"FK061007", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060017"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
        }

        InputBase inputBase = new InputBase();
        inputBase.setVsysId(params.getUvo().getVsysId());
        inputBase.setGenerationNo(CusconConst.ZERO_GENE);
        inputBase.setModFlg(CusconConst.DEL_NUM);

        // 登録上限値とオブジェクト登録数の比較を行う。
        if(limitList.get(0).getAddrGrpMember() < selectAddressList.size()) {
        	log.debug("登録上限値のため登録できませんでした。");
        	messages.add("message", new BLogicMessage("DK060058"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
        }


		/***********************************************************************
		 *  ②複数テーブル間存在チェック
		 **********************************************************************/
		// T_Addressesに今回登録したいフィルタ名と同名のレコードがないか検索
		// 検索条件設定
		Addresses addresses = new Addresses();
		addresses.setName(params.getNewAddressGroupName());
		addresses.setVsysId(vsysId);
		addresses.setGenerationNo(CusconConst.ZERO_GENE);
		addresses.setModFlg(CusconConst.DEL_NUM);

		List<AddressGroups> list;
		try {
			list = queryDAO.executeForObjectList(
					"AddressesBLogic-9", addresses);
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage(
								"FK061007", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060017"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
        }

		// 検索結果が0件でない場合
		if (list.size() > 0) {
			// 重複エラーとしてレスポンスデータを作成し返却
			log.debug("既に該当オブジェクト名が存在する。");
        	messages.add("message", new BLogicMessage("DK060001"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}


		/***********************************************************************
		 *  ③存在チェック
		 **********************************************************************/
		// 検索条件設定
		AddressGroups searchAddressGroupKey = new AddressGroups();
		// Vsys-ID
		searchAddressGroupKey.setVsysId(vsysId);
		// 世代番号
		searchAddressGroupKey.setGenerationNo(CusconConst.ZERO_GENE);
		// アドレスグループ名
		searchAddressGroupKey.setName(params.getNewAddressGroupName());

		// 差分比較処理
		// 変更前と変更後のアドレス名が異なる場合
		if (!params.getNewAddressGroupName().equals(
									params.getOldAddressGroupName())){
			log.debug("変更前と変更後のアドレス名が異なる");
			// アドレスグループオブジェクトテーブルに対し、
			// 変更後アドレスグループ名と同名のレコードが既に存在しているか検索
			List<AddressGroups> addressGrpObjectList;
			try {
				addressGrpObjectList =
						queryDAO.executeForObjectList(
								"AddressGroupsBLogic-6", searchAddressGroupKey);
			} catch(Exception e) {
				// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
				// DBアクセスエラー
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage(
									"FK061007", objectgArray, params.getUvo()));
				messages.add("message", new BLogicMessage("DK060017"));
				result.setErrors(messages);
				result.setResultString("failure");
	        	return result;
	        }

			/*******************************************************************
			 *  ④存在チェックエラー処理
			 ******************************************************************/
			// DB検索結果が1件以上かつ、MOD_FLGのステータスが削除でない場合
			if (!addressGrpObjectList.isEmpty() &&
					addressGrpObjectList.get(0).getModFlg() !=
														CusconConst.DEL_NUM) {
				// 重複エラーとしてレスポンスデータを作成し返却
				log.debug("既に該当オブジェクト名が存在する。");
	        	messages.add("message", new BLogicMessage("DK060001"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}


			/*******************************************************************
			 *  ⑤不要レコード削除処理
			 ******************************************************************/
			// DB検索結果が1件以上かつ、MOD_FLGのステータスが削除の場合
			if (!addressGrpObjectList.isEmpty() &&
					addressGrpObjectList.get(0).getModFlg() == CusconConst.DEL_NUM) {
				log.debug("DB検索結果が1件以上かつ、MOD_FLGのステータスが削除");

				try {
					// SEQ_NOをキーに、UpdateDAOクラスを使用し、アドレスグループテーブルのレコードを削除
					updateDAO.execute("AddressGroupsBLogic-7",
								addressGrpObjectList.get(0).getSeqNo());

					// 紐づくリンク情報を削除
					updateDAO.execute("AddressGroupsBLogic-5",
								addressGrpObjectList.get(0).getSeqNo());
				} catch(Exception e) {
					// トランザクションロールバック
					TransactionUtil.setRollbackOnly();
					// DBアクセスエラー
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage(
										"FK061007", objectgArray, params.getUvo()));
					messages.add("message", new BLogicMessage("DK060017"));
					result.setErrors(messages);
					result.setResultString("failure");
		        	return result;
		        }
			}
		}

		/***********************************************************************
		 *  ⑥更新処理
		 **********************************************************************/
		// 検索条件設定
		AddressGroups searchOldAddressGroupKey = new AddressGroups();
		// Vsys-ID
		searchOldAddressGroupKey.setVsysId(vsysId);
		// 世代番号
		searchOldAddressGroupKey.setGenerationNo(CusconConst.ZERO_GENE);
		// 変更前アドレス名
		searchOldAddressGroupKey.setName(params.getOldAddressGroupName());
		// 変更前アドレスグループ情報を取得
		List<AddressGroups> addressGrpObjectList;
		try {
			// 変更前アドレスグループ情報を取得
			addressGrpObjectList = queryDAO.executeForObjectList(
					"AddressGroupsBLogic-6", searchOldAddressGroupKey);

			// 取得したレコードの変更フラグが追加の場合（更新フラグは書き換えずupdate）
			if (addressGrpObjectList.get(0).getModFlg() == CusconConst.ADD_NUM) {
				log.debug("変更フラグが1");
				// update用param作成
				AddressGroups updateParamSp = new AddressGroups();
				// シーケンス番号
				updateParamSp.setSeqNo(addressGrpObjectList.get(0).getSeqNo());
				// アドレスグループ名
				updateParamSp.setName(params.getNewAddressGroupName());

				updateDAO.execute("AddressGroupsBLogic-9", updateParamSp);

			// 取得したレコードの変更フラグが追加でない場合（更新フラグも書き換えてupdate）
			} else {
				log.debug("変更フラグが1以外");
				// update用param作成
				AddressGroups updateParam = new AddressGroups();
				// シーケンス番号
				updateParam.setSeqNo(addressGrpObjectList.get(0).getSeqNo());
				// アドレスグループ名
				updateParam.setName(params.getNewAddressGroupName());
				// 更新フラグ
				updateParam.setModFlg(CusconConst.MOD_NUM);

				updateDAO.execute("AddressGroupsBLogic-10", updateParam);
			}
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage(
								"FK061007", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060017"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
        }

		/***********************************************************************
		 *  ⑦リンク情報削除処理
		 **********************************************************************/
		try {
			updateDAO.execute("AddressGroupsBLogic-5",
					addressGrpObjectList.get(0).getSeqNo());
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage(
								"FK061007", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060017"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
        }


		/***********************************************************************
		 *  ⑧リンク情報追加処理
		 **********************************************************************/
		// リンク情報を登録
//		List<SeqAndNameInfo> selectAddressList = params.getSelectAddressList();

		// UPDATED: 2016.12 by Plum Systems Inc.
		// アドレスグループ除外対応 -->
		for (int i = 0; i < selectAddressList.size(); i++) {
			log.debug("リンク情報登録:" + i);
			AddressGroupsLink addressGroupsLink = new AddressGroupsLink();
			// 画面でアドレスが選択されている場合
			if (selectAddressList.get(i).getObjectType() == ADDRESS) {
				log.debug("アドレスを選択");
				// アドレス用のオブジェクトタイプを設定
				addressGroupsLink.setObjectType(ADDRESS);

				// アドレスグループシーケンス番号を設定
				addressGroupsLink.setAdrGrpSeqNo(addressGrpObjectList.get(0).getSeqNo());
				// リンクシーケンス番号を設定
				addressGroupsLink.setLinkSeqNo(selectAddressList.get(i).getSeqNo());

				try {
					// リンク情報登録
					updateDAO.execute("AddressGroupsBLogic-8", addressGroupsLink);
				} catch(Exception e) {
					// トランザクションロールバック
					TransactionUtil.setRollbackOnly();
					// DBアクセスエラー
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage(
										"FK061007", objectgArray, params.getUvo()));
					messages.add("message", new BLogicMessage("DK060017"));
					result.setErrors(messages);
					result.setResultString("failure");
		        	return result;
		        }
			}
			// 画面でアドレスグループが選択されている場合
			if (selectAddressList.get(i).getObjectType() == ADDRESS_GROUP) {
				log.debug("アドレスグループを選択");
				/*
				// アドレスグループ用のオブジェクトタイプを設定
				addressGroupsLink.setObjectType(ADDRESS_GROUP);
				*/
				log.debug("アドレスグループへのアドレスグループ追加は無効です。");
			}
		}
		// --> アドレスグループ除外対応

		/***********************************************************************
		 *  ⑨一覧情報検索共通処理
		 **********************************************************************/
		try {
			AddressGroupList addressGroupList = new AddressGroupList();
			output.setAddressGroupList(addressGroupList.getAddressGroupList(queryDAO, uvo));
		} catch(Exception e) {
			// 例外
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage(
								"EK061002", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060011"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}


		/***********************************************************************
		 *  ⑩レスポンス返却
		 **********************************************************************/
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("UpdateAddressGroupBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
