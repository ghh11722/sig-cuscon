/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateAddressBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/18     morisou                 初版作成
 * 2013/09/01     kkato@PROSITE           FQDN登録上限チェック追加
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.blogic;

import java.util.List;
import org.apache.log4j.Logger;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.AddressGroups;
import jp.co.kddi.secgw.cuscon.common.vo.Addresses;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.dto.AddressesOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.dto.UpdateAddressInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.AddressList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectAddressList;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : UpdateAddressBLogic
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/18
 *          新規作成
 * @see
 */
public class UpdateAddressBLogic implements BLogic<UpdateAddressInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.blogic.UpdateAddressBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 :BLogicResult
	 * 機能概要 :
	 * @param params
	 * @return
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(UpdateAddressInput params) {
		log.debug("UpdateAddressBLogic処理開始");

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		 // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得
		CusconUVO uvo = params.getUvo();
		String vsysId = uvo.getVsysId();

		/***********************************************************************
		 *  ①複数テーブル間存在チェック
		 **********************************************************************/
		// T_AddressGroupsに今回登録したいフィルタ名と同名のレコードがないか検索
		// 検索条件設定
		AddressGroups addressGroups = new AddressGroups();
		addressGroups.setName(params.getNewAddressName());
		addressGroups.setVsysId(vsysId);
		addressGroups.setGenerationNo(CusconConst.ZERO_GENE);
		addressGroups.setModFlg(CusconConst.DEL_NUM);

		try{
			List<AddressGroups> grpList = queryDAO.executeForObjectList(
					"AddressGroupsBLogic-12", addressGroups);

			// 検索結果が0件でない場合
			if (grpList.size() > 0) {
				// 重複エラーとしてレスポンスデータを作成し返却
				log.debug("①複数テーブル間存在チェックで存在チェックエラーです");
	        	messages.add("message", new BLogicMessage("DK060001"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}
        } catch(Exception e) {
			// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
        	// 登録失敗ログ出力
        	Object[] objectgArray = {e.getMessage()};
        	log.fatal(messageAccessor.getMessage(
        			"FK061003", objectgArray, params.getUvo()));

        	messages.add("message", new BLogicMessage("DK060010"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
        }

		/***********************************************************************
		 *  ②存在チェック
		 **********************************************************************/
		// 検索条件設定
		Addresses searchAddressKey = new Addresses();
		searchAddressKey.setVsysId(vsysId);                            // Vsys-ID
		searchAddressKey.setGenerationNo(CusconConst.ZERO_GENE);       // 世代番号
		searchAddressKey.setName(params.getNewAddressName());          // アドレス名

		try{
			// 差分比較処理
			// 変更前と変更後のアドレス名が異なる場合
			if (!params.getNewAddressName().equals(params.getOldAddressName())){
				// アドレスオブジェクトテーブルに対し、変更後アドレス名と同名のレコードが既に存在しているか検索
				List<Addresses> addressList = queryDAO.executeForObjectList("AddressesBLogic-1", searchAddressKey);


				/*******************************************************************
				 *  ③存在チェックエラー処理
				 ******************************************************************/
				// DB検索結果が1件以上かつ、MOD_FLGのステータスが削除でない場合
				if (! addressList.isEmpty() && addressList.get(0).getModFlg() != CusconConst.DEL_NUM) {
					// 重複エラーとしてレスポンスデータを作成し返却
					log.debug("③存在チェックエラー処理で存在チェックエラーです");
		        	messages.add("message", new BLogicMessage("DK060001"));
					result.setErrors(messages);
					result.setResultString("failure");
					return result;
				}


				/*******************************************************************
				 *  ④不要レコード削除処理
				 ******************************************************************/
				// DB検索結果が1件以上かつ、MOD_FLGのステータスが削除の場合
				if (!addressList.isEmpty() && addressList.get(0).getModFlg() == CusconConst.DEL_NUM) {
					// SEQ_NOをキーに、UpdateDAOクラスを使用し、アドレステーブルのレコードを削除
					updateDAO.execute("AddressesBLogic-2", addressList.get(0).getSeqNo());
					log.debug("DB検索結果が1件以上かつ、MOD_FLGのステータスが削除で、不要レコード削除処理です");
				}
			}
        } catch(Exception e) {
        	// トランザクションロールバック
        	TransactionUtil.setRollbackOnly();
        	// 登録失敗ログ出力
        	Object[] objectgArray = {e.getMessage()};
        	log.fatal(messageAccessor.getMessage(
        			"FK061003", objectgArray, params.getUvo()));

        	messages.add("message", new BLogicMessage("DK060010"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
        }

        // 20130901 kkato@PROSITE add start
		/***********************************************************************
		 *  ⑤FQDN登録上限チェック
		 **********************************************************************/
		try{
			if(params.getType().equals(CusconConst.ADDRESS_FQDN)) {
				// 検索条件設定
				Addresses searchAddressFqdn = new Addresses();
				searchAddressFqdn.setVsysId(vsysId);                            // Vsys-ID
				searchAddressFqdn.setGenerationNo(CusconConst.ZERO_GENE);       // 世代番号
				searchAddressFqdn.setModFlg(CusconConst.DEL_NUM);               // MODFLG
				searchAddressFqdn.setType(CusconConst.ADDRESS_FQDN);            // TYPE
				searchAddressFqdn.setName(params.getOldAddressName());          // NAME
				// アドレスオブジェクトテーブルに対し、FQDN軒数取得
				int registNum = queryDAO.executeForObject("AddressesBLogic-11", searchAddressFqdn
						, java.lang.Integer.class);

				// DB検索結果が上限値以上の場合
				if(uvo.getAddressFqdnMax() <= registNum) {
					log.debug("登録上限値のため登録できませんでした。");
					messages.add("message", new BLogicMessage("DK060003"));
					result.setErrors(messages);
					result.setResultString("failure");
					return result;
				}
			}
        } catch(Exception e) {
        	// トランザクションロールバック
        	TransactionUtil.setRollbackOnly();
        	// 登録失敗ログ出力
        	Object[] objectgArray = {e.getMessage()};
        	log.fatal(messageAccessor.getMessage(
        			"FK061001", objectgArray, params.getUvo()));

        	messages.add("message", new BLogicMessage("DK060006"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
        }
        // 20130901 kkato@PROSITE add end

        /***********************************************************************
		 *  ⑥更新処理
		 **********************************************************************/
		// 検索条件設定
		Addresses searchOldAddressKey = new Addresses();
		searchOldAddressKey.setVsysId(vsysId);                           // Vsys-ID
		searchOldAddressKey.setGenerationNo(CusconConst.ZERO_GENE);      // 世代番号
		searchOldAddressKey.setName(params.getOldAddressName());         // 変更前アドレス名

		try{
			// 変更前アドレス情報を取得
			List<Addresses> oldAddressList = queryDAO.executeForObjectList("AddressesBLogic-1", searchOldAddressKey);

			// 取得したレコードの変更フラグが追加の場合（更新フラグは書き換えずupdate）
			if (oldAddressList.get(0).getModFlg() == CusconConst.ADD_NUM) {
				// update用param作成
				log.debug("取得したレコードの変更フラグが追加の場合（更新フラグは書き換えずupdate）です");
				Addresses updateParamSp = new Addresses();
				updateParamSp.setSeqNo(oldAddressList.get(0).getSeqNo());     // シーケンス番号
				updateParamSp.setName(params.getNewAddressName());            // アドレス名
				updateParamSp.setType(params.getType());                      // タイプ
				updateParamSp.setAddress(params.getAddress());                // アドレス

				updateDAO.execute("AddressesBLogic-3", updateParamSp);

			// 取得したレコードの変更フラグが追加でない場合（更新フラグも書き換えてupdate）
			} else {
				// update用param作成
				log.debug("取得したレコードの変更フラグが追加でない場合（更新フラグも書き換えてupdate）です");
				Addresses updateParam = new Addresses();
				updateParam.setSeqNo(oldAddressList.get(0).getSeqNo());      // シーケンス番号
				updateParam.setName(params.getNewAddressName());             // アドレス名
				updateParam.setType(params.getType());                       // タイプ
				updateParam.setAddress(params.getAddress());                 // アドレス
				updateParam.setModFlg(CusconConst.MOD_NUM);                  // 更新フラグ

				updateDAO.execute("AddressesBLogic-4", updateParam);
			}
        } catch(Exception e) {
        	// トランザクションロールバック
        	TransactionUtil.setRollbackOnly();
        	// 登録失敗ログ出力
        	Object[] objectgArray = {e.getMessage()};
        	log.fatal(messageAccessor.getMessage(
        			"FK061003", objectgArray, params.getUvo()));

        	messages.add("message", new BLogicMessage("DK060010"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
        }

		/***********************************************************************
		 *  ⑦一覧情報検索共通処理
		 **********************************************************************/
		AddressesOutput output = new AddressesOutput();
		try{
	        AddressList addressList = new AddressList();
			List<SelectAddressList> addressesOutputList =
				addressList.getAddressList(queryDAO, uvo);
			output.setAddressList(addressesOutputList);
		} catch(Exception e) {
			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"EK061001", objectgArray, params.getUvo()));
        	messages.add("message", new BLogicMessage("DK060004"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

		/***********************************************************************
		 *  ⑧レスポンス返却
		 **********************************************************************/
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("UpdateAddressBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
