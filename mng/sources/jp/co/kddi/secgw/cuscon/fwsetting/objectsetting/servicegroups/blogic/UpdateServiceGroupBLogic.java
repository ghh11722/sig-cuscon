/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateServiceGroupBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/27     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.servicegroups.blogic;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.common.vo.ServiceGroups;
import jp.co.kddi.secgw.cuscon.common.vo.ServiceGroupsLink;
import jp.co.kddi.secgw.cuscon.common.vo.Services;
import jp.co.kddi.secgw.cuscon.common.vo.ServicesMaster;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.RegistLimitList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.ServiceGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.ServicesAndGroupsInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.servicegroups.dto.ServiceGroupsOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.servicegroups.dto.UpdateServiceGroupInput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : UpdateServiceGroupBLogic
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/27
 *          新規作成
 * @see
 */
public class UpdateServiceGroupBLogic implements BLogic<UpdateServiceGroupInput> {

	// オブジェクトタイプ定数
	private static final int SERVICE_MASTER = 0;    // サービスマスタ
	private static final int SERVICE_OBJECT = 1;    // サービスオブジェクト
	private static final int SERVICE_GROUP = 2;     // サービスグループオブジェクト

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting." +
			"servicegroups.blogic.UpdateServiceGroupBLogic");

	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 :BLogicResult
	 * 機能概要 :
	 * @param params
	 * @return
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(UpdateServiceGroupInput params) {

		log.debug("UpdateServiceGroupBLogic処理開始");
		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		 // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得
		CusconUVO uvo = params.getUvo();
		String vsysId = uvo.getVsysId();

		// チェック対象サービス格納用リスト宣言
		List<ServicesAndGroupsInfo> selectServiceList = new ArrayList<ServicesAndGroupsInfo>();

		// チェック対象のサービスのインデックスを取得
        String[] indexList = params.getCheckIndex().split(",");

        // チェック対象のデータを格納
        for(int i=0; i< indexList.length; i++) {
        	log.debug("チェック対象のデータを格納");
        	selectServiceList.add(params.getServiceAndServiceGrp4Add().get(Integer.parseInt(indexList[i])));
        }

		/***********************************************************************
		 *  ①メンバ登録上限チェック
		 **********************************************************************/
        // 登録上限値を取得する。
        List<RegistLimitList> limitList;
        try {
	        limitList =
	        	queryDAO.executeForObjectList("CommonM_SystemConstant-2", null);
        } catch(Exception e) {
        	// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
        	// DBアクセスエラーログ出力
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061024", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060045"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
        }

        InputBase inputBase = new InputBase();
        inputBase.setVsysId(params.getUvo().getVsysId());
        inputBase.setGenerationNo(CusconConst.ZERO_GENE);
        inputBase.setModFlg(CusconConst.DEL_NUM);

        // 登録上限値とオブジェクト登録数の比較を行う。
        if(limitList.get(0).getServiceGrpMember() < selectServiceList.size()) {
        	log.debug("メンバ登録上限チェックエラー");
        	messages.add("message", new BLogicMessage("DK060058"));
			result.setErrors(messages);
			result.setResultString("failure");
    		return result;
        }


		/***********************************************************************
		 *  ②複数テーブル間存在チェック
		 **********************************************************************/
		// T_Servicesに今回登録したいサービス名と同名のレコードがないか検索
		// 検索条件設定
		Services services = new Services();
		services.setName(params.getNewServiceGroupName());
		services.setVsysId(vsysId);
		services.setGenerationNo(CusconConst.ZERO_GENE);
		services.setModFlg(CusconConst.DEL_NUM);
		List<Services> list;
		List<ServicesMaster> masterList;

		try {
			list = queryDAO.executeForObjectList(
					"ServicesBLogic-11", services);

			// M_Servicesに今回登録したいサービス名と同名のレコードがないか検索
			// 検索条件設定
			ServiceGroups serviceGroups = new ServiceGroups();
			serviceGroups.setName(params.getNewServiceGroupName());
			serviceGroups.setVsysId(vsysId);
			serviceGroups.setGenerationNo(CusconConst.ZERO_GENE);
			serviceGroups.setModFlg(CusconConst.DEL_NUM);
			masterList = queryDAO.executeForObjectList(
					"ServicesBLogic-12", serviceGroups);
		} catch(Exception e) {
        	// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
       	// DBアクセスエラーログ出力
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061024", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060045"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
        }

		// 検索結果が0件でない場合
		if (list.size() > 0 || masterList.size() > 0) {
			// 重複エラーとしてレスポンスデータを作成し返却
			log.debug("複数テーブル間存在チェックエラー");
        	messages.add("message", new BLogicMessage("DK060068"));
			result.setErrors(messages);
			result.setResultString("failure");
    		return result;
		}


		/***********************************************************************
		 *  ③存在チェック
		 **********************************************************************/
		// 検索条件設定
		ServiceGroups searchServiceGroupKey = new ServiceGroups();
		searchServiceGroupKey.setVsysId(vsysId);                         // Vsys-ID
		searchServiceGroupKey.setGenerationNo(CusconConst.ZERO_GENE);    // 世代番号
		searchServiceGroupKey.setName(params.getNewServiceGroupName());              // サービスグループ名

		// 差分比較処理
		// 変更前と変更後のアドレス名が異なる場合
		if (!params.getNewServiceGroupName().equals(params.getOldServiceGroupName())){
			log.debug("変更前と変更後のアドレス名が異なる");
			// サービスグループオブジェクトテーブルに対し、変更後サービスグループ名と同名のレコードが既に存在しているか検索
			List<ServiceGroups> serviceGrpObjectList;
			try {
				// サービスグループオブジェクトテーブルに対し、変更後サービスグループ名と同名のレコードが既に存在しているか検索
				serviceGrpObjectList = queryDAO.executeForObjectList(
						"ServiceGroupsBLogic-2", searchServiceGroupKey);
			} catch(Exception e) {
	        	// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
	        	// DBアクセスエラーログ出力
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FK061024", objectgArray, params.getUvo()));
				messages.add("message", new BLogicMessage("DK060045"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
	        }
			/*******************************************************************
			 *  ④存在チェックエラー処理
			 ******************************************************************/
			// DB検索結果が1件以上かつ、MOD_FLGのステータスが削除でない場合
			if (!serviceGrpObjectList.isEmpty() && serviceGrpObjectList.get(0).
					getModFlg() != CusconConst.DEL_NUM) {
				// 重複エラーとしてレスポンスデータを作成し返却
				log.debug("既に該当オブジェクト名が存在する。");
	        	messages.add("message", new BLogicMessage("DK060001"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}

			/*******************************************************************
			 *  ⑤不要レコード削除処理
			 ******************************************************************/
			// DB検索結果が1件以上かつ、MOD_FLGのステータスが削除の場合
			if (!serviceGrpObjectList.isEmpty() &&
					serviceGrpObjectList.get(0).getModFlg() == CusconConst.DEL_NUM) {
				log.debug("DB検索結果が1件以上かつ、MOD_FLGのステータスが削除");

				try {
					// SEQ_NOをキーに、UpdateDAOクラスを使用し、アドレスグループテーブルのレコードを削除
					updateDAO.execute("ServiceGroupsBLogic-6", serviceGrpObjectList.get(0).getSeqNo());

					// 紐づくリンク情報を削除
					updateDAO.execute("ServiceGroupsBLogic-4", serviceGrpObjectList.get(0).getSeqNo());
				} catch(Exception e) {
					// トランザクションロールバック
		        	TransactionUtil.setRollbackOnly();
		        	// DBアクセスエラーログ出力
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage("FK061022", objectgArray, params.getUvo()));
					messages.add("message", new BLogicMessage("DK060045"));
					result.setErrors(messages);
					result.setResultString("failure");
					return result;
		        }
			}
		}


		/***********************************************************************
		 *  ⑥更新処理
		 **********************************************************************/
		// 検索条件設定
		ServiceGroups searchOldServiceGroupKey = new ServiceGroups();
		searchOldServiceGroupKey.setVsysId(vsysId);                            // Vsys-ID
		searchOldServiceGroupKey.setGenerationNo(CusconConst.ZERO_GENE);       // 世代番号
		searchOldServiceGroupKey.setName(params.getOldServiceGroupName());                 // 変更前サービスグループ名

		// 変更前アドレス情報を取得
		List<ServiceGroups> serviceGrpObjectList;
		try {
			// 変更前アドレス情報を取得
			serviceGrpObjectList = queryDAO.executeForObjectList(
					"ServiceGroupsBLogic-2", searchOldServiceGroupKey);

			// 取得したレコードの変更フラグが追加の場合（更新フラグは書き換えずupdate）
			if (serviceGrpObjectList.get(0).getModFlg() == CusconConst.ADD_NUM) {
				log.debug("変更フラグが追加");
				// update用param作成
				ServiceGroups updateParamSp = new ServiceGroups();
				updateParamSp.setSeqNo(serviceGrpObjectList.get(0).getSeqNo());  // シーケンス番号
				updateParamSp.setName(params.getNewServiceGroupName());                      // サービスグループ名

				updateDAO.execute("ServiceGroupsBLogic-9", updateParamSp);

			// 取得したレコードの変更フラグが追加でない場合（更新フラグも書き換えてupdate）
			} else {
				log.debug("変更フラグが追加以外");
				// update用param作成
				ServiceGroups updateParam = new ServiceGroups();
				updateParam.setSeqNo(serviceGrpObjectList.get(0).getSeqNo());    // シーケンス番号
				updateParam.setName(params.getNewServiceGroupName());                        // サービスグループ名
				updateParam.setModFlg(CusconConst.MOD_NUM);                      // 更新フラグ

				updateDAO.execute("ServiceGroupsBLogic-8", updateParam);
			}

			/***********************************************************************
			 *  ⑦リンク情報削除処理
			 **********************************************************************/
			// サービスグループに紐づくリンク情報削除
			updateDAO.execute("ServiceGroupsBLogic-4",
					serviceGrpObjectList.get(0).getSeqNo());
		} catch(Exception e) {
			// トランザクションロールバック
        	TransactionUtil.setRollbackOnly();
        	// DBアクセスエラーログ出力
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061023", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060045"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
        }

		/***********************************************************************
		 *  ⑧リンク情報追加処理
		 **********************************************************************/
		// リンク情報を登録
//		List<SeqAndNameInfo> selectServiceList = params.getSelectServiceList();

		for (int i = 0; i < selectServiceList.size(); i++) {
			log.debug("リンク情報を登録:" + i);
			ServiceGroupsLink serviceGroupsLink = new ServiceGroupsLink();
			// 画面でサービスマスタが選択されている場合
			if (selectServiceList.get(i).getObjectType() == SERVICE_MASTER) {
				log.debug("サービスマスタ選択:" + i);
				// サービスマスタ用のオブジェクトタイプを設定
				serviceGroupsLink.setObjectType(SERVICE_MASTER);
			}
			// 画面でサービスが選択されている場合
			if (selectServiceList.get(i).getObjectType() == SERVICE_OBJECT) {
				log.debug("サービス選択:" + i);
				// サービス用のオブジェクトタイプを設定
				serviceGroupsLink.setObjectType(SERVICE_OBJECT);
			}
			// 画面でサービスグループが選択されている場合
			if (selectServiceList.get(i).getObjectType() == SERVICE_GROUP) {
				log.debug("サービスグループ選択:" + i);
				// サービスグループ用のオブジェクトタイプを設定
				serviceGroupsLink.setObjectType(SERVICE_GROUP);
			}
			// サービスグループシーケンス番号を設定
			serviceGroupsLink.setSrvGrpSeqNo(serviceGrpObjectList.get(0).getSeqNo());
			// リンクシーケンス番号を設定
			serviceGroupsLink.setLinkSeqNo(selectServiceList.get(i).getSeqNo());

			try {
				// リンク情報登録
				updateDAO.execute("ServiceGroupsBLogic-7", serviceGroupsLink);
			} catch(Exception e) {
				// トランザクションロールバック
	        	TransactionUtil.setRollbackOnly();
	        	// DBアクセスエラーログ出力
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FK061021", objectgArray, params.getUvo()));
				messages.add("message", new BLogicMessage("DK060045"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
	        }
		}


		/***********************************************************************
		 *  ⑨一覧情報検索共通処理
		 **********************************************************************/
		ServiceGroupsOutput output = new ServiceGroupsOutput();
		try {
			ServiceGroupList serviceList = new ServiceGroupList(messageAccessor);
			output.setServiceGroupList(serviceList.getServiceGroupList(queryDAO, uvo));
		} catch(Exception e) {
			// 例外
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("EK061006", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060045"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

		/***********************************************************************
		 *  ⑩レスポンス返却
		 **********************************************************************/
		result.setResultObject(output);
		result.setResultString("success");
		log.debug("UpdateServiceGroupBLogic処理終了");
		return result;
	}


	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("queryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("queryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
