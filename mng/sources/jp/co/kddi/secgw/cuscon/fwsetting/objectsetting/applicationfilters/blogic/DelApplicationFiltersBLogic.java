/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : DelApplicationFiltersBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/23     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.blogic;

import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.ApplicationFilters;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.dto.ApplicationFiltersOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.dto.DelApplicationFiltersInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.ApplicationFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.ApplicationFilterLinksInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationFilterList;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;
import org.apache.log4j.Logger;

/**
 * クラス名 : DelApplicationFiltersBLogic
 * 機能概要 : 選択したオブジェクトの削除を行う。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/23
 *          新規作成
 * @see
 */
public class DelApplicationFiltersBLogic implements BLogic<DelApplicationFiltersInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.blogic.DelApplicationFiltersBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 : 選択したオブジェクトの削除を行う。
	 * @param params DelApplicationFiltersInput 入力データクラス
	 * @return result BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(DelApplicationFiltersInput params) {

		log.debug("DelApplicationFiltersBLogic処理開始");

		int seqNo;     // シーケンス番号
		int count;     // リンク数

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		 // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得
		CusconUVO uvo = params.getUvo();
		String vsysId = uvo.getVsysId();

		// 画面から渡されたアプリケーションフィルタリストを取得
		List<SelectApplicationFilterList> applicationFilterList4Del = params.getApplicationFilterList();

		int delSize = applicationFilterList4Del.size();
		int delNum = 0;
		String delStr = "";
		// 画面から渡されたアプリケーションフィルタリストの件数分、以下の処理を実行
		for (int i = 0; i < applicationFilterList4Del.size(); i++) {
			// 検索条件設定
			ApplicationFilters searchApplicationFilterKey = new ApplicationFilters();
			searchApplicationFilterKey.setVsysId(vsysId);                         // Vsys-ID
			searchApplicationFilterKey.setGenerationNo(CusconConst.ZERO_GENE);    // 世代番号
			searchApplicationFilterKey.setName(applicationFilterList4Del.get(i).getName());    // アプリケーションフィルタ名

			List<ApplicationFilters> applicationFilterList;

			try {
				// アプリケーションフィルタオブジェクトテーブルを検索し、削除対象アプリケーションフィルタのSEQ_NOを取得
				applicationFilterList = queryDAO.executeForObjectList(
						"ApplicationFiltersBLogic-2", searchApplicationFilterKey);

				seqNo = applicationFilterList.get(0).getSeqNo();

				// 検索条件設定
				ApplicationFilterLinksInfo selectApplicationFilterLinks = new ApplicationFilterLinksInfo();
				selectApplicationFilterLinks.setGrpLinkSeqNo(seqNo);
				selectApplicationFilterLinks.setAplLinkSeqNo(seqNo);

				// 当該アプリケーションフィルタがリンクされている件数を取得
				count= queryDAO.executeForObject("ApplicationFiltersBLogic-3",
						selectApplicationFilterLinks,  java.lang.Integer.class);

			} catch(Exception e) {
	    		// トランザクションロールバック
	    		TransactionUtil.setRollbackOnly();
				// DBアクセスエラー
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FK061010", objectgArray, params.getUvo()));
				messages.add("message", new BLogicMessage("DK060022"));
				result.setErrors(messages);
				result.setResultString("failure");
	        	return result;
			}

			// 削除不可エラー
			if (count > 0) {
				log.debug("オブジェクト削除不可:" + applicationFilterList4Del.get(i).getName());
				delNum++;

				// 削除名称を連結する。
				if(delNum == 1) {
					log.debug("削除名称設定(1回目:" + applicationFilterList4Del.get(i).getName());
					delStr = applicationFilterList4Del.get(i).getName();
				} else {
					log.debug("削除名称設定(2回目以降:" + applicationFilterList4Del.get(i).getName());
					delStr = delStr + ", " + applicationFilterList4Del.get(i).getName();
				}
				if(delSize == delNum) {
		    		// トランザクションロールバック
		    		TransactionUtil.setRollbackOnly();
					// 検索結果が1件以上の場合、削除不可エラー
					// レスポンスデータを作成し返却
					// 削除不可エラーメッセージを出力する。
					Object[] objectgArray = {delStr};
					log.debug(messageAccessor.getMessage(
		        			"DK060002", objectgArray, params.getUvo()));
					messages.add("message",new BLogicMessage("DK060070", objectgArray));
					result.setErrors(messages);
					result.setResultString("failure");
		        	return result;
				}
			// 削除可能
			} else {
				log.debug("削除処理実施：" + applicationFilterList4Del.get(i).getName());
				try {
					// アプリケーションフィルタに紐づくリンク情報削除（T_AplFltCategoryLink）
					updateDAO.execute("ApplicationFiltersBLogic-5", seqNo);
					// アプリケーションフィルタに紐づくリンク情報削除（T_AplFltSubCategoryLink）
					updateDAO.execute("ApplicationFiltersBLogic-6", seqNo);
					// アプリケーションフィルタに紐づくリンク情報削除（T_AplFltTechnologyLink）
					updateDAO.execute("ApplicationFiltersBLogic-7", seqNo);
					// アプリケーションフィルタに紐づくリンク情報削除（T_AplFltRiskLink）
					updateDAO.execute("ApplicationFiltersBLogic-8", seqNo);
					// アプリケーションフィルタオブジェクトテーブルのレコードの変更フラグを'3'（削除）に更新
					updateDAO.execute("ApplicationFiltersBLogic-4", seqNo);

				} catch (Exception e) {
					// トランザクションロールバック
					TransactionUtil.setRollbackOnly();
					// 例外発生
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage("FK061010", objectgArray, params.getUvo()));
					messages.add("message", new BLogicMessage("DK060022"));
					result.setErrors(messages);
					result.setResultString("failure");
		        	return result;
				}
			}
		}

		ApplicationFiltersOutput output;

		try {
			// 一覧情報検索共通処理
			ApplicationFilterList applicationFilterList = new ApplicationFilterList(messageAccessor);
			output = new ApplicationFiltersOutput();
			output.setApplicationFilterList(applicationFilterList.getApplicationFilterList(queryDAO, uvo));

		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("FK061012", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060022"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

		 if(delNum != 0) {
		    	// 削除不可オブジェクトの名称出力
		    	log.debug("削除不可オブジェクト名称:" + delStr);
				Object[] objectgArray = {delStr};
				output.setMessage(messageAccessor.getMessage("DK060070", objectgArray));
		}
		// レスポンス返却
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("DelApplicationFiltersBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
