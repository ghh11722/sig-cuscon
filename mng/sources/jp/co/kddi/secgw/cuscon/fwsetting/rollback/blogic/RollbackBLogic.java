/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : RollbackBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/27     komakiys                初版作成
 * 2010/06/18     oohashij         コミットステータス確認の追加
 * 2011/10/31     inoue@prosite        コミットステータス確認の変更
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.rollback.blogic;

import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.common.vo.SecurityRules;
import jp.co.kddi.secgw.cuscon.fwsetting.common.CommitStatusManager;
import jp.co.kddi.secgw.cuscon.fwsetting.common.PolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto.RollbackInput;
import jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto.RollbackOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : RollbackBLogic
 * 機能概要 :ロールバック対象の一覧を表示する。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/27
 *          新規作成
 * @version 1.1 oohashij
 *          Created 2010/06/18
 *          コミットステータス確認の追加
 * @version 1.2 inoue@prosite
 *          Created 2011/10/31
 *          コミットステータス確認の変更
 * @see
 */
public class RollbackBLogic implements BLogic<RollbackInput> {
	 /**
     * QueryDAO。
     * Springによりインスタンス生成され設定される。
     */
    private QueryDAO queryDAO = null;

    // かっこ
    private static final String PAR = "(";
    // 世代前
    private static final String GENE = "世代前)";
    private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.rollback.blogic.RollbackBLogic");
    private MessageAccessor messageAccessor = null;
	/**
	 * メソッド名 : execute
	 * 機能概要 : 選択した世代のポリシー一覧を表示する。
	 * @param param RollbackBLogicの入力クラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(RollbackInput param) {
		log.debug("RollbackBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");
        // エラーメッセージ
        BLogicMessages messages = new BLogicMessages();

        // 一覧取得対象世代番号
        int generationNo;

        // ビジネスロジックの出力クラスに結果を設定する
        RollbackOutput out = new RollbackOutput();

        // 基本検索条件クラスにパラメータを設定する。
        InputBase base = new InputBase();
        base.setGenerationNo(CusconConst.ZERO_GENE);
        base.setVsysId(param.getUvo().getVsysId());

        // 世代番号、コミット日時を取得する。
        List<SecurityRules> generationList = null;

        CommitStatusManager commtStatManager = new CommitStatusManager();

        try {
        	// コミットステータス確認
//2011/10/31 rep start inoue@prosite
//        	if (commtStatManager.isCommitting(queryDAO, param.getUvo())) {
           	if (commtStatManager.isCommitting_Processing(queryDAO, param.getUvo())) {
//2011/10/31 rep end inoue@prosite
    			messages.add("message", new BLogicMessage("DK070019"));
    			result.setErrors(messages);
    			result.setMessages(messages);
    			result.setResultString("noneFailure");
    			log.debug("RollbackBLogic処理終了");
    			return result;
        	}
        } catch (Exception e) {
        	// コミットステータス確認失敗ログ出力
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage("EK071004", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK070025"));
			result.setErrors(messages);
			result.setMessages(messages);
			result.setResultString("noneFailure");
			return result;
        }

        try {
	        // 世代番号、コミット日時を取得する。
	        generationList = queryDAO.executeForObjectList("RollbackBLogic-1", base);

        } catch(Exception e) {
        	// トランザクションロールバック
        	TransactionUtil.setRollbackOnly();
        	// DBアクセスエラー
        	Object[] objectgArray = {e.getMessage()};
        	log.fatal(messageAccessor.getMessage(
        			"FK071001", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK070006"));
        	result.setErrors(messages);
        	result.setMessages(messages);
        	result.setResultString("noneFailure");
        	return result;
        }

        // ロールバック対象なしの場合
        if(generationList.size() == 0) {

        	// ロールバック対象なしログ出力
        	log.debug("ロールバック対象なし");
        	messages.add("message", new BLogicMessage("DK070005"));
			result.setErrors(messages);
			result.setMessages(messages);
			result.setResultString("noneFailure");
        	return result;
        }

        // 世代番号、コミット日時を画面表示用に編集する。
        for(int i=0; i<generationList.size(); i++) {
        	log.debug("世代番号、コミット日時を表示用に編集");
        	generationList.get(i).setGeneUpdateStr(
        			generationList.get(i).getUpdateTime() +
        			PAR + generationList.get(i).getGenerationNo() + GENE);
        }

        // 初期画面(選択世代番号が0)の場合
        if(param.getSelectGeneNo() == 0) {
        	log.debug("別の画面より遷移");
        	generationNo = CusconConst.ONE_GENE;
        } else {
        	log.debug("ロールバック画面より選択");
        	generationNo = param.getSelectGeneNo();
        }

        // ポリシーリストクラスを生成する。
        PolicyList policy = new PolicyList();

        // 選択された世代番号のポリシーリスト
        List<SelectPolicyList> policyList = null;
        try {
	        // 選択された世代番号のポリシーリストを取得する。
	        policyList = policy.getPolicylist(queryDAO, param.getUvo(),
	        							generationNo, CusconConst.POLICY);
        } catch(Exception e) {
			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"EK071002", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK070006"));
			result.setErrors(messages);
			result.setMessages(messages);
			result.setResultString("noneFailure");
        	return result;
		}

        out.setPolicyList(policyList);
        out.setGenerationList(generationList);
        out.setSelectGeneNo(generationNo);

        result.setResultObject(out);
        result.setResultString("success");

        log.debug("RollbackBLogic処理終了");
		return result;
	}

    /**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}
	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
