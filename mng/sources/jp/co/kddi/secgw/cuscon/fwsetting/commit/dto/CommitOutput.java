/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CommitOutput.java
 *
 * [変更履歴]
 * 日付           更新者               内容
 * 2010/02/17     komakiys             初版作成
 * 2011/06/14     ktakenaka@PROSITE    Webフィルタの追加
 * 2012/08/27     kkato@PROSITE        通信全断アラート対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.commit.dto;

import java.util.List;
import jp.co.kddi.secgw.cuscon.fwsetting.commit.vo.SelectCommitList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;	// 20120827 kkato@PROSITE add

/**
 * クラス名 : CommitOutput
 * 機能概要 : CommitBLogicの出力クラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/17
 *          新規作成
 * @see
 */
public class CommitOutput {

	// セキュリティポリシー情報
	private List<SelectCommitList> rules = null;

	// アドレス情報
	private List<SelectCommitList> addresses = null;

	// アドレスグループ情報
	private List<SelectCommitList> addressGroups = null;

	// アプリケーショングループ情報
	private List<SelectCommitList> applicationGroups = null;

	// アプリケーションフィルタ情報
	private List<SelectCommitList> applicationFilters = null;

	// サービス情報
	private List<SelectCommitList> services = null;

	// サービスグループ情報
	private List<SelectCommitList> serviceGroups = null;

	// スケジュール情報
	private List<SelectCommitList> schedules = null;

	// 20110614 ktakenaka@PROSITE add start
	// Webフィルタ情報
	private List<SelectCommitList> urlFilters = null;
	// 20110614 ktakenaka@PROSITE add end

	// 20120827 kkato@PROSITE add start
	// セキュリティポリシー更新情報
	private List<SelectPolicyList> policyRules = null;
	// 通信全断アラートフラグ
	private String denyAlert = null;
	// 20120827 kkato@PROSITE add end

	/**
	 * メソッド名 : rulesのGetterメソッド
	 * 機能概要 : rulesを取得する。
	 * @return rules
	 */
	public List<SelectCommitList> getRules() {
		return rules;
	}

	/**
	 * メソッド名 : rulesのSetterメソッド
	 * 機能概要 : rulesをセットする。
	 * @param rules
	 */
	public void setRules(List<SelectCommitList> rules) {
		this.rules = rules;
	}

	/**
	 * メソッド名 : addressesのGetterメソッド
	 * 機能概要 : addressesを取得する。
	 * @return addresses
	 */
	public List<SelectCommitList> getAddresses() {
		return addresses;
	}

	/**
	 * メソッド名 : addressesのSetterメソッド
	 * 機能概要 : addressesをセットする。
	 * @param addresses
	 */
	public void setAddresses(List<SelectCommitList> addresses) {
		this.addresses = addresses;
	}

	/**
	 * メソッド名 : addressGroupsのGetterメソッド
	 * 機能概要 : addressGroupsを取得する。
	 * @return addressGroups
	 */
	public List<SelectCommitList> getAddressGroups() {
		return addressGroups;
	}

	/**
	 * メソッド名 : addressGroupsのSetterメソッド
	 * 機能概要 : addressGroupsをセットする。
	 * @param addressGroups
	 */
	public void setAddressGroups(List<SelectCommitList> addressGroups) {
		this.addressGroups = addressGroups;
	}

	/**
	 * メソッド名 : applicationGroupsのGetterメソッド
	 * 機能概要 : applicationGroupsを取得する。
	 * @return applicationGroups
	 */
	public List<SelectCommitList> getApplicationGroups() {
		return applicationGroups;
	}

	/**
	 * メソッド名 : applicationGroupsのSetterメソッド
	 * 機能概要 : applicationGroupsをセットする。
	 * @param applicationGroups
	 */
	public void setApplicationGroups(List<SelectCommitList> applicationGroups) {
		this.applicationGroups = applicationGroups;
	}

	/**
	 * メソッド名 : applicationFiltersのGetterメソッド
	 * 機能概要 : applicationFiltersを取得する。
	 * @return applicationFilters
	 */
	public List<SelectCommitList> getApplicationFilters() {
		return applicationFilters;
	}

	/**
	 * メソッド名 : applicationFiltersのSetterメソッド
	 * 機能概要 : applicationFiltersをセットする。
	 * @param applicationFilters
	 */
	public void setApplicationFilters(List<SelectCommitList> applicationFilters) {
		this.applicationFilters = applicationFilters;
	}

	/**
	 * メソッド名 : servicesのGetterメソッド
	 * 機能概要 : servicesを取得する。
	 * @return services
	 */
	public List<SelectCommitList> getServices() {
		return services;
	}

	/**
	 * メソッド名 : servicesのSetterメソッド
	 * 機能概要 : servicesをセットする。
	 * @param services
	 */
	public void setServices(List<SelectCommitList> services) {
		this.services = services;
	}

	/**
	 * メソッド名 : serviceGroupsのGetterメソッド
	 * 機能概要 : serviceGroupsを取得する。
	 * @return serviceGroups
	 */
	public List<SelectCommitList> getServiceGroups() {
		return serviceGroups;
	}

	/**
	 * メソッド名 : serviceGroupsのSetterメソッド
	 * 機能概要 : serviceGroupsをセットする。
	 * @param serviceGroups
	 */
	public void setServiceGroups(List<SelectCommitList> serviceGroups) {
		this.serviceGroups = serviceGroups;
	}

	/**
	 * メソッド名 : schedulesのGetterメソッド
	 * 機能概要 : schedulesを取得する。
	 * @return schedules
	 */
	public List<SelectCommitList> getSchedules() {
		return schedules;
	}

	/**
	 * メソッド名 : schedulesのSetterメソッド
	 * 機能概要 : schedulesをセットする。
	 * @param schedules
	 */
	public void setSchedules(List<SelectCommitList> schedules) {
		this.schedules = schedules;
	}

	// 20110614 ktakenaka@PROSITE add start
	/**
	 * メソッド名 : urlFiltersのGetterメソッド
	 * 機能概要 : urlFiltersを取得する。
	 * @return urlFilters
	 */
	public List<SelectCommitList> getUrlFilters() {
		return urlFilters;
	}

	/**
	 * メソッド名 : urlFiltersのSetterメソッド
	 * 機能概要 : urlFiltersをセットする。
	 * @param urlFilters
	 */
	public void setUrlFilters(List<SelectCommitList> urlFilters) {
		this.urlFilters = urlFilters;
	}
	// 20110614 ktakenaka@PROSITE add end

	// 20120827 kkato@PROSITE add start
	/**
	 * メソッド名 : policyRulesのGetterメソッド
	 * 機能概要 : policyRulesを取得する。
	 * @return policyRules
	 */
	public List<SelectPolicyList> getPolicyRules() {
		return policyRules;
	}

	/**
	 * メソッド名 : policyRulesのSetterメソッド
	 * 機能概要 : policyRulesをセットする。
	 * @param policyRules
	 */
	public void setPolicyRules(List<SelectPolicyList> policyRules) {
		this.policyRules = policyRules;
	}

	/**
	 * メソッド名 : denyAlertのGetterメソッド
	 * 機能概要 : denyAlertを取得する。
	 * @return denyAlert
	 */
	public String getDenyAlert() {
		return denyAlert;
	}

	/**
	 * メソッド名 : denyAlertのSetterメソッド
	 * 機能概要 : denyAlertをセットする。
	 * @param denyAlert
	 */
	public void setDenyAlert(String denyAlert) {
		this.denyAlert = denyAlert;
	}
	// 20120827 kkato@PROSITE add end
}
