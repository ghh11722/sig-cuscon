/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddSchedulesBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/04     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.blogic;

import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.RegistLimitList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.dto.CommonInput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : AddSchedulesBLogic
 * 機能概要 :スケジュール追加選択時の処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/04
 *          新規作成
 * @see
 */
public class AddSchedulesBLogic implements BLogic<CommonInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;
	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.blogic.AddSchedulesBLogic");
	private MessageAccessor messageAccessor = null;
	/**
	 * メソッド名 :execute
	 * 機能概要 :スケジュール登録上限チェックを行う。
	 * @param param CommonInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(CommonInput param) {
		log.debug("AddSchedulesBLogic処理開始");
		BLogicResult result = new BLogicResult();
		 // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
        result.setResultString("failure");

        // 登録上限値
        List<RegistLimitList> limitList = null;
        try {
	        // 登録上限値を取得する。
	        limitList = queryDAO.executeForObjectList("CommonM_SystemConstant-2", null);
        } catch(Exception e) {
        	// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラーログ出力
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061029", objectgArray, param.getUvo()));
			messages.add("message", new BLogicMessage("DK060047"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

        InputBase base = new InputBase();
        base.setVsysId(param.getUvo().getVsysId());
        base.setGenerationNo(CusconConst.ZERO_GENE);
        base.setModFlg(CusconConst.DEL_NUM);

        // オブジェクトの登録数
        int registNum = 0;
        try {
	        // オブジェクトの登録数を取得する。
	        registNum = queryDAO.executeForObject("AddSchedulesBLogic-1", base, java.lang.Integer.class);

        } catch(Exception e) {
        	// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
        	// DBアクセスエラー
        	Object[] objectgArray = {e.getMessage()};
        	log.fatal(messageAccessor.getMessage(
        			"FK061028", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK060047"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
        }

        // 登録上限値とオブジェクト登録数の比較を行う。
        if(limitList.get(0).getSchedule() <= registNum) {

        	log.debug("登録上限値のため登録できませんでした。");
        	messages.add("message", new BLogicMessage("DK060003"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
        }


        result.setResultString("success");

        log.debug("AddSchedulesBLogic処理終了");
        return result;

	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}

}
