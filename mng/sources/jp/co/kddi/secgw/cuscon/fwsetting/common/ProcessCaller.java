/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ProcessCaller.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/06/21     oohashij                初版作成
 * 2016/12/21     T.Yamazaki@Plum Systems デバッグログ出力の追加
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.common;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;

import jp.terasoluna.fw.util.PropertyUtil;

/**
 * クラス名 : ProcessCaller
 * 機能概要 :
 * 備考 :
 * @author oohashij
 * @version 1.0 oohashij
 *          Created 2010/06/21
 *          新規作成
 *          2.0 T.Yamazaki@Plum Systems Inc.
 *          Updated 2016/12/21
 *          デバッグログ出力の追加
 * @see
 */
public class ProcessCaller {

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 */
	public ProcessCaller() {
	}

	// シェル
	private static final String SHELL = PropertyUtil.getProperty("fwsetting.common.shell.shell");
	// シェルパス
	private static final String SHELL_PATH = PropertyUtil.getProperty("fwsetting.common.shell.path");
	// コミットシェル
	private static final String SHELL_COMMIT = PropertyUtil.getProperty("fwsetting.common.shell.commit");
	// ロールバックシェル
	private static final String SHELL_ROLLBACK = PropertyUtil.getProperty("fwsetting.common.shell.rollback");
	// 待ち時間
	private static final long SHELL_WAIT_TIME = Long.parseLong(PropertyUtil.getProperty("fwsetting.common.shell.wait"));

	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.common.ProcessCaller");
	// メッセージクラス
	private static MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : コミット/ロールバックプロセス起動
	 * 機能概要 : コミット/ロールバック処理を別プロセスで起動する
	 * @param uvo CusconUVO
	 * @param type 処理タイプ
	 * @param gene 世代番号
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void call(CusconUVO uvo, int type, int gene) throws UnsupportedEncodingException, IOException, InterruptedException {
		log.debug("call処理開始");

		List<String> cmdList = new ArrayList<String>();
		cmdList.add(SHELL);
		if (type == CusconConst.COMMIT) {
			cmdList.add(SHELL_COMMIT);
		} else if (type == CusconConst.ROLLBACK) {
			cmdList.add(SHELL_ROLLBACK);
		}
		cmdList.add(encode(uvo.getLoginId()));
		cmdList.add(encode(uvo.getVsysId()));
		cmdList.add(encode(uvo.getGrantFlagStr()));
		cmdList.add(Integer.toString(gene));

		log.info("コマンド呼び出し");
		for(int i = 0; i < cmdList.size(); i++){
			log.debug("  - " + cmdList.get(i));
		}

		ProcessBuilder pb = new ProcessBuilder(cmdList);
		// 作業ディレクトリの指定
		File workDirectory = new File(SHELL_PATH);
		pb.directory(workDirectory);
		log.debug("作業ディレクトリ : " + workDirectory);
		// 標準出力と標準エラーのマージ
		pb.redirectErrorStream(true);

		Process p = null;
		InputStream is = null;
		OutputStream os = null;
		try {
			// プロセス起動
			log.debug("プロセスを起動します...");
			p = pb.start();
			log.debug("プロセスを起動しました。");
			// 一定時間スリープ
			Thread.sleep(SHELL_WAIT_TIME);

			is = p.getInputStream();
			os = p.getOutputStream();
			is.close();
			os.close();
		} catch (IOException e) {
			// プロセス起動エラー
			log.error("プロセスの起動に失敗しました。(IOException)");
			log.error(e.getMessage());
			Object[] objectgArray = { e.getMessage() };
			log.fatal(messageAccessor.getMessage("FZ011038", objectgArray, uvo));
			throw e;
		} catch (InterruptedException e) {
			// 割込例外
			log.error("プロセスの起動に失敗しました。(InterruptedException)");
			log.error(e.getMessage());
			Object[] objectgArray = { e.getMessage() };
			log.fatal(messageAccessor.getMessage("FZ011038", objectgArray, uvo));
			throw e;
		} finally {
			if (is != null) {
				log.debug("InputStreamをclose");
				is.close();
			}
			if (os != null) {
				log.debug("OutputStreamをclose");
				os.close();
			}
		}
		log.debug("call処理終了");
	}

	/**
	 * メソッド名 : エンコード
	 * 機能概要 : 文字列をUTF-8にエンコードする
	 * @param str 指定文字列
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private String encode(String str) throws UnsupportedEncodingException {
		return URLEncoder.encode(str, "UTF-8");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor msgAcc) {
		log.debug("setMessageAccessor処理開始");
		ProcessCaller.messageAccessor = msgAcc;
		log.debug("setMessageAccessor処理終了");
	}

	/**
	 * メソッド名 : getMessage
	 * 機能概要 : messageAccessorをゲットする。
	 * @return MessageAccessor
	 */
	public MessageAccessor getMessage() {
		log.debug("getMessage処理");
		return messageAccessor;
	}

}
