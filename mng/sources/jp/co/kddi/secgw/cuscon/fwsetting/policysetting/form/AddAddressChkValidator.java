/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectServiceChkValidator.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/11     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.form;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.fwsetting.common.IpAddressChk;
import jp.terasoluna.fw.web.struts.form.MultiFieldValidator;

/**
 * クラス名 : SelectServiceChkValidator
 * 機能概要 : ポリシー設定(アドレス編集画面)のチェックを行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/11
 *          新規作成
 * @see
 */
public class AddAddressChkValidator implements MultiFieldValidator {

	/**
	 * メソッド名 :validate
	 * 機能概要 :追加アドレスの形式チェックを行う。
	 * @param checkList 「オブジェクトの選択」の選択インデックス
	 * @param index ラジオボタンのインデックス
	 * @return チェック結果
	 * @see jp.terasoluna.fw.web.struts.form.MultiFieldValidator#validate(java.lang.String, java.lang.String[])
	 */
	@Override
	public boolean validate(String index, String[] fields) {

		// 追加アドレスを取得する。
		String addCheckList = fields[0];

		// 選択するの場合、かつ追加アドレスを選択している場合
		if(Integer.parseInt(index) == CusconConst.SELECT &&
				addCheckList != null && addCheckList != "") {

			String[] checkAddress = addCheckList.split(",");
			for(int i=0; i<checkAddress.length; i++) {
				// IP Addressチェック
	        	if(!IpAddressChk.isIpAddress(checkAddress[i])) {

	        		return false;
	        	}
			}
		}
		return true;
	}
}
