/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : PolicyBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.common.CommitStatusManager;
import jp.co.kddi.secgw.cuscon.fwsetting.common.PolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.PolicyInput;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.PolicyOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;

/**
 * クラス名 : PolicyBLogic
 * 機能概要 :ポリシー一覧を表示処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/01
 *          新規作成
 * @see
 */
public class PolicyBLogic implements BLogic<PolicyInput> {

	/**
     * QueryDAO。
     * Springによりインスタンス生成され設定される。
     */
    private QueryDAO queryDAO = null;

    private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic.PolicyBLogic");
    private MessageAccessor messageAccessor = null;
	/**
	 * メソッド名 :execute
	 * 機能概要 :ポリシー一覧情報をアウトプットクラスに設定する。
	 * @param param ポリシー一覧画面入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(PolicyInput param) {

		log.debug("PolicyBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");
        // エラーメッセージ
        BLogicMessages messages = new BLogicMessages();

		// コミットステータス管理クラスを生成する。
		CommitStatusManager commtStatMgr = new CommitStatusManager();

        // ポリシーリストクラスを生成する。
        PolicyList policy = new PolicyList();

        // 選択された世代番号のポリシーリスト
        List<SelectPolicyList> policyList = null;
        try {
			if (commtStatMgr.isCommitting(queryDAO, param.getUvo())) {
				// コミット中
				log.debug("コミット中です。");
	        	messages.add("message", new BLogicMessage("DK050038"));
				result.setErrors(messages);
				result.setMessages(messages);
				result.setResultString("failure");
	        	return result;
			} else {
				// 選択された世代番号のポリシーリストを取得する。
		        policyList = policy.getPolicylist(queryDAO, param.getUvo(),
		        					CusconConst.ZERO_GENE, CusconConst.POLICY);
			}
        } catch(Exception e) {
			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"EK051001", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK050004"));
			result.setErrors(messages);
			result.setMessages(messages);
			result.setResultString("failure");
        	return result;
		}

		// セッションクリア
		List<SelectedAplInfo> clearSession = new ArrayList<SelectedAplInfo>();
		clearSession = null;

        // ビジネスロジックの出力クラスに結果を設定する
        PolicyOutput out = new PolicyOutput();
        out.setPolicyList(policyList);
        out.setSessionAplList(clearSession);
        result.setResultObject(out);
        result.setResultString("success");

        log.debug("PolicyBLogic処理終了");
		return result;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}
	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
