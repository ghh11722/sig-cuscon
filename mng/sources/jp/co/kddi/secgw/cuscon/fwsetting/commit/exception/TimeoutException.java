/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : TimeoutException.java
 * 
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/15     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.commit.exception;

/**
 * クラス名 : TimeoutException
 * 機能概要 : 
 * 備考 : 
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/15
 *          新規作成
 * @see 
 */
public class TimeoutException extends RuntimeException {
	
}
