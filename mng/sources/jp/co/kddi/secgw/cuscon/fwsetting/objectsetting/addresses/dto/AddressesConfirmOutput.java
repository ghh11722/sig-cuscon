/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddressesConfirmOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/08     kinoshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectAddressList;

/**
 * クラス名 : AddressesConfirmOutput
 * 機能概要 :
 * 備考 :
 * @author kinoshtt
 * @version 1.0 kinoshtt
 *          Created 2010/03/08
 *          新規作成
 * @see
 */
public class AddressesConfirmOutput {

	// アドレス情報リスト
	private List<SelectAddressList> addressList = null;

	/**
	 * メソッド名 : addressListのGetterメソッド
	 * 機能概要 : addressListを取得する。
	 * @return addressList
	 */
	public List<SelectAddressList> getAddressList() {
		return addressList;
	}

	/**
	 * メソッド名 : addressListのSetterメソッド
	 * 機能概要 : addressListをセットする。
	 * @param addressList
	 */
	public void setAddressList(List<SelectAddressList> addressList) {
		this.addressList = addressList;
	}
}
