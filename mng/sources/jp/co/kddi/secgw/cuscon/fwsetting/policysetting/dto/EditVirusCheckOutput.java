/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditVirusCheckFlgOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/05/18     ktakenaka@PROSITE                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto;

/**
 * クラス名 : EditVirusCheckFlgOutput
 * 機能概要 :VirusCheckFlg項目選択時の出力データクラス
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/05/18
 *          新規作成
 * @see
 */
public class EditVirusCheckOutput {

	// ルール名
	private String name = null;

	// Webウィルスチェックフラグ
	private String virusCheckFlg = null;
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : virusCheckFlgのGetterメソッド
	 * 機能概要 : virusCheckFlgを取得する。
	 * @return virusCheckFlg
	 */
	public String getVirusCheckFlg() {
		return virusCheckFlg;
	}
	/**
	 * メソッド名 : virusCheckFlgのSetterメソッド
	 * 機能概要 : virusCheckFlgをセットする。
	 * @param virusCheckFlg
	 */
	public void setVirusCheckFlg(String virusCheckFlg) {
		this.virusCheckFlg = virusCheckFlg;
	}


}
