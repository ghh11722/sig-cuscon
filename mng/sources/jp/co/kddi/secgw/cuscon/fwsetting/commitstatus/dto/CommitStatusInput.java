/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CommitStautsInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/06/16     oohashij              初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.commitstatus.dto;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;

/**
 * クラス名 : CommitStatusInput
 * 機能概要 : CommitStatusBLogicの入力クラス
 * 備考 :
 * @author oohashij
 * @version 1.0 oohashij
 *          Created 2010/06/16
 *          新規作成
 * @see
 */
public class CommitStatusInput {

	// CusconUVO
	private CusconUVO uvo = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}
}
