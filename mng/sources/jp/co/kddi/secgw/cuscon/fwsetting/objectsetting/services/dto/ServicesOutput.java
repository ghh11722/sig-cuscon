/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ServicesOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     morisou                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.dto;

import java.util.List;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectServiceList;

/**
 * クラス名 : ServicesOutput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/01
 *          新規作成
 * @see
 */
public class ServicesOutput {

	// サービス情報リスト
	private List<SelectServiceList> serviceList = null;
	private String message = null;


	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * メソッド名 : serviceListのGetterメソッド
	 * 機能概要 : serviceListを取得する。
	 * @return serviceList
	 */
	public List<SelectServiceList> getServiceList() {
		return serviceList;
	}

	/**
	 * メソッド名 : serviceListのSetterメソッド
	 * 機能概要 : serviceListをセットする。
	 * @param serviceList
	 */
	public void setServiceList(List<SelectServiceList> serviceList) {
		this.serviceList = serviceList;
	}
}
