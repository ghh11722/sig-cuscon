/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditServiceOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectObjectList;

/**
 * クラス名 : EditServiceOutput
 * 機能概要 :Service項目選択時の出力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class EditServiceOutput {

	// ルール名
	private String name = null;

	// サービス名リスト
	private List<SelectObjectList> serviceList = null;

	// サービスマスタリスト
	private List<SelectObjectList> serviceMasterList = null;

	private String serviceName = null;


	/**
	 * メソッド名 : serviceNameのGetterメソッド
	 * 機能概要 : serviceNameを取得する。
	 * @return serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * メソッド名 : serviceNameのSetterメソッド
	 * 機能概要 : serviceNameをセットする。
	 * @param serviceName
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * メソッド名 : serviceListのGetterメソッド
	 * 機能概要 : serviceListを取得する。
	 * @return serviceList
	 */
	public List<SelectObjectList> getServiceList() {
		return serviceList;
	}

	/**
	 * メソッド名 : serviceListのSetterメソッド
	 * 機能概要 : serviceListをセットする。
	 * @param serviceList
	 */
	public void setServiceList(List<SelectObjectList> serviceList) {
		this.serviceList = serviceList;
	}

	/**
	 * メソッド名 : serviceMasterListのGetterメソッド
	 * 機能概要 : serviceMasterListを取得する。
	 * @return serviceMasterList
	 */
	public List<SelectObjectList> getServiceMasterList() {
		return serviceMasterList;
	}

	/**
	 * メソッド名 : serviceMasterListのSetterメソッド
	 * 機能概要 : serviceMasterListをセットする。
	 * @param serviceMasterList
	 */
	public void setServiceMasterList(List<SelectObjectList> serviceMasterList) {
		this.serviceMasterList = serviceMasterList;
	}
}
