/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : DescriptionLengthChkValidator.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/13     kinoshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.form;

import jp.terasoluna.fw.web.struts.form.MultiFieldValidator;

/**
 * クラス名 : DescriptionLengthChkValidator
 * 機能概要 : ポリシー名設定画面の備考の桁数チェックを行う。
 * 備考 :
 * @author kinoshtt
 * @version 1.0 kinoshtt
 *          Created 2010/03/13
 *          新規作成
 * @see
 */
public class DescriptionLengthChkValidator implements MultiFieldValidator {

	private static final String BLANK = "";

	/**
	 * メソッド名 :validate
	 * 機能概要 :ポリシー名設定画面の備考の桁数チェックを行う。
	 * @param description 備考
	 * @param name ポリシー名
	 * @return チェック結果
	 * @see jp.terasoluna.fw.web.struts.form.MultiFieldValidator#validate(java.lang.String, java.lang.String[])
	 */
	@Override

	public boolean validate(String description, String[] fields) {

		// ポリシー名を取得する。
		String name = fields[0];

		// ポリシー名が入力されていて、かつ備考が256桁以上の場合エラー
        if (!(name == null || BLANK.equals(name)) && description.length() >= 256) {

			return false;
		}
		return true;
	}
}
