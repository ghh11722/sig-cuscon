/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectAddressLinks.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/22     morisou                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo;

import java.io.Serializable;

/**
 * クラス名 : SelectAddressLinks
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/22
 *          新規作成
 * @see
 */
public class AddressLinksInfo implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = -4146041484308208634L;
	// T_AddressGroupsLinkテーブルのリンクシーケンス番号
	private int grpLinkSeqNo = 0;
	// T_SourceAddressLinkテーブルのリンクシーケンス番号
	private int srcLinkSeqNo = 0;
	// T_DestinationAddressLinkテーブルのリンクシーケンス番号
	private int dstLinkSeqNo = 0;
	/**
	 * メソッド名 : grpLinkSeqNoのGetterメソッド
	 * 機能概要 : grpLinkSeqNoを取得する。
	 * @return grpLinkSeqNo
	 */
	public int getGrpLinkSeqNo() {
		return grpLinkSeqNo;
	}
	/**
	 * メソッド名 : grpLinkSeqNoのSetterメソッド
	 * 機能概要 : grpLinkSeqNoをセットする。
	 * @param grpLinkSeqNo
	 */
	public void setGrpLinkSeqNo(int grpLinkSeqNo) {
		this.grpLinkSeqNo = grpLinkSeqNo;
	}
	/**
	 * メソッド名 : srcLinkSeqNoのGetterメソッド
	 * 機能概要 : srcLinkSeqNoを取得する。
	 * @return srcLinkSeqNo
	 */
	public int getSrcLinkSeqNo() {
		return srcLinkSeqNo;
	}
	/**
	 * メソッド名 : srcLinkSeqNoのSetterメソッド
	 * 機能概要 : srcLinkSeqNoをセットする。
	 * @param srcLinkSeqNo
	 */
	public void setSrcLinkSeqNo(int srcLinkSeqNo) {
		this.srcLinkSeqNo = srcLinkSeqNo;
	}
	/**
	 * メソッド名 : dstLinkSeqNoのGetterメソッド
	 * 機能概要 : dstLinkSeqNoを取得する。
	 * @return dstLinkSeqNo
	 */
	public int getDstLinkSeqNo() {
		return dstLinkSeqNo;
	}
	/**
	 * メソッド名 : dstLinkSeqNoのSetterメソッド
	 * 機能概要 : dstLinkSeqNoをセットする。
	 * @param dstLinkSeqNo
	 */
	public void setDstLinkSeqNo(int dstLinkSeqNo) {
		this.dstLinkSeqNo = dstLinkSeqNo;
	}
}
