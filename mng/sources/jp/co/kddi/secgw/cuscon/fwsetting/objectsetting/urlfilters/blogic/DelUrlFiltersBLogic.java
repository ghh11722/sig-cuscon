/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : DelUrlFiltersBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/10     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.blogic;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.UrlFilterLink;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.UrlFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectUrlFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.dto.DelUrlFiltersInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.dto.UrlFiltersOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

import org.apache.log4j.Logger;

/**
 * クラス名 : DelUrlFiltersBLogic
 * 機能概要 :
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/10
 *          新規作成
 * @see
 */
public class DelUrlFiltersBLogic implements BLogic<DelUrlFiltersInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.blogic.DelUrlFiltersBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 :
	 * @param params
	 * @return
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(DelUrlFiltersInput params) {
		log.debug("DelUrlFiltersBLogic処理開始");

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		BLogicMessages errorMessages = new BLogicMessages();	// エラーメッセージ
		result.setResultString("failure");

		// 画面から渡されたWebフィルタリストを取得
		List<SelectUrlFilterList> urlFilterList4Del = params.getUrlFilterList();

		int delNum = 0;     // 削除件数
		String delStr = ""; // 削除名称（カンマ区切り）

		// 画面から渡されたWebフィルタリストの件数分、以下の処理を実行
		for (int i = 0; i < urlFilterList4Del.size(); i++) {
			log.debug("画面から渡されたWebフィルタリストの件数分繰り返す:" + i);

			try {
				// 検索条件設定
				UrlFilterLink selectAddressLinks = new UrlFilterLink();
				selectAddressLinks.setLinkSeqNo(urlFilterList4Del.get(i).getSeqNo());

				// 当該Webフィルタがリンクされている件数を取得
				int count= queryDAO.executeForObject("UrlFiltersBLogic-15", selectAddressLinks, Integer.class);

				if (count > 0) {
					log.debug("オブジェクト削除不可:" + urlFilterList4Del.get(i).getName());
					delNum++;

					// 削除名称を連結する。
					if(delNum == 1) {
						delStr = urlFilterList4Del.get(i).getName();
					} else {
						delStr = delStr + ", " + urlFilterList4Del.get(i).getName();
					}
					if(urlFilterList4Del.size() == delNum) {
						// 検索結果が1件以上の場合、削除不可エラー
						// レスポンスデータを作成し返却
						// 削除不可エラーメッセージを出力する。
						// トランザクションロールバック
						TransactionUtil.setRollbackOnly();
						Object[] objectgArray = {delStr};
						// その他のテーブルで使用しているため削除できません。
						log.debug(messageAccessor.getMessage("DK060002", objectgArray, params.getUvo()));
						// {0}はその他のテーブルで使用しているため削除できません。
						errorMessages.add("message",new BLogicMessage("DK060070", objectgArray));
						result.setErrors(errorMessages);
						result.setResultString("failure");
			        	return result;
					}
					// 削除可能
				} else {
					// Webフィルタに紐づくカテゴリ情報削除
					updateDAO.execute("UrlFiltersBLogic-12", urlFilterList4Del.get(i).getSeqNo());

					// Webフィルタオブジェクトテーブルのレコードの変更フラグを'3'（削除）に更新
					updateDAO.execute("UrlFiltersBLogic-16", urlFilterList4Del.get(i).getSeqNo());
				}
			} catch(Exception e) {
				// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
				// DBアクセスエラー
				Object[] objectgArray = {e.getMessage()};
				// Webフィルタの削除に失敗しました。{0}
				log.fatal(messageAccessor.getMessage("FK061031", objectgArray, params.getUvo()));
				// Webフィルタ削除処理に失敗しました。
				errorMessages.add("message", new BLogicMessage("DK060083"));
				result.setErrors(errorMessages);
				result.setResultString("failure");
	        	return result;
			}
		}

		UrlFiltersOutput output = new UrlFiltersOutput();
		try {
			// 一覧情報検索共通処理
			UrlFilterList urlFilterList = new UrlFilterList();
			urlFilterList.setListLimitSet(true);
			output.setUrlFilterList(urlFilterList.getUrlFilterList(queryDAO, params.getUvo()));
		} catch(Exception e) {
			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	// Webフィルタの検索に失敗しました。{0}
        	log.error(messageAccessor.getMessage("FK061030", objectgArray, params.getUvo()));
			// Webフィルタ削除処理に失敗しました。
			errorMessages.add("message", new BLogicMessage("DK060083"));
			result.setErrors(errorMessages);
			result.setResultString("failure");
        	return result;
		}

		 if(delNum != 0) {
	    	// 削除不可オブジェクトの名称出力
	    	log.debug("削除不可オブジェクト名称:" + delStr);
			Object[] objectgArray = {delStr};
			// {0}はその他のテーブルで使用しているため削除できません。
			output.setMessage(messageAccessor.getMessage("DK060070", objectgArray));
		 }

		// レスポンス返却
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("DelUrlFiltersBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理開始");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理開始");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
