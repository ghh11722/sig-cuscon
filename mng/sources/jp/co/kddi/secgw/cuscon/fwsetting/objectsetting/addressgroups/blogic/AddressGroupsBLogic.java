/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddressGroupsBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/19     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.blogic;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.common.CommitStatusManager;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.AddressGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.dto.CommonInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.dto.AddressGroupsOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;

/**
 * クラス名 : AddressGroupsBLogic
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/19
 *          新規作成
 * @see
 */
public class AddressGroupsBLogic implements BLogic<CommonInput> {

	// QueryDAO
    private QueryDAO queryDAO = null;

    private Logger log = Logger.getLogger(
    		"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.blogic.AddressGroupsBLogic");
    private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : BLogicResult
	 * 機能概要 : アドレスグループの一覧情報を取得し、取得した情報を呼び出し元に返却する。
	 * @param param
	 * @return result
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(CommonInput params) {
		log.debug("AddressGroupsBLogic処理開始");

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		 // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");
		CommitStatusManager commtStatMgr = new CommitStatusManager();
		AddressGroupsOutput output = new AddressGroupsOutput();
		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = params.getUvo();
		try {
			if (commtStatMgr.isCommitting(queryDAO, uvo)) {
				// コミット中
				log.debug("コミット中です。");
	        	messages.add("message", new BLogicMessage("DK060074"));
				result.setErrors(messages);
				result.setMessages(messages);
				result.setResultString("failure");
	        	return result;
			} else {
				// 一覧情報検索共通処理
				AddressGroupList addressGroupList = new AddressGroupList();
				output.setAddressGroupList(addressGroupList.getAddressGroupList(queryDAO, uvo));
			}
		} catch(Exception e) {
			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"EK061002", objectgArray, params.getUvo()));
        	messages.add("message", new BLogicMessage("DK060011"));
			result.setErrors(messages);
			result.setMessages(messages);
			result.setResultString("failure");
        	return result;
		}

		// レスポンス返却
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("AddressGroupsBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
