/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CommitStatus.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/06/16     oohashij          初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.common.vo;

import java.io.Serializable;

/**
 * クラス名 : CommitStatus
 * 機能概要 : コミットステータスオブジェクトデータクラス
 * 備考 :
 * @author oohashij
 * @version 1.0 oohashij
 *          Created 2010/06/16
 *          新規作成
 * @see
 */
public class CommitStatus implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 3904999683544129269L;
	// Vsys-ID
	private String vsysId = null;
	// 処理区分
	private int processingKind = 0;
	// ステータス
	private int status = 0;
	// メッセージ
	private String message = null;
	// StartTime
	private String startTime = null;
	// EndTime
	private String endTime = null;

	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得する。
	 * @return vsysId
	 */
	public String getVsysId() {
		return vsysId;
	}

	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdをセットする。
	 * @param vsysId
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}

	/**
	 * メソッド名 : processingKindのGetterメソッド
	 * 機能概要 : processingKindを取得する。
	 * @return processingKind
	 */
	public int getProcessingKind() {
		return processingKind;
	}

	/**
	 * メソッド名 : processingKindのSetterメソッド
	 * 機能概要 : processingKindをセットする。
	 * @param processingKind
	 */
	public void setProcessingKind(int processingKind) {
		this.processingKind = processingKind;
	}

	/**
	 * メソッド名 : statusのGetterメソッド
	 * 機能概要 : statusを取得する。
	 * @return status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * メソッド名 : statusのSetterメソッド
	 * 機能概要 : statusをセットする。
	 * @param status
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * メソッド名 : startTimeのGetterメソッド
	 * 機能概要 : startTimeを取得する。
	 * @return startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * メソッド名 : startTimeのSetterメソッド
	 * 機能概要 : startTimeをセットする。
	 * @param startTime
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * メソッド名 : endTimeのGetterメソッド
	 * 機能概要 : endTimeを取得する。
	 * @return endTime
	 */
	public String getEndTime() {
		return endTime;
	}

	/**
	 * メソッド名 : endTimeのSetterメソッド
	 * 機能概要 : endTimeをセットする。
	 * @param endTime
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

}
