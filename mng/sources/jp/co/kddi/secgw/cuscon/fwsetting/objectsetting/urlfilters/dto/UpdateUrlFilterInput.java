/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : RegistUrlFilterInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/07     ktakenaka@PROSITE                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.UrlFilterCategoryInfo;

/**
 * クラス名 : RegistUrlFilterInput
 * 機能概要 :
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/07
 *          新規作成
 * @see
 */
public class UpdateUrlFilterInput {

	// UVO
	private CusconUVO uvo = null;
	// 変更前Webフィルタ名
	private String newUrlFilterName = null;
	// 変更後Webフィルタ名
	private String oldUrlFilterName = null;
	// Webフィルタカテゴリマスタリスト
	List<UrlFilterCategoryInfo> categoryList = null;
	// 説明
	private String comment = null;
	// 変更前ブロックリストサイト
	private String blockListSite = null;
	// 変更後ブロックリストサイト
	private String newBlockListSite = null;
	// ブロックサイト動作
	private String actionBlockListSite = null;
	// 変更前許可リストサイト
	private String allowListSite = null;
	// 変更後許可リストサイト
	private String newAllowListSite = null;
	// 変更後Webフィルタカテゴリアクションリスト
	private String selectActionList = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}
	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}
	/**
	 * メソッド名 : newUrlFilterNameのGetterメソッド
	 * 機能概要 : newUrlFilterNameを取得する。
	 * @return newUrlFilterName
	 */
	public String getNewUrlFilterName() {
		return newUrlFilterName;
	}
	/**
	 * メソッド名 : newUrlFilterNameのSetterメソッド
	 * 機能概要 : newUrlFilterNameをセットする。
	 * @param newUrlFilterName
	 */
	public void setNewUrlFilterName(String newUrlFilterName) {
		this.newUrlFilterName = newUrlFilterName;
	}

	/**
	 * メソッド名 : oldUrlFilterNameのGetterメソッド
	 * 機能概要 : oldUrlFilterNameを取得する。
	 * @return oldUrlFilterName
	 */
	public String getOldUrlFilterName() {
		return oldUrlFilterName;
	}
	/**
	 * メソッド名 : oldUrlFilterNameのSetterメソッド
	 * 機能概要 : oldUrlFilterNameをセットする。
	 * @param oldUrlFilterName
	 */
	public void setOldUrlFilterName(String oldUrlFilterName) {
		this.oldUrlFilterName = oldUrlFilterName;
	}

	/**
	 * メソッド名 : categoryListのGetterメソッド
	 * 機能概要 : categoryListを取得する。
	 * @return categoryList
	 */
	public List<UrlFilterCategoryInfo> getCategoryList() {
		return categoryList;
	}
	/**
	 * メソッド名 : categoryListのSetterメソッド
	 * 機能概要 : categoryListをセットする。
	 * @param categoryList
	 */
	public void setCategoryList(List<UrlFilterCategoryInfo> categoryList) {
		this.categoryList = categoryList;
	}

	/**
	 * メソッド名 : commentのGetterメソッド
	 * 機能概要 : commentを取得する。
	 * @return comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * メソッド名 : commentのSetterメソッド
	 * 機能概要 : commentをセットする。
	 * @param comment
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * メソッド名 : blockListSiteのGetterメソッド
	 * 機能概要 : blockListSiteを取得する。
	 * @return blockListSite
	 */
	public String getBlockListSite() {
		return blockListSite;
	}
	/**
	 * メソッド名 : blockListSiteのSetterメソッド
	 * 機能概要 : blockListSiteをセットする。
	 * @param blockListSite
	 */
	public void setBlockListSite(String blockListSite) {
		this.blockListSite = blockListSite;
	}
	/**
	 * メソッド名 : newBlockListSiteのGetterメソッド
	 * 機能概要 : newBlockListSiteを取得する。
	 * @return newBlockListSite
	 */
	public String getNewBlockListSite() {
		return newBlockListSite;
	}
	/**
	 * メソッド名 : newBlockListSiteのSetterメソッド
	 * 機能概要 : newBlockListSiteをセットする。
	 * @param newBlockListSite
	 */
	public void setNewBlockListSite(String newBlockListSite) {
		this.newBlockListSite = newBlockListSite;
	}
	/**
	 * メソッド名 : actionBlockListSiteのGetterメソッド
	 * 機能概要 : actionBlockListSiteを取得する。
	 * @return actionBlockListSite
	 */
	public String getActionBlockListSite() {
		return actionBlockListSite;
	}
	/**
	 * メソッド名 : actionBlockListSiteのSetterメソッド
	 * 機能概要 : actionBlockListSiteをセットする。
	 * @param actionBlockListSite
	 */
	public void setActionBlockListSite(String actionBlockListSite) {
		this.actionBlockListSite = actionBlockListSite;
	}
	/**
	 * メソッド名 : allowListSiteのGetterメソッド
	 * 機能概要 : allowListSiteを取得する。
	 * @return allowListSite
	 */
	public String getAllowListSite() {
		return allowListSite;
	}
	/**
	 * メソッド名 : allowListSiteのSetterメソッド
	 * 機能概要 : allowListSiteをセットする。
	 * @param allowListSite
	 */
	public void setAllowListSite(String allowListSite) {
		this.allowListSite = allowListSite;
	}
	/**
	 * メソッド名 : newAllowListSiteのGetterメソッド
	 * 機能概要 : newAllowListSiteを取得する。
	 * @return newAllowListSite
	 */
	public String getNewAllowListSite() {
		return newAllowListSite;
	}
	/**
	 * メソッド名 : newAllowListSiteのSetterメソッド
	 * 機能概要 : newAllowListSiteをセットする。
	 * @param newAllowListSite
	 */
	public void setNewAllowListSite(String newAllowListSite) {
		this.newAllowListSite = newAllowListSite;
	}
	/**
	 * メソッド名 : selectActionListのGetterメソッド
	 * 機能概要 : selectActionListを取得する。
	 * @return selectActionList
	 */
	public String getSelectActionList() {
		return selectActionList;
	}
	/**
	 * メソッド名 : selectActionListのSetterメソッド
	 * 機能概要 : selectActionListをセットする。
	 * @param selectActionList
	 */
	public void setSelectActionList(String selectActionList) {
		this.selectActionList = selectActionList;
	}
}
