/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ServiceGroupsForm.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.servicegroups.form;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectServiceGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.ServicesAndGroupsInfo;
import jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx;

/**
 * クラス名 : ServiceGroupsForm
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/01
 *          新規作成
 * @see
 */
public class ServiceGroupsForm extends ValidatorActionFormEx {

	// シリアルバージョンID
	private static final long serialVersionUID = 1L;
	// サービスグループ名
	private String name = null;
	// サービスグループ名(旧)
	private String oldServiceGroupName = null;
	// サービスグループ名(新)
	private String newServiceGroupName = null;
	// サービスグループリスト
	private List<SelectServiceGroupList> serviceList = null;
	// サービスグループ追加用サービス＆サービスグループ
	private List<ServicesAndGroupsInfo> serviceAndServiceGrp4Add = null;
	// チェック選択インデックス
	private String checkIndex = null;
	// インデックス
	private int index = 0;
	private String message = null;

	public void reset(ActionMapping mapping, HttpServletRequest request){
		 message=null;
	}

	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : oldServiceGroupNameのSetterメソッド
	 * 機能概要 : oldServiceGroupNameをセットする。
	 * @param oldServiceGroupName
	 */
	public void setOldServiceGroupName(String oldServiceGroupName) {
		this.oldServiceGroupName = oldServiceGroupName;
	}
	/**
	 * メソッド名 : oldServiceGroupNameのGetterメソッド
	 * 機能概要 : oldServiceGroupNameを取得する。
	 * @return oldServiceGroupName
	 */
	public String getOldServiceGroupName() {
		return oldServiceGroupName;
	}
	/**
	 * メソッド名 : newServiceGroupNameのSetterメソッド
	 * 機能概要 : newServiceGroupNameをセットする。
	 * @param newServiceGroupName
	 */
	public void setNewServiceGroupName(String newServiceGroupName) {
		this.newServiceGroupName = newServiceGroupName;
	}
	/**
	 * メソッド名 : newServiceGroupNameのGetterメソッド
	 * 機能概要 : newServiceGroupNameを取得する。
	 * @return newServiceGroupName
	 */
	public String getNewServiceGroupName() {
		return newServiceGroupName;
	}

	/**
	 * メソッド名 : serviceListのGetterメソッド
	 * 機能概要 : serviceListを取得する。
	 * @return serviceList
	 */
	public List<SelectServiceGroupList> getServiceList() {
		return serviceList;
	}
	/**
	 * メソッド名 : serviceListのSetterメソッド
	 * 機能概要 : serviceListをセットする。
	 * @param serviceList
	 */
	public void setServiceList(List<SelectServiceGroupList> serviceList) {
		this.serviceList = serviceList;
	}
	/**
	 * メソッド名 : serviceAndServiceGrp4AddのGetterメソッド
	 * 機能概要 : serviceAndServiceGrp4Addを取得する。
	 * @return serviceAndServiceGrp4Add
	 */
	public List<ServicesAndGroupsInfo> getServiceAndServiceGrp4Add() {
		return serviceAndServiceGrp4Add;
	}
	/**
	 * メソッド名 : serviceAndServiceGrp4AddのSetterメソッド
	 * 機能概要 : serviceAndServiceGrp4Addをセットする。
	 * @param serviceAndServiceGrp4Add
	 */
	public void setServiceAndServiceGrp4Add(List<ServicesAndGroupsInfo> serviceAndServiceGrp4Add) {
		this.serviceAndServiceGrp4Add = serviceAndServiceGrp4Add;
	}
	/**
	 * メソッド名 : checkIndexのGetterメソッド
	 * 機能概要 : checkIndexを取得する。
	 * @return checkIndex
	 */
	public String getCheckIndex() {
		return checkIndex;
	}
	/**
	 * メソッド名 : checkIndexのSetterメソッド
	 * 機能概要 : checkIndexをセットする。
	 * @param checkIndex
	 */
	public void setCheckIndex(String checkIndex) {
		this.checkIndex = checkIndex;
	}
	/**
	 * メソッド名 : indexのGetterメソッド
	 * 機能概要 : indexを取得する。
	 * @return index
	 */
	public int getIndex() {
		return index;
	}
	/**
	 * メソッド名 : indexのSetterメソッド
	 * 機能概要 : indexをセットする。
	 * @param index
	 */
	public void setIndex(int index) {
		this.index = index;
	}
	/**
	 * メソッド名 : serialversionuidのGetterメソッド
	 * 機能概要 : serialversionuidを取得する。
	 * @return serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
