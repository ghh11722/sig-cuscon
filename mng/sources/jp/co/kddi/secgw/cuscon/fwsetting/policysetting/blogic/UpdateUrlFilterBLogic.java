/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateUrlFilterBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.SecurityRules;
import jp.co.kddi.secgw.cuscon.fwsetting.common.PolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.LinkInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.PolicyOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.UpdateUrlFilterInput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

import org.apache.log4j.Logger;

/**
 * クラス名 : UpdateUrlFilterBLogic
 * 機能概要 :Webフィルタ項目編集画面にて「OK」押下時の処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class UpdateUrlFilterBLogic implements BLogic<UpdateUrlFilterInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;
	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic.UpdateUrlFilterBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 :execute
	 * 機能概要 :選択Webフィルタを更新する。
	 * @param param SourceZone編集画面入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(UpdateUrlFilterInput param) {

		log.debug("選択Webフィルタ更新処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");
        BLogicMessages errorMessage = new BLogicMessages();// エラーメッセージ

		try {
			// 削除条件を設定する。
			SecurityRules base = new SecurityRules();
			base.setVsysId(param.getUvo().getVsysId());
			base.setGenerationNo(CusconConst.ZERO_GENE);
			base.setModFlg(CusconConst.MOD_NUM);
			base.setName(param.getName());
			base.setModName(param.getName());
			if(param.getRadio() == CusconConst.SELECT) {
				base.setUrlfilterDefaultFlg(1);
			} else {
				base.setUrlfilterDefaultFlg(0);
			}

			// 選択ポリシー情報
			List<SecurityRules> rule = null;
	        // 選択ポリシー情報を取得する。
			rule = queryDAO.executeForObjectList("PolicyRulesList-1", base);

			// 変更フラグが1でない場合
			if(rule.get(0).getModFlg() != CusconConst.ADD_NUM) {
				log.debug("変更フラグが1以外");
				// 変更フラグが1でない場合、変更フラグを2に更新する。
				updateDAO.execute("UpdateUrlFilterBLogic-2", base);
			} else {
				log.debug("変更フラグが1");
				// 変更フラグ、Webフィルタ適用フラグを更新する。
				updateDAO.execute("UpdateUrlFilterBLogic-3", base);
			}

			// シーケンス番号に紐付くWebフィルタリンクを削除する。
			updateDAO.execute("DelPolicyBLogic-11", rule.get(0).getSeqNo());

			// 選択するを選択した場合
			if(param.getRadio() == CusconConst.SELECT) {
				log.debug(String.format("Webフィルタリンク登録処理を行う SEQ_NO=[%d] LINK_SEQ_NO=[%d]",
										rule.get(0).getSeqNo(),
										param.getUrlFilterList().get(param.geturlFilterSelectedIndex()).getSeqNo()));

				// 登録条件を設定する。
				LinkInfo link = new LinkInfo();
				link.setSeqNo(rule.get(0).getSeqNo());
				link.setLinkSeqNo(param.getUrlFilterList().get(param.geturlFilterSelectedIndex()).getSeqNo());

				// Webフィルタリンクを登録する。
				updateDAO.execute("UpdateUrlFilterBLogic-1", link);
			}
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			// ポリシーの更新に失敗しました。{0}
			log.fatal(messageAccessor.getMessage(
					"FK051004", objectgArray, param.getUvo()));
			// Webフィルタ更新処理に失敗しました。
			errorMessage.add("message", new BLogicMessage("DK050042"));
			result.setErrors(errorMessage);
			result.setResultString("failure");
			return result;
		}

		// ポリシーリストクラスを生成する。
        PolicyList rule = new PolicyList();

     // 選択された世代番号のポリシーリストを取得する。
        List<SelectPolicyList> policyList = null;
        try {
	        // 選択された世代番号のポリシーリストを取得する。
	        policyList = rule.getPolicylist(queryDAO, param.getUvo(),
	        				CusconConst.ZERO_GENE, CusconConst.POLICY);
        } catch(Exception e) {
			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	// ポリシー設定で例外が発生しました。{0}
        	log.error(messageAccessor.getMessage(
        			"EK051001", objectgArray, param.getUvo()));
			// Webフィルタ更新処理に失敗しました。
			errorMessage.add("message", new BLogicMessage("DK050042"));
			result.setErrors(errorMessage);
			result.setResultString("failure");
        	return result;
		}

        // ビジネスロジックの出力クラスに結果を設定する
        PolicyOutput out = new PolicyOutput();
        out.setPolicyList(policyList);
        result.setResultObject(out);
        result.setResultString("success");
		log.debug("選択Webフィルタ更新処理終了");
		return result;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}
	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
