/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ApplicationFiltersOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.dto;

import java.util.List;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationFilterList;

/**
 * クラス名 : ApplicationFiltersOutput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/01
 *          新規作成
 * @see
 */
public class ApplicationFiltersOutput {

	// アプリケーションフィルタリスト
	private List<SelectApplicationFilterList> applicationFilterList = null;
	private String message = null;


	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * メソッド名 : applicationFilterListのGetterメソッド
	 * 機能概要 : applicationFilterListを取得する。
	 * @return applicationFilterList
	 */
	public List<SelectApplicationFilterList> getApplicationFilterList() {
		return applicationFilterList;
	}

	/**
	 * メソッド名 : applicationFilterListのSetterメソッド
	 * 機能概要 : applicationFilterListをセットする。
	 * @param applicationFilterList
	 */
	public void setApplicationFilterList(List<SelectApplicationFilterList> applicationFilterList) {
		this.applicationFilterList = applicationFilterList;
	}
}
