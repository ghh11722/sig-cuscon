/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectUrlFilterBlockAllowSiteSumCnt.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/08     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo;

import java.io.Serializable;

/**
 * クラス名 : SelectUrlFilterBlockAllowSiteSumCnt
 * 機能概要 :
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/08
 *          新規作成
 * @see
 */
public class SelectUrlFilterBlockAllowSiteSumCnt implements Serializable{

	// シリアルバージョンID
	private static final long serialVersionUID = 6861842876463980988L;
	// ブロックリストサイト合計数
	private int blockSum = 0;
	// アローリストサイト合計数
	private int allowSum = 0;

	/**
	 * メソッド名 : blockSumのGetterメソッド
	 * 機能概要 : blockSumを取得する。
	 * @return blockSum
	 */
	public int getBlockSum() {
		return blockSum;
	}
	/**
	 * メソッド名 : blockSumのSetterメソッド
	 * 機能概要 : blockSumをセットする。
	 * @param blockSum
	 */
	public void setBlockSum(int blockSum) {
		this.blockSum = blockSum;
	}
	/**
	 * メソッド名 : allowSumのGetterメソッド
	 * 機能概要 : allowSumを取得する。
	 * @return allowSum
	 */
	public int getAllowSum() {
		return allowSum;
	}
	/**
	 * メソッド名 : allowSumのSetterメソッド
	 * 機能概要 : allowSumをセットする。
	 * @param allowSum
	 */
	public void setAllowSum(int allowSum) {
		this.allowSum = allowSum;
	}
}
