/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddServiceBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/04     morisou                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.blogic;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.RegistLimitList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.dto.CommonInput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

import org.apache.log4j.Logger;

/**
 * クラス名 : AddServiceBLogic
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/04
 *          新規作成
 * @see
 */
public class AddServiceBLogic implements BLogic<CommonInput> {
	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.blogic.AddServiceBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 :execute
	 * 機能概要 :スケジュール登録上限チェックを行う。
	 * @param param CommonInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(CommonInput param) {
		log.debug("AddServiceBLogic処理開始");
		BLogicResult result = new BLogicResult();
		 // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
        result.setResultString("failure");
     // 登録上限値を取得する。
        List<RegistLimitList> limitList;
        int registNum;
        try {
	        // 登録上限値を取得する。
	        limitList =
	        	queryDAO.executeForObjectList("CommonM_SystemConstant-2", null);

	        InputBase base = new InputBase();
	        base.setVsysId(param.getUvo().getVsysId());
	        base.setGenerationNo(CusconConst.ZERO_GENE);
	        base.setModFlg(CusconConst.DEL_NUM);

	        // オブジェクトの登録数を取得する。
	        registNum = queryDAO.executeForObject(
	        			"ServicesBLogic-10", base, java.lang.Integer.class);
        } catch (Exception e) {
        	// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラーログ出力
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061020", objectgArray, param.getUvo()));
			messages.add("message", new BLogicMessage("DK060033"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
        // 登録上限値とオブジェクト登録数の比較を行う。
        if(limitList.get(0).getService() <= registNum) {

        	log.debug("メンバ登録上限チェックエラー");
        	messages.add("message", new BLogicMessage("DK060058"));
			result.setErrors(messages);
			result.setResultString("failure");
    		return result;
        }
        result.setResultString("success");
        log.debug("AddServiceBLogic処理終了");
        return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("queryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("queryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
