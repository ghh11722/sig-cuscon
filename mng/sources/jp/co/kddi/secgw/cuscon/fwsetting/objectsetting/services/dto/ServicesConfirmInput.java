/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ServicesConfirmInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/08     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.dto;

import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectServiceList;

/**
 * クラス名 : ServicesConfirmInput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/08
 *          新規作成
 * @see
 */
public class ServicesConfirmInput {

	// CusconUVO
	private CusconUVO uvo = null;
	// チェック選択サービスインデックス
	private String checkIndex = null;
	// サービス情報リスト
	private List<SelectServiceList> serviceList = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}
	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}
	/**
	 * メソッド名 : checkIndexのGetterメソッド
	 * 機能概要 : checkIndexを取得する。
	 * @return index
	 */
	public String getCheckIndex() {
		return checkIndex;
	}
	/**
	 * メソッド名 : indexのSetterメソッド
	 * 機能概要 : indexをセットする。
	 * @param index
	 */
	public void setCheckIndex(String checkIndex) {
		this.checkIndex = checkIndex;
	}
	/**
	 * メソッド名 : serviceListのGetterメソッド
	 * 機能概要 : serviceListを取得する。
	 * @return serviceList
	 */
	public List<SelectServiceList> getServiceList() {
		return serviceList;
	}
	/**
	 * メソッド名 : serviceListのSetterメソッド
	 * 機能概要 : serviceListをセットする。
	 * @param serviceList
	 */
	public void setServiceList(List<SelectServiceList> serviceList) {
		this.serviceList = serviceList;
	}
}
