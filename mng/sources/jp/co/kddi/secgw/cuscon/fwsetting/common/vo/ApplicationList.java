/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ApplicationList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/24     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.common.vo;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationList4Search;
import jp.terasoluna.fw.dao.QueryDAO;

/**
 * クラス名 : ApplicationList
 * 機能概要 : アプリケーション一覧を表示するための情報を取得する。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/24
 *          新規作成
 * @see
 */
public class ApplicationList {

	/**
     * QueryDAO。
     * Springによりインスタンス生成され設定される。
     */
    private QueryDAO queryDAO = null;

    /**
     * メソッド名 : コンストラクタ
     * 機能概要 : -
     * @param queryDAO
     * @param uvo
     */
    public ApplicationList(QueryDAO queryDAO, CusconUVO uvo) {
    	this.queryDAO = queryDAO;
    }

	/**
	 * メソッド名 :getApplicationList
	 * 機能概要 : アプリケーション一覧情報取得処理
	 * @param
	 * @return resultList
	 */
	public List<SelectApplicationList4Search> getApplicationList() {

		// アプリケーション一覧情報取得
		List<SelectApplicationList4Search> resultList = queryDAO.executeForObjectList("CommonM_Applications-1", null);

		// アプリケーション一覧情報取得結果返却
		return resultList;
	}
}
