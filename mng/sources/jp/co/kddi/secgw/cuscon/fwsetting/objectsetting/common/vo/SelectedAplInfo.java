/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectedAplInfo.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/25     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo;

import java.io.Serializable;

/**
 * クラス名 : SelectedAplInfo
 * 機能概要 : 選択中アプリケーションの汎用データクラス
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/25
 *          新規作成
 * @see
 */
public class SelectedAplInfo implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = -5172131541983615540L;
	// SEQ_NO
	private int seqNo = 0;
	// 選択名称
	private String name = null;
	// タイプ
	private String type = null;

	/**
	 * メソッド名 : seqNoのGetterメソッド
	 * 機能概要 : seqNoを取得する。
	 * @return seqNo
	 */
	public int getSeqNo() {
		return seqNo;
	}
	/**
	 * メソッド名 : seqNoのSetterメソッド
	 * 機能概要 : seqNoをセットする。
	 * @param seqNo
	 */
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : typeのGetterメソッド
	 * 機能概要 : typeを取得する。
	 * @return type
	 */
	public String getType() {
		return type;
	}
	/**
	 * メソッド名 : typeのSetterメソッド
	 * 機能概要 : typeをセットする。
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
}
