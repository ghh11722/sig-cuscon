/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddApplicationFilterBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/24     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.blogic;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.fwsetting.common.ApplicationList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.FilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.RegistLimitList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.dto.AddApplicationFilterInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.dto.AddApplicationFilterOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationList4Search;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectFilterMasterList;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

import org.apache.log4j.Logger;

/**
 * クラス名 : AddApplicationFilterBLogic
 * 機能概要 :アプリケーションフィルタ追加選択時の処理を行う。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/24
 *          新規作成
 * @see
 */
public class AddApplicationFilterBLogic implements BLogic<AddApplicationFilterInput>{

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.blogic.AddApplicationFilterBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 : アプリケーションフィルタ追加選択時の処理を行う。
	 * @param params AddApplicationFilterInput入力データクラス
	 * @return result BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(AddApplicationFilterInput params) {

		log.debug("AddApplicationFilterBLogic処理開始");

		SelectFilterMasterList resultFltList = null;                // フィルタマスタ情報
		List<SelectApplicationList4Search> resultAplList = null;    // アプリケーションマスタ情報
		AddApplicationFilterOutput output = null;                   // 返却用リスト

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		 // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// 登録上限値
		List<RegistLimitList> limitList = null;

		try {
	        // 登録上限値を取得する。
	        limitList = queryDAO.executeForObjectList("CommonM_SystemConstant-2", null);

		}  catch (Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061029", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060019"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
        }

        InputBase inputBase = new InputBase();
        inputBase.setVsysId(params.getUvo().getVsysId());
        inputBase.setGenerationNo(CusconConst.ZERO_GENE);
        inputBase.setModFlg(CusconConst.DEL_NUM);

        // オブジェクトの登録数
        int registNum  = 0;

        try {
	        // オブジェクトの登録数を取得する。
	        registNum = queryDAO.executeForObject("ApplicationFiltersBLogic-18"
	        		, inputBase, java.lang.Integer.class);

        } catch (Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
        	// DBアクセスエラー
        	Object[] objectgArray = {e.getMessage()};
        	log.fatal(messageAccessor.getMessage(
        			"FK061012", objectgArray, params.getUvo()));
        	messages.add("message", new BLogicMessage("DK060019"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
        }

        // 登録上限値とオブジェクト登録数の比較を行う。
        if(limitList.get(0).getAppFilter() <= registNum) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
        	log.debug("登録上限値のため登録できませんでした。");
        	messages.add("message", new BLogicMessage("DK060003"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
        }

		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = params.getUvo();

		try {
			// フィルタリスト取得（マスタ情報）
			FilterMasterList filterMasterList = new FilterMasterList();
			resultFltList = filterMasterList.getFilterMasterList(queryDAO, uvo);

			// アプリケーションリスト取得（マスタ情報）
			ApplicationList applicationList = new ApplicationList();
			resultAplList = applicationList.getApplicationList(queryDAO, uvo);

        } catch (Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
        	// DBアクセスエラー
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"FK061012", objectgArray, params.getUvo()));
        	messages.add("message", new BLogicMessage("DK060019"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
        }

		// 返却用リスト
		output = new AddApplicationFilterOutput();
		output.setFilterList(resultFltList);
		output.setApplicationList(resultAplList);

		// レスポンス返却
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("AddApplicationFilterBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
