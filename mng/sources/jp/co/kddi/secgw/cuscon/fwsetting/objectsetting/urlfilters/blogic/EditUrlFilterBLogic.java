/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditUrlFilterBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/08     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.blogic;

import java.util.ArrayList;
import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.UrlFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.CommonSelectItem;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectUrlFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.UrlFilterCategoryInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.UrlFilterCategoryInfoKey;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.dto.EditUrlFilterInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.dto.EditUrlFilterOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;
import jp.terasoluna.fw.util.PropertyUtil;

import org.apache.log4j.Logger;

/**
 * クラス名 : EditUrlFilterBLogic
 * 機能概要 :
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/08
 *          新規作成
 * @see
 */
public class EditUrlFilterBLogic implements BLogic<EditUrlFilterInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.blogic.EditUrlFilterBLogic");

	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 :
	 * @param params
	 * @return
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(EditUrlFilterInput params) {

		log.debug("EditUrlFilterBLogic処理開始");
		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		result.setResultString("failure");
		// エラーメッセージ
		BLogicMessages errorMessage = new BLogicMessages();

		CusconUVO uvo = params.getUvo();

		SelectUrlFilterList urlFilterData = null;
		try {
			// 一覧情報検索共通処理
			UrlFilterList urlFilterList = new UrlFilterList();
			List<SelectUrlFilterList> selUrlFilLst = urlFilterList.getUrlFilterList(queryDAO, uvo);
			for(int i=0; i< selUrlFilLst.size();i++) {
				if(selUrlFilLst.get(i).getSeqNo()
						== params.getUrlFilterList().get(params.getIndex()).getSeqNo()) {
					urlFilterData = selUrlFilLst.get(i);
					break;
				}
			}
		} catch(Exception e) {
			// 例外
			Object[] objectgArray = {e.getMessage()};
			// Webフィルタ処理で例外が発生しました。{0}
			log.error(messageAccessor.getMessage(
								"EK061009", objectgArray, params.getUvo()));
			// Webフィルタ編集画面表示に失敗しました。
			errorMessage.add("message", new BLogicMessage("DK060079"));
			result.setErrors(errorMessage);
			result.setMessages(errorMessage);
			result.setResultString("failure");
			return result;
		}

		/***********************************************************************
		 * 画面表示用Webフィルタカテゴリのリスト作成
		 **********************************************************************/
		List<UrlFilterCategoryInfo> categoryInfoList = null;
		try {
			UrlFilterCategoryInfoKey key = new UrlFilterCategoryInfoKey();
			key.setVsysId(uvo.getVsysId());
			key.setUrlfiltersSeqno(urlFilterData.getSeqNo());
			key.setDefaultAction(PropertyUtil.getProperty(
					"fwsetting.objectsetting.urlfilters.categories_sdefault_action"));

			// Webフィルタカテゴリを取得
			categoryInfoList = queryDAO.executeForObjectList("UrlFiltersBLogic-9", key);
		} catch (Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラーログ出力
			Object[] objectgArray = { e.getMessage() };
			// Webフィルタカテゴリの検索に失敗しました。{0}
			log.fatal(messageAccessor.getMessage("FK061034", objectgArray, params.getUvo()));
			// Webフィルタ編集画面表示に失敗しました。
			errorMessage.add("message", new BLogicMessage("DK060079"));
			result.setErrors(errorMessage);
			result.setResultString("failure");
			return result;
		}

		/***********************************************************************
		 * ブロック時動作のリスト作成
		 **********************************************************************/
		List<CommonSelectItem> blockActionList =new ArrayList<CommonSelectItem>();
		{
			String blockActionCsv = PropertyUtil.getProperty("fwsetting.objectsetting.urlfilters.blockaction");
			String[] strBlockActionList = blockActionCsv.split(",");
			for (String str : strBlockActionList) {
				CommonSelectItem item = new CommonSelectItem();
//				item.setValue(str);
//				item.setText(str);
				String[] str2 = str.split(":");
				item.setValue(str2[0]);
				item.setText(str2[1]);
				blockActionList.add(item);
			}
		}

		/***********************************************************************
		 * カテゴリアクションのリスト作成
		 **********************************************************************/
		List<CommonSelectItem> categoryActionList =new ArrayList<CommonSelectItem>();
		{
			String ctgrActCsv = PropertyUtil.getProperty("fwsetting.objectsetting.urlfilters.categoryaction");
			String[] ctgrActList = ctgrActCsv.split(",");
			for (String str : ctgrActList) {
				CommonSelectItem item = new CommonSelectItem();
//				item.setValue(str);
//				item.setText(str);
				String[] str2 = str.split(":");
				item.setValue(str2[0]);
				item.setText(str2[1]);
				categoryActionList.add(item);
			}
		}

		// アウトプットオブジェクト生成
		EditUrlFilterOutput out = new EditUrlFilterOutput();
		// アウトプット情報設定
		{
			out.setName(urlFilterData.getName());
			out.setComment(urlFilterData.getComment());
			out.setBlockListSite(urlFilterData.getBlockListSite());
			out.setActionBlockListSite(urlFilterData.getActionBlockListSite());
			out.setAllowListSite(urlFilterData.getAllowListSite());
			out.setCategoryList(categoryInfoList);
			out.setBlockActionList(blockActionList);
			out.setCategoryActionList(categoryActionList);
		}
		result.setResultObject(out);
		result.setResultString("success");
		log.debug("EditUrlFilterBLogic処理終了");
		// レスポンス返却
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("queryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("queryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
