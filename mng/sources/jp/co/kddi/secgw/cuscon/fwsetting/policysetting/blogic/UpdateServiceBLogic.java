/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateServiceBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.SecurityRules;
import jp.co.kddi.secgw.cuscon.fwsetting.common.PolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.LinkInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.PolicyOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.UpdateServiceInput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : UpdateServiceBLogic
 * 機能概要 :Service項目編集画面にて「OK」押下時の処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class UpdateServiceBLogic implements BLogic<UpdateServiceInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;
	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic.UpdateServiceBLogic");
	private MessageAccessor messageAccessor = null;
	/**
	 * メソッド名 :execute
	 * 機能概要 :選択サービスを更新する。
	 * @param param Service編集画面入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(UpdateServiceInput param) {
		log.debug("UpdateServiceBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");
        // エラーメッセージ
        BLogicMessages messages = new BLogicMessages();

//2011/10/31 add start inoue@prosite any/application-default対応
        // application-defaulが選択された場合
        if(param.getRadio() == 1) {
    		// ポリシーリストクラスを生成する。
            PolicyList selrule = new PolicyList();
            List<SelectPolicyList> selpolicyList = null;
            try {
    	        // 編集中のポリシー情報を取得する。
            	selpolicyList = selrule.getPolicylist_SelPolicy(queryDAO, param.getUvo(),
    	        				CusconConst.ZERO_GENE, param.getName());
            } catch(Exception e) {
    			// 例外
            	Object[] objectgArray = {e.getMessage()};
            	log.error(messageAccessor.getMessage(
            			"EK051001", objectgArray, param.getUvo()));
            	messages.add("message", new BLogicMessage("DK050043"));
    			result.setErrors(messages);
    			result.setResultString("failure");
            	return result;
    		}
            //	アプリケーションにanyが設定されているか
            if ( selpolicyList.get(0).getApplication().equals(CusconConst.ANY) ){
            	messages.add("message", new BLogicMessage("DK050047"));
    			result.setErrors(messages);
    			result.setResultString("failure");
    			return result;
            }
        }
//2011/10/31 add end inoue@prosite any/application-default対応

        // 削除条件を設定する。
		SecurityRules base = new SecurityRules();
		base.setVsysId(param.getUvo().getVsysId());
		base.setGenerationNo(CusconConst.ZERO_GENE);
		base.setModFlg(CusconConst.MOD_NUM);
		base.setName(param.getName());
		base.setModName(param.getName());
		base.setServiceDefaultFlg(param.getRadio());

		try {

			 // 選択ポリシー情報
			List<SecurityRules> rule = null;
	        // 選択ポリシー情報を取得する。
			rule = queryDAO.executeForObjectList("PolicyRulesList-1", base);

			// 変更フラグが1でない場合
			if(rule.get(0).getModFlg() != CusconConst.ADD_NUM) {
				log.debug("変更フラグが1以外");
				// 変更フラグが1でない場合、変更フラグを2に更新する。
				updateDAO.execute("UpdateServiceBLogic-2", base);
			} else {
				log.debug("変更フラグが1");
				// 変更フラグ、サービスフラグを更新する。
				updateDAO.execute("UpdateServiceBLogic-3", base);
			}

			// シーケンス番号に紐付くサービスリンクを削除する。
			updateDAO.execute("DelPolicyBLogic-9", rule.get(0).getSeqNo());

			// 選択するを選択した場合
			if(param.getRadio() == CusconConst.SELECT) {
				log.debug("登録処理を行う。index = 2");
				// 登録条件を設定する。
				LinkInfo link = new LinkInfo();
				List<String> registList = new ArrayList<String>();

				String[] checkList = param.getCheckList().split(",");
				// 選択されたサービスを取得する。
				for(int i=0; i<checkList.length; i++) {
					log.debug("選択されたサービスを取得:checkList[" +
										i + "] = " + checkList[i]);
					registList.add(param.getServiceMasterList().get(
								Integer.parseInt(checkList[i])).getName());
				}

				link.setVsysId(param.getUvo().getVsysId());
				link.setSeqNo(rule.get(0).getSeqNo());
				link.setLinkNameList(registList);

				// サービスリンクを登録する。
				updateDAO.execute("UpdateServiceBLogic-1", link);
			}
		 } catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage(
					"FK051004", objectgArray, param.getUvo()));
			messages.add("message", new BLogicMessage("DK050023"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		 }

		// ポリシーリストクラスを生成する。
        PolicyList rule = new PolicyList();

        // 選択された世代番号のポリシーリスト
        List<SelectPolicyList> policyList = null;
        try {
	        // 選択された世代番号のポリシーリストを取得する。
	        policyList = rule.getPolicylist(queryDAO, param.getUvo(),
	        				CusconConst.ZERO_GENE, CusconConst.POLICY);
        } catch(Exception e) {
			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"EK051001", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK050023"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

        // ビジネスロジックの出力クラスに結果を設定する
        PolicyOutput out = new PolicyOutput();
        out.setPolicyList(policyList);
        result.setResultObject(out);
        result.setResultString("success");

        log.debug("UpdateServiceBLogic処理終了");
		return result;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
