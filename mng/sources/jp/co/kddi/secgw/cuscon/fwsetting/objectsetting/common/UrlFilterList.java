/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UrlFilterList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/20     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common;

import java.util.ArrayList;
import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.common.vo.UrlFilters;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.CommonSelectItem;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectUrlFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.UrlFilterCategoryInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.UrlFilterCategoryInfoKey;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;
import jp.terasoluna.fw.util.PropertyUtil;

import org.apache.log4j.Logger;

/**
 * クラス名 : UrlFilterList
 * 機能概要 : Webフィルタ一覧を表示するための情報を取得する。
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2010/02/20
 *          新規作成
 * @see
 */
public class UrlFilterList {

	private static Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.UrlFilterList");

	// メッセージクラス
    private static MessageAccessor messageAccessor = null;

    // リストの表示上限件数の有無(true:あり、false:なし）
    private boolean listLimitSet;
    public void setListLimitSet(boolean value) {
    	listLimitSet = value;
    }

    /**
     * メソッド名 : コンストラクタ
     * 機能概要 : -
     * @param queryDAO
     * @param uvo
     */
    public UrlFilterList() {
    	log.debug("UrlFilterListコンストラクタ2処理開始");
    	listLimitSet = false;
    	log.debug("UrlFilterListコンストラクタ2処理終了");
    }

	/**
	 * メソッド名 :getUrlFilterList
	 * 機能概要 : Webフィルタ一覧情報取得処理
	 * @param vsysId
	 * @return resultList
	 * @throws Exception
	 */
	public List<SelectUrlFilterList> getUrlFilterList(QueryDAO queryDAO,
										CusconUVO uvo) throws Exception {
		log.debug("Webフィルタ一覧情報取得処理開始");
		// InputBase設定
		InputBase inputBase = new InputBase();
		inputBase.setVsysId(uvo.getVsysId());                // Vsys-ID
		inputBase.setGenerationNo(CusconConst.ZERO_GENE);    // 世代番号
		inputBase.setModFlg(CusconConst.DEL_NUM);            // 変更フラグ
		// Webフィルタ情報取得
		List<UrlFilters> urlFilterList;
		try {
			// Webフィルタ情報取得
			urlFilterList = new ArrayList<UrlFilters>();
			urlFilterList = queryDAO.executeForObjectList("CommonT_UrlFilters-1", inputBase);
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		// Webフィルタの検索に失敗しました。{0}
    		log.fatal(messageAccessor.getMessage("FK061030", objectgArray, uvo));
    		throw e;
		}

		/***********************************************************************
		 * アクションのリスト作成
		 **********************************************************************/
		List<CommonSelectItem> blockActionList =new ArrayList<CommonSelectItem>();
		{
			String blockActionCsv = PropertyUtil.getProperty("fwsetting.objectsetting.urlfilters.blockaction");
			String[] strBlockActionList = blockActionCsv.split(",");
			for (String str : strBlockActionList) {
				CommonSelectItem item = new CommonSelectItem();
				String[] str2 = str.split(":");
				item.setValue(str2[0]);
				item.setText(str2[1]);
				blockActionList.add(item);
			}
		}

		// 返却用リスト作成
		List <SelectUrlFilterList> resultList = new ArrayList<SelectUrlFilterList>();

		// 取得したWebフィルタ情報の件数分、返却用リストに設定
		for(int i = 0; i < urlFilterList.size(); i++) {
			log.debug("Webフィルタ情報の件数:" + i);
			SelectUrlFilterList selectUrlFilterListOutput = new SelectUrlFilterList();
			{
				selectUrlFilterListOutput.setSeqNo(urlFilterList.get(i).getSeqNo());
				selectUrlFilterListOutput.setName(urlFilterList.get(i).getName());
				selectUrlFilterListOutput.setComment(urlFilterList.get(i).getComment());
				if(listLimitSet == false) {
					selectUrlFilterListOutput.setBlockListSite(urlFilterList.get(i).getBlockListSite());
					selectUrlFilterListOutput.setAllowListSite(urlFilterList.get(i).getAllowListSite());
				} else {
					selectUrlFilterListOutput.setBlockListSite(
							getStringSpecifiedNumberOfLine(urlFilterList.get(i).getBlockListSite()));
					selectUrlFilterListOutput.setAllowListSite(
							getStringSpecifiedNumberOfLine(urlFilterList.get(i).getAllowListSite()));
				}
				selectUrlFilterListOutput.setBlockListSiteCnt(urlFilterList.get(i).getBlockListSiteCnt());
				selectUrlFilterListOutput.setActionBlockListSite(urlFilterList.get(i).getActionBlockListSite());
				for (CommonSelectItem item : blockActionList) {
					if(item.getValue().equals(urlFilterList.get(i).getActionBlockListSite())) {
						selectUrlFilterListOutput.setActionBlockListSiteWamei(item.getText());
						break;
					}
				}
				selectUrlFilterListOutput.setAllowListSiteCnt(urlFilterList.get(i).getAllowListSiteCnt());

				/***********************************************************************
				 * 画面表示用Webフィルタカテゴリのリスト作成
				 **********************************************************************/
				List<UrlFilterCategoryInfo> categoryInfoList = null;
				try {
					UrlFilterCategoryInfoKey key = new UrlFilterCategoryInfoKey();
					key.setVsysId(uvo.getVsysId());
					key.setUrlfiltersSeqno(urlFilterList.get(i).getSeqNo());
					key.setDefaultAction(PropertyUtil.getProperty(
							"fwsetting.objectsetting.urlfilters.categories_default_action"));

					// Webフィルタカテゴリを取得
					categoryInfoList = queryDAO.executeForObjectList("UrlFiltersBLogic-9", key);
				} catch (Exception e) {
					// トランザクションロールバック
					TransactionUtil.setRollbackOnly();
		    		// DBアクセスエラー
		    		Object[] objectgArray = {e.getMessage()};
					// Webフィルタカテゴリの検索に失敗しました。{0}
		    		log.fatal(messageAccessor.getMessage("FK061034", objectgArray, uvo));
		    		throw e;
				}

				StringBuilder alertCategory = new StringBuilder();
				StringBuilder blockCategory = new StringBuilder();
				StringBuilder continueCategory = new StringBuilder();

				for(int j=0; j<categoryInfoList.size(); j++) {
					if(categoryInfoList.get(j) != null && categoryInfoList.get(j).getAction() != null) {
						if(categoryInfoList.get(j).getAction().equals("alert")) {
							alertCategory.append(categoryInfoList.get(j).getName());
							alertCategory.append("\r\n");
						} else if (categoryInfoList.get(j).getAction().equals("block")) {
							blockCategory.append(categoryInfoList.get(j).getName());
							blockCategory.append("\r\n");
						} else if (categoryInfoList.get(j).getAction().equals("continue")) {
							continueCategory.append(categoryInfoList.get(j).getName());
							continueCategory.append("\r\n");
						}
					}
				}
				if(listLimitSet == false) {
					selectUrlFilterListOutput.setAlertCategory(alertCategory.toString());
					selectUrlFilterListOutput.setBlockCategory(blockCategory.toString());
					selectUrlFilterListOutput.setContinueCategory(continueCategory.toString());
				} else {
					selectUrlFilterListOutput.setBlockListSite(
							getStringSpecifiedNumberOfLine(urlFilterList.get(i).getBlockListSite()));
					selectUrlFilterListOutput.setAlertCategory(getStringSpecifiedNumberOfLine(alertCategory.toString()));
					selectUrlFilterListOutput.setBlockCategory(getStringSpecifiedNumberOfLine(blockCategory.toString()));
					selectUrlFilterListOutput.setContinueCategory(getStringSpecifiedNumberOfLine(continueCategory.toString()));
				}
			}
			resultList.add(selectUrlFilterListOutput);
		}
		log.debug("Webフィルタ一覧情報取得処理終了");
		return resultList;
	}

	/**
	 * メソッド名 :指定行数内文字列取得
	 * 機能概要 :指定行数内の文字列を取得する
	 * @param text 文字列
	 * @return 編集後文字列
	 */
	private String getStringSpecifiedNumberOfLine(String text) {
		if(text == null || text.length() == 0) {
			return text;
		}

		int dispListMax = Integer.valueOf(
				PropertyUtil.getProperty("fwsetting.objectsetting.urlfilters.display_list_max"));
		String[] tmpStrLst = text.split("\r?\n|\r");
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i<tmpStrLst.length; i++) {
			if(dispListMax < i + 1) {
				sb.append("more...");
				break;
			}
			sb.append(tmpStrLst[i]);
			sb.append("\r\n");
		}
		return sb.toString();
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor msgAcc){
		log.debug("setMessageAccessor処理開始");
	    setMsgAcc(msgAcc);
	    log.debug("setMessageAccessor処理終了");
	}

	/**
	 * メソッド名 : setMsgAcc
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public static void setMsgAcc(MessageAccessor messageAccessor) {
		log.debug("setMsgAcc処理開始");
		UrlFilterList.messageAccessor = messageAccessor;
		log.debug("setMsgAcc処理終了");
	}

	/**
	 * メソッド名 : getMessage
	 * 機能概要 : messageAccessorをゲットする。
	 * @return MessageAccessor
	 */
	public static MessageAccessor getMessage() {
		log.debug("getMessage処理");
		return messageAccessor;
	}
}
