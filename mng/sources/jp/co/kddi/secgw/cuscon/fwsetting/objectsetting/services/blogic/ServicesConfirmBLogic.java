/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ServicesConfirmInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/08     morisou                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.blogic;

import java.util.ArrayList;
import java.util.List;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectServiceList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.dto.ServicesConfirmInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.dto.ServicesOutput;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicResult;
import org.apache.log4j.Logger;

/**
 * クラス名 : ServicesConfirmInput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/08
 *          新規作成
 * @see
 */
public class ServicesConfirmBLogic implements BLogic<ServicesConfirmInput> {

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.blogic.ServicesConfirmInput");

	/**
	 * メソッド名 :execute
	 * 機能概要 :サービス削除確認対象表示処理を行う。
	 * @param param ServicesConfirmInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(ServicesConfirmInput param) {

		log.debug("ServicesConfirmBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");

		// アウトプットデータクラスに設定する。
        ServicesOutput out = new ServicesOutput();
        List<SelectServiceList> serviceList = new ArrayList<SelectServiceList>();

        String[] indexList = param.getCheckIndex().split(",");

        // 選択チェック分繰り返す。
        for(int i=0; i< indexList.length; i++) {
        	log.debug("選択チェック:" + i);
        	serviceList.add(param.getServiceList().get(Integer.parseInt(indexList[i])));
        }

	    out.setServiceList(serviceList);

	    result.setResultObject(out);
	    result.setResultString("success");

	    log.debug("ServicesConfirmBLogic処理終了");
		return result;
	}
}
