/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateUrlFilterBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/07     ktakenaka@PROSITE                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.blogic;

import java.io.UnsupportedEncodingException;
import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.UrlFilterCategories;
import jp.co.kddi.secgw.cuscon.common.vo.UrlFilters;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.UrlFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectUrlFilterBlockAllowSiteSumCnt;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectUrlFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.UrlFilterCategoryInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.dto.UpdateUrlFilterInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.dto.UrlFiltersOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;
import jp.terasoluna.fw.util.PropertyUtil;

import org.apache.log4j.Logger;

/**
 * クラス名 : UpdateUrlFilterBLogic
 * 機能概要 :
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/07
 *          新規作成
 * @see
 */
public class UpdateUrlFilterBLogic implements BLogic<UpdateUrlFilterInput> {
	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.blogic.UpdateUrlFilterBLogic");

	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 :
	 * @param params
	 * @return
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(UpdateUrlFilterInput params) {
		log.debug("UpdateUrlFilterBLogic処理開始");

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		BLogicMessages errorMessages = new BLogicMessages();	// エラーメッセージ
		result.setResultString("failure");

		/***********************************************************************
		 * 変更前Webフィルタ情報を取得
		 **********************************************************************/
		List<UrlFilters> oldUrlFilterList = null;
		try {
			// 検索条件設定
			UrlFilters oldParam = new UrlFilters();
			oldParam.setVsysId(params.getUvo().getVsysId()); // Vsys-ID
			oldParam.setName(params.getOldUrlFilterName()); // 変更前オブジェクト名
			oldParam.setGenerationNo(CusconConst.ZERO_GENE); // 世代番号

			// 変更前Webフィルタ情報を取得
			oldUrlFilterList = queryDAO.executeForObjectList("UrlFiltersBLogic-14", oldParam);
		} catch (Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラーログ出力
			Object[] objectgArray = { e.getMessage() };
			// Webフィルタの検索に失敗しました。{0}
			log.fatal(messageAccessor.getMessage("FK061030", objectgArray, params.getUvo()));
			// Webフィルタ更新処理に失敗しました。
			errorMessages.add("message", new BLogicMessage("DK060080"));
			result.setErrors(errorMessages);
			result.setResultString("failure");
			return result;
		}

		/***********************************************************************
		 * 更新レコード以外のオブジェクトの登録数を取得
		 **********************************************************************/
		// オブジェクトの登録数
		int registNum = 0;
		try {
			UrlFilters key = new UrlFilters();
			key.setSeqNo(oldUrlFilterList.get(0).getSeqNo());
			key.setVsysId(params.getUvo().getVsysId());
			key.setGenerationNo(CusconConst.ZERO_GENE);
			key.setModFlg(CusconConst.DEL_NUM);

			// オブジェクトの登録数を取得する。
			registNum = queryDAO.executeForObject("UrlFiltersBLogic-18", key, java.lang.Integer.class);
		} catch (Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = { e.getMessage() };
    		// Webフィルタの検索に失敗しました。{0}
    		log.fatal(messageAccessor.getMessage("FK061030", objectgArray, params.getUvo()));
			// Webフィルタ更新処理に失敗しました。
			errorMessages.add("message", new BLogicMessage("DK060080"));
			result.setErrors(errorMessages);
			result.setResultString("failure");
			return result;
		}

		/***********************************************************************
		 * ブロックリスト・許可リスト登録上限数チェック
		 **********************************************************************/
		int addBlockCnt = 0;
		int addAllowCnt = 0;
		StringBuilder editedBlockListSite = new StringBuilder();
		StringBuilder editedAllowListSite = new StringBuilder();
		{
			// サイト文字列の上限を取得
			int siteTextMax = Integer.parseInt(PropertyUtil.getProperty("fwsetting.objectsetting.urlfilters.site_text_max"));

			// ブロックリストをリストに登録
			String blockListSite = params.getNewBlockListSite();
			if (blockListSite != null && blockListSite.length() > 0) {
				for (String line : blockListSite.split("\r?\n|\r")) {
					if (line != null && line.length() > 0) {
						// 文字列長チェック(バイト)
//						if(line.length() > siteTextMax) {
						int len = getByteLength(line);
						if(len < 0){ // 変換エラー時
				    		// トランザクションロールバック
				    		TransactionUtil.setRollbackOnly();
				        	log.debug("ブロックリスト中の文字列のバイト変換に失敗しました。");
				        	// ブロックリストの入力桁数を超えています。{0}
				        	errorMessages.add("message", new BLogicMessage("DK060086", line));
							result.setErrors(errorMessages);
							result.setResultString("failure");
				        	return result;
						}
						else{
							if(len > siteTextMax) {
					    		// トランザクションロールバック
					    		TransactionUtil.setRollbackOnly();
					        	log.debug("ブロックリストの入力桁数を超えているため登録できませんでした。");
					        	// ブロックリストの入力桁数を超えています。{0}
					        	errorMessages.add("message", new BLogicMessage("DK060084", line));
								result.setErrors(errorMessages);
								result.setResultString("failure");
					        	return result;
							}
						}
						addBlockCnt++;
						editedBlockListSite.append(line);
						editedBlockListSite.append("\r\n");
					}
				}
			}

			// 許可リストをリストに登録
			String allowListSite = params.getNewAllowListSite();
			if (allowListSite != null && allowListSite.length() > 0) {
				for (String line : allowListSite.split("\r?\n|\r")) {
					if (line != null && line.length() > 0) {
						// 文字列長チェック(バイト)
//						if(line.length() > siteTextMax) {
						int len = getByteLength(line);
						if(len < 0){ // 変換エラー時
				    		// トランザクションロールバック
				    		TransactionUtil.setRollbackOnly();
				        	log.debug("許可リスト中の文字列のバイト変換に失敗しました。");
				        	// ブロックリストの入力桁数を超えています。{0}
				        	errorMessages.add("message", new BLogicMessage("DK060087", line));
							result.setErrors(errorMessages);
							result.setResultString("failure");
				        	return result;
						}
						else{
							if(len > siteTextMax) {
								// トランザクションロールバック
					    		TransactionUtil.setRollbackOnly();
					        	log.debug("許可リストの入力桁数を超えているため登録できませんでした。");
					        	// 許可リストの入力桁数を超えています。{0}
					        	errorMessages.add("message", new BLogicMessage("DK060085", line));
								result.setErrors(errorMessages);
								result.setResultString("failure");
					        	return result;
							}
						}
						addAllowCnt++;
						editedAllowListSite.append(line);
						editedAllowListSite.append("\r\n");
					}
				}
			}

			// ブロックリスト・許可リストの登録総件数を取得
			SelectUrlFilterBlockAllowSiteSumCnt blockAllowSum = null;
			if (registNum > 0) {
				UrlFilters key = new UrlFilters();
				key.setSeqNo(oldUrlFilterList.get(0).getSeqNo());
				key.setVsysId(params.getUvo().getVsysId());
				key.setGenerationNo(CusconConst.ZERO_GENE);
				key.setModFlg(CusconConst.DEL_NUM);
				try {
					// 自分自身以外の合計件数を取得
					blockAllowSum = queryDAO.executeForObject("UrlFiltersBLogic-17", key, SelectUrlFilterBlockAllowSiteSumCnt.class);
				} catch (Exception e) {
					// トランザクションロールバック
					TransactionUtil.setRollbackOnly();
					// DBアクセスエラー
					Object[] objectgArray = { e.getMessage() };
		    		// Webフィルタの検索に失敗しました。{0}
		    		log.fatal(messageAccessor.getMessage("FK061030", objectgArray, params.getUvo()));
					// Webフィルタ更新処理に失敗しました。
					errorMessages.add("message", new BLogicMessage("DK060080"));
					result.setErrors(errorMessages);
					result.setResultString("failure");
					return result;
				}
			}

	    	// 登録後のブロックリストの合計数を取得
			int addedBlockSum = 0;
			if (blockAllowSum != null) {
				addedBlockSum = blockAllowSum.getBlockSum() + addBlockCnt;
			} else {
				addedBlockSum = addBlockCnt;
			}

			// ブロックリスト上限値チェック
			if (addedBlockSum > params.getUvo().getBlocklistMax()) {
				// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
				log.debug("ブロックリスト登録上限値のため登録できませんでした。");
	        	// ブロックリスト登録上限値のため、追加できません。
				errorMessages.add("message", new BLogicMessage("DK060081"));
				result.setErrors(errorMessages);
				result.setResultString("failure");
				return result;
			}

			// 登録後の許可リストの合計数を取得
			int addedAllowSum = 0;
			if (blockAllowSum != null) {
				addedAllowSum = blockAllowSum.getAllowSum() + addAllowCnt;
			} else {
				addedAllowSum = addAllowCnt;
			}

			// 許可リスト上限値チェック
			if (addedAllowSum > params.getUvo().getBlocklistMax()) {
				// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
				log.debug("許可リスト登録上限値のため登録できませんでした。");
	        	// 許可リスト登録上限値のため、追加できません。
				errorMessages.add("message", new BLogicMessage("DK060082"));
				result.setErrors(errorMessages);
				result.setResultString("failure");
				return result;
			}
		}
		/***********************************************************************
		 * 禁止オブジェクト名チェック
		 **********************************************************************/
		// 禁止オブジェクト名をプロパティファイルから取得
		String unavailableObjectName = PropertyUtil.getProperty("fwsetting.objectsetting.urlfilters.unavailable_object_name");
		if (unavailableObjectName != null && unavailableObjectName.length() > 0) {
			for (String unName : unavailableObjectName.split(",")) {
				if (params.getNewUrlFilterName().equals(unName)) {
		        	log.debug("禁止オブジェクト名のため登録できませんでした。");
					// 使用できないオブジェクト名です
					errorMessages.add("message", new BLogicMessage("DK060078"));
					result.setErrors(errorMessages);
					result.setResultString("failure");
					return result;
				}
			}
		}

		/***********************************************************************
		 * 存在チェック
		 **********************************************************************/
		// 変更前と変更後のWebフィルタ名が異なる場合
		if (!params.getNewUrlFilterName().equals(params.getOldUrlFilterName())) {
			// Webフィルタオブジェクトテーブルに対し、変更後Webフィルタ名と同名のレコードが既に存在しているか検索
			List<UrlFilters> urlFilterObjectList;
			try {
				// 検索条件設定
				UrlFilters searchUrlFilterKey = new UrlFilters();
				searchUrlFilterKey.setVsysId(params.getUvo().getVsysId()); // Vsys-ID
				searchUrlFilterKey.setGenerationNo(CusconConst.ZERO_GENE); // 世代番号
				searchUrlFilterKey.setName(params.getNewUrlFilterName()); // Webフィルタ名

				// Webフィルタオブジェクトテーブルに対し、変更後Webフィルタ名と同名のレコードが既に存在しているか検索
				urlFilterObjectList = queryDAO.executeForObjectList("UrlFiltersBLogic-2", searchUrlFilterKey);
			} catch (Exception e) {
				// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
				// DBアクセスエラーログ出力
				Object[] objectgArray = { e.getMessage() };
				// Webフィルタの検索に失敗しました。{0}
				log.fatal(messageAccessor.getMessage("FK061030", objectgArray, params.getUvo()));
				// Webフィルタ更新処理に失敗しました。
				errorMessages.add("message", new BLogicMessage("DK060080"));
				result.setErrors(errorMessages);
				result.setResultString("failure");
				return result;
			}

			// DB検索結果が1件以上かつ、MOD_FLGのステータスが削除でない場合
			if (!urlFilterObjectList.isEmpty() && urlFilterObjectList.get(0).getModFlg() != CusconConst.DEL_NUM) {
				// 重複エラーとしてレスポンスデータを作成し返却
				log.debug("既に該当オブジェクト名が存在する。");
				// 既にオブジェクト名が登録されています。
	        	errorMessages.add("message", new BLogicMessage("DK060001"));
				result.setErrors(errorMessages);
				result.setResultString("failure");
				return result;

				// DB検索結果が1件以上かつ、MOD_FLGのステータスが削除の場合
			} else if (!urlFilterObjectList.isEmpty() && urlFilterObjectList.get(0).getModFlg() == CusconConst.DEL_NUM) {
				log.debug("DB検索結果が1件以上かつ、MOD_FLGのステータスが削除");
				try {
					// SEQ_NOをキーに、UpdateDAOクラスを使用し、Webフィルタテーブルのレコードを削除
					updateDAO.execute("UrlFiltersBLogic-3", urlFilterObjectList.get(0).getSeqNo());
				} catch (Exception e) {
					// トランザクションロールバック
					TransactionUtil.setRollbackOnly();
					// DBアクセスエラーログ出力
					Object[] objectgArray = { e.getMessage() };
					// Webフィルタの削除に失敗しました。{0}
					log.fatal(messageAccessor.getMessage("FK061031", objectgArray, params.getUvo()));
					// Webフィルタ更新処理に失敗しました。
					errorMessages.add("message", new BLogicMessage("DK060080"));
					result.setErrors(errorMessages);
					result.setResultString("failure");
					return result;
				}
			}
		}

		/***********************************************************************
		 * 更新処理
		 **********************************************************************/
		try {
			// 取得したレコードの変更フラグが追加の場合（更新フラグは書き換えずupdate）
			if (oldUrlFilterList.get(0).getModFlg() == CusconConst.ADD_NUM) {
				log.debug("変更フラグが追加");
				// update用param作成
				UrlFilters updateParamSp = new UrlFilters();
				updateParamSp.setSeqNo(oldUrlFilterList.get(0).getSeqNo()); 			// シーケンス番号
				updateParamSp.setName(params.getNewUrlFilterName());					// Webフィルタ名
				updateParamSp.setComment(params.getComment());							// 説明
				updateParamSp.setBlockListSite(editedBlockListSite.toString());			// ブロックリストサイト
				updateParamSp.setBlockListSiteCnt(addBlockCnt);							// ブロックリストサイト数
				updateParamSp.setActionBlockListSite(params.getActionBlockListSite());	// ブロックサイト動作
				updateParamSp.setAllowListSite(editedAllowListSite.toString());			// 許可リストサイト
				updateParamSp.setAllowListSiteCnt(addAllowCnt);							// 許可リストサイト数
				updateDAO.execute("UrlFiltersBLogic-10", updateParamSp);

				// 取得したレコードの変更フラグが追加でない場合（更新フラグも書き換えてupdate）
			} else {
				log.debug("変更フラグが追加以外");
				// update用param作成
				UrlFilters updateParam = new UrlFilters();
				updateParam.setSeqNo(oldUrlFilterList.get(0).getSeqNo());				// シーケンス番号
				updateParam.setName(params.getNewUrlFilterName());						// Webフィルタ名
				updateParam.setComment(params.getComment());							// 説明
				updateParam.setBlockListSite(editedBlockListSite.toString());			// ブロックリストサイト
				updateParam.setBlockListSiteCnt(addBlockCnt);							// ブロックリストサイト数
				updateParam.setActionBlockListSite(params.getActionBlockListSite());	// ブロックサイト動作
				updateParam.setAllowListSite(editedAllowListSite.toString());			// 許可リストサイト
				updateParam.setAllowListSiteCnt(addAllowCnt);							// 許可リストサイト数
				updateParam.setModFlg(CusconConst.MOD_NUM);								// 更新フラグ
				updateDAO.execute("UrlFiltersBLogic-11", updateParam);
			}
		} catch (Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラーログ出力
			Object[] objectgArray = { e.getMessage() };
			// Webフィルタの更新に失敗しました。{0}
			log.fatal(messageAccessor.getMessage("FK061036", objectgArray, params.getUvo()));
			// Webフィルタ更新処理に失敗しました。
			errorMessages.add("message", new BLogicMessage("DK060080"));
			result.setErrors(errorMessages);
			result.setResultString("failure");
			return result;
		}

		/***********************************************************************
		 * カテゴリ情報削除処理
		 **********************************************************************/
		try {
			// Webフィルタに紐づくカテゴリ情報削除
			updateDAO.execute("UrlFiltersBLogic-12", oldUrlFilterList.get(0).getSeqNo());
		} catch (Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = { e.getMessage() };
			log.fatal(messageAccessor.getMessage("FK061007", objectgArray, params.getUvo()));
			// Webフィルタ更新処理に失敗しました。
			errorMessages.add("message", new BLogicMessage("DK060080"));
			result.setErrors(errorMessages);
			result.setResultString("failure");
			return result;
		}

		/***********************************************************************
		 * 登録処理（Webフィルタカテゴリ情報）
		 **********************************************************************/
		String[] strSelectActionList = params.getSelectActionList().split(",");

		// Webフィルタカテゴリ情報（T_UrlFilter_Categories）を登録
		for (int i = 0; i < params.getCategoryList().size(); i++) {
			if (params.getCategoryList().get(i) == null) {
				continue;
			}

			log.debug("新規追加したWebフィルタに紐づくカテゴリ情報を登録:" + i);
			try {
				UrlFilterCategoryInfo info = params.getCategoryList().get(i);
				UrlFilterCategories categories = new UrlFilterCategories();
				// Webフィルタシーケンス番号を設定
				categories.setVsysId(params.getUvo().getVsysId());
				categories.setUrlfiltersSeqno(oldUrlFilterList.get(0).getSeqNo());
				categories.setUrlcategorySeqno(info.getSeqNo());
				categories.setGenerationNo(CusconConst.ZERO_GENE);
				categories.setAction(strSelectActionList[i]);

				// カテゴリ情報登録
				updateDAO.execute("UrlFiltersBLogic-5", categories);
			} catch (Exception e) {
				// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
				// DBアクセスエラーログ出力
				Object[] objectgArray = { e.getMessage() };
				// Webフィルタカテゴリの登録に失敗しました。{0}
				log.fatal(messageAccessor.getMessage("FK061033", objectgArray, params.getUvo()));
				// Webフィルタ更新処理に失敗しました。
				errorMessages.add("message", new BLogicMessage("DK060080"));
				result.setErrors(errorMessages);
				result.setResultString("failure");
				return result;
			}
		}

		/***********************************************************************
		 * 一覧情報検索共通処理
		 **********************************************************************/
		List<SelectUrlFilterList> selUrlFilLst = null;
		try {
			UrlFilterList urlFilterList = new UrlFilterList();
			urlFilterList.setListLimitSet(true);
			selUrlFilLst = urlFilterList.getUrlFilterList(queryDAO, params.getUvo());
		} catch (Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// 例外
			Object[] objectgArray = { e.getMessage() };
			// Webフィルタの検索に失敗しました。{0}
			log.error(messageAccessor.getMessage("FK061030", objectgArray, params.getUvo()));
			// Webフィルタ更新処理に失敗しました。
			errorMessages.add("message", new BLogicMessage("DK060080"));
			result.setErrors(errorMessages);
			result.setResultString("failure");
			return result;
		}

		/***********************************************************************
		 * レスポンス返却
		 **********************************************************************/
		UrlFiltersOutput output = new UrlFiltersOutput();
		output.setUrlFilterList(selUrlFilLst);
		result.setResultObject(output);
		result.setResultString("success");
		log.debug("UpdateUrlFilterBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("queryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("queryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}

	/**
	 * メソッド名 : getByteLengthのSetterメソッド
	 * 機能概要 : 指定文字列のバイト数を返す。エラー時は-1を返す。
	 * @param getByteLength
	 */
	public int getByteLength(String str) {
		log.debug("getByteLength処理開始");
		int len=0;
		try{
			len = str.getBytes("Shift_JIS").length;
		}catch(UnsupportedEncodingException e){
			len=-1;
		}
		log.debug("getByteLength処理終了");
		return len;
	}

}
