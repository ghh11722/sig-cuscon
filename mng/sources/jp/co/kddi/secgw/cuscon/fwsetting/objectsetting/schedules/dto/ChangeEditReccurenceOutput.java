/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ChangeEditReccurenceOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.vo.Schedules;

/**
 * クラス名 : ChangeEditReccurenceOutput
 * 機能概要 :編集画面recurrence変更出力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class ChangeEditReccurenceOutput {

	// 選択スケジュールデータクラス
	private Schedules selectSchedule = null;

	// 設定時間リスト
	private List<String> timeList = null;

	// 編集名
	private String showName = null;

	/**
	 * メソッド名 : selectScheduleのGetterメソッド
	 * 機能概要 : selectScheduleを取得する。
	 * @return selectSchedule
	 */
	public Schedules getSelectSchedule() {
		return selectSchedule;
	}

	/**
	 * メソッド名 : selectScheduleのSetterメソッド
	 * 機能概要 : selectScheduleをセットする。
	 * @param selectSchedule
	 */
	public void setSelectSchedule(Schedules selectSchedule) {
		this.selectSchedule = selectSchedule;
	}
	/**
	 * メソッド名 : timeListのGetterメソッド
	 * 機能概要 : timeListを取得する。
	 * @return timeList
	 */
	public List<String> getTimeList() {
		return timeList;
	}

	/**
	 * メソッド名 : timeListのSetterメソッド
	 * 機能概要 : timeListをセットする。
	 * @param timeList
	 */
	public void setTimeList(List<String> timeList) {
		this.timeList = timeList;
	}

	/**
	 * メソッド名 : showNameのGetterメソッド
	 * 機能概要 : showNameを取得する。
	 * @return showName
	 */
	public String getShowName() {
		return showName;
	}

	/**
	 * メソッド名 : showNameのSetterメソッド
	 * 機能概要 : showNameをセットする。
	 * @param showName
	 */
	public void setShowName(String showName) {
		this.showName = showName;
	}
}
