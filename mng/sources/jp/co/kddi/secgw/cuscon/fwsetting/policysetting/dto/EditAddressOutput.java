/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditAddressOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectObjectList;

/**
 * クラス名 : EditAddressOutput
 * 機能概要 :Address項目選択時の出力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class EditAddressOutput {

	// ルール名
	private String name = null;

	// アドレス名リスト
	private List<SelectObjectList> addressList = null;

	// アドレスマスタリスト
	private List<SelectObjectList> addressMasterList = null;

	// アドレス名
	private String addressName = null;


	/**
	 * メソッド名 : addressNameのGetterメソッド
	 * 機能概要 : addressNameを取得する。
	 * @return addressName
	 */
	public String getAddressName() {
		return addressName;
	}

	/**
	 * メソッド名 : addressNameのSetterメソッド
	 * 機能概要 : addressNameをセットする。
	 * @param addressName
	 */
	public void setAddressName(String addressName) {
		this.addressName = addressName;
	}

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * メソッド名 : addressListのGetterメソッド
	 * 機能概要 : addressListを取得する。
	 * @return addressList
	 */
	public List<SelectObjectList> getAddressList() {
		return addressList;
	}

	/**
	 * メソッド名 : addressListのSetterメソッド
	 * 機能概要 : addressListをセットする。
	 * @param addressList
	 */
	public void setAddressList(List<SelectObjectList> addressList) {
		this.addressList = addressList;
	}

	/**
	 * メソッド名 : addressMasterListのGetterメソッド
	 * 機能概要 : addressMasterListを取得する。
	 * @return addressMasterList
	 */
	public List<SelectObjectList> getAddressMasterList() {
		return addressMasterList;
	}

	/**
	 * メソッド名 : addressMasterListのSetterメソッド
	 * 機能概要 : addressMasterListをセットする。
	 * @param addressMasterList
	 */
	public void setAddressMasterList(List<SelectObjectList> addressMasterList) {
		this.addressMasterList = addressMasterList;
	}


}
