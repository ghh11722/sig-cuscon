/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectFilterMasterList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/24     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo;

import java.io.Serializable;
import java.util.List;

/**
 * クラス名 : SelectFilterMasterList
 * 機能概要 : フィルタマスタ一覧データクラス（カテゴリ、サブカテゴリ、テクノロジ、リスク）
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/24
 *          新規作成
 * @see
 */
public class SelectFilterMasterList implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 4954194902273458469L;
	// カテゴリリスト
	private List<SeqAndNameInfo> categoryList = null;
	// サブカテゴリリスト
	private List<SeqAndNameInfo> subCategoryList = null;
	// テクノロジリスト
	private List<SeqAndNameInfo> technologyList = null;
	// リスクリスト
	private List<RiskInfo> riskList = null;

	/**
	 * メソッド名 : categoryListのGetterメソッド
	 * 機能概要 : categoryListを取得する。
	 * @return categoryList
	 */
	public List<SeqAndNameInfo> getCategoryList() {
		return categoryList;
	}
	/**
	 * メソッド名 : categoryListのSetterメソッド
	 * 機能概要 : categoryListをセットする。
	 * @param categoryList
	 */
	public void setCategoryList(List<SeqAndNameInfo> categoryList) {
		this.categoryList = categoryList;
	}
	/**
	 * メソッド名 : subCategoryListのGetterメソッド
	 * 機能概要 : subCategoryListを取得する。
	 * @return subCategoryList
	 */
	public List<SeqAndNameInfo> getSubCategoryList() {
		return subCategoryList;
	}
	/**
	 * メソッド名 : subCategoryListのSetterメソッド
	 * 機能概要 : subCategoryListをセットする。
	 * @param subCategoryList
	 */
	public void setSubCategoryList(List<SeqAndNameInfo> subCategoryList) {
		this.subCategoryList = subCategoryList;
	}
	/**
	 * メソッド名 : technologyListのGetterメソッド
	 * 機能概要 : technologyListを取得する。
	 * @return technologyList
	 */
	public List<SeqAndNameInfo> getTechnologyList() {
		return technologyList;
	}
	/**
	 * メソッド名 : technologyListのSetterメソッド
	 * 機能概要 : technologyListをセットする。
	 * @param technologyList
	 */
	public void setTechnologyList(List<SeqAndNameInfo> technologyList) {
		this.technologyList = technologyList;
	}
	/**
	 * メソッド名 : riskListのGetterメソッド
	 * 機能概要 : riskListを取得する。
	 * @return riskList
	 */
	public List<RiskInfo> getRiskList() {
		return riskList;
	}
	/**
	 * メソッド名 : riskListのSetterメソッド
	 * 機能概要 : riskListをセットする。
	 * @param riskList
	 */
	public void setRiskList(List<RiskInfo> riskList) {
		this.riskList = riskList;
	}
}
