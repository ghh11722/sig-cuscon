/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ChangeEditReccurenceBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.blogic;

import org.apache.log4j.Logger;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.dto.ChangeEditReccurenceInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.dto.ChangeEditReccurenceOutput;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicResult;

/**
 * クラス名 : ChangeEditReccurenceBLogic
 * 機能概要 :編集画面にてrecurrence変更した際の処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class ChangeEditReccurenceBLogic implements
									BLogic<ChangeEditReccurenceInput> {

	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.blogic.ChangeEditReccurenceBLogic");
	/**
	 * メソッド名 :execute
	 * 機能概要 :編集画面にてrecurrence変更した際の処理を行う。
	 * @param param ChangeEditReccurenceInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(ChangeEditReccurenceInput param) {
		log.debug("ChangeEditReccurenceBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");

		// アウトプットデータクラスに設定する。
        ChangeEditReccurenceOutput out = new ChangeEditReccurenceOutput();

        // Name項目選択時のrecurrenceと変更したrecurrenceが一致する場合
        if(param.getShowRecurrence() ==
        						param.getSelectSchedule().getRecurrence()) {
        	log.debug("recurrenceが一致:Name選択時recurrence" +
        			param.getShowRecurrence() + ", 変更したrecurrence = " +
        			param.getSelectSchedule().getRecurrence());
        	out.setShowName(param.getShowName());

        	out.setTimeList(param.getSelectSchedule().getTimeList());

        // 一致しない場合
        } else {
        	log.debug("recurrenceが一致:Name選択時recurrence" +
        			param.getSelectSchedule().getRecurrence()+
        			", 変更したrecurrence = " + param.getShowRecurrence()
        			);
        	// name項目のみ設定する。
        	out.setShowName(param.getShowName());

        }

        // インプット情報をそのまま設定する。
        out.setSelectSchedule(param.getSelectSchedule());

        // ディリーの場合
        if(param.getShowRecurrence() == 0) {
        	log.debug("デイリー選択");
        	result.setResultString("DailySuccess");
        } else if(param.getShowRecurrence() == 1) {
        	log.debug("ウィークリー選択");
        	result.setResultString("WeeklySuccess");
        } else {
        	log.debug("指定日時選択");
        	result.setResultString("NonSuccess");
        }

	    result.setResultObject(out);
	    log.debug("ChangeEditReccurenceBLogic処理終了");

		return result;
	}

}
