/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : RegistApplicationGroupBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/26     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.blogic;

import java.util.ArrayList;
import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.ApplicationFilters;
import jp.co.kddi.secgw.cuscon.common.vo.ApplicationGroups;
import jp.co.kddi.secgw.cuscon.common.vo.ApplicationGroupsLink;
import jp.co.kddi.secgw.cuscon.common.vo.ApplicationsMaster;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.RegistLimitList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto.ApplicationGroupsOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto.RegistApplicationGroupInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.ApplicationGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;
import org.apache.log4j.Logger;

/**
 * クラス名 : RegistApplicationGroupBLogic
 * 機能概要 : アプリケーションオブジェクトの登録処理を行う。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/26
 *          新規作成
 * @see
 */
public class RegistApplicationGroupBLogic implements BLogic<RegistApplicationGroupInput> {

	// オブジェクトタイプ定数
	private static final int APL = 0;    // アプリケーションオブジェクトマスタ
	private static final int GRP = 1;    // アプリケーショングループオブジェクト
	private static final int FLT = 2;    // アプリケーションフィルタオブジェクト

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;

	private Logger log = Logger.getLogger(
	"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.blogic." +
	"RegistApplicationGroupBLogic");

	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 : アプリケーションオブジェクトの登録処理を行う。
	 * @param params RegistApplicationGroupInput入力データクラス
	 * @return result BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(RegistApplicationGroupInput params) {
		log.debug("RegistApplicationGroupBLogic処理開始");
		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得
		CusconUVO uvo = params.getUvo();
		String vsysId = uvo.getVsysId();

		/***********************************************************************
		 *  ①メンバ登録上限チェック
		 **********************************************************************/
		List<RegistLimitList> limitList = null;
		try {
			// 登録上限値を取得する。
			limitList = queryDAO.executeForObjectList("CommonM_SystemConstant-2", null);
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage(
					"FK061016", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060027"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

		int memberCnt = 0;
		if (!params.getSelectAplList()[0].equals("null") || params.getSelectAplList().length != 1) {
			log.debug("メンバ数取得");
			// グループ内に入れるメンバ数を取得する。
			memberCnt = (params.getSelectAplList()).length;
		}
		// メンバ数が0の場合
		if(memberCnt == 0) {
			log.debug("メンバ数が0件ののため登録できませんでした。");
			messages.add("message", new BLogicMessage("DK060071"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

		// 登録上限値とオブジェクト登録数の比較を行う。
		if(limitList.get(0).getAppGrpMember() < memberCnt) {
			log.debug("登録上限値のため登録できませんでした。");
			messages.add("message", new BLogicMessage("DK060003"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

//2011.10.31 add start inoue@prosite
		/***********************************************************************
		 *  ApplicationContainer存在チェック
		 **********************************************************************/
		List<String> aplContainList;
		try {
			// アプリケーションコンテナテーブルに対し、変更後アプリケーションフィルタ名と同名のレコードが既に存在しているか検索
			aplContainList = queryDAO.executeForObjectList("ApplicationContainerBLogic-1", params.getName());

		}  catch (Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061037", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060027"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
        }

		// DB検索結果が1件以上の場合
		if (!aplContainList.isEmpty()) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
        	messages.add("message", new BLogicMessage("DK060088"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}
//2011.10.31 add end inoue@prosite

		/***********************************************************************
		 *  ②複数テーブル間存在チェック
		 **********************************************************************/
		List<ApplicationsMaster> aplcationList = null;

		try {
			// M_Applicationsに今回登録したいフィルタ名と同名のレコードがないか検索
			aplcationList = queryDAO.executeForObjectList(
					"ApplicationFiltersBLogic-19", params.getName());

		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage(
					"FK061016", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060027"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

		// T_ApplicationFiltersに今回登録したいフィルタ名と同名のレコードがないか検索
		// 検索条件設定
		ApplicationFilters fltSearchKey = new ApplicationFilters();
		fltSearchKey.setVsysId(vsysId);
		fltSearchKey.setGenerationNo(CusconConst.ZERO_GENE);
		fltSearchKey.setModFlg(CusconConst.DEL_NUM);
		fltSearchKey.setName(params.getName());

		List<ApplicationFilters> fltList = null;
		try {
			fltList = queryDAO.executeForObjectList(
					"ApplicationFiltersBLogic-20", fltSearchKey);

		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage(
					"FK061016", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060027"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

		if (aplcationList.size() > 0 || fltList.size() > 0 ) {
			// 重複エラーとしてレスポンスデータを作成し返却
			log.debug("既に該当オブジェクト名が存在する。");
			messages.add("message", new BLogicMessage("DK060001"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}


		/***********************************************************************
		 *  ③存在チェック
		 **********************************************************************/
		// 検索条件設定
		ApplicationGroups searchApplicationGrpKey = new ApplicationGroups();
		searchApplicationGrpKey.setVsysId(vsysId);                         // Vsys-ID
		searchApplicationGrpKey.setGenerationNo(CusconConst.ZERO_GENE);    // 世代番号
		searchApplicationGrpKey.setName(params.getName());                 // アプリケーショングループ

		List<ApplicationGroups> aplGrpObjectList = null;
		try {
			// アプリケーショングループオブジェクトテーブルに対し、
			// 変更後アプリケーショングループ名と同名のレコードが既に存在しているか検索
			aplGrpObjectList = queryDAO.executeForObjectList(
					"ApplicationGroupsBLogic-2", searchApplicationGrpKey);
		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage(
					"FK061016", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060027"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}


		/***********************************************************************
		 *  ④存在チェックエラー処理
		 **********************************************************************/
		// DB検索結果が1件以上かつ、MOD_FLGのステータスが削除でない場合
		if (!aplGrpObjectList.isEmpty() && aplGrpObjectList.get(0).getModFlg()
				!= CusconConst.DEL_NUM) {

			log.debug("既に該当オブジェクト名が存在する。");
			// 重複エラーとしてレスポンスデータを作成し返却

			messages.add("message", new BLogicMessage("DK060060"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}


		/***********************************************************************
		 *  ⑤不要レコード削除処理
		 **********************************************************************/
		// DB検索結果が1件以上かつ、MOD_FLGのステータスが削除の場合
		if (!aplGrpObjectList.isEmpty() &&
				aplGrpObjectList.get(0).getModFlg() == CusconConst.DEL_NUM) {
			log.debug("不要レコードを削除する。");

			try {
				// SEQ_NOをキーに、UpdateDAOクラスを使用し、アプリケーショングループテーブルのレコードを削除
				updateDAO.execute("ApplicationGroupsBLogic-7", aplGrpObjectList.get(0)
						.getSeqNo());
			} catch(Exception e) {
				// トランザクションロールバック
	        	TransactionUtil.setRollbackOnly();
				// DBアクセスエラー
	        	Object[] objectgArray = {e.getMessage()};
	        	log.fatal(messageAccessor.getMessage(
	        			"FK061013", objectgArray, params.getUvo()));
	        	messages.add("message", new BLogicMessage("DK060027"));
				result.setErrors(messages);
				result.setResultString("failure");
	        	return result;
			}
		}


		/***********************************************************************
		 *  ⑥登録処理（アプリケーショングループ）
		 **********************************************************************/
		// INSERT用param作成（T_ApplicationFilters）
		ApplicationGroups insertParam = new ApplicationGroups();
		insertParam.setVsysId(vsysId);                            // Vsys-ID
		insertParam.setName(params.getName());                    // オブジェクト名
		insertParam.setGenerationNo(CusconConst.ZERO_GENE);       // 世代番号
		insertParam.setModFlg(CusconConst.ADD_NUM);               // 更新フラグ

		try {
			// INSERT実行（T_ApplicationGroups）
			updateDAO.execute("CommonT_ApplicationGroups-5", insertParam);

		} catch(Exception e) {
			// トランザクションロールバック
        	TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
        	Object[] objectgArray = {e.getMessage()};
        	log.fatal(messageAccessor.getMessage(
        			"FK061013", objectgArray, params.getUvo()));
        	messages.add("message", new BLogicMessage("DK060027"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}


		/***********************************************************************
		 *  ⑦登録処理（リンク情報）
		 **********************************************************************/
		int seqNo;

		try{
			// 上記で登録したアプリケーショングループのSEQ_NOを取得
			List<ApplicationGroups> insertAplGrpInfo = queryDAO.executeForObjectList(
					"ApplicationGroupsBLogic-2", searchApplicationGrpKey);
			seqNo = insertAplGrpInfo.get(0).getSeqNo();

		} catch(Exception e) {
			// トランザクションロールバック
        	TransactionUtil.setRollbackOnly();

			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061016", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060027"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
			// 画面から渡された選択中のアプリケーション情報を取得
			String[] aplList = params.getSelectAplList();
			String[] typeList = params.getSelectTypeList();

			for (int i = 0; i < aplList.length; i++) {
				log.debug("オブジェクト登録処理中:" + i);
				ApplicationGroupsLink applicationGroupsLink = new ApplicationGroupsLink();

				// 登録したアプリケーショングループのシーケンス番号をセット
				applicationGroupsLink.setAplGrpSeqNo(seqNo);

				try {
					// 画面指定のTypeがアプリケーションの場合
					if (typeList[i].equals(CusconConst.TYPE_APL)) {
						log.debug("オブジェクトタイプがアプリケーション");

						applicationGroupsLink.setObjectType(APL);

						// シーケンス番号取得
						List<ApplicationsMaster> aplMasterList = queryDAO.executeForObjectList(
								"ApplicationFiltersBLogic-19", aplList[i]);

						// 紐づくアプリケーションのシーケンス番号をセット
						applicationGroupsLink.setLinkSeqNo(aplMasterList.get(0).getSeqNo());
					}
					// 画面指定のTypeがアプリケーショングループの場合
					if (typeList[i].equals(CusconConst.TYPE_GRP)) {
						log.debug("オブジェクトタイプがグループ");

						applicationGroupsLink.setObjectType(GRP);

						// シーケンス番号取得
						ApplicationGroups search4Grp = new ApplicationGroups();
						search4Grp.setVsysId(vsysId);
						search4Grp.setGenerationNo(CusconConst.ZERO_GENE);
						search4Grp.setModFlg(CusconConst.DEL_NUM);
						search4Grp.setName(aplList[i]);

						List<ApplicationGroups> aplGrpList = queryDAO.executeForObjectList(
								"ApplicationGroupsBLogic-12", search4Grp);

						// 紐づくアプリケーションのシーケンス番号をセット
						applicationGroupsLink.setLinkSeqNo(aplGrpList.get(0).getSeqNo());
					}

					// 画面指定のTypeがアプリケーションフィルタの場合
					if (typeList[i].equals(CusconConst.TYPE_FLT)) {
						log.debug("オブジェクトタイプがフィルタ");

						applicationGroupsLink.setObjectType(FLT);

						// シーケンス番号取得
						ApplicationFilters search4Flt = new ApplicationFilters();
						search4Flt.setVsysId(vsysId);
						search4Flt.setGenerationNo(CusconConst.ZERO_GENE);
						search4Flt.setModFlg(CusconConst.DEL_NUM);
						search4Flt.setName(aplList[i]);

						List<ApplicationFilters> aplFltList = queryDAO.executeForObjectList(
								"ApplicationFiltersBLogic-20", search4Flt);

						// 紐づくアプリケーションのシーケンス番号をセット
						applicationGroupsLink.setLinkSeqNo(aplFltList.get(0).getSeqNo());
					}

					// INSERT処理
					updateDAO.execute("ApplicationGroupsBLogic-8", applicationGroupsLink);

				} catch(Exception e) {
					// トランザクションロールバック
		        	TransactionUtil.setRollbackOnly();
					// DBアクセスエラー
		        	Object[] objectgArray = {e.getMessage()};
		        	log.fatal(messageAccessor.getMessage(
		        			"FK061013", objectgArray, params.getUvo()));
		        	messages.add("message", new BLogicMessage("DK060027"));
					result.setErrors(messages);
					result.setResultString("failure");
		        	return result;
				}
		}


		/***********************************************************************
		 *  ⑧一覧情報検索共通処理
		 **********************************************************************/
		ApplicationGroupList applicationGroupList = new ApplicationGroupList(messageAccessor);
		ApplicationGroupsOutput output = new ApplicationGroupsOutput();

		// セッションクリア
		List<SelectedAplInfo> clearSession = new ArrayList<SelectedAplInfo>();
		clearSession = null;
		aplList = null;
		typeList = null;

		try {
			// outputにセット
			output.setApplicationGroupList(applicationGroupList.
					getApplicationGroupList(queryDAO, uvo));
		} catch(Exception e) {
			// トランザクションロールバック
        	TransactionUtil.setRollbackOnly();
        	// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("EK061004", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060027"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		output.setSessionAplList(clearSession);
		output.setDisplayType("");


		/***********************************************************************
		 *  ⑨レスポンス返却
		 **********************************************************************/
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("RegistApplicationGroupBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
