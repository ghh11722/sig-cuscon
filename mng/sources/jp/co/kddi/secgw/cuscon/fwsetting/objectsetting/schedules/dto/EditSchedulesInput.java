/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditSchedulesInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.vo.Schedules;

/**
 * クラス名 : EditSchedulesInput
 * 機能概要 :スケジュール編集画面入力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class EditSchedulesInput {

	// CusconUVO
	private CusconUVO uvo = null;

	// 選択スケジュールインデックス
	private int nameIndex = 0;

	// スケジュールリスト
	private List<Schedules> scheduleList = null;


	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}

	/**
	 * メソッド名 : indexのGetterメソッド
	 * 機能概要 : indexを取得する。
	 * @return index
	 */
	public int getNameIndex() {

		return nameIndex;
	}

	/**
	 * メソッド名 : indexのSetterメソッド
	 * 機能概要 : indexをセットする。
	 * @param index
	 */
	public void setNameIndex(int nameIndex) {
		this.nameIndex = nameIndex;
	}

	/**
	 * メソッド名 : scheduleListのGetterメソッド
	 * 機能概要 : scheduleListを取得する。
	 * @return scheduleList
	 */
	public List<Schedules> getScheduleList() {
		return scheduleList;
	}

	/**
	 * メソッド名 : scheduleListのSetterメソッド
	 * 機能概要 : scheduleListをセットする。
	 * @param scheduleList
	 */
	public void setScheduleList(List<Schedules> scheduleList) {
		this.scheduleList = scheduleList;
	}
}
