/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SearchInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     morisou                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;

/**
 * クラス名 : SearchInput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class SearchInput {

	// UVO
	private CusconUVO uvo = null;
	// アプリケーショングループ名
	private String name = null;
	// カテゴリリスト
	private Object[] categoryList;
	// サブカテゴリリスト
	private Object[] subCategoryList;
	// テクノロジリスト
	private Object[] technologyList;
	// リスクリスト
	private Object[] riskList;
    // 選択グループリスト
	private List<SelectedAplInfo> selectedAplList = null;
	// 画面遷移種別(登録/更新)
	private String displayType = null;
	// 検索ワード
	private String searchWord = null;
	// 選択アプリケーションリスト（JSPのhiddenから取得用）
	private String[] selectAplList = null;
	// 選択中アプリケーションタイプリスト（JSPのhiddenから取得用）
	private String[] selectTypeList = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}
	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : categoryListのGetterメソッド
	 * 機能概要 : categoryListを取得する。
	 * @return categoryList
	 */
	public Object[] getCategoryList() {
		return categoryList;
	}
	/**
	 * メソッド名 : categoryListのSetterメソッド
	 * 機能概要 : categoryListをセットする。
	 * @param categoryList
	 */
	public void setCategoryList(Object[] categoryList) {
		this.categoryList = categoryList;
	}
	/**
	 * メソッド名 : subCategoryListのGetterメソッド
	 * 機能概要 : subCategoryListを取得する。
	 * @return subCategoryList
	 */
	public Object[] getSubCategoryList() {
		return subCategoryList;
	}
	/**
	 * メソッド名 : subCategoryListのSetterメソッド
	 * 機能概要 : subCategoryListをセットする。
	 * @param subCategoryList
	 */
	public void setSubCategoryList(Object[] subCategoryList) {
		this.subCategoryList = subCategoryList;
	}
	/**
	 * メソッド名 : technologyListのGetterメソッド
	 * 機能概要 : technologyListを取得する。
	 * @return technologyList
	 */
	public Object[] getTechnologyList() {
		return technologyList;
	}
	/**
	 * メソッド名 : technologyListのSetterメソッド
	 * 機能概要 : technologyListをセットする。
	 * @param technologyList
	 */
	public void setTechnologyList(Object[] technologyList) {
		this.technologyList = technologyList;
	}
	/**
	 * メソッド名 : riskListのGetterメソッド
	 * 機能概要 : riskListを取得する。
	 * @return riskList
	 */
	public Object[] getRiskList() {
		return riskList;
	}
	/**
	 * メソッド名 : riskListのSetterメソッド
	 * 機能概要 : riskListをセットする。
	 * @param riskList
	 */
	public void setRiskList(Object[] riskList) {
		this.riskList = riskList;
	}
	/**
	 * メソッド名 : selectedAplListのGetterメソッド
	 * 機能概要 : selectedAplListを取得する。
	 * @return selectedAplList
	 */
	public List<SelectedAplInfo> getSelectedAplList() {
		return selectedAplList;
	}
	/**
	 * メソッド名 : selectedAplListのSetterメソッド
	 * 機能概要 : selectedAplListをセットする。
	 * @param selectedAplList
	 */
	public void setSelectedAplList(List<SelectedAplInfo> selectedAplList) {
		this.selectedAplList = selectedAplList;
	}
	/**
	 * メソッド名 : displayTypeのGetterメソッド
	 * 機能概要 : displayTypeを取得する。
	 * @return displayType
	 */
	public String getDisplayType() {
		return displayType;
	}
	/**
	 * メソッド名 : displayTypeのSetterメソッド
	 * 機能概要 : displayTypeをセットする。
	 * @param displayType
	 */
	public void setDisplayType(String displayType) {
		this.displayType = displayType;
	}
	/**
	 * メソッド名 : searchWordのGetterメソッド
	 * 機能概要 : searchWordを取得する。
	 * @return searchWord
	 */
	public String getSearchWord() {
		return searchWord;
	}
	/**
	 * メソッド名 : searchWordのSetterメソッド
	 * 機能概要 : searchWordをセットする。
	 * @param searchWord
	 */
	public void setSearchWord(String searchWord) {
		this.searchWord = searchWord;
	}
	/**
	 * メソッド名 : selectAplListのGetterメソッド
	 * 機能概要 : selectAplListを取得する。
	 * @return selectAplList
	 */
	public String[] getSelectAplList() {
		return selectAplList;
	}
	/**
	 * メソッド名 : selectAplListのSetterメソッド
	 * 機能概要 : selectAplListをセットする。
	 * @param selectAplList
	 */
	public void setSelectAplList(String[] selectAplList) {
		this.selectAplList = selectAplList;
	}
	/**
	 * メソッド名 : selectTypeListのGetterメソッド
	 * 機能概要 : selectTypeListを取得する。
	 * @return selectTypeList
	 */
	public String[] getSelectTypeList() {
		return selectTypeList;
	}
	/**
	 * メソッド名 : selectTypeListのSetterメソッド
	 * 機能概要 : selectTypeListをセットする。
	 * @param selectTypeList
	 */
	public void setSelectTypeList(String[] selectTypeList) {
		this.selectTypeList = selectTypeList;
	}
}
