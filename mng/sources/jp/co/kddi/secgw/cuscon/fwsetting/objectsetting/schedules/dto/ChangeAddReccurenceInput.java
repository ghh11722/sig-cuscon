/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ChangeAddReccurenceInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.dto;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;

/**
 * クラス名 : ChangeAddReccurenceInput
 * 機能概要 : 新規作成画面recurrence変更入力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class ChangeAddReccurenceInput {

	// CusconUVO
	private CusconUVO uvo = null;
	// オブジェクト名
	private String name = null;
	// 周期
	private int selectRecurrence = 0;


	/**
	 * メソッド名 : selectRecurrenceのGetterメソッド
	 * 機能概要 : selectRecurrenceを取得する。
	 * @return selectRecurrence
	 */
	public int getSelectRecurrence() {
		return selectRecurrence;
	}
	/**
	 * メソッド名 : selectRecurrenceのSetterメソッド
	 * 機能概要 : selectRecurrenceをセットする。
	 * @param selectRecurrence
	 */
	public void setSelectRecurrence(int selectRecurrence) {
		this.selectRecurrence = selectRecurrence;
	}
	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}
	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

}
