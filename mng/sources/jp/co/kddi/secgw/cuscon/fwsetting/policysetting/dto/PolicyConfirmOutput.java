/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : PolicyConfirmOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto;

import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;

/**
 * クラス名 : PolicyConfirmOutput
 * 機能概要 :ポリシーの削除選択時の出力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class PolicyConfirmOutput {


	// ポリシー一覧
	private SelectPolicyList selectPolicy = null;

	/**
	 * メソッド名 : selectPolicyのGetterメソッド
	 * 機能概要 : selectPolicyを取得する。
	 * @return selectPolicy
	 */
	public SelectPolicyList getSelectPolicy() {
		return selectPolicy;
	}

	/**
	 * メソッド名 : selectPolicyのSetterメソッド
	 * 機能概要 : selectPolicyをセットする。
	 * @param selectPolicy
	 */
	public void setSelectPolicy(SelectPolicyList selectPolicy) {
		this.selectPolicy = selectPolicy;
	}


}
