/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddressMasterList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.common;

import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectObjectList;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : AddressMasterList
 * 機能概要 : アドレスマスタの取得を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class AddressMasterList {

    private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.common.AddressMasterList");
    // メッセージクラス
	private static MessageAccessor messageAccessor = null;

    /**
     * メソッド名 : コンストラクタ
     * 機能概要 : -
     * @param queryDAO
     * @param uvo
     */
    public AddressMasterList() {

    	log.debug("コンストラクタ2処理開始");
		log.debug("コンストラクタ2処理終了");
    }



    /**
     * メソッド名 : getAddressMasterList
     * 機能概要 : アドレスオブジェクト、アドレスグループオブジェクト情報を取得する。
     * @param queryDAO
     * @param uvo
     * @param generationNo
     * @return
     * @throws Exception
     */
    public List<SelectObjectList> getAddressMasterList(
    			QueryDAO queryDAO, CusconUVO uvo, int generationNo) throws Exception {
    	log.debug("getAddressMasterList処理開始:vsysId = " +
    			uvo.getVsysId() + ", generationNo = " + generationNo);
    	// 検索条件を設定する。
    	InputBase base = new InputBase();
    	base.setVsysId(uvo.getVsysId());
    	base.setGenerationNo(generationNo);
    	base.setModFlg(CusconConst.DEL_NUM);
    	List<SelectObjectList> addressMasterList;

    	try {
    		addressMasterList =
    			queryDAO.executeForObjectList("AddressMasterList-1", base);

	    	log.debug("getAddressMasterList処理終了:addressMasterList.size() = " +
	    												addressMasterList.size());
    	} catch(Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		log.fatal(messageAccessor.getMessage("FZ011004", objectgArray, uvo));
    		throw e;
    	}
    	return addressMasterList;

    }
    /**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor msgAcc){
		log.debug("setMessageAccessor処理開始");
	    setMsgAcc(msgAcc);
	    log.debug("setMessageAccessor処理終了");
	}

	/**
	 * メソッド名 : setMsgAcc
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public static void setMsgAcc(MessageAccessor messageAccessor) {
		log.debug("setMsgAcc処理開始");
		AddressMasterList.messageAccessor = messageAccessor;
		log.debug("setMsgAcc処理終了");
	}

	/**
	 * メソッド名 : getMessage
	 * 機能概要 : messageAccessorをゲットする。
	 * @return MessageAccessor
	 */
	public static MessageAccessor getMessage() {
		log.debug("getMessage処理");
		return messageAccessor;
	}
}
