/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SeqAndName.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/24     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo;

import java.io.Serializable;

/**
 * クラス名 : SeqAndName
 * 機能概要 : シーケンス番号と名称の汎用データクラス
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/24
 *          新規作成
 * @see
 */
public class SeqAndNameInfo implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 4189435595632443846L;
	// シーケンス番号
	private int seqNo = 0;
	// 名称
	private String name = new String();
	// チェック情報（画面にチェックボックスON/OFFがある場合に使用 ON:true/OFF:false）
	private boolean chkFlg = false;
	// オブジェクトタイプ
	private int objectType = 0;

	/**
	 * メソッド名 : seqNoのGetterメソッド
	 * 機能概要 : seqNoを取得する。
	 * @return seqNo
	 */
	public int getSeqNo() {
		return seqNo;
	}
	/**
	 * メソッド名 : seqNoのSetterメソッド
	 * 機能概要 : seqNoをセットする。
	 * @param seqNo
	 */
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : chkFlgのSetterメソッド
	 * 機能概要 : chkFlgをセットする。
	 * @param chkFlg
	 */
	public void setChkFlg(boolean chkFlg) {
		this.chkFlg = chkFlg;
	}

	/**
	 * メソッド名 : chkFlgのGetterメソッド
	 * 機能概要 : chkFlgを取得する。
	 * @return chkFlg
	 */
	public boolean getChkFlg() {
		return chkFlg;
	}

	/**
	 * メソッド名 : objectTypeのGetterメソッド
	 * 機能概要 : objectTypeを取得する。
	 * @return objectType
	 */
	public int getObjectType() {
		return objectType;
	}
	/**
	 * メソッド名 : objectTypeのSetterメソッド
	 * 機能概要 : objectTypeをセットする。
	 * @param objectType
	 */
	public void setObjectType(int objectType) {
		this.objectType = objectType;
	}
	/**
	 * メソッド名 : serialversionuidのGetterメソッド
	 * 機能概要 : serialversionuidを取得する。
	 * @return serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
