/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddressGroupsForm.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.form;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.AddressesAndGroupsInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectAddressGroupList;
import jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx;

/**
 * クラス名 : AddressGroupsForm
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/01
 *          新規作成
 * @see
 */
public class AddressGroupsForm extends ValidatorActionFormEx {

	// シリアルバージョンID
	private static final long serialVersionUID = 1L;
	// アドレスグループ名
	private String name = null;
	// アドレスグループ名(旧)
	private String oldAddressGroupName = null;
	// アドレスグループ名(新)
	private String newAddressGroupName = null;
	// アドレスグループリスト
	private List<SelectAddressGroupList> addressGroupList = null;
	// アドレスグループ追加用アドレス＆アドレスグループ
	private List<AddressesAndGroupsInfo> addressAndAddressGrp4Add = null;
	// チェック選択インデックス
	private String checkIndex = null;
	// インデックス
	private int index = 0;

	private String message = null;

	public void reset(ActionMapping mapping, HttpServletRequest request){
		 message=null;
	}

	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : oldAddressGroupNameのSetterメソッド
	 * 機能概要 : oldAddressGroupNameをセットする。
	 * @param oldAddressGroupName
	 */
	public void setOldAddressGroupName(String oldAddressGroupName) {
		this.oldAddressGroupName = oldAddressGroupName;
	}
	/**
	 * メソッド名 : oldAddressGroupNameのGetterメソッド
	 * 機能概要 : oldAddressGroupNameを取得する。
	 * @return oldAddressGroupName
	 */
	public String getOldAddressGroupName() {
		return oldAddressGroupName;
	}
	/**
	 * メソッド名 : newAddressGroupNameのSetterメソッド
	 * 機能概要 : newAddressGroupNameをセットする。
	 * @param newAddressGroupName
	 */
	public void setNewAddressGroupName(String newAddressGroupName) {
		this.newAddressGroupName = newAddressGroupName;
	}
	/**
	 * メソッド名 : newAddressGroupNameのGetterメソッド
	 * 機能概要 : newAddressGroupNameを取得する。
	 * @return newAddressGroupName
	 */
	public String getNewAddressGroupName() {
		return newAddressGroupName;
	}
	/**
	 * メソッド名 : addressGroupListのGetterメソッド
	 * 機能概要 : addressGroupListを取得する。
	 * @return addressGroupList
	 */
	public List<SelectAddressGroupList> getAddressGroupList() {
		return addressGroupList;
	}
	/**
	 * メソッド名 : addressGroupListのSetterメソッド
	 * 機能概要 : addressGroupListをセットする。
	 * @param addressGroupList
	 */
	public void setAddressGroupList(List<SelectAddressGroupList> addressGroupList) {
		this.addressGroupList = addressGroupList;
	}
	/**
	 * メソッド名 : addressAndAddressGrp4AddのGetterメソッド
	 * 機能概要 : addressAndAddressGrp4Addを取得する。
	 * @return addressAndAddressGrp4Add
	 */
	public List<AddressesAndGroupsInfo> getAddressAndAddressGrp4Add() {
		return addressAndAddressGrp4Add;
	}
	/**
	 * メソッド名 : addressAndAddressGrp4AddのSetterメソッド
	 * 機能概要 : addressAndAddressGrp4Addをセットする。
	 * @param addressAndAddressGrp4Add
	 */
	public void setAddressAndAddressGrp4Add(List<AddressesAndGroupsInfo> addressAndAddressGrp4Add) {
		this.addressAndAddressGrp4Add = addressAndAddressGrp4Add;
	}
	/**
	 * メソッド名 : checkIndexのGetterメソッド
	 * 機能概要 : checkIndexを取得する。
	 * @return checkIndex
	 */
	public String getCheckIndex() {
		return checkIndex;
	}
	/**
	 * メソッド名 : checkIndexのSetterメソッド
	 * 機能概要 : checkIndexをセットする。
	 * @param checkIndex
	 */
	public void setCheckIndex(String checkIndex) {
		this.checkIndex = checkIndex;
	}
	/**
	 * メソッド名 : indexのGetterメソッド
	 * 機能概要 : indexを取得する。
	 * @return index
	 */
	public int getIndex() {
		return index;
	}
	/**
	 * メソッド名 : indexのSetterメソッド
	 * 機能概要 : indexをセットする。
	 * @param index
	 */
	public void setIndex(int index) {
		this.index = index;
	}
	/**
	 * メソッド名 : serialversionuidのGetterメソッド
	 * 機能概要 : serialversionuidを取得する。
	 * @return serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
