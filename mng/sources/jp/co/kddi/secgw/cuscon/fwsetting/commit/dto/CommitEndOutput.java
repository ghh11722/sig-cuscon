/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CommitEndOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/19     komakiys         初版作成
 * 2010/06/23     oohashij         StartTime、受付状態の追加
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.commit.dto;

/**
 * クラス名 : CommitEndOutput
 * 機能概要 : CommitEndOutputの出力クラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/19
 *          新規作成
 * @version 1.1 oohashij
 *          Created 2010/06/23
 *          StartTime、受付状態の追加
 * @see
 */
public class CommitEndOutput {

	// メッセージ
	private String message = null;

	// StartTime
	private String startTime = null;

	// 受付状態
	private String acceptStatus = null;

	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * メソッド名 : startTimeのGetterメソッド
	 * 機能概要 : startTimeを取得する。
	 * @return startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * メソッド名 : startTimeのSetterメソッド
	 * 機能概要 : startTimeをセットする。
	 * @param startTime
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * メソッド名 : acceptStatusのGetterメソッド
	 * 機能概要 : acceptStatusを取得する。
	 * @return acceptStatus
	 */
	public String getAcceptStatus() {
		return acceptStatus;
	}

	/**
	 * メソッド名 : acceptStatusのSetterメソッド
	 * 機能概要 : acceptStatusをセットする。
	 * @param acceptStatus
	 */
	public void setAcceptStatus(String acceptStatus) {
		this.acceptStatus = acceptStatus;
	}
}
