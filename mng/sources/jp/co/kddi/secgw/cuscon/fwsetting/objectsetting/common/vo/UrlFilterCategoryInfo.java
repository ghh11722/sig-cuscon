/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UrlFilterCategoryInfo.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/06     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo;

import java.io.Serializable;

/**
 * クラス名 : UrlFilterCategoryInfo
 * 機能概要 :
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/06
 *          新規作成
 * @see
 */
public class UrlFilterCategoryInfo implements Serializable{

	// シリアルバージョンID
	private static final long serialVersionUID = -4849189213521428194L;

	// シーケンス番号（Webフィルタカテゴリマスタ）
	private int seqNo = 0;
	// Name（Webフィルタカテゴリマスタ）
	private String name = null;
	// Action（Webフィルタカテゴリ）
	private String action = null;

	/**
	 * メソッド名 : seqNoのGetterメソッド
	 * 機能概要 : seqNoを取得する。
	 * @return seqNo
	 */
	public int getSeqNo() {
		return seqNo;
	}
	/**
	 * メソッド名 : seqNoのSetterメソッド
	 * 機能概要 : seqNoをセットする。
	 * @param seqNo
	 */
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : actionのGetterメソッド
	 * 機能概要 : actionを取得する。
	 * @return action
	 */
	public String getAction() {
		return action;
	}
	/**
	 * メソッド名 : actionのSetterメソッド
	 * 機能概要 : actionをセットする。
	 * @param action
	 */
	public void setAction(String action) {
		this.action = action;
	}
	/**
	 * メソッド名 : serialversionuidのGetterメソッド
	 * 機能概要 : serialversionuidを取得する。
	 * @return serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
