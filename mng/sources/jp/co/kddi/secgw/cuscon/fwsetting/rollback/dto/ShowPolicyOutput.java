/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ShowPolicyOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/04     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto;

/**
 * クラス名 : ShowPolicyOutput
 * 機能概要 :Name項目選択時の出力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/04
 *          新規作成
 * @see
 */
public class ShowPolicyOutput {

	// ルール名
	private String name = null;
	// 備考
	private String description = null;
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : descriptionのGetterメソッド
	 * 機能概要 : descriptionを取得する。
	 * @return description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * メソッド名 : descriptionのSetterメソッド
	 * 機能概要 : descriptionをセットする。
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}
