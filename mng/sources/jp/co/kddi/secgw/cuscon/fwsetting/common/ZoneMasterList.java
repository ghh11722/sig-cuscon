/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ZoneMasterList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.common;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

import org.apache.log4j.Logger;

/**
 * クラス名 : ZoneMasterList
 * 機能概要 :ゾーンマスタを取得する。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class ZoneMasterList {

    private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.common.ZoneMasterList");
    private static MessageAccessor messageAccessor = null;

    /**
     * メソッド名 : コンストラクタ
     * 機能概要 : -
     * @param queryDAO
     * @param uvo
     */
    public ZoneMasterList() {
    	log.debug("ZoneMasterListコンストラクタ2処理開始");
    	log.debug("ZoneMasterListコンストラクタ2処理終了");
    }

    /**
     * メソッド名 : getZoneMasterList
     * 機能概要 : ゾーンマスタの一覧を取得する。
     * @param vsysId Vsys-ID
     * @return zoneMasterList ゾーンマスタリスト
     * @throws Exception
     */
    public List<String> getZoneMasterList(QueryDAO queryDAO, CusconUVO uvo) throws Exception {
    	log.debug("getZoneMasterList処理開始:vsysId = " + uvo.getVsysId());
    	List<String> zoneMasterList = null;

    	// 20110616 ktakenaka@PROSITE add start
		// 基本検索条件クラスにパラメータを設定する。
		InputBase base = new InputBase();
        base.setVsysId(uvo.getVsysId());
        base.setDmzFlg(uvo.getDmzFlg());
    	// 20110616 ktakenaka@PROSITE add end

    	try {
	    	//zoneMasterList = queryDAO.executeForObjectList("ZoneMasterList-1", uvo.getVsysId()); 20110616 ktakenaka@PROSITE del
    		zoneMasterList = queryDAO.executeForObjectList("ZoneMasterList-1", base);	// 20110616 ktakenaka@PROSITE add
    	} catch(Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		log.fatal(messageAccessor.getMessage("FZ011032", objectgArray, uvo));
    		throw e;
    	}

    	log.debug("zoneMasterList処理終了:zoneMasterList() = " + zoneMasterList.size());
    	return zoneMasterList;

    }

    /**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor msgAcc){
		log.debug("setMessageAccessor処理開始");
	    setMsgAcc(msgAcc);
	    log.debug("setMessageAccessor処理終了");
	}

	/**
	 * メソッド名 : setMsgAcc
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public static void setMsgAcc(MessageAccessor messageAccessor) {
		log.debug("setMsgAcc処理開始");
		ZoneMasterList.messageAccessor = messageAccessor;
		log.debug("setMsgAcc処理終了");
	}

	/**
	 * メソッド名 : getMessage
	 * 機能概要 : messageAccessorをゲットする。
	 * @return MessageAccessor
	 */
	public static MessageAccessor getMessage() {
		log.debug("getMessage処理");
		return messageAccessor;
	}
}
