/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateServiceGroupInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/27     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.servicegroups.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.ServicesAndGroupsInfo;

/**
 * クラス名 : UpdateServiceGroupInput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/27
 *          新規作成
 * @see
 */
public class UpdateServiceGroupInput {

	// CusconUVO
    private CusconUVO uvo = null;
	// 変更前サービスグループ名
	private String oldServiceGroupName = null;
	// 変更後サービスグループ名
	private String newServiceGroupName = null;
	// サービスグループ追加用サービス＆サービスグループ
	private List<ServicesAndGroupsInfo> serviceAndServiceGrp4Add = null;
	// チェック選択インデックス
	private String checkIndex = null;
	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}
	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}
	/**
	 * メソッド名 : oldServiceGroupNameのGetterメソッド
	 * 機能概要 : oldServiceGroupNameを取得する。
	 * @return oldServiceGroupName
	 */
	public String getOldServiceGroupName() {
		return oldServiceGroupName;
	}
	/**
	 * メソッド名 : oldServiceGroupNameのSetterメソッド
	 * 機能概要 : oldServiceGroupNameをセットする。
	 * @param oldServiceGroupName
	 */
	public void setOldServiceGroupName(String oldServiceGroupName) {
		this.oldServiceGroupName = oldServiceGroupName;
	}
	/**
	 * メソッド名 : newServiceGroupNameのGetterメソッド
	 * 機能概要 : newServiceGroupNameを取得する。
	 * @return newServiceGroupName
	 */
	public String getNewServiceGroupName() {
		return newServiceGroupName;
	}
	/**
	 * メソッド名 : newServiceGroupNameのSetterメソッド
	 * 機能概要 : newServiceGroupNameをセットする。
	 * @param newServiceGroupName
	 */
	public void setNewServiceGroupName(String newServiceGroupName) {
		this.newServiceGroupName = newServiceGroupName;
	}
	/**
	 * メソッド名 : serviceAndServiceGrp4AddのGetterメソッド
	 * 機能概要 : serviceAndServiceGrp4Addを取得する。
	 * @return serviceAndServiceGrp4Add
	 */
	public List<ServicesAndGroupsInfo> getServiceAndServiceGrp4Add() {
		return serviceAndServiceGrp4Add;
	}
	/**
	 * メソッド名 : serviceAndServiceGrp4AddのSetterメソッド
	 * 機能概要 : serviceAndServiceGrp4Addをセットする。
	 * @param serviceAndServiceGrp4Add
	 */
	public void setServiceAndServiceGrp4Add(List<ServicesAndGroupsInfo> serviceAndServiceGrp4Add) {
		this.serviceAndServiceGrp4Add = serviceAndServiceGrp4Add;
	}
	/**
	 * メソッド名 : checkIndexのGetterメソッド
	 * 機能概要 : checkIndexを取得する。
	 * @return checkIndex
	 */
	public String getCheckIndex() {
		return checkIndex;
	}
	/**
	 * メソッド名 : checkIndexのSetterメソッド
	 * 機能概要 : checkIndexをセットする。
	 * @param checkIndex
	 */
	public void setCheckIndex(String checkIndex) {
		this.checkIndex = checkIndex;
	}
}
