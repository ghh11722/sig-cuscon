/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectServiceChkValidator.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/11     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.form;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.terasoluna.fw.web.struts.form.MultiFieldValidator;

/**
 * クラス名 : SelectServiceChkValidator
 * 機能概要 : ポリシー設定(アドレス編集画面)のチェックを行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/11
 *          新規作成
 * @see
 */
public class SelectAddressChkValidator implements MultiFieldValidator {

	/**
	 * メソッド名 :validate
	 * 機能概要 :ポリシー編集画面にて「選択する」を選択時に「オブジェクトの選択」<br>
	 *           を1つ以上選択しているかのチェックを行う。
	 * @param checkList 「オブジェクトの選択」の選択インデックス
	 * @param index ラジオボタンのインデックス
	 * @return チェック結果
	 * @see jp.terasoluna.fw.web.struts.form.MultiFieldValidator#validate(java.lang.String, java.lang.String[])
	 */
	@Override
	public boolean validate(String index, String[] fields) {

		// アドレス、グループ選択ラジオボタンを取得する。
		String checkList = fields[0];
		// 追加アドレス選択ラジオボタンを取得する。
		String addCheckList = fields[1];

		// 選択するの場合
		// チェックボックスが選択されていなければエラーとする。
		if(Integer.parseInt(index) == CusconConst.SELECT ) {
			if(checkList == null || checkList == "") {
				if(addCheckList == null || addCheckList == "") {

					return false;
				}
			}
		}

		return true;
	}
}
