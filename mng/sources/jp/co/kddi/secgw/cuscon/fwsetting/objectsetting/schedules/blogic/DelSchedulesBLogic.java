/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : DelSchedulesBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.blogic;

import java.util.List;
import org.apache.log4j.Logger;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.Schedules;
import jp.co.kddi.secgw.cuscon.fwsetting.common.ScheduleMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.ScheduleList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.dto.DelSchedulesInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.dto.ScheduleOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : DelSchedulesBLogic
 * 機能概要 :選択したオブジェクトの削除を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class DelSchedulesBLogic implements BLogic<DelSchedulesInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;

	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.blogic.DelSchedulesBLogic");
	private MessageAccessor messageAccessor = null;
	/**
	 * メソッド名 :execute
	 * 機能概要 :選択したオブジェクトの削除を行う。
	 * @param param DelSchedulesInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(DelSchedulesInput param) {

		log.debug("DelSchedulesBLogic処理開始");

		BLogicResult result = new BLogicResult();
		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
        result.setResultString("failure");

        List<Schedules> scheduleList = param.getScheduleList();

        // スケジュール共通クラスを生成する。
        ScheduleList schedule = new ScheduleList();

        // スケジュールマスタクラスを生成する。
        ScheduleMasterList scheduleMaster = new ScheduleMasterList();

		Schedules base = new Schedules();
		// 共通検索条件を設定する。
		base.setVsysId(param.getUvo().getVsysId());
		base.setGenerationNo(CusconConst.ZERO_GENE);
		base.setModFlg(CusconConst.DEL_NUM);

		int delSize = scheduleList.size();
		int delNum = 0;
		String delStr = "";
		// 削除選択オブジェクト分繰り返す。
		for(int i=0; i<scheduleList.size(); i++) {
			log.debug("オブジェクト削除処理中:" + i);
			base.setName(scheduleList.get(i).getName());
			List<Schedules> delList;
			try {
				delList = queryDAO.executeForObjectList("DelSchedulesBLogic-1", base);
			} catch(Exception e) {
				// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
				// DBアクセスエラー
	        	Object[] objectgArray = {e.getMessage()};
	        	log.fatal(messageAccessor.getMessage(
	        			"FK061026", objectgArray, param.getUvo()));
	        	messages.add("message", new BLogicMessage("DK060050"));
				result.setErrors(messages);
				result.setResultString("failure");
	        	return result;
			}

			// 取得したシーケンス番号を検索条件に設定する。
			base.setSeqNo(delList.get(0).getSeqNo());
			int count;
			try {
				// スケジュールオブジェクトが使用されている件数を取得する。
				count = queryDAO.executeForObject(
						"DelSchedulesBLogic-2", base, Integer.class);
			} catch(Exception e) {
				// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
				// DBアクセスエラー
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FK061028", objectgArray, param.getUvo()));
				messages.add("message", new BLogicMessage("DK060050"));
				result.setErrors(messages);
				result.setResultString("failure");
	        	return result;
			}

			// 削除不可
			if(count != 0) {
				log.debug("オブジェクト削除不可:" + scheduleList.get(i).getName());
				delNum++;

				// 削除名称を連結する。
				if(delNum == 1) {
					delStr = scheduleList.get(i).getName();
				} else {
					delStr = delStr + ", " + scheduleList.get(i).getName();
				}
				if(delSize == delNum) {
					// 削除不可エラーメッセージを出力する。
					Object[] objectgArray = {delStr};
					log.debug(messageAccessor.getMessage(
		        			"DK060002", objectgArray, param.getUvo()));
					messages.add("message",new BLogicMessage("DK060070", objectgArray));
					result.setErrors(messages);
					result.setResultString("failure");
		        	return result;
				}
	        // 削除可能
			} else {

				log.debug("オブジェクト削除可能:" + scheduleList.get(i).getName());
				try {
					// recurrenceがDAILYの場合
					if(scheduleList.get(i).getRecurrence() == CusconConst.DAILY) {
						log.debug("DAILY削除");
						// DAILYの情報を削除する。
						schedule.delDaiySchedule(updateDAO, param.getUvo(), base);

					// recurrenceがWEEKLYの場合
					} else if(scheduleList.get(i).getRecurrence() ==
															CusconConst.WEEKLY) {
						log.debug("WEEKLY削除");
						// WEEKLYの情報を削除する。
						schedule.delWeeklySchedule(updateDAO, param.getUvo(), base);

					// recurrenceがNONRECURRINGの場合
					} else {
						log.debug("NONRECURRING削除");
						// NONRECURRINGの情報を削除する。
						schedule.delNonSchedule(updateDAO, param.getUvo(), base);
					}
				} catch(Exception e) {
					// 例外
					Object[] objectgArray = {e.getMessage()};
					log.error(messageAccessor.getMessage("EK061007", objectgArray, param.getUvo()));
					messages.add("message", new BLogicMessage("DK060050"));
					result.setErrors(messages);
					result.setResultString("failure");
					return result;
				}
				try {
					// 変更フラグを削除に更新する。
					updateDAO.execute("DelSchedulesBLogic-3", base);
				} catch(Exception e) {
					// トランザクションロールバック
					TransactionUtil.setRollbackOnly();
					// DBアクセスエラー
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage("FK061027", objectgArray, param.getUvo()));
					messages.add("message", new BLogicMessage("DK060050"));
					result.setErrors(messages);
					result.setResultString("failure");
		        	return result;
				}
			}

		}

		// スケジュール一覧
		List<Schedules> sheduleList = null;
		try {
			// スケジュール一覧取得処理を行う。
			sheduleList = scheduleMaster.getScheduleMasterList(
						queryDAO, param.getUvo(), 0, CusconConst.ZERO_GENE);
		} catch(Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("EK061007", objectgArray, param.getUvo()));
			messages.add("message", new BLogicMessage("DK060050"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

		// アウトプットデータクラスに設定する。
		ScheduleOutput out = new ScheduleOutput();
	    out.setScheduleList(sheduleList);

	    if(delNum != 0) {
	    	// 削除不可オブジェクトの名称出力
	    	log.debug("削除不可オブジェクト名称:" + delStr);
			Object[] objectgArray = {delStr};
			out.setMessage(messageAccessor.getMessage("DK060070", objectgArray));
	    }
	    result.setResultObject(out);
	    result.setResultString("success");

	    log.debug("DelSchedulesBLogic処理終了");
		return result;

	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("queryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("queryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("queryDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("queryDAO処理終了");
	}


	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}

}
