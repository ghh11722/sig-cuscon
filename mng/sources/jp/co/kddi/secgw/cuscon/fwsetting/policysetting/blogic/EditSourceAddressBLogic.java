/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditSourceAddressBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/03     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic;

import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.common.AddressMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectObjectList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.EditAddressOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.PolicyInput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;

/**
 * クラス名 : EditSourceAddressBLogic
 * 機能概要 :SourceAddress項目選択時の処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/03
 *          新規作成
 * @see
 */
public class EditSourceAddressBLogic implements BLogic<PolicyInput> {

	/**
     * QueryDAO。
     * Springによりインスタンス生成され設定される。
     */
    private QueryDAO queryDAO = null;

    private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic.EditSourceAddressBLogic");
    private MessageAccessor messageAccessor = null;
    /**
	 * メソッド名 :execute
	 * 機能概要 : アドレスマスタ、選択アドレスリストを取得する。
	 * @param param ポリシー画面入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(PolicyInput param) {

		log.debug("EditSourceAddressBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");
        // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();

        // 選択したリストからオブジェクトを取得する。
        SelectPolicyList selectPolicy =
        	param.getPolicyList().get(param.getIndex());

        AddressMasterList addressList = new AddressMasterList();
        // アドレスマスタリスト。
        List<SelectObjectList> addressMasterList = null;

        try {
	        // アドレスマスタリストを取得する。
	        addressMasterList =  addressList.getAddressMasterList(
	        			queryDAO, param.getUvo(), CusconConst.ZERO_GENE);
        } catch(Exception e) {
			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"EK051001", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK050016"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

        // ビジネスロジックの出力クラスに結果を設定する
        EditAddressOutput out = new EditAddressOutput();
        out.setName(selectPolicy.getName());
        out.setAddressList(selectPolicy.getSourceAddressList());
        out.setAddressMasterList(addressMasterList);
        out.setAddressName(out.getAddressList().get(0).getName());

        result.setResultObject(out);
        result.setResultString("success");

        log.debug("EditSourceAddressBLogic処理終了");
        return result;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}
	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
