/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ChoiceGroupsPolicyBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic;

import java.util.ArrayList;
import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.ApplicationGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.ChoiceGroupsPolicyInput;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.ChoiceGroupsPolicyOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import org.apache.log4j.Logger;

/**
 * クラス名 : ChoiceGroupsPolicyBLogic
 * 機能概要 : グループ設定画面表示処理を行う。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/01
 *          新規作成
 * @see
 */
public class ChoiceGroupsPolicyBLogic implements BLogic<ChoiceGroupsPolicyInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups." +
			"blogic.ChoiceGroupsPolicyBLogic");

	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 : グループ設定画面表示処理を行う。
	 * @param param ChoiceGroupsPolicyInput入力データクラス
	 * @return result BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(ChoiceGroupsPolicyInput params) {
		log.debug("ChoiceGroupsPolicyBLogic処理開始");
		List<SelectApplicationGroupList> list4Out = null;   // アプリケーショングループ情報のリスト

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();

		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = params.getUvo();

		/***********************************************************************
		 *  ①画面から受け取った「選択中のアプリケーション」リストからグループ情報だけ取得
		 **********************************************************************/
		// 「選択中のアプリケーション」リスト
		//List<SelectedAplInfo> selectedAplInfoList = params.getSelectedAplList();

		// 選択中アプリケーション名リスト
		String[] selectAplList = params.getSelectAplList();
		// 選択中アプリケーションタイプリスト
		String[] selectTypeList = params.getSelectTypeList();

		// 編集用tmpグループリスト
		ArrayList<String> tmpGrpList = new ArrayList<String>();

		// 「選択中のアプリケーション」リストの件数分、以下の処理を実行
		if (!selectAplList[0].equals("null") || selectAplList.length != 1) {
			log.debug("「選択中のアプリケーション」リストがnullでない場合");

			for (int i = 0; i < selectAplList.length; i++) {
				log.debug("「選択中のアプリケーション」処理中" + i);

				// タイプがグループの場合、それをリストに格納
				if (selectTypeList[i].equals(CusconConst.TYPE_GRP)) {
					log.debug("タイプがグループの場合、リストに格納");

					tmpGrpList.add(selectAplList[i]);
				}
			}
		}


		/***********************************************************************
		 *  ②DBからアプリケーショングループ情報を取得
		 **********************************************************************/
		ApplicationGroupList applicationGroupList
									= new ApplicationGroupList(messageAccessor);
		List<SelectApplicationGroupList> dbGrpList = null;
		try {
			dbGrpList = applicationGroupList.getApplicationGroupList(queryDAO, uvo);
		} catch (Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("EK051001", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK050035"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}


		/***********************************************************************
		 *  ③マッチング処理
		 **********************************************************************/
		list4Out = new ArrayList<SelectApplicationGroupList>();

		if (dbGrpList != null) {
			log.debug("DBから取得したグループ情報がnullでない場合");

			// ①で得たリストと②で得たリストのアプリケーショングループ名が一致する場合、チェックON対象とみなす。
			for (int i = 0; i < dbGrpList.size(); i++) {
				log.debug("DBから取得したグループ情報分処理中:" + i);

				SelectApplicationGroupList selectApplicationGroupList =
					new SelectApplicationGroupList();

				if(tmpGrpList.size() != 0) {
					log.debug("選択アプリあり");
					for (int j = 0; j < tmpGrpList.size(); j++) {
						log.debug("tmpGrpList処理中" + j);
						// チェックON/OFF判別
						if (tmpGrpList.get(j).equals(dbGrpList.get(i).getName())) {
							selectApplicationGroupList.setChkFlg(true);
						}
					}
				} else {
					log.debug("選択アプリ0件のためチェックボックス初期化");
					selectApplicationGroupList.setChkFlg(false);
				}
				selectApplicationGroupList.setName(dbGrpList.get(i).getName());
				selectApplicationGroupList.setMembers(dbGrpList.get(i).getMembers());
				selectApplicationGroupList.setApplications(dbGrpList.get(i).
						getApplications());
				list4Out.add(selectApplicationGroupList);

			}
		}


		/***********************************************************************
		 *  ④セッション設定処理
		 **********************************************************************/
		// 選択中アプリケーション情報をセッション情報に格納
		List<SelectedAplInfo> sessionAplList = new ArrayList<SelectedAplInfo>();
		if (!selectAplList[0].equals("null") || selectAplList.length != 1) {
			log.debug("「選択中のアプリケーション」リストがnullでない場合");

			for (int i = 0; i < selectAplList.length; i++) {
				log.debug("「選択中のアプリケーション」処理中" + i);

				SelectedAplInfo aplInfo = new SelectedAplInfo();

				aplInfo.setName(selectAplList[i]);
				aplInfo.setType(selectTypeList[i]);

				sessionAplList.add(aplInfo);
			}
		} else {
			log.debug("「選択中のアプリケーション」がnullの場合");
			sessionAplList = null;
		}


		/***********************************************************************
		 *  ⑤レスポンス返却
		 **********************************************************************/
		ChoiceGroupsPolicyOutput output = new ChoiceGroupsPolicyOutput();
		output.setGroupList4Add(list4Out);
		output.setSessionAplList(sessionAplList);
		output.setName(params.getName());
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("ChoiceGroupsPolicyBLogic処理終了");
		return result;
	}


	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}
	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
