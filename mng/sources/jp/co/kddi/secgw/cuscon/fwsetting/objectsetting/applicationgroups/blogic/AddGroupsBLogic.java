/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddGroupsBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.blogic;

import java.util.ArrayList;
import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.common.ApplicationList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.FilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto.AddGroupsInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto.AddGroupsOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationList4Search;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectFilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import org.apache.log4j.Logger;

/**
 * クラス名 : AddGroupsBLogic
 * 機能概要 : グループ情報を選択中アプリケーション情報に追加する。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class AddGroupsBLogic implements  BLogic<AddGroupsInput> {

	// 画面遷移判別定数
	private static final String REGIST = "登録";         // 登録

	// QueryDAO
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups." +
	"blogic.AddGroupsBLogic");

	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 : グループ情報を選択中アプリケーション情報に追加する。
	 * @param params AddGroupsInput入力データクラス
	 * @return result BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(AddGroupsInput params) {

		log.debug("AddGroupsBLogic処理開始");

		String failString;
		SelectFilterMasterList resultFltList = null;                // フィルタマスタリスト
		List<SelectApplicationList4Search> resultAplList = null;    // アプリケーションマスタリスト

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();

		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();

		if (params.getDisplayType().equals(REGIST)) {
			log.debug("遷移先が登録系画面");
			failString = "failure4Regist";
			result.setResultString(failString);
		} else {
			log.debug("遷移先が更新系画面");
			failString = "failure4Update";
			result.setResultString(failString);
		}

		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = params.getUvo();

		String[] indexList = params.getIndex().split(",");

		// 選択チェック分繰り返す。
		List<SelectApplicationGroupList> groupList4Add =
			new ArrayList<SelectApplicationGroupList>();
		if(!indexList[0].equals("") || indexList.length != 1) {
			log.debug("チェックあり");
			for(int i=0; i< indexList.length; i++) {
				log.debug("選択チェック分グループ追加用リストに設定中:" + i);
				groupList4Add.add(params.getGroupList4Add().get(Integer.
						parseInt(indexList[i])));
			}
		}

		try {
			/***********************************************************************
			 *  ①フィルタリスト取得（マスタ情報）
			 **********************************************************************/
			FilterMasterList filterMasterList = new FilterMasterList();
			resultFltList = filterMasterList.getFilterMasterList(queryDAO, uvo);


			/***********************************************************************
			 *  ②アプリケーションリスト取得（マスタ情報）
			 **********************************************************************/
			ApplicationList applicationList = new ApplicationList();
			resultAplList = applicationList.getApplicationList(queryDAO, uvo);

		} catch(Exception e) {
			// 例外
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("EK061004", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060066"));
			result.setErrors(messages);
			result.setResultString(failString);

			return result;
		}


		/***********************************************************************
		 *  ③「選択中のアプリケーション」リストをマージ
		 **********************************************************************/
		List<SelectedAplInfo> resultSelectedAplList = new ArrayList<SelectedAplInfo>();
		// セッション情報取得
		List<SelectedAplInfo> sessionAplList = params.getSessionAplList();

		// セッション情報の件数分、以下の処理を行う
		if (sessionAplList != null) {
			log.debug("「選択中のアプリケーション」がnullでない場合");
			// セッション情報の件数分、以下の処理を行う
			for (int i = 0; i < sessionAplList.size(); i++) {
				log.debug("選択中アプリケーションリストマージ中:" + i);
				// Typeがグループでない場合
				if (!sessionAplList.get(i).getType().equals(CusconConst.TYPE_GRP)) {
					log.debug("グループ以外のアプリケーションを追加");
					SelectedAplInfo selectedAplInfo = new SelectedAplInfo();
					selectedAplInfo.setSeqNo(sessionAplList.get(i).getSeqNo());
					selectedAplInfo.setName(sessionAplList.get(i).getName());
					selectedAplInfo.setType(sessionAplList.get(i).getType());

					// 選択中アプリケーション情報に追加
					resultSelectedAplList.add(selectedAplInfo);
				}
			}
		}

		// グループ情報を選択中アプリケーション情報に追加
		log.debug("画面で選択されたグループを追加");
		for (int i = 0; i < groupList4Add.size(); i++) {
			log.debug("選択中アプリケーションリストマージ中:" + i);
			SelectedAplInfo selectedGrpInfo = new SelectedAplInfo();
			selectedGrpInfo.setName(groupList4Add.get(i).getName());
			selectedGrpInfo.setType(CusconConst.TYPE_GRP);
			resultSelectedAplList.add(selectedGrpInfo);
		}


		/***********************************************************************
		 *  ④結果セット
		 **********************************************************************/
		AddGroupsOutput out = new AddGroupsOutput();
		out.setName(params.getName());
		out.setFilterList(resultFltList);
		out.setApplicationList(resultAplList);
		out.setSelectedAplList(resultSelectedAplList);
		out.setSearchWord("");

		result.setResultObject(out);
		// 遷移先判別（新規登録画面から遷移してきた場合）
		if (params.getDisplayType().equals(REGIST)) {
			log.debug("遷移先が登録系画面");
			result.setResultString("success4Regist");
		} else {
			result.setResultString("success4Update");
			log.debug("遷移先が更新系画面");
		}

		log.debug("AddGroupsBLogic処理終了");
		return result;
	}
	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
