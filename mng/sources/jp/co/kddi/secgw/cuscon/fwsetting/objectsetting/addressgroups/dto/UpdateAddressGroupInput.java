/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateAddressGroupInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/27     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addressgroups.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.AddressesAndGroupsInfo;

/**
 * クラス名 : UpdateAddressGroupInput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/27
 *          新規作成
 * @see
 */
public class UpdateAddressGroupInput {

	// UVO
	private CusconUVO uvo = null;
	// 変更前アドレスグループ名
	private String oldAddressGroupName = null;
	// 変更後アドレスグループ名
	private String newAddressGroupName = null;
	// 選択アドレス＆アドレスグループ情報
	private List<AddressesAndGroupsInfo> addressAndAddressGrp4Add = null;
	// チェック選択インデックス
	private String checkIndex = null;
	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}
	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}
	/**
	 * メソッド名 : oldAddressGroupNameのGetterメソッド
	 * 機能概要 : oldAddressGroupNameを取得する。
	 * @return oldAddressGroupName
	 */
	public String getOldAddressGroupName() {
		return oldAddressGroupName;
	}
	/**
	 * メソッド名 : oldAddressGroupNameのSetterメソッド
	 * 機能概要 : oldAddressGroupNameをセットする。
	 * @param oldAddressGroupName
	 */
	public void setOldAddressGroupName(String oldAddressGroupName) {
		this.oldAddressGroupName = oldAddressGroupName;
	}
	/**
	 * メソッド名 : newAddressGroupNameのGetterメソッド
	 * 機能概要 : newAddressGroupNameを取得する。
	 * @return newAddressGroupName
	 */
	public String getNewAddressGroupName() {
		return newAddressGroupName;
	}
	/**
	 * メソッド名 : newAddressGroupNameのSetterメソッド
	 * 機能概要 : newAddressGroupNameをセットする。
	 * @param newAddressGroupName
	 */
	public void setNewAddressGroupName(String newAddressGroupName) {
		this.newAddressGroupName = newAddressGroupName;
	}
	/**
	 * メソッド名 : selectAddressListのGetterメソッド
	 * 機能概要 : selectAddressListを取得する。
	 * @return selectAddressList
	 */
	public List<AddressesAndGroupsInfo> getAddressAndAddressGrp4Add() {
		return addressAndAddressGrp4Add;
	}
	/**
	 * メソッド名 : selectAddressListのSetterメソッド
	 * 機能概要 : selectAddressListをセットする。
	 * @param selectAddressList
	 */
	public void setAddressAndAddressGrp4Add(List<AddressesAndGroupsInfo> addressAndAddressGrp4Add) {
		this.addressAndAddressGrp4Add = addressAndAddressGrp4Add;
	}
	/**
	 * メソッド名 : checkIndexのGetterメソッド
	 * 機能概要 : checkIndexを取得する。
	 * @return checkIndex
	 */
	public String getCheckIndex() {
		return checkIndex;
	}
	/**
	 * メソッド名 : checkIndexのSetterメソッド
	 * 機能概要 : checkIndexをセットする。
	 * @param checkIndex
	 */
	public void setCheckIndex(String checkIndex) {
		this.checkIndex = checkIndex;
	}
}
