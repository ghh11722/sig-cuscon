/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : DelApplicationGroupsInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/23     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationGroupList;

/**
 * クラス名 : DelApplicationGroupsInput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/23
 *          新規作成
 * @see
 */
public class DelApplicationGroupsInput {

    private CusconUVO uvo = null;
	// アプリケーショングループリスト
	private List<SelectApplicationGroupList> applicationGroupList = null;
	// 削除対象リスト（JSPのhiddenから取得用）
	private String[] selectAplList = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}

	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}

	/**
	 * メソッド名 : applicationGroupListのGetterメソッド
	 * 機能概要 : applicationGroupListを取得する。
	 * @return applicationGroupList
	 */
	public List<SelectApplicationGroupList> getApplicationGroupList() {
		return applicationGroupList;
	}

	/**
	 * メソッド名 : applicationGroupListのSetterメソッド
	 * 機能概要 : applicationGroupListをセットする。
	 * @param applicationGroupList
	 */
	public void setApplicationGroupList(List<SelectApplicationGroupList> applicationGroupList) {
		System.out.println(applicationGroupList);
		this.applicationGroupList = applicationGroupList;
	}

	/**
	 * メソッド名 : selectAplListのGetterメソッド
	 * 機能概要 : selectAplListを取得する。
	 * @return selectAplList
	 */
	public String[] getSelectAplList() {
		return selectAplList;
	}

	/**
	 * メソッド名 : selectAplListのSetterメソッド
	 * 機能概要 : selectAplListをセットする。
	 * @param selectAplList
	 */
	public void setSelectAplList(String[] selectAplList) {
		this.selectAplList = selectAplList;
	}
}
