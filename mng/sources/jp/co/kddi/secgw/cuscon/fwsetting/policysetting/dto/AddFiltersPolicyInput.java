/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddFiltersInput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto;

import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;

/**
 * クラス名 : AddFiltersPolicyInput
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class AddFiltersPolicyInput {

	// UVO
	private CusconUVO uvo = null;
	// ポリシー名
	private String name = null;
	// セッション保持しているSelectedList(セッションデータ)
	private List<SelectedAplInfo> sessionAplList = null;
    // 選択フィルタリスト
	private List<SelectedAplInfo> selectedAplList = null;
	// チェック選択アプリケーショングループインデックス
	private String filterIndex = null;
	// アプリケーションフィルタリスト
	private List<SelectApplicationFilterList> filterList4Add = null;
	// 選択中アプリケーション名リスト（JSPのhiddenから取得用）
	private String[] selectAplList = null;
	// 選択中アプリケーションタイプリスト（JSPのhiddenから取得用）
	private String[] selectTypeList = null;

	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}
	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : sessionAplListのGetterメソッド
	 * 機能概要 : sessionAplListを取得する。
	 * @return sessionAplList
	 */
	public List<SelectedAplInfo> getSessionAplList() {
		return sessionAplList;
	}
	/**
	 * メソッド名 : sessionAplListのSetterメソッド
	 * 機能概要 : sessionAplListをセットする。
	 * @param sessionAplList
	 */
	public void setSessionAplList(List<SelectedAplInfo> sessionAplList) {
		this.sessionAplList = sessionAplList;
	}
	/**
	 * メソッド名 : selectedAplListのGetterメソッド
	 * 機能概要 : selectedAplListを取得する。
	 * @return selectedAplList
	 */
	public List<SelectedAplInfo> getSelectedAplList() {
		return selectedAplList;
	}
	/**
	 * メソッド名 : selectedAplListのSetterメソッド
	 * 機能概要 : selectedAplListをセットする。
	 * @param selectedAplList
	 */
	public void setSelectedAplList(List<SelectedAplInfo> selectedAplList) {
		this.selectedAplList = selectedAplList;
	}
	/**
	 * メソッド名 : filterList4AddのGetterメソッド
	 * 機能概要 : filterList4Addを取得する。
	 * @return filterList4Add
	 */
	public List<SelectApplicationFilterList> getFilterList4Add() {
		return filterList4Add;
	}
	/**
	 * メソッド名 : filterList4AddのSetterメソッド
	 * 機能概要 : filterList4Addをセットする。
	 * @param filterList4Add
	 */
	public void setFilterList4Add(List<SelectApplicationFilterList> filterList4Add) {
		this.filterList4Add = filterList4Add;
	}
	/**
	 * メソッド名 : filterIndexのGetterメソッド
	 * 機能概要 : filterIndexを取得する。
	 * @return filterIndex
	 */
	public String getFilterIndex() {
		return filterIndex;
	}
	/**
	 * メソッド名 : filterIndexのSetterメソッド
	 * 機能概要 : filterIndexをセットする。
	 * @param filterIndex
	 */
	public void setFilterIndex(String filterIndex) {
		this.filterIndex = filterIndex;
	}
	/**
	 * メソッド名 : selectAplListのGetterメソッド
	 * 機能概要 : selectAplListを取得する。
	 * @return selectAplList
	 */
	public String[] getSelectAplList() {
		return selectAplList;
	}
	/**
	 * メソッド名 : selectAplListのSetterメソッド
	 * 機能概要 : selectAplListをセットする。
	 * @param selectAplList
	 */
	public void setSelectAplList(String[] selectAplList) {
		this.selectAplList = selectAplList;
	}
	/**
	 * メソッド名 : selectTypeListのGetterメソッド
	 * 機能概要 : selectTypeListを取得する。
	 * @return selectTypeList
	 */
	public String[] getSelectTypeList() {
		return selectTypeList;
	}
	/**
	 * メソッド名 : selectTypeListのSetterメソッド
	 * 機能概要 : selectTypeListをセットする。
	 * @param selectTypeList
	 */
	public void setSelectTypeList(String[] selectTypeList) {
		this.selectTypeList = selectTypeList;
	}
}
