/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddressesForm.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/27     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.form;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectAddressList;
import jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx;

/**
 * クラス名 : AddressesForm
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/27
 *          新規作成
 * @see
 */
public class AddressesForm extends ValidatorActionFormEx {

	// シリアルバージョンID
	private static final long serialVersionUID = 1L;
	// アドレス情報リスト
	private List<SelectAddressList> addressList = null;
	// アドレス名
	private String name = null;
	// アドレス名(新)
	private String newAddressName = null;
	// アドレス名(旧)
	private String oldAddressName = null;
	// タイプ
	private String type = null;
	// アドレス
	private String address = null;
	// 選択されたインデックス
	private String checkIndex = null;
	// インデックス
	private int index = 0;
	private String message = null;

	public void reset(ActionMapping mapping, HttpServletRequest request){
		 message=null;
	}
	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * メソッド名 : addressListのGetterメソッド
	 * 機能概要 : addressListを取得する。
	 * @return addressList
	 */
	public List<SelectAddressList> getAddressList() {
		return addressList;
	}

	/**
	 * メソッド名 : addressListのSetterメソッド
	 * 機能概要 : addressListをセットする。
	 * @param addressList
	 */
	public void setAddressList(List<SelectAddressList> addressList) {
		this.addressList = addressList;
	}

	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * メソッド名 : oldAddressNameのSetterメソッド
	 * 機能概要 : oldAddressNameをセットする。
	 * @param oldAddressName
	 */
	public void setOldAddressName(String oldAddressName) {
		this.oldAddressName = oldAddressName;
	}

	/**
	 * メソッド名 : oldAddressNameのGetterメソッド
	 * 機能概要 : oldAddressNameを取得する。
	 * @return oldAddressName
	 */
	public String getOldAddressName() {
		return oldAddressName;
	}

	/**
	 * メソッド名 : newAddressNameのGetterメソッド
	 * 機能概要 : newAddressNameを取得する。
	 * @return newAddressName
	 */
	public String getNewAddressName() {
		return newAddressName;
	}

	/**
	 * メソッド名 : newAddressNameのSetterメソッド
	 * 機能概要 : newAddressNameをセットする。
	 * @param newAddressName
	 */
	public void setNewAddressName(String newAddressName) {
		this.newAddressName = newAddressName;
	}

	/**
	 * メソッド名 : typeのGetterメソッド
	 * 機能概要 : typeを取得する。
	 * @return type
	 */
	public String getType() {
		return type;
	}
	/**
	 * メソッド名 : typeのSetterメソッド
	 * 機能概要 : typeをセットする。
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * メソッド名 : addressのGetterメソッド
	 * 機能概要 : addressを取得する。
	 * @return address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * メソッド名 : addressのSetterメソッド
	 * 機能概要 : addressをセットする。
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * メソッド名 : 選択されたindexのGetterメソッド
	 * 機能概要 : 選択されたindexを取得する。
	 * @return checkIndex
	 */
	public String getCheckIndex() {
		return checkIndex;
	}
	/**
	 * メソッド名 : 選択されたindexのSetterメソッド
	 * 機能概要 : 選択されたindexをセットする。
	 * @param checkIndex
	 */
	public void setCheckIndex(String checkIndex) {
		this.checkIndex = checkIndex;
	}

	/**
	 * メソッド名 : indexのGetterメソッド
	 * 機能概要 : indexを取得する。
	 * @return index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * メソッド名 : indexのSetterメソッド
	 * 機能概要 : indexをセットする。
	 * @param index
	 */
	public void setIndex(int index) {
		this.index = index;
	}

}
