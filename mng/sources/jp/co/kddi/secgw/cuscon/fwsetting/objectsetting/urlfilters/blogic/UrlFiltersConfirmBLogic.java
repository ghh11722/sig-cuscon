/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UrlFiltersConfirmBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/10     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.blogic;

import java.util.ArrayList;
import java.util.List;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectUrlFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.dto.UrlFiltersConfirmInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.dto.UrlFiltersOutput;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicResult;

import org.apache.log4j.Logger;

/**
 * クラス名 : UrlFiltersConfirmBLogic
 * 機能概要 :
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/10
 *          新規作成
 * @see
 */
public class UrlFiltersConfirmBLogic implements BLogic<UrlFiltersConfirmInput> {

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.services.blogic.UrlFiltersConfirmBLogic");

	/**
	 * メソッド名 :execute
	 * 機能概要 :Webフィルタ削除確認対象表示処理を行う。
	 * @param param UrlFiltersConfirmInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(UrlFiltersConfirmInput param) {

		log.debug("UrlFiltersConfirmBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");

		// アウトプットデータクラスに設定する。
        UrlFiltersOutput out = new UrlFiltersOutput();
        List<SelectUrlFilterList> urlFilterList = new ArrayList<SelectUrlFilterList>();

        String[] indexList = param.getCheckIndex().split(",");

        // 選択チェック分繰り返す。
        for(int i=0; i< indexList.length; i++) {
        	log.debug("選択チェック:" + i);
        	urlFilterList.add(param.getUrlFilterList().get(Integer.parseInt(indexList[i])));
        }

	    out.setUrlFilterList(urlFilterList);

	    result.setResultObject(out);
	    result.setResultString("success");

	    log.debug("UrlFiltersConfirmBLogic処理終了");
		return result;
	}
}
