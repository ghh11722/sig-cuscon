/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UrlFilterCategoryMasterList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/06     ktakenaka@PROSITE                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common;

import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.UrlFilterCategoryMaster;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

import org.apache.log4j.Logger;

/**
 * クラス名 : UrlFilterCategoryMasterList
 * 機能概要 : Webフィルタカテゴリマスタ一覧を表示するための情報を取得する。
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/06
 *          新規作成
 * @see
 */
public class UrlFilterCategoryMasterList {

    private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.common.UrlFilterCategoryMasterList");
    // メッセージクラス
	private static MessageAccessor messageAccessor = null;

    /**
     * メソッド名 : コンストラクタ
     * 機能概要 : -
     * @param queryDAO
     * @param uvo
     */
    public UrlFilterCategoryMasterList() {
    }

	/**
	 * メソッド名 :getUrlFilterCategoryMasterList
	 * 機能概要 : Webフィルタカテゴリマスタ一覧情報取得処理
	 * @param
	 * @return resultList
	 */
	public List<UrlFilterCategoryMaster> getUrlFilterCategoryMasterList(
						QueryDAO queryDAO, CusconUVO uvo) throws Exception {
		log.debug("Webフィルタカテゴリマスタ取得処理開始");
		List<UrlFilterCategoryMaster> resultList;

		try {
			// Webフィルタカテゴリマスタ取得
			resultList = queryDAO.executeForObjectList("CommonM_UrlFilter_Category-1", null);

		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			// Webフィルタカテゴリマスターの検索に失敗しました。{0}
			log.fatal(messageAccessor.getMessage("FZ011040", objectgArray, uvo));
			throw e;
		}

		// Webフィルタカテゴリマスタ取得結果返却
		log.debug("Webフィルタカテゴリマスタ取得処理終了");
		return resultList;
	}


	 /**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor msgAcc){
		log.debug("setMessageAccessor処理開始");
	    setMsgAcc(msgAcc);
	    log.debug("setMessageAccessor処理終了");
	}

	/**
	 * メソッド名 : setMsgAcc
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public static void setMsgAcc(MessageAccessor messageAccessor) {
		log.debug("setMsgAcc処理開始");
		UrlFilterCategoryMasterList.messageAccessor = messageAccessor;
		log.debug("setMsgAcc処理終了");
	}

	/**
	 * メソッド名 : getMessage
	 * 機能概要 : messageAccessorをゲットする。
	 * @return MessageAccessor
	 */
	public static MessageAccessor getMessage() {
		log.debug("getMessage処理");
		return messageAccessor;
	}
}
