/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateSchedulesBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.blogic;

import java.util.List;
import org.apache.log4j.Logger;
import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.Schedules;
import jp.co.kddi.secgw.cuscon.fwsetting.common.ScheduleMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.CheckDateTime;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.ScheduleList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.dto.ScheduleOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.dto.UpdateSchedulesInput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : UpdateSchedulesBLogic
 * 機能概要 :スケジュールオブジェクト更新処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class UpdateSchedulesBLogic implements BLogic<UpdateSchedulesInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;

	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.blogic.UpdateSchedulesBLogic");
	private MessageAccessor messageAccessor = null;
	/**
	 * メソッド名 :execute
	 * 機能概要 :スケジュールオブジェクト更新処理を行う。
	 * @param param UpdateSchedulesInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(UpdateSchedulesInput param) {

		log.debug("BLogicResult処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");
     // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		CheckDateTime checktime = new CheckDateTime();

		int timeSize = param.getStartTimeList().length;
		String[] timeList = new String[timeSize];

		// デイリーの場合
		if(param.getShowRecurrence() == 0) {
			log.debug("デイリー");

			// 入力設定時間大小関係チェックを行う。
			if(!checktime.checkTime(param.getUvo(),
						param.getStartTimeList(), param.getEndTimeList())) {
				// 入力設定時間大小関係エラー
	        	log.debug("入力設定時間大小関係エラー");
	        	messages.add("message", new BLogicMessage("DK060057"));
				result.setErrors(messages);
				result.setResultString("failure");
	        	return result;
			}
			// 時刻リスト分繰り返す。
			for(int i=0; i< param.getStartTimeList().length; i++) {
				log.debug("startTimeList = " + param.getStartTimeList()[i] +
								", endTimeList = " + param.getEndTimeList()[i]);

				timeList[i] = param.getStartTimeList()[i] + "-" + param.getEndTimeList()[i];
			}

		// ウィークリーの場合
		} else if (param.getShowRecurrence() == 1) {
			log.debug("ウィークリー");

			// 入力設定時間大小関係チェックを行う。
			if(!checktime.checkTime(param.getUvo(),
					param.getStartTimeList(), param.getEndTimeList())) {
				// 入力設定時間大小関係エラー
	        	log.debug("入力設定時間大小関係エラー");
	        	messages.add("message", new BLogicMessage("DK060057"));
				result.setErrors(messages);
				result.setResultString("failure");
	        	return result;
			}
			// 時刻リスト分繰り返す。
			for(int i=0; i< param.getStartTimeList().length; i++) {
				log.debug("weekList = " + param.getWeekList()[i] +
						", startTimeList = " + param.getStartTimeList()[i] +
								", endTimeList = " + param.getEndTimeList()[i]);

				timeList[i] = param.getWeekList()[i] + "@" + param.getStartTimeList()[i] +
													"-" + param.getEndTimeList()[i];
			}

		// 指定日時の場合
		} else {
			log.debug("指定日時の場合:");

			if(!checktime.checkDate(param.getUvo(),
					param.getStartDateList(), param.getStartTimeList(),
						param.getEndDateList(),param.getEndTimeList())) {
				// 入力設定時間大小関係エラー
	        	log.debug("入力設定時間大小関係エラー");
	        	messages.add("message", new BLogicMessage("DK060057"));
				result.setErrors(messages);
				result.setResultString("failure");
	        	return result;
			}

			// 時刻リスト分繰り返す。
			for(int i=0; i< param.getStartTimeList().length; i++) {
				log.debug("startDateList = " + param.getStartDateList()[i] +
						", startTimeList = " + param.getStartTimeList()[i] +
						", endDateList = " + param.getEndDateList()[i] +
								", endTimeList = " + param.getEndTimeList()[i]);

				timeList[i] = param.getStartDateList()[i] + "@" + param.getStartTimeList()[i] +
								"-" + param.getEndDateList()[i] + "@" + param.getEndTimeList()[i];
			}

		}

        // スケジュールマスタクラスを生成する。
    	ScheduleMasterList scheduleMaster = new ScheduleMasterList();

    	// スケジュール共通クラスを生成する。
    	ScheduleList schedule = new ScheduleList();
   		// スケジュール一覧
		List<Schedules> sheduleList = null;

   		try {
   			log.debug("name差分比較処理:変更前 = " + param.getSelectSchedule().getName() +
					", 変更後=" + param.getShowName());
	        // name差分比較処理
	        if(!param.getSelectSchedule().getName().equals(param.getShowName())) {
	        	log.debug("name差分比較処理:変更前 = " + param.getSelectSchedule().getName() +
	        									", 変更後=" + param.getShowName());
	        	Schedules base = new Schedules();
	        	base.setVsysId(param.getUvo().getVsysId());
	        	base.setGenerationNo(CusconConst.ZERO_GENE);
	        	base.setModName(param.getShowName());
	        	base.setName(param.getSelectSchedule().getName());

	        	// オブジェクト名の存在チェックを行う。
	        	if(!schedule.checkScheduleName(updateDAO, queryDAO, param.getUvo(), base)) {
	        		log.debug("既に該当オブジェクト名が存在する。");
		        	messages.add("message", new BLogicMessage("DK060001"));
					result.setErrors(messages);
					result.setResultString("failure");
		    		return result;
	        	}
	        }

	        // DB更新処理を行う。
	        updateDifRecurrence(param, timeList, schedule);

	        // スケジュール一覧取得処理を行う。
			sheduleList = scheduleMaster.getScheduleMasterList(
										queryDAO, param.getUvo(), 0,
														CusconConst.ZERO_GENE);
   		} catch(Exception e) {
   			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"EK061007", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK060052"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
   		}

		// アウトプットデータクラスに設定する。
		ScheduleOutput out = new ScheduleOutput();
	    out.setScheduleList(sheduleList);

	    result.setResultObject(out);
	    result.setResultString("success");

	    log.debug("BLogicResult処理開始");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}

	/**
	 * メソッド名 : updateDifRecurrence
	 * 機能概要 : スケジュールオブジェクトの更新処理を行う。
	 * @param param UpdateSchedulesInputデータクラス
	 * @param schedule
	 * @throws Exception
	 */
	private void updateDifRecurrence(UpdateSchedulesInput param, String[] timeList,
											ScheduleList schedule) throws Exception {

		log.debug("updateDifRecurrence処理開始");
		Schedules base = new Schedules();
    	base.setVsysId(param.getUvo().getVsysId());
    	base.setGenerationNo(CusconConst.ZERO_GENE);
    	base.setModName(param.getShowName());
    	base.setName(param.getSelectSchedule().getName());
    	base.setRecurrence(param.getShowRecurrence());
    	try {
			// 変更フラグが'1'(追加)でない場合のみ更新する
			updateDAO.execute("UpdateSchedulesBLogic-3", base);
			// 変更フラグが'1'(追加)の場合のみ更新する。
			updateDAO.execute("UpdateSchedulesBLogic-4", base);

			// 変更後オブジェクト名にて更新した情報を取得する。
			List<Schedules> scheduleList =
				queryDAO.executeForObjectList("UpdateSchedulesBLogic-1", base);

			base.setSeqNo(scheduleList.get(0).getSeqNo());
    	} catch(Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		log.fatal(messageAccessor.getMessage("FK061027", objectgArray, param.getUvo()));
    		throw e;
    	}

		try {
			// recurrence差分比較処理
			if(param.getSelectSchedule().getRecurrence() == param.getShowRecurrence()) {
				log.debug("recurrence比較処理:変更前 = " +
						param.getSelectSchedule().getRecurrence() + ", 変更後 = " +
						param.getShowRecurrence());
				// recurrenceがDAILYの場合
				if(param.getSelectSchedule().getRecurrence() == CusconConst.DAILY) {
					log.debug("DAILY削除＆登録");
					// DAILYの情報を削除する。
					schedule.delDaiySchedule(updateDAO, param.getUvo(), base);

					// DAILYの情報を登録する。
					schedule.registDaiySchedule(updateDAO, param.getUvo(), base, timeList);

				// recurrenceがWEEKLYの場合
				} else if(param.getSelectSchedule().getRecurrence() == CusconConst.WEEKLY) {
					log.debug("WEEKLY削除＆登録");
					// WEEKLYの情報を削除する。
					schedule.delWeeklySchedule(updateDAO, param.getUvo(), base);

					// WEEKLYの情報を登録する。
					schedule.registWeeklySchedule(updateDAO, param.getUvo(), base, timeList);

				// recurrenceがNONRECURRINGの場合
				} else {
					log.debug("WEEKLY削除＆登録");
					// NONRECURRINGの情報を削除する。
					schedule.delNonSchedule(updateDAO, param.getUvo(), base);

					// NONRECURRINGの情報を登録する。
					schedule.registNonSchedule(updateDAO, param.getUvo(), base, timeList);
				}

			} else {
				// 変更前recurrenceがDAILYの場合
				if(param.getSelectSchedule().getRecurrence() == CusconConst.DAILY) {
					log.debug("recurrence比較処理:変更前 = " +
							param.getSelectSchedule().getRecurrence() + ", 変更後 = " +
							param.getShowRecurrence());
					log.debug("DAILY削除");
					// DAILYの情報を削除する。
					schedule.delDaiySchedule(updateDAO, param.getUvo(), base);

					// 変更後がWEEKLYの場合
					if(param.getShowRecurrence() == CusconConst.WEEKLY) {
						log.debug("WEEKLY登録");
						// WEEKLYの情報を登録する。
						schedule.registWeeklySchedule(
								updateDAO, param.getUvo(), base, timeList);

					// 変更後がNONRECURRINGの場合
					} else {
						log.debug("NONRECURRING登録");
						// NONRECURRINGの情報を登録する。
						schedule.registNonSchedule(
								updateDAO, param.getUvo(), base, timeList);
					}

				// 変更前recurrenceがWEEKLYの場合
				} else if(param.getSelectSchedule().getRecurrence() == CusconConst.WEEKLY) {
					log.debug("WEEKLY削除");
					// WEEKLYの情報を削除する。
					schedule.delWeeklySchedule(updateDAO, param.getUvo(), base);

					// 変更後がDAILYの場合
					if(param.getShowRecurrence() == CusconConst.DAILY) {
						log.debug("DAILY登録");
						// DAILYの情報を登録する。
						schedule.registDaiySchedule(
								updateDAO, param.getUvo(), base, timeList);

					// 変更後がNONRECURRINGの場合
					} else {
						log.debug("NONRECURRING登録");
						// NONRECURRINGの情報を登録する。
						schedule.registNonSchedule(updateDAO, param.getUvo(),
																base, timeList);
					}

				// 変更前recurrenceがNONRECURRINGの場合
				} else {
					log.debug("NONRECURRING削除");
					// NONRECURRINGの情報を削除する。
					schedule.delNonSchedule(updateDAO, param.getUvo(), base);

					// 変更後がDAILYの場合
					if(param.getShowRecurrence() == CusconConst.DAILY) {
						log.debug("DAILY登録");
						// DAILYの情報を登録する。
						schedule.registDaiySchedule(
									updateDAO, param.getUvo(), base, timeList);

					// 変更後がWEEKLYの場合
					} else {
						log.debug("WEEKLY登録");
						// WEEKLYの情報を登録する。
						schedule.registWeeklySchedule(
								updateDAO, param.getUvo(), base, timeList);
					}
				}
			}
		} catch(Exception e) {
    		// 例外
    		Object[] objectgArray = {e.getMessage()};
    		log.error(messageAccessor.getMessage(
    						"EK061007", objectgArray, param.getUvo()));
    		throw e;
    	}

		log.debug("updateDifRecurrence処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
