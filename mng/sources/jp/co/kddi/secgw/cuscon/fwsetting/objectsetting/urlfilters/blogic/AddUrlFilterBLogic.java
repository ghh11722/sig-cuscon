/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddUrlFilterBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/06     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.blogic;

import java.util.ArrayList;
import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.common.vo.UrlFilterCategoryMaster;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.UrlFilterCategoryMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.dto.CommonInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.CommonSelectItem;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.UrlFilterCategoryInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.dto.AddUrlFilterOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;
import jp.terasoluna.fw.util.PropertyUtil;

import org.apache.log4j.Logger;

/**
 * クラス名 : AddUrlFilterBLogic
 * 機能概要 :Webフィルタ追加選択時の処理を行う。
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/06
 *          新規作成
 * @see
 */
public class AddUrlFilterBLogic implements BLogic<CommonInput>{

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationfilters.blogic.AddUrlFilterBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : execute
	 * 機能概要 : Webフィルタ追加選択時の処理を行う。
	 * @param params AddUrlFilterInput入力データクラス
	 * @return result BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(CommonInput params) {

		log.debug("AddUrlFilterBLogic処理開始");

		List<UrlFilterCategoryMaster> urlCategoryMasterList = null;

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		BLogicMessages errorMessage = new BLogicMessages();// エラーメッセージ
		result.setResultString("failure");

        // オブジェクトの登録数
        int registNum  = 0;
    	try {
    		InputBase inputBase = new InputBase();
            inputBase.setVsysId(params.getUvo().getVsysId());
            inputBase.setGenerationNo(CusconConst.ZERO_GENE);
            inputBase.setModFlg(CusconConst.DEL_NUM);

            //オブジェクトの登録数を取得する。
    		registNum = queryDAO.executeForObject("UrlFiltersBLogic-1"
        		, inputBase, java.lang.Integer.class);
    	} catch (Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		// Webフィルタの検索に失敗しました。{0}
    		log.fatal(messageAccessor.getMessage("FK061030", objectgArray, params.getUvo()));
    		// Webフィルタ追加画面表示に失敗しました。
    		errorMessage.add("message", new BLogicMessage("DK060076"));
    		result.setErrors(errorMessage);
    		result.setResultString("failure");
    		return result;
    	}

		// 登録上限値とオブジェクト登録数の比較を行う。
        if(params.getUvo().getUrlProfileMax() <= registNum) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
        	log.debug("登録上限値のため登録できませんでした。");
        	// 登録上限値のため、追加できません。
        	errorMessage.add("message", new BLogicMessage("DK060003"));
			result.setErrors(errorMessage);
			result.setResultString("failure");
        	return result;
        }

		/***********************************************************************
		 *  Webフィルタカテゴリマスタ取得
		 **********************************************************************/
		try {
			// Webフィルタカテゴリマスタ取得
			UrlFilterCategoryMasterList categoryMasterList = new UrlFilterCategoryMasterList();
			urlCategoryMasterList = categoryMasterList.getUrlFilterCategoryMasterList(queryDAO, params.getUvo());
        } catch (Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
        	// DBアクセスエラー
        	Object[] objectgArray = {e.getMessage()};
    		// Webフィルタの検索に失敗しました。{0}
    		log.fatal(messageAccessor.getMessage("FK061030", objectgArray, params.getUvo()));
    		// Webフィルタ追加画面表示に失敗しました。
        	errorMessage.add("message", new BLogicMessage("DK060076"));
			result.setErrors(errorMessage);
			result.setResultString("failure");
        	return result;
        }

		/***********************************************************************
		 * 画面表示用Webフィルタカテゴリのリスト作成
		 **********************************************************************/
		List<UrlFilterCategoryInfo> categoryList = new ArrayList<UrlFilterCategoryInfo>();
		for (UrlFilterCategoryMaster categoryMaster : urlCategoryMasterList) {
			UrlFilterCategoryInfo value = new UrlFilterCategoryInfo();
			value.setSeqNo(categoryMaster.getSeqNo());
			value.setName(categoryMaster.getName());
			value.setAction(PropertyUtil.getProperty("fwsetting.objectsetting.urlfilters.categoryaction_init"));
			categoryList.add(value);
		}

		/***********************************************************************
		 * セレクトボックス（ブロック時動作）のリスト作成
		 **********************************************************************/
		List<CommonSelectItem> blockActionList =new ArrayList<CommonSelectItem>();
		{
			String blockActionCsv = PropertyUtil.getProperty("fwsetting.objectsetting.urlfilters.blockaction");
			String[] strBlockActionList = blockActionCsv.split(",");
			for (String str : strBlockActionList) {
				CommonSelectItem item = new CommonSelectItem();
				String[] str2 = str.split(":");
				item.setValue(str2[0]);
				item.setText(str2[1]);
				blockActionList.add(item);
			}
		}

		/***********************************************************************
		 * セレクトボックス（カテゴリアクション）のリスト作成
		 **********************************************************************/
		List<CommonSelectItem> categoryActionList =new ArrayList<CommonSelectItem>();
		{
			String ctgrActCsv = PropertyUtil.getProperty("fwsetting.objectsetting.urlfilters.categoryaction");
			String[] ctgrActList = ctgrActCsv.split(",");
			for (String str : ctgrActList) {
				CommonSelectItem item = new CommonSelectItem();
				String[] str2 = str.split(":");
				item.setValue(str2[0]);
				item.setText(str2[1]);

				categoryActionList.add(item);
			}
		}

		// 返却用リスト
		AddUrlFilterOutput output = new AddUrlFilterOutput();
		output.setCategoryList(categoryList);
		output.setBlockActionList(blockActionList);
		output.setCategoryActionList(categoryActionList);

		// レスポンス返却
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("AddUrlFilterBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
