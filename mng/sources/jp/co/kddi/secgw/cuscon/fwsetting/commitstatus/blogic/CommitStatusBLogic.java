/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CommitStatusBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/06/16     oohashij         コミットスタータス状態を取得する。
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.commitstatus.blogic;

import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.commitstatus.dto.CommitStatusInput;
import jp.co.kddi.secgw.cuscon.fwsetting.commitstatus.dto.CommitStatusOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.common.CommitStatusManager;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.CommitStatus;

import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;

import org.apache.log4j.Logger;

/**
 * クラス名 : CommitStatusBLogic
 * 機能概要 : コミットスタータス状態を取得する。
 * 備考 :
 * @author oohashij
 * @version 1.0 oohashij
 *          Created 2010/06/16
 *          新規作成
 * @see
 */
public class CommitStatusBLogic implements BLogic<CommitStatusInput> {

	/**
	 * QueryDAO。
	 * Springによりインスタンス生成され設定される。
	 */
	private QueryDAO queryDAO = null;
	// メッセージクラス
	private MessageAccessor messageAccessor = null;

	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.commitstatus.blogic.CommitStatusBLogic");

	/**
	 * メソッド名 :execute
	 * 機能概要 : コミットスタータス状態取得処理を行う
	 * @param param CommitStatusBLogicの入力クラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(CommitStatusInput param) {
		log.debug("CommitStatusBLogic処理開始");
		BLogicResult result = new BLogicResult();
		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		CommitStatus commitStatus = null;
		CommitStatusManager commtStatManager = new CommitStatusManager();

		try {
			// コミット確認状態を取得する。
			commitStatus = commtStatManager.getCommitStatus(queryDAO, param.getUvo());
			if (commitStatus == null) {
				messages.add("message", new BLogicMessage("DK070024"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}
		} catch (Exception e) {
			// コミット確認状態取得処理失敗ログ出力
			Object[] objectgArray = { e.getMessage() };
			log.error(messageAccessor.getMessage("EK071004", objectgArray, param.getUvo()));
			messages.add("message", new BLogicMessage("DK070025"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		// ビジネスロジックの出力クラスに結果を設定する
		CommitStatusOutput out = new CommitStatusOutput();
		out.setProcessingKind(commitStatus.getProcessingKind());
		out.setStatus(commitStatus.getStatus());
		out.setMessage(commitStatus.getMessage());
		out.setStartTime(commitStatus.getStartTime());

		result.setResultObject(out);
		result.setResultString("success");
		log.debug("CommitStatusBLogic処理終了");
		return result;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
