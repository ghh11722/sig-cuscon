/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ApplicationList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/24     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.common;

import java.util.List;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationList4Search;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

import org.apache.log4j.Logger;

/**
 * クラス名 : ApplicationList
 * 機能概要 : アプリケーション一覧を表示するための情報を取得する。
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/24
 *          新規作成
 * @see
 */
public class ApplicationList {

    private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.common.ApplicationList");
    // メッセージクラス
	private static MessageAccessor messageAccessor = null;

    /**
     * メソッド名 : コンストラクタ
     * 機能概要 : -
     * @param queryDAO
     * @param uvo
     */
    public ApplicationList() {
    }

	/**
	 * メソッド名 :getApplicationList
	 * 機能概要 : アプリケーション一覧情報取得処理
	 * @param
	 * @return resultList
	 */
	public List<SelectApplicationList4Search> getApplicationList(
						QueryDAO queryDAO, CusconUVO uvo) throws Exception {
		log.debug("getApplicationList処理開始");
		List<SelectApplicationList4Search> resultList;

		try {
			// アプリケーション一覧情報取得
			resultList = queryDAO.executeForObjectList("CommonM_Applications-1", null);

		} catch(Exception e) {
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FZ011016", objectgArray, uvo));
			throw e;
		}

		// アプリケーション一覧情報取得結果返却
		log.debug("getApplicationList処理終了");
		return resultList;
	}


	 /**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor msgAcc){
		log.debug("setMessageAccessor処理開始");
	    setMsgAcc(msgAcc);
	    log.debug("setMessageAccessor処理終了");
	}

	/**
	 * メソッド名 : setMsgAcc
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public static void setMsgAcc(MessageAccessor messageAccessor) {
		log.debug("setMsgAcc処理開始");
		ApplicationList.messageAccessor = messageAccessor;
		log.debug("setMsgAcc処理終了");
	}

	/**
	 * メソッド名 : getMessage
	 * 機能概要 : messageAccessorをゲットする。
	 * @return MessageAccessor
	 */
	public static MessageAccessor getMessage() {
		log.debug("getMessage処理");
		return messageAccessor;
	}
}
