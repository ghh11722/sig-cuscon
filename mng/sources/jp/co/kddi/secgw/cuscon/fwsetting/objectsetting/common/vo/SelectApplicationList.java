/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectApplicationList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/24     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo;

import java.io.Serializable;

/**
 * クラス名 : SelectApplicationList
 * 機能概要 : アプリケーション一覧データクラス
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/24
 *          新規作成
 * @see
 */
public class SelectApplicationList implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 311405362060207830L;
	// シーケンス番号
	private int seqNo = 0;
	// Name
	private String name = null;
	// カテゴリシーケンス番号
	private int categorySeqNo = 0;
	// Category
	private String category = null;
	// サブカテゴリシーケンス番号
	private int subCategorySeqNo = 0;
	// Subcategory
	private String subCategory = null;
	// テクノロジシーケンス番号
	private int technologySeqNo = 0;
	// technology
	private String technology = null;
	// Risk
	private int risk = 0;

	/**
	 * メソッド名 : seqNoのGetterメソッド
	 * 機能概要 : seqNoを取得する。
	 * @return seqNo
	 */
	public int getSeqNo() {
		return seqNo;
	}
	/**
	 * メソッド名 : seqNoのSetterメソッド
	 * 機能概要 : seqNoをセットする。
	 * @param seqNo
	 */
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : categorySeqNoのGetterメソッド
	 * 機能概要 : categorySeqNoを取得する。
	 * @return categorySeqNo
	 */
	public int getCategorySeqNo() {
		return categorySeqNo;
	}
	/**
	 * メソッド名 : categorySeqNoのSetterメソッド
	 * 機能概要 : categorySeqNoをセットする。
	 * @param categorySeqNo
	 */
	public void setCategorySeqNo(int categorySeqNo) {
		this.categorySeqNo = categorySeqNo;
	}
	/**
	 * メソッド名 : categoryのGetterメソッド
	 * 機能概要 : categoryを取得する。
	 * @return category
	 */
	public String getCategory() {
		return category;
	}
	/**
	 * メソッド名 : categoryのSetterメソッド
	 * 機能概要 : categoryをセットする。
	 * @param category
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	 * メソッド名 : subCategorySeqNoのGetterメソッド
	 * 機能概要 : subCategorySeqNoを取得する。
	 * @return subCategorySeqNo
	 */
	public int getSubCategorySeqNo() {
		return subCategorySeqNo;
	}
	/**
	 * メソッド名 : subCategorySeqNoのSetterメソッド
	 * 機能概要 : subCategorySeqNoをセットする。
	 * @param subCategorySeqNo
	 */
	public void setSubCategorySeqNo(int subCategorySeqNo) {
		this.subCategorySeqNo = subCategorySeqNo;
	}
	/**
	 * メソッド名 : subCategoryのGetterメソッド
	 * 機能概要 : subCategoryを取得する。
	 * @return subCategory
	 */
	public String getSubCategory() {
		return subCategory;
	}
	/**
	 * メソッド名 : subCategoryのSetterメソッド
	 * 機能概要 : subCategoryをセットする。
	 * @param subCategory
	 */
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}
	/**
	 * メソッド名 : technologySeqNoのGetterメソッド
	 * 機能概要 : technologySeqNoを取得する。
	 * @return technologySeqNo
	 */
	public int getTechnologySeqNo() {
		return technologySeqNo;
	}
	/**
	 * メソッド名 : technologySeqNoのSetterメソッド
	 * 機能概要 : technologySeqNoをセットする。
	 * @param technologySeqNo
	 */
	public void setTechnologySeqNo(int technologySeqNo) {
		this.technologySeqNo = technologySeqNo;
	}
	/**
	 * メソッド名 : technologyのGetterメソッド
	 * 機能概要 : technologyを取得する。
	 * @return technology
	 */
	public String getTechnology() {
		return technology;
	}
	/**
	 * メソッド名 : technologyのSetterメソッド
	 * 機能概要 : technologyをセットする。
	 * @param technology
	 */
	public void setTechnology(String technology) {
		this.technology = technology;
	}
	/**
	 * メソッド名 : riskのGetterメソッド
	 * 機能概要 : riskを取得する。
	 * @return risk
	 */
	public int getRisk() {
		return risk;
	}
	/**
	 * メソッド名 : riskのSetterメソッド
	 * 機能概要 : riskをセットする。
	 * @param risk
	 */
	public void setRisk(int risk) {
		this.risk = risk;
	}
}
