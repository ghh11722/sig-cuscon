/*******************************************************************************
 * Copyright(C) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名：サンプル
 * ファイル名  ：AddressesBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/18     morisou                 初版作成
 *
 *******************************************************************************
 */

package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.blogic;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.fwsetting.common.CommitStatusManager;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.AddressList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.dto.CommonInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.dto.AddressesOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;

/**
 * クラス名：AddressesBLogic
 * 機能概要：
 * 備考：サンプル
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/2/16
 *          新規作成
 */

public class AddressesBLogic implements BLogic<CommonInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.addresses.blogic.AddressesBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : BLogicResult
	 * 機能概要 : アドレスの一覧情報を取得し、取得した情報を呼び出し元に返却する。
	 * @param param
	 * @return result
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(CommonInput params) {
		log.debug("AddressesBLogic処理開始");

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		 // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = params.getUvo();

		// コミットステータス管理クラスを生成する。
		CommitStatusManager commtStatMgr = new CommitStatusManager();

		AddressesOutput output = new AddressesOutput();

		try{
			if (commtStatMgr.isCommitting(queryDAO, uvo)) {
				// コミット中
				log.debug("コミット中です。");
	        	messages.add("message", new BLogicMessage("DK060074"));
				result.setErrors(messages);
				result.setMessages(messages);
				result.setResultString("failure");
	        	return result;
			} else {
				// 一覧情報検索共通処理
				AddressList addressList = new AddressList();
				output.setAddressList(addressList.getAddressList(queryDAO, uvo));
			}
		} catch(Exception e) {
			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"EK061001", objectgArray, params.getUvo()));
        	messages.add("message", new BLogicMessage("DK060004"));
			result.setErrors(messages);
			result.setMessages(messages);
			result.setResultString("failure");
        	return result;
		}

		// レスポンス返却
		result.setResultObject(output);
		result.setResultString("success");

		log.debug("AddressesBLogic処理終了");
		return result;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
