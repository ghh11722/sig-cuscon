/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : EditServiceGroupBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/24     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.servicegroups.blogic;

import java.util.ArrayList;
import java.util.List;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.common.vo.ServiceGroups;
import jp.co.kddi.secgw.cuscon.common.vo.Services;
import jp.co.kddi.secgw.cuscon.common.vo.ServicesMaster;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.ServicesAndGroupsInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.servicegroups.dto.EditServiceGroupInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.servicegroups.dto.EditServiceGroupOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

import org.apache.log4j.Logger;

/**
 * クラス名 : EditServiceGroupBLogic
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/02/24
 *          新規作成
 * @see
 */
public class EditServiceGroupBLogic implements BLogic<EditServiceGroupInput> {

	// オブジェクトタイプ定数
	private static final int SERVICE_MASTER = 0;    // サービスマスタ
	private static final int SERVICE_OBJECT = 1;    // サービスオブジェクト
	private static final int SERVICE_GROUP = 2;     // サービスグループオブジェクト

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;

	private Logger log = Logger.getLogger(
			"jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.servicegroups.blogic.EditServiceGroupBLogic");
	private MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : BLogicResult
	 * 機能概要 :
	 * @param param
	 * @return result
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(EditServiceGroupInput params) {
		log.debug("EditServiceGroupBLogic処理開始");
		String services;           // リクエスト情報.Service
		String[] strAry;           // 分割した文字列を格納する配列

		// BLogicResultの生成・設定
		BLogicResult result = new BLogicResult();
		 // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		result.setResultString("failure");

		// セッション情報.CusconUVOからVSYS_IDを取得する
		CusconUVO uvo = params.getUvo();
		String vsysId = uvo.getVsysId();

		// InputBase設定
		InputBase inputBase = new InputBase();
		inputBase.setVsysId(vsysId);                            // Vsys-ID
		inputBase.setGenerationNo(CusconConst.ZERO_GENE);    // 世代番号
		inputBase.setModFlg(CusconConst.DEL_NUM);            // 変更フラグ

		// 返却結果リスト
		EditServiceGroupOutput output = new EditServiceGroupOutput();

		// All Service & Groups 格納用リスト
		List<ServicesAndGroupsInfo> outputServiceAndGroup =
									new ArrayList<ServicesAndGroupsInfo>();

		// インデックス取得変数定義
		int index = params.getIndex();

		// 入力パラメータで受け取ったServiceをカンマで分割
		services = params.getServiceList().get(index).getServices();
		strAry = services.split(",");
		// サービスマスタ情報取得
		List<ServicesMaster> serviceMasterList;
		try {
			// サービスマスタ情報取得
			serviceMasterList = queryDAO.executeForObjectList(
					"CommonM_Services-1", null);
		} catch(Exception e) {

        	// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラーログ出力
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061024", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060044"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		// サービスマスタ情報を格納
		for (int i = 0; i < serviceMasterList.size(); i++) {
			log.debug("サービスマスタ情報を格納:" + i);
			ServicesAndGroupsInfo servicesAndGroups = new ServicesAndGroupsInfo();
			servicesAndGroups.setSeqNo(serviceMasterList.get(i).getSeqNo());      // SEQ_NO
			servicesAndGroups.setName(serviceMasterList.get(i).getName());        // オブジェクト名
			servicesAndGroups.setObjectType(SERVICE_MASTER);                      // オブジェクトタイプ
			servicesAndGroups.setChkFlg(false);                                   // チェック情報
			// チェックボックスON/OFF判別
			for (int j = 0; j < strAry.length; j++) {
				log.debug("チェックボックスON/OFF判別:" + j);
		    	// 入力パラメータのアドレス情報と、DBから取得したアドレス名が一致する場合
		    	if (strAry[j].equals(serviceMasterList.get(i).getName())) {
		    		servicesAndGroups.setChkFlg(true);                            // チェック情報
		    	}
		    }
			outputServiceAndGroup.add(servicesAndGroups);
		}

		// サービスオブジェクト情報取得
		List<Services> serviceObjectList;
		try {
			// サービスオブジェクト情報取得
			serviceObjectList = queryDAO.executeForObjectList(
					"ServiceGroupsBLogic-13", inputBase);
		} catch(Exception e) {

        	// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラーログ出力
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061024", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060044"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		// サービスオブジェクト情報を格納
		for (int i = 0; i < serviceObjectList.size(); i++) {
			log.debug("サービスオブジェクト情報を格納:" + i);
			ServicesAndGroupsInfo servicesAndGroups = new ServicesAndGroupsInfo();
			servicesAndGroups.setSeqNo(serviceObjectList.get(i).getSeqNo());      // SEQ_NO
			servicesAndGroups.setName(serviceObjectList.get(i).getName());        // オブジェクト名
			servicesAndGroups.setObjectType(SERVICE_OBJECT);                      // オブジェクトタイプ
			servicesAndGroups.setChkFlg(false);                                   // チェック情報
			// チェックボックスON/OFF判別
			for (int j = 0; j < strAry.length; j++) {
				log.debug("チェックボックスON/OFF判別:" + j);
		    	// 入力パラメータのアドレス情報と、DBから取得したアドレス名が一致する場合
		    	if (strAry[j].equals(serviceObjectList.get(i).getName())) {
		    		servicesAndGroups.setChkFlg(true);                            // チェック情報
		    	}
		    }
			outputServiceAndGroup.add(servicesAndGroups);
		}

		// サービスグループ検索条件設定
		ServiceGroups searchServiceGroupKey = new ServiceGroups();
		searchServiceGroupKey.setVsysId(vsysId);
		searchServiceGroupKey.setGenerationNo(CusconConst.ZERO_GENE);
		searchServiceGroupKey.setModFlg(CusconConst.DEL_NUM);
		searchServiceGroupKey.setName(params.getServiceList().get(index).getName());

		// サービスグループ情報取得（但し現在編集中のサービスグループは除く）
		List<ServiceGroups> serviceGroupList;
		try {
			// サービスグループ情報取得（但し現在編集中のサービスグループは除く）
			serviceGroupList = queryDAO.executeForObjectList(
					"ServiceGroupsBLogic-5", searchServiceGroupKey);
		} catch(Exception e) {

        	// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// DBアクセスエラーログ出力
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK061024", objectgArray, params.getUvo()));
			messages.add("message", new BLogicMessage("DK060044"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		// サービスグループ情報を格納
		for (int i = 0; i < serviceGroupList.size(); i++) {
			log.debug("サービスグループ情報を格納:" + i);
			ServicesAndGroupsInfo servicesAndGroups = new ServicesAndGroupsInfo();
			servicesAndGroups.setSeqNo(serviceGroupList.get(i).getSeqNo());       // SEQ_NO
			servicesAndGroups.setName(serviceGroupList.get(i).getName());         // オブジェクト名
			servicesAndGroups.setObjectType(SERVICE_GROUP);                       // オブジェクトタイプ
			servicesAndGroups.setChkFlg(false);                                   // チェック情報
			// チェックボックスON/OFF判別
			for (int j = 0; j < strAry.length; j++) {
				log.debug("チェックボックスON/OFF判別:" + j);
		    	// 入力パラメータのアドレス情報と、DBから取得したアドレス名が一致する場合
		    	if (strAry[j].equals(serviceGroupList.get(i).getName())) {
		    		servicesAndGroups.setChkFlg(true);                            // チェック情報
		    	}
		    }
			outputServiceAndGroup.add(servicesAndGroups);
		}

		// 結果設定
		output.setName(params.getServiceList().get(index).getName());
		output.setServiceAndServiceGrp4Add(outputServiceAndGroup);
		// レスポンス返却
		result.setResultObject(output);
		result.setResultString("success");
		log.debug("EditServiceGroupBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("queryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("queryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
