/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ShowApplicationOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/28     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.rollback.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationList;
import jp.co.kddi.secgw.cuscon.fwsetting.rollback.vo.SelectShowFilterList;

/**
 * クラス名 : ShowApplicationOutput
 * 機能概要 :Application項目選択時の出力データクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/28
 *          新規作成
 * @see
 */
public class ShowApplicationOutput {

	// アプリケーションリスト
	private List<SelectApplicationList> appliList = null;

	// アプリケーショングループリスト
	private List<SelectApplicationGroupList> appliGrpList = null;

	// アプリケーションフィルタリスト
	private List<SelectShowFilterList> appliFilList = null;

	/**
	 * メソッド名 : appliListのGetterメソッド
	 * 機能概要 : appliListを取得する。
	 * @return appliList
	 */
	public List<SelectApplicationList> getAppliList() {
		return appliList;
	}

	/**
	 * メソッド名 : appliListのSetterメソッド
	 * 機能概要 : appliListをセットする。
	 * @param appliList
	 */
	public void setAppliList(List<SelectApplicationList> appliList) {
		this.appliList = appliList;
	}

	/**
	 * メソッド名 : appliGrpnListのGetterメソッド
	 * 機能概要 : appliGrpnListを取得する。
	 * @return appliGrpnList
	 */
	public List<SelectApplicationGroupList> getAppliGrpList() {
		return appliGrpList;
	}

	/**
	 * メソッド名 : appliGrpnListのSetterメソッド
	 * 機能概要 : appliGrpnListをセットする。
	 * @param appliGrpnList
	 */
	public void setAppliGrpList(List<SelectApplicationGroupList> appliGrpList) {
		this.appliGrpList = appliGrpList;
	}

	/**
	 * メソッド名 : appliFilListのGetterメソッド
	 * 機能概要 : appliFilListを取得する。
	 * @return appliFilList
	 */
	public List<SelectShowFilterList> getAppliFilList() {
		return appliFilList;
	}

	/**
	 * メソッド名 : appliFilListのSetterメソッド
	 * 機能概要 : appliFilListをセットする。
	 * @param appliFilList
	 */
	public void setAppliFilList(List<SelectShowFilterList> appliFilList) {
		this.appliFilList = appliFilList;
	}
}
