/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateSpywareBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/05/18     ktakenaka@PROSITE                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic;

import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.SecurityRules;
import jp.co.kddi.secgw.cuscon.fwsetting.common.PolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectPolicyList;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.PolicyOutput;
import jp.co.kddi.secgw.cuscon.fwsetting.policysetting.dto.UpdateSpywareInput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : UpdateSpywareBLogic
 * 機能概要 :Spyware項目編集画面にて「OK」押下時の処理を行う。
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/05/18
 *          新規作成
 * @see
 */
public class UpdateSpywareBLogic implements BLogic<UpdateSpywareInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;
	// UpdateDAO Springによりインスタンス生成され設定される。
	private UpdateDAO updateDAO = null;

	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.policysetting.blogic.UpdateSpywareBLogic");
	private MessageAccessor messageAccessor = null;
	/**
	 * メソッド名 :execute
	 * 機能概要 :スパイウェアチェックフラグの更新処理を行う。
	 * @param param スパイウェアチェック編集画面入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(UpdateSpywareInput param) {

		log.debug("UpdateSpywareBLogic処理開始");
		BLogicResult result = new BLogicResult();
        result.setResultString("failure");
        BLogicMessages errorMessage = new BLogicMessages();// エラーメッセージ

        try {
    		// 更新条件を設定する。
            SecurityRules base = new SecurityRules();
            base.setVsysId(param.getUvo().getVsysId());
            base.setGenerationNo(CusconConst.ZERO_GENE);
            base.setName(param.getName());
            base.setModName(param.getName());
            base.setSpywareFlg(param.getRadio());
            base.setModFlg(CusconConst.MOD_NUM);

            // 選択ポリシー情報を取得する。
			List<SecurityRules> rule =
				queryDAO.executeForObjectList("PolicyRulesList-1", base);

			// 変更フラグが1でない場合
			if(rule.get(0).getModFlg() != CusconConst.ADD_NUM) {
				// 変更フラグ、スパイウェアチェックフラグを更新する。
				updateDAO.execute("UpdateSpywareBLogic-1", base);
			} else {
				// スパイウェアチェックフラグを更新する。
				updateDAO.execute("UpdateSpywareBLogic-2", base);
			}
        } catch(Exception e) {
        	// トランザクションロールバック
        	TransactionUtil.setRollbackOnly();
        	// DBアクセスエラー
        	Object[] objectgArray = {e.getMessage()};
        	// ポリシーの更新に失敗しました。{0}
        	log.fatal(messageAccessor.getMessage(
        			"FK051004", objectgArray, param.getUvo()));
        	// スパイウェアチェック利用フラグ更新処理に失敗しました。
        	errorMessage.add("message", new BLogicMessage("DK050040"));
        	result.setErrors(errorMessage);
        	result.setResultString("failure");
        	return result;
        }

		// ポリシーリストクラスを生成する。
        PolicyList policy = new PolicyList();
        // 選択された世代番号のポリシーリスト
        List<SelectPolicyList> policyList = null;

        try {
	        // 選択された世代番号のポリシーリストを取得する。
	        policyList = policy.getPolicylist(queryDAO, param.getUvo(),
	        					CusconConst.ZERO_GENE, CusconConst.POLICY);
        } catch(Exception e) {
        	// トランザクションロールバック
        	TransactionUtil.setRollbackOnly();
        	// DBアクセスエラー
        	Object[] objectgArray = {e.getMessage()};
        	// ポリシー設定で例外が発生しました。{0}
        	log.error(messageAccessor.getMessage(
        			"EK051001", objectgArray, param.getUvo()));
        	// スパイウェアチェック利用フラグ更新処理に失敗しました。
        	errorMessage.add("message", new BLogicMessage("DK050040"));
        	result.setErrors(errorMessage);
        	result.setResultString("failure");
        	return result;
        }

        // ビジネスロジックの出力クラスに結果を設定する
        PolicyOutput out = new PolicyOutput();
        out.setPolicyList(policyList);
        result.setResultObject(out);
        result.setResultString("success");

        log.debug("UpdateSpywareBLogic処理終了");
		return result;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * updateDAOを設定します。
	 * @param updateDAO updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		this.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
