/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UrlFilterCategoryInfoKey.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/06     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo;

import java.io.Serializable;

/**
 * クラス名 : UrlFilterCategoryInfoKey
 * 機能概要 :
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/06
 *          新規作成
 * @see
 */
public class UrlFilterCategoryInfoKey implements Serializable{

	// シリアルバージョンID
	private static final long serialVersionUID = 2001931146427060133L;
	// Vsys-ID（Webフィルタカテゴリ）
	private String vsysId = null;
	// WebフィルタオブジェクトのSEQNO（Webフィルタカテゴリ）
	private int urlfiltersSeqno = 0;
	// Webフィルタカテゴリ アクション初期値
	private String defaultAction = null;

	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得する。
	 * @return vsysId
	 */
	public String getVsysId() {
		return vsysId;
	}
	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdをセットする。
	 * @param vsysId
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}

	/**
	 * メソッド名 : urlfiltersSeqnoのGetterメソッド
	 * 機能概要 : urlfiltersSeqnoを取得する。
	 * @return urlfiltersSeqno
	 */
	public int getUrlfiltersSeqno() {
		return urlfiltersSeqno;
	}
	/**
	 * メソッド名 : urlfiltersSeqnoのSetterメソッド
	 * 機能概要 : urlfiltersSeqnoをセットする。
	 * @param urlfiltersSeqno
	 */
	public void setUrlfiltersSeqno(int urlfiltersSeqno) {
		this.urlfiltersSeqno = urlfiltersSeqno;
	}

	/**
	 * メソッド名 : serialversionuidのGetterメソッド
	 * 機能概要 : serialversionuidを取得する。
	 * @return serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * メソッド名 : defaultActionのGetterメソッド
	 * 機能概要 : defaultActionを取得する。
	 * @return defaultAction
	 */
	public String getDefaultAction() {
		return defaultAction;
	}
	/**
	 * メソッド名 : defaultActionのSetterメソッド
	 * 機能概要 : defaultActionをセットする。
	 * @param defaultAction
	 */
	public void setDefaultAction(String defaultAction) {
		this.defaultAction = defaultAction;
	}
}
