/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SchedulesBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/02     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.blogic;

import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.Schedules;
import jp.co.kddi.secgw.cuscon.fwsetting.common.ScheduleMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.dto.CommonInput;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.dto.ScheduleOutput;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;

/**
 * クラス名 : SchedulesBLogic
 * 機能概要 :スケジュール画面一覧表示処理を行う。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/02
 *          新規作成
 * @see
 */
public class SchedulesBLogic implements BLogic<CommonInput> {

	// QueryDAO Springによりインスタンス生成され設定される。
	private QueryDAO queryDAO = null;
	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.schedules.blogic.SchedulesBLogic");
	private MessageAccessor messageAccessor = null;
	/**
	 * メソッド名 :execute
	 * 機能概要 :スケジュール画面一覧表示処理を行う。
	 * @param param CommonInput入力データクラス
	 * @return BLogicResult BLogicResultクラス
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	@Override
	public BLogicResult execute(CommonInput param) {

		log.debug("SchedulesBLogic処理開始");

		BLogicResult result = new BLogicResult();
        result.setResultString("failure");
        // エラーメッセージ
		BLogicMessages messages = new BLogicMessages();

        // スケジュール一覧取得処理を行う。
        ScheduleMasterList schedule = new ScheduleMasterList();

        List<Schedules> sheduleList = null;
        try {
			sheduleList = schedule.getScheduleMasterList(
										queryDAO, param.getUvo(), 0,
														CusconConst.ZERO_GENE);
        } catch(Exception e) {
			// 例外
        	Object[] objectgArray = {e.getMessage()};
        	log.error(messageAccessor.getMessage(
        			"EK061007", objectgArray, param.getUvo()));
        	messages.add("message", new BLogicMessage("DK060046"));
			result.setErrors(messages);
			result.setResultString("failure");
        	return result;
		}

		// アウトプットデータクラスに設定する。
		ScheduleOutput out = new ScheduleOutput();
	    out.setScheduleList(sheduleList);

	    result.setResultObject(out);
	    result.setResultString("success");

	    log.debug("SchedulesBLogic処理終了");
		return result;
	}

	/**
	 * queryDAOを設定します。
	 * @param queryDAO queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		this.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		this.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
