/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectSystemConstantList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/02/19     komakiys         初版作成
 * 2012/05/04     inoue@prosite    2版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.common.vo;

import java.io.Serializable;

/**
 * クラス名 : SelectSystemConstantList
 * 機能概要 : システム定数マスタデータクラス
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/02/19
 *          新規作成
 * @version 1.1 inoue@prosite
 *          Updated 2011/05/04
 * @see
 */
public class SelectSystemConstantList implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 6824237178731213915L;

	// PA CLI接続アドレス
	private String address;
	// PA XML-API URL
	private String url;
	// PAログインID
	private String loginId;
	// PAログインパスワード
	private String password;
	// コミット排他待ちウェイト時間
	private int waitTime = 0;
	// コミット排他待ちリトライ回数
	private int retryNum = 0;
	// PA コマンドタイムアウト時間
	private int timeout = 0;
	// PA コマンド待ちウェイト時間
	private int cmdWait = 0;
//2012.05.04 add start inoue@prosite
	// 排他チェックコマンドエラーウェイト時間
	private int exclErrwaitTime = 0;
	// 排他チェックコマンドエラーリトライ回数
	private int exclErrretryNum = 0;
//2012.05.04 add end inoue@prosite


	/**
	 * メソッド名 : addressのGetterメソッド
	 * 機能概要 : addressを取得する。
	 * @return address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * メソッド名 : addressのSetterメソッド
	 * 機能概要 : addressをセットする。
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * メソッド名 : urlのGetterメソッド
	 * 機能概要 : urlを取得する。
	 * @return url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * メソッド名 : urlのSetterメソッド
	 * 機能概要 : urlをセットする。
	 * @param url
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * メソッド名 : loginIdのGetterメソッド
	 * 機能概要 : loginIdを取得する。
	 * @return loginId
	 */
	public String getLoginId() {
		return loginId;
	}
	/**
	 * メソッド名 : loginIdのSetterメソッド
	 * 機能概要 : loginIdをセットする。
	 * @param loginId
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	/**
	 * メソッド名 : passwordのGetterメソッド
	 * 機能概要 : passwordを取得する。
	 * @return password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * メソッド名 : passwordのSetterメソッド
	 * 機能概要 : passwordをセットする。
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * メソッド名 : waitTimeのGetterメソッド
	 * 機能概要 : waitTimeを取得する。
	 * @return waitTime
	 */
	public int getWaitTime() {
		return waitTime;
	}
	/**
	 * メソッド名 : waitTimeのSetterメソッド
	 * 機能概要 : waitTimeをセットする。
	 * @param waitTime
	 */
	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}
	/**
	 * メソッド名 : retryNumのGetterメソッド
	 * 機能概要 : retryNumを取得する。
	 * @return retryNum
	 */
	public int getRetryNum() {
		return retryNum;
	}
	/**
	 * メソッド名 : retryNumのSetterメソッド
	 * 機能概要 : retryNumをセットする。
	 * @param retryNum
	 */
	public void setRetryNum(int retryNum) {
		this.retryNum = retryNum;
	}
	/**
	 * メソッド名 : timeoutのGetterメソッド
	 * 機能概要 : timeoutを取得する。
	 * @return timeout
	 */
	public int getTimeout() {
		return timeout;
	}
	/**
	 * メソッド名 : timeoutのSetterメソッド
	 * 機能概要 : timeoutをセットする。
	 * @param timeout
	 */
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	/**
	 * メソッド名 : cmdWaitのSetterメソッド
	 * 機能概要 : cmdWaitをセットする。
	 * @param cmdWait
	 */
	public void setCmdWait(int cmdWait) {
		this.cmdWait = cmdWait;
	}
	/**
	 * メソッド名 : cmdWaitのGetterメソッド
	 * 機能概要 : cmdWaitを取得する。
	 * @return cmdWait
	 */
	public int getCmdWait() {
		return cmdWait;
	}
//2012.05.04 add start inoue@prosite
	/**
	 * メソッド名 : exclErrwaitTimeのGetterメソッド
	 * 機能概要 : exclErrwaitTimeを取得する。
	 * @return exclErrwaitTime
	 */
	public int getexclErrwaitTime() {
		return exclErrwaitTime;
	}
	/**
	 * メソッド名 : exclErrwaitTimeのSetterメソッド
	 * 機能概要 : exclErrwaitTimeをセットする。
	 * @param exclErrwaitTime
	 */
	public void setexclErrwaitTime(int waitTime) {
		this.exclErrwaitTime = waitTime;
	}
	/**
	 * メソッド名 : getexclErrretryNumのGetterメソッド
	 * 機能概要 : getexclErrretryNumを取得する。
	 * @return getexclErrretryNum
	 */
	public int getexclErrretryNum() {
		return exclErrretryNum;
	}
	/**
	 * メソッド名 : getexclErrretryNumのSetterメソッド
	 * 機能概要 : getexclErrretryNumをセットする。
	 * @param getexclErrretryNum
	 */
	public void setexclErrretryNum(int retryNum) {
		this.exclErrretryNum = retryNum;
	}
//2012.05.04 add end inoue@prosite
}
