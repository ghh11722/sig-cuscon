/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ApplicationGroupsForm.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/01     morisou                 初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.applicationgroups.form;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.RiskInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationFilterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationGroupList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectApplicationList4Search;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectFilterMasterList;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SelectedAplInfo;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.SeqAndNameInfo;
import jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx;

/**
 * クラス名 : ApplicationGroupsForm
 * 機能概要 :
 * 備考 :
 * @author morisou
 * @version 1.0 morisou
 *          Created 2010/03/01
 *          新規作成
 * @see
 */
public class ApplicationGroupsForm extends ValidatorActionFormEx {

	// シリアルバージョンID
	private static final long serialVersionUID = 1L;
	// アプリケーショングループ名
	private String name = null;
	// 変更前アプリケーショングループ名
	private String oldName = null;
	// 変更後アプリケーショングループ名
	private String newName = null;
	// アプリケーショングループリスト
	private List<SelectApplicationGroupList> applicationGroupList = null;
	// チェック選択アプリケーショングループインデックス
	private String index = null;
	// フィルタリスト（フィルタエリア）
	private SelectFilterMasterList filterList = null;
	// アプリケーションリスト
	private List<SelectApplicationList4Search> applicationList = null;
    // 選択中アプリケーションリスト
	private List<SelectedAplInfo> selectedAplList = null;
	// 追加用フィルタリスト
	private List<SelectApplicationFilterList> filterList4Add = null;
	// 追加用グループリスト
	private List<SelectApplicationGroupList> groupList4Add = null;
	// 選択中アプリケーション名リスト（JSPのhiddenから取得用）
	private String[] selectAplList = null;
	// 選択中アプリケーションタイプリスト（JSPのhiddenから取得用）
	private String[] selectTypeList = null;
	// 画面遷移種別(登録/更新)
	private String displayType = null;
	// カテゴリーリスト
	private List<SeqAndNameInfo> categoryList = new ArrayList<SeqAndNameInfo>();
	// サブカテゴリリスト
	private List<SeqAndNameInfo> subCategoryList = new ArrayList<SeqAndNameInfo>();
	// テクノロジリスト
	private List<SeqAndNameInfo> technologyList = new ArrayList<SeqAndNameInfo>();
	// リスクリスト
	private List<RiskInfo> riskList = new ArrayList<RiskInfo>();
	// アプリケーションフィルタリスト
	private List<SelectApplicationFilterList> applicationFilterList = new  ArrayList<SelectApplicationFilterList>();
	// 検索ワード
	private String searchWord = null;
	private String message = null;

	public void reset(ActionMapping mapping, HttpServletRequest request){
		 message=null;
	}

	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : oldNameのGetterメソッド
	 * 機能概要 : oldNameを取得する。
	 * @return oldName
	 */
	public String getOldName() {
		return oldName;
	}
	/**
	 * メソッド名 : oldNameのSetterメソッド
	 * 機能概要 : oldNameをセットする。
	 * @param oldName
	 */
	public void setOldName(String oldName) {
		this.oldName = oldName;
	}
	/**
	 * メソッド名 : newNameのGetterメソッド
	 * 機能概要 : newNameを取得する。
	 * @return newName
	 */
	public String getNewName() {
		return newName;
	}
	/**
	 * メソッド名 : newNameのSetterメソッド
	 * 機能概要 : newNameをセットする。
	 * @param newName
	 */
	public void setNewName(String newName) {
		this.newName = newName;
	}
	/**
	 * メソッド名 : applicationGroupListのGetterメソッド
	 * 機能概要 : applicationGroupListを取得する。
	 * @return applicationGroupList
	 */
	public List<SelectApplicationGroupList> getApplicationGroupList() {
		return applicationGroupList;
	}
	/**
	 * メソッド名 : applicationGroupListのSetterメソッド
	 * 機能概要 : applicationGroupListをセットする。
	 * @param applicationGroupList
	 */
	public void setApplicationGroupList(List<SelectApplicationGroupList> applicationGroupList) {
		this.applicationGroupList = applicationGroupList;
	}
	/**
	 * メソッド名 : indexのGetterメソッド
	 * 機能概要 : indexを取得する。
	 * @return index
	 */
	public String getIndex() {
		return index;
	}
	/**
	 * メソッド名 : indexのSetterメソッド
	 * 機能概要 : indexをセットする。
	 * @param index
	 */
	public void setIndex(String index) {
		this.index = index;
	}
	/**
	 * メソッド名 : filterListのGetterメソッド
	 * 機能概要 : filterListを取得する。
	 * @return filterList
	 */
	public SelectFilterMasterList getFilterList() {
		return filterList;
	}
	/**
	 * メソッド名 : filterListのSetterメソッド
	 * 機能概要 : filterListをセットする。
	 * @param filterList
	 */
	public void setFilterList(SelectFilterMasterList filterList) {
		this.filterList = filterList;
	}
	/**
	 * メソッド名 : applicationListのGetterメソッド
	 * 機能概要 : applicationListを取得する。
	 * @return applicationList
	 */
	public List<SelectApplicationList4Search> getApplicationList() {
		return applicationList;
	}
	/**
	 * メソッド名 : applicationListのSetterメソッド
	 * 機能概要 : applicationListをセットする。
	 * @param applicationList
	 */
	public void setApplicationList(List<SelectApplicationList4Search> applicationList) {
		this.applicationList = applicationList;
	}
	/**
	 * メソッド名 : selectedAplListのGetterメソッド
	 * 機能概要 : selectedAplListを取得する。
	 * @return selectedAplList
	 */
	public List<SelectedAplInfo> getSelectedAplList() {
		return selectedAplList;
	}
	/**
	 * メソッド名 : selectedAplListのSetterメソッド
	 * 機能概要 : selectedAplListをセットする。
	 * @param selectedAplList
	 */
	public void setSelectedAplList(List<SelectedAplInfo> selectedAplList) {
		this.selectedAplList = selectedAplList;
	}
	/**
	 * メソッド名 : filterList4AddのGetterメソッド
	 * 機能概要 : filterList4Addを取得する。
	 * @return filterList4Add
	 */
	public List<SelectApplicationFilterList> getFilterList4Add() {
		return filterList4Add;
	}
	/**
	 * メソッド名 : filterList4AddのSetterメソッド
	 * 機能概要 : filterList4Addをセットする。
	 * @param filterList4Add
	 */
	public void setFilterList4Add(List<SelectApplicationFilterList> filterList4Add) {
		this.filterList4Add = filterList4Add;
	}
	/**
	 * メソッド名 : groupList4AddのGetterメソッド
	 * 機能概要 : groupList4Addを取得する。
	 * @return groupList4Add
	 */
	public List<SelectApplicationGroupList> getGroupList4Add() {
		return groupList4Add;
	}
	/**
	 * メソッド名 : groupList4AddのSetterメソッド
	 * 機能概要 : groupList4Addをセットする。
	 * @param groupList4Add
	 */
	public void setGroupList4Add(List<SelectApplicationGroupList> groupList4Add) {
		this.groupList4Add = groupList4Add;
	}
	/**
	 * メソッド名 : selectAplListのGetterメソッド
	 * 機能概要 : selectAplListを取得する。
	 * @return selectAplList
	 */
	public String[] getSelectAplList() {
		return selectAplList;
	}
	/**
	 * メソッド名 : selectAplListのSetterメソッド
	 * 機能概要 : selectAplListをセットする。
	 * @param selectAplList
	 */
	public void setSelectAplList(String[] selectAplList) {
		this.selectAplList = selectAplList;
	}
	/**
	 * メソッド名 : selectTypeListのGetterメソッド
	 * 機能概要 : selectTypeListを取得する。
	 * @return selectTypeList
	 */
	public String[] getSelectTypeList() {
		return selectTypeList;
	}
	/**
	 * メソッド名 : selectTypeListのSetterメソッド
	 * 機能概要 : selectTypeListをセットする。
	 * @param selectTypeList
	 */
	public void setSelectTypeList(String[] selectTypeList) {
		this.selectTypeList = selectTypeList;
	}
	/**
	 * メソッド名 : displayTypeのGetterメソッド
	 * 機能概要 : displayTypeを取得する。
	 * @return displayType
	 */
	public String getDisplayType() {
		return displayType;
	}
	/**
	 * メソッド名 : displayTypeのSetterメソッド
	 * 機能概要 : displayTypeをセットする。
	 * @param displayType
	 */
	public void setDisplayType(String displayType) {
		this.displayType = displayType;
	}
	/**
	 * メソッド名 : categoryListのGetterメソッド
	 * 機能概要 : categoryListを生成し、取得する。
	 * @param cnt
	 * @return categoryList
	 */
	public Object getCategoryList(int cnt) {

		while (this.categoryList.size() <= cnt){
			this.categoryList.add(new SeqAndNameInfo());
		}
		return this.categoryList.get(cnt);
	}

	/**
	 * メソッド名 : subCategoryListのGetterメソッド
	 * 機能概要 : subCategoryListを生成し、取得する。
	 * @param cnt
	 * @return subCategoryList
	 */
	public Object getSubCategoryList(int cnt) {

		while (this.subCategoryList.size() <= cnt){
			this.subCategoryList.add(new SeqAndNameInfo());
		}
		return this.subCategoryList.get(cnt);
	}

	/**
	 * メソッド名 : technologyListのGetterメソッド
	 * 機能概要 : technologyListをを生成し、取得する。
	 * @param cnt
	 * @return technologyList
	 */
	public Object getTechnologyList(int cnt) {

		while (this.technologyList.size() <= cnt){
			this.technologyList.add(new SeqAndNameInfo());
		}
		return this.technologyList.get(cnt);
	}

	/**
	 * メソッド名 : riskListのGetterメソッド
	 * 機能概要 : riskListをを生成し、取得する。
	 * @param cnt
	 * @return riskList
	 */
	public Object getRiskList(int cnt) {

		while (this.riskList.size() <= cnt){
			this.riskList.add(new RiskInfo());
		}
		return this.riskList.get(cnt);
	}
	/**
	 * メソッド名 : categoryListのGetterメソッド
	 * 機能概要 : categoryListを取得する。
	 * @return categoryList
	 */
	public Object[] getCategoryList() {
			return categoryList.toArray();
	}
	/**
	 * メソッド名 : categoryListのSetterメソッド
	 * 機能概要 : categoryListをセットする。
	 * @param categoryList
	 */
	public void setCategoryList(List<SeqAndNameInfo> categoryList) {
		this.categoryList = categoryList;
	}
	/**
	 * メソッド名 : subCategoryListのGetterメソッド
	 * 機能概要 : subCategoryListを取得する。
	 * @return subCategoryList
	 */
	public Object[] getSubCategoryList() {

		return subCategoryList.toArray();
	}
	/**
	 * メソッド名 : subCategoryListのSetterメソッド
	 * 機能概要 : subCategoryListをセットする。
	 * @param subCategoryList
	 */
	public void setSubCategoryList(List<SeqAndNameInfo> subCategoryList) {
		this.subCategoryList = subCategoryList;
	}
	/**
	 * メソッド名 : technologyListのGetterメソッド
	 * 機能概要 : technologyListを取得する。
	 * @return technologyList
	 */
	public Object[] getTechnologyList() {
		return technologyList.toArray();
	}
	/**
	 * メソッド名 : technologyListのSetterメソッド
	 * 機能概要 : technologyListをセットする。
	 * @param technologyList
	 */
	public void setTechnologyList(List<SeqAndNameInfo> technologyList) {
		this.technologyList = technologyList;
	}
	/**
	 * メソッド名 : riskListのGetterメソッド
	 * 機能概要 : riskListを取得する。
	 * @return riskList
	 */
	public Object[] getRiskList() {

		return riskList.toArray();
	}
	/**
	 * メソッド名 : riskListのSetterメソッド
	 * 機能概要 : riskListをセットする。
	 * @param riskList
	 */
	public void setRiskList(List<RiskInfo> riskList) {
		this.riskList = riskList;
	}
	/**
	 * メソッド名 : applicationFilterListのGetterメソッド
	 * 機能概要 : applicationFilterListを取得する。
	 * @return applicationFilterList
	 */
	public List<SelectApplicationFilterList> getApplicationFilterList() {
		return applicationFilterList;
	}
	/**
	 * メソッド名 : applicationFilterListのSetterメソッド
	 * 機能概要 : applicationFilterListをセットする。
	 * @param applicationFilterList
	 */
	public void setApplicationFilterList(List<SelectApplicationFilterList> applicationFilterList) {
		this.applicationFilterList = applicationFilterList;
	}
	/**
	 * メソッド名 : searchWordのGetterメソッド
	 * 機能概要 : searchWordを取得する。
	 * @return searchWord
	 */
	public String getSearchWord() {
		return searchWord;
	}
	/**
	 * メソッド名 : searchWordのSetterメソッド
	 * 機能概要 : searchWordをセットする。
	 * @param searchWord
	 */
	public void setSearchWord(String searchWord) {
		this.searchWord = searchWord;
	}
}
