/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectUrlFilterList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/06     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo;

import java.io.Serializable;
import java.util.List;

import jp.co.kddi.secgw.cuscon.common.vo.UrlFilterCategories;

/**
 * クラス名 : SelectUrlFilterList
 * 機能概要 :
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/06
 *          新規作成
 * @see
 */
public class SelectUrlFilterList implements Serializable{

	// シリアルバージョンID
	private static final long serialVersionUID = 1L;

	// シーケンス番号
	private int seqNo = 0;
	// Name
	private String name = null;
	// 説明
	private String comment = null;
	// ブロックリストサイト
	private String blockListSite = null;
	// ブロックリストサイト数
	private int blockListSiteCnt = 0;
	// ブロックサイト動作
	private String actionBlockListSite = null;
	// ブロックサイト動作（日本語）
	private String actionBlockListSiteWamei = null;
	// 許可リストサイト
	private String allowListSite = null;
	// 許可リストサイト数
	private int allowListSiteCnt = 0;
	// 許可カテゴリ
	private String alertCategory = null;
	// ブロックカテゴリ
	private String blockCategory = null;
	// オーバーライドカテゴリ
	private String continueCategory = null;
	// Webフィルタカテゴリ
	private List<UrlFilterCategories> categoriesList = null;

	/**
	 * メソッド名 : seqNoのGetterメソッド
	 * 機能概要 : seqNoを取得する。
	 * @return seqNo
	 */
	public int getSeqNo() {
		return seqNo;
	}
	/**
	 * メソッド名 : seqNoのSetterメソッド
	 * 機能概要 : seqNoをセットする。
	 * @param seqNo
	 */
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * メソッド名 : nameのGetterメソッド
	 * 機能概要 : nameを取得する。
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * メソッド名 : nameのSetterメソッド
	 * 機能概要 : nameをセットする。
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * メソッド名 : commentのGetterメソッド
	 * 機能概要 : commentを取得する。
	 * @return comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * メソッド名 : commentのSetterメソッド
	 * 機能概要 : commentをセットする。
	 * @param comment
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * メソッド名 : blockListSiteのGetterメソッド
	 * 機能概要 : blockListSiteを取得する。
	 * @return blockListSite
	 */
	public String getBlockListSite() {
		return blockListSite;
	}
	/**
	 * メソッド名 : blockListSiteのSetterメソッド
	 * 機能概要 : blockListSiteをセットする。
	 * @param blockListSite
	 */
	public void setBlockListSite(String blockListSite) {
		this.blockListSite = blockListSite;
	}
	/**
	 * メソッド名 : blockListSiteCntのGetterメソッド
	 * 機能概要 : blockListSiteCntを取得する。
	 * @return blockListSiteCnt
	 */
	public int getBlockListSiteCnt() {
		return blockListSiteCnt;
	}
	/**
	 * メソッド名 : blockListSiteCntのSetterメソッド
	 * 機能概要 : blockListSiteCntをセットする。
	 * @param blockListSiteCnt
	 */
	public void setBlockListSiteCnt(int blockListSiteCnt) {
		this.blockListSiteCnt = blockListSiteCnt;
	}
	/**
	 * メソッド名 : actionBlockListSiteのGetterメソッド
	 * 機能概要 : actionBlockListSiteを取得する。
	 * @return actionBlockListSite
	 */
	public String getActionBlockListSite() {
		return actionBlockListSite;
	}
	/**
	 * メソッド名 : actionBlockListSiteのSetterメソッド
	 * 機能概要 : actionBlockListSiteをセットする。
	 * @param actionBlockListSite
	 */
	public void setActionBlockListSite(String actionBlockListSite) {
		this.actionBlockListSite = actionBlockListSite;
	}

	/**
	 * メソッド名 : actionBlockListSiteWameiのGetterメソッド
	 * 機能概要 : actionBlockListSiteWameiを取得する。
	 * @return actionBlockListSiteWamei
	 */
	public String getActionBlockListSiteWamei() {
		return actionBlockListSiteWamei;
	}
	/**
	 * メソッド名 : actionBlockListSiteWameiのSetterメソッド
	 * 機能概要 : actionBlockListSiteWameiをセットする。
	 * @param actionBlockListSiteWamei
	 */
	public void setActionBlockListSiteWamei(String actionBlockListSiteWamei) {
		this.actionBlockListSiteWamei = actionBlockListSiteWamei;
	}

	/**
	 * メソッド名 : allowListSiteのGetterメソッド
	 * 機能概要 : allowListSiteを取得する。
	 * @return allowListSite
	 */
	public String getAllowListSite() {
		return allowListSite;
	}
	/**
	 * メソッド名 : allowListSiteのSetterメソッド
	 * 機能概要 : allowListSiteをセットする。
	 * @param allowListSite
	 */
	public void setAllowListSite(String allowListSite) {
		this.allowListSite = allowListSite;
	}
	/**
	 * メソッド名 : allowListSiteCntのGetterメソッド
	 * 機能概要 : allowListSiteCntを取得する。
	 * @return allowListSiteCnt
	 */
	public int getAllowListSiteCnt() {
		return allowListSiteCnt;
	}
	/**
	 * メソッド名 : allowListSiteCntのSetterメソッド
	 * 機能概要 : allowListSiteCntをセットする。
	 * @param allowListSiteCnt
	 */
	public void setAllowListSiteCnt(int allowListSiteCnt) {
		this.allowListSiteCnt = allowListSiteCnt;
	}
	/**
	 * メソッド名 : alertCategoryのGetterメソッド
	 * 機能概要 : alertCategoryを取得する。
	 * @return alertCategory
	 */
	public String getAlertCategory() {
		return alertCategory;
	}
	/**
	 * メソッド名 : alertCategoryのSetterメソッド
	 * 機能概要 : alertCategoryをセットする。
	 * @param alertCategory
	 */
	public void setAlertCategory(String alertCategory) {
		this.alertCategory = alertCategory;
	}
	/**
	 * メソッド名 : blockCategoryのGetterメソッド
	 * 機能概要 : blockCategoryを取得する。
	 * @return blockCategory
	 */
	public String getBlockCategory() {
		return blockCategory;
	}
	/**
	 * メソッド名 : blockCategoryのSetterメソッド
	 * 機能概要 : blockCategoryをセットする。
	 * @param blockCategory
	 */
	public void setBlockCategory(String blockCategory) {
		this.blockCategory = blockCategory;
	}
	/**
	 * メソッド名 : continueCategoryのGetterメソッド
	 * 機能概要 : continueCategoryを取得する。
	 * @return continueCategory
	 */
	public String getContinueCategory() {
		return continueCategory;
	}
	/**
	 * メソッド名 : continueCategoryのSetterメソッド
	 * 機能概要 : continueCategoryをセットする。
	 * @param continueCategory
	 */
	public void setContinueCategory(String continueCategory) {
		this.continueCategory = continueCategory;
	}
	/**
	 * メソッド名 : categoriesListのGetterメソッド
	 * 機能概要 : categoriesListを取得する。
	 * @return categoriesList
	 */
	public List<UrlFilterCategories> getCategoriesList() {
		return categoriesList;
	}
	/**
	 * メソッド名 : categoriesListのSetterメソッド
	 * 機能概要 : categoriesListをセットする。
	 * @param categoriesList
	 */
	public void setCategoriesList(List<UrlFilterCategories> categoriesList) {
		this.categoriesList = categoriesList;
	}
}
