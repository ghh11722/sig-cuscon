/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ServiceMasterList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/04     komakiys                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.common;

import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.common.CusconConst;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.common.vo.InputBase;
import jp.co.kddi.secgw.cuscon.fwsetting.common.vo.SelectObjectList;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.transaction.util.TransactionUtil;

/**
 * クラス名 : ServiceMasterList
 * 機能概要 : サービスマスタを取得する。
 * 備考 :
 * @author komakiys
 * @version 1.0 komakiys
 *          Created 2010/03/04
 *          新規作成
 * @see
 */
public class ServiceMasterList {

    private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.fwsetting.common.ServiceMasterList");
    // メッセージクラス
	private static MessageAccessor messageAccessor = null;

    /**
     * メソッド名 : コンストラクタ
     * 機能概要 : -
     */
    public ServiceMasterList() {
    	log.debug("ServiceMasterListコンストラクタ2処理開始");
    	log.debug("ServiceMasterListコンストラクタ2処理終了");
    }


    /**
	 * メソッド名 : getServiceMasterList
	 * 機能概要 : サービスマスタ、サービスオブジェクト、サービスグループオブジェクト
	 *            の一覧を取得する。
	 * @param vsysId Vsys-ID
	 * @return List<SelectObjectList> サービスマスタリスト
     * @throws Exception
	 */
	public List<SelectObjectList> getServiceMasterList(
			QueryDAO queryDAO, CusconUVO uvo, int generationNo) throws Exception {

		log.debug("getServiceMasterList処理開始:vsysId = " + uvo.getVsysId());
		// 検索条件を設定する。
    	InputBase base = new InputBase();
    	base.setVsysId(uvo.getVsysId());
    	base.setGenerationNo(generationNo);
    	base.setModFlg(CusconConst.DEL_NUM);

    	List<SelectObjectList> serviceMasterList = null;
    	try {
	    	serviceMasterList = queryDAO.executeForObjectList(
	    									"EditServiceBLogic-1", base);
    	} catch(Exception e) {
    		// トランザクションロールバック
    		TransactionUtil.setRollbackOnly();
    		// DBアクセスエラー
    		Object[] objectgArray = {e.getMessage()};
    		log.fatal(messageAccessor.getMessage("FZ011020", objectgArray, uvo));
    		throw e;
    	}

    	log.debug("getServiceMasterList処理終了:serviceMasterList.size() = " +
    												serviceMasterList.size());
    	return serviceMasterList;
	}
	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor msgAcc){
		log.debug("setMessageAccessor処理開始");
	    setMsgAcc(msgAcc);
	    log.debug("setMessageAccessor処理終了");
	}

	/**
	 * メソッド名 : setMsgAcc
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public static void setMsgAcc(MessageAccessor messageAccessor) {
		log.debug("setMsgAcc処理開始");
		ServiceMasterList.messageAccessor = messageAccessor;
		log.debug("setMsgAcc処理終了");
	}

	/**
	 * メソッド名 : getMessage
	 * 機能概要 : messageAccessorをゲットする。
	 * @return MessageAccessor
	 */
	public static MessageAccessor getMessage() {
		log.debug("getMessage処理");
		return messageAccessor;
	}
}
