/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : DescriptionFormatChkValidator.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2010/03/18     kinoshtt                初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.policysetting.form;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jp.terasoluna.fw.web.struts.form.MultiFieldValidator;

/**
 * クラス名 : DescriptionFormatChkValidator
 * 機能概要 : ポリシー名設定画面の備考のフォーマットチェックを行う。
 * 備考 :
 * @author kinoshtt
 * @version 1.0 kinoshtt
 *          Created 2010/03/18
 *          新規作成
 * @see
 */
public class DescriptionFormatChkValidator implements MultiFieldValidator {

	private static final String BLANK = "";

	/**
	 * メソッド名 :validate
	 * 機能概要 :ポリシー名設定画面の備考のフォーマットチェックを行う。
	 * @param description 備考
	 * @param name ポリシー名
	 * @return チェック結果
	 * @see jp.terasoluna.fw.web.struts.form.MultiFieldValidator#validate(java.lang.String, java.lang.String[])
	 */
	@Override

	public boolean validate(String description, String[] fields) {

		// ポリシー名を取得する。
		String name = fields[0];

		// ポリシー名、備考がともに入力されているか判定
        if (!(name == null || BLANK.equals(name)) && !(description == null || BLANK.equals(description))) {

    		// 備考が半角英数字または半角記号("(ダブルクォーテーション)は除外)のみで構成されていることをチェック
    		Pattern pattern1 = Pattern.compile("^[a-zA-Z0-9\\ !#-/:-@\\[-\\`\\{-\\~]+$");
    		Matcher matcher1 = pattern1.matcher(description);
    		if (matcher1.matches() == false) {
        		return false;
    		}
		}
		return true;
	}
}
