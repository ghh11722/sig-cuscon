/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AddUrlFilterOutput.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/06/06     ktakenaka@PROSITE       初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.urlfilters.dto;

import java.util.List;

import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.CommonSelectItem;
import jp.co.kddi.secgw.cuscon.fwsetting.objectsetting.common.vo.UrlFilterCategoryInfo;

/**
 * クラス名 : AddUrlFilterOutput
 * 機能概要 :
 * 備考 :
 * @author ktakenaka@PROSITE
 * @version 1.0 ktakenaka@PROSITE
 *          Created 2011/06/06
 *          新規作成
 * @see
 */
public class AddUrlFilterOutput {

	// メッセージ
	private String message = null;

	// Webフィルタカテゴリリスト
	List<UrlFilterCategoryInfo> categoryList = null;

	// アクションセレクトボックスリスト
	List<CommonSelectItem> blockActionList = null;

	// カテゴリアクションセレクトボックスリスト
	List<CommonSelectItem> categoryActionList = null;

	/**
	 * メソッド名 : messageのGetterメソッド
	 * 機能概要 : messageを取得する。
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * メソッド名 : messageのSetterメソッド
	 * 機能概要 : messageをセットする。
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * メソッド名 : categoryListのGetterメソッド
	 * 機能概要 : categoryListを取得する。
	 * @return categoryList
	 */
	public List<UrlFilterCategoryInfo> getCategoryList() {
		return categoryList;
	}

	/**
	 * メソッド名 : categoryListのSetterメソッド
	 * 機能概要 : categoryListをセットする。
	 * @param categoryList
	 */
	public void setCategoryList(List<UrlFilterCategoryInfo> categoryList) {
		this.categoryList = categoryList;
	}

	/**
	 * メソッド名 : blockActionListのGetterメソッド
	 * 機能概要 : blockActionListを取得する。
	 * @return blockActionList
	 */
	public List<CommonSelectItem> getBlockActionList() {
		return blockActionList;
	}
	/**
	 * メソッド名 : blockActionListのSetterメソッド
	 * 機能概要 : blockActionListをセットする。
	 * @param blockActionList
	 */
	public void setBlockActionList(List<CommonSelectItem> blockActionList) {
		this.blockActionList = blockActionList;
	}

	/**
	 * メソッド名 : categoryActionListのGetterメソッド
	 * 機能概要 : categoryActionListを取得する。
	 * @return categoryActionList
	 */
	public List<CommonSelectItem> getCategoryActionList() {
		return categoryActionList;
	}
	/**
	 * メソッド名 : categoryActionListのSetterメソッド
	 * 機能概要 : categoryActionListをセットする。
	 * @param categoryActionList
	 */
	public void setCategoryActionList(List<CommonSelectItem> categoryActionList) {
		this.categoryActionList = categoryActionList;
	}
}
