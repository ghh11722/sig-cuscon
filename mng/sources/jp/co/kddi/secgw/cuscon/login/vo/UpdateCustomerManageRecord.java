/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateCustomerManageRecord.java
 *
 * [変更履歴]
 * 日付        更新者       内容
 * 2010/03/04  h.kubo@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.login.vo;

import java.sql.Timestamp;

/**
 * クラス名 : UpdateCustomerManageRecord
 * 機能概要 :
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class UpdateCustomerManageRecord {

	// ログインID
	private String loginId = null;
	// 最終ログイン時刻
	private Timestamp lastLoginTime = null;

	/**
	 * メソッド名 : loginIdのGetterメソッド
	 * 機能概要 : loginIdを取得する。
	 * @return loginId ログインID
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * メソッド名 : loginIdのSetterメソッド
	 * 機能概要 : loginIdをセットする。
	 * @param loginId ログインID
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * メソッド名 : lastLoginTimeのGetterメソッド
	 * 機能概要 : lastLoginTimeを取得する。
	 * @return lastLoginTime 最終ログイン時刻
	 */
	public Timestamp getLastLoginTime() {
		return lastLoginTime;
	}

	/**
	 * メソッド名 : lastLoginTimeのSetterメソッド
	 * 機能概要 : lastLoginTimeをセットする。
	 * @param lastLoginTime 最終ログイン時刻
	 */
	public void setLastLoginTime(Timestamp lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

}
