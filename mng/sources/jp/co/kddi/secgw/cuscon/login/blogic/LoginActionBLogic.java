/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : LoginActionBLogic.java
 *
 * [変更履歴]
 * 日付        更新者       内容
 * 2010/03/04  h.kubo@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.login.blogic;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.login.dto.LoginInput;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.web.UserValueObject;
import jp.terasoluna.fw.web.struts.actions.BLogicAction;

/**
 * クラス名 : LoginActionBLogic
 * 機能概要 : 企業管理者ログイン処理を行うビジネスロジック
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class LoginActionBLogic extends BLogicAction<LoginInput> {
	// ログイン成功
	private static final String RESULT_SUCCESS = "success";

	/**
	 * メソッド名 : ビジネスロジックの実行後処理
	 * 機能概要 : ビジネスロジックで例外が発生しなかった場合のみ、実行される
	 * @param request リクエスト
	 * @param response レスポンス
	 * @param params パラメータ（JavaBean）
	 * @param result ビジネスロジック実行結果
	 * @throws Exception 予期しない例外
	 */
	public void postDoExecuteBLogic(HttpServletRequest request, HttpServletResponse response, LoginInput params, BLogicResult result) throws Exception {

		// ログイン処理が正常終了以外はセッションの解放/再取得を行わない。
		if (!result.getResultString().equals(RESULT_SUCCESS)) {
			return;
		}

		// セッション解放
		request.getSession(true).invalidate();

		// CusconUVO再取得
		CusconUVO cuscon_uvo = (CusconUVO) UserValueObject.createUserValueObject();

		// セッション再取得
		HttpSession session = request.getSession(true);
    }

}
