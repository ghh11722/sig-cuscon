/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AdminManageRecord.java
 *
 * [変更履歴]
 * 日付        更新者        内容
 * 2010/03/04  h.kubo@JCCH   初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.login.vo;

/**
 * クラス名 : AdminManageRecord
 * 機能概要 : KDDI運用者の取得に使用するレコード定義Bean（DB操作に使用）
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class AdminManageRecord {

	// 暗号化鍵
	private final String cryptKey = "kddi-secgw";
	// ログインID
	private String loginId = null;
	// パスワード
	private String loginPwd = null;
	// 初期パスワード
	private String loginInitPwd = null;
	// 権限フラグ
	private int grantFlg = 0;
	// 初期パスワード状態フラグ
	private int initPwdFlg = 0;

	/**
	 * メソッド名 : cryptKeyのGetterメソッド
	 * 機能概要 : cryptKeyを取得する。
	 * @return cryptKey 暗号化鍵
	 */
	public String getCryptKey() {
		return cryptKey;
	}

	/**
	 * メソッド名 : loginIdのGetterメソッド
	 * 機能概要 : loginIdを取得する。
	 * @return loginId ログインID
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * メソッド名 : loginIdのSetterメソッド
	 * 機能概要 : loginIdをセットする。
	 * @param loginId ログインID
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * メソッド名 : loginPwdのGetterメソッド
	 * 機能概要 : loginPwdを取得する。
	 * @return loginPwd パスワード
	 */
	public String getLoginPwd() {
		return loginPwd;
	}

	/**
	 * メソッド名 : loginPwdのSetterメソッド
	 * 機能概要 : loginPwdをセットする。
	 * @param loginPwd パスワード
	 */
	public void setLoginPwd(String loginPwd) {
		this.loginPwd = loginPwd;
	}

	/**
	 * メソッド名 : loginInitPwdのGetterメソッド
	 * 機能概要 : loginInitPwdを取得する。
	 * @return loginInitPwd 初期パスワード
	 */
	public String getLoginInitPwd() {
		return loginInitPwd;
	}

	/**
	 * メソッド名 : loginInitPwdのSetterメソッド
	 * 機能概要 : loginInitPwdをセットする。
	 * @param loginInitPwd 初期パスワード
	 */
	public void setLoginInitPwd(String loginInitPwd) {
		this.loginInitPwd = loginInitPwd;
	}

	/**
	 * メソッド名 : grantFlgのGetterメソッド
	 * 機能概要 : grantFlgを取得する。
	 * @return grantFlg 権限フラグ
	 */
	public int getGrantFlg() {
		return grantFlg;
	}

	/**
	 * メソッド名 : grantFlgのSetterメソッド
	 * 機能概要 : grantFlgをセットする。
	 * @param grantFlg 権限フラグ
	 */
	public void setGrantFlg(int grantFlg) {
		this.grantFlg = grantFlg;
	}

	/**
	 * メソッド名 : initPwdFlgのGetterメソッド
	 * 機能概要 : initPwdFlgを取得する。
	 * @return initPwdFlg 初期パスワード状態フラグ
	 */
	public int getInitPwdFlg() {
		return initPwdFlg;
	}

	/**
	 * メソッド名 : initPwdFlgのSetterメソッド
	 * 機能概要 : initPwdFlgをセットする。
	 * @param initPwdFlg 初期パスワード状態フラグ
	 */
	public void setInitPwdFlg(int initPwdFlg) {
		this.initPwdFlg = initPwdFlg;
	}

}