/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ExceptionLogoutBLogic.java
 *
 * [変更履歴]
 * 日付        更新者           内容
 * 2010/03/25  m.narikawa@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.login.blogic;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.login.dto.LogoutInput;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicResult;

/**
 * クラス名 : ExceptionLogoutBLogic
 * 機能概要 : 例外発生時のログアウト処理を行うビジネスロジック
 * 備考 :
 * @author m.narikawa@JCCH
 * @version 1.0 m.narikawa@JCCH
 *          Created 2010/03/25
 *          新規作成
 * @see
 */
public class ExceptionLogoutBLogic implements BLogic<LogoutInput> {
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(ExceptionLogoutBLogic.class);

	/**
	 * メソッド名 : 企業管理者ログアウト処理
	 * 機能概要 : ログアウト処理を行う
	 * @param param CusconUVOを保持したMap
	 * @return ログアウト処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(LogoutInput param) {

		log.debug("例外発生ログアウト処理");

		// 処理結果
		BLogicResult result = new BLogicResult();

		// UVOが無かった場合（現実的にはあり得ないが。。。）
		CusconUVO uvo = param.getUvo();
		if(uvo == null){
			result.setResultString("failure");
			return result;
		}

		// KDDI運用者か、企業管理者かを判定
		long grantFlag = uvo.getGrantFlag();
		if( (grantFlag == 1) || (grantFlag == 2) ){
			result.setResultString("admin");
		}
		else {
			result.setResultString("customer");
		}

		return result;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}
}
