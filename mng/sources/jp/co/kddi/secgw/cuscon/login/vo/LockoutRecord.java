/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : LockoutRecord.java
 *
 * [変更履歴]
 * 日付        更新者       内容
 * 2010/03/04  h.kubo@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.login.vo;

import java.sql.Timestamp;

/**
 * クラス名 : LockoutRecord
 * 機能概要 : ロックアウト管理に使用するレコード定義Bean（DB操作に使用）
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class LockoutRecord {

	// ログインID
	private String loginId = null;
	// ロックアウト失敗回数
	private int loginNgCnt = 0;
	// ログイン失敗時刻
	private Timestamp loginNgTime = null;
	// ロックアウト時刻
	private Timestamp lockoutTime = null;

	/**
	 * メソッド名 : loginIdのGetterメソッド
	 * 機能概要 : loginIdを取得する。
	 * @return loginId ログインID
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * メソッド名 : loginIdのSetterメソッド
	 * 機能概要 : loginIdをセットする。
	 * @param loginId ログインID
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * メソッド名 : loginNgCntのGetterメソッド
	 * 機能概要 : loginNgCntを取得する。
	 * @return loginNgCnt ロックアウト失敗回数
	 */
	public int getLoginNgCnt() {
		return loginNgCnt;
	}

	/**
	 * メソッド名 : loginNgCntのSetterメソッド
	 * 機能概要 : loginNgCntをセットする。
	 * @param loginNgCnt ロックアウト失敗回数
	 */
	public void setLoginNgCnt(int loginNgCnt) {
		this.loginNgCnt = loginNgCnt;
	}

	/**
	 * メソッド名 : loginNgTimeのGetterメソッド
	 * 機能概要 : loginNgTimeを取得する。
	 * @return loginNgTime ログイン失敗時刻
	 */
	public Timestamp getLoginNgTime() {
		return loginNgTime;
	}

	/**
	 * メソッド名 : loginNgTimeのSetterメソッド
	 * 機能概要 : loginNgTimeをセットする。
	 * @param loginNgTime ログイン失敗時刻
	 */
	public void setLoginNgTime(Timestamp loginNgTime) {
		this.loginNgTime = loginNgTime;
	}

	/**
	 * メソッド名 : lockoutTimeのGetterメソッド
	 * 機能概要 : lockoutTimeを取得する。
	 * @return lockoutTime ロックアウト時刻
	 */
	public Timestamp getLockoutTime() {
		return lockoutTime;
	}

	/**
	 * メソッド名 : lockoutTimeのSetterメソッド
	 * 機能概要 : lockoutTimeをセットする。
	 * @param lockoutTime ロックアウト時刻
	 */
	public void setLockoutTime(Timestamp lockoutTime) {
		this.lockoutTime = lockoutTime;
	}

	/**
	 * メソッド名 : LockoutRecordクラスのtoStringメソッド
	 * 機能概要 : LockoutRecordクラスの文字列表現を取得する。
	 * @return オブジェクトの文字列表現
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LockoutRecord [lockoutTime=" + lockoutTime + ", loginId=" + loginId + ", loginNgCnt=" + loginNgCnt + ", loginNgTime=" + loginNgTime + "]";
	}

}