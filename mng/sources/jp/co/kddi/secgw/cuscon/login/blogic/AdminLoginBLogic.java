/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AdminLoginBLogic.java
 *
 * [変更履歴]
 * 日付        更新者       内容
 * 2010/03/04  h.kubo@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.login.blogic;

import java.sql.Timestamp;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jp.co.kddi.secgw.cuscon.common.CusconMaintenanceMode;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.login.dto.LoginInput;
import jp.co.kddi.secgw.cuscon.login.dto.LoginOutput;
import jp.co.kddi.secgw.cuscon.login.vo.SystemInfoRecord;
import jp.co.kddi.secgw.cuscon.login.vo.AdminManageRecord;
import jp.co.kddi.secgw.cuscon.login.vo.SelectAdminManageRecordKey;
import jp.co.kddi.secgw.cuscon.login.vo.UpdateAdminManageRecord;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;
import jp.terasoluna.fw.web.UserValueObject;

/**
 * クラス名 : AdminLoginBLogic
 * 機能概要 : KDDI運用者ログイン処理ビジネスロジック
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class AdminLoginBLogic implements BLogic<LoginInput> {
	// DAO
	private QueryDAO queryDAO = null;
	private UpdateDAO updateDAO = null;
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(AdminLoginBLogic.class);
	// KDDI運用管理者(ログインID、パスワード、初期パスワード、権限フラグ、初期パスワードフラグ)
	private AdminManageRecord manage_rec = null;
	// セッション情報(UVO)
	private CusconUVO uvo = null;

	/**
	 * メソッド名 : KDDI運用者ログイン処理
	 * 機能概要 : ログインを行い、UVOにパラメータをセットする。
	 * @param param 入力されたログイン情報を保持したMap
	 * @return ログイン処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(LoginInput param) {

		log.debug("KDDI運用者ログイン処理");

		// 処理結果
		BLogicResult result = new BLogicResult();
		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		// パスワード変更日時
		Timestamp oldPwdTime = null;
		// ログイン数
		Integer count;
		// 入力されたログインID、パスワードの取得
		String loginId = param.getLoginId();
		String password = param.getPassword();

//		// 1) メンテナンスモードチェック
//		// システム情報(動作モード)を取得する
//		log.debug("1) メンテナンスモードチェック");
//		boolean mode = false;
//		try {
//			CusconMaintenanceMode cuscon_maintenance = new CusconMaintenanceMode(queryDAO);
//			mode = cuscon_maintenance.isMaintenanceMode();
//		} catch (Exception e) {
//			e.printStackTrace();
//			// トランザクションロールバック
//			TransactionUtil.setRollbackOnly();
//			// エラー : システム情報取得失敗
//			Object[] objectgArray = {e.getMessage()};
//			log.fatal(messageAccessor.getMessage("FK011001", objectgArray));
//			messages.add("message", new BLogicMessage("DK010001"));
//			result.setErrors(messages);
//			result.setResultString("failure");
//			return result;
//		}
//
//		// true:メンテナンス、false:通常
//		if (mode) {
//			// 準正常 : メンテナンスモード
//			log.info(messageAccessor.getMessage("IK011001", null));
//			messages.add("message", new BLogicMessage("DK010002"));
//			result.setErrors(messages);
//			result.setResultString("failure");
//			return result;
//		}
//		log.debug("1) OK");

		// 2) リクエスト解析
		log.debug("2) リクエスト解析...validatorで実現するので実装なし");

		// 3) KDDI運用者検索
		// KDDI運用者(ログインID、パスワード、初期パスワード、権限フラグ、初期パスワードフラグ)を取得する
		log.debug("3) KDDI運用者検索");
		try {
			SelectAdminManageRecordKey key = new SelectAdminManageRecordKey();
			key.setLoginId(loginId);
			manage_rec= queryDAO.executeForObject("AdminLoginBLogic-2", key, AdminManageRecord.class);
			// 検索Hitしない場合
			if (manage_rec == null) {
				// 準正常 : 該当データなし
				Object[] objectgArray = {loginId};
				log.info(messageAccessor.getMessage("IK011002", objectgArray));
				messages.add("message", new BLogicMessage("DK010004"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : KDDI運用者取得失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK011002", objectgArray));
			messages.add("message", new BLogicMessage("DK010003"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		log.debug("3) OK");

		// 4) パスワードチェック
		log.debug("4) パスワードチェック");
		if (!password.equals(manage_rec.getLoginPwd())) {
			// 準正常 : パスワード不一致
			Object[] objectgArray = {loginId};
			log.info(messageAccessor.getMessage("IK011003", objectgArray));
			messages.add("message", new BLogicMessage("DK010004"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		log.debug("4) OK");

		// 5) 多重ログインチェック
		// KDDI運用者セッション管理情報を取得する
		log.debug("5) 多重ログインチェック");
		try {
			count= queryDAO.executeForObject("AdminLoginBLogic-3", loginId, Integer.class);
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : KDDI運用者セッション管理情報取得失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK011003", objectgArray));
			messages.add("message", new BLogicMessage("DK010005"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		if (count.intValue() != 0) {
			// 準正常 : 多重ログイン
			Object[] objectgArray = {loginId};
			log.info(messageAccessor.getMessage("IK011004",objectgArray));
			messages.add("message", new BLogicMessage("DK010006"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		log.debug("5) OK");

		// 6) ログイン完了
		log.debug("6) ログイン完了");
		try {
			Timestamp t = new Timestamp(System.currentTimeMillis());
			UpdateAdminManageRecord info = new UpdateAdminManageRecord();
			info.setLoginId(loginId);
			info.setLastLoginTime(t);
			// KDDI運用者テーブルを更新する
			updateDAO.execute("AdminLoginBLogic-4", info);
			// KDDI運用者セッション管理テーブルに追加する
			updateDAO.execute("AdminLoginBLogic-5", loginId);
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : KDDI運用者テーブル更新失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK011004", objectgArray));
			messages.add("message", new BLogicMessage("DK010007"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		log.debug("6) OK");

		// 7) セッション開始
		log.debug("7) セッション開始");
		// UVOの生成
		uvo = (CusconUVO) UserValueObject.createUserValueObject();
		// ユーザIDをUVOにセットする。
		uvo.setLoginId(loginId);
		// ユーザ権限(NOC:1,TSC:2,企業管理者:3)をUVOにセットする。
		uvo.setGrantFlag(manage_rec.getGrantFlg());
		// Vsys-IDをUVOにセットする。
		uvo.setVsysId("");
		// PAログインIDをUVOにセットする。
		uvo.setPaLoginId(null);
		// 企業管理者ログインIDをUVOにセットする。
		uvo.setCustomerLoginId(null);
		// UVOを返却用のクラスに設定する。
		LoginOutput out = new LoginOutput();
		out.setUvo(uvo);
		log.debug("7) OK");

		// 8) 初回ログインチェック
		log.debug("8) 初回ログインチェック");
		if (manage_rec.getInitPwdFlg() == 1) {
			log.debug("8) 初期パスワード状態フラグ=1");
			Object[] objectgArray = {loginId};
			log.info(messageAccessor.getMessage("IK011005", objectgArray, uvo));
			// 初期パスワード状態フラグが初期状態の場合。
			// パスワード変更画面に遷移する。
			result.setResultObject(out);
			result.setResultString("invalid-password");
			return result;
		}
		log.debug("8) OK");

		// 9) パスワード有効期限チェック
		// KDDI運用者旧パスワード情報を取得する
		try {
			log.debug("9) パスワード有効期限チェック");
			oldPwdTime = queryDAO.executeForObject("AdminLoginBLogic-6", loginId, Timestamp.class);
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : KDDI運用者旧パスワード情報取得失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK011005", objectgArray));
			messages.add("message", new BLogicMessage("DK010008"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		if (oldPwdTime == null) {
			// エラー : 該当データなし
			log.fatal(messageAccessor.getMessage("FK011006",null));
			messages.add("message", new BLogicMessage("DK010009"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

		// システム定数マスタ(ログインパスワード有効期限)の取得
		SystemInfoRecord system_info_rec = null;
		try {
			log.debug("9) システム定数マスタ(ログインパスワード有効期限)の取得");
			system_info_rec = queryDAO.executeForObject("AdminLoginBLogic-1", null, SystemInfoRecord.class);
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : システム情報取得失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FK011007", objectgArray));
			messages.add("message", new BLogicMessage("DK010001"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		if (system_info_rec == null) {
			// エラー : 該当データなし
			log.fatal(messageAccessor.getMessage("FK011008", null));
			messages.add("message", new BLogicMessage("DK010001"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

		// 有効期限日時
		long expiry_time = system_info_rec.getLoginPwdExpiry() * 3600 * 24 * 1000L;
		Timestamp expiry_date = new Timestamp(oldPwdTime.getTime() + expiry_time);

		// 現在時刻
		Timestamp current_date = new Timestamp(System.currentTimeMillis());

		// 有効期限切れ
		if (!expiry_date.after(current_date)) {
			Object[] objectgArray = {loginId};
			log.info(messageAccessor.getMessage("IK011006", objectgArray, uvo));
			// パスワード変更画面に遷移する。
			result.setResultObject(out);
			result.setResultString("invalid-password");
			return result;
		}

		// 10) レスポンス返却
		log.debug("10) レスポンス返却");
		result.setResultObject(out);
		result.setResultString("success");
		Object[] objectgArray = {loginId};
		log.info(messageAccessor.getMessage("IK011007", objectgArray, uvo));

		return result;
	}

	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * 機能概要 : queryDAOを取得する。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : updateDAOのGetterメソッド
	 * 機能概要 : updateDAOを取得する。
	 * @return updateDAO DAO
	 */
	public UpdateDAO getUpdateDAO() {
		return updateDAO;
	}

	/**
	 * メソッド名 : updateDAOのSetterメソッド
	 * 機能概要 : updateDAOをセットする。
	 * @param updateDAO DAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		this.updateDAO = updateDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}
}
