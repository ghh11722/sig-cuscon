/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名	: CustomerLogoutBLogic.java
 *
 * [変更履歴]
 * 日付 	   更新者		内容
 * 2010/03/04  h.kubo@JCCH	初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.login.blogic;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.login.dto.LogoutInput;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicResult;

/**
 * クラス名 : CustomerLogoutBLogic
 * 機能概要 : 企業管理者ログアウト処理を行うビジネスロジック
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *			Created 2010/03/04
 *			新規作成
 */
public class CustomerLogoutBLogic implements BLogic<LogoutInput> {
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(CustomerLogoutBLogic.class);

	/**
	 * メソッド名 : 企業管理者ログアウト処理
	 * 機能概要 : ログアウト処理を行う
	 * @param param CusconUVOを保持したMap
	 * @return ログアウト処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(LogoutInput param) {

		log.debug("企業管理者ログアウト処理");

		CusconUVO uvo = param.getUvo();
		if(uvo != null){
/*↓MOD 2012/01/05 T.Suzuki(NOP) */
//			String loginId = uvo.getLoginId();
//			Object[] objectgArray = {loginId};
//			log.info(messageAccessor.getMessage("IC011012", objectgArray, param.getUvo()));
			String loginId = uvo.getLoginId();
			String vsysID = uvo.getVsysId();
			Object[] objectgArray = {loginId, vsysID};
			log.info(messageAccessor.getMessage("IC011012", objectgArray));
/*↑MOD 2012/01/05 T.Suzuki(NOP) */
		}

		// 処理結果
		BLogicResult result = new BLogicResult();
		result.setResultString("success");

		return result;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}
}
