/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectCustomerManageRecordKey.java
 *
 * [変更履歴]
 * 日付        更新者       内容
 * 2010/03/04  h.kubo@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.login.vo;

/**
 * クラス名 : SelectCustomerManageRecordKey
 * 機能概要 : 企業管理者に使用するレコード定義Bean（DB操作に使用）
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class SelectCustomerManageRecordKey {

	// 暗号化鍵
	private final String cryptKey = "kddi-secgw";
	// ログインID
	private String loginId = null;
	// FirewallID
	private String firewallId = null;

	/**
	 * メソッド名 : cryptKeyのGetterメソッド
	 * 機能概要 : cryptKeyを取得する。
	 * @return cryptKey 暗号化鍵
	 */
	public String getCryptKey() {
		return cryptKey;
	}

	/**
	 * メソッド名 : loginIdのGetterメソッド
	 * 機能概要 : loginIdを取得する。
	 * @return loginId ログインID
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * メソッド名 : loginIdのSetterメソッド
	 * 機能概要 : loginIdをセットする。
	 * @param loginId ログインID
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * メソッド名 : firewallIdのGetterメソッド
	 * 機能概要 : firewallIdを取得する。
	 * @return firewallId FirewallID
	 */
	public String getFirewallId() {
		return firewallId;
	}

	/**
	 * メソッド名 : firewallIdのSetterメソッド
	 * 機能概要 : firewallIdをセットする。
	 * @param firewallId FirewallID
	 */
	public void setFirewallId(String firewallId) {
		this.firewallId = firewallId;
	}

}
