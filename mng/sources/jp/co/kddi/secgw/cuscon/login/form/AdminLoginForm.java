/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : AdminLoginForm.java
 *
 * [変更履歴]
 * 日付        更新者       内容
 * 2010/03/04  h.kubo@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.login.form;

import jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx;

/**
 * クラス名 : AdminLoginForm
 * 機能概要 : KDDI運用者ログインで使用するアクションフォーム
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *          Created 2010/03/04
 *          新規作成
 * @see jp.terasoluna.fw.web.struts.form.ValidatorActionFormEx
 */
public class AdminLoginForm extends ValidatorActionFormEx {

	//
	private static final long serialVersionUID = 1L;
	// ログインID
	private String loginId = null;
	// パスワード
	private String password = null;

	/**
	 * メソッド名 : loginIdのGetterメソッド
	 * 機能概要 : loginIdを取得する。
	 * @return loginId ログインID
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * メソッド名 : loginIdのSetterメソッド
	 * 機能概要 : loginIdをセットする。
	 * @param loginId ログインID
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * メソッド名 : passwordのGetterメソッド
	 * 機能概要 : passwordを取得する。
	 * @return password パスワード
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * メソッド名 : passwordのSetterメソッド
	 * 機能概要 : passwordをセットする。
	 * @param password パスワード
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
