/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CustomerLoginBLogic.java
 *
 * [変更履歴]
 * 日付        更新者             内容
 * 2010/03/04  h.kubo@JCCH        初版作成
 * 2010/05/20  ktakenaka@PROSITE  T_Manage_Cにフィールド追加に伴う修正
 * 2013/09/01  kkato@PROSITE      URL-DBのVer表示,FQDN登録上限情報を追加
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.login.blogic;

import java.sql.Timestamp;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jp.co.kddi.secgw.cuscon.common.CusconMaintenanceMode;
import jp.co.kddi.secgw.cuscon.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.common.CusconFWSet;
import jp.co.kddi.secgw.cuscon.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.login.dto.LoginInput;
import jp.co.kddi.secgw.cuscon.login.dto.LoginOutput;
import jp.co.kddi.secgw.cuscon.login.vo.SystemInfoRecord;
import jp.co.kddi.secgw.cuscon.login.vo.CustomerManageRecord;
import jp.co.kddi.secgw.cuscon.login.vo.SelectCustomerManageRecordKey;
import jp.co.kddi.secgw.cuscon.login.vo.UpdateCustomerManageRecord;
import jp.co.kddi.secgw.cuscon.login.vo.LockoutRecord;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
import jp.terasoluna.fw.service.thin.BLogic;
import jp.terasoluna.fw.service.thin.BLogicMessage;
import jp.terasoluna.fw.service.thin.BLogicMessages;
import jp.terasoluna.fw.service.thin.BLogicResult;
import jp.terasoluna.fw.transaction.util.TransactionUtil;
import jp.terasoluna.fw.web.UserValueObject;

/**
 * クラス名 : CustomerLoginBLogic
 * 機能概要 : 企業管理者ログイン処理を行うビジネスロジック
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class CustomerLoginBLogic implements BLogic<LoginInput> {
	// DAO
	private QueryDAO queryDAO = null;
	private UpdateDAO updateDAO = null;
	// メッセージクラス
	private MessageAccessor messageAccessor = null;
	// ログクラス
	private static Log log = LogFactory.getLog(CustomerLoginBLogic.class);
	// 企業管理者(FireWall-ID、ログインID、パスワード、初期パスワード、Vsys-ID、PAログインID、PAログインパスワード、初期パスワードフラグ)
	private CustomerManageRecord customer_rec = null;
	// ロックアウト管理テーブル(ログイン失敗回数、ログイン失敗時刻、ロックアウト失敗時刻)
	private LockoutRecord lockout_rec = null;
	// セッション情報(UVO)
	private CusconUVO uvo = null;

	/**
	 * メソッド名 : 企業管理者ログイン処理
	 * 機能概要 : ログインを行い、UVOにパラメータをセットする。
	 * @param param 入力されたログイン情報を保持したMap
	 * @return ログイン処理結果を保持したBLogicResult
	 * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
	 */
	public BLogicResult execute(LoginInput param) {

		log.debug("企業管理者ログイン処理");

		// 処理結果
		BLogicResult result = new BLogicResult();
		// エラーメッセージ
		BLogicMessages messages = new BLogicMessages();
		// パスワード変更日時
		Timestamp oldPwdTime = null;
		// ログイン数
		Integer count;
		// 現在時刻
		Timestamp current_date = new Timestamp(System.currentTimeMillis());
		// 入力されたログインID、Firewall-ID、パスワードの取得
		String loginId = param.getLoginId();
		String filewallID = param.getFirewallId();
		String password = param.getPassword();

		// 1) メンテナンスモードチェック
		// システム情報(動作モード,PAアドレス)を取得する
		log.debug("1) メンテナンスモードチェック");
		boolean mode = false;
		try {
			CusconMaintenanceMode cuscon_maintenance = new CusconMaintenanceMode(queryDAO);
			mode = cuscon_maintenance.isMaintenanceMode();
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : システム情報取得失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FC011001", objectgArray));
			messages.add("message", new BLogicMessage("DC010001"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		// true:メンテナンス、false:通常
		if (mode) {
			// 準正常 : メンテナンスモード
			log.info(messageAccessor.getMessage("IC011001", null));
			messages.add("message", new BLogicMessage("DC010002"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

		// システム定数マスタ(システム定数マスタのログインパスワード有効期限、ログイン失敗回数リセット時間、ロックアウト維持時間、ロックアウト適用回数)を取得する
		SystemInfoRecord system_info_rec = null;

		try {
			system_info_rec = queryDAO.executeForObject("CustomerLoginBLogic-1", null, SystemInfoRecord.class);
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : システム情報取得失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FC011002", objectgArray));
			messages.add("message", new BLogicMessage("DC010001"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		if (system_info_rec == null) {
			// エラー : 該当データなし
			log.fatal(messageAccessor.getMessage("FC011003", null));
			messages.add("message", new BLogicMessage("DC010001"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		log.debug("1) OK");

		// 2) リクエスト解析
		// validatorで実現する
		log.debug("2) リクエスト解析...validatorで実現するので実装なし");

		// 3) 企業管理者検索
		// 企業管理者(FireWall-ID、ログインID、パスワード、初期パスワード、Vsys-ID、初期パスワードフラグ)を取得する
		log.debug("3) 企業管理者検索");
		try {
			SelectCustomerManageRecordKey key = new SelectCustomerManageRecordKey();
			key.setLoginId(loginId);
			key.setFirewallId(filewallID);
			customer_rec= queryDAO.executeForObject("CostomerLoginBLogic-2", key, CustomerManageRecord.class);
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : 企業管理者取得失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FC011004", objectgArray));
			messages.add("message", new BLogicMessage("DC010003"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		// 検索Hitしない場合
		if (customer_rec == null) {
			// 準正常 : 該当データなし
			Object[] objectgArray = {loginId, filewallID};
			log.info(messageAccessor.getMessage("IC011002", objectgArray));
			messages.add("message", new BLogicMessage("DC010004"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		log.debug("3) OK");

		// 4) パスワードチェック
		log.debug("4) パスワードチェック");
		if (!password.equals(customer_rec.getLoginPwd())) {

			// アカウントロック処理
			// 1) ロックアウト管理テーブル取得
			try {
				lockout_rec = queryDAO.executeForObject("CustomerLoginBLogic-9", loginId, LockoutRecord.class);
			} catch (Exception e) {
				e.printStackTrace();
				// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
				// エラー : ロックアウト管理テーブル取得失敗
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FC011005", objectgArray));
				messages.add("message", new BLogicMessage("DC010005"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}

			// ロックアウト管理テーブルなし
			if (lockout_rec == null) {
				// ロックアウト管理テーブルに追加する
				try {
			        updateDAO.execute("CustomerLoginBLogic-10", loginId);
				} catch (Exception e) {
					e.printStackTrace();
					// トランザクションロールバック
					TransactionUtil.setRollbackOnly();
					// エラー : ロックアウト管理テーブル登録失敗
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage("FC011006", objectgArray));
					messages.add("message", new BLogicMessage("DC010006"));
					result.setErrors(messages);
					result.setResultString("failure");
					return result;
				}
				// 準正常 : パスワード不一致
				Object[] objectgArray = {loginId, filewallID};
				log.info(messageAccessor.getMessage("IC011003", objectgArray));
				messages.add("message", new BLogicMessage("DC010004"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}

			// 2) リセット時間チェック
			long reset_time = system_info_rec.getLoginFailResetTime() * 60 * 1000L;
			Timestamp reset_date = new Timestamp(lockout_rec.getLoginNgTime().getTime() + reset_time);

			// リセット対象
			if (!reset_date.after(current_date)) {
				// ロックアウト管理テーブルを削除する
				try {
			        updateDAO.execute("CustomerLoginBLogic-5", loginId);
				} catch (Exception e) {
					e.printStackTrace();
					// トランザクションロールバック
					TransactionUtil.setRollbackOnly();
					// エラー : ロックアウト管理テーブル削除失敗
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage("FC011007", objectgArray));
					messages.add("message", new BLogicMessage("DC010007"));
					result.setErrors(messages);
					result.setResultString("failure");
					return result;
				}
				// 準正常 : パスワード不一致
				Object[] objectgArray = {loginId, filewallID};
				log.info(messageAccessor.getMessage("IC011004", objectgArray));
				messages.add("message", new BLogicMessage("DC010004"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}

			// 3) ログイン失敗回数チェック
			// しきい値以上
			if (lockout_rec.getLoginNgCnt() >= system_info_rec.getLockoutCnt()) {

				// ロックアウト管理テーブルにロックアウト時刻を登録する
				try {
			        updateDAO.execute("CustomerLoginBLogic-11", loginId);
				} catch (Exception e) {
					e.printStackTrace();
					// トランザクションロールバック
					TransactionUtil.setRollbackOnly();
					// エラー : ロックアウト管理テーブル更新失敗
					Object[] objectgArray = {e.getMessage()};
					log.fatal(messageAccessor.getMessage("FC011008", objectgArray));
					messages.add("message", new BLogicMessage("DC010008"));
					result.setErrors(messages);
					result.setResultString("failure");
					return result;
				}
				// 準正常 : パスワード不一致
				Object[] objectgArray = {loginId, filewallID};
				log.info(messageAccessor.getMessage("IC011005", objectgArray));
				messages.add("message", new BLogicMessage("DC010004"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}

			// 4) ログイン失敗回数設定
			// ロックアウト管理テーブルのログイン失敗回数を更新する
			lockout_rec.setLoginNgCnt(lockout_rec.getLoginNgCnt() + 1);
			try {
		        updateDAO.execute("CustomerLoginBLogic-12", lockout_rec);
			} catch (Exception e) {
				e.printStackTrace();
				// トランザクションロールバック
				TransactionUtil.setRollbackOnly();
				// エラー : ロックアウト管理テーブル更新失敗
				Object[] objectgArray = {e.getMessage()};
				log.fatal(messageAccessor.getMessage("FC011009", objectgArray));
				messages.add("message", new BLogicMessage("DC010008"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}
			// 準正常 : パスワード不一致
			Object[] objectgArray = {loginId, filewallID};
			log.info(messageAccessor.getMessage("IC011006", objectgArray));
			messages.add("message", new BLogicMessage("DC010004"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		log.debug("4) OK");

		// 5) 多重ログインチェック
		// 企業管理者セッション管理情報を取得する
		log.debug("5) 多重ログインチェック");
		try {
			count= queryDAO.executeForObject("CustomerLoginBLogic-3", loginId, Integer.class);
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : 企業管理者セッション管理情報取得失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FC011010", objectgArray));
			messages.add("message", new BLogicMessage("DC010009"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		if (count.intValue() != 0) {
			// 準正常 : 多重ログイン
			Object[] objectgArray = {loginId, filewallID};
			log.info(messageAccessor.getMessage("IC011007", objectgArray));
			messages.add("message", new BLogicMessage("DC010010"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		log.debug("5) OK");

		// 6) ロックアウト情報取得
		// ロックアウト管理テーブルを取得する
		log.debug("6) ロックアウト情報取得");
		try {
			lockout_rec = queryDAO.executeForObject("CustomerLoginBLogic-4", loginId, LockoutRecord.class);
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : ロックアウト管理テーブル取得失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FC011011", objectgArray));
			messages.add("message", new BLogicMessage("DC010005"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		log.debug("6) OK");

		// 7) ロックアウト時間チェック
		log.debug("7) ロックアウト時間チェック");
		if (lockout_rec != null) {
			// ロックアウト時間
			long lockout_time = system_info_rec.getLockoutTime() * 60 * 1000L;
			Timestamp lockout_date = new Timestamp(lockout_rec.getLockoutTime().getTime() + lockout_time);
			if (lockout_date.after(current_date)) {
				// 準正常 : ロックアウト
				Object[] objectgArray = {loginId, filewallID};
				log.info(messageAccessor.getMessage("IC011008", objectgArray));
				messages.add("message", new BLogicMessage("DC010011"));
				result.setErrors(messages);
				result.setResultString("failure");
				return result;
			}
		}
		log.debug("7) OK");

		// 8) ロックアウト情報削除
		// ロックアウト管理テーブルを削除する
		log.debug("8) ロックアウト情報削除");
		try {
			updateDAO.execute("CustomerLoginBLogic-5", loginId);
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : ロックアウト管理テーブル削除失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FC011012", objectgArray));
			messages.add("message", new BLogicMessage("DC010007"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		log.debug("8) OK");

		// 9) ログイン完了
		log.debug("9) ログイン完了");
		try {
			Timestamp t = new Timestamp(System.currentTimeMillis());
			UpdateCustomerManageRecord info = new UpdateCustomerManageRecord();
			info.setLoginId(loginId);
			info.setLastLoginTime(t);
			// 企業管理者テーブルを更新する
			updateDAO.execute("CustomerLoginBLogic-6", info);
			// 企業管理者セッション管理テーブルに登録する
			updateDAO.execute("CustomerLoginBLogic-7", loginId);
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : 企業管理者テーブル更新失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FC011013", objectgArray));
			messages.add("message", new BLogicMessage("DC010012"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		log.debug("9) OK");

		// 10) セッション開始
		log.debug("10) セッション開始");
		// UVOの生成
		uvo = (CusconUVO) UserValueObject.createUserValueObject();
		// ユーザIDをUVOにセットする。
		uvo.setLoginId(loginId);
		// ユーザ権限(NOC:1,TSC:2,企業管理者:3)をUVOにセットする。
		uvo.setGrantFlag(3);
		// Vsys-IDをUVOにセットする。
		uvo.setVsysId(customer_rec.getVsysId());
		// PAログインIDをUVOにセットする。
		uvo.setPaLoginId(customer_rec.getPaLoginId());
		// PAログインパスワードをUVOにセットする。
		uvo.setPaPasswd(customer_rec.getLoginInitPwd());
		// 企業管理者ログインIDをUVOにセットする。
		uvo.setCustomerLoginId(loginId);
		// 20110520 ktakenaka@PROSITE add start
		// Webフィルタ利用フラグをUVOにセットする。
		uvo.setUrlFilteringFlg(customer_rec.getUrlFilteringFlg());
		// DMZ利用フラグをUVOにセットする。
		uvo.setDmzFlg(customer_rec.getDmzFlg());
		// Webフィルタプロファイル登録上限をUVOにセットする。
		uvo.setUrlProfileMax(customer_rec.getUrlProfileMax());
		// BlockList登録上限をUVOにセットする。
		uvo.setBlocklistMax(customer_rec.getBlocklistMax());
		// AllowList登録上限をUVOにセットする。
		uvo.setAllowlistMax(customer_rec.getAllowlistMax());
		// 20110520 ktakenaka@PROSITE add start
		// 20130901 kkato@PROSITE add start
		uvo.setUrlDbVersion(customer_rec.getUrlDbVersion());
		uvo.setAddressFqdnMax(customer_rec.getAddressFqdnMax());
		// 20130901 kkato@PROSITE add end

		// UVOを返却用のクラスに設定する。
		LoginOutput out = new LoginOutput();
		out.setUvo(uvo);
		log.debug("10) OK");

//		// 11) ポリシー取得
//		log.debug("11) ポリシー取得");
//		CusconFWSet cuscon_fw = new CusconFWSet();
//		try {
//			cuscon_fw.setFWInfo(updateDAO, queryDAO, uvo);
//		} catch (Exception e) {
//			e.printStackTrace();
//			// トランザクションロールバック
//			TransactionUtil.setRollbackOnly();
//			// エラー : ポリシー取得失敗
//			Object[] objectgArray = {e.getMessage()};
//			log.fatal(messageAccessor.getMessage("FC011016", objectgArray));
//			messages.add("message", new BLogicMessage("DC010017"));
//			result.setErrors(messages);
//			result.setResultString("failure");
//			return result;
//		}
//		log.debug("11) OK");

		// 11) ポリシー取得
		log.debug("11) ポリシー取得");
		CusconFWSet cuscon_fw = new CusconFWSet();
		try {
			cuscon_fw.setFWInfo(updateDAO, queryDAO, uvo);
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : ポリシー取得失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FC011016", objectgArray));
			messages.add("message", new BLogicMessage("DC010017"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		log.debug("11) OK");
//

		// 12) 初回ログインチェック
		log.debug("12) 初回ログインチェック");
		// 初期パスワード状態フラグが否初期状態の場合。
		if (customer_rec.getInitPwdFlg() == 1) {
			Object[] objectgArray = {loginId, filewallID};
			log.info(messageAccessor.getMessage("IC011009", objectgArray));
			// パスワード変更画面に遷移する。
			result.setResultObject(out);
			result.setResultString("invalid-password");
			return result;
		}
		log.debug("12) OK");

		// 13) パスワード有効期限チェック
		// 企業運用者旧パスワード情報を取得する
		try {
			oldPwdTime = queryDAO.executeForObject("CustomerLoginBLogic-8", loginId, Timestamp.class);
		} catch (Exception e) {
			e.printStackTrace();
			// トランザクションロールバック
			TransactionUtil.setRollbackOnly();
			// エラー : KDDI運用者旧パスワード情報取得失敗
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FC011014", objectgArray));
			messages.add("message", new BLogicMessage("DC010013"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}
		if (oldPwdTime == null) {
			// エラー : 該当データなし
			log.fatal(messageAccessor.getMessage("FC011015", null));
			messages.add("message", new BLogicMessage("DC010014"));
			result.setErrors(messages);
			result.setResultString("failure");
			return result;
		}

		// 有効期限日時
		long expiry_time = system_info_rec.getLoginPwdExpiry() * 3600 * 24 * 1000L;
		Timestamp expiry_date = new Timestamp(oldPwdTime.getTime() + expiry_time);
		// 有効期限切れ
		if (!expiry_date.after(current_date)) {
			Object[] objectgArray = {loginId, filewallID};
			log.info(messageAccessor.getMessage("IC011010", objectgArray));
			// パスワード変更画面に遷移する。
			result.setResultObject(out);
			result.setResultString("invalid-password");
			return result;
		}

		// 14) レスポンス返却
		result.setResultObject(out);
		result.setResultString("success");
		Object[] objectgArray = {loginId, filewallID};
		log.info(messageAccessor.getMessage("IC011011", objectgArray));

		return result;
	}

	/**
	 * メソッド名 : queryDAOのGetterメソッド
	 * 機能概要 : queryDAOを取得する。
	 * @return queryDAO DAO
	 */
	public QueryDAO getQueryDAO() {
		return queryDAO;
	}

	/**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO DAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		this.queryDAO = queryDAO;
	}

	/**
	 * メソッド名 : updateDAOのGetterメソッド
	 * 機能概要 : updateDAOを取得する。
	 * @return updateDAO DAO
	 */
	public UpdateDAO getUpdateDAO() {
		return updateDAO;
	}

	/**
	 * メソッド名 : updateDAOのSetterメソッド
	 * 機能概要 : updateDAOをセットする。
	 * @param updateDAO DAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		this.updateDAO = updateDAO;
	}

	/**
	 * メソッド名 : messageAccessorのGetterメソッド
	 * 機能概要 : messageAccessorを取得する。
	 * @return messageAccessor メッセージ
	 */
	public MessageAccessor getMessageAccessor() {
		return messageAccessor;
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor メッセージ
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}
}
