/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SystemInfoRecord.java
 *
 * [変更履歴]
 * 日付        更新者       内容
 * 2010/03/04  h.kubo@JCCH  初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.login.vo;

/**
 * クラス名 : SystemInfoRecord
 * 機能概要 : システム情報に使用するレコード定義Bean（DB操作に使用）
 * 備考 :
 * @author h.kubo@JCCH
 * @version 1.0 h.kubo@JCCH
 *          Created 2010/03/04
 *          新規作成
 */
public class SystemInfoRecord {

	// 動作モード
	private int mode = 0;
	// ログインパスワード有効期限
	private int loginPwdExpiry;
	// ログイン失敗回数リセット時間(t1)
	private int loginFailResetTime;
	// ロックアウト維持時間(t2)
	private int lockoutTime;
	// ロックアウト適用回数
	private int lockoutCnt;

	/**
	 * メソッド名 : modeのGetterメソッド
	 * 機能概要 : modeを取得する。
	 * @return mode 動作モード
	 */
	public int getMode() {
		return mode;
	}

	/**
	 * メソッド名 : modeのSetterメソッド
	 * 機能概要 : modeをセットする。
	 * @param mode 動作モード
	 */
	public void setMode(int mode) {
		this.mode = mode;
	}

	/**
	 * メソッド名 : loginPwdExpiryのGetterメソッド
	 * 機能概要 : loginPwdExpiryを取得する。
	 * @return loginPwdExpiry ログインパスワード有効期限
	 */
	public int getLoginPwdExpiry() {
		return loginPwdExpiry;
	}

	/**
	 * メソッド名 : loginPwdExpiryのSetterメソッド
	 * 機能概要 : loginPwdExpiryをセットする。
	 * @param loginPwdExpiry ログインパスワード有効期限
	 */
	public void setLoginPwdExpiry(int loginPwdExpiry) {
		this.loginPwdExpiry = loginPwdExpiry;
	}

	/**
	 * メソッド名 : loginFailResetTimeのGetterメソッド
	 * 機能概要 : loginFailResetTimeを取得する。
	 * @return loginFailResetTime ログイン失敗回数リセット時間(t1)
	 */
	public int getLoginFailResetTime() {
		return loginFailResetTime;
	}

	/**
	 * メソッド名 : loginFailResetTimeのSetterメソッド
	 * 機能概要 : loginFailResetTimeをセットする。
	 * @param loginFailResetTime ログイン失敗回数リセット時間(t1)
	 */
	public void setLoginFailResetTime(int loginFailResetTime) {
		this.loginFailResetTime = loginFailResetTime;
	}

	/**
	 * メソッド名 : lockoutTimeのGetterメソッド
	 * 機能概要 : lockoutTimeを取得する。
	 * @return lockoutTime ロックアウト維持時間(t2)
	 */
	public int getLockoutTime() {
		return lockoutTime;
	}

	/**
	 * メソッド名 : lockoutTimeのSetterメソッド
	 * 機能概要 : lockoutTimeをセットする。
	 * @param lockoutTime ロックアウト維持時間(t2)
	 */
	public void setLockoutTime(int lockoutTime) {
		this.lockoutTime = lockoutTime;
	}

	/**
	 * メソッド名 : lockoutCntのGetterメソッド
	 * 機能概要 : lockoutCntを取得する。
	 * @return lockoutCnt ロックアウト適用回数
	 */
	public int getLockoutCnt() {
		return lockoutCnt;
	}

	/**
	 * メソッド名 : lockoutCntのSetterメソッド
	 * 機能概要 : lockoutCntをセットする。
	 * @param lockoutCnt ロックアウト適用回数
	 */
	public void setLockoutCnt(int lockoutCnt) {
		this.lockoutCnt = lockoutCnt;
	}

}
