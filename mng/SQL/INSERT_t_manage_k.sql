DELETE FROM t_manage_k;

INSERT INTO t_manage_k ( login_id, login_pwd, grant_flg, login_init_pwd, last_login_time, init_pwd_flg ) VALUES ( 'ope',pgp_sym_encrypt('%Segw!noc', 'kddi-secgw'),1,pgp_sym_encrypt('%Segw!noc', 'kddi-secgw'),current_timestamp,1);
INSERT INTO t_manage_k ( login_id, login_pwd, grant_flg, login_init_pwd, last_login_time, init_pwd_flg ) VALUES ( 'ope2',pgp_sym_encrypt('%Segw!noc', 'kddi-secgw'),1,pgp_sym_encrypt('%Segw!noc', 'kddi-secgw'),current_timestamp,1);
INSERT INTO t_manage_k ( login_id, login_pwd, grant_flg, login_init_pwd, last_login_time, init_pwd_flg ) VALUES ( 'tsc',pgp_sym_encrypt('#Front2g', 'kddi-secgw'),2,pgp_sym_encrypt('#Front2g', 'kddi-secgw'),current_timestamp,1);
INSERT INTO t_manage_k ( login_id, login_pwd, grant_flg, login_init_pwd, last_login_time, init_pwd_flg ) VALUES ( 'sdg',pgp_sym_encrypt('#Svdeskg', 'kddi-secgw'),2,pgp_sym_encrypt('#Svdeskg', 'kddi-secgw'),current_timestamp,1);
INSERT INTO t_manage_k ( login_id, login_pwd, grant_flg, login_init_pwd, last_login_time, init_pwd_flg ) VALUES ( 'tsc2',pgp_sym_encrypt('#Tscyobi', 'kddi-secgw'),2,pgp_sym_encrypt('#Tscyobi', 'kddi-secgw'),current_timestamp,1);
INSERT INTO t_manage_k ( login_id, login_pwd, grant_flg, login_init_pwd, last_login_time, init_pwd_flg ) VALUES ( 'tsc3',pgp_sym_encrypt('#Tscyobi', 'kddi-secgw'),2,pgp_sym_encrypt('#Tscyobi', 'kddi-secgw'),current_timestamp,1);
INSERT INTO t_manage_k ( login_id, login_pwd, grant_flg, login_init_pwd, last_login_time, init_pwd_flg ) VALUES ( 'tsc4',pgp_sym_encrypt('#Tscyobi', 'kddi-secgw'),2,pgp_sym_encrypt('#Tscyobi', 'kddi-secgw'),current_timestamp,1);
INSERT INTO t_manage_k ( login_id, login_pwd, grant_flg, login_init_pwd, last_login_time, init_pwd_flg ) VALUES ( 'tsc5',pgp_sym_encrypt('#Tscyobi', 'kddi-secgw'),2,pgp_sym_encrypt('#Tscyobi', 'kddi-secgw'),current_timestamp,1);
INSERT INTO t_manage_k ( login_id, login_pwd, grant_flg, login_init_pwd, last_login_time, init_pwd_flg ) VALUES ( 'sldev01',pgp_sym_encrypt('sl$$:$01', 'kddi-secgw'),2,pgp_sym_encrypt('sl$$:$01', 'kddi-secgw'),current_timestamp,1);
