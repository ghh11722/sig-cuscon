CREATE OR REPLACE FUNCTION T_TopAppSummary_insert(
		varchar,
		varchar,
		varchar,
		varchar,
		varchar,
		varchar
) RETURNS Integer AS $$

declare
		A_VsysID alias for $1;
		A_TargetDate alias for $2;
		A_App alias for $3;
		A_RiskOfApp alias for $4;
		A_Bytes alias for $5;
		A_Sessions alias for $6;
begin
	   insert into T_TopAppSummary (
			   VSYS_ID,
			   TARGET_DATE,
			   APP,
			   RISK_OF_APP,
			   BYTES,
			   SESSIONS
	   ) values (
			   A_VsysID,
			   to_timestamp(A_TargetDate,'YYYY/MM/DD'),
			   A_App,
			   A_RiskOfApp,
			   A_Bytes,
			   A_Sessions
	   );
	   return 0;
end;

$$ LANGUAGE 'plpgsql' VOLATILE;
