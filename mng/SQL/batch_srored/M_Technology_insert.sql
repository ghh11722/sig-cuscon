CREATE OR REPLACE FUNCTION M_Technology_insert(varchar) RETURNS integer AS $$
DECLARE
 A_Technology ALIAS FOR $1;
 myrec integer;
BEGIN
   insert into M_Technology(Technology) values(A_Technology);
   select seq_no into myrec from M_Technology where Technology = A_Technology;
   return myrec as seq_no;
END;
$$ LANGUAGE 'plpgsql' VOLATILE;