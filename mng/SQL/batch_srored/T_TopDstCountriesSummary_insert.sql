CREATE OR REPLACE FUNCTION M_Category_insert(varchar) RETURNS integer AS $$
DECLARE
 A_Category ALIAS FOR $1;
 myrec integer;
BEGIN
   insert into M_Category(Category) values(A_Category);
   select seq_no into myrec from M_Category where Category = A_Category;
 RETURN myrec as seq_no;
END;
$$ LANGUAGE 'plpgsql' VOLATILE;