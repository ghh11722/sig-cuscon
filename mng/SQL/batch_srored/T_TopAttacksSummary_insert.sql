CREATE OR REPLACE FUNCTION T_TopAttacksSummary_insert(
		varchar,
		varchar,
		varchar,
		varchar,
		varchar,
		varchar,
		varchar
) RETURNS Integer AS $$

declare
		A_VsysID alias for $1;
		A_TargetDate alias for $2;
		A_Tid alias for $3;
		A_ServerityOfThreadID alias for $4;
		A_ThreadID alias for $5;
		A_ThreatSubType alias for $6;
		A_Count alias for $7;
begin
	   insert into T_TopAttacksSummary (
			   VSYS_ID,
			   TARGET_DATE,
			   TID,
			   SEVERITY_OF_THREATID,
			   THREATID,
			   THREAT_SUB_TYPE,
			   COUNT
	   ) values (
			   A_VsysID,
			   to_timestamp(A_TargetDate,'YYYY/MM/DD'),
  			   A_Tid,
  			   A_ServerityOfThreadID,
  			   A_ThreadID,
  			   A_ThreatSubType,
  			   A_Count
	   );
	   return 0;
end;

$$ LANGUAGE 'plpgsql' VOLATILE;
