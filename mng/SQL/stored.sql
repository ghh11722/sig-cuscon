-- Function: m_applications_insert(character varying, integer, integer, numeric, integer)

-- DROP FUNCTION m_applications_insert(character varying, integer, integer, numeric, integer);

CREATE OR REPLACE FUNCTION m_applications_insert(character varying, integer, integer, numeric, integer)
  RETURNS integer AS
$BODY$
DECLARE
 A_Name ALIAS FOR $1;
 A_Category_Seq_No ALIAS FOR $2;
 A_SubCategory_Seq_No ALIAS FOR $3;
 A_Risk ALIAS FOR $4;
 A_Technology_Seq_No ALIAS FOR $5;
BEGIN
  insert into M_Applications
   (NAME,
	CATEGORY_SEQ_NO,
	SUB_CATEGORY_SEQ_NO,
	RISk,
	TECHNOLOGY_SEQ_NO)
  values(A_Name,
		 A_Category_Seq_No,
		 A_SubCategory_Seq_No,
		 A_Risk,
		 A_Technology_Seq_No);
 RETURN 0;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE;
-- Function: m_applications_update(character varying, integer, integer, numeric, integer)

-- DROP FUNCTION m_applications_update(character varying, integer, integer, numeric, integer);

CREATE OR REPLACE FUNCTION m_applications_update(character varying, integer, integer, numeric, integer)
  RETURNS integer AS
$BODY$
DECLARE
 A_Name ALIAS FOR $1;
 A_Category_Seq_No ALIAS FOR $2;
 A_SubCategory_Seq_No ALIAS FOR $3;
 A_Risk ALIAS FOR $4;
 A_Technology_Seq_No ALIAS FOR $5;
BEGIN
UPDATE M_Applications
   SET CATEGORY_SEQ_NO = A_Category_Seq_No,
	   SUB_CATEGORY_SEQ_NO = A_SubCategory_Seq_No,
	   RISK = A_Risk,
	   TECHNOLOGY_SEQ_NO = A_Technology_Seq_No
 WHERE NAME = A_Name
   AND (CATEGORY_SEQ_NO <> A_Category_Seq_No OR
		SUB_CATEGORY_SEQ_NO <> A_SubCategory_Seq_No OR
		RISK <> A_Risk OR
		TECHNOLOGY_SEQ_NO <> A_Technology_Seq_No);
 RETURN 0;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE;
CREATE OR REPLACE FUNCTION M_Category_insert(varchar) RETURNS integer AS $$
DECLARE
 A_Category ALIAS FOR $1;
 myrec integer;
BEGIN
   select count(seq_no) into myrec from M_Category where Category = A_Category;
   IF myrec < 1 THEN
	 insert into M_Category(Category) values(A_Category);
   END IF;
 RETURN 0;
END;
$$ LANGUAGE 'plpgsql' VOLATILE;-- Function: m_services_delete(character varying)

-- DROP FUNCTION m_services_delete(character varying);

CREATE OR REPLACE FUNCTION m_services_delete(character varying)
  RETURNS integer AS
$BODY$
DECLARE
 A_Name ALIAS FOR $1;
BEGIN
DELETE FROM M_Services
 WHERE NAME = A_Name;
 RETURN 0;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE;
-- Function: m_services_insert(character varying, character varying, character varying)

-- DROP FUNCTION m_services_insert(character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION m_services_insert(character varying, character varying, character varying)
  RETURNS integer AS
$BODY$
DECLARE
 A_Name ALIAS FOR $1;
 A_Protocol ALIAS FOR $2;
 A_Port ALIAS FOR $3;
BEGIN
  insert into M_Services
   (NAME,
	PROTOCOL,
	PORT)
  values(A_Name,
		 A_Protocol,
		 A_Port);
 RETURN 0;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE;
-- Function: m_services_update(character varying, character varying, character varying)

-- DROP FUNCTION m_services_update(character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION m_services_update(character varying, character varying, character varying)
  RETURNS integer AS
$BODY$
DECLARE
 A_Name ALIAS FOR $1;
 A_Protocol ALIAS FOR $2;
 A_Port ALIAS FOR $3;
BEGIN
UPDATE M_Services
   SET PROTOCOL = A_Protocol,
	   PORT = A_Port
 WHERE NAME = A_Name
   AND (PROTOCOL <> A_Protocol OR
		PORT <> A_Port);
 RETURN 0;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE;
CREATE OR REPLACE FUNCTION M_SubCategory_insert(varchar) RETURNS integer AS $$
DECLARE
 A_SubCategory ALIAS FOR $1;
 myrec integer;
BEGIN
   select count(seq_no) into myrec from M_SubCategory where Sub_Category = A_SubCategory;
   IF myrec < 1 THEN
	 insert into M_SubCategory(Sub_Category) values(A_SubCategory);
   END IF;
 RETURN 0;
END;
$$ LANGUAGE 'plpgsql' VOLATILE;CREATE OR REPLACE FUNCTION M_Technology_insert(varchar) RETURNS integer AS $$
DECLARE
 A_Technology ALIAS FOR $1;
 myrec integer;
BEGIN
   select count(seq_no) into myrec from M_Technology where Technology = A_Technology;
   IF myrec < 1 THEN
	 insert into M_Technology(Technology) values(A_Technology);
   END IF;
 RETURN 0;
END;
$$ LANGUAGE 'plpgsql' VOLATILE;CREATE OR REPLACE FUNCTION T_TopAppSummary_insert(
		varchar,
		varchar,
		varchar,
		varchar,
		varchar,
		varchar
) RETURNS Integer AS $$

declare
		A_VsysID alias for $1;
		A_TargetDate alias for $2;
		A_App alias for $3;
		A_RiskOfApp alias for $4;
		A_Bytes alias for $5;
		A_Sessions alias for $6;
begin
	   insert into T_TopAppSummary (
			   VSYS_ID,
			   TARGET_DATE,
			   APP,
			   RISK_OF_APP,
			   BYTES,
			   SESSIONS
	   ) values (
			   A_VsysID,
			   to_timestamp(A_TargetDate,'YYYY/MM/DD'),
			   A_App,
			   A_RiskOfApp,
			   A_Bytes,
			   A_Sessions
	   );
	   return 0;
end;

$$ LANGUAGE 'plpgsql' VOLATILE;
CREATE OR REPLACE FUNCTION T_TopAttacksSummary_insert(
		varchar,
		varchar,
		varchar,
		varchar,
		varchar,
		varchar,
		varchar
) RETURNS Integer AS $$

declare
		A_VsysID alias for $1;
		A_TargetDate alias for $2;
		A_Tid alias for $3;
		A_ServerityOfThreadID alias for $4;
		A_ThreadID alias for $5;
		A_ThreatSubType alias for $6;
		A_Count alias for $7;
begin
	   insert into T_TopAttacksSummary (
			   VSYS_ID,
			   TARGET_DATE,
			   TID,
			   SEVERITY_OF_THREATID,
			   THREATID,
			   THREAT_SUB_TYPE,
			   COUNT
	   ) values (
			   A_VsysID,
			   to_timestamp(A_TargetDate,'YYYY/MM/DD'),
  			   A_Tid,
  			   A_ServerityOfThreadID,
  			   A_ThreadID,
  			   A_ThreatSubType,
  			   A_Count
	   );
	   return 0;
end;

$$ LANGUAGE 'plpgsql' VOLATILE;
CREATE OR REPLACE FUNCTION T_TopDstCountriesSummary_insert(
		varchar,
		varchar,
		varchar,
		varchar,
		varchar
) RETURNS Integer AS $$

declare
		A_VsysID alias for $1;
		A_TargetDate alias for $2;
		A_DstLoc alias for $3;
		A_Bytes alias for $4;
		A_Sessions alias for $5;
begin
		insert into T_TopDstCountriesSummary (
			VSYS_ID,
			TARGET_DATE,
			DSTLOC,
			BYTES,
			SESSIONS
		) values (
			A_VsysID,
			to_timestamp(A_TargetDate,'YYYY/MM/DD'),
			A_DstLoc,
			A_Bytes,
			A_Sessions
		);
	return 0;
end;

$$ LANGUAGE 'plpgsql' VOLATILE;CREATE OR REPLACE FUNCTION T_TopDstSummary_insert(
		varchar,
		varchar,
		varchar,
		varchar,
		varchar,
		varchar,
		varchar
) RETURNS Integer AS $$

declare
		A_VsysID alias for $1;
		A_TargetDate alias for $2;
		A_Dst alias for $3;
		A_ResolvedDst alias for $4;
		A_DstUser alias for $5;
		A_Bytes alias for $6;
		A_Sessions alias for $7;
begin
		insert into T_TopDstSummary (
			VSYS_ID,
			TARGET_DATE,
			DST,
			RESOLVED_DST,
			DST_USER,
			BYTES,
			SESSIONS
		) values (
			A_VsysID,
			to_timestamp(A_TargetDate,'YYYY/MM/DD'),
			A_Dst,
			A_ResolvedDst,
			A_DstUser,
			A_Bytes,
			A_Sessions
		);
	return 0;
end;

$$ LANGUAGE 'plpgsql' VOLATILE;CREATE OR REPLACE FUNCTION T_TopRuleSummary_insert(
		varchar,
		varchar,
		varchar,
		varchar,
		varchar,
		varchar
) RETURNS Integer AS $$

declare
		A_VsysID alias for $1;
		A_TargetDate alias for $2;
		A_Serial alias for $3;
		A_Rule alias for $4;
		A_Bytes alias for $5;
		A_Sessions alias for $6;
begin
	   insert into T_TopRuleSummary (
			   VSYS_ID,
			   TARGET_DATE,
			   SERIAL,
			   RULE,
			   BYTES,
			   SESSIONS
	   ) values (
			   A_VsysID,
			   to_timestamp(A_TargetDate,'YYYY/MM/DD'),
			   A_Serial,
			   A_Rule,
			   A_Bytes,
			   A_Sessions
	   );
	   return 0;
end;

$$ LANGUAGE 'plpgsql' VOLATILE;
CREATE OR REPLACE FUNCTION T_TopSrcCountriesSummary_insert(
		varchar,
		varchar,
		varchar,
		varchar,
		varchar
) RETURNS Integer AS $$

declare
		A_VsysID alias for $1;
		A_TargetDate alias for $2;
		A_SrcLoc alias for $3;
		A_Bytes alias for $4;
		A_Sessions alias for $5;
begin
		insert into T_TopSrcCountriesSummary (
			VSYS_ID,
			TARGET_DATE,
			SRCLOC,
			BYTES,
			SESSIONS
		) values (
			A_VsysID,
			to_timestamp(A_TargetDate,'YYYY/MM/DD'),
			A_SrcLoc,
			A_Bytes,
			A_Sessions
		);
	return 0;
end;

$$ LANGUAGE 'plpgsql' VOLATILE;CREATE OR REPLACE FUNCTION T_TopSrcSummary_insert(
		varchar,
		varchar,
		varchar,
		varchar,
		varchar,
		varchar,
		varchar
) RETURNS Integer AS $$

declare
		A_VsysID alias for $1;
		A_TargetDate alias for $2;
		A_Src alias for $3;
		A_ResolvedSrc alias for $4;
		A_SrcUser alias for $5;
		A_Bytes alias for $6;
		A_Sessions alias for $7;
begin
		insert into T_TopSrcSummary (
			VSYS_ID,
			TARGET_DATE,
			SRC,
			RESOLVED_SRC,
			SRC_USER,
			BYTES,
			SESSIONS
		) values (
			A_VsysID,
			to_timestamp(A_TargetDate,'YYYY/MM/DD'),
			A_Src,
			A_ResolvedSrc,
			A_SrcUser,
			A_Bytes,
			A_Sessions
		);
	return 0;
end;

$$ LANGUAGE 'plpgsql' VOLATILE;