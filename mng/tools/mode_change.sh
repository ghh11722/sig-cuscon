#!/bin/bash
#*******************************************************************************
# * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
# * プログラム名 : KDDIセキュリティGWシステム カスコン
# * ファイル名   : mode_change.sh
# *
# * [変更履歴]
# * 日付       更新者            内容
# * 2010/02/24  m.narikawa@JCCH  初期版作成
# * 2010/03/02  T.Sutoh@JCCH     ログメッセージプロパティをパラメータへ追加
# * 2010/03/10  m.narikawaJCCH   Log4Jのプロパティファイルを指定
#*******************************************************************************

### 変数定義 ###
JAVA_HOME=/usr/java/default
BASE_DIR=/usr/local/cuscon-batch

MAIN_CLASS=jp.co.kddi.secgw.cuscon.batch.ModeSet
CONFIG_FILE=$BASE_DIR/conf/context.xml
MESSAGE_POPERTIES=$BASE_DIR/conf/shellmessage.properties
LOG4J_POPERTIES=$BASE_DIR/conf/log4j_batch.properties

### 実行環境設定 ###
export LANG="ja_JP.UTF-8"
export JAVA_HOME=$JAVA_HOME
export PATH=$JAVA_HOME/bin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

### クラスパス設定 ###
CLASSPATH=$BASE_DIR/lib

### ユーザライブラリをクラスパスに追加 ###
for file in `ls $BASE_DIR/lib`; do
  CLASSPATH=$CLASSPATH:$BASE_DIR/lib/$file
done


echo "メンテナンスモードを設定しますか？"
echo "  0.通常モード"
echo "  1.メンテナンスモード"
echo "  9.終了"
read mente_mode

case "$mente_mode" in
  "on" | "ON" | "On" | "1")
    mode_flg=1
    ;;
  "off" | "OFF" | "Off" | "0")
    mode_flg=0
    ;;
  *) 
    echo "終了"
    exit
    ;;
esac

### Java 実行 ###
java -Dlog4j.configuration=file://$LOG4J_POPERTIES -classpath $CLASSPATH $MAIN_CLASS "$CONFIG_FILE" "$MESSAGE_POPERTIES" $mode_flg 2> /dev/null
