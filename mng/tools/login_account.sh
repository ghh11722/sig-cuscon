#!/bin/sh
#
# Checked login status
#

#
SQL_FILE='/var/tmp/t_systeminfo.sql'
PSQL_COMMAND='/opt/powergres/bin/psql cuscondb -U postgres'
LOGIN_SELECT_C_SQL="select login_id, pgp_sym_decrypt(login_pwd::bytea,'kddi-secgw'::text) as password, firewall_id,vsys_id,last_login_time from t_manage_c order by vsys_id;"
LOGIN_SELECT_K_SQL="select login_id, pgp_sym_decrypt(login_pwd::bytea,'kddi-secgw'::text) as password, last_login_time from t_manage_k order by login_id;"

echo "どのパスワードを確認しますか？"
echo "  1.企業管理者"
echo "  2.KDDI管理者"
echo "  9.終了"
read loginstatus

if [ "$loginstatus" = "9" ]; then
    echo "終了"
    exit

elif [ "$loginstatus" = "1" ]; then
    # Create Sql File
    echo $LOGIN_SELECT_C_SQL > $SQL_FILE

elif [ "$loginstatus" = "2" ]; then
    # Create Sql File
    echo $LOGIN_SELECT_K_SQL > $SQL_FILE

else
    echo "終了"
    exit

fi
# Running SQL
$PSQL_COMMAND -f $SQL_FILE

# Delete Sql File
rm $SQL_FILE


