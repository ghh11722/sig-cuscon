#!/bin/sh
#
# Checked commit status
#

#
SQL_FILE='/var/tmp/t_commit.sql'
PSQL_COMMAND='/opt/powergres/bin/psql cuscondb -U postgres'
COMMIT_SELECT_SQL='select commitprocessing_flg as commit_status from t_commitprocessing;'
COMMIT_UPDATE_SQL1='update t_commitprocessing set commitprocessing_flg = 0;'
COMMIT_UPDATE_SQL2='update t_commitstatus set status = 1, end_time = now() where status = 2;'

#
echo "現在のコミット状態を表示します。"
# Create Sql File
echo $COMMIT_SELECT_SQL > $SQL_FILE

# Running SQL
$PSQL_COMMAND -f $SQL_FILE

# Delete Sql File
rm $SQL_FILE

echo "コミットフラグを解除しますか？(y/n)"
read commitstatus

if [ "$commitstatus" != "y" ]; then

    echo "解除を終了します。"
    exit
fi


# Create Sql File
echo $COMMIT_UPDATE_SQL1 > $SQL_FILE

# Running SQL
$PSQL_COMMAND -f $SQL_FILE

# Delete Sql File
rm $SQL_FILE

# Create Sql File
echo $COMMIT_UPDATE_SQL2 > $SQL_FILE

# Running SQL
$PSQL_COMMAND -f $SQL_FILE

# Delete Sql File
rm $SQL_FILE

exit



