#!/bin/sh
#
# Checked login status
#

#
SQL_FILE='/var/tmp/t_systeminfo.sql'
PSQL_COMMAND='/opt/powergres/bin/psql cuscondb -U postgres'
LOGIN_SELECT_C_SQL='select login_id from t_session_c;'
LOGIN_SELECT_K_SQL='select login_id from t_session_k;'
LOGIN_DELETE_C_SQL='delete from t_session_c'
LOGIN_DELETE_K_SQL='delete from t_session_k'

echo "どのログイン状態を確認しますか？"
echo "  1.企業管理者"
echo "  2.KDDI管理者"
echo "  9.終了"
read loginstatus

if [ "$loginstatus" = "9" ]; then
    echo "終了"
    exit

elif [ "$loginstatus" = "1" ]; then
    # Create Sql File
    echo $LOGIN_SELECT_C_SQL > $SQL_FILE

elif [ "$loginstatus" = "2" ]; then
    # Create Sql File
    echo $LOGIN_SELECT_K_SQL > $SQL_FILE

else
    echo "終了"
    exit

fi
# Running SQL
$PSQL_COMMAND -f $SQL_FILE

# Delete Sql File
rm $SQL_FILE


echo "セッションを解除しますか？"
echo "解除するログインIDを入力して下さい。（終了:q）"
read loginreset


if [ "$loginreset" = "q" ]; then

    echo "終了"
    exit
fi
if [ "$loginstatus" = "1" ]; then
    # Create Sql File
    echo $LOGIN_DELETE_C_SQL where login_id = \'$loginreset\'\; > $SQL_FILE

elif [ "$loginstatus" = "2" ]; then
    # Create Sql File
    echo $LOGIN_DELETE_K_SQL where login_id = \'$loginreset\'\; > $SQL_FILE

else
    echo "終了"
    exit

fi
# Running SQL
$PSQL_COMMAND -f $SQL_FILE

# Delete Sql File
rm $SQL_FILE

exit
