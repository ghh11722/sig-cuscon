#!/bin/sh
#
# Checked login status
#

#
SQL_FILE='/var/tmp/t_manage_k.sql'
PSQL_COMMAND='/opt/powergres/bin/psql cuscondb -U postgres'
LOGIN_SELECT_K_SQL="select login_id, pgp_sym_decrypt(login_pwd::bytea,'kddi-secgw'::text) as password, last_login_time from t_manage_k;"
LOGIN_INSERT_K_SQL="INSERT INTO t_manage_k ( login_id, login_pwd, grant_flg, login_init_pwd, last_login_time, init_pwd_flg )"

echo "KDDI管理者を表示します。"
# Create Sql File
echo $LOGIN_SELECT_K_SQL > $SQL_FILE

# Running SQL
$PSQL_COMMAND -f $SQL_FILE

# Delete Sql File
rm $SQL_FILE

echo "KDDI管理者アカウントを追加しますか？(y/n)"
read addaccount


if [ "$addaccount" != "y" ]; then

    echo "終了"
    exit
fi

# input login id
echo "ログインIDを入力して下さい。"
read add_loginid

# input password
echo "パスワードを入力して下さい。"
read add_passwd

# input grant
echo "権限を入力して下さい。"
echo " 1.NOC権限"
echo " 2.TSC権限"
read add_grant

if [ "$add_grant" != "1" ] && [ "$add_grant" != "2" ]; then
    echo "終了"
    exit
fi

# Create Sql File
echo $LOGIN_INSERT_K_SQL VALUES \( \'$add_loginid\',pgp_sym_encrypt\(\'$add_passwd\', \'kddi-secgw\'\),$add_grant,pgp_sym_encrypt\(\'$add_passwd\', \'kddi-secgw\'\),current_timestamp,1\)\; > $SQL_FILE

# Running SQL
$PSQL_COMMAND -f $SQL_FILE

# Delete Sql File
rm $SQL_FILE

exit
