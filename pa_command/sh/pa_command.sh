#!/bin/bash
#*******************************************************************************
# * Copyright(c) 2011 KDDI CORPORATION. All Rights Reserved.
# * プログラム名 : KDDIセキュリティGWシステム カスコン
# * ファイル名   : Provisioning.sh
# *
# * [変更履歴]
# * 日付       更新者             内容
# * 2011/05/19 inoue@prosite      初期版作成
# * 2016/11/14 M.Tsunashima@PLUM  Terasolunaアップデート
#*******************************************************************************
#--------------------------------------------------------------------------------
# System Setting                                                               --
#--------------------------------------------------------------------------------
# JAVA_HOME設定
JAVA_HOME=/usr/java/default
# 処理実行フォルダ
FWS_HOME=/usr/local/pa_command
# 処理実行フォルダ
BIN_FLD=$FWS_HOME/conf
# Lock File
LOCKFILE=/var/tmp/LOCK.PA_COMMAND

# 実行パス設定
PATH=$PATH:$JAVA_HOME/bin
PATH=$PATH:/usr/bin:/sbin:$FWS_HOME/bin

# クラスパス設定
CLASSPATH=$CLASSPATH:$JAVA_HOME/lib/rt.jar
CLASSPATH=$CLASSPATH:$JAVA_HOME/lib/jsse.jar
CLASSPATH=$CLASSPATH:$JAVA_HOME/lib/jce.jar
CLASSPATH=$CLASSPATH:$JAVA_HOME/lib/charsets.jar
CLASSPATH=$CLASSPATH:$JAVA_HOME/lib/deploy.jar
CLASSPATH=$CLASSPATH:$JAVA_HOME/lib/javaws.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/aspectjweaver-1.7.4.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/commons-collections-3.2.2.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/commons-logging-1.1.3.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/commons-net-3.5.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/mybatis-2.3.5.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/jsch-0.1.54.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/log4j-1.2.16.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/postgresql-42.2.5.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/aopalliance-1.0.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/spring-beans-3.2.13.RELEASE.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/spring-context-3.2.13.RELEASE.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/spring-core-3.2.13.RELEASE.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/spring-aop-3.2.13.RELEASE.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/spring-jdbc-3.2.13.RELEASE.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/spring-tx-3.2.13.RELEASE.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/spring-expression-3.2.13.RELEASE.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/spring-orm-3.2.13.RELEASE.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/spring-struts-3.2.13.RELEASE.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/spring-web-3.2.13.RELEASE.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/terasoluna-commons-3.3.1.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/terasoluna-dao-3.3.1.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/terasoluna-ibatis-3.3.1.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/terasoluna-thin-2.0.6.2.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/lib/pa_command.jar
CLASSPATH=$CLASSPATH:$FWS_HOME/conf

# 設定読み込み
LANG=ja_JP.UTF-8
export JAVA_HOME PATH CLASSPATH LANG

# 実行ディレクトリに移動
cd $BIN_FLD;

# 多重起動チェック
if [ -f $LOCKFILE ]; then
  echo "多重起動"
  exit
fi
touch $LOCKFILE

# JAVA起動
$JAVA_HOME/bin/java jp.co.kddi.secgw.cuscon.pa_command.ProvisioningBLogic $1

rm -f $LOCKFILE
