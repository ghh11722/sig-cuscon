/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名	: UpdatePA.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/05/19     inoue@prosite    初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.pa_command.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import jp.co.kddi.secgw.cuscon.pa_command.common.vo.CommandPara;
import jp.co.kddi.secgw.cuscon.pa_command.common.vo.SelectSystemConstantList;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.util.PropertyUtil;

/**
 * クラス名 : UpdatePA
 * 機能概要 : PAの設定全削除、及びDBの指定世代にて<br>
 *			  PAの設定を行う。
 * 備考 :
 * @author inoue@prosite
 * @version 1.0 inoue@prosite
 *          Created 2011/05/19
 *          新規作成
 * @see
 */
public class UpdatePA {

	// コマンドタイムアウト時間
	private int timeout = 0;
	// 改行(コマンド付加用)
	private String CHANGE_LINE ="\n";
	// ダブルクォーテーション(コマンド付加用)
	private String QUOT ="\"";
	// コンフィグモードコマンド
	private static final String CONFIG_COM =
		PropertyUtil.getProperty("fwsetting.common.configure");
	// コマンドエラー表示
	private static final String COM_ERROR =
		PropertyUtil.getProperty("fwsetting.common.commanderror");
	// 日時フォーマット
	private static final String DATE_FORMAT =
		PropertyUtil.getProperty("fwsetting.common.dateformat");
	// コミット応答確認(正規表現)
	private static final String COMMIT_CHECK =
		PropertyUtil.getProperty("fwsetting.common.commitcheck");
	// コマンド待ち時間
	private static int WAIT_COMMAND = 0;
	// コミット成功メッセージ
	private static final String COMMIT_SUCCESS =
		PropertyUtil.getProperty("fwsetting.common.commitsuccess");
	// exitコマンド
	private static final String EXIT_COM =
		PropertyUtil.getProperty("fwsetting.common.exit");
	// commitコマンド
	private static final String COMMIT_COM =
		PropertyUtil.getProperty("fwsetting.common.commit");
	// コマンドinvalid
	private static final String COMMAND_INVALID =
		PropertyUtil.getProperty("fwsetting.common.command_invalid");
	// コマンドunknown
	private static final String COMMAND_UNKNOWN =
		PropertyUtil.getProperty("fwsetting.common.command_unknown");
	// コマンドserver error
	private static final String COMMAND_SERVER_ERROR =
		PropertyUtil.getProperty("fwsetting.common.command_server_error");
	// コメント先頭文字
	private static final String COMMENT_STR =
		PropertyUtil.getProperty("fwsetting.common.comment");
	// ページOFFコマンド
	private static final String PAGEOFFT_COM =
		PropertyUtil.getProperty("fwsetting.common.pageoff");
	// running-configのロードコマンド
	private static final String LOAD_RUNNING_CONFIG =
		PropertyUtil.getProperty("fwsetting.common.running_config");
	// running-configのロード成功メッセージ
	private static final String LOAD_RUNNING_CONFIG_SUCCESS =
		PropertyUtil.getProperty("fwsetting.common.running_configsuccess");

	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.provisioning.common.UpdatePA");
	// メッセージクラス
	private static MessageAccessor messageAccessor = null;

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 * @param cusconUVO
	 * @param queryDAO
	 */
	public UpdatePA() {
		log.debug("UpdatePAコンストラクタ1処理開始");
		log.debug("UpdatePAコンストラクタ1処理終了");
	}

	/**
	 * メソッド名 : updatePAProcess
	 * 機能概要 : 指定ファイルを読み込み、その内容（コマンド）をPAに送信する。
	 * @param File : Fileクラス
	 * @param resultJudge : ResultJudgeDataクラス
	 * @param loginId : PAログインID
	 * @param password : PAパスワード
	 * @param queryDAO : QueryDAO
	 * @param uvo : CusconUVO
	 * @param msgAcc : メッセージアクセッサ
	 * @throws Exception
	 */
	public void updatePAProcess(File file, ResultJudgeData resultJudge, String loginId, String password,
			QueryDAO queryDAO, CusconUVO uvo, MessageAccessor msgAcc) throws Exception{

		log.debug("updatePAProcess処理開始");
		messageAccessor = msgAcc;

		// PA接続情報を取得する。
		SelectSystemConstantList connectionInfo =
									getConnectionInfo(
											queryDAO, uvo, loginId, password);

		// コマンドタイムアウト時間
		timeout = connectionInfo.getTimeout() * 60 * 1000;

		// 設定ファイルより読み出す項目のログ出力
		if (log.isDebugEnabled()) {
			log.debug("コンフィグモードコマンド:" + CONFIG_COM);
			log.debug("コマンドエラー表示:" + COM_ERROR);
			log.debug("日時フォーマット:" + DATE_FORMAT);
			log.debug("コミット応答確認(正規表現):" + COMMIT_CHECK);
			log.debug("コミット成功メッセージ:" + COMMIT_SUCCESS);
		}

		// 設定コマンド用セッション作成
		CommandPara compara = new CommandPara();
		compara.setConnectionInfo(connectionInfo);
		compara.setUvo(uvo);
		compara.setTimeout(timeout);
		compara.setWaitCommand(WAIT_COMMAND);
		CommandExecute command = new CommandExecute(compara, msgAcc);

		String rst=null;
		// コマンドファイル（テキスト）を読み込み、Paloへ送信
		if ((rst=readCommandFile(file, command, compara, uvo)) != null){
			// コミット失敗
			Object[] objectgArray = {rst};
			log.error(messageAccessor.getMessage("EZ190007", objectgArray, uvo));
			resultJudge.setResultFlg(3);
			log.debug("updatePAProcess処理異常終了");
			return;
		}
		// telnet切断
		command.disconnect(compara);

		Date date = new Date();
		// 時刻フォーマットを定義する。
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);

		// 現在時刻を返却する。
		dateFormat.format(date);

		// 結果を格納する。
		resultJudge.setResultFlg(0);
		// 現在時刻を返却する。
		resultJudge.setResultData(dateFormat.format(date));

		log.debug("updatePAProcess処理終了");
		return;
	}

	/**
	 * メソッド名 : getConnectionInfo
	 * 機能概要 : DBよりPA接続情報を取得し、返却する。
	 * @param loginId : PAログインID
	 * @param password : PAパスワード
	 * @return SelectSystemConstantList : PA接続情報
	 * @throws Exception
	 */
	private SelectSystemConstantList getConnectionInfo(QueryDAO queryDAO, CusconUVO uvo,
									String loginId, String password) throws Exception {
		log.debug("getConnectionInfo処理開始: loginId = " +
									loginId + ",password = " + password);

		// PAのホスト、ユーザID、パスワードを取得する。
		// 復号化鍵
		String key = "kddi-secgw";
		// PA接続情報
		List<SelectSystemConstantList> connectionList = null;

		try {
			// PA接続情報を取得する。
			connectionList =
				queryDAO.executeForObjectList("CommonM_SystemConstant-1", key);
		} catch(Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = {e.getMessage()};
			log.fatal(messageAccessor.getMessage("FZ011033", objectgArray, uvo));
			throw e;
		}

		// 接続情報を戻り値に格納する。
//		connectionList.get(0).setLoginId(loginId);
//		connectionList.get(0).setPassword(password);
		WAIT_COMMAND = connectionList.get(0).getCmdWait();
		log.debug("getConnectionInfo処理終了");
		return connectionList.get(0);
	}

	/**
	 * メソッド名 : readCommandFile
	 * 機能概要 : 指定ファイルを読み、PAに送出する。
	 * @param File : Fileクラス
	 * @param password : PAパスワード
	 * @return SelectSystemConstantList : PA接続情報
	 * @throws Exception
	 */
	public String readCommandFile(
			File file, CommandExecute command, CommandPara compara, CusconUVO uvo) throws Exception{
		log.debug("readCommandFile処理開始");

		try{
			BufferedReader br = new BufferedReader(new FileReader(file));
			String cmdstr = null;
			String commitResult = null;
			String loadResult = null;
			int commandMode = 0;
			int icnt = 0;
			boolean configMode = false;
			while((cmdstr = br.readLine()) != null){
				List<String> comList = new ArrayList<String>();
				cmdstr = cmdstr.trim();
				// 最初のみページOFFコマンド送出
				if( icnt==0 ){
					comList.add(PAGEOFFT_COM);
					log.debug(" ** " + PAGEOFFT_COM);
					commitResult = command.getCommandResponce(compara, comList, 0);
					comList.clear();
				}
				log.debug(" ** " + cmdstr);
				if (cmdstr.length() > 0){
					// 先頭がコメント以外なら
					if( !cmdstr.substring(0,1).equals(COMMENT_STR)){
						cmdstr = cmdstr + CHANGE_LINE;
						comList.add(cmdstr);
						// configureなら
						if (cmdstr.equalsIgnoreCase(CONFIG_COM)){
							configMode = true;
							commandMode = 1;
						}
						// exitなら
						if (cmdstr.equalsIgnoreCase(EXIT_COM)){
							configMode = false;
							commandMode = 0;
						}
				        // commitコマンドの場合
						if(cmdstr.equalsIgnoreCase(COMMIT_COM)) {
							// コミットコマンド実行
							commitResult = executeCommit(command, compara, uvo);
							// コミットが失敗の場合
							if(commitResult.indexOf(COMMIT_SUCCESS) == -1) {
								// running-configのロードコマンド実行
								log.info("Commitに失敗した為、running-configに戻します。");
								loadResult = executeLoad_runnning_config(command, compara, uvo);
								br.close();
								return commitResult;
//								return "";
							}
							configMode = false;
						}
						// commit以外のコマンド場合
						else{
							// 設定コマンドログ出力
							Object[] objectgArray = {cmdstr};
							log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));
							// コマンドを実行し、レスポンスを取得する。
							commitResult = command.getCommandResponce(compara, comList, commandMode);
//2011/07/14 showコマンド ログ出力 start
//							if( comList.get(0).startsWith("show")){
								log.info("コマンド結果 = " + commitResult);
//							}
//2011/07/14 showコマンド ログ出力 end
							// コマンドが失敗の場合( invalid or unknown )
							if(commitResult.indexOf(COMMAND_INVALID) != -1 ||
							   commitResult.indexOf(COMMAND_UNKNOWN) != -1 ||
							   commitResult.indexOf(COMMAND_SERVER_ERROR) != -1) {
								// configureモードの場合、running-configのロードコマンド実行
								if( configMode ){
									log.info("Commandの投入に失敗した為、running-configに戻します。");
									loadResult = executeLoad_runnning_config(command, compara, uvo);
								}
								br.close();
								return commitResult;
//								return "";
							}
						}
					}
				}
				icnt++;
			}
			br.close();
			return null;
		}catch(Exception e){
			throw e;
		}
    }

	/**
	 * メソッド名 : executeCommit
	 * 機能概要 : コミットコマンドを実行する。
	 * @throws Exception
	 */
	private String executeCommit(
			CommandExecute command, CommandPara compara, CusconUVO uvo) throws Exception {

		log.debug("Commit開始");
		log.debug("コマンド:" + COMMIT_COM);

		// コマンドリスト
		List<String> comList = new ArrayList<String>();

		// コミットコマンド設定
		comList.add(COMMIT_COM);

		// コミットコマンドログ出力
		Object[] objectgArray = {COMMIT_COM};
		log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));

		// コマンド実行
		String commiReslt = command.getCommandResponce(compara, comList, 1);

		// commit完了ログ出力
		log.info("コマンド実行完了" + commiReslt);

		log.debug("Commit終了");

		return commiReslt;
	}

	/**
	 * メソッド名 : executeLoad_runnning_config
	 * 機能概要 : running-configのロードコマンドを実行する。
	 * @throws Exception
	 */
	private String executeLoad_runnning_config(
			CommandExecute command, CommandPara compara, CusconUVO uvo) throws Exception {

		log.debug("load running-config開始");
		log.debug("コマンド:" + LOAD_RUNNING_CONFIG);

		// コマンドリスト
		List<String> comList = new ArrayList<String>();

		// コマンド設定
		comList.add(LOAD_RUNNING_CONFIG);

		// コマンドログ出力
		Object[] objectgArray = {LOAD_RUNNING_CONFIG};
		log.info(messageAccessor.getMessage("IZ010001", objectgArray, uvo));

		// コマンド実行
		String Result = command.getCommandResponce(compara, comList, 1);

		// load完了ログ出力
		log.debug("コマンド実行完了" + Result);

		// loadが失敗の場合
		if(Result.indexOf(LOAD_RUNNING_CONFIG_SUCCESS) == -1) {
			log.info("running-configのロードに失敗しました。" + Result);
		}else{
			log.info("running-configのロードに成功しました。");
		}
		log.debug("load running-config終了");

		return Result;
	}

}


