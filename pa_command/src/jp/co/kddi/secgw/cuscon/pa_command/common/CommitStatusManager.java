/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CommitStatusManager.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/05/19     inoue@prosite    初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.pa_command.common;

import jp.co.kddi.secgw.cuscon.pa_command.common.CusconConst;
import jp.co.kddi.secgw.cuscon.pa_command.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.pa_command.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.pa_command.common.vo.CommitStatus;
import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;

import org.apache.log4j.Logger;

/**
 * クラス名 : CommitStatusManager
 * 機能概要 : コミットステータス管理クラス
 * 備考 :
 * @author inoue@prosite
 * @version 1.0 inoue@prosite
 *          Created 2011/05/19
 *          新規作成
 * @see
 */
public class CommitStatusManager {

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 */
	public CommitStatusManager() {
	}

	// メッセージクラス
	private static MessageAccessor messageAccessor = null;

	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.pa_command.common.CommitStatusManager");

	/**
	 * メソッド名 : isCommitting
	 * 機能概要 : ステータスがコミット中か判定する。
	 * @param queryDAO QueryDAO
	 * @param uvo CusconUVO
	 * @return コミット中 <code>true</code>
	 * @throws Exception
	 */
	public boolean isCommitting(QueryDAO queryDAO, CusconUVO uvo) throws Exception {

		log.debug("isCommitting処理開始 vsysId = " + uvo.getVsysId());

		CommitStatus commtStat = getCommitStatus(queryDAO, uvo);

		if (commtStat != null && commtStat.getStatus() == CusconConst.STAT_COMMITTING) {
			// コミット中
			log.debug("isCommitting処理終了:コミット中");
			return true;
		}

		// コミット中以外
		log.debug("isCommitting処理終了:コミット中以外");
		return false;
	}

	/**
	 * メソッド名 : コミットステータス取得
	 * 機能概要 : コミットステータスを取得する
	 * @param queryDAO QueryDAO
	 * @param uvo CusconUVO
	 * @return コミットステータス情報
	 * @throws Exception
	 */
	public CommitStatus getCommitStatus(QueryDAO queryDAO, CusconUVO uvo) throws Exception {
		log.debug("getCommitStatus処理開始 vsysId = " + uvo.getVsysId());
		CommitStatus commitStatus = null;
		try {
			commitStatus = queryDAO.executeForObject("CommonT_CommitStatus-1", uvo.getVsysId(), CommitStatus.class);
		} catch (Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = { e.getMessage() };
			log.fatal(messageAccessor.getMessage("FZ190002", objectgArray, uvo));
			throw e;
		}
		log.debug("getCommitStatus処理終了");
		return commitStatus;
	}

	/**
	 * メソッド名 : コミットステータス登録
	 * 機能概要 : コミットステータスを登録する
	 * @param updateDAO UpdateDAO
	 * @param uvo CusconUVO
	 * @param commitStatus コミットステータス情報
	 * @throws Exception
	 */
	public void insertCommitStatus(UpdateDAO updateDAO, CusconUVO uvo, CommitStatus commitStatus) throws Exception {
		log.debug("insertCommitStatus処理開始 vsysId = " + uvo.getVsysId());
		try {
			updateDAO.execute("CommonT_CommitStatus-2", commitStatus);
		} catch (Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = { e.getMessage() };
			log.fatal(messageAccessor.getMessage("FZ190001", objectgArray, uvo));
			throw e;
		}
		log.debug("insertCommitStatus処理終了");
	}

	/**
	 * メソッド名 : コミットステータス削除
	 * 機能概要 : コミットステータスを削除する
	 * @param updateDAO UpdateDAO
	 * @param uvo CusconUVO
	 * @throws Exception
	 */
	public void deleteCommitStatus(UpdateDAO updateDAO, CusconUVO uvo) throws Exception {
		log.debug("deleteCommitStatus処理開始 vsysId = " + uvo.getVsysId());
		int delCnt = 0;
		try {
			delCnt = updateDAO.execute("CommonT_CommitStatus-3", uvo.getVsysId());
		} catch (Exception e) {
			// DBアクセスエラー
			Object[] objectgArray = { e.getMessage() };
			log.fatal(messageAccessor.getMessage("FZ190001", objectgArray, uvo));
			throw e;
		}
		log.debug("deleteCommitStatus処理終了 削除件数 = " + delCnt);
	}

	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		CommitStatusManager.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}