/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : UpdateCommitFlg.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/05/19     inoue@prosite    初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.pa_command.common;

import org.apache.log4j.Logger;

import jp.terasoluna.fw.dao.QueryDAO;

/**
 * クラス名 : UpdateCommitFlg
 * 機能概要 : commitフラグを判定、更新する。
 * 備考 :
 * @author inoue@prosite
 * @version 1.0 inoue@prosite
 *          Created 2011/05/19
 *          新規作成
 * @see
 */
public class UpdateCommitFlg {

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 */
	public UpdateCommitFlg() {
	}

	private Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.pa_command.common.UpdateCommitFlg");

	/**
	 * メソッド名 : updatePAProcessFlg
	 * 機能概要 : commitフラグの設定可否を判定し、<br>
	 *            ON/OFFを設定する。
	 * @param commit_flg commit処理中フラグ
     * @return int 処理判定フラグ
	 * @throws Exception
	 */
	public int updatePAProcessFlg(int commitFlg, QueryDAO queryDAO) throws Exception {
		log.debug("updatePAProcessFlg処理開始");

		Integer myrec = 0;

		// SQL実行
		myrec = queryDAO.executeForObject("CommonT_CommitProcessing-1",
											commitFlg, Integer.class);
   		if (myrec == 1) {
   			log.debug("他でコミット中");
   		}

		log.debug("updatePAProcessFlg処理終了：値=" + myrec);
		return myrec;
	}

}
