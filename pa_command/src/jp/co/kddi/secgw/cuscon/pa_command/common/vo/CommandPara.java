/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CommandPara.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/05/19     inoue@prosite           初版作成
 * 2016/11/24     T.Yamazaki@Plum Systems SSH対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.pa_command.common.vo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;

import org.apache.commons.net.telnet.TelnetClient;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.Session;

import jp.co.kddi.secgw.cuscon.pa_command.common.CusconUVO;


/**
 * クラス名 : CommandPara
 * 機能概要 :
 * 備考 :
 * @author inoue@prosite
 * @version 1.0 inoue@prosite
 *          Created 2011/05/19
 *          新規作成
 *          2.0 T.Yamazaki@Plum Systems
 *          Updated 2016/11/24
 *          SSH対応
 * @see
 */
public class CommandPara {
	// DB接続情報クラス
	private SelectSystemConstantList connectionInfo;
    // コマンドタイムアウト時間
    private int timeout = 0;
    // コマンド発行待ち時間
    private int waitCommand = 0;
	// CusconUVO
    private CusconUVO uvo = null;
    // telnetクライアント
    private TelnetClient telnet = null;
    // 通信用の入出力ストリーム
    private InputStream istream = null;
    private OutputStream ostream = null;
    private Reader reader = null;
    private Writer writer = null;

	// SSHセッション
	private Session sshSession = null;
	// SSHチャンネル
	private Channel sshChannel = null;

	/**
	 * メソッド名 : connectionInfoのSetterメソッド
	 * 機能概要 : connectionInfoをセットする。
	 * @param connectionInfo
	 */
	public void setConnectionInfo(SelectSystemConstantList connectionInfo) {
		this.connectionInfo = connectionInfo;
	}
	/**
	 * メソッド名 : connectionInfoのGetterメソッド
	 * 機能概要 : connectionInfoを取得する。
	 * @return connectionInfo
	 */
	public SelectSystemConstantList getConnectionInfo() {
		return connectionInfo;
	}
	/**
	 * メソッド名 : timeoutのSetterメソッド
	 * 機能概要 : timeoutをセットする。
	 * @param timeout
	 */
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	/**
	 * メソッド名 : timeoutのGetterメソッド
	 * 機能概要 : timeoutを取得する。
	 * @return timeout
	 */
	public int getTimeout() {
		return timeout;
	}
	/**
	 * メソッド名 : waitCommandのSetterメソッド
	 * 機能概要 : waitCommandをセットする。
	 * @param waitCommand
	 */
	public void setWaitCommand(int waitCommand) {
		this.waitCommand = waitCommand;
	}
	/**
	 * メソッド名 : waitCommandのGetterメソッド
	 * 機能概要 : waitCommandを取得する。
	 * @return waitCommand
	 */
	public int getWaitCommand() {
		return waitCommand;
	}
	/**
	 * メソッド名 : uvoのSetterメソッド
	 * 機能概要 : uvoをセットする。
	 * @param uvo
	 */
	public void setUvo(CusconUVO uvo) {
		this.uvo = uvo;
	}
	/**
	 * メソッド名 : uvoのGetterメソッド
	 * 機能概要 : uvoを取得する。
	 * @return uvo
	 */
	public CusconUVO getUvo() {
		return uvo;
	}

	// ----------------------------------------------------------
	// Telnet
	// ----------------------------------------------------------
	/**
	 * メソッド名 : telnetのSetterメソッド
	 * 機能概要 : telnetをセットする。
	 * @param telnet
	 */
	public void setTelnet(TelnetClient telnet) {
		this.telnet = telnet;
	}
	/**
	 * メソッド名 : telnetのGetterメソッド
	 * 機能概要 : telnetを取得する。
	 * @return telnet
	 */
	public TelnetClient getTelnet() {
		return telnet;
	}

	// ----------------------------------------------------------
	// SSH
	// ----------------------------------------------------------
	/**
	 * メソッド名 : sshSessionのGetterメソッド
	 * 機能概要 : sshSessionを取得する。
	 * @return sshSession
	 */
	public Session getSshSession() {
		return sshSession;
	}
	/**
	 * メソッド名 : sshSessionのSetterメソッド
	 * 機能概要 : sshSessionをセットする。
	 * @param sshSession
	 */
	public void setSshSession(Session sshSession) {
		this.sshSession = sshSession;
	}
	/**
	 * メソッド名 : sshChannelのGetterメソッド
	 * 機能概要 : sshChannelを取得する。
	 * @return sshChannel
	 */
	public Channel getSshChannel() {
		return sshChannel;
	}
	/**
	 * メソッド名 : sshChannelのSetterメソッド
	 * 機能概要 : sshChannelをセットする。
	 * @param sshChannel
	 */
	public void setSshChannel(Channel sshChannel) {
		this.sshChannel = sshChannel;
	}

	// ----------------------------------------------------------

	/**
	 * メソッド名 : istreamのSetterメソッド
	 * 機能概要 : istreamをセットする。
	 * @param istream
	 */
	public void setIstream(InputStream istream) {
		this.istream = istream;
	}
	/**
	 * メソッド名 : istreamのGetterメソッド
	 * 機能概要 : istreamを取得する。
	 * @return istream
	 */
	public InputStream getIstream() {
		return istream;
	}
	/**
	 * メソッド名 : ostreamのSetterメソッド
	 * 機能概要 : ostreamをセットする。
	 * @param ostream
	 */
	public void setOstream(OutputStream ostream) {
		this.ostream = ostream;
	}
	/**
	 * メソッド名 : ostreamのGetterメソッド
	 * 機能概要 : ostreamを取得する。
	 * @return ostream
	 */
	public OutputStream getOstream() {
		return ostream;
	}
	/**
	 * メソッド名 : readerのSetterメソッド
	 * 機能概要 : readerをセットする。
	 * @param reader
	 */
	public void setReader(Reader reader) {
		this.reader = reader;
	}
	/**
	 * メソッド名 : readerのGetterメソッド
	 * 機能概要 : readerを取得する。
	 * @return reader
	 */
	public Reader getReader() {
		return reader;
	}
	/**
	 * メソッド名 : writerのSetterメソッド
	 * 機能概要 : writerをセットする。
	 * @param writer
	 */
	public void setWriter(Writer writer) {
		this.writer = writer;
	}
	/**
	 * メソッド名 : writerのGetterメソッド
	 * 機能概要 : writerを取得する。
	 * @return writer
	 */
	public Writer getWriter() {
		return writer;
	}
	/**
	 * メソッド名 : finalize
	 * 機能概要 : 接続終了処理
	 * @param compara
	 */
	protected void finalize() {
        try {
        	if (this.telnet != null) {
        		telnet.disconnect();
        		telnet = null;
        	}
        	if (this.sshChannel != null){
        		sshChannel.disconnect();
				sshChannel = null;
        	}
        	if (this.sshSession != null){
        		sshSession.disconnect();
				sshSession = null;
        	}
		} catch (IOException e) {
    		// 終了処理の例外は無視
		}
	}
}
