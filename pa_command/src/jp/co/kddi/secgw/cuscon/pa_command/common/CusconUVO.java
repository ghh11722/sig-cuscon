/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION. All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CusconUVO.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/05/19     inoue@prosite    初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.pa_command.common;

import jp.terasoluna.fw.web.UserValueObject;

/**
 * クラス名 : CusconUVO
 * 機能概要 : ユーザーバリューオブジェクトの実装
 * 備考 :
 * @author inoue@prosite
 * @version 1.0 inoue@prosite
 *          Created 2011/05/19
 *          新規作成
 * @see
 */
public class CusconUVO extends UserValueObject {
	//
	private static final long serialVersionUID = 1L;
	// カスコンログインID
	private String loginId = null;
	// ユーザ権限
	private long grantFlag = 0;
	// 企業管理者ログインID
	private String customerLoginId = null;
	// Vsys-ID
	private String vsysId = "";
	// PAログインID
	private String paLoginId = null;
	// PAログインパスワード
	private String paPasswd = null;
	// 便利そうなので追加by片桐
	private String grantFlagStr = null;

	/**
	 * メソッド名 : loginIdのGetterメソッド
	 * 機能概要 : loginIdを取得する。
	 * @return loginId カスコンログインID
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * メソッド名 : loginIdのSetterメソッド
	 * 機能概要 : loginIdをセットする。
	 * @param loginId カスコンログインID
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * メソッド名 : grantFlagのGetterメソッド
	 * 機能概要 : grantFlagを取得する。
	 * @return grantFlag ユーザ権限
	 */
	public long getGrantFlag() {
		return grantFlag;
	}

	/**
	 * メソッド名 : grantFlagのSetterメソッド
	 * 機能概要 : grantFlagをセットする。
	 * @param grantFlag ユーザ権限
	 */
	public void setGrantFlag(long grantFlag) {
		this.grantFlag = grantFlag;
		if (grantFlag == 1) {
			setGrantFlagStr("N");
		} else if (grantFlag == 2) {
			setGrantFlagStr("T");
		} else if (grantFlag == 3) {
			setGrantFlagStr("C");
		} else {
			setGrantFlagStr("");
		}
	}

	/**
	 * メソッド名 : customerLoginIdのGetterメソッド
	 * 機能概要 : customerLoginIdを取得する。
	 * @return customerLoginId 企業管理者ログインID
	 */
	public String getCustomerLoginId() {
		return customerLoginId;
	}

	/**
	 * メソッド名 : customerLoginIdのSetterメソッド
	 * 機能概要 : customerLoginIdをセットする。
	 * @param customerLoginId 企業管理者ログインID
	 */
	public void setCustomerLoginId(String customerLoginId) {
		this.customerLoginId = customerLoginId;
	}

	/**
	 * メソッド名 : vsysIdのGetterメソッド
	 * 機能概要 : vsysIdを取得する。
	 * @return vsysId Vsys-ID
	 */
	public String getVsysId() {
		return vsysId;
	}

	/**
	 * メソッド名 : vsysIdのSetterメソッド
	 * 機能概要 : vsysIdをセットする。
	 * @param vsysId Vsys-ID
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}

	/**
	 * メソッド名 : paLoginIdのGetterメソッド
	 * 機能概要 : paLoginIdを取得する。
	 * @return paLoginId PAログインID
	 */
	public String getPaLoginId() {
		return paLoginId;
	}

	/**
	 * メソッド名 : paLoginIdのSetterメソッド
	 * 機能概要 : paLoginIdをセットする。
	 * @param paLoginId PAログインID
	 */
	public void setPaLoginId(String paLoginId) {
		this.paLoginId = paLoginId;
	}

	/**
	 * メソッド名 : paPasswdのGetterメソッド
	 * 機能概要 : paPasswdを取得します。
	 * @return paPasswd PAログインパスワード
	 */
	public String getPaPasswd() {
		return paPasswd;
	}

	/**
	 * メソッド名 : paPasswdのSetterメソッド
	 * 機能概要 : paPasswdを設定します。
	 * @param paPasswd PAログインパスワード
	 */
	public void setPaPasswd(String paPasswd) {
		this.paPasswd = paPasswd;
	}

	/**
	 * メソッド名 : grantFlagStrのSetterメソッド
	 * 機能概要 : grantFlagStrをセットする。
	 * @param grantFlagStr ユーザ権限
	 */
	public void setGrantFlagStr(String grantFlagStr) {
		this.grantFlagStr = grantFlagStr;
	}

	/**
	 * メソッド名 : grantFlagStrのGetterメソッド
	 * 機能概要 : grantFlagStrを取得する。
	 * @return grantFlagStr ユーザ権限
	 */
	public String getGrantFlagStr() {
		return grantFlagStr;
	}

}
