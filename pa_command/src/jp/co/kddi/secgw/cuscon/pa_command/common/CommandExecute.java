/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CommandExecute.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/05/19     inoue@prosite           初版作成
 * 2016/11/24     T.Yamazaki@Plum Systems SSH対応
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.pa_command.common;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
//import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.net.telnet.TelnetClient;
import org.apache.log4j.Logger;

import com.jcraft.jsch.JSch;

import jp.co.kddi.secgw.cuscon.pa_command.common.vo.CommandPara;
import jp.terasoluna.fw.util.PropertyUtil;

/**
 * クラス名 : CommandExecute
 * 機能概要 : PAに対してコマンドを発行する。
 * 備考 :
 * @author inoue@prosite
 * @version 1.0 inoue@prosite
 *          Created 2011/05/19
 *          新規作成
 *          2.0 T.Yamazaki@Plum Systems Inc.
 *          Updated 2016/11/24
 *          SSH対応
 * @see
 */
public class CommandExecute {

	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.pa_command.common.CommandExecute");


	// メッセージクラス
	private static MessageAccessor messageAccessor = null;
	// プロンプト(通常)
	private static String PROMPT_NORMAL = PropertyUtil.getProperty("fwsetting.common.normalPrompt");
	// プロンプト(configモード)
	private static String PROMPT_CONFIG = PropertyUtil.getProperty("fwsetting.common.configPrompt");

	// 接続タイムアウト
	private static Integer TIMEOUT_CONNECT = 60000;
	// コマンドタイムアウト
	private static Integer TIMEOUT_COMMAND = 3600000;

	// 接続パラメータ
	private CommandPara compara = null;

	// SSHを使用するかどうかを示すフラグ
	private Boolean useSecuredShell = true;

	// 使用するプロトコル（プロパティファイルから取得：デフォルトは「telnet」）
	private static String PROTOCOL = PropertyUtil.getProperty("fwsetting.common.pa_protocol", "telnet");

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 */
	public CommandExecute() {
	}

	/**
	 * メソッド名 : コンストラクタ
	 * 機能概要 : -
	 * @param compara
	 * @throws Exception
	 */
	public CommandExecute(CommandPara compara, MessageAccessor msgAcc) throws Exception {
		log.debug("CommandExecute (Enabled SSH) コンストラクタ処理開始");
		messageAccessor = msgAcc;

		try {
			log.debug("PA接続開始");

			// 使用するプロトコルをプロパティファイルから取得（指定されていないときは"telnet"とする）
			if(PROTOCOL.equals("ssh")){
				this.useSecuredShell = true;
				log.debug("SSHを使用します。");
			} else {
				this.useSecuredShell = false;
				log.debug("Telnetを使用します。");
			}

			compara.setTimeout(TIMEOUT_COMMAND);
			this.compara = compara;

			initSession(compara);
		} catch (Exception e) {
			// PA接続エラー
			Object[] objectgArray = { e.getMessage() };
			log.fatal(messageAccessor.getMessage("FZ011035", objectgArray, compara.getUvo()));
			throw e;
		}
		log.debug("CommandExecute (Enabled SSH) コンストラクタ処理終了");
	}

	/**
	 * メソッド名 : ファイナライザ
	 * 機能概要 : -
	 */
	@Override
	protected void finalize() throws Throwable {
		log.debug("ConnectShellファイナライザ (Enabled SSH) 処理開始");
		try {
			super.finalize();
		} finally {
			this.disconnect();
		}
		log.debug("ConnectShellファイナライザ (Enabled SSH) 処理終了");
	}

	/**
	 * メソッド名 : セッション開始
	 * 機能概要 : telnetセッションを開始する。
	 * @param compara
	 * @throws Exception
	 */
	private void initSession(CommandPara compara) throws Exception {
		log.debug("initSession (Enabled SSH) 処理開始");
		this.compara = compara;

		log.info("Host = " + compara.getConnectionInfo().getAddress());
		log.debug("Connection timeout = " + TIMEOUT_CONNECT);
		log.debug("Command wait = " + compara.getWaitCommand());
		log.debug("User = " + compara.getConnectionInfo().getLoginId());
		log.debug("Password = " + compara.getConnectionInfo().getPassword());
		log.debug("Timeout = " + compara.getTimeout());

		log.debug("PROMPT_NORMAL = " + PROMPT_NORMAL);
		log.debug("PROMPT_CONFIG = " + PROMPT_CONFIG);

		if (this.useSecuredShell) {
			// SSH
			log.info("SSH接続開始");

			// クライアントの生成
			JSch jsch = new JSch();
			compara.setSshSession(jsch.getSession(compara.getConnectionInfo().getLoginId(),
					compara.getConnectionInfo().getAddress()));

			compara.getSshSession().setPassword(compara.getConnectionInfo().getPassword());
			compara.getSshSession().setConfig("StrictHostKeyChecking", "no");

			log.info("compara.getSshSession().ConnectTimeout=" + TIMEOUT_CONNECT);
			log.info("compara.getWaitCommand()=" + compara.getWaitCommand());

			log.info("SSHセッションを開始しています... (" + compara.getConnectionInfo().getLoginId() + " => "
					+ compara.getConnectionInfo().getAddress() + ")");
			compara.getSshSession().connect(TIMEOUT_CONNECT);

			compara.setSshChannel(compara.getSshSession().openChannel("shell"));

			// 通信用の入出力ストリームの生成
			compara.setIstream(compara.getSshChannel().getInputStream());
			compara.setOstream(compara.getSshChannel().getOutputStream());

			compara.setReader(new InputStreamReader(compara.getIstream()));
			compara.setWriter(new OutputStreamWriter(compara.getOstream()));

			log.debug("SSHチャンネルを開始しています...");
			compara.getSshChannel().connect(compara.getTimeout());
			log.info("SSH接続を開始しました。");

		} else {
			// Telnet
			log.info("Telnet接続開始");

			// クライアントの生成
			compara.setTelnet(new TelnetClient());

			// 接続タイムアウト指定(ms)
			compara.getTelnet().setConnectTimeout(TIMEOUT_CONNECT);
			log.info("compara.getTelnet().getConnectTimeout()=" + compara.getTelnet().getConnectTimeout());
			log.info("compara.getWaitCommand()=" + compara.getWaitCommand());

			log.info("Telnet接続を開始しています... (" + compara.getConnectionInfo().getAddress() + ")");
			// サーバに接続
			compara.getTelnet().connect(compara.getConnectionInfo().getAddress());
			// 開いたconnectに対するタイムアウト指定
			compara.getTelnet().setSoTimeout(compara.getTimeout());

			// 通信用の入出力ストリームの生成
			compara.setIstream(compara.getTelnet().getInputStream());
			compara.setOstream(compara.getTelnet().getOutputStream());

			compara.setReader(new InputStreamReader(compara.getIstream()));
			compara.setWriter(new OutputStreamWriter(compara.getOstream()));

			// 認証の実行
			log.debug("認証処理を行っています... ");
			commandWait();
			readMessage(compara, ".*login: $");
			sendCommand(compara.getConnectionInfo().getLoginId() + "\n");
			commandWait();
			readMessage(compara, ".*Password: $");
			sendCommand(compara.getConnectionInfo().getPassword() + "\n");
		}

		// プロンプト出力待ち
		this.commandWait();
		// Thread.sleep(compara.getWaitCommand());
		readMessage(compara, ".*" + PROMPT_NORMAL);
		log.debug("コマンド入力待ち");

		log.debug("initSession (Enabled SSH) 処理終了");
		return;
	}

	/**
	 * メソッド名 : sendCommand
	 * 機能概要 : コマンド送信
	 *
	 * @param command
	 */
	public void sendCommand(String command) {
		try {
			this.compara.getWriter().write(command);
			this.compara.getWriter().flush();
			log.debug("<-[" + PROTOCOL  + "]- " + command);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * メソッド名 : commandWait
	 * 機能概要 : コマンド送信待ち
	 *
	 * @throws Exception
	 */
	private void commandWait() {
		try {
			Thread.sleep(this.compara.getWaitCommand());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * メソッド名 : readMessage
	 * 機能概要 : 特定のメッセージが出力されるまで、サーバからのメッセージを読み込む。
	 * @param compara コマンドパラメータ
	 * @param message 終了メッセージ
	 * @return String コマンドレスポンス
	 */
	private static String readMessage(CommandPara compara, String message) throws Exception {

		log.debug("readMessage処理開始 : message = " + message);

		Pattern pattern = Pattern.compile(message, Pattern.DOTALL);
		StringBuffer buffer = new StringBuffer();
		Matcher matcher = null;

		// コマンド時間の計測
		long start;
		long stop;
		long elapsed;

		// 計測開始
		start = System.currentTimeMillis();

		while (true) {
			if (log.isTraceEnabled()) {
				log.trace("ストリームバッファ読取");
			}
			int c = compara.getReader().read();
			if (c < 0) {
				log.trace("** no read data ** ");
				break;
			}

			// 改行コード以外の制御文字を読み飛ばしてみる
			if ((c >= 0) && (c <= 0x1f)) {
				log.trace("改行コード以外の制御文字を読み飛ばし");
				if (c != 0x0a ) continue;
			}

			buffer.append((char) c);
			if (compara.getReader().ready() == false) {
				log.trace("reader.ready() == false");
				matcher = pattern.matcher(buffer.toString());
				if (matcher.matches()) break;
			}
			// log.debug("[" + buffer.toString() + "]");

			stop = System.currentTimeMillis();
			if (log.isTraceEnabled()) {
				log.trace("コマンド取得中:経過秒=" + ((stop- start)/1000));
			}

			// コマンド待ち時間判定
			elapsed = stop - start;
			if (elapsed >= compara.getTimeout()) {
				// タイムアウト
				log.error(messageAccessor.getMessage("EZ011003", null, compara.getUvo()));
				throw new TimeoutException();
			}
		}
		// サーバーの応答を出力
		log.debug("-[" + PROTOCOL  + "]-> " + buffer);

		if (matcher.find(0) && matcher.groupCount() >= 1) {
			log.debug("正常レスポンス");
			return (matcher.group(1));
		}
		log.debug("異常レスポンス");
		return (null);
    }


	/**
	 * メソッド名 : getCommandResponce
	 * 機能概要 : commandListのコマンドを実行し、標準出力内容を返却する。
	 *
	 * @param compara コマンドパラメータ
	 * @param commandList コマンドリスト
	 * @param commandMode コマンドモード（0:normal 1:configure）
	 * @return String コマンドレスポンス
	 * @throws Exception
	 */
	public String getCommandResponce(CommandPara compara, List<String> commandList, int commandMode) throws Exception {
		log.debug("getCommandResponce処理開始 : コマンド数 = " + commandList.size());

		log.debug("------------------------------------------------------------");
		log.debug("送信コマンド");
		for(int i = 0; i < commandList.size(); i++){
			log.debug(" " + i + " : " + commandList.get(i).replace("\n",""));
		}
		log.debug("------------------------------------------------------------");

		// レスポンス
		String response = "";

		try {
			// commandList分繰り返す。
			for (int i = 0; i < commandList.size(); i++) {
				log.debug("コマンド実行:" + commandList.get(i));
				sendCommand(commandList.get(i));

				if (commandMode==1) {
					log.debug("configモードコマンド実行");
					commandWait();
					response = response + readMessage(compara, "(.*)" + PROMPT_CONFIG);
				} else {
					log.debug("通常コマンド実行");
					commandWait();
					response = response + readMessage(compara, "(.*)" + PROMPT_NORMAL);
				}
			}
		} catch (Exception e) {
			// コマンド実行エラー
			Object[] objectgArray = { e.getMessage() };
			log.error(messageAccessor.getMessage("EZ011001", objectgArray, compara.getUvo()));
			throw e;
		}
		if (response.length() == 0) {
			// コマンド実行エラー
			Object[] objectgArray = { "" };
			log.error(messageAccessor.getMessage("EZ011001", objectgArray, compara.getUvo()));
			throw new Exception();
		}
		log.debug("getCommandResponce処理終了2:response = " + response);
		return response;
	}

	/**
	 * メソッド名 : disconnect
	 * 機能概要 : 接続終了処理
	 */
	public void disconnect() {
		this.disconnect(this.compara);
	}

	/**
	 * メソッド名 : disconnect
	 * 機能概要 : 接続終了処理
	 *
	 * @param compara
	 */
	public void disconnect(CommandPara compara) {
		log.debug("CommandExecute disconnect");
		try {
			if (compara.getTelnet() != null) {
				log.debug("Telnet切断");
				compara.getTelnet().disconnect();
				compara.setTelnet(null);
			}
			if (compara.getSshChannel() != null) {
				log.debug("SSH Channel切断");
				compara.getSshChannel().disconnect();
				compara.setSshChannel(null);
			}
			if (compara.getSshSession() != null) {
				log.debug("SSH Session切断");
				compara.getSshSession().disconnect();
				compara.setSshSession(null);
			}
		} catch (IOException e) {
			log.debug("ネットワークの切断時の例外は無視::" + e.getStackTrace());
		}
	}
}