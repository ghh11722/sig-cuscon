/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : MessageAccessorImpl.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/05/19     inoue@prosite    初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.pa_command.common;

import org.springframework.context.support.ApplicationObjectSupport;

/**
 * クラス名 : MessageAccessorImpl
 * 機能概要 :メッセージアクセス実装クラス
 * 備考 :
 * @author inoue@prosite
 * @version 1.0 inoue@prosite
 *          Created 2011/05/19
 *          新規作成
 * @see
 */
public class MessageAccessorImpl extends ApplicationObjectSupport implements MessageAccessor {

	/**
	 * メソッド名 : getMessage
	 * 機能概要 : メッセージキーに該当するメッセージ本文を取得する
	 * @param code メッセージキー
	 * @param args メッセージ中のプレースホルダに埋め込む文字列
	 * @return String メッセージ
	 * @see jp.co.kddi.secgw.cuscon.pa_command.common.MessageAccessor#getMessage(java.lang.String, java.lang.Object[])
	 */
	@Override
	public String getMessage(String code, Object[] args) {

			return (getMessageSourceAccessor().getMessage(code, args));
	}

	/**
	 * メソッド名 : getMessage
	 * 機能概要 : メッセージキーに該当するメッセージ本文を取得し、
	 *            ログフォーマットに整形する
	 * @param code メッセージキー
	 * @param args メッセージ中のプレースホルダに埋め込む文字列
	 * @param uvo  ログインユーザ情報
	 * @return String ログフォーマットメッセージ
	 * @see jp.co.kddi.secgw.cuscon.pa_command.common.MessageAccessor#getMessage(java.lang.String, java.lang.Object[], jp.co.kddi.secgw.cuscon.common.CusconUVO)
	 */
	@Override
	public String getMessage(String code, Object[] args, CusconUVO uvo) {

		String ret_string = null;
		if (null != uvo) {
//			ret_string = (uvo.getGrantFlagStr() + " " + uvo.getLoginId() +
//					" " + uvo.getVsysId() + " " +
//					getMessageSourceAccessor().getMessage(code, args));
			ret_string = (getMessageSourceAccessor().getMessage(code, args));
		} else {
			ret_string = (getMessageSourceAccessor().getMessage(code, args));
		}
		return ret_string;
	}

}
