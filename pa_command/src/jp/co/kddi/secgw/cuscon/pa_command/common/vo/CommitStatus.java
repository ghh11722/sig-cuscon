/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : CommitEnd.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/05/19     inoue@prosite    初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.pa_command.common.vo;

import java.io.Serializable;

public class CommitStatus implements Serializable {
	// シリアルバージョンID
	private static final long serialVersionUID = 0L;

	// Vsys-ID
	private String vsysId = null;
	// 処理区分
	private int processingKind = 0;
	// ステータス
	private int status = 0;
	// メッセージ
	private String message = null;
	// StartTime
	private String startTime = null;
	// EndTime
	private String endTime = null;
	/**
	 * @param vsysId セットする vsysId
	 */
	public void setVsysId(String vsysId) {
		this.vsysId = vsysId;
	}
	/**
	 * @return vsysId
	 */
	public String getVsysId() {
		return vsysId;
	}
	/**
	 * @param processingKind セットする processingKind
	 */
	public void setProcessingKind(int processingKind) {
		this.processingKind = processingKind;
	}
	/**
	 * @return processingKind
	 */
	public int getProcessingKind() {
		return processingKind;
	}
	/**
	 * @param status セットする status
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	/**
	 * @return status
	 */
	public int getStatus() {
		return status;
	}
	/**
	 * @param message セットする message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param startTime セットする startTime
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	/**
	 * @return startTime
	 */
	public String getStartTime() {
		return startTime;
	}
	/**
	 * @param endTime セットする endTime
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	/**
	 * @return endTime
	 */
	public String getEndTime() {
		return endTime;
	}

}
