/*******************************************************************************
 * Copyright(c) 2010 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : SelectSystemConstantList.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/05/19     inoue@prosite    初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.pa_command.common.vo;

import java.io.Serializable;

/**
 * クラス名 : SelectSystemConstantList
 * 機能概要 : システム定数マスタデータクラス
 * 備考 :
 * @author inoue@prosite
 * @version 1.0 inoue@prosite
 *          Created 2011/05/19
 *          新規作成
 * @see
 */
public class SelectSystemConstantList implements Serializable {

	// シリアルバージョンID
	private static final long serialVersionUID = 6824237178731213915L;

	// PA CLI接続アドレス
	private String address;
	// PA XML-API URL
	private String url;
	// PAログインID
	private String loginId;
	// PAログインパスワード
	private String password;
	// コミット排他待ちウェイト時間
	private int waitTime = 0;
	// コミット排他待ちリトライ回数
	private int retryNum = 0;
	// PA コマンドタイムアウト時間
	private int timeout = 0;
	// PA コマンド待ちウェイト時間
	private int cmdWait = 0;


	/**
	 * メソッド名 : addressのGetterメソッド
	 * 機能概要 : addressを取得する。
	 * @return address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * メソッド名 : addressのSetterメソッド
	 * 機能概要 : addressをセットする。
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * メソッド名 : urlのGetterメソッド
	 * 機能概要 : urlを取得する。
	 * @return url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * メソッド名 : urlのSetterメソッド
	 * 機能概要 : urlをセットする。
	 * @param url
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * メソッド名 : loginIdのGetterメソッド
	 * 機能概要 : loginIdを取得する。
	 * @return loginId
	 */
	public String getLoginId() {
		return loginId;
	}
	/**
	 * メソッド名 : loginIdのSetterメソッド
	 * 機能概要 : loginIdをセットする。
	 * @param loginId
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	/**
	 * メソッド名 : passwordのGetterメソッド
	 * 機能概要 : passwordを取得する。
	 * @return password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * メソッド名 : passwordのSetterメソッド
	 * 機能概要 : passwordをセットする。
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * メソッド名 : waitTimeのGetterメソッド
	 * 機能概要 : waitTimeを取得する。
	 * @return waitTime
	 */
	public int getWaitTime() {
		return waitTime;
	}
	/**
	 * メソッド名 : waitTimeのSetterメソッド
	 * 機能概要 : waitTimeをセットする。
	 * @param waitTime
	 */
	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}
	/**
	 * メソッド名 : retryNumのGetterメソッド
	 * 機能概要 : retryNumを取得する。
	 * @return retryNum
	 */
	public int getRetryNum() {
		return retryNum;
	}
	/**
	 * メソッド名 : retryNumのSetterメソッド
	 * 機能概要 : retryNumをセットする。
	 * @param retryNum
	 */
	public void setRetryNum(int retryNum) {
		this.retryNum = retryNum;
	}
	/**
	 * メソッド名 : timeoutのGetterメソッド
	 * 機能概要 : timeoutを取得する。
	 * @return timeout
	 */
	public int getTimeout() {
		return timeout;
	}
	/**
	 * メソッド名 : timeoutのSetterメソッド
	 * 機能概要 : timeoutをセットする。
	 * @param timeout
	 */
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	/**
	 * メソッド名 : cmdWaitのSetterメソッド
	 * 機能概要 : cmdWaitをセットする。
	 * @param cmdWait
	 */
	public void setCmdWait(int cmdWait) {
		this.cmdWait = cmdWait;
	}
	/**
	 * メソッド名 : cmdWaitのGetterメソッド
	 * 機能概要 : cmdWaitを取得する。
	 * @return cmdWait
	 */
	public int getCmdWait() {
		return cmdWait;
	}
}
