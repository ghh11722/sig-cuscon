/*******************************************************************************
 * Copyright(c) 2011 KDDI CORPORATION All Rights Reserved.
 * プログラム名 : KDDIセキュリティGWシステム カスコン
 * ファイル名   : ProvisionBLogic.java
 *
 * [変更履歴]
 * 日付           更新者                  内容
 * 2011/05/19     inoue@prosite    初版作成
 *******************************************************************************
 */
package jp.co.kddi.secgw.cuscon.pa_command;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import jp.co.kddi.secgw.cuscon.pa_command.common.CheckCommit;
import jp.co.kddi.secgw.cuscon.pa_command.common.CommitStatusManager;
import jp.co.kddi.secgw.cuscon.pa_command.common.CusconConst;
import jp.co.kddi.secgw.cuscon.pa_command.common.CusconUVO;
import jp.co.kddi.secgw.cuscon.pa_command.common.MessageAccessor;
import jp.co.kddi.secgw.cuscon.pa_command.common.ResultJudgeData;
import jp.co.kddi.secgw.cuscon.pa_command.common.TimeoutException;
import jp.co.kddi.secgw.cuscon.pa_command.common.UpdateCommitFlg;
import jp.co.kddi.secgw.cuscon.pa_command.common.UpdateCommitStatus;
import jp.co.kddi.secgw.cuscon.pa_command.common.UpdatePA;
import jp.co.kddi.secgw.cuscon.pa_command.common.vo.CommitFlg;
import jp.co.kddi.secgw.cuscon.pa_command.common.vo.CommitStatus;

import jp.terasoluna.fw.dao.QueryDAO;
import jp.terasoluna.fw.dao.UpdateDAO;
//import jp.terasoluna.fw.transaction.util.TransactionUtil;
import jp.terasoluna.fw.util.PropertyUtil;

/**
 * クラス名 : ProvisionBLogic
 * 機能概要 : プロビジョニングツール
 * 備考 :
 * @author inoue@prosite
 * @version 1.0
 *          Created 2011/05/19
 *          新規作成
 * @see
 */
public class ProvisioningBLogic {

  	private static ApplicationContext context = null;
  	private static DataSourceTransactionManager manager = null;
  	private static CusconUVO uvo = null;
	// QueryDAO
	private static QueryDAO queryDAO;
    // UpdateDAO
	private static UpdateDAO updateDAO;

	public static final String contxtFileName = "applicationContext.xml";

    // 処理区分
    private static final int COMMIT = 0;
	// 日時フォーマット
	private static final String DATE_FORMAT = PropertyUtil.getProperty("fwsetting.common.dateformat");
	// ログ
	private static Logger log = Logger.getLogger("jp.co.kddi.secgw.cuscon.pa_command.ProvisioningBLogic");
    // メッセージクラス
    private static MessageAccessor messageAccessor = null;
    // アプリケーション名
	private static final String APP_NAME = "プロビジョニングツール";
    // VSYSID
	private static final String PROV_VSYSID = "provisioning";

	private static String errmsg = null;
    /**
     * メソッド名 :main
     * 機能概要 : 指定ファイルを読み、FWに対してコマンドを送信する
     * @param param CommitEndの入力クラス
     * @return BLogicResult BLogicResultクラス
     * @see jp.terasoluna.fw.service.thin.BLogic#execute(java.lang.Object)
     */
	public static void main(String[] args) {

		log.info(APP_NAME + ":" + "処理を開始しました。");

		log.debug("指定された引数は、" + args.length + "個です");
		if (args.length != 1) {
			errmsg = "引数が不正です。";
        	log.fatal(errmsg);
        	System.out.println(errmsg);
			System.exit(0);
		}

		log.debug("引数表示");
		for (int i=0; i<args.length; ++i) {
        	log.debug(args[i]);
        }

		// 指定ファイルのチェックを行う
		File file = new File(args[0]);
    	if (!file.exists()){
    		log.fatal("指定されたファイルは存在しません。");
			System.exit(0);
    	}
    	else{
    		if (!file.isFile() || !file.canRead()){
        		log.fatal("指定されたファイルはファイルでないか、読み込みできません。");
    			System.exit(0);
    		}
    	}

		// CusconUVOに固定値を設定する
		uvo = new CusconUVO();
		uvo.setLoginId(PROV_VSYSID);
		uvo.setVsysId(PROV_VSYSID);
		uvo.setGrantFlagStr("0");

        try {
      		context = new ClassPathXmlApplicationContext(contxtFileName);
      		manager = (DataSourceTransactionManager)context.getBean("transactionManager");
        } catch (Exception e) {
			errmsg = APP_NAME + " " + "アプリケーションコンテキストの読み込みに失敗しました。";
        	log.fatal( errmsg + e.getMessage());
			System.exit(0);
		}
        // コミット処理結果格納用エリア
        CommitStatus result = new CommitStatus();
        result.setVsysId(PROV_VSYSID);
        result.setProcessingKind(2);

    	// コミットステータス確認テーブルを更新する。
    	UpdateCommitStatus ucs = new UpdateCommitStatus();

    	// コミットフラグ
        CommitFlg commit_flg = new CommitFlg();
        commit_flg.setCommtFlg(false);
    	Date date;
    	SimpleDateFormat dateFormat;

        try{

        	// コミットステータスの削除（前回結果）及び挿入
	        CommitStatusManager commtStatManager = new CommitStatusManager();
	        commtStatManager.setMessageAccessor(messageAccessor);
        	commtStatManager.deleteCommitStatus(updateDAO, uvo);
	    	// 終了時刻を設定する。
	    	date = new Date();
	    	dateFormat = new SimpleDateFormat(DATE_FORMAT);
	    	result.setStartTime(dateFormat.format(date));
        	commtStatManager.insertCommitStatus(updateDAO, uvo, result);

        	// 処理結果格納用
	        ResultJudgeData resultJudge = new ResultJudgeData();

			//------------------------------------------------------
	        //		メンテナンスモード、コミットチェック start
			//------------------------------------------------------
			log.info("メンテナンスモード及び他利用者のコミット中をチェックしています。");
	        // 他利用者コミット中の場合(DB)
	        // 他利用者コミット中の場合(Palo)
	        // のチェックを行い、結果をresultJudgeクラスのResultFlgにセットする。
	        CheckCommit chkcommit = new CheckCommit();
	        chkcommit.setMessageAccessor(messageAccessor);
	        chkcommit.checkCommitProcess(resultJudge, uvo.getPaLoginId(), uvo.getPaPasswd(), queryDAO, uvo, commit_flg);

			// メンテナンス中の場合
			if(resultJudge.getResultFlg() == 1) {
				// メンテナンス中メッセージ出力
				errmsg = "メンテナンス中のため、処理を中止しました。";
				log.fatal(errmsg);
				result.setMessage(messageAccessor.getMessage(resultJudge.getResultData(), null));
				System.exit(0);
			}

			// 他利用者コミット中の場合
			if(resultJudge.getResultFlg() == 2) {
				// 他利用者コミット中メッセージ出力
				errmsg = "他利用者がコミット中のため、処理を中止しました。";
				log.fatal(errmsg);
	        	// コミット中フラグOFF
	        	if (commit_flg.isCommtFlg()) {
	            	UpdateCommitFlg ucf = new UpdateCommitFlg();
	    			try {
	    				ucf.updatePAProcessFlg(CusconConst.COMMIT_OFF, queryDAO);
	    				commit_flg.setCommtFlg(false);
	   			} catch (Exception e1) {
	    				Object[] objectgArray = {e1.getMessage()};
	    				log.error(messageAccessor.getMessage("EZ190003", objectgArray, uvo));
	    			}
	        	}
	        	result.setMessage(messageAccessor.getMessage(resultJudge.getResultData(), null));
				System.exit(0);
			}
			log.info("メンテナンスモード及び他利用者のコミット中チェックOK。");
			//------------------------------------------------------
			//		メンテナンスモード、コミットチェック end
			//------------------------------------------------------

			//------------------------------------------------------
			//		Paloへのコマンド送出 start
			//------------------------------------------------------
			log.info("ファイルを読み込み、FWにコマンドを送出します。");
	        UpdatePA update = new UpdatePA();
	    	update.updatePAProcess(
	    				file,
	    				resultJudge,
	    				uvo.getPaLoginId(),	uvo.getPaPasswd(),
	    				queryDAO, uvo, messageAccessor);
			//------------------------------------------------------
	    	//		Paloへのコマンド送出 end
			//------------------------------------------------------

			//------------------------------------------------------
	    	//		Paloへのコマンド送出結果判定
			//------------------------------------------------------
	    	// コミットコマンド失敗の場合
			if(resultJudge.getResultFlg() == 3) {
				// メッセージ出力(プロビジョニング処理に失敗しました)
				result.setMessage(messageAccessor.getMessage("EZ190004", null));
				// ステータスを設定する。
				result.setStatus(CusconConst.FAiILURE);
		    	// 終了時刻を設定する。
		    	date = new Date();
		    	dateFormat = new SimpleDateFormat(DATE_FORMAT);
		    	result.setEndTime(dateFormat.format(date));
		    	// コミットステータス確認テーブルを更新する。
		    	ucs.setCommitStatus(updateDAO, result, uvo, messageAccessor);

				// コミット中フラグOFF
				UpdateCommitFlg ucf = new UpdateCommitFlg();
				ucf.updatePAProcessFlg(CusconConst.COMMIT_OFF, queryDAO);
				log.info(APP_NAME + ":" + "処理は異常終了しました。");
				System.exit(0);
			}

			// コミット成功のメッセージ格納
			result.setMessage(messageAccessor.getMessage("EZ190005", null));
			// ステータスを設定する。
			result.setStatus(CusconConst.SUCCESS);
	    	// 終了時刻を設定する。
	    	date = new Date();
	    	dateFormat = new SimpleDateFormat(DATE_FORMAT);
	    	result.setEndTime(dateFormat.format(date));
	    	// コミットステータス確認テーブルを更新する。
	    	ucs.setCommitStatus(updateDAO, result, uvo, messageAccessor);

	    	// コミット中フラグOFF
			UpdateCommitFlg ucf = new UpdateCommitFlg();
			ucf.updatePAProcessFlg(CusconConst.COMMIT_OFF, queryDAO);

        } catch(TimeoutException e) {
        	// コミット中フラグOFF
        	if (commit_flg.isCommtFlg()) {
            	UpdateCommitFlg ucf = new UpdateCommitFlg();
    			try {
    				ucf.updatePAProcessFlg(CusconConst.COMMIT_OFF, queryDAO);
    				commit_flg.setCommtFlg(false);
   			} catch (Exception e1) {
    				Object[] objectgArray = {e1.getMessage()};
    				log.error(messageAccessor.getMessage("EZ190003", objectgArray, uvo));
    			}
        	}
        	// コマンドタイムアウトメッセージ出力
        	errmsg = "コマンドタイムアウト";
			log.fatal(errmsg);
			Object[] objectgArray = {errmsg};
			result.setMessage(messageAccessor.getMessage("EZ190004", objectgArray));
			log.info(APP_NAME + ":" + "処理は異常終了しました。");
			System.exit(0);

        } catch(Exception e) {
        	// コミット中フラグOFF
        	if (commit_flg.isCommtFlg()) {
            	UpdateCommitFlg ucf = new UpdateCommitFlg();
    			try {
    				ucf.updatePAProcessFlg(CusconConst.COMMIT_OFF, queryDAO);
    				commit_flg.setCommtFlg(false);
    			} catch (Exception e1) {
    				Object[] objectgArray = {e1.getMessage()};
    				log.error(messageAccessor.getMessage("EZ190003", objectgArray, uvo));
    			}
        	}
			// コミット処理失敗ログ出力
			Object[] objectgArray = {e.getMessage()};
			log.error(messageAccessor.getMessage("EZ190001", objectgArray, uvo));
			result.setMessage(messageAccessor.getMessage("EZ190004", null));
			log.info(APP_NAME + ":" + "処理は異常終了しました。");
			System.exit(0);
		}
        result.setMessage("success");
		log.info(APP_NAME + ":" + "処理は正常終了しました。");
		System.exit(0);
	}

	/**
	 * メソッド名 : コミットステータス登録
	 * 機能概要 : コミットステータステーブルの登録
	 * @param uvo
	 * @return StartTime
	 * @throws Exception
	 */
	private static String insertCommitStatus(CusconUVO uvo) throws Exception {
		log.debug("insertCommitStatus処理開始");

		// コミットステータスの設定
		CommitStatus commitStatus = new CommitStatus();
		commitStatus.setProcessingKind(COMMIT);
		commitStatus.setStatus(CusconConst.STAT_COMMITTING);
		commitStatus.setMessage(messageAccessor.getMessage("EZ190006", null));
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		commitStatus.setStartTime(dateFormat.format(date));
		commitStatus.setEndTime(null);

		CommitStatusManager commtStatManager = new CommitStatusManager();
		commtStatManager.insertCommitStatus(updateDAO, uvo, commitStatus);

		log.debug("insertCommitStatus処理終了 StartTime = " + commitStatus.getStartTime());
		return commitStatus.getStartTime();
	}

    /**
	 * メソッド名 : queryDAOのSetterメソッド
	 * 機能概要 : queryDAOをセットする。
	 * @param queryDAO
	 */
	public void setQueryDAO(QueryDAO queryDAO) {
		log.debug("setQueryDAO処理開始");
		ProvisioningBLogic.queryDAO = queryDAO;
		log.debug("setQueryDAO処理終了");
	}

	/**
	 * メソッド名 : updateDAOのSetterメソッド
	 * 機能概要 : updateDAOをセットする。
	 * @param updateDAO
	 */
	public void setUpdateDAO(UpdateDAO updateDAO) {
		log.debug("setUpdateDAO処理開始");
		ProvisioningBLogic.updateDAO = updateDAO;
		log.debug("setUpdateDAO処理終了");
	}
	/**
	 * メソッド名 : messageAccessorのSetterメソッド
	 * 機能概要 : messageAccessorをセットする。
	 * @param messageAccessor
	 */
	public void setMessageAccessor(MessageAccessor messageAccessor) {
		log.debug("setMessageAccessor処理開始");
		ProvisioningBLogic.messageAccessor = messageAccessor;
		log.debug("setMessageAccessor処理終了");
	}
}
